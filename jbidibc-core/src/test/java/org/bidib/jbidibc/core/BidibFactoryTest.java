package org.bidib.jbidibc.core;

import java.io.File;

import javax.xml.bind.JAXBException;

import org.bidib.jbidibc.core.bidib.DummyBidib;
import org.bidib.jbidibc.core.schema.BidibProductsFactory;
import org.bidib.jbidibc.core.schema.bidib.products.ProductType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BidibFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibFactoryTest.class);

    private static final long UUID_HUB = 0x81000d72006aeaL;

    private static final long UUID_LC = 0x05340d6B001238L;

    private static final long UUID_MULTIDEC = 0x05343E97001238L;

    private static final long UUID_INVALID_VID = 0x0534016B901238L;

    private static final long UUID_INVALID_PID = 0x05340d00901238L;

    private static final long UUID_BOOSTER_DEV = 0x05343E92FFFFFFL;

    @Test
    public void createBidib() {

        String clazzName = "org.bidib.jbidibc.core.bidib.DummyBidib";

        BidibInterface bidib = BidibFactory.createBidib(clazzName);

        Assert.assertNotNull(bidib);
        Assert.assertTrue(bidib instanceof DummyBidib);
    }

    @Test
    public void loadProductForHub() throws JAXBException {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/classes/xml/BiDiBProducts");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        final Node node = new Node(1, new byte[] { 0 }, UUID_HUB);

        ProductType product = new BidibProductsFactory().loadProductForNode(node, file.getAbsolutePath());
        Assert.assertNotNull(product);
        LOGGER.debug("Returned product: {}", product);
        Assert.assertEquals("OneHub", product.getName());
        Assert.assertNotNull(product.getDocumentation());
        Assert.assertEquals(product.getDocumentation().size(), 2);
    }

    @Test
    public void loadProductForLC() throws JAXBException {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/classes/xml/BiDiBProducts");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        final Node node = new Node(1, new byte[] { 0 }, UUID_LC);

        ProductType product = new BidibProductsFactory().loadProductForNode(node, file.getAbsolutePath());
        Assert.assertNotNull(product);
        LOGGER.debug("Returned product: {}", product);
        Assert.assertEquals("LightControl 1", product.getName());
        Assert.assertNotNull(product.getDocumentation());
        Assert.assertEquals(product.getDocumentation().size(), 2);
    }

    @Test
    public void loadProductForMultiDecoder() throws JAXBException {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/classes/xml/BiDiBProducts");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        final Node node = new Node(1, new byte[] { 0 }, UUID_MULTIDEC);

        ProductType product = new BidibProductsFactory().loadProductForNode(node, file.getAbsolutePath());
        Assert.assertNotNull(product);
        LOGGER.debug("Returned product: {}", product);
        Assert.assertEquals("Multi-Decoder", product.getName());
        Assert.assertNotNull(product.getDocumentation());
        Assert.assertEquals(product.getDocumentation().size(), 2);
    }

    @Test
    public void loadProductForTamsBoosterDev() throws JAXBException {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/classes/xml/BiDiBProducts");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        final Node node = new Node(1, new byte[] { 0 }, UUID_BOOSTER_DEV);
        node.setRelevantPidBits(8);

        ProductType product = new BidibProductsFactory().loadProductForNode(node, file.getAbsolutePath());
        Assert.assertNotNull(product);
        LOGGER.debug("Returned product: {}", product);
        Assert.assertEquals("Booster", product.getName());
        Assert.assertNotNull(product.getDocumentation());
        Assert.assertEquals(product.getDocumentation().size(), 2);
    }

    @Test
    public void loadProductForInvalidVID() throws JAXBException {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/classes/xml/BiDiBProducts");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        final Node node = new Node(1, new byte[] { 0 }, UUID_INVALID_VID);

        ProductType product =
            new BidibProductsFactory().loadProductForNode(node, file.getAbsolutePath(), "/BiDiBProducts");
        Assert.assertNull(product);
    }

    @Test
    public void loadProductForInvalidPID() throws JAXBException {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/classes/xml/BiDiBProducts");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        final Node node = new Node(1, new byte[] { 0 }, UUID_INVALID_PID);

        ProductType product = new BidibProductsFactory().loadProductForNode(node, file.getAbsolutePath());
        Assert.assertNull(product);
    }
}
