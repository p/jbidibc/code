package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcPlusBindAcceptedData extends RcPlusDecoderAnswerData {

    private static final Logger LOGGER = LoggerFactory.getLogger(RcPlusBindAcceptedData.class);

    private final AddressData address;

    public RcPlusBindAcceptedData(RcPlusAcknowledge acknState, DecoderUniqueIdData uniqueIdData, AddressData address) {
        super(acknState, uniqueIdData);
        this.address = address;
    }

    public AddressData getAddress() {
        return address;
    }

    /**
     * Create a RcPlusBindAcceptedData instance from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset to start
     * @return the RcPlusBindAcceptedData instance
     */
    public static RcPlusBindAcceptedData fromByteArray(byte[] data, int offset) {
        if (data.length < (offset + 7)) {
            LOGGER.warn("The size of the provided data does not meet the expected length.");
            throw new IllegalArgumentException(
                "The size of the provided data does not meet the expected length (=7). Provided length: " + data.length);
        }

        RcPlusAcknowledge acknState = RcPlusAcknowledge.valueOf(data[offset]);
        DecoderUniqueIdData uniqueIdData = DecoderUniqueIdData.fromByteArray(data, offset + 1);
        AddressData address = AddressData.fromByteArray(data, offset + 1 + 5);

        RcPlusBindAcceptedData answerData = new RcPlusBindAcceptedData(acknState, uniqueIdData, address);

        LOGGER.trace("Return RcPlusBindAcceptedData: {}", answerData);
        return answerData;
    }

    public void writeToStream(ByteArrayOutputStream out) {
        getUniqueId().writeToStream(out);
        getAddress().writeToStream(out);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
