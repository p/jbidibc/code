package org.bidib.jbidibc.core.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ProductUtilsTest {

    @Test
    public void isLightControl() {
        long uniqueId = 0x05000d6B001234L;
        Assert.assertTrue(ProductUtils.isLightControl(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isMobaList() {
        long uniqueId = 0x05000D6C003300L;
        Assert.assertTrue(ProductUtils.isMobaList(uniqueId));
        Assert.assertFalse(ProductUtils.isLightControl(uniqueId));
    }

    @Test
    public void isOneDMX() {
        long uniqueId = 0x05000D73001235L;
        Assert.assertTrue(ProductUtils.isOneDMX(uniqueId));
        Assert.assertFalse(ProductUtils.isLightControl(uniqueId));
    }

    @Test
    public void isOneControl() {
        long uniqueId = 0x05000d750045ebL;
        Assert.assertTrue(ProductUtils.isOneControl(uniqueId));
        Assert.assertFalse(ProductUtils.isLightControl(uniqueId));
    }

    @Test
    public void isOneHub() {
        long uniqueId = 0x81000D72006AEAL;
        Assert.assertTrue(ProductUtils.isOneHub(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));

        Assert.assertTrue(NodeUtils.hasSubNodesFunctions(uniqueId));
    }

    @Test
    public void isOneOC() {
        // 20:01:31.459 [INFO] org.bidib.wizard.comm.bidib.BidibCommunication [Thread-12] - Magic of new node: 45054,
        // node: Node[version=1,addr=[8],uniqueId=0x40000dcc009dea]

        long uniqueId = 0x040000dcc009deaL;
        Assert.assertTrue(ProductUtils.isOneOC(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isOneServoTurn() {
        long uniqueId = 0x05000D79001234L;
        Assert.assertTrue(ProductUtils.isOneServoTurn(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));

        Assert.assertTrue(NodeUtils.hasAccessoryFunctions(uniqueId));
        Assert.assertTrue(NodeUtils.hasSwitchFunctions(uniqueId));
    }

    @Test
    public void isSTu() {
        long uniqueId = 0x05000D7B001234L;
        Assert.assertTrue(ProductUtils.isSTu(uniqueId));
        Assert.assertFalse(ProductUtils.isOneServoTurn(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));

        Assert.assertTrue(NodeUtils.hasAccessoryFunctions(uniqueId));
        Assert.assertTrue(NodeUtils.hasSwitchFunctions(uniqueId));
    }

    @Test
    public void isOneDriveTurn() {
        long uniqueId = 0x05000D7A001234L;
        Assert.assertTrue(ProductUtils.isOneDriveTurn(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));

        Assert.assertTrue(NodeUtils.hasAccessoryFunctions(uniqueId));
        Assert.assertTrue(NodeUtils.hasSwitchFunctions(uniqueId));
    }

    @Test
    public void isOneBootloader() {
        long uniqueId = 0x00000d700009ebL;
        Assert.assertTrue(ProductUtils.isOneBootloader(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isWs2812() {
        long uniqueId = 0x05000d9F001234L;
        Assert.assertTrue(ProductUtils.isWs2812(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isStepControl() {
        long uniqueId = 0x05000d78001234L;
        Assert.assertTrue(ProductUtils.isStepControl(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isNeoControlLight() {
        long uniqueId = 0x05000dcd001234L;
        Assert.assertTrue(ProductUtils.isNeoControl(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isNeoControlSignal() {
        long uniqueId = 0x05000dce001234L;
        Assert.assertTrue(ProductUtils.isNeoControl(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isNeoEWS() {
        long uniqueId = 0x05000d81001234L;
        Assert.assertTrue(ProductUtils.isNeoEWS(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isRFBasisNode() {
        long uniqueId = 0x50000d02808a00L;
        Assert.assertTrue(ProductUtils.isRFBasisNode(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }

    @Test
    public void isMultiDecoder() {
        long uniqueId = 0x04003e970004f2L;
        Assert.assertTrue(ProductUtils.isMultiDecoder(uniqueId));
        Assert.assertFalse(ProductUtils.isOneControl(uniqueId));
    }
}
