package org.bidib.jbidibc.rxtx;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.exception.InvalidLibraryException;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.serial.AbstractSerialBidib;
import org.bidib.jbidibc.serial.SerialMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gnu.io.CommPortIdentifier;
import gnu.io.CommPortOwnershipListener;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

/**
 * This is the default bidib implementation. It creates and initializes the MessageReceiver and the NodeFactory that is
 * used in the system.
 * 
 */
public final class RxtxSerialBidib extends AbstractSerialBidib {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxtxSerialBidib.class);

    private static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("RAW");

    // private static final Logger MSG_OUTPUTSTREAM_LOGGER = LoggerFactory.getLogger("OPS");

    private SerialPort port;

    private CommPortOwnershipListener commPortOwnershipListener;

    private CommPortIdentifier commPort;

    private byte[] inputBuffer = new byte[2048];

    private boolean useHardwareFlowControl;

    private RxtxSerialBidib() {
    }

    /**
     * Get a new initialized instance of RxtxSerialBidib.
     *
     * @return the instance of RxtxSerialBidib
     */
    public static BidibInterface createInstance() {
        LOGGER.info("Create new instance of RxtxSerialBidib.");

        RxtxSerialBidib instance = new RxtxSerialBidib();
        instance.initialize();

        return instance;
    }

    @Override
    public void close() {
        if (port != null) {
            LOGGER.info("Start closing the port: {}", port);
            long start = System.currentTimeMillis();

            // Deactivate DTR
            if (this.useHardwareFlowControl) {
                try {
                    LOGGER.info("Deactivate DTR.");

                    port.setDTR(false); // pin 1 in DIN8; on main connector, this is DTR
                }
                catch (Exception e) {
                    LOGGER.warn("Set DTR to false failed.", e);
                }

                // Deactivate RTS
                try {
                    LOGGER.info("Deactivate RTS.");
                    port.setRTS(false);
                }
                catch (Exception e) {
                    LOGGER.warn("Set RTS to false failed.", e);
                }
            }

            // this makes the close operation faster ...
            try {
                LOGGER.info("Remove event listener.");
                port.removeEventListener();
                LOGGER.info("Removed event listener.");
                // port.enableReceiveTimeout(200);
            }
            catch (Exception e) {
                LOGGER.warn("Remove event listener failed.", e);
            }

            final BidibMessageProcessor serialMessageReceiver = getMessageReceiver();
            stopReceiverAndQueues(serialMessageReceiver);

            try {
                if (this.useHardwareFlowControl) {
                    fireCtsChanged(port.isCTS());
                }
                else {
                    fireCtsChanged(false);
                }
            }
            catch (Exception e) {
                LOGGER.warn("Get CTS value failed.", e);
            }

            LOGGER.info("Close the port.");
            try {
                port.close();
            }
            catch (Exception e) {
                LOGGER.warn("Close port failed.", e);
            }

            fireCtsChanged(false);

            long end = System.currentTimeMillis();
            LOGGER.info("Closed the port. duration: {}", end - start);

            if (commPortOwnershipListener != null) {
                LOGGER.info("Remove the PortOwnershipListener from commPort: {}", commPort);
                try {
                    commPort.removePortOwnershipListener(commPortOwnershipListener);
                }
                catch (Exception ex) {
                    LOGGER.warn("Remove port ownership listener failed.", ex);
                }
                commPortOwnershipListener = null;
            }

            port = null;
            commPort = null;

            cleanupAfterClose(serialMessageReceiver);
        }
        else {
            LOGGER.info("No port to close available.");
        }
    }

    @Override
    public List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new ArrayList<String>();

        try {
            // get the comm port identifiers
            Enumeration<?> e = CommPortIdentifier.getPortIdentifiers();
            while (e.hasMoreElements()) {
                CommPortIdentifier id = (CommPortIdentifier) e.nextElement();
                LOGGER.debug("Process current CommPortIdentifier, name: {}, portType: {}", id.getName(),
                    id.getPortType());

                if (id.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                    portIdentifiers.add(id.getName());
                }
                else {
                    LOGGER.debug("Skip port because no serial port, name: {}, portType: {}", id.getName(),
                        id.getPortType());
                }
            }
        }
        catch (UnsatisfiedLinkError ule) {
            LOGGER.warn("Get comm port identifiers failed.", ule);
            throw new InvalidLibraryException(ule.getMessage(), ule.getCause());
        }
        catch (Error error) {
            LOGGER.warn("Get comm port identifiers failed.", error);
            throw new RuntimeException(error.getMessage(), error.getCause());
        }
        return portIdentifiers;
    }

    @Override
    protected boolean isImplAvaiable() {
        return (port != null);
    }

    @Override
    protected void internalOpen(String portName, final Context context) throws Exception {

        final BidibMessageProcessor serialMessageReceiver = getMessageReceiver();

        if (commPort == null) {
            try {
                commPort = CommPortIdentifier.getPortIdentifier(portName);
            }
            catch (NoSuchPortException ex) {
                LOGGER.warn("Requested port is not available: {}", portName, ex);
                throw new PortNotFoundException(portName);
            }
        }
        else {
            LOGGER.info("Use existing commPort: {}", commPort);
        }

        if (commPortOwnershipListener == null) {
            commPortOwnershipListener = new CommPortOwnershipListener() {

                @Override
                public void ownershipChange(int type) {
                    LOGGER.info("Ownership changed, type: {}", type);
                }
            };
        }
        commPort.addPortOwnershipListener(commPortOwnershipListener);

        // open the port
        final SerialPort serialPort = (SerialPort) commPort.open(RxtxSerialBidib.class.getName(), 2000);

        Boolean useHardwareFlowControl = context.get("serial.useHardwareFlowControl", Boolean.class, Boolean.TRUE);

        LOGGER.info("Open port with portName: {}, useHardwareFlowControl: {}", portName, useHardwareFlowControl);

        if (useHardwareFlowControl) {
            LOGGER
                .info("Set flow control mode to SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT!");
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);

            this.useHardwareFlowControl = true;
        }
        else {
            LOGGER.info("Set flow control mode to SerialPort.FLOWCONTROL_NONE!");
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);

            this.useHardwareFlowControl = false;
        }

        Integer baudRate = context.get("serial.baudrate", Integer.class, Integer.valueOf(115200));
        LOGGER.info("Open port with baudRate: {}", baudRate);

        serialPort.setSerialPortParams(baudRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

        startReceiverAndQueues(serialMessageReceiver, context);

        final SerialPortEventListener eventListener = new SerialPortEventListener() {

            @Override
            public void serialEvent(SerialPortEvent event) {
                // this callback is called every time data is available
                LOGGER.trace("serialEvent received: {}", event);
                switch (event.getEventType()) {
                    case SerialPortEvent.DATA_AVAILABLE:
                        // MSG_OUTPUTSTREAM_LOGGER.info("<<<<");
                        try {
                            synchronized (receiveLock) {
                                InputStream input = serialPort.getInputStream();
                                int len = -1;
                                try {
                                    len = input.read(inputBuffer, 0, inputBuffer.length);

                                    int remaining = input.available();
                                    if (remaining > 0) {
                                        LOGGER.warn("More data in inputStream might be available, remaining: {}",
                                            remaining);
                                    }
                                }
                                catch (IOException ex) {
                                    LOGGER.warn("Read data from input stream to buffer failed.", ex);
                                }

                                if (len > -1) {
                                    receive(inputBuffer, len);
                                }
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.error("Message receiver returned from receive with an exception!", ex);
                        }
                        break;
                    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                        LOGGER.trace("The output buffer is empty.");

                        // // only the sendQueueWorker must release the semaphore
                        // if (sendQueueWorkerThreadId.get() == Thread.currentThread().getId()) {
                        // MSG_OUTPUTSTREAM_LOGGER.info(">>>> OBE");
                        // // sendSemaphore.release();
                        // }
                        break;
                    case SerialPortEvent.CD:
                        LOGGER.warn("CD is signalled.");
                        break;
                    case SerialPortEvent.CTS:
                        LOGGER.warn("The CTS value has changed, old value: {}, new value: {}",
                            new Object[] { event.getOldValue(), event.getNewValue() });

                        // TODO this should only stop sending messages but not close the port
                        fireCtsChanged(event.getNewValue());

                        if (event.getNewValue() == false) {
                            LOGGER.warn("Close the port.");
                            Thread worker = new Thread(new Runnable() {
                                public void run() {

                                    LOGGER.info("Start close port because error was detected.");
                                    try {
                                        // the listeners are notified in close()
                                        close();
                                    }
                                    catch (Exception ex) {
                                        LOGGER.warn("Close after error failed.", ex);
                                    }
                                    LOGGER.warn("The port was closed.");
                                }
                            });
                            worker.start();
                        }
                        break;
                    case SerialPortEvent.OE:
                        LOGGER.warn("OE (overrun error) is signalled.");
                        break;
                    case SerialPortEvent.DSR:
                        LOGGER.warn("DSR is signalled.");
                        break;
                    default:
                        LOGGER.warn("SerialPortEvent was triggered, type: {}, old value: {}, new value: {}",
                            new Object[] { event.getEventType(), event.getOldValue(), event.getNewValue() });
                        break;
                }
            }
        };
        serialPort.addEventListener(eventListener);

        // react on port removed ...
        serialPort.notifyOnCTS(true);
        serialPort.notifyOnCarrierDetect(true);
        serialPort.notifyOnBreakInterrupt(true);
        serialPort.notifyOnDSR(true);
        serialPort.notifyOnOverrunError(true);
        serialPort.notifyOnOutputEmpty(true);

        if (this.useHardwareFlowControl) {
            // Activate DTR
            try {
                LOGGER.info("Activate DTR.");

                serialPort.setDTR(true); // pin 1 in DIN8; on main connector, this is DTR
            }
            catch (Exception e) {
                LOGGER.warn("Set DTR true failed.", e);
            }
        }

        LOGGER.info("Let the serial port notify data available.");
        serialPort.notifyOnDataAvailable(true);

        // // react on port removed ...
        // serialPort.notifyOnCTS(true);
        // serialPort.notifyOnCarrierDetect(true);
        // serialPort.notifyOnBreakInterrupt(true);
        // serialPort.notifyOnDSR(true);
        // serialPort.notifyOnOverrunError(true);
        // serialPort.notifyOnOutputEmpty(true);

        if (this.useHardwareFlowControl) {
            // Activate RTS
            try {
                LOGGER.info("Activate RTS.");
                serialPort.setRTS(true);
            }
            catch (Exception e) {
                LOGGER.warn("Set RTS true failed.", e);
            }
        }

        try {
            if (this.useHardwareFlowControl) {
                fireCtsChanged(serialPort.isCTS());
            }
            else {
                fireCtsChanged(true);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Get CTS value failed.", e);
        }

        // keep the port
        port = serialPort;
    }

    @Override
    public boolean isOpened() {
        boolean isOpened = false;
        try {
            LOGGER.debug("Check if port is opened: {}", port);
            isOpened = (port != null && port.getOutputStream() != null);
        }
        catch (IOException ex) {
            LOGGER.warn("OutputStream is not available.", ex);
        }
        return isOpened;
    }

    private final ByteArrayOutputStream sendBuffer = new ByteArrayOutputStream(2048);

    @Override
    protected void sendData(ByteArrayOutputStream data) {

        if (port != null && data != null) {
            try {
                sendBuffer.reset();

                OutputStream os = port.getOutputStream();

                if (!firstPacketSent) {
                    LOGGER.info("Send initial sequence.");

                    try {
                        byte[] initialSequence = new byte[] { (byte) BidibLibrary.BIDIB_PKT_MAGIC };
                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
                            MSG_RAW_LOGGER.info(">> [{}] - {}", initialSequence.length,
                                ByteUtils.bytesToHex(initialSequence));
                        }
                        os.write(initialSequence);
                        Thread.sleep(10);
                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
                            MSG_RAW_LOGGER.info(">> [{}] - {}", initialSequence.length,
                                ByteUtils.bytesToHex(initialSequence));
                        }
                        os.write(initialSequence);

                        firstPacketSent = true;

                        LOGGER.info("Send initial sequence passed.");
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Send initial sequence failed.", ex);
                    }
                }

                SerialMessageEncoder.encodeMessage(data, sendBuffer);

                if (MSG_RAW_LOGGER.isInfoEnabled()) {
                    MSG_RAW_LOGGER.info(">> [{}] - {}", sendBuffer.toByteArray().length,
                        ByteUtils.bytesToHex(sendBuffer.toByteArray()));
                }

                os.write(sendBuffer.toByteArray());

                os.flush();
            }
            catch (Exception ex) {
                byte[] bytes = data.toByteArray();

                LOGGER.warn("Send message to output stream failed: [{}] - {}", data.size(),
                    ByteUtils.bytesToHex(bytes));

                throw new RuntimeException("Send message to output stream failed: " + ByteUtils.bytesToHex(bytes), ex);
            }
            finally {
                sendBuffer.reset();
            }
        }

    }
}
