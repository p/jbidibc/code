package org.bidib.jbidibc.decoder.schema.decoderfirmware;

import org.bidib.jbidibc.core.schema.exception.InvalidContentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DecoderFirmwareDefinitionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderFirmwareDefinitionTest.class);

    @Test
    public void loadFirmwareDefinition2() {
        String filePath = "/firmware/97/Firmware_97_1.06_sound.xml";

        filePath = DecoderFirmwareDefinitionTest.class.getResource(filePath).getPath();
        LOGGER.info("Load firmwareDefinition from path: {}", filePath);

        DecoderFirmwareDefinitionFactory factory = new DecoderFirmwareDefinitionFactory();
        DecoderFirmwareDefinition firmwareDefinition = factory.loadFirmwareDefinition(filePath);

        Assert.assertNotNull(firmwareDefinition);
    }

    @Test(expectedExceptions = InvalidContentException.class)
    public void loadFirmwareDefinition3() {
        String filePath = "/firmware/invalid/Firmware_invalid_1.0.xml";

        filePath = DecoderFirmwareDefinitionTest.class.getResource(filePath).getPath();
        LOGGER.info("Load firmwareDefinition from path: {}", filePath);

        DecoderFirmwareDefinitionFactory factory = new DecoderFirmwareDefinitionFactory();
        DecoderFirmwareDefinition firmwareDefinition = factory.loadFirmwareDefinition(filePath);

        Assert.assertNotNull(firmwareDefinition);
    }

    @Test
    public void loadFirmwareDefinition4() {
        String filePath = "/firmware/97/Firmware_97_3.06.xml";

        filePath = DecoderFirmwareDefinitionTest.class.getResource(filePath).getPath();
        LOGGER.info("Load firmwareDefinition from path: {}", filePath);

        DecoderFirmwareDefinitionFactory factory = new DecoderFirmwareDefinitionFactory();
        DecoderFirmwareDefinition firmwareDefinition = factory.loadFirmwareDefinition(filePath);

        Assert.assertNotNull(firmwareDefinition);
    }
}
