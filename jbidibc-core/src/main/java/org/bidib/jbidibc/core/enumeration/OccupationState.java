package org.bidib.jbidibc.core.enumeration;

public enum OccupationState implements BidibEnum {
    // @formatter:off
    OCCUPIED(0), FREE(1);
    // @formatter:on

    private final byte type;

    OccupationState(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create an occupation state.
     * 
     * @param type
     *            numeric value of the occupation state
     * 
     * @return the occupation state
     */
    public static OccupationState valueOf(byte type) {
        OccupationState result = null;

        for (OccupationState e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to an occupation state");
        }
        return result;
    }
}
