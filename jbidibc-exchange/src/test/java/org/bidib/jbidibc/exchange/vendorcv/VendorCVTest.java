package org.bidib.jbidibc.exchange.vendorcv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.helpers.DefaultContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

public class VendorCVTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(VendorCVTest.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.exchange.vendorcv";

    private static final String XSD_LOCATION = "xsd/vendor_cv.xsd";

    private static final String EXPORTED_CVDEF_TARGET_DIR = "target/vendorcv";

    private JAXBContext jaxbContext;

    @BeforeTest
    public void prepare() {
        LOGGER.info("Create report directory: {}", EXPORTED_CVDEF_TARGET_DIR);

        try {
            FileUtils.forceMkdir(new File(EXPORTED_CVDEF_TARGET_DIR));
        }
        catch (IOException e) {
            LOGGER.warn("Create report directory failed: " + EXPORTED_CVDEF_TARGET_DIR, e);
        }

        LOGGER.info("Create JAXB context.");
        try {
            jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
        }
        catch (JAXBException ex) {
            LOGGER.error("Create JAXB context failed.", ex);

            throw new RuntimeException("Create JAXB context failed.", ex);
        }
    }

    @Test
    public void saveVendorCVLCTest() throws JAXBException, SAXException, IOException {

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, XSD_LOCATION);

        VersionInfoType versionInfo = new VersionInfoType();
        versionInfo.setAuthor("Max Mustermann");
        versionInfo.setVersion("0.1");
        versionInfo.setVendor("013");
        versionInfo.setPid("107");
        versionInfo.setDescription("BiDiB-LightControl 1");

        VendorCV vendorCV = new VendorCV();
        vendorCV.setVersion(versionInfo);

        TemplatesType templatesType = new TemplatesType();

        TemplateType ledTemplate = new TemplateType();
        ledTemplate.setName("LED");

        templatesType.getTemplate().add(ledTemplate);

        CVType cv = new CVType();
        cv.setNumber("0");
        cv.setType(DataType.BYTE);
        cv.setMode(ModeType.RW);
        DescriptionType desc = new DescriptionType().withLang("de-DE").withText("LED: Einstellung der Stromquelle");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("LED: courrent source setup");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("-");
        cv.setHigh("-");
        cv.setValues("-");
        ledTemplate.getCVOrNodeOrRepeater().add(cv);

        cv = new CVType();
        cv.setNumber("1");
        cv.setType(DataType.BYTE);
        cv.setMode(ModeType.RW);
        desc = new DescriptionType().withLang("de-DE").withText("LED: Helligkeit für Zustand „aus“");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("LED: light intensity at status 'off'");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("-");
        cv.setHigh("-");
        cv.setValues("-");
        ledTemplate.getCVOrNodeOrRepeater().add(cv);

        TemplateType servoTemplate = new TemplateType();
        servoTemplate.setName("Servo");

        templatesType.getTemplate().add(servoTemplate);

        cv = new CVType();
        cv.setNumber("0");
        cv.setType(DataType.INT);
        cv.setMode(ModeType.RW);
        desc = new DescriptionType().withLang("de-DE").withText("Min Low");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("Min Low");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("0");
        cv.setHigh("1");
        cv.setValues("-");
        servoTemplate.getCVOrNodeOrRepeater().add(cv);

        cv = new CVType();
        cv.setNumber("1");
        cv.setType(DataType.INT);
        cv.setMode(ModeType.RW);
        desc = new DescriptionType().withLang("de-DE").withText("Min High");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("Min High");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("0");
        cv.setHigh("1");
        cv.setValues("-");
        servoTemplate.getCVOrNodeOrRepeater().add(cv);

        vendorCV.setTemplates(templatesType);

        File exportFile = new File(EXPORTED_CVDEF_TARGET_DIR, "vendorcv_LC1.xml");
        OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFile));

        marshaller.marshal(vendorCV, new OutputStreamWriter(os, Charset.forName("UTF-8")));

        os.flush();
    }

    @Test
    public void saveVendorCVSignedCharTest() throws JAXBException, SAXException, IOException {

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, XSD_LOCATION);

        VersionInfoType versionInfo = new VersionInfoType();
        versionInfo.setAuthor("Max Mustermann");
        versionInfo.setVersion("0.1");
        versionInfo.setVendor("013");
        versionInfo.setPid("999");
        versionInfo.setDescription("BiDiB-Test Signed Char 1");

        VendorCV vendorCV = new VendorCV();
        vendorCV.setVersion(versionInfo);

        TemplatesType templatesType = new TemplatesType();

        TemplateType ledTemplate = new TemplateType();
        ledTemplate.setName("LED");

        templatesType.getTemplate().add(ledTemplate);

        CVType cv = new CVType();
        cv.setNumber("0");
        cv.setType(DataType.SIGNED_CHAR);
        cv.setMode(ModeType.RW);
        DescriptionType desc = new DescriptionType().withLang("de-DE").withText("Temperatur Offset");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("Temperature offset");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("-");
        cv.setHigh("-");
        cv.setValues("-");
        ledTemplate.getCVOrNodeOrRepeater().add(cv);

        vendorCV.setTemplates(templatesType);

        File exportFile = new File(EXPORTED_CVDEF_TARGET_DIR, "vendorcv_signedChar.xml");
        OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFile));

        marshaller.marshal(vendorCV, new OutputStreamWriter(os, Charset.forName("UTF-8")));

        os.flush();
    }

    @Test
    public void saveVendorCVWithLowAndHighbitsTest() throws JAXBException, SAXException, IOException {

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, XSD_LOCATION);

        VersionInfoType versionInfo = new VersionInfoType();
        versionInfo.setAuthor("Max Mustermann");
        versionInfo.setVersion("0.1");
        versionInfo.setVendor("013");
        versionInfo.setPid("107");
        versionInfo.setDescription("BiDiB-LightControl 1");

        VendorCV vendorCV = new VendorCV();
        vendorCV.setVersion(versionInfo);

        TemplatesType templatesType = new TemplatesType();

        TemplateType ledTemplate = new TemplateType();
        ledTemplate.setName("LED");

        templatesType.getTemplate().add(ledTemplate);

        CVType cv = new CVType();
        TemplateType servoTemplate = new TemplateType();
        servoTemplate.setName("Servo");

        templatesType.getTemplate().add(servoTemplate);

        cv = new CVType();
        cv.setNumber("0");
        cv.setType(DataType.INT);
        cv.setMode(ModeType.RW);
        DescriptionType desc = new DescriptionType().withLang("de-DE").withText("Min Low");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("Min Low");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("0");
        cv.setHigh("1");
        cv.setValues("-");
        cv.setLowbits(1);
        cv.setHighbits(2);
        servoTemplate.getCVOrNodeOrRepeater().add(cv);

        cv = new CVType();
        cv.setNumber("1");
        cv.setType(DataType.INT);
        cv.setMode(ModeType.RW);
        desc = new DescriptionType().withLang("de-DE").withText("Min High");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("Min High");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("0");
        cv.setHigh("1");
        cv.setValues("-");
        cv.setLowbits(2);
        cv.setHighbits(1);
        servoTemplate.getCVOrNodeOrRepeater().add(cv);

        vendorCV.setTemplates(templatesType);

        File exportFile = new File(EXPORTED_CVDEF_TARGET_DIR, "vendorcv_lowandhighbits.xml");
        OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFile));

        marshaller.marshal(vendorCV, new OutputStreamWriter(os, Charset.forName("UTF-8")));

        os.flush();
    }

    private static final long UUID_ONECONTROL = 0x05000d75002e00L;

    private static final long UUID_ONEDMX = 0x05340D73001235L;

    @Test
    public void loadCvDefinitionsOneControl() {
        Node node = new Node(0, new byte[] { 1 }, UUID_ONECONTROL);
        node.setSoftwareVersion(new SoftwareVersion(1, 0, 0));

        final Context context = new DefaultContext();

        VendorCvData vendorCV = VendorCvFactory.getCvDefinition(node, context, "classpath:/bidib");
        Assert.assertNotNull(vendorCV);
    }

    @Test
    public void loadCvDefinitionsOneDMX() {
        Node nodeOneDmx = new Node(0, new byte[] { 1 }, UUID_ONEDMX);
        nodeOneDmx.setSoftwareVersion(new SoftwareVersion(1, 0, 1));

        final Context context = new DefaultContext();

        VendorCvData vendorCVOneDmx = VendorCvFactory.getCvDefinition(nodeOneDmx, context, "classpath:/bidib-test");
        Assert.assertNotNull(vendorCVOneDmx);
        Assert.assertEquals(vendorCVOneDmx.getFilename(), "BiDiBCV-13-115-1.0.3.xml");
    }

    @Test
    public void loadCvDefinitionsOneDMX_1_0_255() {
        Node nodeOneDmx = new Node(0, new byte[] { 1 }, UUID_ONEDMX);
        nodeOneDmx.setSoftwareVersion(new SoftwareVersion(1, 0, 90));

        final Context context = new DefaultContext();

        VendorCvData vendorCVOneDmx = VendorCvFactory.getCvDefinition(nodeOneDmx, context, "classpath:/bidib-test");
        Assert.assertNotNull(vendorCVOneDmx);
        // the minor version does not match and is bigger that 3 (1.0.3), must fallback to default BiDiBCV-13-115.xml
        Assert.assertEquals(vendorCVOneDmx.getFilename(), "BiDiBCV-13-115.xml");
    }

    @Test
    public void loadCvDefinitionsOneDMX_1_0_3() {
        Node nodeOneDmx = new Node(0, new byte[] { 1 }, UUID_ONEDMX);
        nodeOneDmx.setSoftwareVersion(new SoftwareVersion(1, 0, 3));

        final Context context = new DefaultContext();

        VendorCvData vendorCVOneDmx = VendorCvFactory.getCvDefinition(nodeOneDmx, context, "classpath:/bidib-test");
        Assert.assertNotNull(vendorCVOneDmx);
        Assert.assertEquals(vendorCVOneDmx.getFilename(), "BiDiBCV-13-115-1.0.3.xml");
    }

    @Test
    public void saveVendorCVRadioTest() throws JAXBException, SAXException, IOException {

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, XSD_LOCATION);

        VersionInfoType versionInfo = new VersionInfoType();
        versionInfo.setAuthor("Max Mustermann");
        versionInfo.setVersion("0.1");
        versionInfo.setVendor("013");
        versionInfo.setPid("153");
        versionInfo.setDescription("BiDiB-Test Radio 1");
        versionInfo.setLastupdate("20150829");

        VendorCV vendorCV = new VendorCV();
        vendorCV.setVersion(versionInfo);

        TemplatesType templatesType = new TemplatesType();

        TemplateType ledTemplate = new TemplateType();
        ledTemplate.setName("LED");

        templatesType.getTemplate().add(ledTemplate);

        CVType cv = new CVType();
        cv.setNumber("81");
        cv.setType(DataType.RADIO);
        cv.setMode(ModeType.RW);
        DescriptionType desc = new DescriptionType().withLang("de-DE").withText("Anzeige-Hardware");
        cv.getDescription().add(desc);
        desc = new DescriptionType().withLang("en-EN").withText("Display-Hardware");
        cv.getDescription().add(desc);
        cv.setMin("-");
        cv.setMax("-");
        cv.setLow("-");
        cv.setHigh("-");
        cv.setValues("143");
        cv.setRadiobits(15);
        cv.setRadiovalues("0,1,2,3");
        ledTemplate.getCVOrNodeOrRepeater().add(cv);

        vendorCV.setTemplates(templatesType);

        File exportFile = new File(EXPORTED_CVDEF_TARGET_DIR, "vendorcv_radio.xml");
        OutputStream os = new BufferedOutputStream(new FileOutputStream(exportFile));

        marshaller.marshal(vendorCV, new OutputStreamWriter(os, Charset.forName("UTF-8")));

        os.flush();
    }

    @Test
    public void loadCvDefinitionsRadio() {

        InputStream is = VendorCvFactory.class.getResourceAsStream("/vendorcv/vendorcv_radio.xml");
        Assert.assertNotNull(is);

        VendorCvFactory factory = new VendorCvFactory();

        final Context context = new DefaultContext();

        VendorCV vendorCV = factory.loadVendorCvFile(is, context);
        Assert.assertNotNull(vendorCV);

        LOGGER.info("Returned vendorCV: {}", vendorCV);

        Assert.assertNotNull(vendorCV.getTemplates());

        TemplatesType templatesType = vendorCV.getTemplates();
        Assert.assertNotNull(templatesType.getTemplate());

        List<TemplateType> templates = templatesType.getTemplate();
        Assert.assertTrue(templates.size() > 0);
        TemplateType template = templates.get(0);

        Assert.assertEquals(template.getName(), "LED");

        List<Object> cvList = template.getCVOrNodeOrRepeater();
        Assert.assertNotNull(cvList);
        Assert.assertTrue(cvList.size() > 0);

        CVType cvType = (CVType) cvList.get(0);
        Assert.assertNotNull(cvType);
        Assert.assertEquals(cvType.getRadiovalues(), "0,1,2,3");
        Assert.assertEquals(cvType.getRadiobits(), Integer.valueOf(15));
    }

    @Test
    public void loadCvDefinitionsTemplateWithRepeater() {

        InputStream is = VendorCvFactory.class.getResourceAsStream("/vendorcv/vendorcv_repeater.xml");
        Assert.assertNotNull(is);

        VendorCvFactory factory = new VendorCvFactory();

        final Context context = new DefaultContext();

        VendorCV vendorCV = factory.loadVendorCvFile(is, context);
        Assert.assertNotNull(vendorCV);

        LOGGER.info("Returned vendorCV: {}", vendorCV);

        Assert.assertNotNull(vendorCV.getTemplates());

        TemplatesType templatesType = vendorCV.getTemplates();
        Assert.assertNotNull(templatesType.getTemplate());

        List<TemplateType> templates = templatesType.getTemplate();
        Assert.assertTrue(templates.size() > 0);
        TemplateType template = templates.get(0);

        Assert.assertEquals(template.getName(), "LED");

        List<Object> cvOrNodeOrRepeater = template.getCVOrNodeOrRepeater();
        Assert.assertNotNull(cvOrNodeOrRepeater);
        Assert.assertTrue(cvOrNodeOrRepeater.size() > 0);

        Assert.assertTrue(cvOrNodeOrRepeater.get(0) instanceof CVType);

        CVType cvType = (CVType) cvOrNodeOrRepeater.get(0);
        Assert.assertNotNull(cvType);
        Assert.assertEquals(cvType.getNumber(), "81");
        Assert.assertEquals(cvType.getRadiovalues(), "0,1,2,3");
        Assert.assertEquals(cvType.getRadiobits(), Integer.valueOf(15));

        Assert.assertTrue(cvOrNodeOrRepeater.get(1) instanceof RepeaterType);

        RepeaterType repeater = (RepeaterType) cvOrNodeOrRepeater.get(1);
        Assert.assertEquals(repeater.getCount(), Integer.valueOf(3));
        Assert.assertEquals(repeater.getOffset(), Integer.valueOf(60));

        Assert.assertNotNull(repeater.getCV());
        List<CVType> repeaterCVs = repeater.getCV();
        Assert.assertEquals(repeaterCVs.size(), 2);
        CVType repeaterCV = repeaterCVs.get(0);
        Assert.assertEquals(repeaterCV.getNumber(), "1");
        repeaterCV = repeaterCVs.get(1);
        Assert.assertEquals(repeaterCV.getNumber(), "2");

        Assert.assertTrue(cvOrNodeOrRepeater.get(2) instanceof CVType);

        cvType = (CVType) cvOrNodeOrRepeater.get(2);
        Assert.assertNotNull(cvType);
        Assert.assertEquals(cvType.getNumber(), "181");
        Assert.assertEquals(cvType.getRadiovalues(), "0,1,2,3");
        Assert.assertEquals(cvType.getRadiobits(), Integer.valueOf(15));
    }

    @Test
    public void loadCvDefinitionsNodeWithRepeater() {

        InputStream is = VendorCvFactory.class.getResourceAsStream("/vendorcv/vendorcv_repeater2.xml");
        Assert.assertNotNull(is);

        VendorCvFactory factory = new VendorCvFactory();

        final Context context = new DefaultContext();

        VendorCV vendorCV = factory.loadVendorCvFile(is, context);
        Assert.assertNotNull(vendorCV);

        LOGGER.info("Returned vendorCV: {}", vendorCV);

        Assert.assertNotNull(vendorCV.getCVDefinition());
        CVDefinitionType cvDefinition = vendorCV.getCVDefinition();
        List<NodeType> nodes = cvDefinition.getNode();
        Assert.assertNotNull(nodes);
        Assert.assertEquals(nodes.size(), 1);

        NodeType node = nodes.get(0);
        List<NodetextType> nodeTextItems = node.getNodetext();
        Assert.assertNotNull(nodeTextItems);
        Assert.assertEquals(nodeTextItems.get(0).getText(), "Allgemeine Daten");
        Assert.assertEquals(nodeTextItems.get(1).getText(), "Basis");

        List<Object> items = node.getNodeOrCVOrRepeater();
        Assert.assertTrue(items.get(0) instanceof CVType);
        Assert.assertTrue(items.get(1) instanceof RepeaterType);
        Assert.assertTrue(items.get(2) instanceof CVType);
    }

    @Test
    public void loadInvalidVendorCVFile() throws IOException {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("/vendorcv/vendorcv_invalid.xml");
        files.add(expected);

        String defaultFilename = "vendorcv_invalid.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(2, 3, 0);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);
        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);

        String absoluteFileName = matchingFile.getParentFile().getAbsolutePath();
        LOGGER.info("Found absolute path: {}", absoluteFileName);

        File temp = new File(System.getProperty("user.dir"));

        File absoluteFile = new File(temp, "target/test-classes/" + expected.getPath());

        final Context context = new DefaultContext();

        VendorCV vendorCV = vendorCvFactory.loadVendorCvFile(absoluteFile, context);
        Assert.assertNull(vendorCV);

        // try to delete the file to make sure its not locked
        boolean deleted = absoluteFile.delete();
        Assert.assertTrue(deleted);
    }
}
