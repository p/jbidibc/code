package org.bidib.jbidibc.core.exception;

public class UnexpectedCharacterException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnexpectedCharacterException(String message) {
        super(message);
    }
}
