package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum PortConfigKeys implements BidibEnum {
    // @formatter:off
    BIDIB_PCFG_NONE(BidibLibrary.BIDIB_PCFG_NONE), 
    BIDIB_PCFG_LEVEL_PORT_ON(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON), 
    BIDIB_PCFG_LEVEL_PORT_OFF(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF), 
    BIDIB_PCFG_DIMM_UP(BidibLibrary.BIDIB_PCFG_DIMM_UP), 
    BIDIB_PCFG_DIMM_DOWN(BidibLibrary.BIDIB_PCFG_DIMM_DOWN), 
    BIDIB_PCFG_DIMM_STRETCH(BidibLibrary.BIDIB_PCFG_DIMM_STRETCH), 
    BIDIB_PCFG_OUTPUT_MAP(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP), 
    BIDIB_PCFG_SERVO_ADJ_L(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L), 
    BIDIB_PCFG_SERVO_ADJ_H(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H), 
    BIDIB_PCFG_SERVO_SPEED(BidibLibrary.BIDIB_PCFG_SERVO_SPEED), 
    BIDIB_PCFG_DIMM_UP_8_8(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8), 
    BIDIB_PCFG_DIMM_DOWN_8_8(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8), 
    BIDIB_PCFG_RGB(BidibLibrary.BIDIB_PCFG_RGB), 
    BIDIB_PCFG_CONTINUE(BidibLibrary.BIDIB_PCFG_CONTINUE), 
    BIDIB_PCFG_RECONFIG(BidibLibrary.BIDIB_PCFG_RECONFIG), 
    BIDIB_PCFG_TICKS(BidibLibrary.BIDIB_PCFG_TICKS), 
    BIDIB_PCFG_SWITCH_CTRL(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL), 
    BIDIB_PCFG_INPUT_CTRL(BidibLibrary.BIDIB_PCFG_INPUT_CTRL), 
    BIDIB_PCFG_TRANSITION_TIME(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME),
    BIDIB_PCFG_LOAD_TYPE(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
    // @formatter:on

    private final byte type;

    PortConfigKeys(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a port config key.
     * 
     * @param type
     *            numeric value of the port config key
     * 
     * @return PortConfigKeys
     */
    public static PortConfigKeys valueOf(byte type) {
        PortConfigKeys result = null;

        for (PortConfigKeys e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException(
                "Cannot map value '0x" + ByteUtils.byteToHex(type) + "' to a port config key.");
        }
        return result;
    }

}
