package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class FeedbackOccupiedResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_BM_OCC;

    FeedbackOccupiedResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        // len == 1 if no sensor time is delivered
        if (data == null || (data.length < 1 /* && data.length != 3 && data.length != 5 */)) {
            throw new ProtocolException("No valid MSG_BM_OCC received.");
        }
    }

    public FeedbackOccupiedResponse(byte[] addr, int num, int detectorAddress) throws ProtocolException {
        // the detectorAddress must be encapsulated into byte[] because otherwise it would call the ctor with the
        // timestamp
        this(addr, num, BidibLibrary.MSG_BM_OCC, new byte[] { ByteUtils.getLowByte(detectorAddress) });
    }

    public FeedbackOccupiedResponse(byte[] addr, int num, int detectorAddress, int timestamp) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_OCC, ByteUtils.getLowByte(detectorAddress), ByteUtils.getLowByte(timestamp),
            ByteUtils.getHighByte(timestamp));
    }

    public String getName() {
        return "MSG_BM_OCC";
    }

    public int getDetectorNumber() {
        return ByteUtils.getInt(getData()[0]);
    }

    public Long getTimestamp() {
        switch (getData().length) {
            case 3:
                int ts = ByteUtils.getWORD(getData(), 1);
                return Long.valueOf(ts);
            case 5:
                return ByteUtils.getDWORD(getData(), 1);
            default:
                break;
        }
        return null;
    }
}
