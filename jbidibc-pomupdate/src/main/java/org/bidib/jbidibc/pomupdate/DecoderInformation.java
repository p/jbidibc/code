package org.bidib.jbidibc.pomupdate;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DecoderInformation {
    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderInformation.class);

    public enum DecoderType {
        locoDecoder, functionDecoder, accessoryDecoder
    }

    public static final int DECODER_INFO_CV_COUNT = 16;

    public static final int INDEX_CV_VERSION_MAJOR = 4;

    public static final int INDEX_CV_VERSION_MINOR = 5;

    public static final int INDEX_CV_VERSION_MICRO = 6;

    public static final int INDEX_CV_VERSION_STEP = 7;

    private byte[] decoderInfoCvValues;

    public DecoderInformation(String encodedValues) {
        decoderInfoCvValues = ByteUtils.parseHexBinary(encodedValues);
    }

    public DecoderInformation(DecoderInformation source) {
        decoderInfoCvValues = source.decoderInfoCvValues.clone();
    }

    public byte getCvValue(int cvNumber) {
        if (cvNumber < 800 || cvNumber > 815) {
            throw new IllegalArgumentException("Only CV numbers from 800..815 are supported");
        }
        return decoderInfoCvValues[cvNumber - 800];
    }

    public int getVendorId() {
        return ByteUtils.getInt(getCvValue(800));
    }

    public DecoderType getDecoderType() {
        DecoderType decoderType = null;
        switch (ByteUtils.getInt(getCvValue(801))) {
            case 0:
                decoderType = DecoderType.locoDecoder;
                break;
            case 1:
                decoderType = DecoderType.locoDecoder;
                break;
            case 2:
                decoderType = DecoderType.locoDecoder;
                break;
            default:
                LOGGER.warn("Invalid decoder type: {}", ByteUtils.getInt(getCvValue(801)));
                break;
        }
        return decoderType;
    }

    public int getDecoderId() {
        return ByteUtils.getInt(getCvValue(803), getCvValue(802));
    }

    public String getFirmwareVersion() {

        StringBuilder sb = new StringBuilder();
        sb.append(ByteUtils.getInt(getCvValue(804))).append(".");
        sb.append(ByteUtils.getInt(getCvValue(805))).append(".");
        sb.append(ByteUtils.getInt(getCvValue(806))).append(".");
        sb.append(ByteUtils.getInt(getCvValue(807)));
        return sb.toString();
    }

    public String getFirmwareReleaseDate() {

        StringBuilder sb = new StringBuilder();
        sb.append(ByteUtils.byteToHex(getCvValue(808))).append(".");
        sb.append(ByteUtils.byteToHex(getCvValue(809))).append(".");
        sb.append(ByteUtils.byteToHex(getCvValue(810))).append(ByteUtils.byteToHex(getCvValue(811)));
        return sb.toString();
    }

    /**
     * Use this method to force a firmware update by setting all version related CV values to <code>0xFF</code>.
     */
    public void forceFirmwareUpdate() {
        // set all CV value with version information to 0xFF
        decoderInfoCvValues[INDEX_CV_VERSION_MAJOR] = (byte) 0xFF;
        decoderInfoCvValues[INDEX_CV_VERSION_MINOR] = (byte) 0xFF;
        decoderInfoCvValues[INDEX_CV_VERSION_MICRO] = (byte) 0xFF;
        decoderInfoCvValues[INDEX_CV_VERSION_STEP] = (byte) 0xFF;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append("[vendor=").append(getVendorId()).append(",decoder id=").append(getDecoderId()).append(",type=")
            .append(getDecoderType()).append(",version=").append(getFirmwareVersion()).append(",release date=")
            .append(getFirmwareReleaseDate()).append("]");
        return sb.toString();
    }
}
