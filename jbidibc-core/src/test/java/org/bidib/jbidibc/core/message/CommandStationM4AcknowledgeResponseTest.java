package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.enumeration.M4OpCodesAck;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CommandStationM4AcknowledgeResponseTest {

    @Test
    public void getDid() throws ProtocolException {
        CommandStationM4AcknowledgeResponse commandStationM4AcknowledgeResponse =
            new CommandStationM4AcknowledgeResponse(new byte[] { 1 }, 1, CommandStationM4AcknowledgeResponse.TYPE,
                new byte[] { M4OpCodesAck.M4_NEW_LOCO.getType(), (byte) 0xFA, (byte) 0x0A, (byte) 0x13, (byte) 0x75 });

        Integer did = commandStationM4AcknowledgeResponse.getDid(M4OpCodesAck.M4_NEW_LOCO);

        Assert.assertNotNull(did);
        Assert.assertEquals(did, Integer.valueOf(0xFA0A1375));
    }
}
