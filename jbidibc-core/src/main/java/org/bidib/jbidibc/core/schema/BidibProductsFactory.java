package org.bidib.jbidibc.core.schema;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.bidib.products.BiDiB;
import org.bidib.jbidibc.core.schema.bidib.products.ObjectFactory;
import org.bidib.jbidibc.core.schema.bidib.products.ProductType;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class BidibProductsFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibProductsFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.bidib.products";

    public static final String XSD_LOCATION = "/xsd/bidib2Products.xsd";

    public static final String XSD_LOCATION_BASE = "/xsd/bidib2Common.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/bidib xsd/bidib2Products.xsd";

    private static JAXBContext jaxbContext;

    public BidibProductsFactory() {
    }

    public static synchronized List<ProductType> getProducts(int vendorId, String... searchPaths) {
        LOGGER.info("Load the products for vendorId: {}", vendorId);

        return new BidibProductsFactory().loadProducts(vendorId, searchPaths);
    }

    public static synchronized ProductType getProduct(int vendorId, final int productId, String... searchPaths) {
        LOGGER.info("Load the product with vendorId: {} and productId: {}", vendorId, productId);

        List<ProductType> products = new BidibProductsFactory().loadProducts(vendorId, searchPaths);
        if (CollectionUtils.isNotEmpty(products)) {

            ProductType product = IterableUtils.find(products, new Predicate<ProductType>() {

                @Override
                public boolean evaluate(ProductType product) {
                    return product.getProductTypeId() == productId;
                }
            });
            return product;
        }

        return null;
    }

    private List<ProductType> loadProducts(int vendorId, String... searchPaths) {
        List<ProductType> products = null;
        BiDiB bidib = loadBidib(vendorId, searchPaths);
        if (bidib != null) {
            products = bidib.getProducts().getProduct();
        }
        return products;
    }

    public static ProductType getProduct(final org.bidib.jbidibc.core.Node node, String... searchPaths) {
        LOGGER.info("Load the Product info for node: {}", node);

        return new BidibProductsFactory().loadProductForNode(node, searchPaths);
    }

    public ProductType loadProductForNode(final org.bidib.jbidibc.core.Node node, String... searchPaths) {

        long uniqueId = node.getUniqueId();
        int relevantPidBits = node.getRelevantPidBits();

        int pid = NodeUtils.getPid(uniqueId, relevantPidBits);
        int vid = NodeUtils.getVendorId(uniqueId);
        LOGGER
            .info("Load the vendor cv definition for uniqueId: {}, pid: {}, vid: {}",
                NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        ProductType product = null;
        BiDiB bidib = loadBidib(vid, searchPaths);
        if (bidib != null) {

            List<ProductType> products = bidib.getProducts().getProduct();
            if (products != null) {
                short productTypeId = (short) pid;
                for (ProductType current : products) {
                    if (current.getProductTypeId() == productTypeId) {
                        product = current;
                        break;
                    }
                }
            }
        }

        LOGGER.trace("Loaded product: {}", product);
        return product;
    }

    public BiDiB loadBiDiBFile(File productsFile) throws FileNotFoundException {

        BiDiB bidib = null;
        InputStream is = null;

        try {
            is = new FileInputStream(productsFile);
            bidib = loadBiDiBFile(is);
        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load bidib file failed due to schema violation. Try to transform.", ex);

            if (is != null) {
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }
        }
        finally {
            if (is != null) {
                LOGGER.info("Close input stream.");
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }
        }
        return bidib;
    }

    private BiDiB loadBidib(long vid, String... searchPaths) {
        BiDiB bidib = null;
        for (String searchPath : searchPaths) {
            StringBuilder filename = new StringBuilder("Products_");
            filename.append(vid).append(".bidib");

            LOGGER.info("Prepared filename to load products: {}", filename.toString());
            if (searchPath.startsWith("classpath:")) {
                int beginIndex = "classpath:".length();
                String lookup = searchPath.substring(beginIndex) + "/" + filename.toString();
                LOGGER.info("Lookup products file internally: {}", lookup);
                InputStream is = BidibProductsFactory.class.getResourceAsStream(lookup);
                if (is != null) {
                    bidib = loadBiDiBFile(is);
                    if (bidib != null) {
                        break;
                    }
                }
                else {
                    LOGGER.warn("Internal lookup for products file failed.");
                }
            }
            else {
                File productsFile = new File(searchPath, filename.toString());
                if (productsFile.exists()) {
                    LOGGER.info("Found product file: {}", productsFile.getAbsolutePath());

                    try {
                        // try to load products
                        bidib = loadBiDiBFile(productsFile);
                    }
                    catch (FileNotFoundException ex) {
                        LOGGER.info("No Products file found.");
                    }

                    if (bidib != null) {
                        break;
                    }
                }
                else {
                    LOGGER.info("File does not exist: {}", productsFile.getAbsolutePath());
                }
            }
        }
        return bidib;
    }

    public BiDiB loadBiDiBFile(final InputStream is) {

        BiDiB bidib = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", JAXB_PACKAGE);
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(BidibProductsFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase =
                new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            try {
                JAXBElement<BiDiB> jaxbElement = (JAXBElement<BiDiB>) unmarshaller.unmarshal(xmlr, BiDiB.class);
                bidib = jaxbElement.getValue();
            }
            catch (UnmarshalException ex) {
                LOGGER.warn("Load content from file failed.", ex);
                if (ex.getLinkedException() instanceof SAXException) {
                    validate(is);
                    throw new InvalidSchemaException("Load BiDiB from file failed");
                }
            }

        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from file failed.", ex);
        }
        return bidib;
    }

    private List<String> validate(final InputStream is) {
        List<String> errors = null;

        if (is instanceof FileInputStream) {
            FileInputStream fis = (FileInputStream) is;
            try {
                LOGGER.info("Try to set file position to 0.");
                fis.getChannel().position(0);
            }
            catch (IOException e) {
                LOGGER.warn("Set file position to 0 failed.", e);
            }
        }

        StreamSource inputStreamSource = new StreamSource(is);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(BidibProductsFactory.class.getResourceAsStream(XSD_LOCATION));
        StreamSource streamSourceBase =
            new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION_BASE));

        XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();

        try {
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);

            validator.validate(inputStreamSource);
        }
        catch (IOException ex) {
            LOGGER.warn("Validate failed.", ex);
        }
        catch (SAXException ex) {
            LOGGER.warn("Validate failed.", ex);
        }

        errors = errorHandler.getErrors();
        LOGGER.info("Found errors: {}", errors);

        return errors;
    }

    /**
     * Save the bidib instance to a file.
     * 
     * @param bidib
     *            the bidib instance
     * @param fileName
     *            the file
     * @param gzip
     *            use gzip
     */
    public static void saveBiDiB(BiDiB bidib, String fileName, boolean gzip) {

        LOGGER.info("Save bidib content to file: {}, bidib: {}", fileName, bidib);
        OutputStream os = null;
        boolean passed = false;
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, JAXB_SCHEMA_LOCATION);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(AccessoryLabelFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase =
                new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });
            marshaller.setSchema(schema);

            os = new BufferedOutputStream(new FileOutputStream(fileName));
            if (gzip) {
                LOGGER.debug("Use gzip to compress bidib.");
                os = new GZIPOutputStream(os);
            }

            JAXBElement<BiDiB> jaxbElement = new ObjectFactory().createBiDiB(bidib);
            marshaller.marshal(jaxbElement, new OutputStreamWriter(os, Charset.forName("UTF-8")));

            os.flush();

            LOGGER.info("Save bidib content to file passed: {}", fileName);

            passed = true;
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save bidib failed.", ex);

            throw new RuntimeException("Save bidib failed.", ex);
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }

            if (!passed) {
                LOGGER.warn("Delete the file because the export has failed.");
                FileUtils.deleteQuietly(new File(fileName));
            }
        }
    }
}
