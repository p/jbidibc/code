set TAG_NAME=1.10.0
set NEXT_DEV_VERSION=1.10-SNAPSHOT

set PROJECT_NAME=jbidibc

set GIT_USERNAME=<your-sourceforge-username>
set GIT_PASSWORD=<your-sourceforge-password>
set GPG_PASSPHRASE=<your-gpg-passphrase>

rem the local path to the directory where the release will be performed (clean checkout, build, create tag, build release version)
SET LOCAL_RELEASE_BASE=D:\release

SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_131
SET M2_HOME=D:\tools\apache-maven-3.5.2
SET ANT_HOME=D:\tools\apache-ant-1.8.4
SET GIT_HOME=D:\tools\PortableGit-2.13.2-64bit

SET GIT_BASE_PATH=https://gitlab.com/akuhtz/jbidibc.git

SET PATH=%M2_HOME%\bin;%JAVA_HOME%\bin;%GIT_HOME%\bin;%ANT_HOME%\bin;%PATH%

set MAVEN_OPTS=-Xmx1024m -XX:MaxPermSize=256m
