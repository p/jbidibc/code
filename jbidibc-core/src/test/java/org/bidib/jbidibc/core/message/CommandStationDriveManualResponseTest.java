package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.enumeration.DirectionEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CommandStationDriveManualResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationDriveManualResponseTest.class);

    @Test
    public void getFunctions1() throws ProtocolException {

        // 18.06.2017 10:21:17.502: [receiveQueueWorker] - << MSG_CS_DRIVE_MANUAL[[0],num=56,type=229,data=[235, 21, 2,
        // 3, 0, 1, 0, 0, 0]] : 0C 00 38 E5 EB 15 02 03 00 01 00 00 00

        byte[] message =
            new byte[] { 0x0C, 0x00, 0x38, (byte) 0xE5, (byte) 0xEB, 0x15, 0x02, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 };
        BidibMessage result = new BidibMessage(message);

        Assert.assertEquals(ByteUtils.getInt(result.getType()), CommandStationDriveManualResponse.TYPE.intValue());

        CommandStationDriveManualResponse response =
            new CommandStationDriveManualResponse(result.getAddr(), result.getNum(), result.getType(),
                result.getData());

        Assert.assertNotNull(response);
        LOGGER.info("Prepared response: {}", response);

        LOGGER.info("Prepared driveState: {}", response.getDriveState());
        Assert.assertNotNull(response.getDriveState());

        byte functionsF0toF4 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F0_F4);
        Assert.assertEquals(functionsF0toF4, (byte) 0x01);
        byte functionsF5toF12 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F5_F12);
        Assert.assertEquals(functionsF5toF12, (byte) 0x00);
        byte functionsF13toF20 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F13_F20);
        Assert.assertEquals(functionsF13toF20, (byte) 0x00);
        byte functionsF21toF28 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F21_F28);
        Assert.assertEquals(functionsF21toF28, (byte) 0x00);
    }

    @Test
    public void getFunctions2() throws ProtocolException {

        // 18.06.2017 10:21:18.349: [receiveQueueWorker] - << MSG_CS_DRIVE_MANUAL[[0],num=58,type=229,data=[235, 21, 2,
        // 3, 0, 0, 0, 0, 0]] : 0C 00 3A E5 EB 15 02 03 00 00 00 00 00

        byte[] message =
            new byte[] { 0x0C, 0x00, 0x3A, (byte) 0xE5, (byte) 0xEB, 0x15, 0x02, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00 };
        BidibMessage result = new BidibMessage(message);

        Assert.assertEquals(ByteUtils.getInt(result.getType()), CommandStationDriveManualResponse.TYPE.intValue());

        CommandStationDriveManualResponse response =
            new CommandStationDriveManualResponse(result.getAddr(), result.getNum(), result.getType(),
                result.getData());

        Assert.assertNotNull(response);
        LOGGER.info("Prepared response: {}", response);

        LOGGER.info("Prepared driveState: {}", response.getDriveState());
        Assert.assertNotNull(response.getDriveState());

        byte functionsF0toF4 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F0_F4);
        Assert.assertEquals(functionsF0toF4, (byte) 0x00);
        byte functionsF5toF12 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F5_F12);
        Assert.assertEquals(functionsF5toF12, (byte) 0x00);
        byte functionsF13toF20 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F13_F20);
        Assert.assertEquals(functionsF13toF20, (byte) 0x00);
        byte functionsF21toF28 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F21_F28);
        Assert.assertEquals(functionsF21toF28, (byte) 0x00);
    }

    @Test
    public void getFunctions3() throws ProtocolException {

        // 18.06.2017 10:21:19.117: [receiveQueueWorker] - << MSG_CS_DRIVE_MANUAL[[0],num=60,type=229,data=[235, 21, 2,
        // 3, 0, 2, 0, 0, 0]] : 0C 00 3C E5 EB 15 02 03 00 02 00 00 00

        byte[] message =
            new byte[] { 0x0C, 0x00, 0x3C, (byte) 0xE5, (byte) 0xEB, 0x15, 0x02, 0x03, 0x00, 0x02, 0x00, 0x00, 0x00 };
        BidibMessage result = new BidibMessage(message);

        Assert.assertEquals(ByteUtils.getInt(result.getType()), CommandStationDriveManualResponse.TYPE.intValue());

        CommandStationDriveManualResponse response =
            new CommandStationDriveManualResponse(result.getAddr(), result.getNum(), result.getType(),
                result.getData());

        Assert.assertNotNull(response);
        LOGGER.info("Prepared response: {}", response);

        LOGGER.info("Prepared driveState: {}", response.getDriveState());
        Assert.assertNotNull(response.getDriveState());
        Assert.assertEquals(response.getDriveState().getDirection(), DirectionEnum.BACKWARD); // backward

        byte functionsF0toF4 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F0_F4);
        Assert.assertEquals(functionsF0toF4, (byte) 0x02);
        byte functionsF5toF12 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F5_F12);
        Assert.assertEquals(functionsF5toF12, (byte) 0x00);
        byte functionsF13toF20 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F13_F20);
        Assert.assertEquals(functionsF13toF20, (byte) 0x00);
        byte functionsF21toF28 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F21_F28);
        Assert.assertEquals(functionsF21toF28, (byte) 0x00);
    }

    // @formatter:off
    // 18.06.2017 10:21:21.930: [receiveQueueWorker] - << MSG_CS_DRIVE_MANUAL[[0],num=65,type=229,data=[235, 21, 2, 3, 130, 0, 0, 0, 0]] : 0C 00 41 E5 EB 15 02 03 82 00 00 00 00
    // 18.06.2017 10:21:22.234: [receiveQueueWorker] - << MSG_CS_DRIVE_MANUAL[[0],num=66,type=229,data=[235, 21, 2, 3, 130, 0, 0, 0, 0]] : 0C 00 42 E5 EB 15 02 03 82 00 00 00 00
    // @formatter:on
    @Test
    public void getFunctions4() throws ProtocolException {

        // @formatter:off
        // 18.06.2017 10:21:21.930: [receiveQueueWorker] - << MSG_CS_DRIVE_MANUAL[[0],num=65,type=229,data=[235, 21, 2, 3, 130, 0, 0, 0, 0]] : 0C 00 41 E5 EB 15 02 03 82 00 00 00 00
        // @formatter:on

        byte[] message =
            new byte[] { 0x0C, 0x00, 0x41, (byte) 0xE5, (byte) 0xEB, 0x15, 0x02, 0x03, (byte) 0x82, 0x00, 0x00, 0x00,
                0x00 };
        BidibMessage result = new BidibMessage(message);

        Assert.assertEquals(ByteUtils.getInt(result.getType()), CommandStationDriveManualResponse.TYPE.intValue());

        CommandStationDriveManualResponse response =
            new CommandStationDriveManualResponse(result.getAddr(), result.getNum(), result.getType(),
                result.getData());

        Assert.assertNotNull(response);
        LOGGER.info("Prepared response: {}", response);

        LOGGER.info("Prepared driveState: {}", response.getDriveState());
        Assert.assertNotNull(response.getDriveState());
        Assert.assertEquals(response.getDriveState().getAddress(), 5611); // 0x15EB
        Assert.assertEquals(response.getDriveState().getFormat(), DriveState.DRIVE_ADDRESS_FORMAT_DCC28);
        Assert.assertEquals(response.getDriveState().getOutputActive(), 0x03);
        Assert.assertEquals(response.getDriveState().getSpeed(), 0x02); // 2
        Assert.assertEquals(response.getDriveState().getDirection(), DirectionEnum.FORWARD); // forward

        byte functionsF0toF4 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F0_F4);
        Assert.assertEquals(functionsF0toF4, (byte) 0x00);
        byte functionsF5toF12 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F5_F12);
        Assert.assertEquals(functionsF5toF12, (byte) 0x00);
        byte functionsF13toF20 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F13_F20);
        Assert.assertEquals(functionsF13toF20, (byte) 0x00);
        byte functionsF21toF28 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F21_F28);
        Assert.assertEquals(functionsF21toF28, (byte) 0x00);
    }

}
