package org.bidib.jbidibc.core.node;

public interface ConfigVar {

    /**
     * @return the name
     */
    String getName();

    /**
     * @param name
     *            the name to set
     */
    void setName(String name);

    /**
     * @return the value
     */
    String getValue();

    /**
     * @param value
     *            the value to set
     */
    void setValue(String value);

    /**
     * @return the timeout
     */
    boolean isTimeout();

    /**
     * @param timeout
     *            the timeout to set
     */
    void setTimeout(boolean timeout);

    /**
     * @return the skipOnTimeout flag
     */
    boolean isSkipOnTimeout();

    /**
     * @param skipOnTimeout
     *            the skipOnTimeout flag to set
     */
    void setSkipOnTimeout(boolean skipOnTimeout);

    /**
     * @return the minCvNumber
     */
    int getMinCvNumber();

    /**
     * @param minCvNumber
     *            the maxCvNumber to set
     */
    void setMinCvNumber(int minCvNumber);

    /**
     * @return the maxCvNumber
     */
    int getMaxCvNumber();

    /**
     * @param maxCvNumber
     *            the maxCvNumber to set
     */
    void setMaxCvNumber(int maxCvNumber);

}
