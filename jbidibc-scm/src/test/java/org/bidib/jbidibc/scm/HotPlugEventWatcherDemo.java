package org.bidib.jbidibc.scm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HotPlugEventWatcherDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(HotPlugEventWatcherDemo.class);

    public static void main(String[] args) {
        LOGGER.info("Create and start HotPlugEventWatcher.");
        HotPlugEventWatcher watcher = new HotPlugEventWatcher();
        Thread worker = new Thread(watcher);
        worker.start();
        LOGGER.info("Create and start HotPlugEventWatcher passed.");
    }

}
