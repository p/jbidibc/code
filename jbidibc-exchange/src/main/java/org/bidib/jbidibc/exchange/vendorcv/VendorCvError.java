package org.bidib.jbidibc.exchange.vendorcv;

public class VendorCvError {

    private final String message;

    private final String fileName;

    /**
     * @param message
     *            the message
     * @param fileName
     *            the filename
     */
    public VendorCvError(String message, String fileName) {
        this.message = message;
        this.fileName = fileName;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return new StringBuilder()
            .append("Message: ").append(message).append(", filename: ").append(fileName).toString();
    }
}
