package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

public interface BidibMessageProcessor extends MessageProcessor {

    /**
     * Add a message listener.
     * 
     * @param messageListener
     *            the message listener to add
     */
    void addMessageListener(MessageListener messageListener);

    /**
     * Remove a message listener.
     * 
     * @param messageListener
     *            the message listener to remove
     */
    void removeMessageListener(MessageListener messageListener);

    /**
     * Add a node listener.
     * 
     * @param nodeListener
     *            the node listener to add
     */
    void addNodeListener(NodeListener nodeListener);

    /**
     * Remove a node listener.
     * 
     * @param nodeListener
     *            the node listener to remove
     */
    void removeNodeListener(NodeListener nodeListener);

    /**
     * @return returns device specific error information
     */
    String getErrorInformation();

    /**
     * @param ignoreWrongMessageNumber
     *            set the flag to ignore wrong message numbers
     */
    void setIgnoreWrongMessageNumber(boolean ignoreWrongMessageNumber);

    /**
     * Receive data.
     * 
     * @param data
     *            the byte array with the received data
     */
    void receive(final ByteArrayOutputStream data);
}
