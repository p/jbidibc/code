package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class CommandStationQueryMessage extends BidibCommandMessage {

    /**
     * Create command to query all items of the specified type.
     * 
     * @param queryType
     *            the query type
     */
    public CommandStationQueryMessage(CsQueryTypeEnum queryType) {
        super(0, BidibLibrary.MSG_CS_QUERY, ByteUtils.setBit(queryType.getType(), true, 7));
    }

    public CommandStationQueryMessage(CsQueryTypeEnum queryType, int param) {
        super(0, BidibLibrary.MSG_CS_QUERY,
            ByteUtils.concat(new byte[] { queryType.getType() }, ByteUtils.toWORD(param)));
    }

    public CommandStationQueryMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_CS_QUERY";
    }

    /**
     * @return the value of csQuery converted to a {@code CsQueryTypeEnum}.
     */
    public CsQueryTypeEnum getCsQueryType() {
        byte csQueryType = getData()[0];
        return CsQueryTypeEnum.valueOf(ByteUtils.getLowByte(csQueryType & 0x7F /* mask the MSB */));
    }

    /**
     * @return the value of the csQuery
     */
    public byte getCsQueryValue() {
        return getData()[0];
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return new Integer[] { CommandStationDriveStateResponse.TYPE };
    }
}
