package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcPortQueryAllMessage extends BidibCommandMessage implements BidibBulkCommand {

    private int expectedCountResponses;

    protected LcPortQueryAllMessage() {
        super(0, BidibLibrary.MSG_LC_PORT_QUERY_ALL);
    }

    protected LcPortQueryAllMessage(int portTypeMask, int rangeFrom, int rangeTo) {

        super(0, BidibLibrary.MSG_LC_PORT_QUERY_ALL,
            new byte[] { ByteUtils.getLowByte(portTypeMask), ByteUtils.getHighByte(portTypeMask),
                ByteUtils.getLowByte(rangeFrom), ByteUtils.getHighByte(rangeFrom), ByteUtils.getLowByte(rangeTo),
                ByteUtils.getHighByte(rangeTo) });
    }

    public LcPortQueryAllMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_LC_PORT_QUERY_ALL";
    }

    public int getPortTypeMask() {
        return ByteUtils.getInt(getData()[0], getData()[1]);
    }

    public int getPortRangeFrom(PortModelEnum portModel) {
        return PortModelEnum.getPortNumber(portModel, getData()[2], getData()[3]);
    }

    public int getPortRangeTo(PortModelEnum portModel) {
        return PortModelEnum.getPortNumber(portModel, getData()[4], getData()[5]);
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }

    @Override
    public Integer[] getExpectedBulkResponseTypes() {
        return new Integer[] { LcStatResponse.TYPE, LcNotAvailableResponse.TYPE };
    }

    /**
     * @return the expectedCountResponses
     */
    public int getExpectedCountResponses() {
        return expectedCountResponses;
    }

    /**
     * @param expectedCountResponses
     *            the expectedCountResponses to set
     */
    public void setExpectedCountResponses(int expectedCountResponses) {
        this.expectedCountResponses = expectedCountResponses;
    }
}
