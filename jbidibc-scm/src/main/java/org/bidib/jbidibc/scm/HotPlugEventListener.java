package org.bidib.jbidibc.scm;

public interface HotPlugEventListener {

    /**
     * An USB device was added.
     * 
     * @param usbDevice
     *            the usb device
     */
    void usbDeviceAdded(UsbDevice usbDevice);

    /**
     * An USB device was removed.
     * 
     * @param usbDevice
     *            the usb device
     */
    void usbDeviceRemoved(UsbDevice usbDevice);
}
