package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcNotAvailableResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_LC_NA;

    LcNotAvailableResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 2 /* !(data.length == 2 || data.length == 3) */) {
            throw new ProtocolException("No valid MSG_LC_NA received.");
        }
    }

    public LcNotAvailableResponse(byte[] addr, int num, BidibPort bidibPort) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_LC_NA, bidibPort.getValues());
    }

    public String getName() {
        return "MSG_LC_NA";
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port type
     */
    public LcOutputType getPortType(PortModelEnum portModel) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat_extended:
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(getData()[0]);
                break;
        }
        return outputType;
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port number
     */
    public int getPortNumber(PortModelEnum portModel) {
        int portNumber = -1;
        switch (portModel) {
            case flat_extended:
                portNumber = ByteUtils.getInt(getData()[0], getData()[1] & 0x7F);
                break;
            case flat:
                portNumber = ByteUtils.getInt(getData()[0]);
                break;
            default:
                portNumber = ByteUtils.getInt(getData()[1], 0x7F);
                break;
        }
        return portNumber;
    }

    public BidibPort getBidibPort() {
        return BidibPort.prepareBidibPort(getData()[0], getData()[1]);
    }

    /**
     * @return the optional error code or <code>null</code> if no error code available
     */
    public Integer getErrorCode() {
        if (getData().length > 2) {
            return Integer.valueOf(ByteUtils.getInt(getData()[2]));
        }
        return null;
    }
}
