package org.bidib.jbidibc.core.enumeration;

public enum IoBehaviourSwitchEnum implements BidibEnum {
    // @formatter:off
    LOW(0), HIGH(1), Z(2), WEAK_LOW(3), WEAK_HIGH(4), UNKNOWN(0xFF);
    // @formatter:on

    private final byte type;

    IoBehaviourSwitchEnum(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a io behaviour switch enum.
     * 
     * @param type
     *            numeric value of the io behaviour switch enum
     * 
     * @return IoBehaviourSwitchEnum
     */
    public static IoBehaviourSwitchEnum valueOf(byte type) {
        IoBehaviourSwitchEnum result = null;

        for (IoBehaviourSwitchEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a io behaviour switch enum");
        }
        return result;
    }

    public static IoBehaviourSwitchEnum[] getValues() {

        return values();
    }
}
