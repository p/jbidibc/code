package org.bidib.jbidibc.decoder.version;

public interface DecoderVersion {

    /**
     * @return the major version
     */
    Integer getMajorVersion();

    /**
     * @return the minor version
     */
    Integer getMinorVersion();

    /**
     * @return the micro version
     */
    Integer getMicroVersion();

    /**
     * @param versionString
     *            the version string to parse
     */
    void parse(String versionString);
}
