package org.bidib.jbidibc.core.enumeration;

public enum AddressTypeEnum implements BidibEnum {
    // @formatter:off
    LOCOMOTIVE_FORWARD("loco-forward", 0), ACCESSORY("accessory", 1), LOCOMOTIVE_BACKWARD("loco-backward", 2), EXTENDED_ACCESSORY(
        "extended-accessory", 3);
    // @formatter:on

    private final byte type;

    private final String key;

    AddressTypeEnum(String key, int type) {
        this.key = key;
        this.type = (byte) type;
    }

    public String getKey() {
        return key;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create an address type enum.
     * 
     * @param type
     *            numeric value of the address type enum
     * 
     * @return AddressTypeEnum
     */
    public static AddressTypeEnum valueOf(byte type) {
        AddressTypeEnum result = null;

        for (AddressTypeEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a address type enum");
        }
        return result;
    }
}
