package org.bidib.jbidibc.exchange.vendorcv;

public class VendorCvData {

    private final VendorCV vendorCV;

    private final String filename;

    private final String filePath;

    public VendorCvData(final VendorCV vendorCV, final String filename, final String filePath) {
        this.vendorCV = vendorCV;
        this.filename = filename;
        this.filePath = filePath;
    }

    /**
     * @return the vendorCV
     */
    public VendorCV getVendorCV() {
        return vendorCV;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }
}
