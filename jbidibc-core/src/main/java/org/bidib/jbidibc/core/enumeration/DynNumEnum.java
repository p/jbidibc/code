package org.bidib.jbidibc.core.enumeration;

public enum DynNumEnum {

    // @formatter:off
    DYN_STATE_NUM_RESERVED(0), DYN_STATE_NUM_SIGNAL_QUALITY(1), DYN_STATE_NUM_TEMPERATURE(2), DYN_STATE_NUM_CONTAINER_1(3);
    // @formatter:on

    private final byte type;

    DynNumEnum(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    public int getValue() {
        return type & 0xFF;
    }

    /**
     * Create an dyn num enum.
     * 
     * @param type
     *            numeric value of the dyn num enum
     * 
     * @return DynNumEnum
     */
    public static DynNumEnum valueOf(byte type) {
        DynNumEnum result = null;

        for (DynNumEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to an DynNum enum");
        }
        return result;
    }

}
