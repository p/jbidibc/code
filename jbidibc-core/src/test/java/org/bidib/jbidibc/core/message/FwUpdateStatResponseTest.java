package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.FirmwareUpdateStat;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateState;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FwUpdateStatResponseTest {

    @Test
    public void getUpdateStatTimeout0() throws ProtocolException {
        byte timeout = ByteUtils.getLowByte(0);
        FwUpdateStatResponse response =
            new FwUpdateStatResponse(new byte[] { 1 }, 1, FirmwareUpdateState.READY.getType(), timeout);

        FirmwareUpdateStat stat = response.getUpdateStat();
        Assert.assertNotNull(stat);

        Assert.assertEquals(stat.getState(), FirmwareUpdateState.READY);
        Assert.assertEquals(stat.getTimeout(), 0);
    }

    @Test
    public void getUpdateStatTimeout200() throws ProtocolException {
        byte timeout = ByteUtils.getLowByte(200);
        FwUpdateStatResponse response =
            new FwUpdateStatResponse(new byte[] { 1 }, 1, FirmwareUpdateState.READY.getType(), timeout);

        FirmwareUpdateStat stat = response.getUpdateStat();
        Assert.assertNotNull(stat);

        Assert.assertEquals(stat.getState(), FirmwareUpdateState.READY);
        Assert.assertEquals(stat.getTimeout(), 200);
    }
}
