package org.bidib.jbidibc;

import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.ActivateCoilEnum;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.node.CommandStationNode;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 * This commands reads the value of the specified CV from the specified node.
 * 
 */
@Parameters(separators = "=")
public class DccAccessory extends BidibNodeCommand {
    @Parameter(names = { "-address" }, description = "The DCC decoder address", required = true)
    private int decoderAddress;

    @Parameter(names = { "-activate" }, description = "0: coil-off, 1: coil-on", required = true)
    private int activate;

    @Parameter(names = { "-aspect" }, description = "The aspect to set", required = true)
    private int aspect;

    public static void main(String[] args) {
        run(new DccAccessory(), args);
    }

    public int execute() {
        int result = 20;

        try {
            openPort(getPortName(), null);

            Node node = findNode();

            if (node != null) {
                CommandStationNode bidibNode = getBidib().getCommandStationNode(node);

                AddressTypeEnum addressType = AddressTypeEnum.ACCESSORY;
                TimingControlEnum timingControl = TimingControlEnum.OUTPUT_UNIT;
                ActivateCoilEnum activateCoil = (activate != 0 ? ActivateCoilEnum.COIL_ON : ActivateCoilEnum.COIL_OFF);

                TimeBaseUnitEnum timeBaseUnit = TimeBaseUnitEnum.UNIT_100MS;
                byte time = 5;

                AccessoryAcknowledge acknowledge =
                    bidibNode.setAccessory(decoderAddress, addressType, timingControl, activateCoil, aspect,
                        timeBaseUnit, time);

                System.out.println("Acknowledge: " + acknowledge); // NOSONAR
            }
            else {
                System.err.println("node with unique id \"" + getNodeIdentifier() + "\" not found"); // NOSONAR
            }

            getBidib().close();

        }
        catch (InvalidConfigurationException ex) {
            System.err.println("Execute command failed: " + ex); // NOSONAR
        }
        catch (PortNotFoundException ex) {
            System.err.println("The provided port was not found: " + ex.getMessage() // NOSONAR
                + ". Verify that the BiDiB device is connected.");
        }
        catch (Exception ex) {
            System.err.println("Execute command failed: " + ex); // NOSONAR
        }
        return result;
    }
}
