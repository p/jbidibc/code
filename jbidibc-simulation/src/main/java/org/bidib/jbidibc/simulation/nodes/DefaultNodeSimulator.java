package org.bidib.jbidibc.simulation.nodes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.VendorData;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateOperation;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.AccessoryNotifyResponse;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.FeatureCountResponse;
import org.bidib.jbidibc.core.message.FeatureGetMessage;
import org.bidib.jbidibc.core.message.FeatureNotAvailableResponse;
import org.bidib.jbidibc.core.message.FeatureResponse;
import org.bidib.jbidibc.core.message.FeatureSetMessage;
import org.bidib.jbidibc.core.message.FwUpdateOpMessage;
import org.bidib.jbidibc.core.message.FwUpdateStatResponse;
import org.bidib.jbidibc.core.message.LocalEmitterMessage;
import org.bidib.jbidibc.core.message.NodeTabCountResponse;
import org.bidib.jbidibc.core.message.NodeTabResponse;
import org.bidib.jbidibc.core.message.StallResponse;
import org.bidib.jbidibc.core.message.StringGetMessage;
import org.bidib.jbidibc.core.message.StringResponse;
import org.bidib.jbidibc.core.message.StringSetMessage;
import org.bidib.jbidibc.core.message.SysErrorResponse;
import org.bidib.jbidibc.core.message.SysIdentifyResponse;
import org.bidib.jbidibc.core.message.SysMagicResponse;
import org.bidib.jbidibc.core.message.SysPVersionResponse;
import org.bidib.jbidibc.core.message.SysPingMessage;
import org.bidib.jbidibc.core.message.SysPongResponse;
import org.bidib.jbidibc.core.message.SysSwVersionResponse;
import org.bidib.jbidibc.core.message.SysUniqueIdResponse;
import org.bidib.jbidibc.core.message.VendorAckResponse;
import org.bidib.jbidibc.core.message.VendorEnableMessage;
import org.bidib.jbidibc.core.message.VendorGetMessage;
import org.bidib.jbidibc.core.message.VendorResponse;
import org.bidib.jbidibc.core.message.VendorSetMessage;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.jbidibc.simulation.events.AccessoryStateErrorEvent;
import org.bidib.jbidibc.simulation.events.IdentifyEvent;
import org.bidib.jbidibc.simulation.events.NodeAvailableEvent;
import org.bidib.jbidibc.simulation.events.NodeLostEvent;
import org.bidib.jbidibc.simulation.events.SimulatorStatusEvent;
import org.bidib.jbidibc.simulation.events.StallEvent;
import org.bidib.jbidibc.simulation.events.SysErrorEvent;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.annotation.EventSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultNodeSimulator implements SimulatorNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultNodeSimulator.class);

    protected byte[] nodeAddress;

    protected long uniqueId;

    boolean autoAddFeature;

    private boolean identifyActive;

    protected SortedMap<String, SimulatorNode> subNodes = new TreeMap<String, SimulatorNode>();

    private int sendNum;

    private String[] stringValue = new String[] { "", "" };

    private int currentFeature;

    protected SortedSet<Feature> features = new TreeSet<>();

    protected Map<String, String> configurationVariables = new LinkedHashMap<String, String>();

    private BlockingQueue<BidibCommand> sendQueue = new LinkedBlockingQueue<BidibCommand>();

    private Thread requestWorker;

    protected final ScheduledExecutorService responseWorker = Executors.newScheduledThreadPool(1);

    protected final ScheduledExecutorService availableAfterResetWorker = Executors.newScheduledThreadPool(1);

    private final SimulationBidibMessageProcessor messageReceiver;

    private ProtocolVersion protocolVersion;

    private SoftwareVersion softwareVersion = new SoftwareVersion(1, 0, 0);

    private PortModelEnum portModelEnum;

    public DefaultNodeSimulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        this.nodeAddress = nodeAddress;
        this.messageReceiver = messageReceiver;
        this.uniqueId = uniqueId;
        this.autoAddFeature = autoAddFeature;

        LOGGER
            .info("Create default node simulator with address: {}, uniqueId: {}, autoAddFeature: {}", nodeAddress,
                uniqueId, autoAddFeature);
    }

    protected byte[] getNodeAddress() {
        return nodeAddress;
    }

    @Override
    public void setNodeName(String nodeName) {
        if (StringUtils.isNotBlank(nodeName)) {
            stringValue[1] = nodeName;
        }
    }

    @Override
    public void setProductName(String productName) {
        if (StringUtils.isNotBlank(productName)) {
            stringValue[0] = productName;
        }
    }

    @Override
    public void setProtocolVersion(String protocolVersion) {
        LOGGER.info("Set the protocol version: {}", protocolVersion);
        String[] splited = protocolVersion.split("\\.");

        if (splited.length == 2) {
            this.protocolVersion =
                new ProtocolVersion(Integer.parseInt(splited[0]), ByteUtils.getLowByte(Integer.parseInt(splited[1])));
        }
        else {
            LOGGER.error("Invalid protocol version provided: {}", protocolVersion);
        }
    }

    protected ProtocolVersion getProtocolVersion() {
        return protocolVersion;
    }

    @Override
    public void setSoftwareVersion(String version) {
        LOGGER.info("Set the software version: {}", version);

        try {
            softwareVersion = SoftwareVersion.parse(version);
        }
        catch (Exception ex) {
            LOGGER.warn("Parse software version failed, fall back to default.", ex);

            softwareVersion = new SoftwareVersion(1, 0, 0);
        }

    }

    @Override
    public void setFeatures(FeaturesType featuresType) {
        if (featuresType == null || CollectionUtils.isEmpty(featuresType.getFeature())) {
            return;
        }

        LOGGER.info("Set the provided features: {}", featuresType);
        // add the features from the simulation.xml
        for (FeatureType featureType : featuresType.getFeature()) {

            if (featureType.getValue() != null) {
                Feature feature = Feature.valueOf(featureType.getType(), featureType.getValue());
                boolean removed = features.remove(feature);
                LOGGER.info("Removed existing feature: {}, feature id: {}", removed, feature.getType());
                features.add(feature);
            }
            else {
                LOGGER.info("Remove feature: {}", featureType.getType());
                Integer requestedFeatureId = Feature.getFeatureNumberByName(featureType.getType());

                if (requestedFeatureId != null) {
                    Feature feature = IterableUtils.find(features, new Predicate<Feature>() {
                        @Override
                        public boolean evaluate(Feature feature) {
                            return feature.isRequestedFeature(requestedFeatureId);
                        }
                    });

                    if (feature != null) {
                        features.remove(feature);

                        // update the feature count
                        prepared();
                    }
                    else {
                        LOGGER.warn("No feature found to remove for id: {}", requestedFeatureId);
                    }
                }
                else {
                    LOGGER.warn("No feature found with id: {}", featureType.getType());
                }
            }
        }
    }

    protected void prepareFeatures() {
        features.add(new Feature(BidibLibrary.FEATURE_FW_UPDATE_MODE, 1));
        features.add(new Feature(BidibLibrary.FEATURE_STRING_SIZE, 24));
        features.add(new Feature(BidibLibrary.FEATURE_RELEVANT_PID_BITS, 16));
    }

    @Override
    public void setCVs(CvsType cvsType) {
        if (cvsType == null || CollectionUtils.isEmpty(cvsType.getCv())) {
            return;
        }

        for (CvType cvType : cvsType.getCv()) {
            LOGGER.info("Set the CV: {}", cvType);
            configurationVariables.put(cvType.getNumber(), cvType.getValue());
        }
    }

    protected void prepareCVs() {
        // do nothing
    }

    /**
     * @return the number of ports in the flat model or <code>0</code> if node does not support the flat port model
     */
    protected int getFlatPortModelPortCount() {
        Feature feature = getFeature(BidibLibrary.FEATURE_CTRL_PORT_FLAT_MODEL);
        if (feature != null) {
            int numPorts = feature.getValue();
            return numPorts;
        }

        return 0;
    }

    protected boolean isPortFlatModelAvailable() {
        Feature feature = getFeature(BidibLibrary.FEATURE_CTRL_PORT_FLAT_MODEL);
        if (feature != null && feature.getValue() > 0) {
            return true;
        }
        return false;
    }

    public PortModelEnum getPortModel() {

        if (portModelEnum == null) {

            if (getFlatPortModelPortCount() > 0) {
                portModelEnum = PortModelEnum.flat;
            }
            else {
                portModelEnum = PortModelEnum.type;
            }
        }
        return portModelEnum;
    }

    protected BidibPort prepareBidibPort(PortModelEnum portModelEnum, LcOutputType lcOutputType, int port) {
        BidibPort bidibPort = null;
        if (portModelEnum == PortModelEnum.type) {
            bidibPort = new BidibPort(new byte[] { lcOutputType.getType(), ByteUtils.getLowByte(port) });
        }
        else {
            bidibPort = new BidibPort(new byte[] { ByteUtils.getLowByte(port), ByteUtils.getHighByte(port) });
        }
        return bidibPort;
    }

    @Override
    public Feature getFeature(int featureNum) {
        for (Feature feature : features) {
            if (feature.getType() == featureNum) {
                return feature;
            }
        }
        return null;
    }

    protected void prepared() {

        // featureCount = features.size();
    }

    @Override
    public String getSimulationPanelClass() {
        return null;
    }

    @Override
    public long getUniqueId() {
        return uniqueId;
    }

    @Override
    public String getAddress() {
        return ByteUtils.bytesToHex(nodeAddress);
    }

    @Override
    public String getLocalAddress() {
        int len = nodeAddress.length;

        return ByteUtils.byteToHex(nodeAddress[len - 1]);
    }

    protected byte getLocalAddr() {
        int len = nodeAddress.length;
        return nodeAddress[len - 1];
    }

    @Override
    public void init() {

        prepareFeatures();
        prepareCVs();
        prepared();

    }

    @Override
    public void postConstruct() {
    }

    private AtomicBoolean requestWorkerRunning = new AtomicBoolean();

    @Override
    public void start() {

        if (requestWorker != null) {
            LOGGER.error("The simulator with address {} is already started!", getAddress());
            throw new IllegalStateException("The simulator is already started!");
        }

        // the address of myself is always 0
        String ownNodeAddress = "00";
        LOGGER.info("Add myself as subnode, address: {}, simulator: {}", ownNodeAddress, this);
        subNodes.put(ownNodeAddress.trim(), this);

        requestWorkerRunning.set(true);
        // use executor to send response
        requestWorker = new Thread(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Started request worker.");
                while (requestWorkerRunning.get()) {
                    try {
                        BidibCommand bidibMessage = sendQueue.take();
                        if (bidibMessage != null) {
                            LOGGER.info("Process message: {}", bidibMessage);
                            byte[] response = prepareResponse(bidibMessage);
                            publishResponse(response);

                            LOGGER.info("Process message has finished: {}", bidibMessage);
                        }
                        else {
                            LOGGER.info("No message available.");
                        }
                    }
                    catch (InterruptedException ex) {
                        if (requestWorkerRunning.get()) {
                            LOGGER.warn("Process request and send response failed.", ex);
                        }
                        else {
                            LOGGER.info("Request worker was interrupted while waiting for messages.");
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Process request and send response failed.", ex);
                    }
                }
                LOGGER.info("Process message worker has finished.");
            }
        });
        requestWorker.start();

        SimulatorStatusEvent simulatorStatusEvent =
            new SimulatorStatusEvent(ByteUtils.bytesToHex(nodeAddress), SimulatorStatusEvent.Status.started);
        EventBus.publish(simulatorStatusEvent);

    }

    @Override
    public void stop() {
        LOGGER.info("The simulator is stopped.");
        // for (SimulatorListener listener : simulatorListeners) {
        // listener.stop();
        // }

        SimulatorStatusEvent simulatorStatusEvent =
            new SimulatorStatusEvent(ByteUtils.bytesToHex(nodeAddress), SimulatorStatusEvent.Status.stopped);
        EventBus.publish(simulatorStatusEvent);

        availableAfterResetWorker.shutdownNow();
        responseWorker.shutdownNow();

        if (requestWorker != null) {
            try {
                requestWorkerRunning.set(false);
                requestWorker.interrupt();
                requestWorker.join(2000);
            }
            catch (InterruptedException e) {
                LOGGER.warn("Interrupt and wait for request worker failed.", e);
            }
            requestWorker = null;
        }
    }

    protected byte[] prepareResponse(BidibCommand bidibMessage) {
        byte[] response = null;
        switch (ByteUtils.getInt(bidibMessage.getType())) {
            case BidibLibrary.MSG_SYS_GET_MAGIC:
                response = processSysGetMagicRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_CLOCK:
                processSysClockRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_GET_P_VERSION:
                response = processSysGetPVersionRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_GET_SW_VERSION:
                response = processSysGetSwVersionRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_ENABLE:
                processSysEnableRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_DISABLE:
                processSysDisableRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_GET_UNIQUE_ID:
                response = processSysGetUniqueIdRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_IDENTIFY:
                processSysIdentifyRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_PING:
                response = processSysPingRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_NODETAB_GETALL:
                response = processNodeTabGetAllRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_NODETAB_GETNEXT:
                response = processNodeTabGetNextRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_NODE_CHANGED_ACK:
                processNodeChangedAckRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_GET_ERROR:
                response = processSysGetErrorRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_FEATURE_GET:
                response = processFeatureGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_FEATURE_SET:
                response = processFeatureSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_FEATURE_GETALL:
                response = processFeatureGetAllRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_FEATURE_GETNEXT:
                response = processFeatureGetNextRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_VENDOR_ENABLE:
                response = processVendorEnableRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_VENDOR_DISABLE:
                response = processVendorDisableRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_VENDOR_SET:
                response = processVendorSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_VENDOR_GET:
                response = processVendorGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_STRING_SET:
                response = processStringSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_STRING_GET:
                response = processStringGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_SYS_RESET:
                LOGGER.info("MSG_SYS_RESET, reset sendNum.");
                // resetSendNum();
                processResetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_FW_UPDATE_OP:
                response = processFwUpdateOpRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LOCAL_EMITTER:
                processLocalEmitterRequest(bidibMessage);
                break;

            default:
                LOGGER.warn("Unknown message detected: {}", bidibMessage);
                break;
        }
        return response;
    }

    @Override
    public void processRequest(final BidibCommand bidibCommand) {
        LOGGER.info("Add request to send queue: {}", bidibCommand);

        sendQueue.offer(bidibCommand);
        LOGGER.info("Scheduled request for processing: {}", bidibCommand);
    }

    /**
     * This method is only called by the single worker thread.
     * 
     * @param response
     *            the response to send
     */
    protected synchronized void publishResponse(final byte[] response) {
        LOGGER.debug("Publish response.");
        if (response != null) {
            try {
                // set the send message number
                int sendMsgNum = getNextResponseSendNum();
                int index = 1;
                while (response[index] != 0) {
                    index++;
                }
                response[index + 1] = ByteUtils.getLowByte(sendMsgNum);

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                output.write(response);
                output.flush();
                LOGGER
                    .info("Send output with sendMsgNum: {} to receiver: {}, content: {}", sendMsgNum, messageReceiver,
                        ByteUtils.bytesToHex(response));

                // process the messages
                messageReceiver.publishResponse(output);
            }
            catch (ProtocolException | IOException ex) {
                LOGGER.warn("Send message to publish in messageReceiver failed.", ex);
            }
            catch (Exception ex) {
                LOGGER.warn("Publish messages failed.", ex);
            }
        }
        else {
            LOGGER.info("No response available to send.");
        }
    }

    protected void sendSpontanousResponse(final byte[] response) {
        LOGGER.info("Send spontanous response: {}", response);

        // use executor to send response
        responseWorker.schedule(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Send response: {}", response);
                publishResponse(response);
                LOGGER.info("Send response worker has finished.");
            }
        }, 0, TimeUnit.MILLISECONDS);
    }

    protected byte[] processSysGetMagicRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            LOGGER.info("Reset the sendNum because the SYS_MAGIC is tranferred with 0.");
            resetSendNum();
            SysMagicResponse magicResponse =
                new SysMagicResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) 0xFE, (byte) 0xAF);
            response = magicResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create magic response failed.", ex);
        }
        return response;
    }

    protected void processSysClockRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysClockMessage: {}, do nothing ...", bidibMessage);
    }

    protected void processLocalEmitterRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LocalEmitterMessage: {}, do nothing ...", bidibMessage);

        LocalEmitterMessage message = (LocalEmitterMessage) bidibMessage;
        LOGGER.info("The emitter of the message: {}", message.getEmitter());

    }

    protected byte[] processSysGetPVersionRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysGetPVersion request: {}, do nothing ...", bidibMessage);
        byte[] response = null;
        try {
            LOGGER.info("Current protocolVersion: {}", protocolVersion);
            SysPVersionResponse sysPVersionResponse =
                new SysPVersionResponse(bidibMessage.getAddr(), getNextSendNum(), protocolVersion.getMajorVersion(),
                    protocolVersion.getMinorVersion());
            response = sysPVersionResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create sysPVersion response failed.", ex);
        }
        return response;
    }

    protected byte[] processSysGetSwVersionRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysGetSwVersion request: {}", bidibMessage);
        byte[] response = null;
        try {
            LOGGER.info("Current softwareVersion: {}", softwareVersion);
            SysSwVersionResponse sysSwVersionResponse =
                new SysSwVersionResponse(bidibMessage.getAddr(), getNextSendNum(), softwareVersion.asByteArray());
            response = sysSwVersionResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create sysSwVersion response failed.", ex);
        }
        return response;
    }

    protected byte[] processSysGetErrorRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysGetError request: {}", bidibMessage);
        byte[] response = null;
        try {
            SysErrorResponse sysErrorResponse =
                new SysErrorResponse(bidibMessage.getAddr(), getNextSendNum(), SysErrorEnum.BIDIB_ERR_NONE);
            response = sysErrorResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create sysError response failed.", ex);
        }
        return response;
    }

    protected void processSysEnableRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysEnable request: {}, do nothing ...", bidibMessage);
    }

    protected void processSysDisableRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysDisable request: {}, do nothing ...", bidibMessage);
    }

    protected byte[] processSysGetUniqueIdRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysGetUniqueId request: {}, do nothing ...", bidibMessage);

        byte[] response = null;
        try {
            SysUniqueIdResponse sysUniqueIdResponse =
                new SysUniqueIdResponse(bidibMessage.getAddr(), getNextSendNum(), getUniqueId());
            response = sysUniqueIdResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create sysUniqueId response failed.", ex);
        }
        return response;
    }

    protected void processSysIdentifyRequest(BidibCommand bidibMessage) {
        // TODO Process the SysIdentify request
        LOGGER.info("Process the SysIdentify request: {}", bidibMessage);
    }

    protected byte[] processSysPingRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the SysPing request: {}", bidibMessage);

        byte[] response = null;
        try {
            SysPingMessage sysPingMessage = (SysPingMessage) bidibMessage;
            SysPongResponse sysPongResponse =
                new SysPongResponse(bidibMessage.getAddr(), getNextSendNum(), sysPingMessage.getMarker());
            response = sysPongResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create sysPong response failed.", ex);
        }
        return response;
    }

    protected byte[] processNodeTabGetAllRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            NodeTabCountResponse nodeTabCountResponse =
                new NodeTabCountResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) 0x01);
            response = nodeTabCountResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create nodeTabCount response failed.", ex);
        }
        return response;
    }

    protected byte[] processNodeTabGetNextRequest(BidibCommand bidibMessage) {
        // 01.01.2014 20:35:29.917: receive NodeTabResponse[[0],num=2,type=137,data=[1, 0, 210, 0, 13, 104, 0, 0, 54]] :
        // 0C 00 02 89 01 00 D2 00 0D 68 00 00 36

        byte[] response = null;
        byte nodeTabVersion = 1;
        byte localAddr = 0;
        try {
            NodeTabResponse nodeTabResponse =
                new NodeTabResponse(bidibMessage.getAddr(), getNextSendNum(), nodeTabVersion, localAddr, uniqueId);
            response = nodeTabResponse.getContent();
            LOGGER.info("Prepared nodeTab response: {}", ByteUtils.bytesToHex(response));
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create nodeTab response failed.", ex);
        }
        return response;
    }

    protected void processNodeChangedAckRequest(BidibCommand bidibMessage) {
        LOGGER.info("Node changed ack was received: {}", bidibMessage);
    }

    protected byte[] processFeatureGetRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            FeatureGetMessage featureGetMessage = (FeatureGetMessage) bidibMessage;
            int featureNum = featureGetMessage.getNumber();
            LOGGER.info("Get feature with number: {}", featureNum);

            Feature foundFeature = null;

            for (Feature feature : features) {
                if (feature.getType() == featureNum) {
                    foundFeature = feature;
                    LOGGER.info("Found feature: {}", foundFeature);
                    break;
                }
            }

            if (foundFeature != null) {
                FeatureResponse featureResponse =
                    new FeatureResponse(featureGetMessage.getAddr(), getNextSendNum(), featureNum,
                        foundFeature.getValue());
                response = featureResponse.getContent();
                LOGGER.info("Prepared response: {}", ByteUtils.bytesToHex(response));
            }
            else {
                FeatureNotAvailableResponse featureResponse =
                    new FeatureNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), featureNum);
                response = featureResponse.getContent();
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create feature response failed.", ex);
        }
        return response;
    }

    protected byte[] processFeatureSetRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            FeatureSetMessage featureSetMessage = (FeatureSetMessage) bidibMessage;
            int featureNum = featureSetMessage.getNumber();
            int featureValue = featureSetMessage.getValue();
            LOGGER.info("Set feature with number: {}, value: {}", featureNum, featureValue);

            Feature foundFeature = null;

            for (Feature feature : features) {
                if (feature.getType() == featureNum) {
                    foundFeature = updateFeatureValue(feature, featureValue);

                    LOGGER.info("Found feature: {}", foundFeature);
                    break;
                }
            }

            if (foundFeature == null && autoAddFeature) {
                LOGGER.info("AutoAddFeature is activated.");

                foundFeature = autoAddFeature(featureNum, featureValue);
            }

            if (foundFeature != null) {
                FeatureResponse featureResponse =
                    new FeatureResponse(featureSetMessage.getAddr(), getNextSendNum(), featureNum,
                        foundFeature.getValue());
                response = featureResponse.getContent();
                LOGGER.info("Prepared response: {}", ByteUtils.bytesToHex(response));
            }
            else {
                FeatureNotAvailableResponse featureResponse =
                    new FeatureNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), featureNum);
                response = featureResponse.getContent();
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create feature response failed.", ex);
        }
        return response;
    }

    protected Feature updateFeatureValue(Feature feature, int featureValue) {
        feature.setValue(featureValue);

        return feature;
    }

    protected Feature autoAddFeature(int featureNum, int featureValue) {

        Feature foundFeature = new Feature(featureNum, featureValue);
        features.add(foundFeature);
        // featureCount = features.size();

        return foundFeature;
    }

    protected byte[] processFeatureGetAllRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            FeatureCountResponse featureResponse =
                new FeatureCountResponse(bidibMessage.getAddr(), getNextSendNum(), features.size());
            response = featureResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create feature count response failed.", ex);
        }
        currentFeature = 0;

        return response;
    }

    protected byte[] processFeatureGetNextRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        if (currentFeature >= features.size()) {
            try {
                FeatureNotAvailableResponse featureResponse =
                    new FeatureNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), 255);
                response = featureResponse.getContent();
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create feature N/A response failed.", ex);
            }
        }
        else {
            try {
                Feature feature = IterableUtils.get(features, currentFeature);
                // Feature feature = features.toArray(new Feature[0])[currentFeature];
                FeatureResponse featureResponse =
                    new FeatureResponse(bidibMessage.getAddr(), getNextSendNum(), feature.getType(),
                        feature.getValue());
                response = featureResponse.getContent();
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create feature response failed.", ex);
            }
            catch (Exception ex) {
                LOGGER.warn("Create feature response failed.", ex);
            }
        }
        currentFeature++;
        return response;
    }

    protected byte[] processVendorEnableRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            VendorEnableMessage vendorEnableMessage = (VendorEnableMessage) bidibMessage;
            long uniqueId = vendorEnableMessage.getUniqueId();
            LOGGER.info("Enable the user config mode for uniqueId: {}", uniqueId);
            byte userConfigModeActive = 1;
            VendorAckResponse vendorAckResponse =
                new VendorAckResponse(bidibMessage.getAddr(), getNextSendNum(), userConfigModeActive);
            response = vendorAckResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create vendor ack response failed.", ex);
        }
        return response;
    }

    protected byte[] processVendorDisableRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            // VendorDisableMessage vendorDisableMessage = (VendorDisableMessage) bidibMessage;
            LOGGER.info("Disable the user config mode");
            byte userConfigModeActive = 0;
            VendorAckResponse vendorAckResponse =
                new VendorAckResponse(bidibMessage.getAddr(), getNextSendNum(), userConfigModeActive);
            response = vendorAckResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create vendor ack response failed.", ex);
        }
        return response;
    }

    protected byte[] processVendorSetRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            VendorSetMessage vendorSetMessage = (VendorSetMessage) bidibMessage;
            VendorData vendorData = vendorSetMessage.getVendorData();
            LOGGER.info("Set the vendor data: {}", vendorData);

            // store the vendor data ...
            configurationVariables.put(vendorData.getName(), vendorData.getValue());

            VendorResponse vendorResponse =
                new VendorResponse(bidibMessage.getAddr(), getNextSendNum(), vendorData.getName(),
                    vendorData.getValue());
            response = vendorResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create vendor response failed.", ex);
        }
        return response;
    }

    protected byte[] processVendorGetRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            VendorGetMessage vendorGetMessage = (VendorGetMessage) bidibMessage;
            String vendorDataName = vendorGetMessage.getVendorDataName();
            LOGGER.info("Get the vendor data with name: {}", vendorDataName);

            // fetch the value from the stored data ...
            String vendorDataValue = configurationVariables.get(vendorDataName);
            if (StringUtils.isBlank(vendorDataValue)) {
                vendorDataValue = "";
            }

            VendorResponse vendorResponse =
                new VendorResponse(bidibMessage.getAddr(), getNextSendNum(), vendorDataName, vendorDataValue);
            response = vendorResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create vendor response failed.", ex);
        }
        return response;
    }

    protected byte[] processStringSetRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            StringSetMessage stringSetMessage = (StringSetMessage) bidibMessage;
            int stringId = stringSetMessage.getStringId();
            stringValue[stringId] = stringSetMessage.getString();
            StringResponse stringResponse =
                new StringResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(stringSetMessage.getNamespace()),
                    ByteUtils.getLowByte(stringSetMessage.getStringId()), stringValue[stringId]);
            response = stringResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create string response failed.", ex);
        }
        return response;
    }

    protected byte[] processStringGetRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            StringGetMessage stringGetMessage = (StringGetMessage) bidibMessage;
            int stringId = stringGetMessage.getStringId();

            LOGGER.info("Get STRING[{}]: {}", stringId, stringValue[stringId]);

            StringResponse stringResponse =
                new StringResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(stringGetMessage.getNamespace()),
                    ByteUtils.getLowByte(stringGetMessage.getStringId()), stringValue[stringId]);
            response = stringResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create string response failed.", ex);
        }
        return response;
    }

    protected byte[] processFwUpdateOpRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            FwUpdateOpMessage fwUpdateOpMessage = (FwUpdateOpMessage) bidibMessage;
            FirmwareUpdateOperation operation = fwUpdateOpMessage.getOperation();

            LOGGER.info("processFwUpdateOpRequest, operation: {}", operation);

            int timeout = 0;
            int status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_ERROR;
            switch (operation) {
                case ENTER:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_READY;
                    break;
                case DATA:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_DATA;
                    break;
                case SETDEST:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_DATA;
                    break;
                case EXIT:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_EXIT;
                    break;
                default:
                    break;
            }
            FwUpdateStatResponse fwUpdateStatResponse =
                new FwUpdateStatResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(status),
                    ByteUtils.getLowByte(timeout));
            response = fwUpdateStatResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create fwUpdateStat response failed.", ex);
        }
        return response;
    }

    protected void processResetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the reset request, bidibMessage: {}", bidibMessage);
        resetSendNum();

        final byte[] oldNodeAddress = nodeAddress;

        LOGGER.info("Notify the parent that we have gone, address: {}, uniqueId: {}", getLocalAddress(), uniqueId);
        // notify the master that we're gone
        EventBus.publish(new NodeLostEvent(nodeAddress, uniqueId));

        // send the node available event after 1 second
        availableAfterResetWorker.schedule(new Runnable() {
            @Override
            public void run() {
                NodeAvailableEvent nodeAvailableEvent = new NodeAvailableEvent(oldNodeAddress, uniqueId);
                LOGGER.info("Send NodeAvailableEvent: {}", nodeAvailableEvent);
                EventBus.publish(nodeAvailableEvent);

                LOGGER.info("Send availableAfterResetWorker has finished.");
            }
        }, 1, TimeUnit.SECONDS);

    }

    protected void resetSendNum() {
        LOGGER.info("Reset the sendNum to 0.");
        sendNum = 0;
    }

    protected int getNextSendNum() {
        return 0;
    }

    protected int getCurrentSendNum() {
        return sendNum;
    }

    protected int getNextResponseSendNum() {
        int nextSendNum = sendNum;
        sendNum++;
        if (sendNum > 255) {
            sendNum = 0;
        }
        return nextSendNum;
    }

    /**
     * @param address
     *            the address
     * @return <code>true</code>: if the nodeAddress equals the provided address, <code>false</code>: otherwise
     */
    protected boolean isAddressEqual(String address) {
        if (!ByteUtils.bytesToHex(this.nodeAddress).equals(address)) {
            LOGGER.trace("Another node is addressed.");
            return false;
        }
        return true;
    }

    @Override
    public void queryStatus(Class<?> portClass) {
    }

    @EventSubscriber(eventClass = AccessoryStateErrorEvent.class)
    public void accessoryStateError(AccessoryStateErrorEvent accessoryStateErrorEvent) {
        LOGGER.info("The change of the accessoryStateErrorEvent was requested: {}", accessoryStateErrorEvent);
        String nodeAddress = accessoryStateErrorEvent.getNodeAddr();

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        // prepare the request
        byte accessoryNum = 0x01;
        byte aspect = 0x02;
        byte[] value = new byte[] { 0x03, (byte) 0x81, BidibLibrary.BIDIB_ACC_STATE_ERROR_SERVO };
        try {
            final AccessoryNotifyResponse accessoryNotifyResponse =
                new AccessoryNotifyResponse(getNodeAddress(), getNextSendNum(), accessoryNum, aspect, value);
            publishResponse(accessoryNotifyResponse.getContent());
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Send accessoryNotifyResponse failed.", ex);
        }
    }

    @EventSubscriber(eventClass = SysErrorEvent.class)
    public void sysError(SysErrorEvent sysErrorEvent) {
        LOGGER.info("The change of the sysError was requested: {}", sysErrorEvent);
        String nodeAddress = sysErrorEvent.getNodeAddr();

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        // prepare the request
        try {
            byte[] reason = null;
            switch (sysErrorEvent.getSysError()) {
                case BIDIB_ERR_ADDRSTACK:
                    byte[] nodeAddr = getNodeAddress();
                    if (nodeAddr.length < 4) {
                        byte[] fullAddr = new byte[] { 0, 0, 0, 0 };
                        for (int index = 0; index < nodeAddr.length; index++) {
                            byte addr = nodeAddr[index];
                            fullAddr[index] = addr;
                        }
                        nodeAddr = fullAddr;
                    }
                    reason = nodeAddr;
                    break;
                default:
                    reason = sysErrorEvent.getReason();
                    break;
            }
            final SysErrorResponse sysErrorResponse =
                new SysErrorResponse(getNodeAddress(), getNextSendNum(), sysErrorEvent.getSysError(), reason) {
                    @Override
                    public byte[] getAddr() {
                        return getNodeAddress();
                    }
                };
            publishResponse(sysErrorResponse.getContent());
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Send sysErrorResponse failed.", ex);
        }
    }

    @EventSubscriber(eventClass = StallEvent.class)
    public void stall(StallEvent stallEvent) {
        LOGGER.info("The change of the stall was requested: {}", stallEvent);
        String nodeAddress = stallEvent.getNodeAddr();

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        // prepare the request
        try {
            final StallResponse stallResponse =
                new StallResponse(getNodeAddress(), getNextSendNum(), (byte) (stallEvent.getStall() ? 1 : 0)) {
                    @Override
                    public byte[] getAddr() {
                        return getNodeAddress();
                    }
                };
            publishResponse(stallResponse.getContent());
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Send stallResponse failed.", ex);
        }
    }

    @EventSubscriber(eventClass = IdentifyEvent.class)
    public void identify(IdentifyEvent identifyEvent) {
        LOGGER.info("The change of the identify was requested: {}", identifyEvent);
        String nodeAddress = identifyEvent.getNodeAddr();

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        // prepare the request
        try {
            if (identifyEvent.getIdentify() == null) {
                // toggle
                identifyActive = !identifyActive;
            }
            else {
                identifyActive = identifyEvent.getIdentify();
            }

            final SysIdentifyResponse stallResponse =
                new SysIdentifyResponse(getNodeAddress(), getNextSendNum(), (byte) (identifyActive ? 1 : 0)) {
                    @Override
                    public byte[] getAddr() {
                        return getNodeAddress();
                    }
                };
            publishResponse(stallResponse.getContent());
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Send identifyResponse failed.", ex);
        }
    }
}
