package org.bidib.jbidibc.net.udp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;

import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.net.DataPacket;
import org.bidib.jbidibc.net.NetBidibPort;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UdpNetMessageHandler implements NetMessageHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(UdpNetMessageHandler.class);

    private BidibMessageProcessor messageReceiverDelegate;

    private BidibUdpNetAddress remoteAddress;

    private final ConnectionListener connectionListener;

    /**
     * Creates a new instance of DefaultNetMessageHandler.
     * 
     * @param messageReceiverDelegate
     *            the delegate message receiver that processes the BiDiB messages
     * @param address
     *            the address of the master to connect to
     * @param port
     *            the port of the master to connect to
     * @param connectionListener
     *            the connection listener
     */
    public UdpNetMessageHandler(BidibMessageProcessor messageReceiverDelegate, InetAddress address, int port,
        final ConnectionListener connectionListener) {
        this.messageReceiverDelegate = messageReceiverDelegate;
        this.connectionListener = connectionListener;

        LOGGER.info("Set the remote address: {}, port: {}", address, port);
        remoteAddress = new BidibUdpNetAddress(address, port);
    }

    private ByteArrayOutputStream receivePacketBuffer = new ByteArrayOutputStream(100);

    @Override
    public void receive(final DataPacket packet) {
        // a data packet was received ... process the envelope and extract the message
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Received a packet from address: {}, port: {}, data: {}", packet.getAddress(),
                packet.getPort(), ByteUtils.bytesToHex(packet.getData()));
        }

        // prepare data from packet wrapper data and forward to the MessageReceiver
        try {
            receivePacketBuffer.write(packet.getData());

            LOGGER.info("Forward received message to messageReceiverDelegate: {}, data: {}", messageReceiverDelegate,
                ByteUtils.bytesToHex(receivePacketBuffer));

            messageReceiverDelegate.receive(receivePacketBuffer);
        }
        catch (Exception ex) {
            LOGGER.warn("Process messages failed.", ex);
            throw new RuntimeException(ex);
        }
        finally {
            receivePacketBuffer.reset();
        }
    }

    private ByteArrayOutputStream sendPacketBuffer = new ByteArrayOutputStream(100);

    @Override
    public void send(NetBidibPort port, byte[] bytes) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Send message to port: {}, message: {}", port, ByteUtils.bytesToHex(bytes));
        }

        if (remoteAddress == null) {
            LOGGER.warn("### No remote addresses available. The message will not be sent!");
            return;
        }

        if (port != null) {

            try {
                // add the bidib message payload
                sendPacketBuffer.write(bytes);

                LOGGER.info("Send message to remote address, address: {}, port: {}", remoteAddress.getAddress(),
                    remoteAddress.getPortNumber());

                // send the message to the port
                port.send(sendPacketBuffer.toByteArray(), remoteAddress.getAddress(), remoteAddress.getPortNumber());
            }
            catch (IOException ex) {
                LOGGER.warn("Send message to port failed.", ex);
                throw new RuntimeException("Send message to datagram socket failed.", ex);
            }
            finally {
                sendPacketBuffer.reset();
            }
        }
        else {
            LOGGER.warn("Send not possible, the port is closed.");
        }
    }

    @Override
    public void acceptClient(String remoteHost) {
        LOGGER.info("Accept client with address: {}", remoteHost);
    }

    @Override
    public void cleanup(String remoteHost) {
        LOGGER.info("Cleanup client with address: {}", remoteHost);

        connectionListener.closed(remoteHost);
    }

}
