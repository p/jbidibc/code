package org.bidib.jbidibc.decoder.json;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DecoderDbStatusResponse {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ManufacturersStatusResponse extends StatusResponse {

        private Date nmraListDate;

        /**
         * @return the nmraListDate
         */
        public Date getNmraListDate() {
            return nmraListDate;
        }

        /**
         * @param nmraListDate
         *            the nmraListDate to set
         */
        public void setNmraListDate(Date nmraListDate) {
            this.nmraListDate = nmraListDate;
        }

        // @Override
        // public String toString() {
        // return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        // }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DecoderStatusResponse extends StatusResponse {
        private Integer manufacturerId;

        private Integer manufacturerExtendedId;

        private String name;

        public Integer getManufacturerId() {
            return manufacturerId;
        }

        public void setManufacturerId(Integer manufacturerId) {
            this.manufacturerId = manufacturerId;
        }

        /**
         * @return the manufacturerExtendedId
         */
        public Integer getManufacturerExtendedId() {
            return manufacturerExtendedId;
        }

        /**
         * @param manufacturerExtendedId
         *            the manufacturerExtendedId to set
         */
        public void setManufacturerExtendedId(Integer manufacturerExtendedId) {
            this.manufacturerExtendedId = manufacturerExtendedId;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name
         *            the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        // @Override
        // public String toString() {
        // return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        // }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FirmwareStatusResponse extends StatusResponse {
        private String manufacturerId;

        private String manufacturerExtendedId;

        private String version;

        private String versionExtension;

        public String getManufacturerId() {
            return manufacturerId;
        }

        public void setManufacturerId(String manufacturerId) {
            this.manufacturerId = manufacturerId;
        }

        /**
         * @return the manufacturerExtendedId
         */
        public String getManufacturerExtendedId() {
            return manufacturerExtendedId;
        }

        /**
         * @param manufacturerExtendedId
         *            the manufacturerExtendedId to set
         */
        public void setManufacturerExtendedId(String manufacturerExtendedId) {
            this.manufacturerExtendedId = manufacturerExtendedId;
        }

        /**
         * @return the version
         */
        public String getVersion() {
            return version;
        }

        /**
         * @param version
         *            the version to set
         */
        public void setVersion(String version) {
            this.version = version;
        }

        /**
         * @return the versionExtension
         */
        public String getVersionExtension() {
            return versionExtension;
        }

        /**
         * @param versionExtension
         *            the versionExtension to set
         */
        public void setVersionExtension(String versionExtension) {
            this.versionExtension = versionExtension;
        }

        // @Override
        // public String toString() {
        // return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        // }
    }

    private ManufacturersStatusResponse manufacturers;

    private StatusResponse decoderDetections;

    /**
     * @return the decoderDetections
     */
    public StatusResponse getDecoderDetections() {
        return decoderDetections;
    }

    /**
     * @param decoderDetections
     *            the decoderDetections to set
     */
    public void setDecoderDetections(StatusResponse decoderDetections) {
        this.decoderDetections = decoderDetections;
    }

    /**
     * @return the manufacturers
     */
    public ManufacturersStatusResponse getManufacturers() {
        return manufacturers;
    }

    /**
     * @param manufacturers
     *            the manufacturers to set
     */
    public void setManufacturers(ManufacturersStatusResponse manufacturers) {
        this.manufacturers = manufacturers;
    }

    private DecoderStatusResponse[] decoder;

    /**
     * @return the decoderStatuses
     */
    public DecoderStatusResponse[] getDecoder() {
        return decoder;
    }

    /**
     * @param decoderStatuses
     *            the decoderStatuses to set
     */
    public void setDecoder(DecoderStatusResponse[] decoder) {
        this.decoder = decoder;
    }

    private FirmwareStatusResponse[] firmware;

    /**
     * @return the firmware
     */
    public FirmwareStatusResponse[] getFirmware() {
        return firmware;
    }

    /**
     * @param firmware
     *            the firmware to set
     */
    public void setFirmware(FirmwareStatusResponse[] firmware) {
        this.firmware = firmware;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
