package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class FeedbackMirrorFreeMessage extends BidibCommandMessage {
    public FeedbackMirrorFreeMessage(int detectorNumber) {
        super(0, BidibLibrary.MSG_BM_MIRROR_FREE, ByteUtils.getLowByte(detectorNumber));
    }

    public FeedbackMirrorFreeMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_BM_MIRROR_FREE";
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }
}
