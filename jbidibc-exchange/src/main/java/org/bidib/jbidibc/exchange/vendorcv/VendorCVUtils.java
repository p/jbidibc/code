package org.bidib.jbidibc.exchange.vendorcv;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VendorCVUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(VendorCVUtils.class);

    public static NodetextType getNodetextOfLanguage(NodeType nodeType, String language) {
        if (!CollectionUtils.isEmpty(nodeType.getNodetext())) {
            for (Object obj : nodeType.getNodetext()) {
                if (obj instanceof NodetextType) {
                    NodetextType nodetext = (NodetextType) obj;
                    if (language.equalsIgnoreCase(nodetext.getLang())) {
                        return nodetext;
                    }
                }
            }
        }
        return null;
    }

    public static List<CVType> getCVs(TemplateType templateType) {
        List<CVType> cvTypes = new LinkedList<CVType>();
        if (!CollectionUtils.isEmpty(templateType.getCVOrNodeOrRepeater())) {
            for (Object obj : templateType.getCVOrNodeOrRepeater()) {
                if (obj instanceof CVType) {
                    CVType cv = (CVType) obj;

                    cvTypes.add(cv);
                }
                else if (obj instanceof RepeaterType) {
                    // process repeater
                    LOGGER.debug("Expand the repeater.");
                    RepeaterType repeater = (RepeaterType) obj;
                    // offset is mandatory
                    Integer offset = repeater.getOffset();

                    Integer count = repeater.getCount();
                    if (count == null) {
                        count = Integer.valueOf(1);
                    }

                    Integer step = repeater.getNext();
                    if (step == null) {
                        step = Integer.valueOf(1);
                    }

                    List<CVType> cvList = repeater.getCV();
                    if (!CollectionUtils.isEmpty(cvList)) {
                        // prepare the CVs node text if the node text contains placeholders

                        for (int index = 0; index < count; index++) {

                            for (CVType repeaterCV : cvList) {
                                CVType clone = prepareCVClone(repeaterCV, index, offset, step);

                                LOGGER.debug("Add clone: {}", clone);
                                cvTypes.add(clone);
                            }

                        }
                    }
                }
            }
        }
        return cvTypes;
    }

    public static List<CVType> getCVs(NodeType nodeType) {
        List<CVType> cvTypes = new LinkedList<CVType>();
        if (!CollectionUtils.isEmpty(nodeType.getNodeOrCVOrRepeater())) {
            for (Object obj : nodeType.getNodeOrCVOrRepeater()) {
                if (obj instanceof CVType) {
                    CVType cv = (CVType) obj;
                    cvTypes.add(cv);
                }
                else if (obj instanceof RepeaterType) {
                    // process repeater
                    LOGGER.debug("Expand the repeater.");
                    RepeaterType repeater = (RepeaterType) obj;
                    // offset is mandatory
                    Integer offset = repeater.getOffset();

                    Integer count = repeater.getCount();
                    if (count == null) {
                        count = Integer.valueOf(1);
                    }

                    Integer step = repeater.getNext();
                    if (step == null) {
                        step = Integer.valueOf(1);
                    }

                    List<CVType> cvList = repeater.getCV();
                    if (!CollectionUtils.isEmpty(cvList)) {
                        // prepare the CVs node text if the node text contains placeholders

                        for (int index = 0; index < count; index++) {

                            for (CVType repeaterCV : cvList) {
                                CVType clone = prepareCVClone(repeaterCV, index, offset, step);

                                LOGGER.debug("Add clone: {}", clone);
                                cvTypes.add(clone);
                            }

                        }
                    }
                }
            }
        }
        return cvTypes;
    }

    public static List<NodeType> getSubNodes(NodeType nodeType) {
        List<NodeType> subNodes = new LinkedList<NodeType>();
        if (nodeType.getNodeOrCVOrRepeater() != null) {
            for (Object obj : nodeType.getNodeOrCVOrRepeater()) {
                if (obj instanceof NodeType) {
                    NodeType node = (NodeType) obj;
                    subNodes.add(node);
                }
            }
        }
        return subNodes;
    }

    public static CVType prepareCVClone(CVType cv, int index, int offset, int step) {
        LOGGER.trace("Create new CV clone, index: {}, offset: {}, step: {}", index, offset, step);
        // correct the high and low CV
        String highCv = cv.getHigh();
        if (StringUtils.isNotBlank(highCv) && StringUtils.isNumeric(highCv)) {
            int highCvVal = Integer.parseInt(highCv);
            highCvVal += (index * step) + offset;
            highCv = String.valueOf(highCvVal);
        }
        else {
            highCv = null;
        }
        String lowCv = cv.getLow();
        if (StringUtils.isNotBlank(lowCv) && StringUtils.isNumeric(lowCv)) {
            int lowCvVal = Integer.parseInt(lowCv);
            lowCvVal += (index * step) + offset;
            lowCv = String.valueOf(lowCvVal);
        }
        else {
            lowCv = null;
        }

        String cvNum = cv.getNumber();
        if (StringUtils.isNumeric(cvNum)) {
            int cvNumVal = Integer.parseInt(cvNum);
            cvNumVal += (index * step) + offset;
            cvNum = Integer.toString(cvNumVal);
        }
        else {
            cvNum = cvNum + " (" + ((index * step) + offset) + ")";
        }

        CVType clone =
            new CVType()
                .withNumber(cvNum/* cv.getNumber() + (index * step) + offset */).withDescription(cv.getDescription())
                .withHigh(highCv).withLow(lowCv).withMin(cv.getMin()).withMax(cv.getMax()).withValues(cv.getValues())
                .withMode(cv.getMode()).withType(cv.getType()).withBitdescription(cv.getBitdescription())
                .withRebootneeded(cv.isRebootneeded()).withLowbits(cv.getLowbits()).withHighbits(cv.getHighbits())
                .withRadiobits(cv.getRadiobits()).withRadiovalues(cv.getRadiovalues())
                .withRadioGroups(cv.getRadioGroups()).withKeyword(cv.getKeyword()).withRegex(cv.getRegex());

        List<DescriptionType> descriptions = clone.getDescription();
        if (!CollectionUtils.isEmpty(descriptions)) {
            List<DescriptionType> replacedDescriptions = new ArrayList<>();
            for (DescriptionType description : descriptions) {

                if (description != null) {
                    // create a copy of descriptions because all lists point to the same content
                    DescriptionType newDescription =
                        new DescriptionType()
                            .withHelp(description.help).withLang(description.lang).withText(description.text);

                    String value = newDescription.getText();
                    value = replacePlaceholders(value, index);
                    // String replacementD = Integer.toString(index /* + 1 */);
                    // value = value.replaceAll("%%d", replacementD);
                    // String replacementP = Integer.toString(index + 1 /* + 2 */);
                    // value = value.replaceAll("%%p", replacementP);
                    // LOGGER.trace("Prepared value: {}", value);

                    newDescription.setText(value);

                    replacedDescriptions.add(newDescription);
                }
            }
            clone.getDescription().clear();
            clone.getDescription().addAll(replacedDescriptions);
        }

        return clone;
    }

    public static CVType prepareCVClone(CVType cv, String cvNumber, String lowCv, String highCv, int index) {
        LOGGER.trace("Create new CV clone, index: {}", index);

        CVType clone =
            new CVType()
                .withNumber(cvNumber).withDescription(cv.getDescription()).withHigh(highCv).withLow(lowCv)
                .withMin(cv.getMin()).withMax(cv.getMax()).withValues(cv.getValues()).withMode(cv.getMode())
                .withType(cv.getType()).withBitdescription(cv.getBitdescription()).withRebootneeded(cv.isRebootneeded())
                .withLowbits(cv.getLowbits()).withHighbits(cv.getHighbits()).withRadiobits(cv.getRadiobits())
                .withRadiovalues(cv.getRadiovalues()).withRadioGroups(cv.getRadioGroups()).withKeyword(cv.getKeyword())
                .withRegex(cv.getRegex());

        List<DescriptionType> descriptions = clone.getDescription();
        if (!CollectionUtils.isEmpty(descriptions)) {
            List<DescriptionType> replacedDescriptions = new ArrayList<>();
            for (DescriptionType description : descriptions) {

                if (description != null) {
                    // create a copy of descriptions because all lists point to the same content
                    DescriptionType newDescription =
                        new DescriptionType()
                            .withHelp(description.help).withLang(description.lang).withText(description.text);

                    String value = newDescription.getText();
                    value = replacePlaceholders(value, index);
                    // String replacementD = Integer.toString(index /* + 1 */);
                    // value = value.replaceAll("%%d", replacementD);
                    // String replacementP = Integer.toString(index + 1 /* + 2 */);
                    // value = value.replaceAll("%%p", replacementP);
                    // LOGGER.trace("Prepared value: {}", value);

                    newDescription.setText(value);

                    replacedDescriptions.add(newDescription);
                }
            }
            clone.getDescription().clear();
            clone.getDescription().addAll(replacedDescriptions);
        }

        return clone;
    }

    public static CVType processKeyword(CVType cvType, String keyword, int index) {

        keyword = replacePlaceholders(keyword, index);
        cvType = cvType.withKeyword(keyword);

        // // special processing for keyword
        // if (StringUtils.isNotBlank(keyword)) {
        // String replacementD = Integer.toString(index);
        // keyword = keyword.replaceAll("%%d", replacementD);
        // String replacementP = Integer.toString(index + 1);
        // keyword = keyword.replaceAll("%%p", replacementP);
        // LOGGER.trace("Prepared value: {}", keyword);
        // cvType = cvType.withKeyword(keyword);
        // }

        return cvType;
    }

    private static String replacePlaceholders(String text, int index) {

        if (StringUtils.isNotBlank(text)) {
            String replacementD = Integer.toString(index);
            text = text.replaceAll("%%d", replacementD);
            String replacementP = Integer.toString(index + 1);
            text = text.replaceAll("%%p", replacementP);
            LOGGER.trace("Prepared value: {}", text);
        }
        return text;
    }

}
