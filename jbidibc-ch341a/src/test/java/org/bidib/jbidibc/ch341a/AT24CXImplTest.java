package org.bidib.jbidibc.ch341a;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class AT24CXImplTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AT24CXImplTest.class);

    @Test(enabled = false)
    public void read() {

        AT24CX at24cx = new AT24CXImpl();

        at24cx.open(0);

        byte readValue = at24cx.read(0);
        LOGGER.info("Read value: {}", ByteUtils.byteToHex(readValue));

        byte[] readValues = at24cx.read(0, 1);
        LOGGER.info("Read values: {}", ByteUtils.bytesToHex(readValues));

        readValues = at24cx.read(1, 1);
        LOGGER.info("Read values: {}", ByteUtils.bytesToHex(readValues));

        at24cx.close();

    }
}
