package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class MessageReceiverTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiverTest.class);

    @Test
    public void processMessages() throws ProtocolException, IOException {

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        NodeRegistry nodeRegistry = new NodeRegistry();
        nodeRegistry.setBidib(bidib);
        nodeRegistry.setIgnoreMissingTransferListeners(true);

        // create the request factory
        RequestFactory requestFactory = new RequestFactory();
        nodeRegistry.setRequestFactory(requestFactory);

        requestFactory.initialize();

        Node coreNode = new Node(1, new byte[] { 0 }, 0xda000d680064eaL);
        nodeRegistry.createNode(coreNode);

        // create the message receiver
        MessageReceiver messageReceiver = new MessageReceiver(nodeRegistry, new BidibResponseFactory(), true) {
            @Override
            public void receive(ByteArrayOutputStream data) {
                LOGGER.warn("receive is not implemented!");
            }
        };

        ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

        // 09.09.2015 22:25:47.901: [Thread-6] - << [12] - 09 00 1A B2 00 32 01 A5 02 19 32 FE

        output.write(new byte[] { 0x09, 0x00, 0x1A, (byte) 0xB2, 0x00, 0x32, 0x01, (byte) 0xA5, 0x02, 0x19 });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));

        output.write(new byte[] { 0x09, 0x00, 0x1B, (byte) 0xB2, 0x00, 0x32, 0x01, (byte) 0xA5, 0x02, 0x1A });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));

        output.write(new byte[] { 0x09, 0x00, 0x1C, (byte) 0xB2, 0x00, 0x32, 0x01, (byte) 0xA5, 0x02, 0x1C });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));

        output.write(new byte[] { 0x09, 0x00, 0x1D, (byte) 0xB2, 0x00, 0x32, 0x01, (byte) 0xA5, 0x02, 0x1A });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));

        output.write(new byte[] { 0x09, 0x00, 0x1E, (byte) 0xB2, 0x00, 0x32, 0x01, (byte) 0xA5, 0x02, 0x1C });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));

        output.write(new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));
    }

    @Test
    public void receive2MessagesTest() throws IOException, ProtocolException {

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        NodeRegistry nodeRegistry = new NodeRegistry();
        nodeRegistry.setBidib(bidib);
        nodeRegistry.setIgnoreMissingTransferListeners(true);

        // create the request factory
        RequestFactory requestFactory = new RequestFactory();
        nodeRegistry.setRequestFactory(requestFactory);

        requestFactory.initialize();

        Node coreNode = new Node(1, new byte[] { 0 }, 0xda000d680064eaL);
        BidibNode bidibNode = nodeRegistry.createNode(coreNode);

        // bidibNode.setNodeMagic(12345);

        // create the message receiver
        MessageReceiver messageReceiver = new MessageReceiver(nodeRegistry, new BidibResponseFactory(), true) {
            @Override
            public void receive(ByteArrayOutputStream data) {
                LOGGER.warn("receive is not implemented!");
            }
        };
        messageReceiver.setIgnoreWrongMessageNumber(false);

        ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

        // 17.06.2018 17:34:18.297: [receiveQueueWorker] - <<<< len: 15, data: 05 00 14 90 FD DD 10 05 00 15 90 FC 20 B4
        // FE
        // 17.06.2018 17:34:18.297: [receiveQueueWorker] - << [13] - 05 00 14 90 FD 10 05 00 15 90 FC 20 B4

        output.write(new byte[] { 0x05, 0x00, 0x14, (byte) 0x90, (byte) 0xFD, 0x10, 0x05, 0x00, 0x15, (byte) 0x90,
            (byte) 0xFC, 0x20, (byte) 0xB4 });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));
    }

    @Test(expectedExceptions = ProtocolException.class)
    public void receive2MessagesWithWrongMessageNumberAndNodeMagicSetTest() throws IOException, ProtocolException {

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        NodeRegistry nodeRegistry = new NodeRegistry();
        nodeRegistry.setBidib(bidib);
        nodeRegistry.setIgnoreMissingTransferListeners(true);

        // create the request factory
        RequestFactory requestFactory = new RequestFactory();
        nodeRegistry.setRequestFactory(requestFactory);

        requestFactory.initialize();

        Node coreNode = new Node(1, new byte[] { 0 }, 0xda000d680064eaL);
        BidibNode bidibNode = nodeRegistry.createNode(coreNode);

        bidibNode.setNodeMagic(12345);

        // create the message receiver
        MessageReceiver messageReceiver = new MessageReceiver(nodeRegistry, new BidibResponseFactory(), true) {
            @Override
            public void receive(ByteArrayOutputStream data) {
                LOGGER.warn("receive is not implemented!");
            }
        };
        messageReceiver.setIgnoreWrongMessageNumber(false);

        ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

        // 17.06.2018 17:34:18.297: [receiveQueueWorker] - <<<< len: 15, data: 05 00 14 90 FD DD 10 05 00 15 90 FC 20 B4
        // FE
        // 17.06.2018 17:34:18.297: [receiveQueueWorker] - << [13] - 05 00 14 90 FD 10 05 00 15 90 FC 20 B4

        output.write(new byte[] { 0x05, 0x00, 0x14, (byte) 0x90, (byte) 0xFD, 0x10, 0x05, 0x00, 0x15, (byte) 0x90,
            (byte) 0xFC, 0x20, (byte) 0xB4 });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));
    }

    @Test
    public void receive2MessagesWithWrongMessageNumberAndNodeMagicSetWithIgnoreWrongMsgNumbersTest()
        throws IOException, ProtocolException {

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        NodeRegistry nodeRegistry = new NodeRegistry() {
            @Override
            protected BidibNode createBidibNode(Node node) {
                // add the mockito spy here to be able to verify the invocations
                return Mockito.spy(super.createBidibNode(node));
            }
        };
        NodeRegistry spyNodeRegistry = Mockito.spy(nodeRegistry);

        spyNodeRegistry.setBidib(bidib);
        spyNodeRegistry.setIgnoreMissingTransferListeners(true);

        // create the request factory
        RequestFactory requestFactory = new RequestFactory();
        spyNodeRegistry.setRequestFactory(requestFactory);

        requestFactory.initialize();

        Node coreNode = new Node(1, new byte[] { 0 }, 0xda000d680064eaL);
        BidibNode bidibNode = spyNodeRegistry.createNode(coreNode);

        bidibNode.setNodeMagic(12345);

        // create the message receiver
        MessageReceiver messageReceiver = new MessageReceiver(spyNodeRegistry, new BidibResponseFactory(), true) {
            @Override
            public void receive(ByteArrayOutputStream data) {
                LOGGER.warn("receive is not implemented!");
            }
        };
        messageReceiver.setIgnoreWrongMessageNumber(true);

        ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

        // 17.06.2018 17:34:18.297: [receiveQueueWorker] - <<<< len: 15, data: 05 00 14 90 FD DD 10 05 00 15 90 FC 20 B4
        // FE
        // 17.06.2018 17:34:18.297: [receiveQueueWorker] - << [13] - 05 00 14 90 FD 10 05 00 15 90 FC 20 B4

        output.write(new byte[] { 0x05, 0x00, 0x14, (byte) 0x90, (byte) 0xFD, 0x10, 0x05, 0x00, 0x15, (byte) 0x90,
            (byte) 0xFC, 0x20, (byte) 0xB4 });
        messageReceiver.processMessages(output);
        LOGGER.info("Remaining bytes in output: {}", ByteUtils.bytesToHex(output.toByteArray()));

        Mockito.verify(bidibNode, Mockito.times(2)).addResponseToReceiveQueue(Mockito.any());
    }

}
