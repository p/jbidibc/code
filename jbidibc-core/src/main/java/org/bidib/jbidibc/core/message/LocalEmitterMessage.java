package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;

/**
 * Message to signal the local emitter.
 */
public class LocalEmitterMessage extends BidibCommandMessage {
    public static final Integer TYPE = BidibLibrary.MSG_LOCAL_EMITTER;

    public LocalEmitterMessage() {
        super(0, BidibLibrary.MSG_LOCAL_EMITTER);
    }

    public LocalEmitterMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_LOCAL_EMITTER";
    }

    public String getEmitter() {
        return new String(getData());
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }
}
