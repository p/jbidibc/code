package org.bidib.jbidibc.net;

import java.net.InetAddress;

public class DataPacket {

    private byte[] receivedPacket;

    private InetAddress address;

    private int port;

    public DataPacket(byte[] receivedPacket, int startIndex, int receivedLen, final InetAddress address, final int port) {

        this.receivedPacket = new byte[receivedLen];
        System.arraycopy(receivedPacket, startIndex, this.receivedPacket, 0, receivedLen);

        this.address = address;
        this.port = port;
    }

    public byte[] getData() {
        return receivedPacket;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }
}
