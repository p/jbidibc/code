package org.bidib.jbidibc.core;

import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BidibPortTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void BidibPortInvalidNull() {
        new BidibPort(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void BidibPortInvalidOneByte() {
        new BidibPort(new byte[] { 0 });
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void BidibPortInvalidThreeBytes() {
        new BidibPort(new byte[] { 0, 1, 2 });
    }

    @Test
    public void BidibPortValid() {
        BidibPort bidibPort = new BidibPort(new byte[] { 0, 1 });
        Assert.assertEquals(bidibPort.getLowValue(), (byte) 0);
        Assert.assertEquals(bidibPort.getHighValue(), (byte) 1);
    }

    @Test
    public void BidibPortSystemFunctionTypeModel() {
        BidibPort bidibPort = BidibPort.prepareSystemFunctionBidibPort(PortModelEnum.type, (byte) 1, (byte) 0);
        Assert.assertEquals(bidibPort.getLowValue(), (byte) 0);
        Assert.assertEquals(bidibPort.getHighValue(), (byte) 1);

        Assert.assertEquals(bidibPort.getPortNumber(PortModelEnum.type), (byte) 1);
    }

    @Test
    public void BidibPortSystemFunctionFlatModel() {
        BidibPort bidibPort = BidibPort.prepareSystemFunctionBidibPort(PortModelEnum.flat, (byte) 1, (byte) 0);
        Assert.assertEquals(bidibPort.getLowValue(), (byte) 1);
        Assert.assertEquals(bidibPort.getHighValue(), (byte) 0);

        Assert.assertEquals(bidibPort.getPortNumber(PortModelEnum.flat), (byte) 1);
    }
}
