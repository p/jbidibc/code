package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.M4OpCodesAck;
import org.bidib.jbidibc.core.enumeration.OccupationState;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusDecoderType;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.jbidibc.core.exception.NodeAlreadyRegisteredException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.AccessoryNotifyResponse;
import org.bidib.jbidibc.core.message.AccessoryStateResponse;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.BoostDiagnosticResponse;
import org.bidib.jbidibc.core.message.BoostStatResponse;
import org.bidib.jbidibc.core.message.CommandStationAccessoryAcknowledgeResponse;
import org.bidib.jbidibc.core.message.CommandStationAccessoryManualResponse;
import org.bidib.jbidibc.core.message.CommandStationDriveManualResponse;
import org.bidib.jbidibc.core.message.CommandStationDriveStateResponse;
import org.bidib.jbidibc.core.message.CommandStationM4AcknowledgeResponse;
import org.bidib.jbidibc.core.message.CommandStationProgStateResponse;
import org.bidib.jbidibc.core.message.CommandStationRcPlusAcknowledgeResponse;
import org.bidib.jbidibc.core.message.CommandStationStateResponse;
import org.bidib.jbidibc.core.message.FeedbackAccessoryResponse;
import org.bidib.jbidibc.core.message.FeedbackAddressResponse;
import org.bidib.jbidibc.core.message.FeedbackConfidenceResponse;
import org.bidib.jbidibc.core.message.FeedbackCvResponse;
import org.bidib.jbidibc.core.message.FeedbackDynStateResponse;
import org.bidib.jbidibc.core.message.FeedbackFreeResponse;
import org.bidib.jbidibc.core.message.FeedbackMultipleResponse;
import org.bidib.jbidibc.core.message.FeedbackOccupiedResponse;
import org.bidib.jbidibc.core.message.FeedbackPositionResponse;
import org.bidib.jbidibc.core.message.FeedbackRcPlusResponse;
import org.bidib.jbidibc.core.message.FeedbackSpeedResponse;
import org.bidib.jbidibc.core.message.FeedbackXPomResponse;
import org.bidib.jbidibc.core.message.LcConfigResponse;
import org.bidib.jbidibc.core.message.LcConfigXResponse;
import org.bidib.jbidibc.core.message.LcKeyResponse;
import org.bidib.jbidibc.core.message.LcNotAvailableResponse;
import org.bidib.jbidibc.core.message.LcStatResponse;
import org.bidib.jbidibc.core.message.LcWaitResponse;
import org.bidib.jbidibc.core.message.NodeLostResponse;
import org.bidib.jbidibc.core.message.NodeNewResponse;
import org.bidib.jbidibc.core.message.ResponseFactory;
import org.bidib.jbidibc.core.message.StallResponse;
import org.bidib.jbidibc.core.message.SysErrorResponse;
import org.bidib.jbidibc.core.message.SysIdentifyResponse;
import org.bidib.jbidibc.core.message.SysMagicResponse;
import org.bidib.jbidibc.core.message.SysPongResponse;
import org.bidib.jbidibc.core.node.AccessoryNode;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The message receiver is responsible for creating the messages based on the received bytes from the stream. It is
 * created and initialized by the (default) Bidib implementation.
 */
public abstract class MessageReceiver implements BidibMessageProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiver.class);

    protected static final Logger MSG_RX_LOGGER = LoggerFactory.getLogger("RX");

    protected static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("RAW");

    private final Set<MessageListener> messageListeners =
        Collections.synchronizedSet(new LinkedHashSet<MessageListener>());

    private final Collection<NodeListener> nodeListeners = Collections.synchronizedList(new LinkedList<NodeListener>());

    protected AtomicBoolean running = new AtomicBoolean();

    private AtomicBoolean ignoreWrongMessageNumber = new AtomicBoolean();

    private NodeRegistry nodeRegistry;

    private boolean checkCRC;

    private final ResponseFactory responseFactory;

    /**
     * Create a new instance of MessageReceiver.
     * 
     * @param nodeRegistry
     *            the node registry
     * @param responseFactory
     *            the response factory
     * @param checkCRC
     *            flag if we must check the CRC
     */
    public MessageReceiver(final NodeRegistry nodeRegistry, final ResponseFactory responseFactory, boolean checkCRC) {
        this.nodeRegistry = nodeRegistry;
        this.nodeRegistry.setMessageReceiver(this);
        this.responseFactory = responseFactory;
        this.checkCRC = checkCRC;

        // enable the running flag
        running.set(true);
    }

    public void setIgnoreWrongMessageNumber(boolean ignoreWrongMessageNumber) {
        LOGGER.info("Set the ignoreWrongMessageNumber flag: {}", ignoreWrongMessageNumber);
        this.ignoreWrongMessageNumber.set(ignoreWrongMessageNumber);
    }

    @Override
    public String getErrorInformation() {
        return null;
    }

    @Override
    public abstract void receive(final ByteArrayOutputStream data);

    @Override
    public void processMessages(final ByteArrayOutputStream messageData) throws ProtocolException {

        if (messageData.size() < 1) {
            LOGGER.info("No data in provided buffer, skip process messages.");
            return;
        }

        // if a CRC error is detected in splitMessages the reading loop will terminate ...
        Collection<byte[]> messages = MessageUtils.splitBidibMessages(messageData, checkCRC);

        if (messages == null) {
            LOGGER.warn("No messages to process available.");
            return;
        }

        LOGGER.debug("Number of splited messages: {}", messages.size());

        BidibNode bidibNode = null;

        for (byte[] messageArray : messages) {
            BidibMessage message = null;

            try {
                message = responseFactory.create(messageArray);
                if (MSG_RX_LOGGER.isInfoEnabled()) {

                    StringBuilder sb = new StringBuilder("<< ");
                    sb.append(message);
                    sb.append(" : ");
                    sb.append(ByteUtils.bytesToHex(messageArray));

                    MSG_RX_LOGGER.info(sb.toString());
                }

                bidibNode = nodeRegistry.findNode(message.getAddr());

                evaluateMessage(bidibNode, message);
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Process received messages failed: {}", ByteUtils.bytesToHex(messageArray), ex);

                StringBuilder sb = new StringBuilder("<< received invalid: ");
                sb.append(message);
                sb.append(" : ");
                sb.append(ByteUtils.bytesToHex(messageArray));

                MSG_RX_LOGGER.warn(sb.toString());

                throw ex;
            }
            catch (Exception ex) {
                LOGGER.warn("Process received messages failed: {}", ByteUtils.bytesToHex(messageArray), ex);
            }
            finally {
                if (message != null) {
                    // the message numbers are evaluated only after the magic message on the node was received ...

                    // verify that the receive message number is valid
                    // use findNode to not create a node if none is registered in nodeFactory
                    if (bidibNode != null
                        && (bidibNode.getNodeMagic() != null || message instanceof SysMagicResponse)) {
                        int numExpected = bidibNode.getNextReceiveMsgNum(message);
                        int numReceived = message.getNum();
                        LOGGER
                            .trace("Compare the message numbers, expected: {}, received: {}", numExpected, numReceived);
                        if (numReceived != numExpected) {

                            LOGGER
                                .warn("Received wrong message number for message: {}, expected: {}, node: {}",
                                    new Object[] { message, numExpected, bidibNode });

                            if (!ignoreWrongMessageNumber.get() && bidibNode.getNodeMagic() != null) {
                                throw new ProtocolException(
                                    "wrong message number: expected " + numExpected + " but got " + numReceived);
                            }
                            else {
                                LOGGER
                                    .info(
                                        "Wrong receive message number is ignored due to preferences. The next expected number is adjusted to the received number: {}",
                                        numReceived);

                                bidibNode.adjustReceiveMsgNum(numReceived);
                            }
                        }
                    }
                    else {
                        int numReceived = message.getNum();
                        LOGGER
                            .warn(
                                "Ignore compare message number because the magic is not set on the node. Current received message number: {}, message: {}",
                                numReceived, message);
                    }
                }
            }
        }

        LOGGER.debug("Process messages has finished.");
    }

    /**
     * Evaluate the message type and call the distribution functions.
     * 
     * @param message
     *            the message to evaluate
     * @throws ProtocolException
     */
    protected void evaluateMessage(final BidibNode bidibNode, final BidibMessage message) throws ProtocolException {

        // some messages are notified directly to listeners
        int type = ByteUtils.getInt(message.getType());
        switch (type) {
            case BidibLibrary.MSG_BOOST_DIAGNOSTIC:
                BoostDiagnosticResponse diag = (BoostDiagnosticResponse) message;
                fireBoosterDiagnostic(message.getAddr(), diag.getCurrent(), diag.getVoltage(), diag.getTemperature());
                break;
            case BidibLibrary.MSG_BOOST_STAT:
                fireBoosterState(message.getAddr(), ((BoostStatResponse) message).getState());
                break;
            case BidibLibrary.MSG_BM_ADDRESS:
                fireAddress(message.getAddr(), ((FeedbackAddressResponse) message).getDetectorNumber(),
                    ((FeedbackAddressResponse) message).getAddresses());
                break;
            case BidibLibrary.MSG_BM_CONFIDENCE:
                fireConfidence(message.getAddr(), ((FeedbackConfidenceResponse) message).getInvalid(),
                    ((FeedbackConfidenceResponse) message).getFreeze(),
                    ((FeedbackConfidenceResponse) message).getNoSignal());
                break;
            case BidibLibrary.MSG_BM_FREE:
                // acknowledge message

                // let the node factory trigger the acknowledge
                // bidibNode = nodeRegistry.findNode(message.getAddr());

                if (bidibNode != null && bidibNode.isSecureAckEnabled()) {
                    bidibNode.acknowledgeFree(((FeedbackFreeResponse) message).getDetectorNumber());
                    nodeRegistry.triggerPendingAcknowledge(bidibNode);
                }

                fireFree(message.getAddr(), ((FeedbackFreeResponse) message).getDetectorNumber(),
                    ((FeedbackFreeResponse) message).getTimestamp());
                break;
            case BidibLibrary.MSG_BM_MULTIPLE:
                int baseAddress = ((FeedbackMultipleResponse) message).getBaseAddress();
                int size = ((FeedbackMultipleResponse) message).getSize();
                byte[] detectorData = ((FeedbackMultipleResponse) message).getDetectorData();

                LOGGER
                    .debug("Received FeedbackMultipleResponse, baseAddress: {}, size: {}, detectorData: {}",
                        baseAddress, size, detectorData);

                // let the node registry trigger the acknowledge
                if (bidibNode != null && bidibNode.isSecureAckEnabled()) {
                    bidibNode.acknowledgeMultiple(baseAddress, size, detectorData);
                    nodeRegistry.triggerPendingAcknowledge(bidibNode);
                }

                int offset = 0;
                for (byte detectorByte : detectorData) {
                    if (offset >= size) {
                        break;
                    }
                    for (int bit = 0; bit <= 7; bit++) {
                        if (((detectorByte >> bit) & 1) == 1) {
                            fireOccupied(message.getAddr(), baseAddress + offset, null);
                        }
                        else {
                            fireFree(message.getAddr(), baseAddress + offset, null);
                        }
                        offset++;
                    }
                }
                break;
            case BidibLibrary.MSG_BM_OCC:
                FeedbackOccupiedResponse feedbackOccupiedResponse = (FeedbackOccupiedResponse) message;
                // let the node registry trigger the acknowledge
                if (bidibNode != null && bidibNode.isSecureAckEnabled()) {
                    bidibNode.acknowledgeOccupied(feedbackOccupiedResponse.getDetectorNumber());
                    nodeRegistry.triggerPendingAcknowledge(bidibNode);
                }

                fireOccupied(message.getAddr(), feedbackOccupiedResponse.getDetectorNumber(),
                    feedbackOccupiedResponse.getTimestamp());
                break;
            case BidibLibrary.MSG_BM_POSITION:
                FeedbackPositionResponse feedbackPositionResponse = (FeedbackPositionResponse) message;

                // let the node registry trigger the acknowledge
                if (bidibNode != null && bidibNode.isSecureAckEnabled()) {
                    bidibNode
                        .acknowledgePosition(feedbackPositionResponse.getDecoderAddress(),
                            feedbackPositionResponse.getLocationType(), feedbackPositionResponse.getLocationAddress());
                    nodeRegistry.triggerPendingAcknowledge(bidibNode);
                }

                firePosition(feedbackPositionResponse.getAddr(), feedbackPositionResponse.getDecoderAddress(),
                    feedbackPositionResponse.getLocationType(), feedbackPositionResponse.getLocationAddress());

                break;
            case BidibLibrary.MSG_BM_SPEED:
                fireSpeed(message.getAddr(), ((FeedbackSpeedResponse) message).getAddress(),
                    ((FeedbackSpeedResponse) message).getSpeed());
                break;
            case BidibLibrary.MSG_BM_DYN_STATE:
                LOGGER.trace("Received MSG_BM_DYN_STATE: {}", message);
                fireDynState(message.getAddr(), (FeedbackDynStateResponse) message);
                break;
            case BidibLibrary.MSG_ACCESSORY_STATE:
                // process the AccessoryStateResponse message
                AccessoryStateResponse accessoryStateResponse = (AccessoryStateResponse) message;

                fireAccessoryState(message.getAddr(), accessoryStateResponse.getAccessoryState(),
                    accessoryStateResponse.getAccessoryStateOptions());
                break;
            case BidibLibrary.MSG_ACCESSORY_NOTIFY:
                // process the AccessoryNotifyResponse message
                AccessoryNode accessoryNode = nodeRegistry.getAccessoryNode(message.getAddr());

                if (accessoryNode != null) {

                    AccessoryNotifyResponse accessoryNotifyResponse = (AccessoryNotifyResponse) message;

                    // let the node registry trigger the acknowledge
                    accessoryNode.acknowledgeAccessoryNotify(accessoryNotifyResponse.getAccessoryState());
                    nodeRegistry.triggerPendingAcknowledge(bidibNode);
                }
                else {
                    LOGGER
                        .warn("No accessory node available for node with address: {}",
                            ByteUtils.bytesToHex(message.getAddr()));
                }

                AccessoryNotifyResponse accessoryNotifyResponse = (AccessoryNotifyResponse) message;

                fireAccessoryState(message.getAddr(), accessoryNotifyResponse.getAccessoryState(),
                    accessoryNotifyResponse.getAccessoryStateOptions());
                break;
            case BidibLibrary.MSG_LC_KEY:
                fireKey(message.getAddr(), ((LcKeyResponse) message).getKeyNumber(),
                    ((LcKeyResponse) message).getKeyState());
                break;
            case BidibLibrary.MSG_LC_STAT:
                LOGGER.debug("Received LcStatResponse: {}", message);
                LcStatResponse lcStatResponse = (LcStatResponse) message;
                fireLcStat(lcStatResponse);
                break;
            case BidibLibrary.MSG_LC_WAIT:
                LOGGER.info("Received LcWaitResponse: {}", message);
                LcWaitResponse lcWaitResponse = (LcWaitResponse) message;
                fireLcWait(lcWaitResponse);
                break;

            case BidibLibrary.MSG_LC_CONFIG:
                LOGGER.trace("Received LcConfigResponse: {}", message);
                LcConfigResponse lcConfigResponse = (LcConfigResponse) message;
                fireLcConfig(lcConfigResponse);
                break;

            case BidibLibrary.MSG_LC_CONFIGX:
                LOGGER.trace("Received LcConfigXResponse: {}", message);
                LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) message;
                fireLcConfigX(lcConfigXResponse);
                break;

            case BidibLibrary.MSG_LC_NA:
                LOGGER.info("Received LcNotAvailableResponse: {}", message);
                LcNotAvailableResponse lcNotAvailableResponse = (LcNotAvailableResponse) message;
                try {
                    // handle the LC_NA
                    if (bidibNode != null) {
                        // async delivery
                        fireLcNa(lcNotAvailableResponse);
                    }
                    else {
                        messageReceived(bidibNode, message);
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Signal MSG_LC_NA failed.", ex);
                }
                break;
            case BidibLibrary.MSG_LOGON:
                // ignored
                break;

            case BidibLibrary.MSG_STALL:
                LOGGER.warn("The node has sent the MSG_STALL: {}", bidibNode);

                fireStall((StallResponse) message);
                break;
            case BidibLibrary.MSG_NODE_NEW:
                Node node = ((NodeNewResponse) message).getNode(message.getAddr());

                LOGGER.info("Send node changed acknowledge for nodetab version: {}", node.getVersion());

                boolean fireNodeNew = false;
                try {
                    // create and register the new node in the node factory because we might receive
                    // spontaneous messages
                    BidibNode newNode = nodeRegistry.createNode(node);
                    LOGGER.info("The node factory has registered the new node: {}", newNode);
                    fireNodeNew = true;
                }
                catch (NodeAlreadyRegisteredException ex) {
                    LOGGER
                        .warn(
                            "The new node is already registered in the nodes list. Signal new node to application is skipped.",
                            ex);
                }
                finally {
                    // let the node registry trigger the acknowledge
                    if (bidibNode != null) {
                        bidibNode.acknowledgeNodeChanged(node.getVersion());
                        nodeRegistry.triggerPendingAcknowledge(bidibNode);
                    }
                    else {
                        LOGGER.warn("new node was not registered in node factory, addr: {}", message.getAddr());
                    }
                }
                if (fireNodeNew) {
                    LOGGER.info("Signal new node in system to the application.");
                    fireNodeNew(message.getAddr(), node);
                }
                break;
            case BidibLibrary.MSG_NODE_LOST:
                NodeLostResponse nodeLostResponse = (NodeLostResponse) message;

                // the lost node is coded in the message
                Node lostNode = nodeLostResponse.getNode(message.getAddr());
                fireNodeLost(message.getAddr(), lostNode);

                // acknowledge the new nodetab version to the interface
                // let the node registry trigger the acknowledge
                if (bidibNode != null) {
                    LOGGER.info("Acknowledge node lost on node: {}", bidibNode);
                    bidibNode.acknowledgeNodeChanged(nodeLostResponse.getNodeTabVersion());
                    nodeRegistry.triggerPendingAcknowledge(bidibNode);
                }
                else {
                    LOGGER.warn("Lost node was not registered in node factory, addr: {}", message.getAddr());
                }
                break;
            case BidibLibrary.MSG_SYS_ERROR:
                SysErrorResponse errorResponse = (SysErrorResponse) message;
                switch (errorResponse.getErrorCode()) {
                    case BidibLibrary.BIDIB_ERR_IDDOUBLE:
                        LOGGER
                            .warn("A node attempted to register with an already registered ID: {}",
                                errorResponse.getAddr());

                        // forward to application!
                        fireError(message.getAddr(), errorResponse.getErrorCode(), errorResponse.getReasonData());
                        break;
                    default:
                        fireError(message.getAddr(), errorResponse.getErrorCode(), errorResponse.getReasonData());
                        break;
                }
                break;
            case BidibLibrary.MSG_SYS_IDENTIFY_STATE:
                fireIdentify(message.getAddr(), ((SysIdentifyResponse) message).getState());
                break;
            case BidibLibrary.MSG_SYS_PONG:
                firePong(message.getAddr(), ((SysPongResponse) message).getMarker());
                break;
            case BidibLibrary.MSG_BM_CURRENT:
                LOGGER.warn("MSG_BM_CURRENT is currently not processed by application: {}", message);
                break;
            case BidibLibrary.MSG_BM_ACCESSORY:
                fireBmAccessory(message.getAddr(), ((FeedbackAccessoryResponse) message).getDetectorNumber(),
                    ((FeedbackAccessoryResponse) message).getAddress());
                break;
            case BidibLibrary.MSG_BM_CV:
                fireBmCv((FeedbackCvResponse) message);
                break;
            case BidibLibrary.MSG_BM_XPOM:
                fireBmXPom((FeedbackXPomResponse) message);
                break;
            case BidibLibrary.MSG_BM_RCPLUS:
                FeedbackRcPlusResponse feedbackRcPlusResponse = (FeedbackRcPlusResponse) message;
                fireBmRcPlus(feedbackRcPlusResponse);
                break;
            case BidibLibrary.MSG_CS_PROG_STATE:
                CommandStationProgStateResponse commandStationProgStateResponse =
                    (CommandStationProgStateResponse) message;
                fireCsProgState(message.getAddr(), commandStationProgStateResponse.getState(),
                    commandStationProgStateResponse.getRemainingTime(), commandStationProgStateResponse.getCvNumber(),
                    commandStationProgStateResponse.getCvData());
                break;
            case BidibLibrary.MSG_CS_STATE:
                CommandStationStateResponse commandStationStateResponse = (CommandStationStateResponse) message;
                fireCsState(message.getAddr(), commandStationStateResponse.getState());
                break;
            case BidibLibrary.MSG_CS_DRIVE_MANUAL:
                CommandStationDriveManualResponse commandStationDriveManualResponse =
                    (CommandStationDriveManualResponse) message;
                fireCsDriveManual(commandStationDriveManualResponse);
                break;
            case BidibLibrary.MSG_CS_DRIVE_STATE:
                CommandStationDriveStateResponse commandStationDriveStateResponse =
                    (CommandStationDriveStateResponse) message;
                fireCsDriveState(commandStationDriveStateResponse);
                break;
            case BidibLibrary.MSG_CS_ACCESSORY_ACK:
                CommandStationAccessoryAcknowledgeResponse commandStationAccessoryAcknowledgeResponse =
                    (CommandStationAccessoryAcknowledgeResponse) message;
                fireCsAccessoryAck(commandStationAccessoryAcknowledgeResponse);
                break;
            case BidibLibrary.MSG_CS_ACCESSORY_MANUAL:
                CommandStationAccessoryManualResponse commandStationAccessoryManualResponse =
                    (CommandStationAccessoryManualResponse) message;
                fireCsAccessoryManual(commandStationAccessoryManualResponse);
                break;
            case BidibLibrary.MSG_CS_RCPLUS_ACK:
                CommandStationRcPlusAcknowledgeResponse commandStationRcPlusAcknowledgeResponse =
                    (CommandStationRcPlusAcknowledgeResponse) message;
                fireCsRcPlusAcknowledge(commandStationRcPlusAcknowledgeResponse);
                break;
            case BidibLibrary.MSG_CS_M4_ACK:
                CommandStationM4AcknowledgeResponse commandStationM4AcknowledgeResponse =
                    (CommandStationM4AcknowledgeResponse) message;
                fireCsM4Acknowledge(commandStationM4AcknowledgeResponse);
                break;
            default:
                messageReceived(bidibNode, message);
                break;
        }

    }

    protected void messageReceived(final BidibNode bidibNode, final BidibMessage message) {
        // put the message into the receiveQueue because somebody waits for it ...
        LOGGER.debug("Offer received message to node: {}, message: {}", bidibNode, message);

        try {
            bidibNode.addResponseToReceiveQueue(message);
        }
        catch (Exception ex) {
            LOGGER.error("Offer received message to node failed. Message was: " + message, ex);
        }
    }

    @Override
    public void addMessageListener(MessageListener messageListener) {
        synchronized (messageListeners) {
            messageListeners.add(messageListener);
        }
    }

    @Override
    public void addNodeListener(NodeListener nodeListener) {
        synchronized (nodeListeners) {
            nodeListeners.add(nodeListener);
        }
    }

    @Override
    public void removeNodeListener(NodeListener nodeListener) {
        synchronized (nodeListeners) {
            nodeListeners.remove(nodeListener);
        }
    }

    @Override
    public void removeMessageListener(MessageListener messageListener) {
        synchronized (messageListeners) {
            messageListeners.remove(messageListener);
        }
    }

    public void clearMessageListeners() {
        synchronized (messageListeners) {
            messageListeners.clear();
        }
    }

    public void clearNodeListeners() {
        synchronized (nodeListeners) {
            nodeListeners.clear();
        }
    }

    public void disable() {
        LOGGER.debug("disable is called.");
        running.set(false);
    }

    public void enable() {
        LOGGER.debug("enable is called.");
        running.set(true);
    }

    public boolean isEnabled() {
        return running.get();
    }

    protected void fireAddress(byte[] address, int detectorNumber, List<AddressData> addresses) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.address(address, detectorNumber, addresses);
            }
        }
    }

    protected void fireBmAccessory(byte[] address, int detectorNumber, int accessoryAddress) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackAccessory(address, detectorNumber, accessoryAddress);
            }
        }
    }

    protected void fireBmCv(final FeedbackCvResponse feedbackCvResponse) {
        byte[] address = feedbackCvResponse.getAddr();
        int cvNumber = feedbackCvResponse.getCvNumber();
        int dat = feedbackCvResponse.getDat();

        AddressData decoderAddress = feedbackCvResponse.getAddress();
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackCv(address, decoderAddress, cvNumber, dat);
            }
        }
    }

    protected void fireBmXPom(FeedbackXPomResponse response) {
        byte[] address = response.getAddr();
        int cvNumber = response.getCvNumberX();
        int[] cvValue = response.getCvXValue();

        if (response.isDiDAddress()) {
            DecoderIdAddressData did = response.getDID();
            synchronized (messageListeners) {
                for (MessageListener l : messageListeners) {
                    l.feedbackXPom(address, did, cvNumber, cvValue);
                }
            }
        }
        else {
            AddressData decoderAddress = response.getDecoderAddress();
            synchronized (messageListeners) {
                for (MessageListener l : messageListeners) {
                    l.feedbackXPom(address, decoderAddress, cvNumber, cvValue);
                }
            }
        }
    }

    protected void fireBmRcPlus(FeedbackRcPlusResponse feedbackRcPlusResponse) {
        LOGGER.info("fireBmRcPlus: {}", feedbackRcPlusResponse);

        // get the opCode and prepare the message
        switch (feedbackRcPlusResponse.getOpCode()) {
            case RC_BIND_ACCEPTED_LOCO:
                LOGGER.info("Prepare message for RC_BIND_ACCEPTED_LOCO.");
                RcPlusFeedbackBindData rcPlusBindAccepted = feedbackRcPlusResponse.getRcPlusFeedbackBind();
                publishRcPlusBindAccepted(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusDecoderType.LOCO, rcPlusBindAccepted);
                break;
            case RC_BIND_ACCEPTED_ACCESSORY:
                LOGGER.info("Prepare message for RC_BIND_ACCEPTED_ACCESSORY.");
                rcPlusBindAccepted = feedbackRcPlusResponse.getRcPlusFeedbackBind();
                publishRcPlusBindAccepted(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusDecoderType.ACCESSORY, rcPlusBindAccepted);
                break;
            case RC_PING_COLLISION_P0:
                LOGGER.info("Prepare message for RC_PING_COLLISION_P0.");
                publishRcPlusPingCollision(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P0);
                break;
            case RC_PING_COLLISION_P1:
                LOGGER.info("Prepare message for RC_PING_COLLISION_P1.");
                publishRcPlusPingCollision(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P1);
                break;
            case RC_FIND_COLLISION_P0:
                LOGGER.info("Prepare message for RC_FIND_COLLISION_P0.");
                DecoderUniqueIdData uniqueId = feedbackRcPlusResponse.getRcPlusUniqueId();
                publishRcPlusFindCollision(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P0, uniqueId);
                break;
            case RC_FIND_COLLISION_P1:
                LOGGER.info("Prepare message for RC_FIND_COLLISION_P1.");
                uniqueId = feedbackRcPlusResponse.getRcPlusUniqueId();
                publishRcPlusFindCollision(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P1, uniqueId);
                break;
            case RC_PONG_OKAY_LOCO_P0:
                LOGGER.info("Prepare message for RC_PONG_OKAY_LOCO_P0.");
                uniqueId = feedbackRcPlusResponse.getRcPlusUniqueId();
                publishRcPlusPongOkay(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P0, RcPlusDecoderType.LOCO, uniqueId);
                break;
            case RC_PONG_OKAY_LOCO_P1:
                LOGGER.info("Prepare message for RC_PONG_OKAY_LOCO_P1.");
                uniqueId = feedbackRcPlusResponse.getRcPlusUniqueId();
                publishRcPlusPongOkay(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P1, RcPlusDecoderType.LOCO, uniqueId);
                break;
            case RC_PONG_NEW_LOCO_P0:
                LOGGER.info("Prepare message for RC_PONG_NEW_LOCO_P0.");
                uniqueId = feedbackRcPlusResponse.getRcPlusUniqueId();
                publishRcPlusPongNew(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P0, RcPlusDecoderType.LOCO, uniqueId);
                break;
            case RC_PONG_NEW_LOCO_P1:
                LOGGER.info("Prepare message for RC_PONG_NEW_LOCO_P1.");
                uniqueId = feedbackRcPlusResponse.getRcPlusUniqueId();
                publishRcPlusPongNew(feedbackRcPlusResponse.getAddr(), feedbackRcPlusResponse.getDetectorNumber(),
                    RcPlusPhase.P1, RcPlusDecoderType.LOCO, uniqueId);
                break;
            default:
                break;
        }
    }

    private void publishRcPlusBindAccepted(
        byte[] address, int detectorNum, RcPlusDecoderType decoderType, RcPlusFeedbackBindData rcPlusBindAccepted) {
        LOGGER
            .info("Publish RcPlusBindAccepted, decoderType: {}, rcPlusBindAccepted: {}, detectorNum: {}", decoderType,
                rcPlusBindAccepted, detectorNum);

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackRcPlusBindAccepted(address, detectorNum, decoderType, rcPlusBindAccepted);
            }
        }
    }

    private void publishRcPlusPingCollision(byte[] address, int detectorNum, RcPlusPhase phase) {
        LOGGER.info("Publish RcPlusPingCollision, phase: {}, detectorNum: {}", phase, detectorNum);

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackRcPlusPingCollision(address, detectorNum, phase);
            }
        }
    }

    private void publishRcPlusFindCollision(
        byte[] address, int detectorNum, RcPlusPhase phase, DecoderUniqueIdData uniqueId) {
        LOGGER
            .info("Publish RcPlusFindCollision, phase: {}, uniqueId: {}, detectorNum: {}", phase, uniqueId,
                detectorNum);

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackRcPlusFindCollision(address, detectorNum, phase, uniqueId);
            }
        }
    }

    private void publishRcPlusPongOkay(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId) {
        LOGGER
            .info("Publish RcPlusPongOkay, phase: {}, decoderType: {}, uniqueId: {}, detectorNum: {}", phase,
                decoderType, uniqueId, detectorNum);

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackRcPlusPongOkay(address, detectorNum, phase, decoderType, uniqueId);
            }
        }
    }

    private void publishRcPlusPongNew(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId) {
        LOGGER
            .info("Publish RcPlusPongNew, phase: {}, decoderType: {}, uniqueId: {}, detectorNum: {}", phase,
                decoderType, uniqueId, detectorNum);

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.feedbackRcPlusPongNew(address, detectorNum, phase, decoderType, uniqueId);
            }
        }
    }

    protected void fireCsProgState(
        byte[] address, CommandStationProgState commandStationProgState, int remainingTime, int cvNumber, int cvData) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.csProgState(address, commandStationProgState, remainingTime, cvNumber, cvData);
            }
        }
    }

    protected void fireCsState(byte[] address, CommandStationState commandStationState) {
        LOGGER
            .info("The state of the command station has changed: {}, address: {}", commandStationState,
                ByteUtils.bytesToHex(address));
        synchronized (messageListeners) {

            Set<MessageListener> safeListeners = Collections.unmodifiableSet(messageListeners);
            for (MessageListener l : safeListeners) {
                l.csState(address, commandStationState);
            }
        }
    }

    protected void fireCsDriveManual(CommandStationDriveManualResponse commandStationDriveManualResponse) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l
                    .csDriveManual(commandStationDriveManualResponse.getAddr(),
                        commandStationDriveManualResponse.getDriveState());
            }
        }
    }

    protected void fireCsDriveState(final CommandStationDriveStateResponse commandStationDriveStateResponse) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l
                    .csDriveState(commandStationDriveStateResponse.getAddr(),
                        commandStationDriveStateResponse.getDriveState());
            }
        }
    }

    protected void fireCsAccessoryManual(CommandStationAccessoryManualResponse commandStationAccessoryManualResponse) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l
                    .csAccessoryManual(commandStationAccessoryManualResponse.getAddr(),
                        commandStationAccessoryManualResponse.getDecoderAddress(),
                        commandStationAccessoryManualResponse.getActivate(),
                        commandStationAccessoryManualResponse.getAspect());
            }
        }
    }

    protected void fireCsAccessoryAck(
        CommandStationAccessoryAcknowledgeResponse commandStationAccessoryAcknowledgeResponse) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l
                    .csAccessoryAcknowledge(commandStationAccessoryAcknowledgeResponse.getAddr(),
                        commandStationAccessoryAcknowledgeResponse.getAddress(),
                        commandStationAccessoryAcknowledgeResponse.getAcknState());
            }
        }
    }

    protected void fireCsRcPlusAcknowledge(
        CommandStationRcPlusAcknowledgeResponse commandStationRcPlusAcknowledgeResponse) {
        LOGGER.info("fireCsRcPlusAcknowledge: {}", commandStationRcPlusAcknowledgeResponse);

        // get the opCode and prepare the message
        switch (commandStationRcPlusAcknowledgeResponse.getOpCode()) {
            case RC_TID:
                TidData tidData = commandStationRcPlusAcknowledgeResponse.getTid();
                publishTidData(commandStationRcPlusAcknowledgeResponse.getAddr(), tidData);
                break;
            case RC_PING_ONCE_P0:
                RcPlusAcknowledge acknState = commandStationRcPlusAcknowledgeResponse.getAcknState();
                publishPingAcknState(commandStationRcPlusAcknowledgeResponse.getAddr(), RcPlusPhase.P0, acknState);
                break;
            case RC_PING_ONCE_P1:
                acknState = commandStationRcPlusAcknowledgeResponse.getAcknState();
                publishPingAcknState(commandStationRcPlusAcknowledgeResponse.getAddr(), RcPlusPhase.P1, acknState);
                break;
            case RC_BIND:
                RcPlusDecoderAnswerData decoderAnswer =
                    commandStationRcPlusAcknowledgeResponse.getRcPlusDecoderAnswer();
                publishBindAnswer(commandStationRcPlusAcknowledgeResponse.getAddr(), decoderAnswer);
                break;
            case RC_FIND_P0:
                decoderAnswer = commandStationRcPlusAcknowledgeResponse.getRcPlusDecoderAnswer();
                publishFindAnswer(commandStationRcPlusAcknowledgeResponse.getAddr(), RcPlusPhase.P0, decoderAnswer);
                break;
            case RC_FIND_P1:
                decoderAnswer = commandStationRcPlusAcknowledgeResponse.getRcPlusDecoderAnswer();
                publishFindAnswer(commandStationRcPlusAcknowledgeResponse.getAddr(), RcPlusPhase.P1, decoderAnswer);
                break;
            default:
                break;
        }
    }

    protected void fireCsM4Acknowledge(CommandStationM4AcknowledgeResponse commandStationM4AcknowledgeResponse) {
        LOGGER.info("fireCsM4Acknowledge: {}", commandStationM4AcknowledgeResponse);

        // TODO
        // get the opCode and prepare the message
        switch (commandStationM4AcknowledgeResponse.getOpCode()) {
            case M4_TID:
                TidData tidData = commandStationM4AcknowledgeResponse.getTid();
                publishTidData(commandStationM4AcknowledgeResponse.getAddr(), tidData);
                break;
            case M4_BEACON:
                int beaconInterval = commandStationM4AcknowledgeResponse.getInterval(M4OpCodesAck.M4_BEACON);
                LOGGER.info("Received the beaconInterval: {}", beaconInterval);
                break;
            case M4_SEARCH:
                int searchInterval = commandStationM4AcknowledgeResponse.getInterval(M4OpCodesAck.M4_SEARCH);
                LOGGER.info("Received the searchInterval: {}", searchInterval);
                break;
            case M4_NEW_LOCO:
                LOGGER.info("Received M4_NEW_LOCO");
                Integer decoderId = commandStationM4AcknowledgeResponse.getDid(M4OpCodesAck.M4_NEW_LOCO);
                LOGGER.info("Received the decoderId: {}", decoderId);
                break;
            default:
                LOGGER.warn("Unhandled opCode: {}", commandStationM4AcknowledgeResponse.getOpCode());
                break;
        }
    }

    private void publishTidData(byte[] address, TidData tidData) {

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.csRcPlusTid(address, tidData);
            }
        }
    }

    private void publishPingAcknState(byte[] address, RcPlusPhase phase, RcPlusAcknowledge acknState) {

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.csRcPlusPingAcknState(address, phase, acknState);
            }
        }
    }

    private void publishBindAnswer(byte[] address, RcPlusDecoderAnswerData decoderAnswer) {

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.csRcPlusBindAnswer(address, decoderAnswer);
            }
        }
    }

    private void publishFindAnswer(byte[] address, RcPlusPhase phase, RcPlusDecoderAnswerData decoderAnswer) {

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.csRcPlusFindAnswer(address, phase, decoderAnswer);
            }
        }
    }

    protected void fireBoosterDiagnostic(byte[] address, int current, int voltage, int temperature) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.boosterDiag(address, current, voltage, temperature);
            }
        }
    }

    protected void fireBoosterState(byte[] address, BoosterState state) {
        synchronized (messageListeners) {

            Set<MessageListener> safeListeners = Collections.unmodifiableSet(messageListeners);
            for (MessageListener l : safeListeners) {
                l.boosterState(address, state);
            }
        }
    }

    protected void fireConfidence(byte[] address, int valid, int freeze, int signal) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.confidence(address, valid, freeze, signal);
            }
        }
    }

    private void fireFree(byte[] address, int detectorNumber, Long timestamp) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.occupation(address, detectorNumber, OccupationState.FREE, timestamp);
            }
        }
    }

    private void fireOccupied(byte[] address, int detectorNumber, Long timestamp) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.occupation(address, detectorNumber, OccupationState.OCCUPIED, timestamp);
            }
        }
    }

    private void firePosition(byte[] address, int decoderAddress, int locationType, int locationAddress) {

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.position(address, decoderAddress, locationType, locationAddress);
            }
        }

    }

    protected void fireSpeed(byte[] address, AddressData addressData, int speed) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.speed(address, addressData, speed);
            }
        }
    }

    private void fireIdentify(byte[] address, IdentifyState identifyState) {
        LOGGER.debug("Identify, addr: {}, identifyState: {}", address, identifyState);
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.identify(address, identifyState);
            }
        }
    }

    private void firePong(byte[] address, int marker) {
        LOGGER.debug("Pong, addr: {}, marker: {}", address, marker);
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.pong(address, marker);
            }
        }
    }

    private void fireKey(byte[] address, int keyNumber, int keyState) {
        LOGGER.info("LcKey is signalled, keyNumber: {}, keyState: {}", keyNumber, keyState);
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.key(address, keyNumber, keyState);
            }
        }

        // notify the bulk sender
        notifyBulkSender(address, LcKeyResponse.TYPE);
    }

    private void fireLcStat(LcStatResponse lcStatResponse) {
        byte[] address = lcStatResponse.getAddr();
        final BidibPort bidibPort = lcStatResponse.getBidibPort();
        int portStatus = lcStatResponse.getPortStatus();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.lcStat(address, bidibPort, portStatus);
            }
        }

        // notify the bulk sender
        notifyBulkSender(address, LcStatResponse.TYPE);
    }

    private void fireLcWait(LcWaitResponse lcWaitResponse) {
        byte[] address = lcWaitResponse.getAddr();
        final BidibPort bidibPort = lcWaitResponse.getBidibPort();
        int predRotationTime = lcWaitResponse.getPredictedRotationTime();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.lcWait(address, bidibPort, predRotationTime);
            }
        }
    }

    private void fireLcNa(LcNotAvailableResponse lcNotAvailableResponse) {
        byte[] address = lcNotAvailableResponse.getAddr();
        final BidibPort bidibPort = lcNotAvailableResponse.getBidibPort();
        Integer errorCode = lcNotAvailableResponse.getErrorCode();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.lcNa(address, bidibPort, errorCode);
            }
        }

        // notify the bulk sender
        notifyBulkSender(address, LcNotAvailableResponse.TYPE);
    }

    private void notifyBulkSender(byte[] address, Integer answerType) {
        // notify the bulk sender
        BidibNode node = nodeRegistry.findNode(address);
        LOGGER.trace("Notify bulk answer to node: {}", node);
        if (node != null) {
            try {
                node.notifyBulkAnswer(answerType);
            }
            catch (Exception ex) {
                LOGGER.error("Notify bulk answer to node failed.", ex);
            }
        }
    }

    private void fireLcConfig(LcConfigResponse lcConfigResponse) {

        byte[] address = lcConfigResponse.getAddr();
        LcConfig lcConfig = lcConfigResponse.getLcConfig();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.lcConfig(address, lcConfig);
            }
        }

        // notify the bulk sender
        notifyBulkSender(lcConfigResponse.getAddr(), LcConfigResponse.TYPE);
    }

    private void fireLcConfigX(LcConfigXResponse lcConfigXResponse) {

        byte[] address = lcConfigXResponse.getAddr();
        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.lcConfigX(address, lcConfigX);
            }
        }

        // check if the config is finished
        if (!lcConfigX.isContinueDetected()) {
            // notify the bulk sender
            notifyBulkSender(lcConfigXResponse.getAddr(), LcConfigXResponse.TYPE);
        }
        else {
            LOGGER.info("Continue detected in LcConfigX.");
        }
    }

    private void fireStall(final StallResponse stallResponse) {
        byte[] address = stallResponse.getAddr();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.stall(address, stallResponse.isStall());
            }
        }
    }

    private void fireNodeLost(byte[] address, Node node) {
        synchronized (nodeListeners) {
            for (NodeListener l : nodeListeners) {
                l.nodeLost(node);
            }
        }

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.nodeLost(address, node);
            }
        }

        // call the node factory directly
        if (nodeRegistry != null) {
            nodeRegistry.removeNode(node);
        }
    }

    private void fireNodeNew(byte[] parentAddress, Node node) {
        synchronized (nodeListeners) {
            for (NodeListener l : nodeListeners) {
                l.nodeNew(node);
            }
        }

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.nodeNew(parentAddress, node);
            }
        }
    }

    private void fireError(byte[] address, int errorCode, byte[] reasonData) {
        LOGGER
            .error("Error received from system, addr: {}, errorCode: {}, reasonData: {}", address, errorCode,
                reasonData);
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.error(address, errorCode, reasonData);
            }
        }
    }

    private void fireAccessoryState(
        byte[] address, final AccessoryState accessoryState, final AccessoryStateOptions accessoryStateOptions) {
        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.accessoryState(address, accessoryState, accessoryStateOptions);
            }
        }

        // notify the bulk sender
        notifyBulkSender(address, AccessoryStateResponse.TYPE);
    }

    protected void fireDynState(byte[] address, final FeedbackDynStateResponse feedbackDynStateResponse) {
        // populate the dynamic state ...

        int detectorNumber = feedbackDynStateResponse.getDetectorNumber();
        AddressData decoderAddress = feedbackDynStateResponse.getAddress();
        int dynNumber = feedbackDynStateResponse.getDynNumber();
        int dynValue = feedbackDynStateResponse.getDynValue();
        Integer timestamp = feedbackDynStateResponse.getTimestamp();

        synchronized (messageListeners) {
            for (MessageListener l : messageListeners) {
                l.dynState(address, detectorNumber, decoderAddress, dynNumber, dynValue, timestamp);
            }
        }
    }

    /**
     * Remove an orphan node. If the node does not disconnect according to specification or the node is an interface
     * node during update this update this can lead to orphan nodes.
     * 
     * @param node
     *            the node to be removed
     */
    public void removeOrphanNode(Node node) {
        LOGGER.info("Remove orphan node: {}", node);
        fireNodeLost(node.getAddr(), node);
    }
}
