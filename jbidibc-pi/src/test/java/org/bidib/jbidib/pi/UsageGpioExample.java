package org.bidib.jbidib.pi;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.trigger.GpioSyncStateTrigger;

public class UsageGpioExample {

    public static void main(String[] args) throws InterruptedException {

        // START SNIPPET: usage-create-controller-snippet
        // create gpio controller instance
        final GpioController gpio = GpioFactory.getInstance();
        // END SNIPPET: usage-create-controller-snippet

        // START SNIPPET: usage-provision-input-pin-snippet
        // provision gpio pin #02 as an input pin with its internal pull down resistor enabled
        // (configure pin edge to both rising and falling to get notified for HIGH and LOW state
        // changes)
        GpioPinDigitalInput myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_06, // PIN NUMBER
            "MyButton", // PIN FRIENDLY NAME (optional)
            PinPullResistance.PULL_DOWN); // PIN RESISTANCE (optional)

        // END SNIPPET: usage-provision-input-pin-snippet
        // START SNIPPET: usage-provision-output-pin-snippet
        // provision gpio pins #04 as an output pin and make sure is is set to LOW at startup
        GpioPinDigitalOutput myLed = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, // PIN NUMBER
            "My LED", // PIN FRIENDLY NAME (optional)
            PinState.LOW); // PIN STARTUP STATE (optional)
        // END SNIPPET: usage-provision-output-pin-snippet

        // START SNIPPET: usage-shutdown-pin-snippet
        // configure the pin shutdown behavior; these settings will be
        // automatically applied to the pin when the application is terminated
        // ensure that the LED is turned OFF when the application is shutdown
        myLed.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
        // END SNIPPET: usage-shutdown-pin-snippet

        // START SNIPPET: usage-control-pin-snippet
        System.out.println("Set LED high.");
        // explicitly set a state on the pin object
        myLed.setState(PinState.HIGH);

        // use convenience wrapper method to set state on the pin object
        System.out.println("Set LED low.");
        myLed.low();

        System.out.println("Set LED high.");
        myLed.high();

        // use toggle method to apply inverse state on the pin object
        System.out.println("Toggle LED.");
        myLed.toggle();

        Thread.sleep(500);

        // use pulse method to set the pin to the HIGH state for
        // an explicit length of time in milliseconds
        System.out.println("Pulse LED.");
        myLed.pulse(1000);
        // END SNIPPET: usage-control-pin-snippet

        // START SNIPPET: usage-read-pin-snippet
        // get explicit state enumeration for the GPIO pin associated with the button
        PinState myButtonState = myButton.getState();

        // use convenience wrapper method to interrogate the button state
        boolean buttonPressed = myButton.isHigh();
        System.out.println("Current buttonPressed: " + buttonPressed);
        // END SNIPPET: usage-read-pin-snippet

        // START SNIPPET: usage-register-listener-snippet
        // create and register gpio pin listener
        myButton.addListener(new GpioUsageExampleListener());
        // END SNIPPET: usage-register-listener-snippet

        // START SNIPPET: usage-trigger-snippet
        // create a gpio synchronization trigger on the input pin
        // when the input state changes, also set LED controlling gpio pin to same state
        myButton.addTrigger(new GpioSyncStateTrigger(myLed));
        // END SNIPPET: usage-trigger-snippet

        // keep program running until user aborts (CTRL-C)
        while (true) {
            Thread.sleep(500);
        }

        // stop all GPIO activity/threads by shutting down the GPIO controller
        // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
        // gpio.shutdown(); <--- implement this method call if you wish to terminate the Pi4J GPIO controller
    }

    // START SNIPPET: usage-listener-snippet
    public static class GpioUsageExampleListener implements GpioPinListenerDigital {
        @Override
        public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            // display pin state on console
            System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
        }
    }
    // END SNIPPET: usage-listener-snippet
}
