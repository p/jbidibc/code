package org.bidib.jbidibc.core.schema;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.bidib.jbidibc.core.schema.decodervendor.DecoderVendorType;
import org.bidib.jbidibc.core.schema.decodervendor.VendorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public final class DecoderVendorFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderVendorFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.decodervendor";

    public static final String XSD_LOCATION = "/xsd/decoderVendor.xsd";

    // public static final String JAXB_SCHEMA_LOCATION =
    // "http://www.bidib.org/schema/decodervendor xsd/decoderVendor.xsd";

    private DecoderVendorFactory() {
    }

    public static List<VendorType> getDecoderVendors() {
        return new DecoderVendorFactory().loadDecoderVendors();
    }

    private List<VendorType> loadDecoderVendors() {
        final String fileName = "/xml/protocol/DecoderIndex.bidib";

        InputStream is = BidibFactory.class.getResourceAsStream(fileName);
        if (is != null) {
            DecoderVendorType decoderVendor = loadDecoderVendorFile(is);
            if (decoderVendor != null) {
                try {
                    List<VendorType> vendors = decoderVendor.getVendor();
                    LOGGER.info("Loaded number of vendors: {}", vendors.size());
                    return vendors;
                }
                catch (Exception ex) {
                    LOGGER.warn("Get the vendors failed.", ex);
                }
            }
        }
        else {
            LOGGER.warn("Load DecoderIndex.bidib file failed.");
        }
        return null;
    }

    private DecoderVendorType loadDecoderVendorFile(File decoderVendorFile) {

        DecoderVendorType decoderVendor = null;
        InputStream is;
        try {
            is = new FileInputStream(decoderVendorFile);
            decoderVendor = loadDecoderVendorFile(is);
        }
        catch (FileNotFoundException ex) {
            LOGGER.info("No decoderVendor file found.");
        }
        return decoderVendor;
    }

    private DecoderVendorType loadDecoderVendorFile(InputStream is) {

        DecoderVendorType decoderVendor = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            JAXBElement<DecoderVendorType> jaxbElement =
                (JAXBElement<DecoderVendorType>) unmarshaller.unmarshal(xmlr, DecoderVendorType.class);
            decoderVendor = jaxbElement.getValue();
        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from input stream failed failed.", ex);
        }
        return decoderVendor;
    }

    public static String getDecoderVendorName(List<VendorType> vendors, int vendorId) {

        for (VendorType vendor : vendors) {

            if (vendor.getId() == vendorId) {
                return vendor.getName();
            }
        }

        return null;
    }

}
