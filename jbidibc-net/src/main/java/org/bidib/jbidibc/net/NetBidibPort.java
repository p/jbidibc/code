package org.bidib.jbidibc.net;

import java.io.IOException;
import java.net.InetAddress;

public interface NetBidibPort extends Runnable {

    static final byte[] BIDIB_NET_PREFIX = new byte[] { 'B', 'i', 'D', 'i', 'B' };

    static final byte[] BIDIB_WIZARD_NET_PREFIX = new byte[] { 'B', 'i', 'D', 'i', 'B', '-', 'W', 'i', 'z', 'a', 'r',
        'd' };

    /**
     * Send the data to the host.
     * 
     * @param sendData
     *            the data to send
     * @param address
     *            the receiving address of the host
     * @param portNumber
     *            the receiving port number of the host
     * @throws IOException
     */
    void send(byte[] sendData, InetAddress address, int portNumber) throws IOException;

    /**
     * Stop the port.
     */
    void stop();
}
