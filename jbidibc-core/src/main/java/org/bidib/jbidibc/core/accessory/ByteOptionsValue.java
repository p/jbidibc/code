package org.bidib.jbidibc.core.accessory;

import org.bidib.jbidibc.core.utils.ByteUtils;

public class ByteOptionsValue implements OptionsValue<Byte> {

    private final Byte value;

    public ByteOptionsValue(Byte value) {
        this.value = value;
    }

    @Override
    public Byte getValue() {
        return value;
    }

    public static int getIntValue(OptionsValue<?> value) {

        return ByteUtils.getInt(((ByteOptionsValue) value).value.byteValue());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ByteOptionsValue)) {
            return false;
        }
        ByteOptionsValue other = (ByteOptionsValue) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        }
        else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ByteOptionsValue:");
        sb.append(ByteUtils.byteToHex(value));
        return sb.toString();
    }
}
