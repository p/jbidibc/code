package org.bidib.jbidibc.ch341a;

import java.nio.ByteBuffer;

import org.bidib.jbidibc.ch341a.Helper.UCHARByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinBase;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.PVOID;
import com.sun.jna.platform.win32.WinDef.UCHAR;
import com.sun.jna.platform.win32.WinDef.ULONG;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

public class Ch341aTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ch341aTest.class);

    @Test(enabled = false)
    public void USBIO_GetVersion() {

        // Kernel32 INSTANCE = (Kernel32) Native.loadLibrary("kernel32", Kernel32.class);
        // Assert.assertNotNull(INSTANCE);

        // Ch341a ch341a = Native.loadLibrary("CH341DLL", Ch341a.class);
        // Ch341a ch341a = Native.loadLibrary("ch341dll.dll", Ch341a.class);

        Ch341a ch341a = Ch341a.INSTANCE;

        Assert.assertNotNull(ch341a);

        ULONG version = ch341a.CH341GetVersion();
        LOGGER.info("Current version: {}", version);

        ULONG iIndex = new ULONG(0);
        HANDLE handle = ch341a.CH341OpenDevice(iIndex);
        LOGGER.info("Current handle: {}", handle);

        Assert.assertNotEquals(handle, WinBase.INVALID_HANDLE_VALUE, "No valid HANDLE");

        IntByReference ioLength = new IntByReference();
        ioLength.setValue(1024);
        // char[] deviceDescription = new char[256];

        Pointer deviceDescription = new Memory(2048);
        PVOID ptrDeviceDescription = new PVOID(deviceDescription);

        // final PointerByReference p = new PointerByReference();
        // p.setValue(deviceDescription);
        BOOL retVal = ch341a.CH341GetDeviceDescr(iIndex, ptrDeviceDescription, ioLength);

        int bufferSize = ioLength.getValue();
        ByteBuffer bb = ptrDeviceDescription.getPointer().getByteBuffer(0, bufferSize);
        // String val = new String(bb.asCharBuffer().array());
        LOGGER.info("Current result: {}, bufferSize: {}, deviceDescription: {}", retVal.booleanValue(), bufferSize, bb);

        Pointer configDescription = new Memory(2048);
        PVOID ptrConfigDescription = new PVOID(configDescription);

        retVal = ch341a.CH341GetConfigDescr(iIndex, ptrConfigDescription, ioLength);

        int bufferSize2 = ioLength.getValue();
        LOGGER.info("Current result: {}, bufferSize2: {}, configDescription: {}", retVal.booleanValue(), bufferSize2,
            ptrConfigDescription.getPointer().getString(0, "UTF-8"));

        PVOID pDeviceName = ch341a.CH341GetDeviceName(iIndex);
        String deviceName = pDeviceName.getPointer().getString(0);
        // String deviceName = pDeviceName.toString();
        LOGGER.info("Current deviceName: {}", deviceName);

        ULONG versionIC = ch341a.CH341GetVerIC(iIndex);
        LOGGER.info("Current versionIC: {}", versionIC.longValue());

        ULONG iMode = new ULONG(0);
        retVal = ch341a.CH341SetStream(iIndex, iMode);
        LOGGER.info("SetStream, current result: {}", retVal.booleanValue());

        // // instantiate a callback wrapper instance
        // final Ch341a.PCH341_INT_ROUTINEImplementation callbackImpl = new Ch341a.PCH341_INT_ROUTINEImplementation();
        // ch341a.CH341SetIntRoutine(iIndex, callbackImpl);
        //
        // LOGGER.info("Wait for interrupt.");
        // try {
        // Thread.sleep(5000);
        // }
        // catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        //
        // LOGGER.info("Release interrupt routine.");
        // ch341a.CH341SetIntRoutine(iIndex, null);

        // CHARByReference iDeviceID = new CHARByReference(new CHAR((byte) (0x3C & 0xFF)));
        //
        // // instantiate a callback wrapper instance
        // final Ch341a.PCH341_NOTIFY_ROUTINEImplementation notifyCallbackImpl =
        // new Ch341a.PCH341_NOTIFY_ROUTINEImplementation();
        // ch341a.CH341SetDeviceNotify(iIndex, iDeviceID, notifyCallbackImpl);
        //
        // LOGGER.info("Wait for status notify.");
        // try {
        // Thread.sleep(30000);
        // }
        // catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        //
        // LOGGER.info("Release status notify routine.");
        // ch341a.CH341SetDeviceNotify(iIndex, iDeviceID, null);

        UCHAR iAddr = new UCHAR();
        iAddr.setValue(1);
        UCHAR iDevice = new UCHAR();
        iDevice.setValue(0x3C);
        UCHARByReference oByte = new UCHARByReference();

        retVal = ch341a.CH341ReadI2C(iIndex, iDevice, iAddr, oByte);
        LOGGER.info("Current result: {}, read byte: {}", retVal.booleanValue(), oByte.getValue());

        ULONG iWriteLength = new ULONG(2); // I2C slave address and one byte
        ULONG iReadLength = new ULONG(1);

        PointerByReference iWriteBuffer = new PointerByReference(new Memory(8));
        iWriteBuffer.getPointer().setByte(0, (byte) ((0x3C << 1) & 0xFF));
        iWriteBuffer.getPointer().setByte(1, (byte) (0x01 & 0xFF));

        LOGGER.info("CH341StreamI2C, prepared wr buffer: {}", iWriteBuffer.getPointer().getByteArray(0, 2));

        PointerByReference oReadBuffer = new PointerByReference(new Memory(8));

        retVal = ch341a.CH341StreamI2C(iIndex, iWriteLength, iWriteBuffer, iReadLength, oReadBuffer);
        LOGGER.info("CH341StreamI2C, current result: {}, read byte: {}", retVal.booleanValue(),
            oReadBuffer.getPointer().getByte(0));

        // close the device
        ch341a.CH341CloseDevice(iIndex);

        WinDef.ULONG drvVersion = ch341a.CH341GetDrvVersion();
        LOGGER.info("Current driver version: {}", drvVersion);

        IntByReference iStatus = new IntByReference();
        BOOL result = ch341a.CH341GetStatus(iIndex, iStatus);
        LOGGER.info("Current result: {}, status: {}", result.booleanValue(), iStatus.getValue());

    }
}
