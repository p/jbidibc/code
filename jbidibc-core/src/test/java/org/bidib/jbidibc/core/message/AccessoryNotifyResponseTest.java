package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AccessoryNotifyResponseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryNotifyResponseTest.class);

    @Test
    public void getAccessoryState() throws ProtocolException {
        byte[] message =
            new byte[] { 0x09, 0x01, 0x00, (byte) 0x79, (byte) 0xba, 0x02, 0x01, 0x03, (byte) 0x81,
                BidibLibrary.BIDIB_ACC_STATE_ERROR_SERVO };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_ACCESSORY_NOTIFY);

        AccessoryNotifyResponse accessoryNotifyResponse = (AccessoryNotifyResponse) bidibMessage;
        LOGGER.info("accessoryNotifyResponse: {}", accessoryNotifyResponse);

        Assert.assertNotNull(accessoryNotifyResponse.getAccessoryState());
        Assert.assertEquals(accessoryNotifyResponse.getAccessoryState().getAccessoryNumber(), 2);
        Assert.assertEquals(accessoryNotifyResponse.getAccessoryState().getAspect(), ByteUtils.getLowByte(1));
        Assert.assertEquals(accessoryNotifyResponse.getAccessoryState().getErrorCode(),
            Integer.valueOf(BidibLibrary.BIDIB_ACC_STATE_ERROR_SERVO));

    }
}
