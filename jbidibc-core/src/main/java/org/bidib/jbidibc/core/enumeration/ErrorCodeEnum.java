package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ErrorCodeEnum implements BidibEnum {
    // @formatter:off
    BIDIB_ERR_LC_PORT_NONE(BidibLibrary.BIDIB_ERR_LC_PORT_NONE), BIDIB_ERR_LC_PORT_GENERAL(
        BidibLibrary.BIDIB_ERR_LC_PORT_GENERAL), BIDIB_ERR_LC_PORT_UNKNOWN(BidibLibrary.BIDIB_ERR_LC_PORT_UNKNOWN), BIDIB_ERR_LC_PORT_INACTIVE(
        BidibLibrary.BIDIB_ERR_LC_PORT_INACTIVE), BIDIB_ERR_LC_PORT_EXEC(BidibLibrary.BIDIB_ERR_LC_PORT_EXEC), BIDIB_ERR_LC_PORT_BROKEN(
        BidibLibrary.BIDIB_ERR_LC_PORT_BROKEN);
    // @formatter:on

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorCodeEnum.class);

    private final byte type;

    ErrorCodeEnum(byte type) {
        this.type = type;
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create error code enum value from the provided byte value.
     * 
     * @param type
     *            numeric value of the error code
     * 
     * @return the error code enum value
     */
    public static ErrorCodeEnum valueOf(byte type) {
        ErrorCodeEnum result = null;

        for (ErrorCodeEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a error code.");
        }
        return result;
    }

    public static String formatErrorCode(Integer error) {
        if (error != null) {
            byte type = ByteUtils.getLowByte(error);
            try {
                ErrorCodeEnum errorCode = valueOf(type);
                String formatedErrorCode = errorCode.name();
                return formatedErrorCode;
            }
            catch (Exception ex) {
                LOGGER.warn("Format error code to string failed: {}", ByteUtils.getInt(type), ex);
                return "n/a";
            }
        }
        return null;
    }

}
