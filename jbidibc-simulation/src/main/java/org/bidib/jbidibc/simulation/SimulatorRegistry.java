package org.bidib.jbidibc.simulation;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPOutputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.CollectionUtils;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.jbidibc.simulation.nodes.HubType;
import org.bidib.jbidibc.simulation.nodes.MasterType;
import org.bidib.jbidibc.simulation.nodes.NodeType;
import org.bidib.jbidibc.simulation.nodes.Simulation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulatorRegistry {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulatorRegistry.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.simulation.nodes";

    private static final String XSD_LOCATION = "/xsd/simulation.xsd";

    public static final String JAXB_SCHEMA_LOCATION =
        "http://www.bidib.org/jbidibc/simulation/nodes xsd/simulation.xsd";

    private static SimulatorRegistry instance;

    private Map<String, SimulatorNode> simulators;

    private AtomicInteger nodeTabVersion = new AtomicInteger(0);

    private SimulatorRegistry() {
        LOGGER.info("Create instance of SimulatorRegistry.");
        simulators = new HashMap<String, SimulatorNode>();
    }

    public static synchronized SimulatorRegistry getInstance() {

        if (instance == null) {
            instance = new SimulatorRegistry();
        }
        return instance;
    }

    public void removeAll() {
        // remove all simulators and force close all simulation windows
        LOGGER.info("removeAll() will stop the simulators and clear the registry.");

        synchronized (simulators) {
            for (SimulatorNode simulator : simulators.values()) {
                LOGGER.info("Stop the simulator: {}", simulator);
                simulator.stop();
            }

            simulators.clear();
        }
    }

    public Map<String, SimulatorNode> getSimulators() {
        synchronized (simulators) {
            return Collections.unmodifiableMap(simulators);
        }
    }

    public void addSimulator(String nodeAddress, SimulatorNode simulator) {
        LOGGER.info("Add new simulator: {}, nodeAddress: {}", simulator, nodeAddress);
        synchronized (simulators) {
            simulators.put(nodeAddress, simulator);
        }
    }

    public SimulatorNode getSimulator(String nodeAddress) {
        LOGGER.info("Get simulator with nodeAddress: {}", nodeAddress);
        SimulatorNode simulator = null;
        synchronized (simulators) {
            simulator = simulators.get(nodeAddress);
        }
        if (simulator == null) {
            LOGGER.warn("No simulator found for nodeAddress: {}", nodeAddress);
        }
        return simulator;
    }

    public void loadSimulationConfiguration(
        InputStream simulationConfiguration, SimulationBidibMessageProcessor messageReceiver) {
        LOGGER.info("Load simulation configuration from: {}", simulationConfiguration);

        if (simulationConfiguration == null) {
            throw new IllegalArgumentException("The simulationConfiguration instance must be provided.");
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            // create a validating unmarshaller
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(SimulatorRegistry.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            Simulation simulation = (Simulation) unmarshaller.unmarshal(simulationConfiguration);
            MasterType master = simulation.getMaster();

            LOGGER.info("Fetched master from simulation configuration: {}", master);
            SimulatorNode simMaster = createSimulator(null, master, messageReceiver);

            if (master.getSubNodes() != null && master.getSubNodes().getNode() != null) {
                // create the subnodes of the current node
                createSubNodes(null, master.getSubNodes().getNode(), simMaster, messageReceiver);
            }

            // start the simulator
            simMaster.start();
        }
        catch (UnmarshalException ex) {
            LOGGER.warn("Load simulation configuration failed.", ex);

            throw new InvalidConfigurationException(
                "Load simulation configuration failed. Reason: " + ex.getCause().getMessage());
        }
        catch (Exception ex) {
            LOGGER.warn("Load simulation configuration failed.", ex);

            throw new InvalidConfigurationException("Load simulation configuration failed. Reason: "
                + (ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage()));
        }

    }

    private void createSubNodes(
        String parentAddress, List<NodeType> subNodes, SimulatorNode simParent,
        SimulationBidibMessageProcessor messageReceiver) {

        for (NodeType subNode : subNodes) {
            LOGGER.info("Create simulator for subNode: {}", subNode);

            // create the simulator for the subnode
            try {
                SimulatorNode simNode = createSimulator(parentAddress, subNode, messageReceiver);

                // if the parent is an interface node add it to the parent
                if (simParent != null && simParent instanceof InterfaceNode) {
                    LOGGER.info("Adding simulator to simParent: {}, simulator: {}", simParent, simNode);
                    ((InterfaceNode) simParent).addSubNode(simNode);
                }

                // start the simulator
                simNode.start();

                if (subNode instanceof HubType && ((HubType) subNode).getSubNodes() != null
                    && ((HubType) subNode).getSubNodes().getNode() != null) {

                    StringBuilder subNodeAddress = new StringBuilder();
                    if (StringUtils.isNotBlank(parentAddress) && !"0".equals(parentAddress)) {
                        LOGGER.info("The parent has address: {}", parentAddress);
                        subNodeAddress.append(parentAddress).append(".");
                    }
                    subNodeAddress.append(subNode.getAddress());

                    createSubNodes(subNodeAddress.toString(), ((HubType) subNode).getSubNodes().getNode(), simNode,
                        messageReceiver);
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Create simulator failed.", ex);
            }
        }

    }

    private SimulatorNode createSimulator(
        String parentAddress, NodeType node, SimulationBidibMessageProcessor messageReceiver) {
        LOGGER.info("Create new simulator for node: {}, parentAddress: {}", node, parentAddress);

        // create and register the nodes in the registry
        String className = node.getClassName();

        String nodeAddress = node.getAddress().trim();
        byte address = Byte.parseByte(nodeAddress);
        byte[] nodeAddressArray = new byte[] { address };
        long uniqueId = ByteUtils.convertUniqueIdToLong(node.getUniqueId());
        boolean autoAddFeature = (node.isAutoAddFeature() != null ? node.isAutoAddFeature().booleanValue() : false);

        try {

            // if the simulator has a parent we must combine the address
            StringBuilder sb = new StringBuilder();
            if (StringUtils.isNotBlank(parentAddress) && !"0".equals(parentAddress)) {
                LOGGER.info("The parent has address: {}", parentAddress);
                sb.append(parentAddress).append(".");

                // split the parent address
                String[] splitedParentAddress = parentAddress.split("\\.");
                nodeAddressArray = new byte[splitedParentAddress.length + 1];
                int index = 0;
                for (String part : splitedParentAddress) {
                    nodeAddressArray[index] = Byte.parseByte(part);
                    index++;
                }
                nodeAddressArray[index] = address;

                LOGGER.info("Prepared new nodeAddress for simulator: {}", new Object[] { nodeAddressArray });
            }
            sb.append(nodeAddress);
            LOGGER.info("Adding simulator with address: {}", sb.toString());

            String newNodeAddress = sb.toString();

            Constructor<SimulatorNode> constructor =
                (Constructor<SimulatorNode>) Class
                    .forName(className)
                    .getConstructor(byte[].class, long.class, boolean.class, SimulationBidibMessageProcessor.class);
            SimulatorNode simulator =
                constructor.newInstance(nodeAddressArray, uniqueId, autoAddFeature, messageReceiver);

            if (simulator != null) {
                LOGGER.info("Created new simulator: {}", simulator);

                simulator.setProtocolVersion(node.getProtocolVersion());
                simulator.setSoftwareVersion(node.getSoftwareVersion());

                simulator.init();

                if (node.getFeatures() != null && CollectionUtils.hasElements(node.getFeatures().getFeature())) {
                    simulator.setFeatures(node.getFeatures());
                }

                if (node.getCVs() != null && CollectionUtils.hasElements(node.getCVs().getCv())) {
                    simulator.setCVs(node.getCVs());
                }

                if (simulator instanceof DmxNode) {
                    DmxNode dmxNode = (DmxNode) simulator;
                    dmxNode.setDmxConfig(node.getDmxChannels());
                }

                if (simulator instanceof SwitchingFunctionsNode) {
                    SwitchingFunctionsNode switchingFunctionsNode = (SwitchingFunctionsNode) simulator;
                    switchingFunctionsNode.setPortsConfig(node.getINPUT());
                    switchingFunctionsNode.setPortsConfig(node.getSERVO());
                    switchingFunctionsNode.setPortsConfig(node.getSOUND());
                    switchingFunctionsNode.setPortsConfig(node.getSPORT());
                    switchingFunctionsNode.setPortsConfig(node.getLPORT());
                    switchingFunctionsNode.setPortsConfig(node.getBACKLIGHT());
                    switchingFunctionsNode.setPortsConfig(node.getMOTOR());
                }

                simulator.setNodeName(node.getUserName());
                simulator.setProductName(node.getProductName());

                // post construction processing
                simulator.postConstruct();

                addSimulator(newNodeAddress, simulator);
            }
            else {
                LOGGER.warn("No simulator available for configured node: {}", node);
            }

            return simulator;
        }
        catch (Exception ex) {
            LOGGER.warn("Create simulator failed.", ex);
            throw new RuntimeException("Create simulator failed.", ex);
        }
    }

    /**
     * @return the nodeTabVersion
     */
    public int getNextNodeTabVersion() {
        synchronized (nodeTabVersion) {
            int nextNodeTabVersion = nodeTabVersion.incrementAndGet();
            if (nextNodeTabVersion > 100) {
                nodeTabVersion.set(0);
                nextNodeTabVersion = nodeTabVersion.incrementAndGet();
            }
            return nextNodeTabVersion;
        }
    }

    /**
     * @param nodeTabVersion
     *            the nodeTabVersion to set
     */
    public void setNodeTabVersion(int nodeTabVersion) {
        this.nodeTabVersion.set(nodeTabVersion);
    }

    /**
     * Save the simulation data to file.
     * 
     * @param simulation
     *            the simulation
     * @param fileName
     *            the filename
     * @param gzip
     *            use gzip
     */
    public void saveSimulationConfiguration(final Simulation simulation, String fileName, boolean gzip) {

        OutputStream os = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, JAXB_SCHEMA_LOCATION);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(SimulatorRegistry.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            marshaller.setSchema(schema);

            os = new BufferedOutputStream(new FileOutputStream(fileName));
            if (gzip) {
                LOGGER.debug("Use gzip to compress simulation.");
                os = new GZIPOutputStream(os);
            }

            marshaller.marshal(simulation, new OutputStreamWriter(os, Charset.forName("UTF-8")));

            os.flush();

            LOGGER.info("Save simulation content to file passed: {}", fileName);
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save simulation failed.", ex);

            throw new RuntimeException("Save simulation failed.", ex);
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }
        }

    }
}
