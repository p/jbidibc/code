package org.bidib.jbidibc.ch341a.libusb;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.usb4java.Device;
import org.usb4java.LibUsb;

public class Ch341AUartTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ch341AUartTest.class);

    @BeforeTest
    public void prepare() throws Exception {
        LOGGER.info("Prepare Ch341AUartTest.");
        if (!SystemUtils.IS_OS_WINDOWS) {

            LOGGER.info("Prepare Ch341ATest, skip test because OS is: {}", SystemUtils.OS_NAME);
            throw new SkipException("Test should only run under Windows OS");
        }

        boolean deviceAttached = false;

        try (Ch341A ch341a = new Ch341A()) {
            ch341a.init(LibUsb.LOG_LEVEL_INFO);
            Device device = ch341a.findDevice(Ch341A.USB_VENDOR_ID_CH341, Ch341A.USB_DEVICE_ID_CH341_UART);
            Assert.assertNotNull(device);

            deviceAttached = true;
        }
        catch (RuntimeException ex) {
            LOGGER.warn("No device attached.", ex);
        }

        if (!deviceAttached) {
            throw new SkipException("Test should only run if a CH341A device in UART mode is attached.");
        }
    }

    @Test(enabled = true)
    public void initTest() throws Exception {

        try (Ch341A ch341a = new Ch341A()) {

            ch341a.init(LibUsb.LOG_LEVEL_DEBUG);
        }
    }
}
