package org.bidib.jbidibc.core;

import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddressDataTest {

    @Test
    public void fromByteArrayLocoForward() {
        byte[] message =
            new byte[] { 0x09, 0x00, 0x1B, (byte) BidibLibrary.MSG_CS_POM_ACK, 0x03, 0x00, 0x00, 0x00, 0x00, 0x01 };

        AddressData addressData = AddressData.fromByteArray(message, 4);
        Assert.assertNotNull(addressData);

        Assert.assertEquals(addressData.getAddress(), 3);
        Assert.assertEquals(addressData.getType(), AddressTypeEnum.LOCOMOTIVE_FORWARD);
    }

    @Test
    public void toByteArrayLocoForward() {

        byte[] message = new byte[2];

        AddressData addressData = new AddressData(4, AddressTypeEnum.LOCOMOTIVE_FORWARD);

        AddressData.toByteArray(addressData, message, 0);

        Assert.assertEquals(message, new byte[] { 0x04, 0x00 });
    }

    @Test
    public void toByteArrayLocoForward2() {

        AddressData addressData = new AddressData(4, AddressTypeEnum.LOCOMOTIVE_FORWARD);

        byte[] message = AddressData.toByteArray(addressData);

        Assert.assertEquals(message, new byte[] { 0x04, 0x00 });
    }

    @Test
    public void toByteArrayLocoBackward() {

        byte[] message = new byte[2];

        AddressData addressData = new AddressData(4, AddressTypeEnum.LOCOMOTIVE_BACKWARD);

        AddressData.toByteArray(addressData, message, 0);

        Assert.assertEquals(message, new byte[] { 0x04, (byte) 0x80 });
    }

    @Test
    public void toByteArrayLocoBackward2() {

        AddressData addressData = new AddressData(4, AddressTypeEnum.LOCOMOTIVE_BACKWARD);

        byte[] message = AddressData.toByteArray(addressData);

        Assert.assertEquals(message, new byte[] { 0x04, (byte) 0x80 });
    }

    @Test
    public void fromByteArrayLocoBackward() {
        byte[] message =
            new byte[] { 0x09, 0x00, 0x1B, (byte) BidibLibrary.MSG_CS_POM_ACK, 0x03, (byte) 0x80, 0x00, 0x00, 0x00,
                0x01 };

        AddressData addressData = AddressData.fromByteArray(message, 4);
        Assert.assertNotNull(addressData);

        Assert.assertEquals(addressData.getAddress(), 3);
        Assert.assertEquals(addressData.getType(), AddressTypeEnum.LOCOMOTIVE_BACKWARD);
    }

    @Test
    public void fromByteArrayStdAccessory() {
        byte[] message =
            new byte[] { 0x09, 0x00, 0x1B, (byte) BidibLibrary.MSG_CS_POM_ACK, 0x03, (byte) 0x40, 0x00, 0x00, 0x00,
                0x01 };

        AddressData addressData = AddressData.fromByteArray(message, 4);
        Assert.assertNotNull(addressData);

        Assert.assertEquals(addressData.getAddress(), 3);
        Assert.assertEquals(addressData.getType(), AddressTypeEnum.ACCESSORY);
    }

    @Test
    public void fromByteArrayExtAccessory() {
        byte[] message =
            new byte[] { 0x09, 0x00, 0x1B, (byte) BidibLibrary.MSG_CS_POM_ACK, 0x03, (byte) 0xC0, 0x00, 0x00, 0x00,
                0x01 };

        AddressData addressData = AddressData.fromByteArray(message, 4);
        Assert.assertNotNull(addressData);

        Assert.assertEquals(addressData.getAddress(), 3);
        Assert.assertEquals(addressData.getType(), AddressTypeEnum.EXTENDED_ACCESSORY);
    }
}
