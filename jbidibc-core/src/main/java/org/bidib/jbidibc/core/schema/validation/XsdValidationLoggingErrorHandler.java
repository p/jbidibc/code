package org.bidib.jbidibc.core.schema.validation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XsdValidationLoggingErrorHandler implements ErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(XsdValidationLoggingErrorHandler.class);

    private final List<String> errors = new LinkedList<>();

    public void clearErrors() {
        errors.clear();
    }

    public List<String> getErrors() {
        return Collections.unmodifiableList(errors);
    }

    @Override
    public void warning(SAXParseException ex) throws SAXException {
        LOGGER.warn("Warning detected, line: {}, col: {}, message: {}", ex.getLineNumber(), ex.getColumnNumber(),
            ex.getMessage());

        errors.add(String.format("Warning, line: %d, column: %d, message: %s", ex.getLineNumber(),
            ex.getColumnNumber(), ex.getMessage()));
    }

    @Override
    public void error(SAXParseException ex) throws SAXException {
        LOGGER.error("Error detected, line: {}, col: {}, message: {}", ex.getLineNumber(), ex.getColumnNumber(),
            ex.getMessage());

        errors.add(String.format("Error, line: %d, column: %d, message: %s", ex.getLineNumber(), ex.getColumnNumber(),
            ex.getMessage()));
    }

    @Override
    public void fatalError(SAXParseException ex) throws SAXException {
        LOGGER.error("Fatal error detected, line: {}, col: {}, message: {}", ex.getLineNumber(), ex.getColumnNumber(),
            ex.getMessage());

        errors.add(String.format("Fatal error, line: %d, column: %d, message: %s", ex.getLineNumber(),
            ex.getColumnNumber(), ex.getMessage()));
    }
}
