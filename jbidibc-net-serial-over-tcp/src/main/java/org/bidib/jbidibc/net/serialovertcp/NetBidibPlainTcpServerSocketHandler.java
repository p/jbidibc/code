package org.bidib.jbidibc.net.serialovertcp;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.net.DataPacket;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the handler for TCP client connections. Every client connection is handled with its own handler
 * instance.
 */
public class NetBidibPlainTcpServerSocketHandler extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibPlainTcpServerSocketHandler.class);

    protected static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("RAW");

    private Socket socket;

    private final NetMessageHandler messageReceiver;

    private AtomicBoolean runEnabled = new AtomicBoolean();

    private ByteArrayOutputStream receiveBuffer = new ByteArrayOutputStream(2048);

    private final String remoteHost;

    public NetBidibPlainTcpServerSocketHandler(final Socket socket, final NetMessageHandler messageReceiver) {
        this.socket = socket;
        this.messageReceiver = messageReceiver;

        remoteHost = socket.getInetAddress().getHostAddress();
    }

    public void run() {
        runEnabled.set(true);

        byte[] receiveData = new byte[1024];
        try (BufferedInputStream in = new BufferedInputStream(socket.getInputStream());) {
            int receivedCount = 0;

            // wait for client sending data
            while ((receivedCount = in.read(receiveData)) > 0 && runEnabled.get()) {

                // forward processing to handler
                if (messageReceiver != null) {
                    // LOGGER.info("Received a packet. Forward packet to messageReveiver: {}", messageReceiver);

                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Received data: {}", ByteUtils.bytesToHex(receiveData, receivedCount));
                    }

                    InetAddress address = socket.getInetAddress();
                    int portNumber = socket.getPort();

                    // append the received data to the buffer
                    receiveBuffer.write(receiveData, 0, receivedCount);

                    // the received data can contain more than a single packet !!!!

                    // 11:36:23.529 [INFO] org.bidib.jbidibc.net.NetBidibTcpPort [pool-26-thread-1] - Received data:
                    // 00 01 00 09 FE 05 00 05 90 01 01 D8 FE 00 01 00 0A FE 05 00 06 90 02 01 05 FE 00 01 00 0B FE
                    // 05 00 07 90 03 64 14 FE
                    try {
                        parsePackets(receiveBuffer, messageReceiver, address, portNumber);
                    }
                    catch (Exception pex) {
                        LOGGER.warn("Receive message failed.", pex);
                    }
                    finally {
                        receiveBuffer.reset();
                    }
                }
                else {
                    LOGGER.warn("No message receiver configured, data: {}",
                        ByteUtils.bytesToHex(receiveData, receivedCount));
                }
            }
        }
        catch (IOException ex) {
            if (runEnabled.get()) {
                LOGGER.warn("--- Interrupt NetBidibTcpPort-run", ex);
            }
            else {
                LOGGER.info("The NetBidibTcpPort worker is terminating.");
            }
        }
        finally {
            LOGGER.info("The socket connection was closed.");
            if (socket != null) {
                try {
                    socket.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close socket failed.", ex);
                }
                socket = null;
            }

            if (messageReceiver != null) {
                LOGGER.info("Cleanup the messageReceiver.");
                // say goodbye to the message receiver to cleanup resources
                messageReceiver.cleanup(remoteHost);
            }
            LOGGER.info("Cleanup work after close socked has finished.");
        }
    }

    protected void parsePackets(
        final ByteArrayOutputStream output, NetMessageHandler messageReceiver, InetAddress address, int portNumber)
        throws ProtocolException {

        // iterate over the data and find the packets and strip out all serial encoding
        try {

            parseInput(output, messageReceiver, address, portNumber);
        }
        catch (RuntimeException pex) {
            LOGGER.warn("Receive message failed, reason: {}", pex.getMessage());
        }
        finally {
            // append the remaining data
            output.reset();
        }
    }

    private void processMessages(
        final ByteArrayOutputStream output, NetMessageHandler messageReceiver, InetAddress address, int portNumber)
        throws ProtocolException {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Process messages will put data into DataPacket: {}", ByteUtils.bytesToHex(output));
        }
        // publish the packet

        // do not add the CRC to the data packet
        final DataPacket receivedPacket =
            new DataPacket(output.toByteArray(), 0, output.size() /*- 1*/, address, portNumber);
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Received data: {}", ByteUtils.bytesToHex(receivedPacket.getData()));
        }
        messageReceiver.receive(receivedPacket);

    }

    private boolean escapeHot;

    private ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream(100);

    /**
     * Parse the received data to process the received bidib packets.
     * 
     * @param receivedData
     *            the received data
     * @param len
     *            the len of the recevided data packet
     * @throws ProtocolException
     */
    protected void parseInput(
        final ByteArrayOutputStream receiveData, NetMessageHandler messageReceiver, InetAddress address, int portNumber)
        throws ProtocolException {
        if (receiveData != null) {
            int data = 0;

            int receivedCount = receiveData.size();
            byte[] receivedData = receiveData.toByteArray();

            MSG_RAW_LOGGER.info("<<<< len: {}, data: {}", receivedCount, ByteUtils.bytesToHex(receiveData));
            // LOGGER.info("<<<< len: {}, data: {}", receivedCount, ByteUtils.bytesToHex(receiveData));

            for (int index = 0; index < receivedCount; index++) {
                data = (receivedData[index] & 0xFF);
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("received data: {}", ByteUtils.byteToHex(data));
                }

                // check if the current is the end of a packet
                if (data == BidibLibrary.BIDIB_PKT_MAGIC) {

                    if (outputBuffer.size() == 0) {
                        // skip leading magic if no data in output
                        LOGGER.debug("Skip leading MAGIC packet.");
                        continue;
                    }

                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Received raw message: {}", ByteUtils.bytesToHex(outputBuffer));
                    }

                    if (MSG_RAW_LOGGER.isInfoEnabled()) {
                        MSG_RAW_LOGGER.info("<< [{}] - {}", outputBuffer.size(), ByteUtils.bytesToHex(outputBuffer));
                    }

                    // if a CRC error is detected in splitMessages the reading loop will terminate ...
                    try {
                        processMessages(outputBuffer, messageReceiver, address, portNumber);

                        outputBuffer.reset();
                    }
                    catch (ProtocolException ex) {
                        LOGGER.warn("Process messages failed.", ex);
                        // reset the escape hot flag
                        escapeHot = false;
                    }

                    // after process messages there could be more data in the stream that will be continued with
                    // the next packet
                }
                else if (data == BidibLibrary.BIDIB_PKT_ESCAPE) {
                    escapeHot = true;
                }
                else {
                    if (escapeHot) {
                        data ^= 0x20;
                        escapeHot = false;
                    }
                    // append data to output array
                    outputBuffer.write(ByteUtils.getLowByte(data));
                }
            }

            if (outputBuffer != null && outputBuffer.size() > 0) {
                byte[] remaining = outputBuffer.toByteArray();
                String remainingString = ByteUtils.bytesToHex(remaining);
                LOGGER.debug("Data remaining in output: {}", remainingString);
            }

        }
        else {
            LOGGER.error("No input available.");
        }

    }

}