package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum RcPlusOpCodesAck implements BidibEnum {

    // @formatter:off
    RC_BIND(BidibLibrary.RC_BIND_RES, "bind"), RC_PING(BidibLibrary.RC_PING_RES, "ping"), 
    RC_TID(BidibLibrary.RC_TID_RES, "tid"), RC_PING_ONCE_P0(BidibLibrary.RC_PING_ONCE_P0, "ping-once-p0"), 
    RC_PING_ONCE_P1(BidibLibrary.RC_PING_ONCE_P1, "ping-once-p1"), 
    RC_FIND_P0(BidibLibrary.RC_FIND_P0, "find-p0"), RC_FIND_P1(BidibLibrary.RC_FIND_P1, "find-p1");
    // @formatter:on

    private final String key;

    private final byte type;

    private RcPlusOpCodesAck(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create a rcplus opcode.
     * 
     * @param type
     *            numeric value of the rcplus opcode
     * 
     * @return RcPlusOpCodesAck
     */
    public static RcPlusOpCodesAck valueOf(byte type) {
        RcPlusOpCodesAck result = null;

        for (RcPlusOpCodesAck e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a rcplus opcode");
        }
        return result;
    }

}
