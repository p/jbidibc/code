package org.bidib.jbidibc.serial;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerialMessageReceiver extends MessageReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(SerialMessageReceiver.class);

    private ByteArrayOutputStream receiveBuffer = new ByteArrayOutputStream(2048);

    private SerialMessageParser messageParser;

    public SerialMessageReceiver(NodeRegistry nodeFactory) {
        super(nodeFactory, new BidibResponseFactory(), true);

        messageParser = new SerialMessageParser();
    }

    @Override
    public void enable() {
        LOGGER.info("enable is called.");

        messageParser.setEscapeHot(false);

        MSG_RAW_LOGGER.info("++++ Enable the message receiver.");

        try {
            receiveBuffer.reset();
        }
        catch (Exception ex) {
            LOGGER.warn("Reset buffered received data failed.", ex);
        }

        try {
            messageParser.reset();
        }
        catch (Exception ex) {
            LOGGER.warn("Reset buffered received data failed.", ex);
        }

        super.enable();
    }

    @Override
    public void disable() {
        LOGGER.info("Disable is called.");
        super.disable();

        MSG_RAW_LOGGER.info("++++ Disable the message receiver.");

        messageParser.setEscapeHot(false);
    }

    @Override
    public String getErrorInformation() {
        if (receiveBuffer.size() > 0) {

            byte[] remaining = receiveBuffer.toByteArray();
            return new String(remaining);
        }

        return null;
    }

    public void purgeReceivedDataInBuffer() {
        LOGGER.info("Purge the received data in the message buffer before get the lock.");

        LOGGER.info("Purge the received data in the message buffer. Current escapeHot: {}",
            messageParser.isEscapeHot());

        messageParser.setEscapeHot(false);

        if (receiveBuffer.size() > 0) {

            try {
                byte[] remaining = receiveBuffer.toByteArray();

                String remainingValue = new String(remaining, StandardCharsets.UTF_8);

                LOGGER.info("Cleared remaining data from message output buffer: {}, text: {}",
                    ByteUtils.bytesToHex(remaining), remainingValue);

                if (remainingValue.indexOf("I=") > -1) {
                    // This is the detection of the debug interface active
                    InvalidConfigurationException ice =
                        new InvalidConfigurationException("An active debug interface configuration was detected.");
                    ice.setReason("debug-interface-active");
                    throw ice;
                }
            }
            finally {
                receiveBuffer.reset();
            }

            LOGGER.info("Finished purge output stream.");
        }
        else {
            LOGGER.info("The message buffer is empty.");
        }
    }

    /**
     * Receive messages from the configured port
     * 
     * @param data
     *            the received data
     */
    @Override
    public void receive(final ByteArrayOutputStream data) {

        if (!running.get()) {
            LOGGER.info("The receiver is not running. Skip processing of messages.");
            try {
                byte[] rawdata = data.toByteArray();
                LOGGER.info("Receiver is stopped, number of bytes read: {}, buffer: {}", rawdata.length,
                    ByteUtils.bytesToHex(rawdata));
            }
            catch (Exception ex) {
                LOGGER.warn("Read data from input stream to buffer failed.", ex);
            }
            return;
        }

        MSG_RAW_LOGGER.info("<<<< start parse input");

        try {
            byte[] rawdata = data.toByteArray();
            parseInput(rawdata, rawdata.length);
        }
        catch (Exception e) {
            LOGGER.warn("Exception detected in message receiver!", e);

            // reset the escapeHot flag
            if (messageParser.isEscapeHot()) {
                LOGGER.warn("Reset the escapeHot to false.");

                messageParser.setEscapeHot(false);
            }
            throw new RuntimeException(e);
        }
        finally {
            MSG_RAW_LOGGER.info("<<<< finished parse input");
        }
    }

    /**
     * Parse the received data to process the received bidib packets.
     * 
     * @param receivedData
     *            the received data
     * @param len
     *            the len of the recevided data packet
     * @throws ProtocolException
     */
    protected void parseInput(final byte[] receivedData, int len) throws ProtocolException {

        messageParser.parseInput(this, receivedData, len);
    }
}
