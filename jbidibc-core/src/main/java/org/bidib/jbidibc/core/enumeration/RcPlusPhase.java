package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;

public enum RcPlusPhase implements BidibEnum {

    P0(0, "p0"), P1(1, "p1");

    private final byte type;

    private final String key;

    private RcPlusPhase(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }
}
