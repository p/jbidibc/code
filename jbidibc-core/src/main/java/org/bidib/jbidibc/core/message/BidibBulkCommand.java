package org.bidib.jbidibc.core.message;

public interface BidibBulkCommand {

    /**
     * @return the expected bulk response types
     */
    Integer[] getExpectedBulkResponseTypes();
}
