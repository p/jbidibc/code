package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.enumeration.SpeedStepsEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Response from command station with the information of manual drive
 */
public class CommandStationDriveStateResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_CS_DRIVE_STATE;

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationDriveStateResponse.class);

    CommandStationDriveStateResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 4) {
            throw new ProtocolException("No valid MSG_CS_DRIVE_STATE received. Expected more than length: "
                + (data != null ? data.length : "0"));
        }

        LOGGER.debug("Received response, speed: {}", getSpeed());
    }

    public CommandStationDriveStateResponse(byte[] addr, int num, int opCode, AddressData decoderAddress,
        SpeedStepsEnum speedSteps, int speed, byte[] functions) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_CS_DRIVE_STATE,
            prepareData(opCode, decoderAddress, speedSteps, speed, functions));
    }

    public String getName() {
        return "MSG_CS_DRIVE_STATE";
    }

    private static byte[] prepareData(
        int opCode, AddressData decoderAddress, SpeedStepsEnum speedSteps, int speed, byte[] functions) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteUtils.getLowByte(opCode));
        // write decoder address
        decoderAddress.writeToStream(out);
        // data
        out.write(speedSteps.getType());
        out.write((byte) 1); // output active
        out.write(ByteUtils.getLowByte(speed));
        out.write(functions, 0, functions.length);

        return out.toByteArray();
    }

    public byte getOpCode() {
        return getData()[0];
    }

    public int getAddress() {
        byte[] data = getData();

        return (data[1] & 0xFF) + ((data[2] & 0xFF) << 8);
    }

    public byte getFormat() {
        return getData()[3];
    }

    public byte getOutputActive() {
        return getData()[4];
    }

    public int getSpeed() {
        return ByteUtils.getInt(getData()[5]);
    }

    public byte getLights() {
        return (byte) (getData()[6] & 0x10);
    }

    public byte getFunctions(int index) {
        return getData()[6 + index];
    }

    public DriveState getDriveState() {
        DriveState driveState =
            new DriveState(getAddress(), ByteUtils.getInt(getData()[3]), ByteUtils.getInt(getData()[4]), getSpeed(),
                ByteUtils.getInt(getLights()));

        // TODO add the functions ...
        byte[] functions = new byte[4];
        for (int index = 0; index < DriveState.FUNCTIONS_INDEX_F21_F28 + 1; index++) {
            functions[index] = getFunctions(index);
        }
        driveState.setFunctions(functions);

        return driveState;
    }
}
