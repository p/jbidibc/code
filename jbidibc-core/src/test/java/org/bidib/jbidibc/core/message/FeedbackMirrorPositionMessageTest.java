package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FeedbackMirrorPositionMessageTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackMirrorPositionMessageTest.class);

    @Test
    public void feedbackMirrorPositionMessageTest() {

        FeedbackMirrorPositionMessage feedbackMirrorPositionMessage =
            new FeedbackMirrorPositionMessage(0x0201, 0, 0x0403);
        feedbackMirrorPositionMessage.setAddr(new byte[] { 1 });

        LOGGER.debug("Created message: {}", feedbackMirrorPositionMessage);

        byte[] content = feedbackMirrorPositionMessage.getContent();
        Assert.assertNotNull(content);
        Assert.assertEquals(content.length, 10);

        byte[] data = feedbackMirrorPositionMessage.getData();

        Assert.assertEquals(ByteUtils.getWORD(data, 0), 0x0201);
        Assert.assertEquals(ByteUtils.getWORD(data, 3), 0x0403);
    }
}
