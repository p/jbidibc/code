package org.bidib.jbidibc.core.protocol;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProtocolTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProtocolTest.class);

    private static final String INPUT_XSL = "/xml/protocol/protocol.xsl";

    private static final String DATA1_XML = "/xml/protocol/Protocol.bidib";

    @Test
    public void transformationTest() throws TransformerException, URISyntaxException {

        InputStream inputXSL = ProtocolTest.class.getResourceAsStream(INPUT_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = ProtocolTest.class.getResourceAsStream(DATA1_XML);
        Assert.assertNotNull(dataXML);

        TransformerFactory factory = TransformerFactory.newInstance();
        // prevent XXE
        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        }
        catch (IllegalArgumentException e) {
            LOGGER.warn("XSL transformer implementation doesn't support {} feature", XMLConstants.ACCESS_EXTERNAL_DTD);
        }
        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        }
        catch (IllegalArgumentException e) {
            LOGGER.warn("XSL transformer implementation doesn't support {} feature", XMLConstants.ACCESS_EXTERNAL_DTD);
        }
        StreamSource xslStream = new StreamSource(inputXSL);

        Transformer transformer = factory.newTransformer(xslStream);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

        StreamSource in = new StreamSource(dataXML);

        StringWriter outputXML = new StringWriter();
        StreamResult out = new StreamResult(outputXML);

        transformer.transform(in, out);

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File("target/bidib-messages.html"));
            IOUtils.write(outputXML.toString(), fos, Charset.forName("UTF-8"));
            fos.flush();
        }
        catch (Exception ex) {
            LOGGER.warn("Write messages.html file failed.", ex);
        }
        finally {
            if (fos != null) {
                try {
                    fos.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close file output stream failed.", ex);
                }
            }
        }
    }
}
