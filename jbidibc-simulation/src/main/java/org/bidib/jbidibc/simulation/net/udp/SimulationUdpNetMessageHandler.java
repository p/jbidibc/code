package org.bidib.jbidibc.simulation.net.udp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.net.DataPacket;
import org.bidib.jbidibc.net.NetBidibPort;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationUdpNetMessageHandler implements NetMessageHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationUdpNetMessageHandler.class);

    private BidibMessageProcessor messageReceiverDelegate;

    public SimulationUdpNetMessageHandler(BidibMessageProcessor messageReceiverDelegate) {
        this.messageReceiverDelegate = messageReceiverDelegate;
    }

    private ByteArrayOutputStream output = new ByteArrayOutputStream(2048);

    @Override
    public void receive(final DataPacket packet) {
        // a data packet was received ... process the envelope and extract the message
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Received a packet from address: {}, port: {}, data: {}", packet.getAddress(),
                packet.getPort(), ByteUtils.bytesToHex(packet.getData()));
        }

        try {
            output.write(packet.getData(), 0, packet.getData().length);

            // forward to the MessageReceiver

            LOGGER.info("Forward received message to messageReceiverDelegate: {}, data: {}", messageReceiverDelegate,
                ByteUtils.bytesToHex(output));

            messageReceiverDelegate.receive(output);
        }
        catch (Exception ex) {
            LOGGER.warn("Process messages failed.", ex);
            throw new RuntimeException(ex);
        }
        finally {
            output.reset();
        }
    }

    @Override
    public void send(NetBidibPort port, byte[] message) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Send message to port: {}, message: {}", port, ByteUtils.bytesToHex(message));
        }

        if (port != null) {

            try {
                port.send(message, null, 0);
            }
            catch (IOException ex) {
                LOGGER.warn("Send message to port failed.", ex);
                throw new RuntimeException("Send message to datagram socket failed.", ex);
            }
        }
        else {
            LOGGER.warn("Send not possible, the port is closed.");
        }
    }

    @Override
    public void acceptClient(String remoteHost) {
        LOGGER.info("Accept client with address: {}", remoteHost);
    }

    @Override
    public void cleanup(String remoteHost) {
        LOGGER.info("Cleanup client with address: {}", remoteHost);
    }

}
