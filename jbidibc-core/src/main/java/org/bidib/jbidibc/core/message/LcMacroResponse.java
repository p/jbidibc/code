package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.exception.ProtocolException;

public class LcMacroResponse extends BidibMessage {

    public static final Integer TYPE = BidibLibrary.MSG_LC_MACRO;

    LcMacroResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 6) {
            throw new ProtocolException("No valid MSG_LC_MACRO received.");
        }
    }

    public LcMacroResponse(byte[] addr, int num, byte macroNum, byte step, LcMacro lcMacro) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_LC_MACRO, lcMacro.getContent());
    }

    public String getName() {
        return "MSG_LC_MACRO";
    }
}
