package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum AccessoryStateOptionsKeys implements BidibEnum {
    // @formatter:off
    STATE_OPTION_CURRENT_ANGLE(BidibLibrary.BIDIB_ACC_DETAIL_CURR_ANGLE1DEG5), 
    STATE_OPTION_TARGET_ANGLE(BidibLibrary.BIDIB_ACC_DETAIL_TARGET_ANGLE1DEG5);
    // @formatter:on

    private final byte type;

    AccessoryStateOptionsKeys(int type) {
        this.type = ByteUtils.getLowByte(type);
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a accessory state options key.
     * 
     * @param type
     *            numeric value of the accessory state options key
     * 
     * @return AccessoryStateOptionsKeys
     */
    public static AccessoryStateOptionsKeys valueOf(byte type) {
        AccessoryStateOptionsKeys result = null;

        for (AccessoryStateOptionsKeys e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException(
                "Cannot map value '0x" + ByteUtils.byteToHex(type) + "' to an accessory state options key.");
        }
        return result;
    }

}
