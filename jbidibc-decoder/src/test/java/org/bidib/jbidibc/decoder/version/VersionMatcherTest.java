package org.bidib.jbidibc.decoder.version;

import org.bidib.jbidibc.decoder.version.VersionMatcher.VersionCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VersionMatcherTest {

    @Test
    public void patternATest() {

        String versionString = "123";
        VersionMatcher versionMatcher = new VersionMatcher();
        DecoderVersion result = versionMatcher.detectVersion(versionString);

        Assert.assertEquals(result.getMajorVersion(), Integer.valueOf(versionString));
    }

    @Test
    public void patternAplusTest() {

        VersionCheck versionCheck = new VersionCheck(VersionMatcher.PATTERN_A, "1.2.3");
        String result = versionCheck.check(VersionMatcher.versionMatcher);

        Assert.assertNull(result);

        result = new VersionCheck(VersionMatcher.PATTERN_A, "102").check(VersionMatcher.versionMatcher);

        Assert.assertEquals(result, "102");
    }

    @Test
    public void patternBTest() {

        String versionString = "1.2.3";

        VersionMatcher versionMatcher = new VersionMatcher();
        DecoderVersion result = versionMatcher.detectVersion(versionString);

        PatternBVersion expected = new PatternBVersion(versionString);
        expected.setMajorVersion(1);
        expected.setMinorVersion(2);
        expected.setMicroVersion(3);

        Assert.assertEquals(result, expected);
    }

    @Test
    public void patternCTest() {

        String versionString = "V1.2R3";

        VersionMatcher versionMatcher = new VersionMatcher();
        DecoderVersion result = versionMatcher.detectVersion(versionString);

        PatternCVersion expected = new PatternCVersion(versionString);
        expected.setMajorVersion(1);
        expected.setMinorVersion(2);
        expected.setMicroVersion(3);

        Assert.assertEquals(result, expected);

        PatternCVersion patternCVersion = (PatternCVersion) result;
        Assert.assertEquals(patternCVersion.getRevision(), Integer.valueOf(3));
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(VersionMatcherTest.class);

    @Test
    public void patternCExtTest() {
        // V{0:M0x07}.{0:M0x78}R{1}
        String pattern = "V{0:M0x07}.{0:M0x78}R{1}";
        PatternCVersion patternVersion = PatternCVersion.parsePattern(pattern);

        // PatternCVersion expected = new PatternCVersion(versionString);
        // expected.

        // String versionString = "V{0:M0x07}.{0:M0x78}R{1}";
        // // must get some bits by mask
        // // ^V(\{([0-9]){1,2}\:M([0-9]|x){1,4}\}\.?){1,2}R\{([0-9]){1,2}\}$
        // final String PATTERN_C = "^V(\\{([0-9]){1,2}\\:M([0-9]|x){1,4}\\}\\.?){1,2}R\\{([0-9]){1,2}\\}$";
        //
        // // pattern to get the part of the version
        // final String versionPatternStart = "V(\\{([0-9]){1,2}\\:M([0-9]|x){1,4}\\}\\.?){1,2}";
        // final String versionPattern = "\\{([0-9]){1,2}\\:M([0-9]|x){1,4}\\}";
        // // pattern to get the part of the revision
        // final String revisionPatternStart = "R\\{([0-9]){1,2}\\}";
        // final String revisionPattern = "([0-9]){1,2}";
        //
        // Matcher m = Pattern.compile(PATTERN_C).matcher(versionString);
        // if (m.matches()) {
        // LOGGER.info("The pattern matches! Current group: {}", m.group());
        // int groupCount = m.groupCount();
        // LOGGER.info("Current groupCount: {}", groupCount);
        //
        // for (int i = 0; i < groupCount; i++) {
        //
        // LOGGER.info("Current index: {}, group: {}", i, m.group(i));
        // }
        //
        // Matcher versionStartMatcher = Pattern.compile(versionPatternStart).matcher(versionString);
        // if (versionStartMatcher.find()) {
        // String versionGroup = versionStartMatcher.group();
        // LOGGER.info("VersionStart, current group: {}", versionGroup);
        //
        // int index = 0;
        // Matcher versionMatcher = Pattern.compile(versionPattern).matcher(versionGroup);
        // while (versionMatcher.find()) {
        // String currentGroup = versionMatcher.group();
        // LOGGER.info("Version, current group: {}, index: {}", currentGroup, index);
        //
        // index++;
        // }
        // }
        //
        // Matcher revisionStartMatcher = Pattern.compile(revisionPatternStart).matcher(versionString);
        // if (revisionStartMatcher.find()) {
        // String currentRevisionGroup = revisionStartMatcher.group();
        // LOGGER.info("RevisionStart, current group: {}", currentRevisionGroup);
        //
        // Matcher revisionMatcher = Pattern.compile(revisionPattern).matcher(currentRevisionGroup);
        // if (revisionMatcher.find()) {
        // String currentGroup = revisionMatcher.group();
        // LOGGER.info("Revision, current group: {}", currentGroup);
        //
        // Assert.assertEquals(currentGroup, "1");
        // }
        // }
        // }
        // else {
        // LOGGER.warn("The pattern does not match!");
        // }
        //
        // Assert.assertTrue(m.matches());
    }
}
