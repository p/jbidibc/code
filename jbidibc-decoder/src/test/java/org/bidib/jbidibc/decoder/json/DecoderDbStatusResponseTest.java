package org.bidib.jbidibc.decoder.json;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bidib.jbidibc.decoder.json.DecoderDbStatusResponse.DecoderStatusResponse;
import org.bidib.jbidibc.decoder.json.DecoderDbStatusResponse.FirmwareStatusResponse;
import org.bidib.jbidibc.decoder.json.DecoderDbStatusResponse.ManufacturersStatusResponse;
import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturerType;
import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturersList;
import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturersType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.github.markusbernhardt.proxy.ProxySearch;

public class DecoderDbStatusResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderDbStatusResponseTest.class);

    private static final String REST_SERVICE_URI = "https://www.decoderdb.de";

    @Test
    public void listAll() throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
        JaxbAnnotationModule module = new JaxbAnnotationModule();
        // configure as necessary
        mapper.registerModule(module);

        InputStream is = DecoderDbStatusResponseTest.class.getResourceAsStream("/json/listAll.json");

        DecoderDbStatusResponse listAll = mapper.readValue(is, DecoderDbStatusResponse.class);
        LOGGER.info("Loaded listAll: {}", listAll);

        Assert.assertNotNull(listAll);
        Assert.assertNotNull(listAll.getManufacturers());
        Assert.assertEquals(listAll.getManufacturers().getFilename(), "Manufacturers.xml");
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.set(2016, Calendar.MAY, 19, 00, 00, 00);
        cal.set(Calendar.MILLISECOND, 0);
        Assert.assertEquals(listAll.getManufacturers().getNmraListDate(), cal.getTime());
        Assert.assertTrue(listAll.getManufacturers().isValid());

        Assert.assertNotNull(listAll.getDecoderDetections());
        Assert.assertEquals(listAll.getDecoderDetections().getFilename(), "decoderDetection.xml");
        Assert.assertEquals(listAll.getDecoderDetections().getLink(), "https://www.decoderdb.de/?decoderdetection");
        Assert.assertTrue(listAll.getDecoderDetections().isValid());

        Assert.assertNotNull(listAll.getDecoder());
        Assert.assertEquals(listAll.getDecoder().length, 15);

        DecoderStatusResponse product = listAll.getDecoder()[0];
        Assert.assertNotNull(product);
        Assert.assertEquals(product.getFilename(), "Product_0_NMRA-Standard.xml");
        Assert.assertEquals(product.getManufacturerId(), Integer.valueOf(0));
        Assert.assertNull(product.getManufacturerExtendedId());
        Assert.assertEquals(product.getName(), "NMRA-Standard");

        product = listAll.getDecoder()[1];
        Assert.assertNotNull(product);
        Assert.assertEquals(product.getFilename(), "Product_13-256_DecoderDBLok.xml");
        Assert.assertEquals(product.getManufacturerId(), Integer.valueOf(13));
        Assert.assertEquals(product.getManufacturerExtendedId(), Integer.valueOf(256));
        Assert.assertEquals(product.getName(), "Decoder DB Lok");
    }

    @Test(enabled = true)
    public void listManufacturersRemote() throws MalformedURLException, URISyntaxException {
        LOGGER.info("listManufacturersRemote");

        String plainCreds = "cv:cv";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        LOGGER.info("base64Creds: {}", base64Creds);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);

        // RestTemplate restTemplate = new RestTemplate();
        RestTemplate restTemplate = createRestTemplate(REST_SERVICE_URI); // new RestTemplate();

        // ListAll listAll = restTemplate.getForObject("https://www.decoderdb.de/?listAll", ListAll.class);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<ManufacturersList> response =
            restTemplate.exchange(REST_SERVICE_URI + "/?manufacturers", HttpMethod.GET, request,
                ManufacturersList.class);
        ManufacturersList listManufacturers = (ManufacturersList) response.getBody();

        LOGGER.info("Retrieved listManufacturers: {}", listManufacturers);

        Assert.assertNotNull(listManufacturers);

        Assert.assertNotNull(listManufacturers.getManufacturers());
        Assert.assertNotNull(listManufacturers.getManufacturers().getManufacturer());
        Assert.assertFalse(listManufacturers.getManufacturers().getManufacturer().isEmpty());

        // find the DIY manufacturer
        final short ID = 13;

        List<ManufacturerType> manufacturers = listManufacturers.getManufacturers().getManufacturer();
        ManufacturerType manufacturerDiy = IterableUtils.find(manufacturers, new Predicate<ManufacturerType>() {

            @Override
            public boolean evaluate(ManufacturerType manufacturer) {
                return manufacturer.getId() == ID;
            }
        });

        Assert.assertNotNull(manufacturerDiy);
        LOGGER.info("Found DIY manufacturer: {}", manufacturerDiy);

        // find the OpenDCC manufacturer
        final Integer EXTENDED_ID_OPENDCC = Integer.valueOf(258);

        ManufacturerType manufacturerOpenDcc = IterableUtils.find(manufacturers, new Predicate<ManufacturerType>() {

            @Override
            public boolean evaluate(ManufacturerType manufacturer) {
                return manufacturer.getId() == ID && EXTENDED_ID_OPENDCC.equals(manufacturer.getExtendedId());
            }
        });

        Assert.assertNotNull(manufacturerOpenDcc);
        LOGGER.info("Found OpenDCC manufacturer: {}", manufacturerOpenDcc);
    }

    @Test(enabled = true)
    public void listAllRemote() throws MalformedURLException, URISyntaxException {
        LOGGER.info("listAllRemote");

        String plainCreds = "cv:cv";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        LOGGER.info("base64Creds: {}", base64Creds);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);

        // RestTemplate restTemplate = new RestTemplate();
        RestTemplate restTemplate = createRestTemplate(REST_SERVICE_URI); // new RestTemplate();

        // ListAll listAll = restTemplate.getForObject("https://www.decoderdb.de/?listAll", ListAll.class);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<DecoderDbStatusResponse> response =
            restTemplate.exchange(REST_SERVICE_URI + "/?listAll", HttpMethod.GET, request,
                DecoderDbStatusResponse.class);
        DecoderDbStatusResponse listAll = (DecoderDbStatusResponse) response.getBody();

        LOGGER.info("Retrieved listAll: {}", listAll);

        ManufacturersStatusResponse manufacturersStatusResponse = listAll.getManufacturers();
        Assert.assertNotNull(manufacturersStatusResponse);

        Date nmraListDate = manufacturersStatusResponse.getNmraListDate();
        Assert.assertNotNull(nmraListDate);
        Date lastUpdate = manufacturersStatusResponse.getLastUpdate();
        Assert.assertNotNull(lastUpdate);

        // get the decoders
        DecoderStatusResponse[] decoders = listAll.getDecoder();
        Assert.assertNotNull(decoders);
        Assert.assertTrue(decoders.length > 0);

        LOGGER.info("Current decoders: {}", new Object[] { decoders });

        // get the firmware items
        FirmwareStatusResponse[] firmwareItems = listAll.getFirmware();
        Assert.assertNotNull(firmwareItems);
        Assert.assertTrue(firmwareItems.length > 0);

        LOGGER.info("Current firmwareItems: {}", new Object[] { firmwareItems });

        // get the manufacturers
        String link = manufacturersStatusResponse.getLink();
        String filename = manufacturersStatusResponse.getFilename();
        LOGGER.info("Current nmraListDate: {}, lastUpdate: {}, filename: {}, link: {}", nmraListDate, lastUpdate,
            filename, link);

        // get the list of manufacturers
        ResponseEntity<ManufacturersList> responseManufacturers =
            restTemplate.exchange(link, HttpMethod.GET, request, ManufacturersList.class);
        Assert.assertNotNull(responseManufacturers);
        ManufacturersList manufacturersList = responseManufacturers.getBody();
        Assert.assertNotNull(manufacturersList);

        LOGGER.info("Current manufacturersList: {}", manufacturersList);

        Assert.assertNotNull(manufacturersList.getManufacturers());
        ManufacturersType manufacturersType = manufacturersList.getManufacturers();
        List<ManufacturerType> manufacturers = manufacturersType.getManufacturer();

        Assert.assertTrue(manufacturers.size() > 0);
    }

    private RestTemplate createRestTemplate(String restServiceUri) throws MalformedURLException, URISyntaxException {
        // final String username = "username";
        // final String password = "pa$$word";
        // final String proxyUrl = "proxy.nyc.bigtower.com";
        // final int port = 8080;
        //
        //
        // CredentialsProvider credsProvider = new BasicCredentialsProvider();
        // credsProvider.setCredentials(new AuthScope(proxyUrl, port),
        // new UsernamePasswordCredentials(username, password));

        HttpHost myProxy = null;
        Proxy proxy = findProxy(new URL(REST_SERVICE_URI).toURI());
        if (!Proxy.NO_PROXY.equals(proxy)) {
            try {
                InetSocketAddress addr = (InetSocketAddress) proxy.address();
                final String proxyUrl = addr.getHostName();
                final int port = addr.getPort();

                myProxy = new HttpHost(proxyUrl, port);
            }
            catch (Exception ex) {
                LOGGER.warn("Prepare proxy HttpHost failed.", ex);
            }
        }

        HttpClientBuilder clientBuilder = HttpClientBuilder.create();

        if (myProxy != null) {
            clientBuilder.setProxy(myProxy)/* .setDefaultCredentialsProvider(credsProvider). */;
        }
        clientBuilder.disableCookieManagement();

        HttpClient httpClient = clientBuilder.build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(httpClient);

        return new RestTemplate(factory);
    }

    @Test
    public void proxyTest() throws URISyntaxException, MalformedURLException {

        URL url = new URL("http://www.bidib.org");

        Proxy proxy = findProxy(url.toURI());

        LOGGER.info("Current proxy: {}", proxy);

        if (!Proxy.NO_PROXY.equals(proxy)) {
            try {
                InetSocketAddress addr = (InetSocketAddress) proxy.address();
                Properties systemSettings = System.getProperties();
                systemSettings.put("proxySet", "true");
                systemSettings.put("http.proxyHost", addr.getHostName());
                systemSettings.put("http.proxyPort", Integer.toString(addr.getPort()));
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                LOGGER.info(con.getResponseCode() + " : " + con.getResponseMessage());
                LOGGER.info("HTTP_OK: {} ", con.getResponseCode() == HttpURLConnection.HTTP_OK);
            }
            catch (Exception e) {
                LOGGER.warn("Check connection failed.", e);
            }
        }
    }

    private Proxy findProxy(URI uri) throws URISyntaxException {

        // Use the static factory method getDefaultProxySearch to create a proxy search instance
        // configured with the default proxy search strategies for the current environment.
        ProxySearch proxySearch = ProxySearch.getDefaultProxySearch();

        // Invoke the proxy search. This will create a ProxySelector with the detected proxy settings.
        ProxySelector proxySelector = proxySearch.getProxySelector();

        if (proxySelector != null) {
            // Install this ProxySelector as default ProxySelector for all connections.
            ProxySelector.setDefault(proxySelector);

            Proxy proxy = (Proxy) ProxySelector.getDefault().select(uri).iterator().next();

            LOGGER.info("proxy type: {}", proxy.type());
            InetSocketAddress addr = (InetSocketAddress) proxy.address();
            if (addr == null) {
                LOGGER.info("No Proxy");
            }
            else {
                LOGGER.info("proxy hostname: {}", addr.getHostName());
                LOGGER.info("proxy port: {}", addr.getPort());

                return proxy;
            }
        }
        else {
            LOGGER.info("No proxy selector available.");
        }
        return Proxy.NO_PROXY;
    }
}
