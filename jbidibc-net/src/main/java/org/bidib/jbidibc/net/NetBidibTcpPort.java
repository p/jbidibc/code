package org.bidib.jbidibc.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetBidibTcpPort implements NetBidibPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibTcpPort.class);

    private final InetAddress address;

    private final int portNumber;

    private final NetMessageHandler messageReceiver;

    private AtomicBoolean runEnabled = new AtomicBoolean();

    private Socket clientSocket;

    private ServerSocket serverSocket;

    public NetBidibTcpPort(InetAddress address, int portNumber, NetMessageHandler messageReceiver) throws IOException {

        this.messageReceiver = messageReceiver;
        this.address = address;
        this.portNumber = portNumber;

        if (address != null) {
            this.clientSocket = new Socket(address, portNumber);
            LOGGER.info("Created TCP client socket for address: {},  port number: {}", address, portNumber);
        }
        else {
            this.serverSocket = new ServerSocket(portNumber);
            LOGGER.info("Created TCP server socket on port number: {}", portNumber);
        }

    }

    private final ScheduledExecutorService acceptWorker = Executors.newScheduledThreadPool(1);

    private Map<Integer, Socket> clientMap = MapUtils.synchronizedMap(new HashedMap<>());

    @Override
    public void run() {
        LOGGER.info("Start the TCP socket.");
        runEnabled.set(true);

        if (clientSocket == null) {
            // add a task to the worker to let the node process the send queue
            acceptWorker.submit(new Runnable() {

                @Override
                public void run() {
                    // we must receive from multiple clients
                    while (runEnabled.get()) {
                        try {
                            LOGGER.info("Wait for client to connect.");

                            Socket socket = serverSocket.accept();
                            LOGGER.info("The client was accepted, socket: {}", socket);

                            clientMap.put(socket.getPort(), socket);

                            NetBidibTcpServerSocketHandler handler =
                                new NetBidibTcpServerSocketHandler(socket, messageReceiver);
                            handler.start();
                        }
                        catch (IOException ex) {
                            LOGGER.error("Start listen on server socket failed.", ex);
                            runEnabled.set(false);

                            return;
                        }
                    }
                }
            });

            LOGGER.info("Receiver thread has finished.");
        }
        else {
            LOGGER.info("Start client receiver handler.");
            // add a task to the worker to let the node process the send queue
            acceptWorker.submit(new NetBidibTcpServerSocketHandler(clientSocket, messageReceiver));
            LOGGER.info("Start client receiver handler has passed.");
        }

        LOGGER.info("Start has passed.");
    }

    public void stop() {
        LOGGER.info("Stop the TCP packet receiver, socket: {}, serverSocket: {}", clientSocket, serverSocket);
        runEnabled.set(false);

        if (clientSocket != null) {
            try {
                clientSocket.close();
            }
            catch (IOException ex) {
                LOGGER.warn("Close socket failed.", ex);
            }
            clientSocket = null;
        }

        if (serverSocket != null) {
            LOGGER.info("Close the server socket.");
            try {
                serverSocket.close();
            }
            catch (IOException ex) {
                LOGGER.warn("Close serverSocket failed.", ex);
            }

            serverSocket = null;
        }
    }

    private boolean initialPacketSent;

    private ByteArrayOutputStream sendPacketBuffer = new ByteArrayOutputStream(100);

    /**
     * Send the data to the host.
     * 
     * @param sendData
     *            the data to send
     * @param address
     *            the receiving address of the host
     * @param portNumber
     *            the receiving port number of the host
     * @throws IOException
     */
    public void send(byte[] sendData, InetAddress address, int portNumber) throws IOException {
        // if (LOGGER.isTraceEnabled()) {
        LOGGER.info("Send data to socket, port: {}, bytes: {}", portNumber, ByteUtils.bytesToHex(sendData));
        // }

        if (clientSocket != null) {

            if (!initialPacketSent) {

                // calculate len
                int len = NetBidibPort.BIDIB_WIZARD_NET_PREFIX.length + 3;
                sendPacketBuffer.write(ByteUtils.getLowByte(len));

                sendPacketBuffer.write(ByteUtils.getLowByte(0));
                sendPacketBuffer.write(ByteUtils.getLowByte(0));

                sendPacketBuffer.write(ByteUtils.getLowByte(BidibLibrary.MSG_LOCAL_EMITTER));
                // prepend the BIDIB_WIZARD_NET_PREFIX for the initial packet
                sendPacketBuffer.write(NetBidibPort.BIDIB_WIZARD_NET_PREFIX);
                initialPacketSent = true;
            }

            sendPacketBuffer.write(sendData);

            try {
                sendBuffer(sendPacketBuffer);
            }
            finally {
                sendPacketBuffer.reset();
            }
        }
        else {
            // get the socket from the registry to send the data to the correct client
            Socket socket = clientMap.get(portNumber);
            LOGGER.info("Send data to socket: {}", socket);

            socket.getOutputStream().write(sendData);
            socket.getOutputStream().flush();
        }
    }

    private void sendBuffer(final ByteArrayOutputStream sendData) throws IOException {

        LOGGER.info("Send data to client socket: {}", ByteUtils.bytesToHex(sendData));

        clientSocket.getOutputStream().write(sendData.toByteArray());
        clientSocket.getOutputStream().flush();
    }
}
