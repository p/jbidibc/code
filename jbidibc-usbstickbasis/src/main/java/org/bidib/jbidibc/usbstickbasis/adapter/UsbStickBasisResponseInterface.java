package org.bidib.jbidibc.usbstickbasis.adapter;

import org.bidib.jbidibc.core.message.BidibMessage;

public interface UsbStickBasisResponseInterface {

    void addLog(String message);

    /**
     * Publish the response.
     * 
     * @param bidibMessage
     *            the message
     */
    void publishReponse(final BidibMessage bidibMessage);

    /**
     * Publish the product name.
     * 
     * @param productName
     *            the product name
     */
    void publishProductName(String productName);

    /**
     * @return the selected car model
     */
    SelectedCar getSelectedCarModel();
}
