package org.bidib.jbidibc.decoder.decoderdb;

import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturerType;
import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturersList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ManufacturerAccessTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManufacturerAccessTest.class);

    @Test
    public void fetch() {
        LOGGER.info("Fetch manufacturers from DecoderDB.");

        ManufacturerAccess manufacturerAccess = new ManufacturerAccess();
        ManufacturersList listManufacturers = manufacturerAccess.fetch("cv", "cv".toCharArray());

        LOGGER.info("Retrieved listManufacturers: {}", listManufacturers);

        Assert.assertNotNull(listManufacturers);

        Assert.assertNotNull(listManufacturers.getManufacturers());
        Assert.assertNotNull(listManufacturers.getManufacturers().getManufacturer());
        Assert.assertFalse(listManufacturers.getManufacturers().getManufacturer().isEmpty());

        // find the DIY manufacturer
        final short ID = 13;

        List<ManufacturerType> manufacturers = listManufacturers.getManufacturers().getManufacturer();
        ManufacturerType manufacturerDiy = IterableUtils.find(manufacturers, new Predicate<ManufacturerType>() {

            @Override
            public boolean evaluate(ManufacturerType manufacturer) {
                return manufacturer.getId() == ID;
            }
        });

        Assert.assertNotNull(manufacturerDiy);
        LOGGER.info("Found DIY manufacturer: {}", manufacturerDiy);

        // find the OpenDCC manufacturer
        final Integer EXTENDED_ID_OPENDCC = Integer.valueOf(258);

        ManufacturerType manufacturerOpenDcc = IterableUtils.find(manufacturers, new Predicate<ManufacturerType>() {

            @Override
            public boolean evaluate(ManufacturerType manufacturer) {
                return manufacturer.getId() == ID && EXTENDED_ID_OPENDCC.equals(manufacturer.getExtendedId());
            }
        });

        Assert.assertNotNull(manufacturerOpenDcc);
        LOGGER.info("Found OpenDCC manufacturer: {}", manufacturerOpenDcc);
    }
}
