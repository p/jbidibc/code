package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateOperation;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.BidibNode.EncodedMessage;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FwUpdateOpMessageTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FwUpdateOpMessageTest.class);

    @Test
    public void fwUpdateOpMessageTest() throws ProtocolException {

        DummyNode bidibNode = new DummyNode(new byte[] { 1 }, null);

        long uniqueId = 0x0d70000d00L;
        byte[] data = ByteUtils.convertLongToUniqueId(uniqueId);

        bidibNode.sendFirmwareUpdateOperation(FirmwareUpdateOperation.ENTER, data);

        Assert.assertNotNull(bidibNode.encodedMessage);
        LOGGER.info("The encoded message: {}", ByteUtils.bytesToHex(bidibNode.encodedMessage.getMessage()));

        EncodedMessage encodedMessage = bidibNode.encodedMessage;
        Assert.assertEquals(encodedMessage.getMessage().length, 13);

        Assert.assertEquals(encodedMessage.getMessage()[8], (byte) 0x0D);
        Assert.assertEquals(encodedMessage.getMessage()[9], (byte) 0x70);
        Assert.assertEquals(encodedMessage.getMessage()[10], (byte) 0x00);
        Assert.assertEquals(encodedMessage.getMessage()[11], (byte) 0x0D);
        Assert.assertEquals(encodedMessage.getMessage()[12], (byte) 0x00);
    }

    @Test
    public void fwUpdateOpMessageBootloaderTest() throws ProtocolException {

        // 0xfe, 0x0c, 0x01, 0x00, 0x06, 0x0f, 0x00, 0x0d, 0xe9, 0x12, 0x34, 0x56, 0x00, 0x00, 0x8b, 0xfe

        DummyNode bidibNode = new DummyNode(new byte[] { 1 }, null);

        long uniqueId = 0x0DE9123456L;
        byte[] data = ByteUtils.convertLongToUniqueId(uniqueId);

        bidibNode.sendFirmwareUpdateOperation(FirmwareUpdateOperation.ENTER, data);

        Assert.assertNotNull(bidibNode.encodedMessage);
        LOGGER.info("The encoded message: {}", ByteUtils.bytesToHex(bidibNode.encodedMessage.getMessage()));

        EncodedMessage encodedMessage = bidibNode.encodedMessage;
        Assert.assertEquals(encodedMessage.getMessage().length, 13);

        Assert.assertEquals(encodedMessage.getMessage()[8], (byte) 0x0D);
        Assert.assertEquals(encodedMessage.getMessage()[9], (byte) 0xE9);
        Assert.assertEquals(encodedMessage.getMessage()[10], (byte) 0x12);
        Assert.assertEquals(encodedMessage.getMessage()[11], (byte) 0x34);
        Assert.assertEquals(encodedMessage.getMessage()[12], (byte) 0x56);
    }

    private static final class DummyNode extends BidibNode {

        private EncodedMessage encodedMessage;

        protected DummyNode(byte[] address, MessageReceiver messageReceiver) {
            super(address, messageReceiver, false);
        }

        @Override
        protected BidibMessage send(
            BidibCommand message, Integer receiveTimeout, boolean expectAnswer, Integer... expectedResponseTypes)
            throws ProtocolException {

            // shortcut for test
            encodeMessage(message);

            BidibMessage response = new FwUpdateStatResponse(getAddr(), 1, (byte) 0, (byte) 0);
            return response;
        }

        @Override
        protected EncodedMessage encodeMessage(BidibCommand message) {
            encodedMessage = super.encodeMessage(message);

            return encodedMessage;
        }

    }

}
