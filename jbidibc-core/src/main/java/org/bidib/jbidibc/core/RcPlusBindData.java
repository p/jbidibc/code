package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcPlusBindData {

    private static final Logger LOGGER = LoggerFactory.getLogger(RcPlusBindData.class);

    private final DecoderUniqueIdData uniqueIdData;

    private final AddressData address;

    public RcPlusBindData(DecoderUniqueIdData uniqueIdData, AddressData address) {
        this.uniqueIdData = uniqueIdData;
        this.address = address;
    }

    /**
     * @return the rcPlus unique id
     */
    public DecoderUniqueIdData getUniqueId() {
        return uniqueIdData;
    }

    public AddressData getAddress() {
        return address;
    }

    /**
     * Create a RcPlusBindData instance from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset to start
     * @return the RcPlusBindData instance
     */
    public static RcPlusBindData fromByteArray(byte[] data, int offset) {
        if (data.length < (offset + 7)) {
            LOGGER.warn("The size of the provided data does not meet the expected length.");
            throw new IllegalArgumentException(
                "The size of the provided data does not meet the expected length (=7). Provided length: " + data.length);
        }

        DecoderUniqueIdData uniqueIdData = DecoderUniqueIdData.fromByteArray(data, offset);
        AddressData address = AddressData.fromByteArray(data, offset + 5);

        RcPlusBindData answerData = new RcPlusBindData(uniqueIdData, address);

        LOGGER.trace("Return RcPlusBindData: {}", answerData);
        return answerData;
    }

    public void writeToStream(ByteArrayOutputStream out) {
        getUniqueId().writeToStream(out);
        getAddress().writeToStream(out);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
