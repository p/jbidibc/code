package org.bidib.jbidibc.core.node;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationVariable implements Serializable, Comparable<ConfigurationVariable>, ConfigVar {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationVariable.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_VALUE = "value";

    private transient PropertyChangeSupport changeSupport;

    private String name;

    private String value;

    private boolean timeout;

    private boolean skipOnTimeout;

    private int minCvNumber;

    private int maxCvNumber;

    public ConfigurationVariable() {
    }

    /**
     * Creates a new instance of ConfigurationVariable
     * 
     * @param name
     *            the CV name
     * @param value
     *            the CV value
     */
    public ConfigurationVariable(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public final synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        if (changeSupport == null) {
            changeSupport = createPropertyChangeSupport(this);
        }
        changeSupport.addPropertyChangeListener(listener);
    }

    public final synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null || changeSupport == null) {
            return;
        }
        changeSupport.removePropertyChangeListener(listener);
    }

    protected PropertyChangeSupport createPropertyChangeSupport(final Object bean) {
        return new PropertyChangeSupport(bean);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        PropertyChangeSupport aChangeSupport = this.changeSupport;
        if (aChangeSupport == null) {
            return;
        }
        aChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    @Override
    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;

        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    /**
     * @return the value
     */
    @Override
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    @Override
    public void setValue(String value) {
        String oldValue = this.value;
        this.value = value;

        firePropertyChange(PROPERTY_VALUE, oldValue, value);
    }

    /**
     * @return the timeout
     */
    public boolean isTimeout() {
        return timeout;
    }

    /**
     * @param timeout
     *            the timeout to set
     */
    public void setTimeout(boolean timeout) {
        this.timeout = timeout;
    }

    /**
     * @return the skipOnTimeout flag
     */
    public boolean isSkipOnTimeout() {
        return skipOnTimeout;
    }

    /**
     * @param skipOnTimeout
     *            the skipOnTimeout flag to set
     */
    public void setSkipOnTimeout(boolean skipOnTimeout) {
        this.skipOnTimeout = skipOnTimeout;
    }

    /**
     * @return the minCvNumber
     */
    public int getMinCvNumber() {
        return minCvNumber;
    }

    /**
     * @param minCvNumber
     *            the maxCvNumber to set
     */
    public void setMinCvNumber(int minCvNumber) {
        LOGGER.debug("Set the minCvNumber: {}", minCvNumber);
        this.minCvNumber = minCvNumber;
    }

    /**
     * @return the maxCvNumber
     */
    public int getMaxCvNumber() {
        return maxCvNumber;
    }

    /**
     * @param maxCvNumber
     *            the maxCvNumber to set
     */
    @Override
    public void setMaxCvNumber(int maxCvNumber) {
        LOGGER.debug("Set the maxCvNumber: {}", maxCvNumber);
        this.maxCvNumber = maxCvNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ConfigurationVariable) {
            ConfigurationVariable other = (ConfigurationVariable) obj;
            return name.equals(other.getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(ConfigurationVariable o) {
        return name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
