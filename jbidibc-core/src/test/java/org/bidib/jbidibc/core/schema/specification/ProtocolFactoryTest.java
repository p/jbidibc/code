package org.bidib.jbidibc.core.schema.specification;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.bidib2.protocol.FeatureCode;
import org.bidib.jbidibc.core.schema.bidib2.protocol.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ProtocolFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProtocolFactoryTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/xml-test/bidib/protocol";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void getMessageTypes() {
        List<MessageType> messageTypes = ProtocolFactory.getMessageTypes();
        Assert.assertNotNull(messageTypes);
        Assert.assertEquals(messageTypes.size(), 127);
    }

    @Test
    public void loadFeatureCodesTest() {
        List<FeatureCode> featureCodes = ProtocolFactory.getFeatureCodes();
        Assert.assertNotNull(featureCodes);
        Assert.assertEquals(featureCodes.size(), 72);
    }
}
