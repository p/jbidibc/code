package org.bidib.jbidibc.decoder.schema.decoderfirmware;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.exception.InvalidContentException;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class DecoderFirmwareDefinitionFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderFirmwareDefinitionFactory.class);

    private static JAXBContext jaxbContext;

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.decoder.schema.decoderfirmware";

    public static final String XSD_LOCATION = "/xsd/decoderFirmware.xsd";

    public static final String XSD_LOCATION_COMMON_TYPES = "/xsd/commonTypes.xsd";

    // public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/labels xsd/wizard-labels.xsd";

    public DecoderFirmwareDefinition loadFirmwareDefinition(String filePath) {

        if (StringUtils.isBlank(filePath)) {
            LOGGER.error("The decoderFirmwareDefinition file must be specified!");
            return null;
        }

        LOGGER.info("Load decoder firmware from file: {}", filePath);

        DecoderFirmwareDefinition decoderFirmwareDefinition = null;
        FileInputStream is = null;
        File file = null;
        try {
            file = new File(filePath);

            is = new FileInputStream(file);

            decoderFirmwareDefinition = loadFirmwareDefinitionFile(is);
            LOGGER.trace("Loaded decoderFirmwareDefinition: {}", decoderFirmwareDefinition);

        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load saved decoderFirmwareDefinition from file failed with schema exception: {}",
                ex.getMessage());

            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    LOGGER.warn("Close input stream failed.", e);
                }
                is = null;
            }

            if (file != null) {

                try {
                    is = new FileInputStream(file);
                    List<String> errors = validateFirmwareDefinitionFile(is, filePath);

                    // LOGGER.warn("The following validation errors were detected: {}", errors);

                    throw new InvalidContentException(
                        "Validation of decoder firmware definition file failed: " + filePath, errors);
                }
                catch (FileNotFoundException e) {
                    LOGGER.warn("Load saved decoderFirmwareDefinition for validation from file failed: {}",
                        e.getMessage());
                }
            }
        }
        catch (IllegalArgumentException | FileNotFoundException ex) {
            LOGGER.warn("Load saved decoderFirmwareDefinition from file failed: {}", ex.getMessage());
        }
        catch (InvalidContentException ex) {
            // rethrow exception
            throw ex;
        }
        catch (Exception ex) {
            LOGGER.warn("Load saved decoderFirmwareDefinition from file failed: {}", ex.getMessage());
        }

        return decoderFirmwareDefinition;
    }

    protected String getFirmwareDefinitionFileName() {
        // TODO change it
        return "test";
    }

    /**
     * Load the labels from the provided input stream.
     * 
     * @param is
     *            the input stream
     * @return the labels
     */
    private List<String> validateFirmwareDefinitionFile(final InputStream is, String filePath) {
        LOGGER.info("Validate decoder firmware definition file: {}", filePath);

        List<String> errors = null;
        try {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            // Schema schema =
            // schemaFactory
            // .newSchema(new File(DecoderFirmwareDefinitionFactory.class.getResource(XSD_LOCATION).getPath()));

            StreamSource streamSourceSchemaCommonTypes =
                new StreamSource(DecoderFirmwareDefinitionFactory.class.getResourceAsStream(XSD_LOCATION_COMMON_TYPES));
            StreamSource streamSourceSchema =
                new StreamSource(DecoderFirmwareDefinitionFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceSchemaCommonTypes, streamSourceSchema });

            StreamSource streamSource = new StreamSource(is);

            Validator validator = schema.newValidator();
            XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();
            validator.setErrorHandler(errorHandler);

            validator.validate(streamSource);

            errors = errorHandler.getErrors();
        }
        catch (Exception ex) {
            LOGGER.warn("Validate decoderFirmwareDefinition from file failed: {}", getFirmwareDefinitionFileName(), ex);
            throw new InvalidSchemaException("Load decoderFirmwareDefinition from file failed.");
        }

        return errors;
    }

    /**
     * Load the labels from the provided input stream.
     * 
     * @param is
     *            the input stream
     * @return the labels
     */
    private DecoderFirmwareDefinition loadFirmwareDefinitionFile(final InputStream is) {
        LOGGER.info("Load decoder firmware definition file.");

        DecoderFirmwareDefinition decoderFirmwareDefinition = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for decoder firmware definition.");
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            StreamSource streamSourceSchemaCommonTypes =
                new StreamSource(DecoderFirmwareDefinitionFactory.class.getResourceAsStream(XSD_LOCATION_COMMON_TYPES));
            StreamSource streamSourceSchema =
                new StreamSource(DecoderFirmwareDefinitionFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceSchemaCommonTypes, streamSourceSchema });

            // Schema schema =
            // schemaFactory
            // .newSchema(new File(DecoderFirmwareDefinitionFactory.class.getResource(XSD_LOCATION).getPath()));

            unmarshaller.setSchema(schema);

            decoderFirmwareDefinition = (DecoderFirmwareDefinition) unmarshaller.unmarshal(is);
        }
        catch (UnmarshalException ex) {
            LOGGER.warn("Load decoderFirmwareDefinition from file failed with UnmarshalException.", ex);
            if (ex.getCause() instanceof SAXParseException) {
                throw new InvalidSchemaException("Load decoderFirmwareDefinition from file failed");
            }

            throw new IllegalArgumentException(
                "Load decoderFirmwareDefinition from file failed with UnmarshalException.");
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load decoderFirmwareDefinition from file failed: {}", getFirmwareDefinitionFileName(), ex);

            throw new InvalidSchemaException(
                "Load decoderFirmwareDefinition from file failed: " + getFirmwareDefinitionFileName());
        }
        catch (Exception ex) {
            LOGGER.warn("Load firmwareDefinition from file failed: {}", getFirmwareDefinitionFileName(), ex);
            throw new RuntimeException(
                "Load decoderFirmwareDefinition from file failed: " + getFirmwareDefinitionFileName());
        }
        return decoderFirmwareDefinition;
    }
}
