package org.bidib.jbidibc.core.enumeration;

public enum AccessoryAddressingEnum implements BidibEnum {
    // @formatter:off
    RCN_213("rcn-213", 0), DIRECT("direct", 1);
    // @formatter:on
    private final String key;

    private final byte type;

    AccessoryAddressingEnum(String key, int type) {
        this.key = key;
        this.type = (byte) type;
    }

    public String getKey() {
        return key;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create an accessory addressing enum.
     * 
     * @param type
     *            numeric value of the accessory addressing enum
     * 
     * @return AccessoryAddressingEnum
     */
    public static AccessoryAddressingEnum valueOf(byte type) {
        AccessoryAddressingEnum result = null;

        for (AccessoryAddressingEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to an accessory addressing enum");
        }
        return result;
    }
}
