package org.bidib.jbidibc.core;

import java.util.Objects;

import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LcMacro {
    private static final Logger LOGGER = LoggerFactory.getLogger(LcMacro.class);

    private final byte[] content;

    private static final byte SYSTEM_FUNCTION = ByteUtils.getLowByte(255);

    // TODO think about changing the param outputType to a new MacroOutputType because macro supports system functions
    // with macro level 2
    public LcMacro(byte[] content) {
        this.content = content;
    }

    public LcMacro(byte macroNumber, byte stepNumber, byte delay, byte outputType, byte portNumber, byte value) {
        content = new byte[] { macroNumber, stepNumber, delay, outputType, portNumber, value };
    }

    public LcMacro(byte macroNumber, byte stepNumber, byte delay, LcOutputType outputType, byte portNumber,
        BidibEnum status) {
        content =
            new byte[] { macroNumber, stepNumber, delay, outputType.getType(), portNumber,
                (status != null ? status.getType() : (byte) 0) };
    }

    public byte[] getContent() {
        return content;
    }

    public byte getMacroNumber() {
        return content[0];
    }

    public byte getStepNumber() {
        return content[1];
    }

    /**
     * @return the delay value
     */
    public byte getDelay() {
        return content[2];
    }

    public BidibPort getBidibPort() {
        return BidibPort.prepareBidibPort(content[3], content[4]);
    }

    public BidibEnum getStatus(LcOutputType outputType) {
        try {
            return MessageUtils.toPortStatus(outputType, content[5]);
        }
        catch (IllegalArgumentException ex) {
            LOGGER.warn("Convert value {} for outputType {} to port status failed.", ByteUtils.getInt(content[5]),
                outputType, ex);
            throw new InvalidConfigurationException("No port status available for output type: " + outputType);
        }
    }

    public byte getPortValue(LcOutputType outputType) {
        return MessageUtils.getPortValue(outputType, content[5]);
    }

    public boolean isSystemFunction() {
        return (getDelay() == LcMacro.SYSTEM_FUNCTION);
    }

    /**
     * @return the type of the system function
     */
    public LcOutputType getSystemFunctionType() {
        return LcOutputType.valueOf(content[3]);
    }

    /**
     * @return the value of the system function
     */
    public byte getSystemFunctionValue() {
        return content[4];
    }

    /**
     * @return the extended value of the system function
     */
    public byte getSystemFunctionExtValue() {
        return content[5];
    }

    @Override
    public int hashCode() {
        return content.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof LcMacro) {
            LcMacro other = (LcMacro) obj;
            return Objects.deepEquals(content, other.content);
        }

        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb
            .append("[macroNumber=").append(ByteUtils.getInt(getMacroNumber())).append(",stepNumber=")
            .append(ByteUtils.getInt(getStepNumber())).append(",delay=").append(ByteUtils.getInt(getDelay()))
            .append(",bidibPort=").append(getBidibPort()).append(",portstatus=").append(ByteUtils.getInt(content[5]))
            .append("]");
        return sb.toString();
    }
}
