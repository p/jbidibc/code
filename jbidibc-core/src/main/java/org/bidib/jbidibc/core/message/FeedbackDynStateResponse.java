package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Signal the detection of a dynamic state from loco in the specified section.
 */
public class FeedbackDynStateResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_BM_DYN_STATE;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackDynStateResponse.class);

    FeedbackDynStateResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 5) {
            throw new ProtocolException("No valid MSG_BM_DYN_STATE received.");
        }
    }

    public FeedbackDynStateResponse(byte[] addr, int num, int detectorNumber, AddressData addressData, byte dynNumber,
        byte dynValue) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_DYN_STATE,
            prepareDynamicStateData(detectorNumber, addressData, dynNumber, dynValue));
    }

    public String getName() {
        return "MSG_BM_DYN_STATE";
    }

    public int getDetectorNumber() {
        return ByteUtils.getInt(getData()[0], 0x7F);
    }

    private static byte[] prepareDynamicStateData(
        int detectorNumber, AddressData addressData, byte dynNumber, byte dynValue) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteUtils.getLowByte(detectorNumber));

        addressData.writeToStream(out);

        out.write(dynNumber);
        out.write(dynValue);

        return out.toByteArray();
    }

    public AddressData getAddress() {
        byte[] data = getData();
        int index = 1;

        byte lowByte = data[index++];
        byte highByte = data[index];
        int address = ByteUtils.getWord(lowByte, (byte) (highByte & 0x3F));
        LOGGER.debug("Return the address of MSG_BM_DYN_STATE: {}", address);
        return new AddressData(address, AddressTypeEnum.valueOf((byte) ((highByte & 0xC0) >> 6)));
    }

    public int getDynNumber() {
        return ByteUtils.getInt(getData()[3], 0xFF);
    }

    public int getDynValue() {

        // if DYN_NUM == 6 we need to publish a INT16 value
        if (getDynNumber() == 6 && getData().length >= 6) {

            return ByteUtils.getInt(getData()[4], getData()[5]);
        }

        return ByteUtils.getInt(getData()[4], 0xFF);
    }

    public Integer getTimestamp() {
        if (getDynNumber() == 6 && getData().length >= 8) {
            return ByteUtils.getWORD(getData(), 6);
        }
        return null;
    }
}
