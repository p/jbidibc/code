package org.bidib.jbidibc.core;

import java.io.Serializable;
import java.util.Collection;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Feature implements Serializable, Comparable<Feature> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Feature.class);

    private int type;

    private int value;

    public Feature() {
        type = -1;
    }

    public Feature(int type, int value) {
        this.type = type;
        this.value = value;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getFeatureName() {
        try {
            return FeatureEnum.valueOf(ByteUtils.getLowByte(type)).name();
        }
        catch (IllegalArgumentException ex) {
            return String.valueOf(type);
        }
    }

    public static String getFeatureName(byte type) {
        try {
            return FeatureEnum.valueOf(type).name();
        }
        catch (IllegalArgumentException ex) {
            return String.valueOf(type);
        }
    }

    public FeatureEnum getFeatureEnum() {
        try {
            return FeatureEnum.valueOf(ByteUtils.getLowByte(type));
        }
        catch (IllegalArgumentException ex) {
            return null;
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isRequestedFeature(int requestedFeature) {
        return getType() == requestedFeature;
    }

    public boolean isRequestedFeature(FeatureEnum requestedFeature) {
        return ByteUtils.getLowByte(getType()) == requestedFeature.getType();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Feature) {
            return getType() == ((Feature) obj).getType();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getType();
    }

    @Override
    public int compareTo(Feature other) {
        return ((getType() > other.getType()) ? 1 : (getType() < other.getType()) ? -1 : 0);
    }

    public String toString() {
        return getClass().getSimpleName() + "[type=" + type + ",value=" + value + "]";
    }

    public static Feature valueOf(String key, int value) {
        Feature result = null;

        FeatureEnum featureEnum = null;
        for (FeatureEnum e : FeatureEnum.values()) {
            if (e.name().equals(key)) {
                featureEnum = e;
                break;
            }
        }

        if (featureEnum != null) {
            result = new Feature(ByteUtils.getInt(featureEnum.getType()), value);
        }
        else {
            LOGGER.warn("cannot map the key '" + key + "' to a known feature, use key direct to create a feature.");
            result = new Feature(Integer.valueOf(key), value);
        }
        return result;
    }

    /**
     * Find a feature in the provided list by the feature id.
     * 
     * @param features
     *            the list of features
     * @param requestedFeatureId
     *            the requested feature id
     * @return the feature of <code>null</code> if the feature is not in the provided list
     */
    public static Feature findFeature(final Collection<Feature> features, final int requestedFeatureId) {

        Feature feature = IterableUtils.find(features, new Predicate<Feature>() {
            @Override
            public boolean evaluate(Feature feature) {
                return feature.isRequestedFeature(requestedFeatureId);
            }
        });
        return feature;
    }

    public static Boolean getBooleanFeatureValue(final Collection<Feature> features, final int requestedFeatureId) {
        Boolean result = null;
        Feature feature = Feature.findFeature(features, requestedFeatureId);
        if (feature != null) {
            result = feature.getValue() > 0;
        }
        return result;
    }

    public static Boolean getBooleanFeatureValue(Feature feature) {
        Boolean result = null;
        if (feature != null) {
            result = feature.getValue() > 0;
        }
        return result;
    }

    public static int getIntFeatureValue(final Collection<Feature> features, final int requestedFeatureId) {
        int result = 0;
        Feature feature = Feature.findFeature(features, requestedFeatureId);
        if (feature != null) {
            result = feature.getValue();
        }
        return result;
    }

    /**
     * Get the feature number by the name of the feature.
     * 
     * @param name
     *            the name
     * @return the feature number
     */
    public static Integer getFeatureNumberByName(String name) {
        Integer result = null;

        FeatureEnum featureEnum = null;
        for (FeatureEnum e : FeatureEnum.values()) {
            if (e.name().equals(name)) {
                featureEnum = e;
                break;
            }
        }

        if (featureEnum != null) {
            result = ByteUtils.getInt(featureEnum.getType());
        }
        return result;
    }
}
