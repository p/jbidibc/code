package org.bidib.jbidibc.core;

import org.bidib.jbidibc.core.utils.AccessoryStateUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class AccessoryState {
    private final byte accessoryNumber;

    private final int aspect;

    private final byte total;

    private final byte execute;

    private final byte wait;

    public AccessoryState(byte accessoryNumber, byte aspect, byte total, byte execute, byte wait) {
        this.accessoryNumber = accessoryNumber;
        this.aspect = ByteUtils.getInt(aspect);
        this.total = total;
        this.execute = execute;
        this.wait = wait;
    }

    public byte getAccessoryNumber() {
        return accessoryNumber;
    }

    public int getAspect() {
        return aspect;
    }

    public byte getTotal() {
        return total;
    }

    public byte getExecute() {
        return execute;
    }

    /**
     * @return the remaining time in milliseconds
     */
    public int getWait() {

        int remainingTime = 0;
        switch (wait & 0x80) {
            case 0x00: // 100ms
                remainingTime = (wait & 0x7F) * 100;
                break;
            default: // 1s
                remainingTime = (wait & 0x7F) * 1000;
                break;
        }

        return remainingTime;
    }

    public boolean hasError() {
        return (execute & 0x80) == 0x80;
    }

    public boolean hasMoreErrors() {
        if (hasError()) {
            return ((wait & 0x40) == 0x40);
        }
        return false;
    }

    public Integer getErrorCode() {
        if (hasError()) {

            return (wait & 0x3F);
        }
        // illegal
        return null;
    }

    public String getErrorInformation() {
        if (hasError()) {
            StringBuilder sb = new StringBuilder();
            // evaluate the error
            int errorVal = ByteUtils.getInt(wait);
            boolean moreErrorsAvailable = (errorVal & 0x40) == 0x40;
            sb.append("Error detected: ");

            switch (errorVal & 0x1F) {
                case 0x00: // no more pending errors
                    sb.append("no error (remaining)");
                    break;
                case 0x01:
                    sb.append("command was not executable / unknown command / unknown aspect.");
                    break;
                case 0x02:
                    sb.append("power consumption too high.");
                    break;
                case 0x03:
                    sb.append("power supply below limits, function not garanteed.");
                    break;
                case 0x04:
                    sb.append("fuse blown.");
                    break;
                case 0x05:
                    sb.append("temperature too high.");
                    break;
                case 0x06:
                    sb.append("feedback error / unwanted change of position.");
                    break;
                case 0x07:
                    sb.append("manual control (eg. with local button)");
                    break;
                case 0x08:
                    sb.append("bulb out of order.");
                    break;
                case 0x09:
                    sb.append("servo out of order.");
                    break;
                case 0x3F:
                    sb.append("internal error (eg. selftest, checksum error, ..).");
                    break;
                default:
                    sb.append("Unknown error: ").append(ByteUtils.byteToHex(errorVal));
                    break;
            }

            sb.append(", more errors pending: ").append(moreErrorsAvailable);
            return sb.toString();
        }
        else if (aspect == 0xFF) {
            return "Unknown aspect state. Check previous errors.";
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[ ");
        sb.append(getClass().getSimpleName());
        sb.append(", accessoryNumber: ").append(ByteUtils.getInt(accessoryNumber));
        sb.append(", aspect: ").append(aspect);
        sb.append(", total: ").append(ByteUtils.getInt(total));
        sb.append(", execute: ").append(ByteUtils.getInt(execute));

        boolean errorDetected = false;
        if (hasError()) {
            sb.append(" => Error detected.");
            errorDetected = true;
        }
        else if (aspect == 0xFF) {
            sb.append("=> Unknown aspect state. Check previous errors.");
        }
        else {
            sb.append("[");
            sb.append(AccessoryStateUtils.getOperationResult(execute));
            sb.append("]");
        }

        if (!errorDetected) {
            // calculate the real time ...
            int remainingTime = 0;
            switch (wait & 0x80) {
                case 0x00: // 100ms
                    remainingTime = (wait & 0x7F) * 100;
                    break;
                default: // 1s
                    remainingTime = (wait & 0x7F) * 1000;
                    break;
            }
            sb.append(", remaing wait time: ").append(remainingTime).append("ms");
        }
        else {
            // evaluate the error
            int errorVal = ByteUtils.getInt(wait);
            boolean moreErrorsAvailable = (errorVal & 0x40) == 0x40;
            sb.append(", more errors pending: ").append(moreErrorsAvailable).append(", error: ");

            switch (errorVal & 0x1F) {
                case 0x00: // no more pending errors
                    sb.append("no error (remaining)");
                    break;
                case 0x01:
                    sb.append("command was not executable / unknown command / unknown aspect.");
                    break;
                case 0x02:
                    sb.append("power consumption too high.");
                    break;
                case 0x03:
                    sb.append("power supply below limits, function not garanteed.");
                    break;
                case 0x04:
                    sb.append("fuse blown.");
                    break;
                case 0x05:
                    sb.append("temperature too high.");
                    break;
                case 0x06:
                    sb.append("feedback error / unwanted change of position.");
                    break;
                case 0x07:
                    sb.append("manual control (eg. with local button)");
                    break;
                case 0x08:
                    sb.append("bulb out of order.");
                    break;
                case 0x09:
                    sb.append("servo out of order.");
                    break;
                case 0x3F:
                    sb.append("internal error (eg. selftest, checksum error, ..).");
                    break;
                default:
                    sb.append("Unknown error: ").append(ByteUtils.byteToHex(errorVal));
                    break;
            }
        }
        sb.append("]");
        return sb.toString();
    }

}
