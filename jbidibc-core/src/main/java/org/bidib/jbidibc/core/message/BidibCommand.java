package org.bidib.jbidibc.core.message;

public interface BidibCommand {

    /**
     * @return the expected response types
     */
    Integer[] getExpectedResponseTypes();

    /**
     * Set the send message number.
     * 
     * @param sendMsgNum
     *            the send message number
     */
    void setSendMsgNum(int sendMsgNum);

    /**
     * Get the node address of the message.
     * 
     * @return the node address
     */
    byte[] getAddr();

    /**
     * Set the node address in the message.
     * 
     * @param addr
     *            the node address to set
     */
    void setAddr(byte[] addr);

    /**
     * Get the type of the message.
     * 
     * @return the type of the message
     */
    byte getType();

    /**
     * Get the message data.
     * 
     * @return the message data
     */
    byte[] getData();

    /**
     * @return the answerSize
     */
    int getAnswerSize();

    /**
     * @param answerSize
     *            the answerSize to set
     */
    void setAnswerSize(int answerSize);

    /**
     * Returns the raw content of the message.
     * 
     * @return the raw content
     */
    byte[] getContent();
}
