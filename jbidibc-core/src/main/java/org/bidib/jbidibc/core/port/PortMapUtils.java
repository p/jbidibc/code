package org.bidib.jbidibc.core.port;

import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;

public class PortMapUtils {

    private PortMapUtils() {

    }

    public static boolean supportsPortType(LcOutputType lcOutputType, Map<Byte, PortConfigValue<?>> portConfigX) {
        ReconfigPortConfigValue reconfigPortConfigValue =
            (ReconfigPortConfigValue) portConfigX.get(Byte.valueOf(BidibLibrary.BIDIB_PCFG_RECONFIG));
        if (reconfigPortConfigValue != null) {

            return supportsPortType(lcOutputType, reconfigPortConfigValue);
        }
        return false;
    }

    public static boolean supportsPortType(LcOutputType lcOutputType, ReconfigPortConfigValue reconfigPortConfigValue) {
        boolean supportsPortType = false;
        int portMap = reconfigPortConfigValue.getPortMap();
        switch (lcOutputType) {
            case SWITCHPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_SWITCH) & 0x01) == 0x01;
                break;
            case LIGHTPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_LIGHT) & 0x01) == 0x01;
                break;
            case SERVOPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_SERVO) & 0x01) == 0x01;
                break;
            case SOUNDPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_SOUND) & 0x01) == 0x01;
                break;
            case MOTORPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_MOTOR) & 0x01) == 0x01;
                break;
            case ANALOGPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_ANALOGOUT) & 0x01) == 0x01;
                break;
            case BACKLIGHTPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_BACKLIGHT) & 0x01) == 0x01;
                break;
            case SWITCHPAIRPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_SWITCHPAIR) & 0x01) == 0x01;
                break;
            case INPUTPORT:
                supportsPortType = ((portMap >> BidibLibrary.BIDIB_PORTTYPE_INPUT) & 0x01) == 0x01;
                break;
            default:
                break;
        }

        return supportsPortType;
    }

    /**
     * Verify if the port supports mapping to another port type. We assume that more than 1 bit set in the
     * <code>portMap</code> signals that the port is mappable.
     * 
     * @param reconfigPortConfigValue
     *            the reconfigPortConfigValue
     * @return port supports remapping to another port type
     */
    public static boolean supportsPortRemapping(ReconfigPortConfigValue reconfigPortConfigValue) {

        int portMap = reconfigPortConfigValue.getPortMap();

        return Integer.bitCount(portMap) > 1;
    }
}
