package org.bidib.jbidibc.decoder.version;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VersionMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(VersionMatcher.class);

    public static final String PATTERN_A = "^([0-9]{1,4})$";

    public static final String PATTERN_B = "^([0-9]\\.?){1,3}$";

    public static final String PATTERN_C = "^V([0-9]\\.?){1,2}(R[0-9]+)$";

    // V{0:M0x07}.{0:M0x78}R{1}

    public static class VersionCheck {
        private final String versionString;

        private final String pattern;

        public VersionCheck(String pattern, String versionString) {
            this.pattern = pattern;
            this.versionString = versionString;
        }

        public String check(Function<VersionCheck, String> fun) {
            return fun.apply(this);
        }
    }

    public static Function<VersionCheck, String> versionMatcher = versionCheck -> {
        Matcher m = Pattern.compile(versionCheck.pattern).matcher(versionCheck.versionString);
        if (m.matches()) {
            LOGGER.info("The pattern matches!");

            return m.group();
        }
        return null;
    };

    /**
     * Detect the concrete version based on the provided version pattern.
     * 
     * @param versionString
     *            the version string
     * @return the decoder version parser
     */
    public DecoderVersion detectVersion(String versionString) {

        VersionCheck versionCheck = new VersionCheck(VersionMatcher.PATTERN_A, versionString);
        String result = versionCheck.check(VersionMatcher.versionMatcher);

        if (result != null) {
            LOGGER.info("Found version of pattern A: {}", result);
            DecoderVersion version = new PatternAVersion(versionString);
            version.parse(result);
            return version;
        }

        versionCheck = new VersionCheck(VersionMatcher.PATTERN_B, versionString);
        result = versionCheck.check(VersionMatcher.versionMatcher);

        if (result != null) {
            LOGGER.info("Found version of pattern B: {}", result);
            DecoderVersion version = new PatternBVersion(versionString);
            version.parse(result);
            return version;
        }

        versionCheck = new VersionCheck(VersionMatcher.PATTERN_C, versionString);
        result = versionCheck.check(VersionMatcher.versionMatcher);

        if (result != null) {
            LOGGER.info("Found version of pattern C: {}", result);
            DecoderVersion version = new PatternCVersion(versionString);
            version.parse(result);
            return version;
        }
        return null;
    }

}
