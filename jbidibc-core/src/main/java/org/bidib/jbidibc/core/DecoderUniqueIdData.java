package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@code DecoderUniqueIdData} contains the uniqueId information about a RcPlus or M4 decoder, that identifies this
 * decoder.
 */
public class DecoderUniqueIdData {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderUniqueIdData.class);

    // manufacturer unique number
    private final long mun;

    // manufacturer ID (like DCC vendor ID)
    private final int mid;

    public DecoderUniqueIdData(long mun, int mid) {
        this.mun = mun;
        this.mid = mid;
    }

    public long getMun() {
        return mun;
    }

    public int getMid() {
        return mid;
    }

    public String toString() {
        return getClass().getSimpleName() + "[mun=" + String.format("0x%08X", mun) + ", mid=" + mid + " ("
            + String.format("0x%02X", mid) + ")]";
    }

    public void writeToStream(ByteArrayOutputStream out) {

        for (int index = 0; index < 4; index++) {
            byte val = (byte) ((int) (mun >> (index * 8)) & 0xFF);
            out.write(val);
        }

        out.write((byte) mid & 0xFF);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DecoderUniqueIdData) {
            DecoderUniqueIdData other = (DecoderUniqueIdData) obj;
            if (mun == other.mun && mid == other.mid) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hc = 17;
        int hashMultiplier = 59;
        hc = hc * hashMultiplier + (int) mun;
        hc = hc * hashMultiplier + mid;
        return hc;
    }

    /**
     * Create a DecoderUniqueIdData instance from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset to start
     * @return the DecoderUniqueIdData instance
     */
    public static DecoderUniqueIdData fromByteArray(byte[] data, int offset) {
        if (data.length < (offset + 5)) {
            LOGGER.warn("The size of the provided data does not meet the expected length.");
            throw new IllegalArgumentException(
                "The size of the provided data does not meet the expected length (>=5). Provided length: "
                    + data.length);
        }

        long mun = ByteUtils.getDWORD(data, offset);
        int mid = ByteUtils.getInt(data[offset + 4]);

        DecoderUniqueIdData uniqueIdData = new DecoderUniqueIdData(mun, mid);
        LOGGER.trace("Return the uniqueIdData: {}", uniqueIdData);
        return uniqueIdData;
    }
}
