package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;

public enum DirectionEnum {
    // @formatter:off
    FORWARD(0x80), BACKWARD(0x00);
    // @formatter:on

    private byte type;

    DirectionEnum(int value) {
        this.type = ByteUtils.getLowByte(value);
    }

    public byte getValue() {
        return type;
    }

    public static DirectionEnum valueOf(int type) {
        return valueOf(ByteUtils.getLowByte(type));
    }

    public static DirectionEnum valueOf(byte type) {
        DirectionEnum result = null;

        for (DirectionEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a direction enum");
        }
        return result;
    }
}
