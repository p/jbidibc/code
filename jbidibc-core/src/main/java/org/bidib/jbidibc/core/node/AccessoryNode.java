package org.bidib.jbidibc.core.node;

import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.LcMacroParaValue;
import org.bidib.jbidibc.core.enumeration.LcMacroOperationCode;
import org.bidib.jbidibc.core.enumeration.LcMacroState;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.exception.ProtocolInvalidParamException;
import org.bidib.jbidibc.core.exception.ProtocolNoAnswerException;
import org.bidib.jbidibc.core.message.AccessoryGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaResponse;
import org.bidib.jbidibc.core.message.AccessoryParaSetMessage;
import org.bidib.jbidibc.core.message.AccessorySetMessage;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.LcMacroGetMessage;
import org.bidib.jbidibc.core.message.LcMacroHandleMessage;
import org.bidib.jbidibc.core.message.LcMacroParaResponse;
import org.bidib.jbidibc.core.message.LcMacroParaSetMessage;
import org.bidib.jbidibc.core.message.LcMacroResponse;
import org.bidib.jbidibc.core.message.LcMacroSetMessage;
import org.bidib.jbidibc.core.message.LcMacroStateResponse;
import org.bidib.jbidibc.core.node.BidibNode.ProcessSendQueue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.CollectionUtils;
import org.bidib.jbidibc.core.utils.MacroUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessoryNode implements MacroNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryNode.class);

    private static final Integer RECEIVE_TIMEOUT = 300;

    private BidibNode delegate;

    /**
     * Creates a new instance of {@code AccessoryNode} that encapsulates the provided delegate.
     * 
     * @param delegate
     *            the encapsulated {@code BidibNode}
     */
    AccessoryNode(BidibNode delegate) {
        this.delegate = delegate;
    }

    /**
     * @return the encapsulated {@code BidibNode} instance.
     */
    public BidibNode getBidibNode() {
        return delegate;
    }

    /**
     * Get the accessory parameter of the specified accessory.
     * 
     * @param accessoryNumber
     *            the number of the accessory
     * @param parameter
     *            the parameter to retrieve
     * @return the parameter values of the accessory
     * @throws ProtocolException
     */
    public byte[] getAccessoryParameter(int accessoryNumber, int parameter) throws ProtocolException {

        byte[] result = null;
        BidibMessage response =
            delegate
                .send(new AccessoryParaGetMessage(accessoryNumber, parameter), RECEIVE_TIMEOUT, true,
                    AccessoryParaResponse.TYPE);

        if (response instanceof AccessoryParaResponse) {

            if (((AccessoryParaResponse) response).getParameter() == BidibLibrary.BIDIB_ACCESSORY_PARA_NOTEXIST) {
                LOGGER.warn("The requested accessory param does not exist: {}", parameter);

                throw new ProtocolInvalidParamException("The requested accessory param does not exist!", parameter);
            }

            result = ((AccessoryParaResponse) response).getValue();
        }
        return result;
    }

    public byte[] setAccessoryParameter(int accessoryNumber, int parameter, byte[] value) throws ProtocolException {
        byte[] result = null;
        BidibMessage response =
            delegate
                .send(new AccessoryParaSetMessage(accessoryNumber, parameter, value), RECEIVE_TIMEOUT, true,
                    AccessoryParaResponse.TYPE);

        if (response instanceof AccessoryParaResponse) {
            result = ((AccessoryParaResponse) response).getValue();
        }
        return result;
    }

    /**
     * Get the accessory state of the specified accessory.
     * 
     * @param accessoryNumber
     *            the number of the accessory
     * @throws ProtocolException
     */
    public void getAccessoryState(int... accessoryNumber) throws ProtocolException {
        // response is signaled asynchronously
        if (accessoryNumber.length > 1) {
            int windowSize = BidibNode.BULK_WINDOW_SIZE;
            List<BidibCommand> messages = new LinkedList<>();
            for (int i = 0; i < accessoryNumber.length; i++) {
                messages.add(delegate.getRequestFactory().createAccessoryGet(accessoryNumber[i]));
            }
            LOGGER
                .info("Get the accessory states with bulk messages, windowSize: {}, accessoryNumber: {}",
                    new Object[] { windowSize, accessoryNumber });
            delegate.sendBulk(windowSize, messages, true, ProcessSendQueue.enabled);
        }
        else {
            LOGGER.info("Get the accessory states with single message, accessoryNumber: {}", accessoryNumber[0]);
            delegate.sendNoWait(new AccessoryGetMessage(accessoryNumber[0]));
        }
    }

    /**
     * Set the state of the specified accessory.
     * 
     * @param accessoryNumber
     *            the number of the accessory
     * @param aspect
     *            the aspect to set
     * @throws ProtocolException
     */
    public void setAccessoryState(int accessoryNumber, int aspect) throws ProtocolException {
        // response is signaled asynchronously
        delegate.sendNoWait(new AccessorySetMessage(accessoryNumber, aspect));
    }

    /**
     * Send the accessory state acknowledgement message for the specified accessory.
     * 
     * @param accessoryState
     *            the accessory state
     * @throws ProtocolException
     */
    public void acknowledgeAccessoryNotify(AccessoryState accessoryState) throws ProtocolException {
        // TODO check if we must handle this differently ... currently auto-acknowledge new state
        LOGGER.info("Accessory change notification was received: {}", accessoryState);
        // if (!AccessoryStateUtils.hasError(accessoryState.getExecute())) {
        int accessoryNumber = ByteUtils.getInt(accessoryState.getAccessoryNumber());
        int aspect = accessoryState.getAspect();
        LOGGER
            .info("Acknowledge the accessory state change for accessory number: {}, aspect: {}", accessoryNumber,
                aspect);

        // send acknowledge
        // TODO check how this works ... currently does not work correct ...
        // setAccessoryState(accessoryNumber, aspect);
        // sendQueue.add(AccessoryStateMessage(detectorNumber));

        // get the errors, see 4.6.4. Uplink: Messages for accessory functions
        // TODO verify what happens exactly before enable this ...
        getAccessoryState(accessoryNumber);
        // }
        // else {
        // LOGGER.warn("An accessory error was detected: {}", accessoryState);
        // }
    }

    /**
     * Get the macro parameter.
     * 
     * @param macroNumber
     *            the macro numbr
     * @param parameter
     *            the parameter number
     * @return the parameter value
     * @throws ProtocolException
     */
    public LcMacroParaValue getMacroParameter(int macroNumber, int parameter) throws ProtocolException {

        LcMacroParaValue result = null;
        BidibMessage response =
            delegate
                .send(delegate.getRequestFactory().createLcMacroParaGet(macroNumber, parameter), RECEIVE_TIMEOUT, true,
                    LcMacroParaResponse.TYPE);

        if (response instanceof LcMacroParaResponse) {
            result = ((LcMacroParaResponse) response).getLcMacroParaValue();
        }
        else {
            LOGGER
                .warn("No response received for LcMacroParaGetMessage, macroNumber: {}, parameter: {}", macroNumber,
                    parameter);
            throw new ProtocolNoAnswerException(String
                .format("No response received for LcMacroParaGetMessage, macroNumber: %d, parameter: %d", macroNumber,
                    parameter));
        }
        return result;
    }

    /**
     * Get the macro parameter.
     * 
     * @param macroNumber
     *            the macro number
     * @param parameters
     *            the parameter numbers
     * @return the list of parameter values
     * @throws ProtocolException
     */
    public List<LcMacroParaValue> getMacroParameters(int macroNumber, int... parameters) throws ProtocolException {
        List<LcMacroParaValue> results = new LinkedList<>();
        List<BidibCommand> messages = new LinkedList<>();
        for (int parameter : parameters) {

            messages.add(delegate.getRequestFactory().createLcMacroParaGet(macroNumber, parameter));
        }

        int windowSize = BidibNode.BULK_WINDOW_SIZE;

        // wait for answers from node
        List<BidibMessage> responses = delegate.sendBulk(windowSize, messages, true, ProcessSendQueue.enabled);

        if (CollectionUtils.hasElements(responses)) {
            int index = 0;
            for (BidibMessage response : responses) {
                if (response instanceof LcMacroParaResponse) {
                    LcMacroParaValue result = ((LcMacroParaResponse) response).getLcMacroParaValue();
                    results.add(result);
                }
                else {
                    LOGGER
                        .warn("No response received for LcMacroParaGetMessage, macroNumber: {}, parameter: {}",
                            macroNumber, parameters[index]);

                    LcMacroParaValue result = new LcMacroParaValue(macroNumber, parameters[index], null);
                    results.add(result);
                    throw new ProtocolNoAnswerException(String
                        .format("No response received for LcMacroParaGetMessage, macroNumber: %d, parameter: %d",
                            macroNumber, parameters[index]));
                }

                index++;
            }
        }
        return results;
    }

    /**
     * Get the macro step with the specified step number.
     * 
     * @param macroNumber
     *            the number of the macro
     * @param stepNumber
     *            the number of the step
     * @return the macro step
     * @throws ProtocolException
     */
    public LcMacro getMacroStep(int macroNumber, int stepNumber) throws ProtocolException {
        LOGGER.info("Get the macro step, macroNumber: {}, stepNumber: {}", macroNumber, stepNumber);

        LcMacro result = null;
        BidibMessage response =
            delegate.send(new LcMacroGetMessage(macroNumber, stepNumber), RECEIVE_TIMEOUT, true, LcMacroResponse.TYPE);

        if (response instanceof LcMacroResponse) {
            result = MacroUtils.getMacro(response.getData());
            LOGGER.info("The returned macro step is: {}", result);
        }
        return result;
    }

    public LcMacroState handleMacro(int macroNumber, LcMacroOperationCode macroOperationCode) throws ProtocolException {
        LOGGER.debug("handle macro, macroNumber: {}, macroOperationCode: {}", macroNumber, macroOperationCode);

        Integer receiveTimeout = RECEIVE_TIMEOUT;
        if (LcMacroOperationCode.SAVE.equals(macroOperationCode)
            || LcMacroOperationCode.RESTORE.equals(macroOperationCode)
            || LcMacroOperationCode.DELETE.equals(macroOperationCode)) {
            receiveTimeout = 2 * RECEIVE_TIMEOUT;
        }
        BidibMessage response =
            delegate
                .send(new LcMacroHandleMessage(macroNumber, macroOperationCode), receiveTimeout, true,
                    LcMacroStateResponse.TYPE);

        LcMacroState result = null;
        if (response instanceof LcMacroStateResponse) {
            result = ((LcMacroStateResponse) response).getMacroState();
            LOGGER
                .debug("handle macro returned: {}, response: {}", result,
                    ((LcMacroStateResponse) response).toExtendedString());
        }
        return result;
    }

    @Override
    public LcMacro setMacro(LcMacro macroStep) throws ProtocolException {
        LOGGER.info("Set the macro point: {}", macroStep);

        BidibMessage response =
            delegate.send(new LcMacroSetMessage(macroStep), RECEIVE_TIMEOUT, true, LcMacroResponse.TYPE);

        LcMacro result = null;
        if (response instanceof LcMacroResponse) {
            result = MacroUtils.getMacro(response.getData());
            LOGGER.debug("Set macro returned: {}", result);
        }
        return result;
    }

    public void setMacroParameter(int macroNumber, int parameter, byte... value) throws ProtocolException {
        LOGGER
            .debug("Set macro parameter, macroNumber: {}, parameter: {}, value: {}",
                new Object[] { macroNumber, parameter, value });

        BidibMessage response =
            delegate
                .send(new LcMacroParaSetMessage(macroNumber, parameter, value), RECEIVE_TIMEOUT, true,
                    LcMacroParaResponse.TYPE);
        if (response instanceof LcMacroParaResponse) {
            int result = ((LcMacroParaResponse) response).getMacroNumber();
            LOGGER.debug("Set macro parameter returned macronumber: {}", result);
        }
    }
}
