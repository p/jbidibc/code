package org.bidib.jbidibc.net;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetMessageReceiver extends MessageReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetMessageReceiver.class);

    private ByteArrayOutputStream output = new ByteArrayOutputStream(2048);

    /**
     * Create a new instance of MessageReceiver.
     * 
     * @param nodeRegistry
     *            the node registry
     * @param checkCRC
     *            flag if we must check the CRC
     */
    public NetMessageReceiver(NodeRegistry nodeFactory, boolean checkCRC) {

        super(nodeFactory, new BidibResponseFactory(), checkCRC);
    }

    @Override
    public void enable() {
        LOGGER.info("enable is called.");
        // escapeHot.set(false);

        MSG_RAW_LOGGER.info("++++ Enable the message receiver.");

        try {
            output.reset();
        }
        catch (Exception ex) {
            LOGGER.warn("Reset buffered received data failed.", ex);
        }

        super.enable();
    }

    @Override
    public void disable() {
        LOGGER.info("Disable is called.");
        super.disable();

        MSG_RAW_LOGGER.info("++++ Disable the message receiver.");

        // escapeHot.set(false);
    }

    /**
     * Receive messages from the configured port
     * 
     * @param data
     *            the received data
     */
    @Override
    public void receive(final ByteArrayOutputStream data) {

        if (!running.get()) {
            LOGGER.info("The receiver is not running. Skip processing of messages.");
            try {
                byte[] rawdata = data.toByteArray();
                LOGGER.info("Receiver is stopped, number of bytes read: {}, buffer: {}", rawdata.length,
                    ByteUtils.bytesToHex(rawdata));
            }
            catch (Exception ex) {
                LOGGER.warn("Read data from input stream to buffer failed.", ex);
            }
            return;
        }

        MSG_RAW_LOGGER.info("<<<< start parse input: {}", ByteUtils.bytesToHex(data));

        try {
            parseInput(data);
        }
        catch (Exception e) {
            LOGGER.warn("Exception detected in message receiver!", e);

            throw new RuntimeException(e);
        }
        finally {
            MSG_RAW_LOGGER.info("<<<< finished parse input");
        }
    }

    /**
     * Parse the received data to process the received bidib packets.
     * 
     * @param input
     *            the received data
     * @throws ProtocolException
     */
    protected void parseInput(final ByteArrayOutputStream input) throws ProtocolException {

        MSG_RAW_LOGGER.info("<<<< len: {}, data: {}", input.size(), ByteUtils.bytesToHex(input));

        // if a CRC error is detected in splitMessages the reading loop will terminate ...
        try {
            processMessages(input);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Process messages failed.", ex);
        }
    }
}
