package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.RcPlusDecoderAnswerData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodesAck;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Response from command station with the pom acknowledge state
 */
public class CommandStationRcPlusAcknowledgeResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_CS_RCPLUS_ACK;

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationRcPlusAcknowledgeResponse.class);

    CommandStationRcPlusAcknowledgeResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 1) {
            throw new ProtocolException("No valid MSG_CS_RCPLUS_ACK received.");
        }

        LOGGER.debug("Received MSG_CS_RCPLUS_ACK response, opCode: {}", getOpCode());
    }

    public CommandStationRcPlusAcknowledgeResponse(byte[] addr, int num, TidData tid) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_CS_RCPLUS_ACK, prepareData(tid));
    }

    public CommandStationRcPlusAcknowledgeResponse(byte[] addr, int num, RcPlusPhase phase, int pingOnceData)
        throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_CS_RCPLUS_ACK, prepareData(phase, pingOnceData));
    }

    public String getName() {
        return "MSG_CS_RCPLUS_ACK";
    }

    private static byte[] prepareData(TidData tid) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        out.write(RcPlusOpCodesAck.RC_TID.getType());

        // write tid
        tid.writeToStream(out);

        return out.toByteArray();
    }

    private static byte[] prepareData(RcPlusPhase phase, int pingOnceData) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        out
            .write(RcPlusPhase.P0 == phase ? RcPlusOpCodesAck.RC_PING_ONCE_P0.getType() : RcPlusOpCodesAck.RC_PING_ONCE_P0
                .getType());

        // write ping once data
        out.write(ByteUtils.getLowByte(pingOnceData));

        return out.toByteArray();
    }

    /**
     * @return the opCode of the message
     */
    public RcPlusOpCodesAck getOpCode() {
        return RcPlusOpCodesAck.valueOf(getData()[0]);
    }

    /**
     * @return the tid data
     */
    public TidData getTid() {
        byte[] data = getData();

        if (!RcPlusOpCodesAck.RC_TID.equals(getOpCode())) {
            LOGGER.warn("The current opCode is not RC_TID!");
            return null;
        }
        return TidData.fromByteArray(data, 1);
    }

    /**
     * @return the rcPlus acknowledge state
     */
    public RcPlusAcknowledge getAcknState() {
        RcPlusAcknowledge rcPlusAcknowledge = RcPlusAcknowledge.valueOf(getData()[1]);
        LOGGER.info("Return the current RcPlusAcknowledge: {}", rcPlusAcknowledge);
        return rcPlusAcknowledge;
    }

    public int getPingInterval() {
        if (!RcPlusOpCodesAck.RC_PING.equals(getOpCode())) {
            LOGGER.warn("The current opCode is not RC_PING!");
            return 0;
        }

        int pingInterval = ByteUtils.getInt(getData()[1]);
        LOGGER.info("Return the current pingInterval: {}", pingInterval);
        return pingInterval;
    }

    public RcPlusDecoderAnswerData getRcPlusDecoderAnswer() {

        byte[] data = getData();
        RcPlusDecoderAnswerData answerData = null;
        switch (getOpCode()) {
            case RC_BIND:
            case RC_FIND_P0:
            case RC_FIND_P1:
                answerData = RcPlusDecoderAnswerData.fromByteArray(data, 1);
                break;
            default:
                LOGGER.warn("The current opCode is not RC_BIND or RC_FIND_P0/P1!");
                break;
        }
        return answerData;
    }
}
