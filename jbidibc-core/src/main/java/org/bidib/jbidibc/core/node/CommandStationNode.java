package org.bidib.jbidibc.core.node;

import java.util.Arrays;
import java.util.BitSet;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.DecoderIdAddressData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.RcPlusBindData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.ActivateCoilEnum;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.enumeration.CommandStationPt;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;
import org.bidib.jbidibc.core.enumeration.DirectionEnum;
import org.bidib.jbidibc.core.enumeration.DriveAcknowledge;
import org.bidib.jbidibc.core.enumeration.M4OpCodes;
import org.bidib.jbidibc.core.enumeration.PomAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodes;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.jbidibc.core.enumeration.SpeedStepsEnum;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.CommandStationBinaryStateMessage;
import org.bidib.jbidibc.core.message.CommandStationDriveAcknowledgeResponse;
import org.bidib.jbidibc.core.message.CommandStationDriveMessage;
import org.bidib.jbidibc.core.message.CommandStationM4Message;
import org.bidib.jbidibc.core.message.CommandStationPomAcknowledgeResponse;
import org.bidib.jbidibc.core.message.CommandStationPomMessage;
import org.bidib.jbidibc.core.message.CommandStationProgMessage;
import org.bidib.jbidibc.core.message.CommandStationQueryMessage;
import org.bidib.jbidibc.core.message.CommandStationRcPlusMessage;
import org.bidib.jbidibc.core.message.CommandStationSetStateMessage;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandStationNode {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationNode.class);

    private BidibNode delegate;

    private boolean m4Enabled = true;

    CommandStationNode(BidibNode delegate) {
        this.delegate = delegate;
    }

    public DriveAcknowledge setBinaryState(int address, int state, boolean value) throws ProtocolException {

        LOGGER.info("Set binary state, address: {}, state: {}, value: {}", address, state, value);

        DriveAcknowledge result = null;
        BidibMessage response =
            delegate.send(new CommandStationBinaryStateMessage(address, state, value), delegate.getResponseTimeout(),
                true, CommandStationDriveAcknowledgeResponse.TYPE);

        if (response instanceof CommandStationDriveAcknowledgeResponse) {
            result = ((CommandStationDriveAcknowledgeResponse) response).getState();
        }
        return result;
    }

    public DriveAcknowledge setDrive(
        int address, SpeedStepsEnum speedSteps, Integer speed, DirectionEnum direction, BitSet activeFunctions,
        BitSet functions) throws ProtocolException {

        LOGGER.debug("set drive, address: {}, speed: {}", address, speed);

        DriveAcknowledge result = null;
        BidibMessage response =
            delegate.send(new CommandStationDriveMessage(address, speedSteps, speed, direction, activeFunctions,
                functions), null, true, CommandStationDriveAcknowledgeResponse.TYPE);

        if (response instanceof CommandStationDriveAcknowledgeResponse) {
            result = ((CommandStationDriveAcknowledgeResponse) response).getState();
        }
        return result;
    }

    /**
     * Clear loco from command station.
     * 
     * @param address
     *            the address
     * @return the acknowledge
     * @throws ProtocolException
     */
    public DriveAcknowledge clearLoco(int address) throws ProtocolException {

        LOGGER.info("Clear loco from command station, address: {}", address);

        DriveAcknowledge result = null;
        BidibMessage response =
            delegate.send(new CommandStationDriveMessage(address, SpeedStepsEnum.DCC14, null, DirectionEnum.BACKWARD,
                null, null), null, true, CommandStationDriveAcknowledgeResponse.TYPE);

        if (response instanceof CommandStationDriveAcknowledgeResponse) {
            result = ((CommandStationDriveAcknowledgeResponse) response).getState();
        }
        return result;
    }

    public CommandStationState setState(CommandStationState commandStationState) throws ProtocolException {

        LOGGER.debug("set state, commandStationState: {}", commandStationState);

        CommandStationState resultCommandStationState = CommandStationState.OFF;

        final Object commandStationStateFeedbackLock = new Object();
        final CommandStationState[] resultHolder = new CommandStationState[1];

        final byte[] nodeAddr = delegate.getAddr();
        // create a temporary message listener
        MessageListener messageListener = new DefaultMessageListener() {
            @Override
            public void csState(byte[] address, CommandStationState state) {
                LOGGER.debug("+++ Command station state was signalled: {}, address: {}", state, address);

                if (Arrays.equals(nodeAddr, address)) {
                    resultHolder[0] = state;

                    synchronized (commandStationStateFeedbackLock) {
                        commandStationStateFeedbackLock.notify();
                    }
                }
                else {
                    LOGGER.info("Received command station state from another node.");
                }
            }
        };

        try {
            // add the message listener to the node
            addMessageListener(messageListener);

            synchronized (commandStationStateFeedbackLock) {
                // send the query command station state command
                delegate.sendNoWait(new CommandStationSetStateMessage(commandStationState));

                LOGGER.debug("+++ The command station set state message was sent, wait for response.");
                // wait for the response
                try {
                    commandStationStateFeedbackLock.wait(2000L);
                }
                catch (InterruptedException ie) {
                    LOGGER.warn("Wait for command station state was interrupted.", ie);
                }

                LOGGER.debug("+++ After wait for response.");
            }

            resultCommandStationState = resultHolder[0];
            LOGGER.debug("+++ Return the current command station state: {}", resultCommandStationState);
        }
        finally {
            // remove the temporary message listener
            removeMessageListener(messageListener);
        }

        return resultCommandStationState;
    }

    public CommandStationState queryState() throws ProtocolException {
        return queryState(true);
    }

    public CommandStationState queryState(boolean waitForResult) throws ProtocolException {

        LOGGER.debug("Query the state of the commandStation, waitForResult: {}", waitForResult);

        CommandStationState commandStationState = CommandStationState.OFF;

        final Object commandStationStateFeedbackLock = new Object();
        final CommandStationState[] resultHolder = new CommandStationState[1];

        final byte[] nodeAddr = delegate.getAddr();

        MessageListener messageListener = null;
        if (waitForResult) {
            // create a temporary message listener
            messageListener = new DefaultMessageListener() {
                @Override
                public void csState(byte[] address, CommandStationState state) {
                    LOGGER.info("+++ Command station state was signalled: {}, address: {}", state, address);

                    if (Arrays.equals(nodeAddr, address)) {
                        resultHolder[0] = state;

                        synchronized (commandStationStateFeedbackLock) {
                            commandStationStateFeedbackLock.notify();
                        }
                    }
                    else {
                        LOGGER.info("Received command station state from another node.");
                    }
                }
            };
        }

        if (messageListener != null) {

            try {
                // add the message listener to the node
                addMessageListener(messageListener);

                synchronized (commandStationStateFeedbackLock) {
                    // send the query command station state command
                    delegate.sendNoWait(new CommandStationSetStateMessage(CommandStationState.QUERY));

                    LOGGER.info("+++ The command station state query message was sent, wait for response.");
                    // wait for the response
                    try {
                        commandStationStateFeedbackLock.wait(2000L);
                    }
                    catch (InterruptedException ie) {
                        LOGGER.warn("Wait for command station state was interrupted.", ie);
                    }

                    LOGGER.info("+++ After wait for response.");
                }

                commandStationState = resultHolder[0];
                LOGGER.info("+++ Return the current command station state: {}", commandStationState);
            }
            finally {
                // remove the temporary message listener
                removeMessageListener(messageListener);
            }
        }
        else {
            // send the query command station state command
            delegate.sendNoWait(new CommandStationSetStateMessage(CommandStationState.QUERY));
        }

        return commandStationState;
    }

    public AccessoryAcknowledge setAccessory(
        int address, AddressTypeEnum addressType, TimingControlEnum timingControl, ActivateCoilEnum activateCoil,
        int aspect, TimeBaseUnitEnum timeBaseUnit, int time) throws ProtocolException {

        LOGGER.debug("Set accessory, address: {}", address);

        final Object commandStationAccessoryFeedbackLock = new Object();
        final AccessoryAcknowledge[] resultHolder = new AccessoryAcknowledge[1];

        final int requestedDecoderAddress = address;
        // create a temporary message listener
        MessageListener messageListener = new DefaultMessageListener() {
            @Override
            public void csAccessoryAcknowledge(byte[] address, int decoderAddress, AccessoryAcknowledge acknowledge) {
                LOGGER.info("+++ CS accessory ackn was signalled, decoderAddress: {}, acknowledge: {}", decoderAddress,
                    acknowledge);

                if (requestedDecoderAddress != decoderAddress) {
                    LOGGER.info("Acknowledge from different decoder: {}, requested: {}", decoderAddress,
                        requestedDecoderAddress);
                }

                resultHolder[0] = acknowledge;

                synchronized (commandStationAccessoryFeedbackLock) {
                    commandStationAccessoryFeedbackLock.notify();
                }
            }
        };

        AccessoryAcknowledge accessoryAcknowledge = null;
        try {
            // add the message listener to the node
            addMessageListener(messageListener);

            synchronized (commandStationAccessoryFeedbackLock) {
                // send the query command station accessory command
                BidibCommand message =
                    delegate.getRequestFactory().createCommandStationAccessory(address, addressType, timingControl,
                        activateCoil, aspect, timeBaseUnit, time);
                delegate.sendNoWait(message);

                LOGGER.info("+++ The command station accessory message was sent, wait for response.");
                // wait for the response
                try {
                    commandStationAccessoryFeedbackLock.wait(2000L);
                }
                catch (InterruptedException ie) {
                    LOGGER.warn("Wait for command station accessory ackn was interrupted.", ie);
                }

                LOGGER.info("+++ After wait for response.");
            }

            accessoryAcknowledge = resultHolder[0];
            LOGGER.info("+++ Return the current command station accessory ackn: {}", accessoryAcknowledge);
        }
        finally {
            // remove the temporary message listener
            removeMessageListener(messageListener);
        }

        return accessoryAcknowledge;
    }

    public DriveAcknowledge releaseLoco(int address) throws ProtocolException {

        LOGGER.debug("Release loco, address: {}", address);

        DriveAcknowledge result = null;
        BidibMessage response =
            delegate.send(new CommandStationDriveMessage(address, SpeedStepsEnum.DCC128, null, DirectionEnum.BACKWARD,
                null, null), null, true, CommandStationDriveAcknowledgeResponse.TYPE);

        if (response instanceof CommandStationDriveAcknowledgeResponse) {
            result = ((CommandStationDriveAcknowledgeResponse) response).getState();
        }
        return result;
    }

    public PomAcknowledge readPom(AddressData locoAddress, CommandStationPom opCode, int cvNumber)
        throws ProtocolException {
        byte[] data = { 0 };
        BidibMessage response =
            delegate.send(new CommandStationPomMessage(locoAddress, opCode, cvNumber, data), null, true,
                CommandStationPomAcknowledgeResponse.TYPE);
        PomAcknowledge result = null;
        if (response instanceof CommandStationPomAcknowledgeResponse) {
            result = ((CommandStationPomAcknowledgeResponse) response).getAcknState();
        }
        LOGGER.debug("Return the pomAcknowledge: {}", result);
        return result;
    }

    public PomAcknowledge readPom(DecoderIdAddressData locoAddress, CommandStationPom opCode, int cvNumber)
        throws ProtocolException {
        byte[] data = { 0 };
        BidibMessage response =
            delegate.send(new CommandStationPomMessage(locoAddress, opCode, cvNumber, data), null, true,
                CommandStationPomAcknowledgeResponse.TYPE);
        PomAcknowledge result = null;
        if (response instanceof CommandStationPomAcknowledgeResponse) {
            result = ((CommandStationPomAcknowledgeResponse) response).getAcknState();
        }
        LOGGER.debug("Return the pomAcknowledge: {}", result);
        return result;
    }

    public PomAcknowledge writePom(AddressData locoAddress, CommandStationPom opCode, int cvNumber, int cvValue)
        throws ProtocolException {

        byte[] data = { ByteUtils.getLowByte(cvValue) };

        if (CommandStationPom.XWR_BYTE2.equals(opCode)) {
            data = new byte[] { ByteUtils.getLowByte(cvValue), ByteUtils.getHighByte(cvValue) };

            cvNumber = 0x04; // short form CV access for long address

            LOGGER.info("Prepared data for short form CV access write, cvNumber (instruction type): {}, 2 bytes: {}",
                cvNumber, ByteUtils.bytesToHex(data));
        }

        BidibMessage response =
            delegate.send(new CommandStationPomMessage(locoAddress, opCode, cvNumber, data), null, true,
                CommandStationPomAcknowledgeResponse.TYPE);

        PomAcknowledge result = null;
        if (response instanceof CommandStationPomAcknowledgeResponse) {
            result = ((CommandStationPomAcknowledgeResponse) response).getAcknState();
        }
        LOGGER.debug("Return the pomAcknowledge: {}", result);
        return result;
    }

    public void readPt(CommandStationPt opCode, int cvNumber) throws ProtocolException {
        LOGGER.info("Send PT read command, opCode: {}, cvNumber: {}", opCode, cvNumber);
        byte data = 0;
        delegate.sendNoWait(new CommandStationProgMessage(opCode, cvNumber, data));
    }

    public void writePt(CommandStationPt opCode, int cvNumber, int cvValue) throws ProtocolException {
        LOGGER.info("Send PT write command, opCode: {}, cvNumber: {}, cvValue: {}", opCode, cvNumber, cvValue);
        byte data = ByteUtils.getLowByte(cvValue);
        delegate.sendNoWait(new CommandStationProgMessage(opCode, cvNumber, data));
    }

    private void addMessageListener(final MessageListener messageListener) {
        delegate.getMessageReceiver().addMessageListener(messageListener);
    }

    private void removeMessageListener(final MessageListener messageListener) {
        delegate.getMessageReceiver().removeMessageListener(messageListener);
    }

    /**
     * Get the current RailCom+ TID.
     * 
     * @throws ProtocolException
     */
    public void getRcPlusTid() throws ProtocolException {

        if (m4Enabled) {
            LOGGER.info("Send CommandStationM4Message(M4_GET_TID) to node");
            delegate.sendNoWait(new CommandStationM4Message(M4OpCodes.M4_GET_TID));
        }
        else {
            LOGGER.info("Send CommandStationRcPlusMessage(RC_GET_TID) to node");
            delegate.sendNoWait(new CommandStationRcPlusMessage(RcPlusOpCodes.RC_GET_TID));
        }
    }

    /**
     * Set the current RailCom+ TID.
     * 
     * @param tid
     *            the TID
     * @throws ProtocolException
     */
    public void setRcPlusTid(TidData tid) throws ProtocolException {

        if (m4Enabled) {
            LOGGER.info("Send CommandStationM4Message(M4_SET_TID) to node, tid: {}", tid);
            delegate.sendNoWait(new CommandStationM4Message(M4OpCodes.M4_SET_TID, tid));
        }
        else {
            LOGGER.info("Send CommandStationRcPlusMessage(RC_SET_TID) to node, tid: {}", tid);
            delegate.sendNoWait(new CommandStationRcPlusMessage(RcPlusOpCodes.RC_SET_TID, tid));
        }
    }

    /**
     * Send the RailCom+ ping once command.
     * 
     * @param phase
     *            the phase
     * @throws ProtocolException
     */
    public void sendPingOnce(RcPlusPhase phase) throws ProtocolException {
        LOGGER.info("Send CommandStationRcPlusMessage(RC_PING_ONCE) to node, phase: {}", phase);

        delegate.sendNoWait(new CommandStationRcPlusMessage(
            (phase == RcPlusPhase.P0 ? RcPlusOpCodes.RC_PING_ONCE_P0 : RcPlusOpCodes.RC_PING_ONCE_P1)));
    }

    /**
     * Send the RailCom+ ping command.
     * 
     * @param interval
     *            the repeat interval in units of 100ms, the value 0 stops the repeated sends.
     * @throws ProtocolException
     */
    public void sendPing(int interval) throws ProtocolException {

        if (m4Enabled) {
            LOGGER.info("Send CommandStationM4Message(M4_SET_BEACON) to node, interval: {}", interval);
            delegate.sendNoWait(new CommandStationM4Message(M4OpCodes.M4_SET_BEACON, new byte[] { ByteUtils
                .getLowByte(interval) }));
        }
        else {
            LOGGER.info("Send CommandStationRcPlusMessage(RC_PING) to node, interval: {}", interval);
            delegate.sendNoWait(new CommandStationRcPlusMessage(RcPlusOpCodes.RC_PING, new byte[] { ByteUtils
                .getLowByte(interval) }));
        }
    }

    /**
     * Send the RailCom+ find command.
     * 
     * @param phase
     *            the phase
     * @param decoder
     *            the decoder data
     * @throws ProtocolException
     */
    public void sendFind(RcPlusPhase phase, DecoderUniqueIdData decoder) throws ProtocolException {
        LOGGER.info("Send CommandStationRcPlusMessage(RC_FIND) to node, phase: {}, decoder: {}", phase, decoder);

        delegate.sendNoWait(new CommandStationRcPlusMessage(
            (phase == RcPlusPhase.P0 ? RcPlusOpCodes.RC_FIND_P0 : RcPlusOpCodes.RC_FIND_P1), decoder));
    }

    /**
     * Send the RailCom+ find command.
     * 
     * @param bindData
     *            the bind data information
     * @throws ProtocolException
     */
    public void sendBind(RcPlusBindData bindData) throws ProtocolException {
        LOGGER.info("Send CommandStationRcPlusMessage(RC_BIND) to node, bindData: {}", bindData);

        delegate.sendNoWait(new CommandStationRcPlusMessage(bindData));
    }

    /**
     * Query the requested command station value.
     * 
     * @param csQueryValue
     *            the value
     * @param address
     *            the optional address
     * @throws ProtocolException
     */
    public void queryValue(CsQueryTypeEnum csQueryValue, Integer address) throws ProtocolException {
        LOGGER.info("Send CommandStationQueryMessage to node, csQueryValue: {}, address: {}", csQueryValue, address);

        if (address != null) {
            delegate.sendNoWait(new CommandStationQueryMessage(csQueryValue, address));
        }
        else {
            delegate.sendNoWait(new CommandStationQueryMessage(csQueryValue));
        }
    }
}
