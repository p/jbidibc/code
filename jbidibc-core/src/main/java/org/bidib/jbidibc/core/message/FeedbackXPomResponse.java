package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DecoderIdAddressData;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

/**
 * Signal the XPOM message from feedback device.
 */
public class FeedbackXPomResponse extends BidibMessage {

    public static final Integer TYPE = BidibLibrary.MSG_BM_XPOM;

    FeedbackXPomResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 10) {
            throw new ProtocolException("No valid MSG_BM_XPOM received.");
        }
    }

    // 1..4:addr/did, 5:0/vid, 6:opcode, 7:cv_l, 8:cv_h, 9:cv_x, 10[..13]: data
    public FeedbackXPomResponse(byte[] addr, int num, AddressData decoderAddress, int vendorId,
        CommandStationPom opCode, int cvNumber, byte[] data) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_XPOM, prepareData(decoderAddress, vendorId, opCode, cvNumber, data));
    }

    // 1..4:addr/did, 5:0/vid, 6:opcode, 7:cv_l, 8:cv_h, 9:cv_x, 10[..13]: data
    public FeedbackXPomResponse(byte[] addr, int num, DecoderIdAddressData addressDID, CommandStationPom opCode,
        int cvNumber, byte[] data) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_XPOM, prepareData(addressDID, opCode, cvNumber, data));
    }

    public String getName() {
        return "MSG_BM_XPOM";
    }

    // support for DID
    private static byte[] prepareData(
        DecoderIdAddressData addressDID, CommandStationPom opCode, int cvNumber, byte[] data) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // 1..4:addr/did, 5:0/vid, 6:opcode, 7:cv_l, 8:cv_h, 9:cv_x, 10[..13]: data
        addressDID.writeToStream(out);

        // op code
        byte opCodeValue = opCode.getType();
        out.write(opCodeValue);

        // CV number
        out.write(ByteUtils.getLowByte(cvNumber - 1));
        out.write(ByteUtils.getHighByte(cvNumber - 1));

        if ((opCodeValue & 0x80) == 0x80) {
            // XPOM
            byte xCV = (byte) ((cvNumber >> 16) & 0xFF);
            out.write(xCV);
        }
        else {
            // no XPOM
            out.write((byte) 0);
        }
        // data
        switch (opCode) {
            case RD_BLOCK:
                break;
            case RD_BYTE:
            case WR_BIT:
            case WR_BYTE:
                out.write(data, 0, 1 /* only 1 byte */);
                break;
            case XWR_BYTE1:
                out.write(data, 0, 1 /* only 1 byte */);
                break;
            case XWR_BYTE2:
                out.write(data, 0, 2 /* only 2 byte */);
                break;
            // XPOM
            case X_RD_BLOCK:
                break;
            case X_WR_BIT:
            case X_WR_BYTE1:
                out.write(data, 0, 1 /* only 1 byte */);
                break;
            case X_WR_BYTE2:
                out.write(data, 0, 2 /* only 2 byte */);
                break;
            case X_WR_BYTE3:
                out.write(data, 0, 3 /* only 2 byte */);
                break;
            case X_WR_BYTE4:
                out.write(data, 0, 4 /* 4 byte */);
                break;
            default:
                break;
        }

        return out.toByteArray();
    }

    private static byte[] prepareData(
        AddressData decoderAddress, int vendorId, CommandStationPom opCode, int cvNumber, byte[] data) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // 1..4:addr/did, 5:0/vid, 6:opcode, 7:cv_l, 8:cv_h, 9:cv_x, 10[..13]: data
        decoderAddress.writeToStream(out);
        // the vendor id
        out.write(ByteUtils.getLowByte(vendorId));
        // op code
        byte opCodeValue = opCode.getType();
        out.write(opCodeValue);

        // CV number
        out.write(ByteUtils.getLowByte(cvNumber - 1));
        out.write(ByteUtils.getHighByte(cvNumber - 1));

        if ((opCodeValue & 0x80) == 0x80) {
            // XPOM
            byte xCV = (byte) ((cvNumber >> 16) & 0xFF);
            out.write(xCV);
        }
        else {
            // no XPOM
            out.write((byte) 0);
        }
        // data
        switch (opCode) {
            case RD_BLOCK:
                break;
            case RD_BYTE:
            case WR_BIT:
            case WR_BYTE:
                out.write(data, 0, 1 /* only 1 byte */);
                break;
            case XWR_BYTE1:
                out.write(data, 0, 1 /* only 1 byte */);
                break;
            case XWR_BYTE2:
                out.write(data, 0, 2 /* only 2 byte */);
                break;
            // XPOM
            case X_RD_BLOCK:
                break;
            case X_WR_BIT:
            case X_WR_BYTE1:
                out.write(data, 0, 1 /* only 1 byte */);
                break;
            case X_WR_BYTE2:
                out.write(data, 0, 2 /* only 2 byte */);
                break;
            case X_WR_BYTE3:
                out.write(data, 0, 3 /* only 2 byte */);
                break;
            case X_WR_BYTE4:
                out.write(data, 0, 4 /* 4 byte */);
                break;
            default:
                break;
        }

        return out.toByteArray();
    }

    public boolean isDiDAddress() {
        return (getData()[4] != 0);
    }

    public DecoderIdAddressData getDID() {
        byte[] data = ByteUtils.subArray(getData(), 0, 5);

        DecoderIdAddressData did = new DecoderIdAddressData(data);
        return did;
    }

    public AddressData getDecoderAddress() {
        if (getDecoderVendorId() > 0) {
            // the decVid is > 0 -> no address
            return null;
        }

        int index = 0;
        byte lowByte = getData()[index++];
        byte highByte = getData()[index++];
        int address = ByteUtils.getWord(lowByte, (byte) (highByte & 0x3F));

        AddressData addressData = new AddressData(address, AddressTypeEnum.valueOf((byte) ((highByte & 0xC0) >> 6)));
        return addressData;
    }

    public int getDecoderVendorId() {
        return ByteUtils.getInt(getData()[4]);
    }

    public int getDecoderSerialNum() {
        return ByteUtils.convertSerial(getData(), 0);
    }

    public int getAddressX() {
        byte[] data = getData();

        return ByteUtils.getInt(data[2], data[3]);
    }

    public int getMid() {
        int opCode = ByteUtils.getInt(getData()[4]);
        return opCode;
    }

    public int getOpCode() {
        int opCode = ByteUtils.getInt(getData()[5]);
        return opCode;
    }

    /**
     * @return the CV number (CVX value is not evaluated)
     */
    public int getCvNumber() {
        byte[] data = getData();

        return ByteUtils.getInt(data[6], data[7]) + 1; // 0 -> CV1
    }

    /**
     * @return the CVX number
     */
    public int getCvNumberX() {
        byte[] data = getData();

        return ByteUtils.getCvXNumber(data, 6) + 1; // 0 -> CV1
    }

    /**
     * @return the CV value
     */
    public int getCvValue() {
        byte[] data = getData();

        return ByteUtils.getInt(data[9]);
    }

    /**
     * @return the CVX value
     */
    public int[] getCvXValue() {
        byte[] data = getData();
        int[] result = null;
        switch (getOpCode()) {
            case BidibLibrary.BIDIB_CS_XPOM_WR_BYTE2:
                result = new int[] { ByteUtils.getInt(data[9]), ByteUtils.getInt(data[10]) };
                break;
            case BidibLibrary.BIDIB_CS_XPOM_WR_BYTE3:
                result =
                    new int[] { ByteUtils.getInt(data[9]), ByteUtils.getInt(data[10]), ByteUtils.getInt(data[11]) };
                break;
            case BidibLibrary.BIDIB_CS_XPOM_WR_BYTE4:
                result =
                    new int[] { ByteUtils.getInt(data[9]), ByteUtils.getInt(data[10]), ByteUtils.getInt(data[11]),
                        ByteUtils.getInt(data[12]) };
                break;
            case BidibLibrary.BIDIB_CS_XPOM_RD_BLOCK:
                result =
                    new int[] { ByteUtils.getInt(data[9]), ByteUtils.getInt(data[10]), ByteUtils.getInt(data[11]),
                        ByteUtils.getInt(data[12]) };
                break;
            case BidibLibrary.BIDIB_CS_XPOM_WR_BIT:
            case BidibLibrary.BIDIB_CS_XPOM_WR_BYTE1:
            default:
                result = new int[] { ByteUtils.getInt(data[9]) };
                break;
        }
        return result;
    }
}
