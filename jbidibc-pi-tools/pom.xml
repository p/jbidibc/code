<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.bidib.jbidib</groupId>
        <artifactId>jbidibc-parent</artifactId>
        <version>${revision}</version>
        <relativePath>../jbidibc-parent/pom.xml</relativePath>
	</parent>
    
	<artifactId>jbidibc-pi-tools</artifactId>
	<packaging>jar</packaging>
	<name>jBiDiB :: jbidibc Pi tools</name>
	<description>jBiDiB jbidibc Pi tools POM. Use -Dpi4j.dev.transfer=true to transfer the artifact to the raspberry pi.</description>
	<properties>
		<ant-jsch.version>1.9.7</ant-jsch.version>
		<jsch.version>0.1.53</jsch.version>
		<ant-contrib.version>20020829</ant-contrib.version>
        
        <main-class>org.bidib.jbidib.pi.tools.BiDiBPiStandalone</main-class>

		<!-- DEFAULT SETTINGS FOR REMOTELY BUILDING NATIVE LIBRARY ON A REMOTE 
			PLATFORM -->
		<pi4j.default.host>192.168.1.13</pi4j.default.host>
		<pi4j.default.port>22</pi4j.default.port>
		<pi4j.default.user>pi</pi4j.default.user>
		<pi4j.default.password>bidib-wizard</pi4j.default.password>
		<pi4j.default.directory>pi4j-temp</pi4j.default.directory>

		<!-- SETTINGS FOR COPYING Pi4J ARTIFACTS TO DEVELOPMENT DEVICE/PLATFORM -->
		<pi4j.dev.transfer>false</pi4j.dev.transfer>
		<pi4j.dev.host>${pi4j.default.host}</pi4j.dev.host>
		<pi4j.dev.port>${pi4j.default.port}</pi4j.dev.port>
		<pi4j.dev.user>${pi4j.default.user}</pi4j.dev.user>
		<pi4j.dev.password>${pi4j.default.password}</pi4j.dev.password>
		<pi4j.dev.directory>pi4j-dev</pi4j.dev.directory>

		<!-- DEFAULT Pi4J BUILD PROPERTIES FOR THE 'RaspberryPi' PLATFORM -->
		<raspberrypi.build>false</raspberrypi.build>
		<raspberrypi.platform>raspberrypi</raspberrypi.platform>
		<raspberrypi.name>RaspberryPi</raspberrypi.name>
		<raspberrypi.host>${pi4j.default.host}</raspberrypi.host>
		<raspberrypi.port>${pi4j.default.port}</raspberrypi.port>
		<raspberrypi.user>${pi4j.default.user}</raspberrypi.user>
		<raspberrypi.password>${pi4j.default.password}</raspberrypi.password>
		<raspberrypi.directory>${pi4j.default.directory}</raspberrypi.directory>

	</properties>
	<dependencies>
 		<dependency>
 			<groupId>ch.qos.logback</groupId>
 			<artifactId>logback-classic</artifactId>
 		</dependency>
		<dependency>
			<groupId>org.bidib.jbidib</groupId>
			<artifactId>jbidibc-pi</artifactId>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<configuration>
					<includePom>true</includePom>
				</configuration>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addClasspath>true</addClasspath>
                            <!--classpathPrefix>lib</classpathPrefix-->
                            <mainClass>${main-class}</mainClass>
                            <useUniqueVersions>true</useUniqueVersions>
                        </manifest>
                        <manifestEntries>
                            <Implementation-Version>${project.version} (${buildnumber-and-branch-info})</Implementation-Version>
                            <Implementation-Build>${project.artifactId}-${project.version} (${buildnumber-and-branch-info})</Implementation-Build>
                            <BuildNumber>${buildNumber}</BuildNumber>
                            <Class-Path>.</Class-Path>
                            <!--SplashScreen-Image>icons/BiDiBWizardSplash.png</SplashScreen-Image-->
                        </manifestEntries>
                    </archive>
                </configuration>
			</plugin>

			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<mainClass>org.bidib.jbidib.pi.tools.BiDiBPiStandalone</mainClass>
						</manifest>
					</archive>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id> <!-- this is used for inheritance merges -->
						<phase>package</phase> <!-- bind to the packaging phase -->
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- OPTIONALLY DEPLOY THE FINAL JAR TO THE RASPBERRY PI -->
			<plugin>
				<artifactId>maven-antrun-plugin</artifactId>
				<executions>

					<!-- copy the compiled JAR file to the Raspberry Pi platform platform -->
					<execution>
						<id>transfer-compiled-pi4j-example-jar</id>
						<phase>install</phase>
						<goals>
							<goal>run</goal>
						</goals>
						<configuration>
							<target>
								<taskdef resource="net/sf/antcontrib/antcontrib.properties" classpathref="maven.plugin.classpath" />
								<if>
									<equals arg1="${pi4j.dev.transfer}" arg2="true" />
									<then>

										<!-- ensure the target directory exists on the Raspberry Pi -->
										<sshexec host="${pi4j.dev.host}" port="${pi4j.dev.port}" username="${pi4j.dev.user}" password="${pi4j.dev.password}" trust="true" failonerror="false" verbose="true" command="mkdir --parents ${pi4j.dev.directory}" />

										<!-- copy the JAR file to the Raspberry Pi -->
										<scp file="${project.build.directory}/${project.build.finalName}-jar-with-dependencies.jar" todir="${pi4j.dev.user}:${pi4j.dev.password}@${pi4j.dev.host}:${pi4j.dev.directory}" port="${pi4j.dev.port}" trust="true" verbose="true" failonerror="true">
										</scp>

										<!-- copy the example source files to the Raspberry Pi -->
										<scp todir="${pi4j.dev.user}:${pi4j.dev.password}@${pi4j.dev.host}:${pi4j.dev.directory}" port="${pi4j.dev.port}" trust="true" verbose="true" failonerror="true">
											<fileset dir="src/main/java" />
										</scp>
									</then>
								</if>
							</target>
						</configuration>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>org.apache.ant</groupId>
						<artifactId>ant-jsch</artifactId>
						<version>${ant-jsch.version}</version>
					</dependency>
					<dependency>
						<groupId>com.jcraft</groupId>
						<artifactId>jsch</artifactId>
						<version>${jsch.version}</version>
					</dependency>
					<dependency>
						<groupId>ant-contrib</groupId>
						<artifactId>ant-contrib</artifactId>
						<version>${ant-contrib.version}</version>
					</dependency>
				</dependencies>
			</plugin>

		</plugins>
	</build>
    
	<profiles>
		<profile>
			<id>docker</id>
			
			<build>
				<plugins>
		            <plugin>
		                <groupId>io.fabric8</groupId>
		                <artifactId>docker-maven-plugin</artifactId>
                        <version>0.23.0</version>
                        <!--
                        <artifactId>fabric8-maven-plugin</artifactId> 
                        <version>3.5.31</version>
                        -->
		                <configuration>
		                    <!--dockerHost>http://localhost:22375</dockerHost-->
                            <!--dockerHost>http://192.168.1.18:2375</dockerHost-->
		                    <dockerHost>http://localhost:2375</dockerHost>
		                    <registry>localhost:5000</registry>
		                    <pushRegistry>localhost:5000</pushRegistry>
                                    <verbose>true</verbose>
		                    <images>
		                        <image>
		                            <name>jbidibc/pi-tools</name>
		                            <alias>pi-tools</alias>
		                            <build>
                                                <!-- dockerFileDir>${project.basedir}/src/main/docker/file</dockerFileDir -->
		                                <!-- from>openjdk:latest</from -->
                                                <!-- from>akuhtz/rpi-java8:latest</from -->
                                        <from>akuhtz/rpi-java8:8u151</from>

		                                <assembly>
		                                    <descriptorRef>artifact-with-dependencies</descriptorRef>
		                                </assembly>

		                                <cmd>java -jar /maven/${project.build.finalName}.jar</cmd>

		                                <!-- cmd>java -jar maven/${project.build.finalName}.jar</cmd -->
                                        <tags>
                                            <tag>latest</tag>
                                            <tag>${project.version}</tag>
                                        </tags>
		                            </build>
		                            <run>
                                        <volumes>
                                            <bind>
                                             <!-- volume>/opt/logs:/logs</volume -->
                                             <!-- volume>tmp:/tmp</volume -->
                                             <!-- volume>/opt/host_export:/opt/container_import</volume -->
                                            </bind>
                                        </volumes>

		                            	<!-- wait for log statement ... this will not come :-) -->
		                                <!--wait>
		                                    <log>Hello CSP-Gateway!</log>
		                                </wait-->
		                            </run>
		                        </image>
		                    </images>
		                </configuration>
		                <executions>
		                    <execution>
		                        <id>start-build</id>
		                        <phase>install</phase>
		                        <goals>
		                            <goal>build</goal>
		                            <goal>push</goal>
		                        </goals>
		                    </execution>
		                </executions>
		            </plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
    
</project>
