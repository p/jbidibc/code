package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

/**
 * This response is sent as direct response to a request that takes some time to execute. The final execution state will
 * be signaled by a LcStatResponse message.
 * 
 */
public class LcWaitResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_LC_WAIT;

    LcWaitResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 3) {
            throw new ProtocolException("No valid MSG_LC_WAIT received.");
        }
    }

    public String getName() {
        return "MSG_LC_WAIT";
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port type
     */
    public LcOutputType getPortType(PortModelEnum portModel) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat_extended:
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(getData()[0]);
                break;
        }
        return outputType;
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port number
     */
    public int getPortNumber(PortModelEnum portModel) {
        int portNumber = -1;
        switch (portModel) {
            case flat_extended:
                portNumber = ByteUtils.getInt(getData()[0], getData()[0] & 0x7F);
                break;
            case flat:
                portNumber = ByteUtils.getInt(getData()[0]);
                break;
            default:
                portNumber = ByteUtils.getInt(getData()[1], 0x7F);
                break;
        }
        return portNumber;
    }

    public BidibPort getBidibPort() {
        return BidibPort.prepareBidibPort(getData()[0], getData()[1]);
    }

    public int getPredictedRotationTime() {
        int result = ByteUtils.getInt(getData()[2]);

        return result > 127 ? result * 1000 : result * 100;
    }
}
