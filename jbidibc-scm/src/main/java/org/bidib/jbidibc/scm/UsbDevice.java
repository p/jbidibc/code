package org.bidib.jbidibc.scm;

import com.serialpundit.usb.SerialComUSBdevice;

public class UsbDevice {

    private final SerialComUSBdevice usbDevice;

    private String[] comPorts;

    public UsbDevice(final SerialComUSBdevice usbDevice) {
        this(usbDevice, null);
    }

    public UsbDevice(final SerialComUSBdevice usbDevice, String[] comPorts) {
        this.usbDevice = usbDevice;
        this.comPorts = comPorts;
    }

    /**
     * @return the usbDevice
     */
    public SerialComUSBdevice getUsbDevice() {
        return usbDevice;
    }

    /**
     * @return the comPorts
     */
    public String[] getComPorts() {
        return comPorts;
    }

    /**
     * @param comPorts
     *            the comPorts to set
     */
    public void setComPorts(String[] comPorts) {
        this.comPorts = comPorts;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UsbDevice) {
            return ((UsbDevice) obj).usbDevice.getSerialNumber().equals(usbDevice.getSerialNumber());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return usbDevice.getSerialNumber().hashCode();
    }

}
