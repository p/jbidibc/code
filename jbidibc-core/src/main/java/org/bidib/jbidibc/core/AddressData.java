package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class AddressData {
    private final int address;

    private final AddressTypeEnum type;

    public AddressData(int address, AddressTypeEnum type) {
        this.address = address;
        this.type = type;
    }

    public int getAddress() {
        return address;
    }

    public AddressTypeEnum getType() {
        return type;
    }

    public String toString() {
        return getClass().getSimpleName() + "[address=" + address + ",type=" + type + "]";
    }

    public void writeToStream(ByteArrayOutputStream out) {

        if (address > 0) {
            // write address
            out.write(ByteUtils.getLowByte(address));
            byte highAddr = ByteUtils.getHighByte(address);
            switch (getType()) {
                case LOCOMOTIVE_BACKWARD:
                    highAddr = (byte) (highAddr & 0x3F);
                    highAddr = (byte) (highAddr | (1 << 7));
                    break;
                case ACCESSORY:
                    highAddr = (byte) (highAddr & 0x3F);
                    highAddr = (byte) (highAddr | (1 << 6));
                    break;
                case EXTENDED_ACCESSORY:
                    highAddr = (byte) (highAddr | (1 << 6));
                    highAddr = (byte) (highAddr | (1 << 7));
                    break;
                default:
                    highAddr = (byte) (highAddr & 0x3F);
                    break;
            }
            out.write(highAddr);
        }
        else {
            out.write((byte) 0);
            out.write((byte) 0);
        }
    }

    /**
     * Get the address data from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset inside the provided byte array
     * @return the AddressData instance
     */
    public static AddressData fromByteArray(byte[] data, int offset) {
        int index = offset;
        byte lowByte = data[index++];
        byte highByte = data[index++];
        int address = ByteUtils.getWord(lowByte, (byte) (highByte & 0x3F));

        AddressData addressData = new AddressData(address, AddressTypeEnum.valueOf((byte) ((highByte & 0xC0) >> 6)));
        return addressData;
    }

    /**
     * Add the address data to the provided byte array.
     * 
     * @param addressData
     *            the address
     * @param data
     *            the byte array
     * @param offset
     *            the offset to insert the address data
     */
    public static void toByteArray(AddressData addressData, byte[] data, int offset) {

        byte lowByte = ByteUtils.getLowByte(addressData.getAddress());
        data[offset] = lowByte;

        byte highByte =
            ByteUtils
                .getLowByte((ByteUtils.getHighByte(addressData.getAddress()) | (addressData.getType().getType() << 6)));
        data[offset + 1] = highByte;
    }

    /**
     * Convert the address data into a byte array.
     * 
     * @param addressData
     *            the address
     */
    public static byte[] toByteArray(AddressData addressData) {

        byte[] data = new byte[2];
        byte lowByte = ByteUtils.getLowByte(addressData.getAddress());
        data[0] = lowByte;

        byte highByte =
            ByteUtils
                .getLowByte((ByteUtils.getHighByte(addressData.getAddress()) | (addressData.getType().getType() << 6)));
        data[1] = highByte;

        return data;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AddressData) {
            AddressData other = (AddressData) obj;
            if (address == other.getAddress() && type.equals(other.getType())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
