package org.bidib.jbidibc.ch341a.lococard;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LocoCardDecoderTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocoCardDecoderTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/lococard";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdir();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void decodeLocoCardData() throws IOException {

        String sampleData =
            IOUtils.toString(LocoCardDecoderTest.class.getResourceAsStream("/lococard/sample-data.txt"));

        byte[] cardData = ByteUtils.parseHexBinarySpaceTerminated(sampleData);
        Assert.assertNotNull(cardData);
        Assert.assertEquals(cardData.length, 8192);

        Assert.assertEquals(cardData[0], ByteUtils.getLowByte(0x02));
        Assert.assertEquals(cardData[1], ByteUtils.getLowByte(0xC5));

        LocoCardDecoder decoder = new LocoCardDecoder();
        decoder.decodeLocoCardData(cardData, new File(OUTPUT_TARGET_DIR));

    }
}
