package org.bidib.jbidibc.ch341a;

import com.sun.jna.platform.win32.WinDef.UCHAR;
import com.sun.jna.ptr.ByReference;

public class Helper {

    /**
     * The Class CHARByReference.
     */
    public static class UCHARByReference extends ByReference {

        /**
         * Instantiates a new UCHAR by reference.
         */
        public UCHARByReference() {
            this(new UCHAR(0));
        }

        /**
         * Instantiates a new CHAR by reference.
         * 
         * @param value
         *            the value
         */
        public UCHARByReference(UCHAR value) {
            super(UCHAR.SIZE);
            setValue(value);
        }

        /**
         * Sets the value.
         * 
         * @param value
         *            the new value
         */
        public void setValue(UCHAR value) {
            getPointer().setByte(0, value.byteValue());
        }

        /**
         * Gets the value.
         * 
         * @return the value
         */
        public UCHAR getValue() {
            return new UCHAR(getPointer().getChar(0));
        }
    }

}
