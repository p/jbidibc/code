package org.bidib.jbidibc.core.schema;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.decoder.commontypes.DecoderTypeType;
import org.bidib.jbidibc.core.schema.decoder.commontypes.VersionType;
import org.bidib.jbidibc.core.schema.decoder.userdevices.DecoderType;
import org.bidib.jbidibc.core.schema.decoder.userdevices.UserDeviceType;
import org.bidib.jbidibc.core.schema.decoder.userdevices.UserDevicesList;
import org.bidib.jbidibc.core.schema.decoder.userdevices.UserDevicesType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UserDevicesListFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDevicesListFactoryTest.class);

    private static final String FILENAME_USER_DEVICES = "/xml-test/userDevices/UserDevices.xml";

    private static final String OUTPUT_TARGET_DIR = "target/xml-test/userDevices";

    private static final String OUTPUT_FILENAME = "userDevices-test1.xml";

    private static final String OUTPUT_FILENAME_2 = "userDevices-test2.xml";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void getUserDevicesList() throws URISyntaxException {

        LOGGER.info("Load labels from UserDevices.");

        URL fileUrl = UserDevicesListFactoryTest.class.getResource(FILENAME_USER_DEVICES);
        Assert.assertNotNull(fileUrl);

        File userDevicesFile = new File(fileUrl.toURI());

        UserDevicesList userDevicesList = UserDevicesListFactory.getUserDevicesList(userDevicesFile);
        Assert.assertNotNull(userDevicesList);

        Assert.assertNotNull(userDevicesList.getUserDevices());

        List<UserDeviceType> userDevices = userDevicesList.getUserDevices().getUserDevice();
        Assert.assertEquals(userDevices.size(), 5);
    }

    @Test
    public void getUserDevicesLocoList() throws URISyntaxException {

        LOGGER.info("Load labels from UserDevices.");

        URL fileUrl = UserDevicesListFactoryTest.class.getResource(FILENAME_USER_DEVICES);
        Assert.assertNotNull(fileUrl);

        File userDevicesFile = new File(fileUrl.toURI());

        UserDevicesList userDevicesList = UserDevicesListFactory.getUserDevicesList(userDevicesFile);
        Assert.assertNotNull(userDevicesList);

        Assert.assertNotNull(userDevicesList.getUserDevices());

        List<UserDeviceType> userDevices = userDevicesList.getUserDevices().getUserDevice();
        Assert.assertEquals(userDevices.size(), 5);

        List<UserDeviceType> locoDevices = UserDevicesListFactory.findUserDevices(DecoderTypeType.LOCO, userDevices);
        Assert.assertNotNull(locoDevices);
        Assert.assertEquals(locoDevices.size(), 4);
    }

    @Test
    public void saveUserDevicesList() throws DatatypeConfigurationException {

        File fileName = new File(OUTPUT_TARGET_DIR, "/" + OUTPUT_FILENAME);

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        UserDevicesList userDevicesList = new UserDevicesList();
        userDevicesList.withVersion(new VersionType()
            .withAuthor("Wizard").withCreatedBy("Wizard").withLastUpdate("2018-03-08T07:35:49").withCreated(date2)
            .withCreatorLink("http://bidib.org"));

        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        LOGGER.info("UUID=" + randomUUIDString);

        userDevicesList.withUserDevices(new UserDevicesType().withUserDevice(new UserDeviceType()
            .withAddress(3).withId(randomUUIDString).withName("Test").withDeviceType(DecoderTypeType.LOCO)
            .withDecoder(new DecoderType().withDecoderId("00 00000000").withSpeedsteps(128).withName("Test decoder"))));
        UserDevicesListFactory.saveUserDevicesList(userDevicesList, fileName);
    }

    @Test
    public void saveUserDevicesList2() throws DatatypeConfigurationException {

        File fileName = new File(OUTPUT_TARGET_DIR, "/" + OUTPUT_FILENAME_2);

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        UserDevicesList userDevicesList = new UserDevicesList();
        userDevicesList.withVersion(new VersionType()
            .withAuthor("Wizard").withCreatedBy("Wizard").withLastUpdate("2018-03-08T07:35:49").withCreated(date2)
            .withCreatorLink("http://bidib.org"));

        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        LOGGER.info("UUID=" + randomUUIDString);

        userDevicesList.withUserDevices(new UserDevicesType().withUserDevice(new UserDeviceType()
            .withAddress(3).withId(randomUUIDString).withName("Test").withDeviceType(DecoderTypeType.LOCO)));
        UserDevicesListFactory.saveUserDevicesList(userDevicesList, fileName);
    }
}
