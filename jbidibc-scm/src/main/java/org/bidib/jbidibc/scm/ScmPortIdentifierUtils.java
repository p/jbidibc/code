package org.bidib.jbidibc.scm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bidib.jbidibc.core.exception.InvalidLibraryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.serialpundit.serial.SerialComManager;

public class ScmPortIdentifierUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScmPortIdentifierUtils.class);

    public static List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new ArrayList<String>();

        try {

            String tempDir = System.getProperty("java.io.tmpdir");
            File temp = new File(tempDir, "jbidibc");
            // temp.mkdirs();

            // get the comm port identifiers
            SerialComManager scm = new SerialComManager("scm", temp.getAbsolutePath(), true, false);
            String[] ports = scm.listAvailableComPorts();
            for (String port : ports) {
                portIdentifiers.add(port);
            }
        }
        catch (UnsatisfiedLinkError ule) {
            LOGGER.warn("Get comm port identifiers failed.", ule);
            throw new InvalidLibraryException(ule.getMessage(), ule.getCause());
        }
        catch (Error error) {
            LOGGER.warn("Get comm port identifiers failed.", error);
            throw new RuntimeException(error.getMessage(), error.getCause());
        }
        catch (IOException ex) {
            LOGGER.warn("Get comm port identifiers failed.", ex);
            throw new RuntimeException(ex.getMessage(), ex.getCause());
        }
        return portIdentifiers;
    }
}
