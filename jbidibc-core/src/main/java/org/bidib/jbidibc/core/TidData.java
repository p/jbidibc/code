package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TidData {
    private static final Logger LOGGER = LoggerFactory.getLogger(TidData.class);

    private final DecoderUniqueIdData uniqueIdData;

    private final int sid;

    public TidData(DecoderUniqueIdData uniqueIdData, int sid) {
        this.uniqueIdData = uniqueIdData;
        this.sid = sid;
    }

    /**
     * @return the rcPlus unique id
     */
    public DecoderUniqueIdData getUniqueId() {
        return uniqueIdData;
    }

    /**
     * @return the session id
     */
    public int getSid() {
        return sid;
    }

    public String toString() {
        return getClass().getSimpleName() + "[sid=" + sid + ", uniqueId=" + uniqueIdData + "]";
    }

    public void writeToStream(ByteArrayOutputStream out) {

        // write the unique id
        uniqueIdData.writeToStream(out);

        // write the session id
        out.write((byte) sid & 0xFF);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TidData) {
            TidData other = (TidData) obj;
            if (sid == other.sid && uniqueIdData.equals(other.uniqueIdData)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hc = 17;
        int hashMultiplier = 59;
        hc = hc * hashMultiplier + uniqueIdData.hashCode();
        hc = hc * hashMultiplier + sid;
        return hc;
    }

    /**
     * Create a TidData instance from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset to start
     * @return the TidData instance
     */
    public static TidData fromByteArray(byte[] data, int offset) {
        if (data.length < (offset + 5)) {
            LOGGER.warn("The size of the provided data does not meet the expected length.");
            throw new IllegalArgumentException(
                "The size of the provided data does not meet the expected length (>=5). Provided length: "
                    + data.length);
        }

        DecoderUniqueIdData uniqueIdData = DecoderUniqueIdData.fromByteArray(data, offset);
        int sid = ByteUtils.getInt(data[offset + 5]);
        TidData tidData = new TidData(uniqueIdData, sid);

        LOGGER.info("Return tidData: {}", tidData);
        return tidData;
    }
}
