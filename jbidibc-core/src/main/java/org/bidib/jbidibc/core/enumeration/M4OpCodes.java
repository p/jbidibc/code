package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum M4OpCodes implements BidibEnum {
    M4_GET_TID(BidibLibrary.M4_GET_TID, "get-tid"), M4_SET_TID(BidibLibrary.M4_SET_TID, "set-tid"), M4_SET_BEACON(
        BidibLibrary.M4_SET_BEACON,
        "set-beacon"), M4_SET_SEARCH(BidibLibrary.M4_SET_SEARCH, "set-search"), M4_BIND_ADDR(BidibLibrary.M4_BIND_ADDR,
            "bind-addr"), M4_UNBIND_ADDR(BidibLibrary.M4_UNBIND_ADDR, "unbind-address"),;

    private final String key;

    private final byte type;

    private M4OpCodes(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create a M4 opcode.
     * 
     * @param type
     *            numeric value of the M4 opcode
     * 
     * @return M4OpCodes
     */
    public static M4OpCodes valueOf(byte type) {
        M4OpCodes result = null;

        for (M4OpCodes e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a M4 opcode");
        }
        return result;
    }

}
