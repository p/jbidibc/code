package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum M4OpCodesAck implements BidibEnum {

    // @formatter:off
    M4_TID(BidibLibrary.M4_TID, "tid"), M4_BEACON(BidibLibrary.M4_BEACON, "beacon"), 
    M4_SEARCH(BidibLibrary.M4_SEARCH, "search"), M4_BIND_ADDR_ACK(BidibLibrary.M4_BIND_ADDR_ACK, "bind-addr-ack"), 
    M4_UNBIND_ADDR_ACK(BidibLibrary.M4_UNBIND_ADDR_ACK, "unbind-addr-ack"), 
    M4_NEW_LOCO(BidibLibrary.M4_NEW_LOCO, "new-loco"), M4_PING_OKAY(BidibLibrary.M4_PING_OKAY, "ping-ok"),
    M4_SEARCH_ACK(BidibLibrary.M4_SEARCH_ACK, "search-ack");
    // @formatter:on

    private final String key;

    private final byte type;

    private M4OpCodesAck(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create a M4 opcode.
     * 
     * @param type
     *            numeric value of the M4 opcode
     * 
     * @return M4OpCode
     */
    public static M4OpCodesAck valueOf(byte type) {
        M4OpCodesAck result = null;

        for (M4OpCodesAck e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a M4 opcode");
        }
        return result;
    }

}
