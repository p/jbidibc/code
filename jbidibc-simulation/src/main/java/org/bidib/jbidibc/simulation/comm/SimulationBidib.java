package org.bidib.jbidibc.simulation.comm;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.AbstractBidib;
import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.node.AccessoryNode;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.BoosterNode;
import org.bidib.jbidibc.core.node.CommandStationNode;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.net.NetBidib;
import org.bidib.jbidibc.simulation.SimulationInterface;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.jbidibc.simulation.SimulatorRegistry;
import org.bidib.jbidibc.simulation.net.SimulationNetBidib;
import org.bidib.jbidibc.simulation.plaintcp.SimulationNetPlainTcpBidib;
import org.bidib.jbidibc.simulation.serial.SimulationSerialBidib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationBidib implements BidibInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationBidib.class);

    private int firmwarePacketTimeout = BidibInterface.DEFAULT_TIMEOUT;

    private String connectedPortName;

    private boolean isOpened;

    private boolean ignoreWaitTimeout;

    private int responseTimeout;

    private SimulationInterface delegateBidib;

    private SimulationBidib() {
    }

    /**
     * Get a new initialized instance of SimulationBidib.
     *
     * @return the instance of SimulationBidib
     */
    public static BidibInterface createInstance() {
        LOGGER.info("Create new instance of SimulationBidib.");

        SimulationBidib instance = new SimulationBidib();
        instance.initialize();

        return instance;
    }

    /**
     * Initialize the instance. This must only be called from this class
     */
    protected void initialize() {
        LOGGER.info("Initialize SimulationBidib.");
    }

    @Override
    public RootNode getRootNode() {
        RootNode rootNode = null;
        try {
            rootNode = delegateBidib.getRootNode();
        }
        catch (Exception ex) {
            LOGGER.warn("Get the root node failed.", ex);
        }

        if (rootNode != null) {
            String nodeAddress = NodeUtils.formatAddress(rootNode.getAddr());
            SimulatorNode simulator = SimulatorRegistry.getInstance().getSimulator(nodeAddress);
            if (simulator == null) {
                LOGGER.warn("No simulator configured for the root node.");
            }
        }
        else {
            LOGGER.error("The root node is not available.");
        }

        return rootNode;
    }

    @Override
    public void releaseRootNode() {

        if (delegateBidib != null) {
            delegateBidib.releaseRootNode();
        }
    }

    @Override
    public void send(byte[] bytes) {
        LOGGER.info("Send is called with bytes: {}", ByteUtils.bytesToHex(bytes));

        try {
            delegateBidib.send(bytes);
        }
        catch (Exception ex) {
            LOGGER.warn("Send bytes to delegate failed.", ex);
        }
    }

    private static final String SIMULATION_FILENAME = "simulation.xml";

    @Override
    public void open(
        String portName, ConnectionListener connectionListener, Set<NodeListener> nodeListeners,
        Set<MessageListener> messageListeners, Set<TransferListener> transferListeners, final Context context)
        throws PortNotFoundException, PortNotOpenedException {

        LOGGER.info("Open port: {}", portName);

        String simulationFilename = SIMULATION_FILENAME;

        // select the simulation interface
        if ("udp".equals(portName) || "tcp".equals(portName)) {
            // TODO change the fix portName
            portName = portName + ":localhost:" + NetBidib.BIDIB_UDP_PORT_NUMBER;
            delegateBidib = new SimulationNetBidib(portName);
        }
        else if ("plaintcp".equals(portName)) {
            portName = portName + ":localhost:" + NetBidib.BIDIB_UDP_PORT_NUMBER;
            delegateBidib = new SimulationNetPlainTcpBidib(portName);
        }
        else {
            delegateBidib = new SimulationSerialBidib();
            if (StringUtils.endsWithIgnoreCase(portName, ".xml")) {
                LOGGER.info("Use the provided portName as simulationFilename: {}", simulationFilename);
                simulationFilename = portName;
            }
        }

        // make the initialize method accessible and call it ...
        try {
            Method method = AbstractBidib.class.getDeclaredMethod("initialize", null);
            method.setAccessible(true);
            method.invoke(delegateBidib, null);
        }
        catch (Exception ex) {
            LOGGER.warn("Call initialize on delegateBidib failed.", ex);
            throw new PortNotFoundException("Call initialize on delegateBidib failed.");
        }

        delegateBidib.setIgnoreWaitTimeout(ignoreWaitTimeout);
        delegateBidib.setResponseTimeout(responseTimeout);
        delegateBidib.setFirmwarePacketTimeout(firmwarePacketTimeout);
        delegateBidib.setConnectionListener(connectionListener);
        delegateBidib.registerListeners(nodeListeners, messageListeners, transferListeners);

        // start the simulation part
        File file = new File("");
        file = new File(file.getAbsoluteFile(), "data/simulation");

        String userHome = System.getProperty("user.home");
        File searchPathUserHome = new File(userHome, ".bidib/data/simulation");

        String labelPath = context.get("LabelPath", String.class, null);
        File searchPathLabelPath = null;
        if (StringUtils.isNotBlank(labelPath)) {
            searchPathLabelPath = new File(labelPath, "data/simulation");
        }

        LOGGER.info("Search simulationFilename: {}", simulationFilename);

        File simulationConfig =
            searchSimulationConfiguration(simulationFilename,
                new String[] { searchPathUserHome.getAbsolutePath(),
                    (searchPathLabelPath != null ? searchPathLabelPath.getAbsolutePath() : null),
                    file.getAbsolutePath(), "classpath:/simulation" });

        if (simulationConfig == null) {
            LOGGER.warn("No simulation configuration available.");
            throw new PortNotFoundException("No simulation configuration available.");
        }

        delegateBidib.start(simulationConfig);

        // open the interface
        delegateBidib.open(portName, connectionListener, nodeListeners, messageListeners, transferListeners, context);

        connectedPortName = portName;
        isOpened = true;
    }

    private File searchSimulationConfiguration(String filename, String[] searchPaths) {

        LOGGER.info("Search simulation configuration in searchPaths: {}", new Object[] { searchPaths });

        File selectedFile = null;

        for (String searchPath : searchPaths) {

            if (StringUtils.isBlank(searchPath)) {
                LOGGER.info("Skip invalid searchPath: {}", searchPath);
                continue;
            }

            // StringBuilder filename = new StringBuilder(SIMULATION_FILENAME);
            // filename.append(".xml");

            LOGGER.info("Prepared filename to load simulation: {}", filename.toString());
            if (searchPath.startsWith("classpath:")) {
                int beginIndex = "classpath:".length();
                String lookup = searchPath.substring(beginIndex) + "/" + filename.toString();
                LOGGER.info("Lookup simulation file internally: {}", lookup);

                // final StringBuilder filenameSearch = new StringBuilder("*" + SIMULATION_FILENAME);
                // filenameSearch.append(".xml");
                final StringBuilder filenameSearch = new StringBuilder("*" + filename);

                final List<File> files = new LinkedList<>();

                URL pathString = getClass().getResource(lookup);
                LOGGER.info("Prepared pathString: {}", pathString);

                if (pathString == null) {
                    LOGGER.info("No resource for lookup '{}' found.", lookup);
                    continue;
                }

                FileSystem fs = null;
                try {

                    URI lookupURI = pathString.toURI();
                    LOGGER.info("Prepared lookupURI: {}", lookupURI);

                    final String[] array = lookupURI.toString().split("!");

                    Path path = null;
                    if (array.length > 1) {
                        final Map<String, String> env = new HashMap<>();
                        LOGGER.info("Create new filesystem for: {}", array[0]);
                        fs = FileSystems.newFileSystem(URI.create(array[0]), env);
                        path = fs.getPath(array[1]);
                        LOGGER.info("Prepared path: {}", path);
                    }
                    else {
                        path = Paths.get(lookupURI);
                    }

                    final FileSystem fsInner = fs;

                    Files.walkFileTree(path.getParent(), new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                            LOGGER.info("Current file: {}", path);

                            if (FilenameUtils
                                .wildcardMatch(path.toString(), filenameSearch.toString(), IOCase.INSENSITIVE)) {
                                LOGGER.info("Found matching path: {}, absolutePath: {}", path, path.toAbsolutePath());

                                File file = null;
                                if (fsInner != null) {
                                    String filePath = array[0] + "!" + path.toAbsolutePath();
                                    file = new File(filePath);
                                }
                                else {

                                    file = path.toFile();
                                }

                                LOGGER.info("Add matching file: {}", file);

                                files.add(file);
                            }

                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                            if (e == null) {
                                LOGGER.info("Current directory: {}", dir);
                                return FileVisitResult.CONTINUE;
                            }
                            else {
                                // directory iteration failed
                                throw e;
                            }
                        }
                    });
                }
                catch (Exception e) {
                    LOGGER.warn("Convert uri to path failed.", e);
                }
                finally {
                    if (fs != null) {
                        try {
                            fs.close();
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Close filesystem failed.", ex);
                        }
                    }
                }

                LOGGER.info("Found matching files: {}", files);

                if (CollectionUtils.isNotEmpty(files)) {
                    for (File file : files) {

                        selectedFile = file;
                        break;
                    }
                }
            }
            else {
                LOGGER.info("Search for files in searchPath: {}", searchPath);
                File vendorCvFile = new File(searchPath, filename.toString());
                File searchDirectory = vendorCvFile.getParentFile();
                if (searchDirectory.exists()) {
                    // use filename with wildcards
                    // StringBuilder filenameSearch = new StringBuilder(SIMULATION_FILENAME);
                    // filenameSearch.append(".xml");
                    final StringBuilder filenameSearch = new StringBuilder("*" + filename);

                    IOFileFilter fileFilter = new WildcardFileFilter(filenameSearch.toString(), IOCase.INSENSITIVE);
                    Collection<File> files = FileUtils.listFiles(searchDirectory, fileFilter, TrueFileFilter.INSTANCE);

                    LOGGER.info("Found matching files: {}", files);

                    if (CollectionUtils.isNotEmpty(files)) {
                        for (File file : files) {

                            selectedFile = file;
                            LOGGER.info("Use selected file: {}", selectedFile);
                            break;
                        }
                    }
                    // vendorCvFile = findMatchingVendorCV(files, filename.toString(), softwareVersion);

                    // LOGGER.info("Use vendorCvFile: {}", vendorCvFile);
                }
                else {
                    LOGGER.info("The directory to search does not exist: {}", searchDirectory.toString());
                }

                if (selectedFile != null) {
                    LOGGER.info("Found simulation configuration file: {}", selectedFile);
                    break;
                }
            }
        }

        LOGGER.info("Return selectedFileName: {}", selectedFile);
        return selectedFile;
    }

    @Override
    public boolean isOpened() {
        return isOpened;
    }

    @Override
    public void close() {
        LOGGER.info("Close port, connectedPortName: {}", connectedPortName);
        try {
            delegateBidib.close();
        }
        catch (Exception ex) {
            LOGGER.warn("Close delegateBidib failed.", ex);
        }
        isOpened = false;
        connectedPortName = null;

        LOGGER.info("Stop the simulation.");

        try {
            delegateBidib.stop();
        }
        catch (Exception ex) {
            LOGGER.warn("Stop delegateBidib failed.", ex);
        }
    }

    @Override
    public List<String> getPortIdentifiers() {
        return delegateBidib.getPortIdentifiers();
    }

    @Override
    public BidibNode getNode(Node node) {
        return delegateBidib.getNode(node);
    }

    @Override
    public BidibNode findNode(byte[] nodeAddress) {
        return delegateBidib.findNode(nodeAddress);
    }

    @Override
    public AccessoryNode getAccessoryNode(Node node) {
        return delegateBidib.getAccessoryNode(node);
    }

    @Override
    public BoosterNode getBoosterNode(Node node) {
        return delegateBidib.getBoosterNode(node);
    }

    @Override
    public CommandStationNode getCommandStationNode(Node node) {
        return delegateBidib.getCommandStationNode(node);
    }

    @Override
    public BidibMessageProcessor getMessageReceiver() {
        LOGGER.info("getMessageReceiver returns the message receiver of the delegate!");
        return delegateBidib.getMessageReceiver();
    }

    @Override
    public void setIgnoreWaitTimeout(boolean ignoreWaitTimeout) {
        this.ignoreWaitTimeout = ignoreWaitTimeout;
        if (delegateBidib != null) {
            delegateBidib.setIgnoreWaitTimeout(ignoreWaitTimeout);
        }
    }

    @Override
    public void setResponseTimeout(int timeout) {
        // LOGGER.info("Set the response timeout for simulation to: {}", timeout * 1000);
        LOGGER.info("Set the response timeout for simulation to: {}", timeout);
        // this.responseTimeout = timeout * 1000;
        this.responseTimeout = timeout;
        if (delegateBidib != null) {
            delegateBidib.setResponseTimeout(responseTimeout);
        }
    }

    @Override
    public int getResponseTimeout() {
        return delegateBidib.getResponseTimeout();
    }

    @Override
    public int getFirmwarePacketTimeout() {
        return delegateBidib.getFirmwarePacketTimeout();
    }

    @Override
    public void setFirmwarePacketTimeout(int firmwarePacketTimeout) {
        LOGGER.info("Set the firmwarePacketTimeout: {}", firmwarePacketTimeout);
        this.firmwarePacketTimeout = firmwarePacketTimeout;
        if (delegateBidib != null) {
            delegateBidib.setFirmwarePacketTimeout(firmwarePacketTimeout);
        }
    }

    @Override
    public boolean isValidCoreNode(Node node) {
        if (delegateBidib != null) {
            return delegateBidib.isValidCoreNode(node);
        }
        return false;
    }
}
