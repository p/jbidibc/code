package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PortQueryAllMessageTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortQueryAllMessageTest.class);

    @Test
    public void portQueryAllMessageTest() {

        int portTypeMask = 0x0000;
        int rangeFrom = 0;
        int rangeTo = 16;

        LcPortQueryAllMessage message = new LcPortQueryAllMessage(portTypeMask, rangeFrom, rangeTo);
        message.setAddr(new byte[] { 1 });

        LOGGER.debug("Created message: {}", message);
        Assert.assertNotNull(message);

        LOGGER.debug("Created message: {}", ByteUtils.bytesToHex(message.getContent()));

        Assert.assertEquals(message.getData()[0], (byte) 0x00); // port type mask L
        Assert.assertEquals(message.getData()[1], (byte) 0x00); // port type mask H

        Assert.assertEquals(message.getData()[2], (byte) 0x00); // port range from L
        Assert.assertEquals(message.getData()[3], (byte) 0x00); // port range from H

        Assert.assertEquals(message.getData()[4], (byte) 0x10); // port range to L
        Assert.assertEquals(message.getData()[5], (byte) 0x00); // port range to H
    }
}
