package org.bidib.jbidibc.gateway;

import org.bidib.jbidibc.core.message.BidibCommand;

public interface GatewayMessagePeer {

    /**
     * @param bidibCommand
     *            the command to forward
     */
    void forwardMessage(final BidibCommand bidibCommand);
}
