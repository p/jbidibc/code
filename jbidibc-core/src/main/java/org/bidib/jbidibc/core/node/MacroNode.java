package org.bidib.jbidibc.core.node;

import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.exception.ProtocolException;

public interface MacroNode {

    /**
     * Set a macro step.
     * 
     * @param macroStep
     *            the macro step
     * @return the returned macro step from the node
     * @throws ProtocolException
     */
    LcMacro setMacro(LcMacro macroStep) throws ProtocolException;
}
