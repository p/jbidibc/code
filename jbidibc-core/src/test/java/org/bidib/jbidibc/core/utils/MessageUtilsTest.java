package org.bidib.jbidibc.core.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.enumeration.AnalogPortEnum;
import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.SoundPortEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MessageUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageUtilsTest.class);

    @Test
    public void getBacklightPortStatus() {
        LcMacro macro =
            new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.BACKLIGHTPORT.getType(), (byte) 2, (byte) 10);
        byte portStatus = MessageUtils.getPortStatus(macro, LcOutputType.BACKLIGHTPORT);
        Assert.assertEquals(portStatus, 10);
        Assert.assertEquals(macro.getPortValue(LcOutputType.BACKLIGHTPORT), (byte) 10);
        Assert.assertEquals(macro.getBidibPort().getPortNumber(PortModelEnum.type), (byte) 2);
    }

    @Test
    public void getBacklightPortStatusFlat() {
        LcMacro macro = new LcMacro(new byte[] { (byte) 0, (byte) 0, (byte) 50, (byte) 2, (byte) 0, (byte) 10 });
        byte portStatus = MessageUtils.getPortStatus(macro, LcOutputType.BACKLIGHTPORT);
        Assert.assertEquals(portStatus, 10);
        Assert.assertEquals(macro.getPortValue(LcOutputType.BACKLIGHTPORT), (byte) 10);
        Assert.assertEquals(macro.getBidibPort().getPortNumber(PortModelEnum.flat), (byte) 2);
    }

    @Test
    public void getServoPortStatus() {
        LcMacro macro =
            new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.SERVOPORT.getType(), (byte) 2, (byte) 10);
        byte portStatus = MessageUtils.getPortStatus(macro, LcOutputType.SERVOPORT);
        Assert.assertEquals(portStatus, 10);
        Assert.assertEquals(macro.getPortValue(LcOutputType.SERVOPORT), (byte) 10);
        Assert.assertEquals(macro.getBidibPort().getPortNumber(PortModelEnum.type), (byte) 2);
    }

    @Test
    public void getServoPortStatusFlat() {
        LcMacro macro = new LcMacro(new byte[] { (byte) 0, (byte) 0, (byte) 50, (byte) 2, (byte) 0, (byte) 10 });
        byte portStatus = MessageUtils.getPortStatus(macro, LcOutputType.SERVOPORT);
        Assert.assertEquals(portStatus, 10);
        Assert.assertEquals(macro.getPortValue(LcOutputType.SERVOPORT), (byte) 10);
        Assert.assertEquals(macro.getBidibPort().getPortNumber(PortModelEnum.flat), (byte) 2);
    }

    @Test
    public void getSoundPortStatus() {

        byte portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.SOUNDPORT, (byte) 1, SoundPortEnum.PLAY),
                LcOutputType.SOUNDPORT);
        Assert.assertEquals(portStatus, SoundPortEnum.PLAY.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.SOUNDPORT, (byte) 1, SoundPortEnum.STOP),
                LcOutputType.SOUNDPORT);
        Assert.assertEquals(portStatus, SoundPortEnum.STOP.getType());
    }

    @Test
    public void getAnalogPortStatus() {

        byte portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.ANALOGPORT, (byte) 1, AnalogPortEnum.START),
                LcOutputType.ANALOGPORT);
        Assert.assertEquals(portStatus, AnalogPortEnum.START.getType());
    }

    @Test
    public void getSwitchPortStatus() {

        byte portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.SWITCHPORT, (byte) 1, SwitchPortEnum.ON),
                LcOutputType.SWITCHPORT);
        Assert.assertEquals(portStatus, SwitchPortEnum.ON.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.SWITCHPORT, (byte) 1, SwitchPortEnum.OFF),
                LcOutputType.SWITCHPORT);
        Assert.assertEquals(portStatus, SwitchPortEnum.OFF.getType());

    }

    @Test
    public void getLightPortStatus() {

        byte portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.LIGHTPORT, (byte) 10, LightPortEnum.ON),
                LcOutputType.LIGHTPORT);
        Assert.assertEquals(portStatus, LightPortEnum.ON.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.LIGHTPORT, (byte) 1, LightPortEnum.OFF),
                LcOutputType.LIGHTPORT);
        Assert.assertEquals(portStatus, LightPortEnum.OFF.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.LIGHTPORT, (byte) 1, LightPortEnum.BLINKA),
                LcOutputType.LIGHTPORT);
        Assert.assertEquals(portStatus, LightPortEnum.BLINKA.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.LIGHTPORT, (byte) 1, LightPortEnum.DOUBLEFLASH),
                LcOutputType.LIGHTPORT);
        Assert.assertEquals(portStatus, LightPortEnum.DOUBLEFLASH.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.LIGHTPORT, (byte) 1, LightPortEnum.DOWN),
                LcOutputType.LIGHTPORT);
        Assert.assertEquals(portStatus, LightPortEnum.DOWN.getType());

        portStatus =
            MessageUtils.getPortStatus(
                new LcMacro((byte) 0, (byte) 0, (byte) 50, LcOutputType.LIGHTPORT, (byte) 1, LightPortEnum.UP),
                LcOutputType.LIGHTPORT);
        Assert.assertEquals(portStatus, LightPortEnum.UP.getType());
    }

    @Test
    public void toPortStatus() {
        byte value = 0;
        for (LcOutputType outputType : LcOutputType.values()) {
            LOGGER.info("Prepare port status for outputType: {}", outputType);

            BidibEnum portStatus = MessageUtils.toPortStatus(outputType, value);
            if (outputType.hasPortStatus()) {
                Assert.assertNotNull(portStatus);
                Assert.assertEquals(portStatus.getType(), 0);
            }
            else {
                Assert.assertNull(portStatus);
            }
        }
    }

    @Test
    public void getLcConfigX() {

        // 0x82 is assumed to be a real 32-bit value
        byte[] data =
            { 0x00, 0x10, 0x01, (byte) 0xFF, 0x02, 0x00, 0x03, 0x06, 0x04, 0x05, 0x41, 0x02, (byte) 0x80, (byte) 0x82,
                0x02, (byte) 0x80, 0x02, (byte) 0x81 };

        LcConfigX lcConfigX = MessageUtils.getLcConfigX(data);
        Assert.assertNotNull(lcConfigX);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);
        Assert.assertEquals(portConfig.size(), 6);

        Assert.assertEquals((long) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 0x82)),
            ByteUtils.getDWORD(new byte[] { 0x02, (byte) 0x80, 0x02, (byte) 0x81 }));
    }

    @Test
    public void getLcConfigX2() {

        byte[] data =
            { 0x00, 0x10, 0x01, (byte) 0xFF, 0x02, 0x00, 0x03, 0x06, 0x04, 0x05, 0x7F, 0x02, (byte) 0x80, (byte) 0xC1,
                0x02, (byte) 0x80, 0x02, (byte) 0x81 };

        LcConfigX lcConfigX = MessageUtils.getLcConfigX(data);
        Assert.assertNotNull(lcConfigX);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);
        Assert.assertEquals(portConfig.size(), 6);

        Assert.assertEquals((long) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 0xC1)),
            ByteUtils.getDWORD(new byte[] { 0x02, (byte) 0x80, 0x02, (byte) 0x81 }));
    }

    @Test
    public void getLcConfigX3() {
        // 27.01.2015 14:58:04.165: receive MSG_LC_CONFIGX[[2],num=35,type=198,data=[0, 19, 129, 0, 1, 0, 10, 0, 11,
        // 15]] : 0E 02 00 23 C6 00 13 81 00 01 00 0A 00 0B 0F

        // original
        // 26.01.2015 22:40:34.484: receive MSG_LC_CONFIGX[[2],num=36,type=198,data=[0, 19, 129, 0, 1, 0, 10, 0, 11,
        // 15]] : 0E 02 00 24 C6 00 13 81 00 01 00 0A 00 0B 0F

        byte[] data = { 0x00, 0x13, (byte) 0x81, 0x00, 0x01, 0x00, (byte) 0x0A, 0x00, (byte) 0x0B, 0x0F };

        LcConfigX lcConfigX = MessageUtils.getLcConfigX(data);
        Assert.assertNotNull(lcConfigX);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);
        Assert.assertEquals(portConfig.size(), 3);
    }

    @Test
    public void splitBidibMessages() throws ProtocolException, IOException {
        // 11:38:59.289 [WARN] org.bidib.jbidibc.serial.SerialMessageReceiver [Thread-10] - Data remaining in output: 08
        // 00 09 AA 05 03 00 01 00

        // 11.04.2015 11:38:59.320: [Thread-10] - << [2] - 9C FE

        byte[] output = new byte[] { 0x08, 0x00, 0x09, (byte) 0xAA, 0x05, 0x03, 0x00, 0x01, 0x00, (byte) 0x9C };

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(output);

        Collection<byte[]> messages = MessageUtils.splitBidibMessages(outputStream, true);
        Assert.assertNotNull(messages);
        Assert.assertEquals(messages.size(), 1);
    }

    @Test
    public void splitBidibMessages2() throws ProtocolException, IOException {
        // 11.04.2015 12:31:20.747: [Thread-10] - << [10] - FE 05 00 00 81 FD DE AF 89 FE
        // 11.04.2015 12:31:22.356: [Thread-10] - << [9] - 05 00 00 81 FD DE AF 89 FE

        byte[] output = new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF, (byte) 0x89 };

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(output);

        Collection<byte[]> messages = MessageUtils.splitBidibMessages(outputStream, true);
        Assert.assertNotNull(messages);
        Assert.assertEquals(messages.size(), 1);
    }

    @Test
    public void splitBidibMessages3() throws ProtocolException, IOException {

        // split message without CRC check because TCP and UDP do not have message with CRC secured
        byte[] output =
            new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF, 0x05, 0x00, 0x00, (byte) 0x81,
                (byte) 0xFE, (byte) 0xAF };

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(output);

        Collection<byte[]> messages = MessageUtils.splitBidibMessages(outputStream, false);
        Assert.assertNotNull(messages);
        Assert.assertEquals(messages.size(), 2);
    }

    @Test
    public void formatSysError() {
        byte[] data = new byte[] { 1, 2 };

        String message = MessageUtils.formatSysError(new byte[] { 1 }, SysErrorEnum.BIDIB_ERR_SEQUENCE, data);
        LOGGER.info("Formatted message: {}", message);
        Assert.assertNotNull(message);
        Assert.assertEquals(message,
            "BIDIB_ERR_SEQUENCE for address: 01, errorCode: 4 (0x04), last correct sequence: 1, current sequence: 2");
    }

    @Test
    public void formatSysError2() {
        byte[] data = new byte[] { 1 };

        String message = MessageUtils.formatSysError(new byte[] { 1 }, SysErrorEnum.BIDIB_ERR_SEQUENCE, data);
        LOGGER.info("Formatted message: {}", message);
        Assert.assertNotNull(message);
        Assert.assertEquals(message,
            "BIDIB_ERR_SEQUENCE for address: 01, errorCode: 4 (0x04), last correct sequence: 1");
    }
}
