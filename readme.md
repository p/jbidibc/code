﻿# jbidibc - BiDiB client library for Java

## Preparation to build

A working Maven 3 installation is required.

## Build

Run the maven build in the bibid directory: ```mvn install``` (or to rebuild all: ```mvn clean install```).


## Eclipse

Use latest Eclipse (Mars SR1 or newer, m2e tools are included).

Start Eclipse with clean workspace (m2e support added by default). 

## Preparation

### TestNG

Install TestNG Plugin
http://testng.org/doc/download.html

Configure Maven installation in Eclipse:
```Window > Preferences > Maven > Installations > Add ...``` and select your local maven installation to use. 
Click ```Finish``` button in the dialog and check the new maven installation. Press Apply. 
Verify under ```Maven > User Settings``` that the correct user settings (```settings.xml```) and local maven repository location is used if you have 
changed the default settings.

### Import project:
! The maven installation steps must be executed before importing projects !

Import the maven project: ```File -> Import -> Maven > Existing Maven Projects -> 
Root directory > Select the bidib directory > Select the projects -> Finish```

## Maven release

See https://blog.soebes.de/blog/2017/04/02/maven-pom-files-without-a-version-in-it/

Make sure that the ```flattenMode``` is ```bom``` to provide the correct versions in the ```jbidibc-parent``` pom as we need it in other projects.

The following commandline creates a version ```1.11.0``` in the repository:

```mvn clean deploy -Dgpg.keyname="<your keyname>" -Dgpg.passphrase=<your passphrase> -Prelease-sign-artifacts -Drevision=1.11.0```

Login to https://oss.sonatype.org/ and open the staging repositories.



## Git commands

 * ```git rebase -i HEAD~3``` : Rebase interactive with the last 3 commits.
 * ```git pull --autostash --rebase``` : Stash changes and pull latest from remote repo.

## vi shortcuts

 * ```dd``` : cut line into clipboard.
 * ```Shift+P``` : Insert line from clipboard.
