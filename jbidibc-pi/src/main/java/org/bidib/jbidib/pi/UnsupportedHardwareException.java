package org.bidib.jbidib.pi;

public class UnsupportedHardwareException extends Exception {

    private static final long serialVersionUID = 1L;

    public UnsupportedHardwareException(String message) {
        super(message);
    }
}
