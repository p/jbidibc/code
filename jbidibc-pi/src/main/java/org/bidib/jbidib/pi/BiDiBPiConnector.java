package org.bidib.jbidib.pi;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.nio.file.Paths;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;

import eu.ondryaso.ssd1306.Constants;
import eu.ondryaso.ssd1306.Constants.ScrollSpeed;
import eu.ondryaso.ssd1306.Display;

public class BiDiBPiConnector implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(BiDiBPiConnector.class);

    public static final Pin RESET_PIN = RaspiPin.GPIO_04;

    public static final Pin PAIRING_BUTTON_PIN = RaspiPin.GPIO_06;

    public static final Pin PAIRING_LED_PIN = RaspiPin.GPIO_25;

    private GpioController gpio;

    private GpioPinDigitalOutput resetPin;

    private GpioPinDigitalInput pairingButton;

    private GpioPinDigitalOutput pairingLed;

    public BiDiBPiConnector() {

    }

    @Override
    public void close() throws Exception {
        disconnect();
    }

    public static void checkPlatform() throws UnsupportedHardwareException {

        // check if the file exists
        String cpuInfoFileName = "file:/proc/cpuinfo";
        File cpuInfoFile = Paths.get(URI.create(cpuInfoFileName)).toFile();
        if (!cpuInfoFile.exists()) {
            throw new IllegalArgumentException("No CPU info available.");
        }
        if (!cpuInfoFile.canRead()) {
            throw new IllegalArgumentException("The CPU info is not read enabled.");
        }

        Properties props = new Properties();
        try (FileInputStream fis = new FileInputStream(cpuInfoFile)) {
            props.load(fis);
        }
        catch (Exception e) {
            LOGGER.warn("Load content of cpu info failed.", e);
        }

        LOGGER.info("Fetched information from cpuinfo: {}", props);

        String hardware = null;

        for (Entry<Object, Object> entry : props.entrySet()) {
            if (Objects.toString(entry.getKey()).equalsIgnoreCase("Hardware")) {

                hardware = Objects.toString(entry.getValue(), "");
                LOGGER.info("Found hardware: {}", hardware);
                break;
            }
        }

        if (BCM2708.equals(hardware) || BCM2709.equals(hardware) || BCM2835.equals(hardware)) {
            LOGGER.info("Found supported hardware: {}", hardware);
        }
        else {
            LOGGER.warn("Found unsupported hardware: {}", hardware);

            throw new UnsupportedHardwareException("Found unsupported hardware: " + hardware);
        }

    }

    private static final String BCM2708 = "BCM2708";

    private static final String BCM2709 = "BCM2709";

    private static final String BCM2835 = "BCM2835";

    public void connect() {
        LOGGER.info("Connect the GPIO pins for pairing.");

        if (gpio != null) {
            LOGGER.warn("GPIO is assigned already!");
            return;
        }

        // create gpio controller instance
        gpio = GpioFactory.getInstance();

        resetPin =
            gpio.provisionDigitalOutputPin(RESET_PIN, // PIN NUMBER
                "RESET PIN", // PIN FRIENDLY NAME (optional)
                PinState.HIGH); // PIN STARTUP STATE (optional)

        // provision gpio pin #06 as an input pin with its internal pull down resistor enabled
        // (configure pin edge to both rising and falling to get notified for HIGH and LOW state
        // changes)
        pairingButton =
            gpio.provisionDigitalInputPin(PAIRING_BUTTON_PIN, // PIN NUMBER
                "PAIRING BUTTON", // PIN FRIENDLY NAME (optional)
                PinPullResistance.PULL_UP); // PIN RESISTANCE (optional)

        // provision gpio pins #25 as an output pin and make sure is is set to LOW at startup
        pairingLed =
            gpio.provisionDigitalOutputPin(PAIRING_LED_PIN, // PIN NUMBER
                "PAIRING LED", // PIN FRIENDLY NAME (optional)
                PinState.LOW); // PIN STARTUP STATE (optional)

        // configure the pin shutdown behavior; these settings will be
        // automatically applied to the pin when the application is terminated
        // ensure that the LED is turned OFF when the application is shutdown
        pairingLed.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

        // LOGGER.info("Start blinking the pairing led for 2s.");
        //
        // pairingLed.blink(100, 2000, PinState.LOW);

        // create and register gpio pin listener
        pairingButton.addListener(new GpioUsageExampleListener(pairingLock, pairingAllowed));

        initializeDisplay();
    }

    /**
     * Reset the BiDiB-Pi when the GPIO_04 is connected to the reset pin of the processor.
     */
    public void resetBidibPi() {
        LOGGER.info("Reset the BiDiB-Pi with the reset pin connected to GPIO_04.");

        resetPin.pulse(50, PinState.LOW, true);

        LOGGER.info("Reset the BiDiB-Pi with the reset pin connected to GPIO_04 has finished.");
    }

    private Display display;

    private boolean checkSpiAvailable = true;

    private void initializeDisplay() {

        // check if SPI is available
        SpiDevice spiDevice = null;
        try {
            LOGGER.info("Try to get the SPI device.");

            SpiChannel spiChannel = SpiChannel.CS0;

            if (checkSpiAvailable) {
                LOGGER.info("Check if SPI is available until the fix is in pi4j library.");

                // check if the file exists
                String spiFileName = "file:/dev/spidev0." + spiChannel.getChannel();
                File spiDeviceFile = Paths.get(URI.create(spiFileName)).toFile();
                if (!spiDeviceFile.exists()) {
                    throw new IllegalArgumentException("No SPI device \"" + spiFileName + "\" available.");
                }
                if (!spiDeviceFile.canWrite()) {
                    throw new IllegalArgumentException("The SPI device \"" + spiFileName + "\" is not write enabled.");
                }
            }
            else {
                LOGGER.info("Check if SPI is available is disabled, trust in pi4j ...");
            }

            LOGGER.info("Get the SPI device instance of SPI channel 0.");
            spiDevice = SpiFactory.getInstance(spiChannel, 8000000);
        }
        catch (Error e) {
            LOGGER.error("The SPI device is not available!", e);
            return;
        }
        catch (Throwable th) {
            LOGGER.error("The SPI device is not available!", th);
            return;
        }

        try {
            LOGGER.info("Create display.");
            display =
                new Display(Constants.LCD_WIDTH_128, Constants.LCD_HEIGHT_64, GpioFactory.getInstance(), spiDevice,
                    RaspiPin.GPIO_02, RaspiPin.GPIO_03);

            LOGGER.info("Begin display.");

            display.begin();

            LOGGER.info("Change pixels.");
            display.clear();

            // display.displayString("BiDiB ist cool :-)");
            // Thread.sleep(500);

            display.displayString("Wait for client ...");
        }
        catch (Exception ex) {
            LOGGER.warn("Prepare display failed.", ex);
        }
        catch (UnsatisfiedLinkError ex) {
            LOGGER.warn("Get display from SpiFactory failed.", ex);
        }

    }

    public void disconnect() {

        if (gpio != null) {
            LOGGER.info("Shutdown the GPIO.");
            // stop all GPIO activity/threads by shutting down the GPIO controller
            // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
            gpio.shutdown();

            gpio = null;
        }

        shutdownDisplay();
    }

    private void shutdownDisplay() {
        if (display != null) {
            try {
                display.displayString("BiDiB ist cool :-)");
                Thread.sleep(500);
                display.clear();

                display.reset();
            }
            catch (Exception ex) {
                LOGGER.warn("Shutdown display failed.", ex);
            }
        }
    }

    private Object pairingLock = new Object();

    private volatile AtomicBoolean pairingAllowed = new AtomicBoolean();

    public static class GpioUsageExampleListener implements GpioPinListenerDigital {
        private Object pairingLock;

        private AtomicBoolean pairingAllowed;

        protected GpioUsageExampleListener(Object pairingLock, AtomicBoolean pairingAllowed) {
            this.pairingLock = pairingLock;
            this.pairingAllowed = pairingAllowed;
        }

        @Override
        public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            // display pin state on console
            LOGGER.info(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());

            synchronized (pairingLock) {
                pairingAllowed.set(true);
                pairingLock.notifyAll();
            }
        }
    }

    public boolean acceptClient(String remoteHost) {

        LOGGER.info("Check to accept the client from remoteHost: {}", remoteHost);

        pairingAllowed.set(false);

        pairingLed.blink(100, 10000, PinState.LOW);

        if (display != null) {
            showDisplayMessage("Accept client from", remoteHost, " ?");
        }

        synchronized (pairingLock) {
            try {
                pairingLock.wait(10000);
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Wait for pairing lock was interrupted.", ex);
            }
        }

        if (!pairingAllowed.get()) {
            LOGGER.warn("Pairing is not allowed!");
            showDisplayMessage("Pairing is not allowed!");

            pairingLed.blink(200, 800, PinState.LOW);

            showDisplayMessage("Wait for client ...");
        }
        else {
            LOGGER.info("Pairing is allowed!");
            showDisplayMessage("Accepted client from", remoteHost);

            pairingLed.blink(500, 2000, PinState.LOW);
        }

        return pairingAllowed.get();
    }

    private void showDisplayMessage(String message) {
        if (display != null) {
            try {

                int stringWidth = display.checkStringWidth(message);
                LOGGER.info("Width of string '{}': {}", message, stringWidth);

                display.displayString(message);

                if (stringWidth > Constants.LCD_WIDTH_128) {
                    display.scrollHorizontally(true, 0, 50, ScrollSpeed.SPEED_5_FRAMES);
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Show display message failed.", ex);
            }
        }

    }

    private void showDisplayMessage(String... message) {
        if (display != null) {
            try {

                // int stringWidth = display.checkStringWidth(message);
                // LOGGER.info("Width of string '{}': {}", message, stringWidth);

                display.displayString(message);

                // if (stringWidth > Constants.LCD_WIDTH_128) {
                // display.scrollHorizontally(true, 0, 50, ScrollSpeed.SPEED_5_FRAMES);
                // }
            }
            catch (Exception ex) {
                LOGGER.warn("Show display message failed.", ex);
            }
        }

    }
}
