package org.bidib.jbidibc.core;

import java.util.List;

import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.ActivateCoilEnum;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.OccupationState;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusDecoderType;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;

/**
 * The message Listener interface.
 * 
 */
public interface MessageListener {
    /**
     * Signals the address data received by the detector.
     * 
     * @param address
     *            the node address
     * @param detectorNumber
     *            the detector number
     * @param addressData
     *            the address data
     */
    void address(byte[] address, int detectorNumber, List<AddressData> addressData);

    /**
     * Accessory address was signaled from feedback device.
     * 
     * @param address
     *            the node address
     * @param detectorNumber
     *            the local detector number.
     * @param accessoryAddress
     *            the accessory address
     */
    void feedbackAccessory(byte[] address, int detectorNumber, int accessoryAddress);

    /**
     * CV was signaled from feedback device.
     * 
     * @param address
     *            the node address
     * @param decoderAddress
     *            the decoder address
     * @param cvNumber
     *            the CV number
     * @param dat
     *            the date
     */
    void feedbackCv(byte[] address, AddressData decoderAddress, int cvNumber, int dat);

    /**
     * CV was signaled in XPOM message from feedback device.
     * 
     * @param address
     *            the node address
     * @param decoderAddress
     *            the decoder address
     * @param cvNumber
     *            the CV number
     * @param data
     *            the date
     */
    void feedbackXPom(byte[] address, AddressData decoderAddress, int cvNumber, int[] data);

    /**
     * CV was signaled in XPOM message from feedback device.
     * 
     * @param address
     *            the node address
     * @param did
     *            the decoder address as DID
     * @param cvNumber
     *            the CV number
     * @param data
     *            the date
     */
    void feedbackXPom(byte[] address, DecoderIdAddressData did, int cvNumber, int[] data);

    /**
     * Signals the booster diag values.
     * 
     * @param address
     *            the node address
     * @param current
     *            the booster current value
     * @param temperature
     *            the booster temperature
     * @param voltage
     *            the booster voltage
     */
    void boosterDiag(byte[] address, int current, int voltage, int temperature);

    /**
     * Signals the booster state.
     * 
     * @param address
     *            the node address
     * @param state
     *            the booster state
     */
    void boosterState(byte[] address, BoosterState state);

    /**
     * Signals identify state of node.
     * 
     * @param address
     *            the node address
     * @param identifyState
     *            the identify mode of the node
     */
    void identify(byte[] address, IdentifyState identifyState);

    /**
     * Signals the pong result.
     * 
     * @param address
     *            the node address
     * @param marker
     *            the marker byte
     */
    void pong(byte[] address, int marker);

    /**
     * Signals key/input state of node.
     * 
     * @param address
     *            the node address
     * @param keyNumber
     *            the key/input number
     * @param keyState
     *            the state of the key/input
     */
    void key(byte[] address, int keyNumber, int keyState);

    /**
     * Signals confidence of feedback port.
     * 
     * @param address
     *            the node address
     * @param invalid
     *            the invalid (void) flag
     * @param freeze
     *            the freeze state
     * @param noSignal
     *            the confidence was generate by invalid or missing signal
     */
    void confidence(byte[] address, int invalid, int freeze, int noSignal);

    /**
     * Signals feedback port occupation.
     * 
     * @param address
     *            the node address
     * @param detectorNumber
     *            the detector number
     * @param occupationState
     *            the occupation state
     * @param timestamp
     *            the optional timestamp
     */
    default void occupation(byte[] address, int detectorNumber, OccupationState occupationState, Long timestamp) {

    }

    /**
     * Signals feedback position.
     * 
     * @param address
     *            the node address
     * @param decoderAddress
     *            the decoderAddress
     * @param locationType
     *            the location type
     * @param locationAddress
     *            the location address
     */
    void position(byte[] address, int decoderAddress, int locationType, int locationAddress);

    /**
     * Signals the speed with the provided addressData.
     * 
     * @param address
     *            the node address
     * @param addressData
     *            the address data information
     * @param speed
     *            the speed value
     */
    void speed(byte[] address, AddressData addressData, int speed);

    /**
     * Signals that an error message was received.
     * 
     * @param address
     *            the address
     * @param errorCode
     *            the error code
     */
    void error(byte[] address, int errorCode, byte[] reasonData);

    /**
     * Signals that the stall message was received.
     * 
     * @param address
     *            the address
     * @param stall
     *            the stall flag
     */
    void stall(byte[] address, boolean stall);

    /**
     * Signals that an accessory state message was received.
     * 
     * @param address
     *            the address
     * @param accessoryState
     *            the accessory state
     * @param accessoryStateOptions
     *            the accessory state options
     */
    void accessoryState(
        byte[] address, final AccessoryState accessoryState, final AccessoryStateOptions accessoryStateOptions);

    /**
     * Signals the status of a port.
     * 
     * @param address
     *            the node address
     * @param bidibPort
     *            the bidib port
     * @param portStatus
     *            the port status
     */
    void lcStat(byte[] address, final BidibPort bidibPort, int portStatus);

    /**
     * Signals the execution of a command on a port needs some time (> 100ms) to finish execution.
     * 
     * @param address
     *            the node address
     * @param bidibPort
     *            the bidib port
     * @param predRotationTime
     *            the estimated time to finish the execution
     */
    void lcWait(byte[] address, final BidibPort bidibPort, int predRotationTime);

    /**
     * Signals the the port was not able to perform the command or the addressed port is not available.
     * 
     * @param address
     *            the node address
     * @param bidibPort
     *            the bidib port
     * @param errorCode
     *            the optional error code
     */
    void lcNa(byte[] address, final BidibPort bidibPort, Integer errorCode);

    /**
     * Signals the configuration of the port.
     * 
     * @param address
     *            the node address
     * @param lcConfig
     *            the port configuration data
     */
    void lcConfig(byte[] address, LcConfig lcConfig);

    /**
     * Signals the configuration of the port.
     * 
     * @param address
     *            the node address
     * @param lcConfigX
     *            the port configuration data
     */
    void lcConfigX(byte[] address, LcConfigX lcConfigX);

    /**
     * Signals dynamic state data received by the detector.
     * 
     * @param address
     *            the node address
     * @param detectorNumber
     *            the detector number
     * @param decoderAddress
     *            the decoder address
     * @param dynNumber
     *            the dynamic state identifier
     * @param dynValue
     *            the dynamic state value
     * @param timestamp
     *            the optional timestamp value
     */
    void dynState(
        byte[] address, int detectorNumber, AddressData decoderAddress, int dynNumber, int dynValue, Integer timestamp);

    /**
     * Signals the state of the command station programmer.
     * 
     * @param address
     *            the node address
     * @param commandStationProgState
     *            the command station programmer state
     * @param remainingTime
     *            the remaining time
     * @param cvNumber
     *            the CV number
     * @param cvData
     *            the CV value
     */
    void csProgState(
        byte[] address, CommandStationProgState commandStationProgState, int remainingTime, int cvNumber, int cvData);

    /**
     * Signals the state of the command station programmer.
     * 
     * @param address
     *            the node address
     * @param commandStationState
     *            the command station state
     */
    void csState(byte[] address, CommandStationState commandStationState);

    /**
     * Signals the manual drive message.
     * 
     * @param address
     *            the node address
     * @param driveState
     *            the drive state
     */
    void csDriveManual(byte[] address, DriveState driveState);

    /**
     * Signals the drive state message.
     * 
     * @param address
     *            the node address
     * @param driveState
     *            the drive state
     */
    void csDriveState(byte[] address, DriveState driveState);

    /**
     * Signals the manual execution of a dcc accessory.
     * 
     * @param address
     *            the node address
     * @param decoderAddress
     *            the decoder address
     * @param activate
     *            the activate coil value
     * @param aspect
     *            the aspect
     */
    void csAccessoryManual(byte[] address, AddressData decoderAddress, ActivateCoilEnum activate, int aspect);

    /**
     * Signals the dcc accessory acknowledge.
     * 
     * @param address
     *            the node address
     * @param decoderAddress
     *            the decoder address
     * @param acknowledge
     *            the acknowledge
     */
    void csAccessoryAcknowledge(byte[] address, int decoderAddress, AccessoryAcknowledge acknowledge);

    /**
     * Signal the tid event.
     * 
     * @param address
     *            the node address
     * @param tid
     *            the railcom plus tid data
     */
    void csRcPlusTid(byte[] address, TidData tid);

    /**
     * Signal the RailCom+ ping acknowledge state.
     * 
     * @param address
     *            the node address
     * @param phase
     *            the phase
     * @param acknState
     *            the acknowledge state
     */
    void csRcPlusPingAcknState(byte[] address, RcPlusPhase phase, RcPlusAcknowledge acknState);

    /**
     * Signal the RailCom+ bind answer.
     * 
     * @param address
     *            the node address
     * @param decoderAnswer
     *            the decoder answer
     */
    void csRcPlusBindAnswer(byte[] address, RcPlusDecoderAnswerData decoderAnswer);

    /**
     * Signal the RailCom+ find answer.
     * 
     * @param address
     *            the node address
     * @param phase
     *            the phase
     * @param decoderAnswer
     *            the decoder answer
     */
    void csRcPlusFindAnswer(byte[] address, RcPlusPhase phase, RcPlusDecoderAnswerData decoderAnswer);

    /**
     * Signal the RailCom+ bind accepted answer.
     * 
     * @param address
     *            the node address
     * @param detectorNum
     *            the detector number
     * @param decoderType
     *            the decoder type
     * @param rcPlusBindAccepted
     *            the bind accepted answer
     */
    void feedbackRcPlusBindAccepted(
        byte[] address, int detectorNum, RcPlusDecoderType decoderType, RcPlusFeedbackBindData rcPlusBindAccepted);

    /**
     * Signal the RailCom+ ping collision answer.
     * 
     * @param address
     *            the node address
     * @param phase
     *            the phase
     * @param detectorNum
     *            the detector number
     */
    void feedbackRcPlusPingCollision(byte[] address, int detectorNum, RcPlusPhase phase);

    /**
     * Signal the RailCom+ find collision answer.
     * 
     * @param address
     *            the node address
     * @param detectorNum
     *            the detector number
     * @param phase
     *            the phase
     * @param uniqueId
     *            the unique id
     */
    void feedbackRcPlusFindCollision(byte[] address, int detectorNum, RcPlusPhase phase, DecoderUniqueIdData uniqueId);

    /**
     * Signal the RailCom+ pong okay answer.
     * 
     * @param address
     *            the node address
     * @param detectorNum
     *            the detector number
     * @param phase
     *            the phase
     * @param decoderType
     *            the decoder type
     * @param uniqueId
     *            the unique id
     */
    void feedbackRcPlusPongOkay(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId);

    /**
     * Signal the RailCom+ pong new answer.
     * 
     * @param address
     *            the node address
     * @param detectorNum
     *            the detector number
     * @param phase
     *            the phase
     * @param decoderType
     *            the decoder type
     * @param uniqueId
     *            the unique id
     */
    void feedbackRcPlusPongNew(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId);

    /**
     * Signals that a node was lost in the system.
     * 
     * @param address
     *            the parent node address
     * @param node
     *            the lost node
     */
    void nodeLost(byte[] address, Node node);

    /**
     * Signals that a new node was found in the system.
     * 
     * @param address
     *            the parent node address
     * @param node
     *            the new node
     */
    void nodeNew(byte[] address, Node node);

}
