package org.bidib.jbidibc.simulation.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.nodes.MasterType;
import org.bidib.jbidibc.simulation.nodes.NodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationFactory.class);

    private enum VidPidKey {
        GBMBOOST_MASTER(13, 104), GBMBOOST_NODE(13, 103), ONE_DMX(13, 115), NEOCONTROL_LIGHT(13,
            205), NEOCONTROL_SIGNAL(13, 206);

        private String key;

        VidPidKey(int vid, int pid) {
            key = String.format("%d_%d", vid, pid);
        }

        public String getKey() {
            return key;
        }

        public static VidPidKey fromVidAndPid(int vid, int pid) {

            String key = String.format("%d_%d", vid, pid);
            for (VidPidKey current : values()) {
                if (key.equals(current.getKey())) {
                    return current;
                }
            }

            LOGGER.warn("No matching VidPidKey found for combination, vid: {}, pid: {}", vid, pid);

            return null;
        }

    }

    private Map<VidPidKey, String> mapClassName = new HashMap<>();

    public SimulationFactory() {

    }

    public void initialize() {

        mapClassName.put(VidPidKey.GBMBOOST_MASTER, "org.bidib.wizard.simulation.GBMboostMasterSimulator");
        mapClassName.put(VidPidKey.GBMBOOST_NODE, "org.bidib.wizard.simulation.GBMboostNodeSimulator");
        mapClassName.put(VidPidKey.ONE_DMX, "org.bidib.wizard.simulation.OneDMXSimulator");
        mapClassName.put(VidPidKey.NEOCONTROL_LIGHT, "org.bidib.wizard.simulation.NeoControlSimulator");
        mapClassName.put(VidPidKey.NEOCONTROL_SIGNAL, "org.bidib.wizard.simulation.NeoControlSimulator");
    }

    public NodeType toNodeType(final Node node) {

        VidPidKey key =
            VidPidKey.fromVidAndPid(NodeUtils.getVendorId(node.getUniqueId()),
                (int) NodeUtils.getPid(node.getUniqueId(), node.getRelevantPidBits()));

        if (key == null) {
            LOGGER.info("No key registered for node: {}", node);
            return null;
        }

        String className = mapClassName.get(key);
        if (StringUtils.isBlank(className)) {
            LOGGER.info("No simulator classname registered for node: {}", node);
            return null;
        }

        NodeType nodeType = null;
        if (NodeUtils.isAddressEqual(node.getAddr(), NodeUtils.ROOT_ADDRESS)) {

            MasterType masterNode = new MasterType();
            masterNode
                .withAddress(NodeUtils.formatAddress(node.getAddr()))
                .withUniqueId(NodeUtils.getUniqueId(node.getUniqueId())).withClassName(className)
                .withProductName(node.getStoredString(StringData.INDEX_PRODUCTNAME))
                .withUserName(node.getStoredString(StringData.INDEX_USERNAME))
                .withProtocolVersion(node.getProtocolVersion().toString())
                .withSoftwareVersion(node.getSoftwareVersion().toString());

            nodeType = masterNode;
        }
        else {
            NodeType subNode =
                new NodeType()
                    .withAddress(NodeUtils.formatAddress(node.getAddr()))
                    .withUniqueId(NodeUtils.getUniqueId(node.getUniqueId())).withClassName(className)
                    .withProductName(node.getStoredString(StringData.INDEX_PRODUCTNAME))
                    .withUserName(node.getStoredString(StringData.INDEX_USERNAME))
                    .withProtocolVersion(node.getProtocolVersion().toString())
                    .withSoftwareVersion(node.getSoftwareVersion().toString());

            nodeType = subNode;
        }
        return nodeType;
    }

}
