package org.bidib.jbidibc.core;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.DirectionEnum;

public class DriveState {

    public static final int DRIVE_ADDRESS_FORMAT_DCC14 = 0x00;

    public static final int DRIVE_ADDRESS_FORMAT_DCC28 = 0x02;

    public static final int DRIVE_ADDRESS_FORMAT_DCC128 = 0x03;

    public static final int DRIVE_ADDRESS_FORMAT_MM14 = 0x04;

    public static final int DRIVE_ADDRESS_FORMAT_MM27a = 0x05;

    public static final int DRIVE_ADDRESS_FORMAT_MM27b = 0x06;

    public static final int DRIVE_ADDRESS_FORMAT_M4 = 0x07;

    public static final int FUNCTIONS_INDEX_F0_F4 = 0;

    public static final int FUNCTIONS_INDEX_F5_F12 = 1;

    public static final int FUNCTIONS_INDEX_F13_F20 = 2;

    public static final int FUNCTIONS_INDEX_F21_F28 = 3;

    private int address;

    private int format;

    private int outputActive;

    private int speed;

    private DirectionEnum direction;

    private int lights;

    private byte[] functions;

    public DriveState(int address, int format, int outputActive, int speed, int lights) {
        this.address = address;
        this.format = format;
        this.outputActive = outputActive;
        this.speed = speed & 0x7F;
        this.direction = DirectionEnum.valueOf(speed & 0x80);
        this.lights = lights;
    }

    /**
     * @return the address
     */
    public int getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(int address) {
        this.address = address;
    }

    /**
     * @return the format
     */
    public int getFormat() {
        return format;
    }

    /**
     * @param format
     *            the format to set
     */
    public void setFormat(int format) {
        this.format = format;
    }

    /**
     * @return the outputActive
     */
    public int getOutputActive() {
        return outputActive;
    }

    /**
     * @param outputActive
     *            the outputActive to set
     */
    public void setOutputActive(int outputActive) {
        this.outputActive = outputActive;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed & 0x7F;
    }

    /**
     * @param speed
     *            the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed & 0x7F;
    }

    /**
     * @return the direction
     */
    public DirectionEnum getDirection() {
        return direction;
    }

    /**
     * @param direction
     *            the direction to set
     */
    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    /**
     * @return the lights
     */
    public int getLights() {
        return lights;
    }

    /**
     * @param lights
     *            the lights to set
     */
    public void setLights(int lights) {
        this.lights = lights;
    }

    public byte[] getFunctions() {
        return functions;
    }

    public void setFunctions(byte[] functions) {
        this.functions = functions;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
