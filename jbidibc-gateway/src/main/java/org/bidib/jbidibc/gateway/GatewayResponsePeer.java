package org.bidib.jbidibc.gateway;

import org.bidib.jbidibc.core.message.BidibMessage;

public interface GatewayResponsePeer {

    /**
     * @param bidibMessage
     *            the message to forward
     */
    void forwardMessage(final BidibMessage bidibMessage);
}
