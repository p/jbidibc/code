package org.bidib.jbidibc;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.node.AccessoryNode;

import com.beust.jcommander.Parameters;

/**
 * This commands gets the list of macros from the specified node.
 * 
 */
@Parameters(separators = "=")
public class GetMacros extends BidibNodeCommand {

    public static void main(String[] args) {

        run(new GetMacros(), args);
    }

    public int execute() {
        int result = 20;

        try {
            openPort(getPortName(), null);

            Node node = findNode();

            if (node != null) {
                AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

                if (accessoryNode != null) {
                    Feature feature = accessoryNode.getBidibNode().getFeature(BidibLibrary.FEATURE_CTRL_MAC_COUNT);
                    int macroCount = feature != null ? feature.getValue() : 0;

                    feature = accessoryNode.getBidibNode().getFeature(BidibLibrary.FEATURE_CTRL_MAC_SIZE);

                    int macroLength = feature != null ? feature.getValue() : 0;

                    for (int macroNumber = 0; macroNumber < macroCount; macroNumber++) {
                        int stepNumber = 0;

                        System.out.println("Macro " + macroNumber + ":"); // NOSONAR
                        System.out.println("\tcycles: " // NOSONAR
                            + accessoryNode.getMacroParameter(macroNumber, BidibLibrary.BIDIB_MACRO_PARA_REPEAT));
                        System.out.println("\tspeed: " // NOSONAR
                            + accessoryNode.getMacroParameter(macroNumber, BidibLibrary.BIDIB_MACRO_PARA_SLOWDOWN));
                        System.out.println("\tsteps:"); // NOSONAR
                        for (;;) {
                            final LcMacro macroStep = accessoryNode.getMacroStep(macroNumber, stepNumber++);

                            if (macroStep.getBidibPort().getPortType(PortModelEnum.type) == LcOutputType.END_OF_MACRO
                                || stepNumber > macroLength) {
                                break;
                            }
                            System.out.println("\t\t" + stepNumber + ". " + macroStep); // NOSONAR
                        }
                    }
                    result = 0;
                }
                else {
                    System.err.println("node with unique id \"" + getNodeIdentifier() + "\" doesn't have macros"); // NOSONAR
                }
            }
            else {
                System.err.println("node with unique id \"" + getNodeIdentifier() + "\" not found"); // NOSONAR
            }

            getBidib().close();

        }
        catch (PortNotFoundException ex) {
            System.err.println("The provided port was not found: " + ex.getMessage() // NOSONAR
                + ". Verify that the BiDiB device is connected.");
        }
        catch (Exception ex) {
            System.err.println("Execute command failed: " + ex); // NOSONAR
        }
        return result;
    }
}
