package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FeedbackPositionResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPositionResponseTest.class);

    @Test
    public void decoderFeedbackPositionResponse() throws ProtocolException {
        byte[] message = new byte[] { 0x08, 0x00, (byte) 0x1F, (byte) 0xAC, 0x01, 0x02, 0x01, 0x03, 0x04 };
        BidibMessage result = new BidibMessage(message);

        FeedbackPositionResponse feedbackPositionResponse =
            new FeedbackPositionResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(feedbackPositionResponse);
        LOGGER.info("Prepared feedbackPositionResponse: {}", feedbackPositionResponse);

        Assert.assertEquals(feedbackPositionResponse.getDecoderAddress(), 0x0201);
        Assert.assertEquals(feedbackPositionResponse.getLocationType(), 0x01);
        Assert.assertEquals(feedbackPositionResponse.getLocationAddress(), 0x0403);
    }

    @Test(expectedExceptions = {
        ProtocolException.class }, expectedExceptionsMessageRegExp = "No valid MSG_BM_POSITION received.")
    public void decoderInvalidLengthFeedbackPositionResponse() throws ProtocolException {
        byte[] message = new byte[] { 0x07, 0x00, (byte) 0x1F, (byte) 0xAC, 0x01, 0x02, 0x03, 0x04 };
        BidibMessage result = new BidibMessage(message);

        new FeedbackPositionResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.fail();
    }
}
