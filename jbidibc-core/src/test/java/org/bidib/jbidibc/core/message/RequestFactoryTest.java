package org.bidib.jbidibc.core.message;

import java.util.List;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.M4OpCodes;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodes;
import org.bidib.jbidibc.core.enumeration.SpeedStepsEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RequestFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestFactoryTest.class);

    private RequestFactory requestFactory;

    @BeforeClass
    public void init() {
        requestFactory = new RequestFactory();
    }

    @Test
    public void createSysGetMagicTest() throws ProtocolException {
        byte[] message = new byte[] { (byte) 0xFE, 0x03, 0x00, 0x00, 0x01, (byte) 0xD6, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_SYS_GET_MAGIC, ByteUtils.getInt(bidibMessages.get(0).getType()));
    }

    // 22:39:33.288 [INFO] org.bidib.wizard.comm.mock.SimulationBidib [AWT-EventQueue-0] - Send is called with bytes: FE
    // 06 01 00 0C 17 01 32 06 01 00 0D 17 01 33 06 01 00 0E 17 01 34 06 01 00 0F 17 01 35 D4 FE
    @Test
    public void bulkGetVendorTest() throws ProtocolException {
        byte[] message =
            new byte[] { (byte) 0xFE, 0x06, 0x01, 0x00, 0x0C, 0x17, 0x01, 0x32, 0x06, 0x01, 0x00, 0x0D, 0x17, 0x01,
                0x33, 0x06, 0x01, 0x00, 0x0E, 0x17, 0x01, 0x34, 0x06, 0x01, 0x00, 0x0F, 0x17, 0x01, 0x35, (byte) 0xD4,
                (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_VENDOR_GET, ByteUtils.getInt(bidibMessages.get(0).getType()));
    }

    @Test
    public void commandStationDriveMessageTest() throws ProtocolException {
        byte[] message =
            new byte[] { (byte) 0xFE, 0x0C, 0x00, (byte) 0xFC, 0x64, 0x62, 0x02, 0x03, 0x03, 0x22, 0x10, 0x00, 0x00,
                0x00, (byte) 0x1B, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_DRIVE, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationDriveMessage commandStationDriveMessage = (CommandStationDriveMessage) bidibMessages.get(0);

        Assert.assertEquals(commandStationDriveMessage.getSpeedSteps(), SpeedStepsEnum.DCC128);
        Assert.assertEquals(new AddressData(610, AddressTypeEnum.LOCOMOTIVE_BACKWARD),
            commandStationDriveMessage.getDecoderAddress());

        Assert.assertEquals(0x22, commandStationDriveMessage.getSpeed());
    }

    @Test
    public void commandStationDriveMessageM4Test() throws ProtocolException {
        byte[] message =
            new byte[] { (byte) 0xFE, 0x0C, 0x00, (byte) 0xFC, 0x64, 0x62, 0x02, 0x07 /* M4 */, 0x03, 0x22, 0x10, 0x00,
                0x00, 0x00, (byte) 0x1B, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_DRIVE, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationDriveMessage commandStationDriveMessage = (CommandStationDriveMessage) bidibMessages.get(0);

        Assert.assertEquals(commandStationDriveMessage.getSpeedSteps(), SpeedStepsEnum.M4);
        Assert.assertEquals(commandStationDriveMessage.getDecoderAddress(),
            new AddressData(610, AddressTypeEnum.LOCOMOTIVE_BACKWARD));

        Assert.assertEquals(commandStationDriveMessage.getSpeed(), 0x22);
    }

    @Test
    public void commandStationDriveMessageSpeed0Test() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x0C, 0x00, (byte) 0xFC, 0x64, 0x62, 0x02, 0x03, 0x03, 0x00, 0x10, 0x00, 0x00,
                0x00, (byte) 0xB5, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_DRIVE, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationDriveMessage commandStationDriveMessage = (CommandStationDriveMessage) bidibMessages.get(0);

        Assert.assertEquals(new AddressData(610, AddressTypeEnum.LOCOMOTIVE_BACKWARD),
            commandStationDriveMessage.getDecoderAddress());

        Assert.assertEquals(0, commandStationDriveMessage.getSpeed());
    }

    @Test
    public void commandStationRcPlusMessageGetTidTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x04, 0x00, (byte) 0x54, 0x68, BidibLibrary.RC_GET_TID_REQ, (byte) 0x6A,
                (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_RCPLUS, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationRcPlusMessage commandStationRcPlusMessage = (CommandStationRcPlusMessage) bidibMessages.get(0);

        Assert.assertEquals(commandStationRcPlusMessage.getOpCode(), RcPlusOpCodes.RC_GET_TID);
    }

    @Test
    public void commandStationRcPlusMessageSetTidTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x0A, 0x00, (byte) 0x54, 0x68, BidibLibrary.RC_SET_TID_REQ, 0x0A, 0x0B, 0x0C,
                0x0D, 0x0D, 0x04, (byte) 0xB6, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_RCPLUS, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationRcPlusMessage commandStationRcPlusMessage = (CommandStationRcPlusMessage) bidibMessages.get(0);

        Assert.assertEquals(commandStationRcPlusMessage.getOpCode(), RcPlusOpCodes.RC_SET_TID);
        Assert.assertEquals(commandStationRcPlusMessage.getTid(),
            new TidData(new DecoderUniqueIdData(0x0D0C0B0A, 13), 4));
    }

    @Test
    public void commandStationRcPlusMessageBindTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x0A, 0x00, (byte) 0x24, 0x68, BidibLibrary.RC_BIND_REQ, 0x00, 0x03, 0x7C, 0x3E,
                0x0C, 0x00, 0x21, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_RCPLUS, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationRcPlusMessage commandStationRcPlusMessage = (CommandStationRcPlusMessage) bidibMessages.get(0);

        Assert.assertEquals(commandStationRcPlusMessage.getOpCode(), RcPlusOpCodes.RC_BIND);
        // Assert.assertEquals(commandStationRcPlusMessage.getTid(),
        // new TidData(new RcPlusUniqueIdData(0x0D0C0B0A, 13), 4));
    }

    @Test
    public void commandStationM4MessageGetTidTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x04, 0x00, (byte) 0x54, 0x69, BidibLibrary.M4_GET_TID, (byte) 0x6A,
                (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_M4, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationM4Message commandStationM4Message = (CommandStationM4Message) bidibMessages.get(0);

        Assert.assertEquals(commandStationM4Message.getOpCode(), M4OpCodes.M4_GET_TID);
    }

    @Test
    public void commandStationM4MessageSetTidTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x0A, 0x00, (byte) 0x54, 0x69, BidibLibrary.M4_SET_TID, 0x0A, 0x0B, 0x0C, 0x0D,
                0x0D, 0x04, (byte) 0xB6, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_M4, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationM4Message commandStationM4Message = (CommandStationM4Message) bidibMessages.get(0);

        Assert.assertEquals(commandStationM4Message.getOpCode(), M4OpCodes.M4_SET_TID);
        Assert.assertEquals(commandStationM4Message.getTid(), new TidData(new DecoderUniqueIdData(0x0D0C0B0A, 13), 4));
    }

    @Test
    public void commandStationM4MessageBeaconDisabledTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x05, 0x00, (byte) 0x54, 0x69, BidibLibrary.M4_SET_BEACON, 0x00, (byte) 0x6A,
                (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_M4, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationM4Message commandStationM4Message = (CommandStationM4Message) bidibMessages.get(0);

        Assert.assertEquals(commandStationM4Message.getOpCode(), M4OpCodes.M4_SET_BEACON);
    }

    @Test
    public void commandStationM4MessageSearchTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x05, 0x00, (byte) 0x54, 0x69, BidibLibrary.M4_SET_SEARCH, 0x00, (byte) 0x6A,
                (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_M4, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationM4Message commandStationM4Message = (CommandStationM4Message) bidibMessages.get(0);

        Assert.assertEquals(commandStationM4Message.getOpCode(), M4OpCodes.M4_SET_SEARCH);
    }

    @Test
    public void commandStationM4MessageBindTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x0A, 0x00, (byte) 0x24, 0x69, BidibLibrary.M4_BIND_ADDR, 0x00, 0x03, 0x7C, 0x3E,
                0x0C, 0x00, 0x21, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_M4, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationM4Message commandStationM4Message = (CommandStationM4Message) bidibMessages.get(0);

        Assert.assertEquals(commandStationM4Message.getOpCode(), M4OpCodes.M4_BIND_ADDR);
        // Assert.assertEquals(commandStationM4Message.getTid(),
        // new TidData(new RcPlusUniqueIdData(0x0D0C0B0A, 13), 4));
    }

    @Test
    public void commandStationM4MessageUnbindTest() throws ProtocolException {

        byte[] message =
            new byte[] { (byte) 0xFE, 0x0A, 0x00, (byte) 0x24, 0x69, BidibLibrary.M4_UNBIND_ADDR, 0x00, 0x03, 0x7C,
                0x3E, 0x0C, 0x00, 0x21, (byte) 0xFE };
        List<BidibCommand> bidibMessages = requestFactory.create(message);
        LOGGER.info("Created messages: {}", bidibMessages);

        Assert.assertNotNull(bidibMessages);
        Assert.assertEquals(BidibLibrary.MSG_CS_M4, ByteUtils.getInt(bidibMessages.get(0).getType()));

        CommandStationM4Message commandStationM4Message = (CommandStationM4Message) bidibMessages.get(0);

        Assert.assertEquals(commandStationM4Message.getOpCode(), M4OpCodes.M4_UNBIND_ADDR);
        // Assert.assertEquals(commandStationM4Message.getTid(),
        // new TidData(new RcPlusUniqueIdData(0x0D0C0B0A, 13), 4));
    }
}
