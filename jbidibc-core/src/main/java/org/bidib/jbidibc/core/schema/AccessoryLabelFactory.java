package org.bidib.jbidibc.core.schema;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.bidib2.Accessories;
import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public final class AccessoryLabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryLabelFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.bidib2";

    public static final String XSD_LOCATION = "/xsd/bidib2.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/bidib/2.0 xsd/bidib2.xsd";

    private List<Accessory> loadAccessories(InputStream is) {

        List<Accessory> accessories = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(AccessoryLabelFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            JAXBElement<BiDiB> jaxbElement = (JAXBElement<BiDiB>) unmarshaller.unmarshal(is);
            BiDiB bidib = jaxbElement.getValue();
            accessories = bidib.getAccessories().getAccessory();
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load Accessories from file failed.", ex);

            throw new RuntimeException("Load Accessories from file failed.");
        }
        return accessories;
    }

    public void saveAccessoryAspectsLabels(final List<Accessory> accessories, String fileName, boolean gzip) {

        LOGGER.info("Save accessories content to file: {}, accessories: {}", fileName, accessories);
        OutputStream os = null;
        boolean passed = false;
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, JAXB_SCHEMA_LOCATION);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(AccessoryLabelFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            marshaller.setSchema(schema);

            os = new BufferedOutputStream(new FileOutputStream(fileName));
            if (gzip) {
                LOGGER.debug("Use gzip to compress accessories.");
                os = new GZIPOutputStream(os);
            }

            BiDiB bidib = new BiDiB();
            bidib.setSchemaVersion("1.0");
            bidib.withAccessories(new Accessories());
            bidib.getAccessories().getAccessory().addAll(accessories);

            JAXBElement<BiDiB> jaxbElement = new ObjectFactory().createBiDiB(bidib);
            marshaller.marshal(jaxbElement, new OutputStreamWriter(os, Charset.forName("UTF-8")));

            os.flush();

            LOGGER.info("Save accessories content to file passed: {}", fileName);

            passed = true;
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save accessories failed.", ex);

            throw new RuntimeException("Save accessories failed.", ex);
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }

            if (!passed) {
                LOGGER.warn("Delete the file because the export has failed.");
                FileUtils.deleteQuietly(new File(fileName));
            }
        }
    }

    public List<Accessory> getAccessories(String filePath) throws FileNotFoundException {

        LOGGER.info("Load accessories from file: {}", filePath);
        InputStream is = null;
        try {
            if (filePath.startsWith("/")) {
                is = AccessoryLabelFactory.class.getResourceAsStream(filePath);
            }
            else {
                File file = new File(filePath);
                is = new FileInputStream(file);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load saved accessories from file failed: {}", ex.getMessage());

            throw ex;
        }

        List<Accessory> accessories = loadAccessories(is);
        LOGGER.trace("Loaded accessories: {}", accessories);

        return accessories;
    }

}
