package org.bidib.jbidibc.core.schema.validation;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibValidationEventHandler implements ValidationEventHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibValidationEventHandler.class);

    public boolean handleEvent(ValidationEvent event) {
        LOGGER.info("\nEVENT");
        LOGGER.info("SEVERITY:  " + event.getSeverity());
        LOGGER.info("MESSAGE:  " + event.getMessage());
        LOGGER.info("LINKED EXCEPTION:  " + event.getLinkedException());
        LOGGER.info("LOCATOR");
        LOGGER.info("    LINE NUMBER:  " + event.getLocator().getLineNumber());
        LOGGER.info("    COLUMN NUMBER:  " + event.getLocator().getColumnNumber());
        LOGGER.info("    OFFSET:  " + event.getLocator().getOffset());
        LOGGER.info("    OBJECT:  " + event.getLocator().getObject());
        LOGGER.info("    NODE:  " + event.getLocator().getNode());
        LOGGER.info("    URL:  " + event.getLocator().getURL());
        return true;
    }

}
