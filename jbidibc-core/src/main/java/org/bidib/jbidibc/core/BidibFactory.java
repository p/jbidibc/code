package org.bidib.jbidibc.core;

import java.lang.reflect.Method;

import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory class to create the used bidib implementation.
 */
public final class BidibFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibFactory.class);

    /**
     * Create the bidib instance of the provided classname.
     * 
     * @param clazzName
     *            the bidib class
     * @return the instance
     */
    public static BidibInterface createBidib(String clazzName) {
        BidibInterface bidib = null;
        LOGGER.info("Try to create instance of class: {}", clazzName);

        try {
            Class<?> clazz = Class.forName(clazzName);

            Method method = clazz.getDeclaredMethod("createInstance", null);
            bidib = (BidibInterface) method.invoke(null);
        }
        catch (Exception ex) {
            LOGGER.error("Create Bidib instance with createInstance failed: {}", clazzName, ex);

            throw new InvalidConfigurationException("Create Bidib instance failed: " + clazzName);
        }

        LOGGER.info("Return created instance of bidib: {}", bidib);

        return bidib;
    }
}
