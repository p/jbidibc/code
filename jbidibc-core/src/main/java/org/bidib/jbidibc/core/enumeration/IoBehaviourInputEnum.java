package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;

public enum IoBehaviourInputEnum implements BidibEnum {
    // @formatter:off
    ACTIVE_LOW(0, "active_low"), ACTIVE_HIGH(1, "active_high"), 
    ACTIVE_LOW_PULLUP(2, "active_low_pullup"), ACTIVE_HIGH_PULLDOWN(3, "active_high_pulldown"), 
    UNKNOWN(0xFF, "unknown");
    // @formatter:on

    private final byte type;

    private final String key;

    IoBehaviourInputEnum(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public byte getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    /**
     * Create a io behaviour input enum.
     * 
     * @param type
     *            numeric value of the io behaviour input enum
     * 
     * @return IoBehaviourInputEnum
     */
    public static IoBehaviourInputEnum valueOf(byte type) {
        IoBehaviourInputEnum result = null;

        for (IoBehaviourInputEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a io behaviour input enum");
        }
        return result;
    }

    public static IoBehaviourInputEnum[] getValues() {

        return values();
    }
}
