package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum RcPlusOpCodes implements BidibEnum {
    RC_BIND(BidibLibrary.RC_BIND_REQ, "bind"), RC_PING(BidibLibrary.RC_PING_REQ, "ping"), RC_GET_TID(
        BidibLibrary.RC_GET_TID_REQ, "get-tid"), RC_SET_TID(BidibLibrary.RC_SET_TID_REQ, "set-tid"), RC_PING_ONCE_P0(
        BidibLibrary.RC_PING_ONCE_P0, "ping-once-p0"), RC_PING_ONCE(BidibLibrary.RC_PING_ONCE_REQ, "ping-once"), RC_PING_ONCE_P1(
        BidibLibrary.RC_PING_ONCE_P1, "ping-once-p1"), RC_FIND_P0(BidibLibrary.RC_FIND_P0, "find-p0"), RC_FIND_P1(
        BidibLibrary.RC_FIND_P1, "find-p1");

    private final String key;

    private final byte type;

    private RcPlusOpCodes(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create a rcplus opcode.
     * 
     * @param type
     *            numeric value of the rcplus opcode
     * 
     * @return RcPlusOpCode
     */
    public static RcPlusOpCodes valueOf(byte type) {
        RcPlusOpCodes result = null;

        for (RcPlusOpCodes e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a rcplus opcode");
        }
        return result;
    }

}
