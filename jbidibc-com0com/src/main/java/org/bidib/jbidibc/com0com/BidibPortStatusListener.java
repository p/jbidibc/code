package org.bidib.jbidibc.com0com;

public interface BidibPortStatusListener {

    enum PortStatus {
        DISCONNECTED, CONNECTED
    }

    /**
     * The status of the bidib port has changed.
     * 
     * @param portStatus
     *            the new port status
     */
    void statusChanged(PortStatus portStatus);
}
