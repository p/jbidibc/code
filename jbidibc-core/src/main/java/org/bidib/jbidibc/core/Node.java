package org.bidib.jbidibc.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@code Node} class represents some core properties of a node in BiDiB. It's used as kind of primary key to get
 * the {@code BiDiBNode} from the {@code NodeRegistry}.
 */
public class Node {
    private static final Logger LOGGER = LoggerFactory.getLogger(Node.class);

    public static final String PROPERTY_USERNAME = "userName";

    public static final String PROPERTY_PRODUCTNAME = "productName";

    public static final String PROPERTY_FEATURES = "features";

    public static final String PROPERTY_SOFTWARE_VERSION = "softwareVersion";

    public static final String PROPERTY_PROTOCOL_VERSION = "firmwareVersion";

    public static final String PROPERTY_PORT_FLAT_MODEL = "portFlatModel";

    private final byte[] addr;

    private final long uniqueId;

    private final int version;

    private int relevantPidBits = 16;

    private String[] storedStrings;

    private SoftwareVersion softwareVersion;

    private ProtocolVersion protocolVersion;

    private CommandStationState requestedCommandStationState;

    private List<Feature> features = new LinkedList<>();

    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    /**
     * the maximum length for strings that can be stored in the node
     */
    private int stringSize;

    /**
     * the number of total port if the node supports the flat model
     */
    private Integer portFlatModel;

    /**
     * This constructor is used to find a BiDiBNode with the same address in the NodeFactory.
     * 
     * @param addr
     *            the address of the node
     */
    protected Node(byte[] addr) {
        this(0, addr, 0);
    }

    /**
     * This constructor is used when the NodeTabResponse is evaluated.
     * 
     * @param version
     *            the version of the node
     * @param addr
     *            the address of the node
     * @param uniqueId
     *            the uniqueId of the node
     */
    public Node(int version, byte[] addr, long uniqueId) {
        this.addr = addr != null ? addr.clone() : null;
        this.uniqueId = uniqueId;
        this.version = version;

        storedStrings = new String[2];

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Created new node, addr: {}, version: {}, {}", addr, version,
                NodeUtils.getUniqueIdAsString(uniqueId));
        }
    }

    /**
     * @param listener
     *            the property change listener to add
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    /**
     * @param listener
     *            the property change listener to remove
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    /**
     * @return returns address of node
     */
    public byte[] getAddr() {
        return addr;
    }

    /**
     * @return returns the version of the node
     */
    public int getVersion() {
        return version;
    }

    /**
     * @return returns the uniqueId of the node
     */
    public long getUniqueId() {
        return uniqueId;
    }

    /**
     * @return the relevantPidBits
     */
    public int getRelevantPidBits() {
        return relevantPidBits;
    }

    /**
     * @param relevantPidBits
     *            the relevantPidBits to set
     */
    public void setRelevantPidBits(int relevantPidBits) {
        this.relevantPidBits = relevantPidBits;
    }

    /**
     * @return the stringSize
     */
    public int getStringSize() {
        return stringSize;
    }

    /**
     * @param stringSize
     *            the stringSize to set
     */
    public void setStringSize(int stringSize) {
        this.stringSize = stringSize;
    }

    /**
     * Set the string that is stored on the node.
     * 
     * @param index
     *            the index
     * @param value
     *            the string value
     */
    public void setStoredString(int index, String value) {
        if (index < StringData.INDEX_PRODUCTNAME || index > StringData.INDEX_USERNAME) {
            throw new IllegalArgumentException("Index not allowed: " + index);
        }

        String oldValue = storedStrings[index];

        storedStrings[index] = value;

        pcs.firePropertyChange((index == StringData.INDEX_PRODUCTNAME ? PROPERTY_PRODUCTNAME : PROPERTY_USERNAME),
            oldValue, value);
    }

    /**
     * Get the stored string on node at the provided index.
     * 
     * @param index
     *            the index value. Currently {@code StringData.INDEX_PRODUCTNAME} and {@code StringData.INDEX_USERNAME}
     *            are supported.
     * @return the stored string
     */
    public String getStoredString(int index) {
        if (index < StringData.INDEX_PRODUCTNAME || index > StringData.INDEX_USERNAME) {
            throw new IllegalArgumentException("Index not allowed: " + index);
        }
        return storedStrings[index];
    }

    /**
     * @return {@code true} if the node has a stored string, {@code false} if the node has no stored strings
     */
    public boolean hasStoredStrings() {
        return StringUtils.isNotBlank(storedStrings[StringData.INDEX_PRODUCTNAME])
            || StringUtils.isNotBlank(storedStrings[StringData.INDEX_USERNAME]);
    }

    /**
     * @return the softwareVersion
     */
    public SoftwareVersion getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * @param softwareVersion
     *            the softwareVersion to set
     */
    public void setSoftwareVersion(SoftwareVersion softwareVersion) {
        SoftwareVersion oldValue = this.softwareVersion;
        this.softwareVersion = softwareVersion;

        pcs.firePropertyChange(PROPERTY_SOFTWARE_VERSION, oldValue, softwareVersion);
    }

    /**
     * @return the protocolVersion
     */
    public ProtocolVersion getProtocolVersion() {
        return protocolVersion;
    }

    /**
     * @param protocolVersion
     *            the protocolVersion to set
     */
    public void setProtocolVersion(ProtocolVersion protocolVersion) {
        ProtocolVersion oldValue = this.protocolVersion;
        this.protocolVersion = protocolVersion;

        pcs.firePropertyChange(PROPERTY_PROTOCOL_VERSION, oldValue, protocolVersion);
    }

    /**
     * @return the portFlatModel
     */
    public Integer getPortFlatModel() {
        return portFlatModel;
    }

    /**
     * @return is node supports the flat port model
     */
    public boolean isPortFlatModelAvailable() {
        return (portFlatModel != null && portFlatModel.intValue() > 0);
    }

    /**
     * @param portFlatModel
     *            the portFlatModel to set
     */
    public void setPortFlatModel(Integer portFlatModel) {
        LOGGER.info("Set the port flat model value: {}", portFlatModel);
        Integer oldValue = this.portFlatModel;

        this.portFlatModel = portFlatModel;

        pcs.firePropertyChange(PROPERTY_PORT_FLAT_MODEL, oldValue, portFlatModel);
    }

    /**
     * @return the requestedCommandStationState
     */
    public CommandStationState getRequestedCommandStationState() {
        return requestedCommandStationState;
    }

    /**
     * @param requestedCommandStationState
     *            the requestedCommandStationState to set
     */
    public void setRequestedCommandStationState(CommandStationState requestedCommandStationState) {
        this.requestedCommandStationState = requestedCommandStationState;
    }

    /**
     * @return the features
     */
    public List<Feature> getFeatures() {
        return features;
    }

    /**
     * Set a feature.
     * 
     * @param feature
     *            the feature to set
     */
    public void setFeature(Feature feature) {
        LOGGER.info("Set feature: {}", feature);

        Feature existing = Feature.findFeature(this.features, feature.getType());

        if (existing != null) {
            LOGGER.info("Remove existing feature: {}", existing);
            features.remove(existing);
        }

        features.add(feature);

        LOGGER.debug("The feature has changed: {}", feature);
        pcs.firePropertyChange(PROPERTY_FEATURES, null, features);
    }

    /**
     * @param features
     *            the features to set
     */
    public void setFeatures(List<Feature> features) {
        List<Feature> oldValue = new LinkedList<>();
        oldValue.addAll(this.features);

        LOGGER.info("Set the new features: {}", features);

        this.features.clear();
        if (features != null) {
            this.features.addAll(features);
        }
        pcs.firePropertyChange(PROPERTY_FEATURES, oldValue, features);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Node) {
            Node node = (Node) other;
            if (Arrays.equals(node.getAddr(), getAddr()) && node.getUniqueId() == uniqueId) {
                LOGGER.debug("Found equal node: {}", node);
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (NodeUtils.convertAddress(addr) + getUniqueId() + version);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb
            .append("[version=").append(version).append(",addr=").append(Arrays.toString(addr)).append(",uniqueId=")
            .append(String.format("0x%014x", uniqueId & 0xffffffffffffffL));
        if (softwareVersion != null) {
            sb.append(",sw-version=").append(softwareVersion);
        }
        if (protocolVersion != null) {
            sb.append(",protocol-version=").append(protocolVersion);
        }
        sb.append("]");
        return sb.toString();
    }
}
