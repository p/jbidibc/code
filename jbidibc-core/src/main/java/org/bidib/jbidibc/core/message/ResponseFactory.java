package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.exception.ProtocolException;

public interface ResponseFactory {

    /**
     * Create a {@code BidibMessage} from the plain (unencoded) message data.
     * 
     * @param message
     *            the message
     * @return the {@code BidibMessage} instance
     * @throws ProtocolException
     */
    BidibMessage create(byte[] message) throws ProtocolException;
}
