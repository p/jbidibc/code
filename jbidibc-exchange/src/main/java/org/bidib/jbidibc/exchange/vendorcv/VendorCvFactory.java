package org.bidib.jbidibc.exchange.vendorcv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class VendorCvFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(VendorCvFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.exchange.vendorcv";

    public static final String XSD_LOCATION = "/xsd/vendor_cv.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/jbidibc/vendorcv xsd/vendor_cv.xsd";

    private static final String VENDORCV_FILENAME_PREFIX = "BiDiBCV-";

    public static final String VENDORCV_ERRORS = "vendorCVErrors";

    private static final String VENDORCV_FILENAME = "vendorCvFileName";

    private static volatile XMLInputFactory xmlInputFactory;

    private static volatile JAXBContext jaxbContext;

    public static synchronized VendorCvData getCvDefinition(
        final Node node, final Context context, String... searchPaths) {
        LOGGER.info("Load the vendor cv definition for node: {}, searchPaths: {}", new Object[] { node, searchPaths });

        SoftwareVersion softwareVersion = node.getSoftwareVersion();

        if (softwareVersion == null) {
            LOGGER.warn("The mandatory software version is not provided. Skip load CV definition for node: {}", node);
            return null;
        }

        return new VendorCvFactory().loadCvDefintionForNode(node, softwareVersion, context, searchPaths);
    }

    public static synchronized void saveCvDefinition(VendorCvData vendorCVData, File path) {

        new VendorCvFactory().saveCvDefinitionToFile(vendorCVData, path);

    }

    private VendorCvData loadCvDefintionForNode(
        final Node node, SoftwareVersion softwareVersion, final Context context, String... searchPaths) {

        long uniqueId = node.getUniqueId();
        int relevantPidBits = node.getRelevantPidBits();

        int pid = NodeUtils.getPid(uniqueId, relevantPidBits);
        int vid = NodeUtils.getVendorId(uniqueId);
        LOGGER
            .info("Load the vendor cv definition for uniqueId: {}, pid: {}, vid: {}, software version: {}",
                NodeUtils.getUniqueIdAsString(uniqueId), pid, vid, softwareVersion);

        VendorCvData vendorCvData = null;
        for (String searchPath : searchPaths) {
            StringBuilder filename = new StringBuilder(VENDORCV_FILENAME_PREFIX);
            filename.append(vid).append("-").append(pid).append(".xml");

            LOGGER.info("Prepared filename to load vendorCv: {}", filename.toString());
            if (searchPath.startsWith("classpath:")) {
                int beginIndex = "classpath:".length();
                String lookup = searchPath.substring(beginIndex) + "/" /* + filename.toString() */;
                LOGGER.info("Lookup vendorCv file internally: {}", lookup);

                final StringBuilder filenameSearch = new StringBuilder("*" + VENDORCV_FILENAME_PREFIX);
                filenameSearch.append(vid).append("-").append(pid).append("*").append(".xml");

                final List<File> files = new LinkedList<>();

                URL pathString = VendorCvFactory.class.getResource(lookup);
                LOGGER.info("Prepared pathString: {}", pathString);

                if (pathString == null) {
                    LOGGER.info("No resource for lookup '{}' found.", lookup);
                    continue;
                }

                FileSystem fs = null;
                try {

                    URI lookupURI = pathString.toURI();
                    LOGGER.info("Prepared lookupURI: {}", lookupURI);

                    final String[] array = lookupURI.toString().split("!");

                    Path path = null;
                    if (array.length > 1) {
                        final Map<String, String> env = new HashMap<>();
                        LOGGER.info("Create new filesystem for: {}", array[0]);
                        fs = FileSystems.newFileSystem(URI.create(array[0]), env);
                        path = fs.getPath(array[1]);
                        LOGGER.info("Prepared path: {}", path);
                    }
                    else {
                        path = Paths.get(lookupURI);
                    }

                    final FileSystem fsInner = fs;

                    Files.walkFileTree(path.getParent(), new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                            LOGGER.info("Current file: {}", path);

                            if (FilenameUtils
                                .wildcardMatch(path.toString(), filenameSearch.toString(), IOCase.INSENSITIVE)) {
                                LOGGER.info("Found matching path: {}, absolutePath: {}", path, path.toAbsolutePath());

                                File file = null;
                                if (fsInner != null) {
                                    String filePath = array[0] + "!" + path.toAbsolutePath();
                                    file = new File(filePath);
                                }
                                else {

                                    file = path.toFile();
                                }

                                LOGGER.info("Add matching file: {}", file);

                                files.add(file);
                            }

                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                            if (e == null) {
                                LOGGER.info("Current directory: {}", dir);
                                return FileVisitResult.CONTINUE;
                            }
                            else {
                                // directory iteration failed
                                throw e;
                            }
                        }
                    });
                }
                catch (Exception e) {
                    LOGGER.warn("Convert uri to path failed.", e);
                }
                finally {
                    if (fs != null) {
                        try {
                            fs.close();
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Close filesystem failed.", ex);
                        }
                    }
                }

                LOGGER.info("Found matching files: {}", files);
                String selectedFileName = null;
                if (CollectionUtils.isNotEmpty(files)) {
                    List<File> fileCollection = new LinkedList<>();
                    for (File file : files) {

                        fileCollection.add(file);

                    }

                    File vendorCvFile = findMatchingVendorCV(fileCollection, filename.toString(), softwareVersion);
                    LOGGER.info("Matching vendorCvFile: {}", vendorCvFile);
                    if (vendorCvFile != null) {
                        String lookupPath = vendorCvFile.getName();

                        selectedFileName = lookupPath;

                        lookup = searchPath.substring(beginIndex) + "/" + lookupPath;

                        LOGGER.info("Prepared vendor CV lookup: {}", lookup);
                    }
                    else {
                        LOGGER.warn("No matching vendorCV file found.");
                    }
                }
                else {
                    LOGGER.info("No matching vendorCV file found!");
                    lookup = null;
                }

                if (lookup != null) {
                    InputStream is = VendorCvFactory.class.getResourceAsStream(lookup);
                    if (is != null) {
                        LOGGER.info("Lookup directly proceeded: {}", lookup);
                        VendorCV vendorCV = null;
                        try {
                            context.register(VENDORCV_FILENAME, lookup);
                            vendorCV = loadVendorCvFile(is, context);
                        }
                        finally {
                            context.unregister(VENDORCV_FILENAME);
                        }

                        if (vendorCV != null) {
                            if (StringUtils.isBlank(selectedFileName)) {
                                selectedFileName = filename.toString();
                            }
                            vendorCvData = new VendorCvData(vendorCV, selectedFileName, lookup);

                            break;
                        }
                    }
                    else {
                        LOGGER.warn("Internal lookup for products file failed.");
                    }
                }
            }
            else {
                LOGGER.info("Search for files in searchPath: {}", searchPath);
                File vendorCvFile = new File(searchPath, filename.toString());
                File searchDirectory = vendorCvFile.getParentFile();
                if (searchDirectory.exists()) {
                    // use filename with wildcards
                    StringBuilder filenameSearch = new StringBuilder(VENDORCV_FILENAME_PREFIX);
                    filenameSearch.append(vid).append("-").append(pid).append("*").append(".xml");
                    IOFileFilter fileFilter = new WildcardFileFilter(filenameSearch.toString(), IOCase.INSENSITIVE);
                    Collection<File> files = FileUtils.listFiles(searchDirectory, fileFilter, TrueFileFilter.INSTANCE);

                    LOGGER.info("Found matching files: {}", files);

                    vendorCvFile = findMatchingVendorCV(files, filename.toString(), softwareVersion);

                    LOGGER.info("Use vendorCvFile: {}", vendorCvFile);
                }
                else {
                    LOGGER.info("The directory to search does not exist: {}", searchDirectory.toString());
                }

                if (vendorCvFile != null && vendorCvFile.exists()) {
                    LOGGER.info("Found vendorCvFile file: {}", vendorCvFile.getAbsolutePath());
                    // try to load vendorCVs
                    VendorCV vendorCV = loadVendorCvFile(vendorCvFile, context);
                    if (vendorCV != null) {

                        vendorCvData = new VendorCvData(vendorCV, vendorCvFile.getName(), vendorCvFile.getPath());
                        break;
                    }
                }
                else {
                    LOGGER
                        .warn(
                            "No CV defintion file available for node with uniqueId: {}, pid: {}, vid: {}. Used searchpath: {}",
                            uniqueId, pid, vid, searchPath);
                }
            }
        }
        LOGGER.trace("Loaded vendorCvData: {}", vendorCvData);
        return vendorCvData;
    }

    protected File findMatchingVendorCV(
        Collection<File> files, String defaultFilename, SoftwareVersion softwareVersion) {
        if (CollectionUtils.isNotEmpty(files)) {
            LOGGER.info("Search for matching CV definition for software version: {}", softwareVersion);

            File defaultCvDefinition = null;
            File cvDefinition = null;

            // logic to detect the matching CV definition
            SortedMap<Integer, File> weightedMap = new TreeMap<Integer, File>(Collections.reverseOrder());
            for (File file : files) {
                String fileName = FilenameUtils.getBaseName(file.getName());
                LOGGER.info("Check if filename is matching: {}", fileName);

                int index = fileName.lastIndexOf("-");
                if (index > -1 && index < fileName.length()) {
                    String lastPart = fileName.substring(index + 1);
                    LOGGER.info("Check last part: {}", lastPart);
                    // scan version
                    if (lastPart.matches("^\\d+(\\.\\d+){0,2}")) {
                        String[] splited = lastPart.split("\\.");

                        LOGGER.info("Found version schema: {}", (Object[]) splited);

                        int majorVersion = Integer.parseInt(splited[0]);
                        int minorVersion = -1;
                        if (splited.length > 1) {
                            minorVersion = Integer.parseInt(splited[1]);
                        }
                        int microVersion = -1;
                        if (splited.length > 2) {
                            microVersion = Integer.parseInt(splited[2]);
                        }

                        LOGGER
                            .info("Found version, major: {}, minor: {}, micro: {}", majorVersion, minorVersion,
                                microVersion);

                        SoftwareVersion current = new SoftwareVersion(majorVersion, minorVersion, microVersion) {
                            @Override
                            public int compareTo(SoftwareVersion other) {
                                LOGGER.info("Compare sw version, other: {}, this: {}", other, this);

                                int myVersion = toInt();
                                int otherVersion = other.toInt();

                                if (secondVersion == -1) {
                                    // skip compare second and third version
                                    SoftwareVersion myVersion2 = new SoftwareVersion(firstVersion, 255, 255);
                                    myVersion = myVersion2.toInt();
                                    LOGGER
                                        .info("Skip compare the micro version, otherVersion: {}, myVersion: {}",
                                            otherVersion, myVersion);
                                }
                                else if (thirdVersion == -1) {
                                    // skip compare third version
                                    SoftwareVersion myVersion2 = new SoftwareVersion(firstVersion, secondVersion, 255);
                                    myVersion = myVersion2.toInt();
                                    LOGGER
                                        .info("Skip compare the micro version, otherVersion: {}, myVersion: {}",
                                            otherVersion, myVersion);
                                }

                                return (otherVersion - myVersion);
                            }
                        };

                        int difference = current.compareTo(softwareVersion);
                        LOGGER.info("The comparison difference: {}", difference);
                        weightedMap.put(difference, file);
                        if (difference == 0) {
                            LOGGER.info("Found excactly matching version in file: {}", file);

                            cvDefinition = file;
                            break;
                        }
                    }
                    else {
                        LOGGER.info("Last part does not match version schema: {}", lastPart);
                    }
                }

                if (file.getName().equalsIgnoreCase(defaultFilename)) {
                    LOGGER.info("Found the default CV definition: {}", file);
                    defaultCvDefinition = file;
                }
            }

            LOGGER.info("Weighted map: {}", weightedMap);

            if (cvDefinition == null && MapUtils.isNotEmpty(weightedMap)) {
                // get the shortest distance
                for (Entry<Integer, File> weightedKey : weightedMap.entrySet()) {
                    if (weightedKey.getKey() > 0) {
                        continue;
                    }

                    cvDefinition = weightedKey.getValue();
                    LOGGER.info("Use closest cvDefinition: {}", cvDefinition);

                    break;
                }
            }

            if (cvDefinition == null) {
                LOGGER.info("Use defaultCvDefinition: {}", defaultCvDefinition);
                cvDefinition = defaultCvDefinition;
            }
            return cvDefinition;
        }

        return null;
    }

    protected VendorCV loadVendorCvFile(File vendorCvFile, final Context context) {
        VendorCV vendorCV = null;
        context.register(VENDORCV_FILENAME, vendorCvFile.getPath());

        try (InputStream is = new FileInputStream(vendorCvFile)) {

            vendorCV = loadVendorCvFile(is, context);
        }
        catch (FileNotFoundException ex) {
            LOGGER.info("No vendorCV file found.");
        }
        catch (IOException ex) {
            LOGGER.warn("Close input stream failed.", ex);
        }
        finally {
            context.unregister(VENDORCV_FILENAME);
        }
        return vendorCV;
    }

    protected VendorCV loadVendorCvFile(InputStream is, final Context context) {

        VendorCV vendorCV = null;

        try {
            if (xmlInputFactory == null) {
                XMLInputFactory factory = XMLInputFactory.newInstance();
                factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.TRUE);
                factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
                factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, Boolean.FALSE);
                factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
                factory.setXMLResolver(new XMLResolver() {
                    public Object resolveEntity(String publicID, String systemID, String baseURI, String namespace)
                        throws XMLStreamException {
                        throw new XMLStreamException("Reading external entities is disabled");
                    }
                });
                xmlInputFactory = factory;
            }

            if (jaxbContext == null) {
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(VendorCvFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            vendorCV = (VendorCV) unmarshaller.unmarshal(xmlInputFactory.createXMLStreamReader(is));
        }
        catch (JAXBException | SAXException | XMLStreamException ex) {
            LOGGER.warn("Load VendorCV from file failed.", ex);

            List<VendorCvError> vendorCVErrors = context.get(VENDORCV_ERRORS, List.class, null);
            if (vendorCVErrors == null) {
                vendorCVErrors = new ArrayList<>();
                context.register(VENDORCV_ERRORS, vendorCVErrors);
            }
            if (ex instanceof UnmarshalException) {
                String message = ((UnmarshalException) ex).getLinkedException().getMessage();
                vendorCVErrors
                    .add(new VendorCvError(message, context.get(VENDORCV_FILENAME, String.class, "<unknown>")));
            }
            else {
                vendorCVErrors
                    .add(new VendorCvError(ex.getMessage(), context.get(VENDORCV_FILENAME, String.class, "<unknown>")));
            }
        }
        return vendorCV;
    }

    private void saveCvDefinitionToFile(VendorCvData vendorCVData, File path) {
        LOGGER.info("Save vendorCV content to file: {}, path: {}", vendorCVData, path);
        OutputStream os = null;
        boolean passed = false;
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, JAXB_SCHEMA_LOCATION);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(VendorCvFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            marshaller.setSchema(schema);

            os = new BufferedOutputStream(new FileOutputStream(path));

            marshaller.marshal(vendorCVData.getVendorCV(), new OutputStreamWriter(os, Charset.forName("UTF-8")));

            os.flush();

            LOGGER.info("Save VendorCV content to file passed: {}", path);

            passed = true;
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save VendorCV failed.", ex);

            throw new RuntimeException("Save VendorCV failed.", ex);
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }

            if (!passed) {
                LOGGER.warn("Delete the file because the copy has failed.");
                FileUtils.deleteQuietly(path);
            }
        }

    }

    public static Integer getNumberOfPorts(String portType, Node node, String... searchPaths) {
        LOGGER.info("Load number of configured port from the vendor cv definition for node: {}", node);

        return new VendorCvFactory().internalGetNumberOfPorts(portType, node, searchPaths);
    }

    private Integer internalGetNumberOfPorts(String portType, final Node node, String... searchPaths) {
        long uniqueId = node.getUniqueId();
        int relevantPidBits = node.getRelevantPidBits();

        int pid = NodeUtils.getPid(uniqueId, relevantPidBits);
        int vid = NodeUtils.getVendorId(uniqueId);
        LOGGER
            .info("Load the vendor cv definition for uniqueId: {}, pid: {}, vid: {}",
                NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        Integer numberOfPorts = null;
        for (String searchPath : searchPaths) {
            StringBuilder filename = new StringBuilder("BiDiBCV-");
            filename.append(vid).append("-").append(pid).append(".xml");

            LOGGER.info("Prepared filename to load vendorCv: {}", filename.toString());
            if (searchPath.startsWith("classpath:")) {
                int beginIndex = "classpath:".length();
                String lookup = searchPath.substring(beginIndex) + "/" + filename.toString();
                LOGGER.info("Lookup vendorCv file internally: {}", lookup);
                InputStream is = VendorCvFactory.class.getResourceAsStream(lookup);
                if (is != null) {
                    numberOfPorts = getNumberOfPorts(is, portType);
                    if (numberOfPorts != null) {
                        break;
                    }
                }
                else {
                    LOGGER.warn("Internal lookup for products file failed.");
                }
            }
            else {
                File productsFile = new File(searchPath, filename.toString());
                if (productsFile.exists()) {
                    LOGGER.info("Found product file: {}", productsFile.getAbsolutePath());
                    // try to get the number of ports
                    numberOfPorts = getNumberOfPorts(productsFile, portType);
                    if (numberOfPorts != null) {
                        break;
                    }
                }
                else {
                    LOGGER.info("File does not exist: {}", productsFile.getAbsolutePath());
                }
            }
        }
        LOGGER.trace("Loaded numberOfPorts: {}", numberOfPorts);
        return numberOfPorts;
    }

    public Integer getNumberOfPorts(InputStream is, String portType) {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document xmlDocument = null;
        try {
            builder = builderFactory.newDocumentBuilder();

            xmlDocument = builder.parse(is);
        }
        catch (Exception e) {
            LOGGER.warn("Parse document failed.", e);
        }

        Integer numberOfPorts = null;
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "VendorCV/CVDefinition/Node/Node[@Template='" + portType + "']/@Count";
        try {
            String configuredPorts = xPath.compile(expression).evaluate(xmlDocument);
            LOGGER.info("Found requested node: {}", configuredPorts);
            numberOfPorts = Integer.valueOf(configuredPorts);
        }
        catch (XPathExpressionException e) {
            LOGGER.warn("Get number of port failed.", e);
        }

        return numberOfPorts;
    }

    public Integer getNumberOfPorts(File vendorCvFile, String portType) {
        Integer numberOfPorts = null;

        try (InputStream is = new FileInputStream(vendorCvFile)) {

            numberOfPorts = getNumberOfPorts(is, portType);
        }
        catch (FileNotFoundException ex) {
            LOGGER.info("No vendorCV file found.");
        }
        catch (IOException ex) {
            LOGGER.warn("Close input stream failed.", ex);
        }
        return numberOfPorts;
    }
}
