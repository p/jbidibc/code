package org.bidib.jbidibc.ui.plaf.synth;

import java.lang.reflect.Field;

import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.synth.SynthSliderUI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogSynthSliderUI extends SynthSliderUI {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogSynthSliderUI.class);

    private Field trackBorderField;

    public LogSynthSliderUI(JSlider c) {
        super(c);

        // TODO make the trackBorder visible
        try {
            trackBorderField = SynthSliderUI.class.getDeclaredField("trackBorder");
            trackBorderField.setAccessible(true);
        }
        catch (Exception ex) {
            LOGGER.warn("Make the trackBorder accessible failed.", ex);
        }
    }

    public static ComponentUI createUI(JComponent c) {
        return new LogSynthSliderUI((JSlider) c);
    }

    private int getTrackBorder() {
        int trackBorder = 0;
        if (trackBorderField != null) {
            try {
                Number val = (Number) trackBorderField.get(this);
                trackBorder = val.intValue();
            }
            catch (Exception ex) {
                // TODO: handle exception
            }
        }
        return trackBorder;
    }

    @Override
    protected int xPositionForValue(int value) {
        int min = slider.getMinimum();
        int max = slider.getMaximum();
        int trackLeft = trackRect.x + thumbRect.width / 2 + getTrackBorder();
        int trackRight = trackRect.x + trackRect.width - thumbRect.width / 2 - getTrackBorder();
        int trackLength = trackRight - trackLeft;
        // double valueRange = (double) max - (double) min;
        double valueRange = (double) Math.log(max) - (double) Math.log(min);
        double pixelsPerValue = (double) trackLength / valueRange;
        int xPosition;

        if (!drawInverted()) {
            xPosition = trackLeft;
            // xPosition += Math.round(pixelsPerValue * ((double) value - min));
            xPosition += Math.round(pixelsPerValue * ((double) Math.log(value) - Math.log(min)));
        }
        else {
            xPosition = trackRight;
            // xPosition -= Math.round(pixelsPerValue * ((double) value - min));
            xPosition -= Math.round(pixelsPerValue * ((double) Math.log(value) - Math.log(min)));
        }

        xPosition = Math.max(trackLeft, xPosition);
        xPosition = Math.min(trackRight, xPosition);

        return xPosition;
    }

    @Override
    public int valueForXPosition(int xPos) {
        int value;
        int minValue = slider.getMinimum();
        int maxValue = slider.getMaximum();
        int trackLeft = trackRect.x + thumbRect.width / 2 + getTrackBorder();
        int trackRight = trackRect.x + trackRect.width - thumbRect.width / 2 - getTrackBorder();
        int trackLength = trackRight - trackLeft;

        if (xPos <= trackLeft) {
            value = drawInverted() ? maxValue : minValue;
        }
        else if (xPos >= trackRight) {
            value = drawInverted() ? minValue : maxValue;
        }
        else {
            int distanceFromTrackLeft = xPos - trackLeft;
            // double valueRange = (double) maxValue - (double) minValue;
            double valueRange = (double) Math.log(maxValue) - (double) Math.log(minValue);
            // double valuePerPixel = valueRange / (double) trackLength;
            // int valueFromTrackLeft = (int) Math.round(distanceFromTrackLeft * valuePerPixel);
            int valueFromTrackLeft =
                (int) Math.round(Math.pow(Math.E,
                    Math.log(minValue) + ((((double) distanceFromTrackLeft) * valueRange) / (double) trackLength)));

            value = drawInverted() ? maxValue - valueFromTrackLeft : minValue + valueFromTrackLeft;
        }
        return value;
    }

}
