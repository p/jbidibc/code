package org.bidib.jbidibc.usbstickbasis.adapter;

import org.bidib.jbidibc.debug.LineEndingEnum;

import com.jgoodies.binding.beans.Model;

public class UsbStickBasisModel extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SELECTED_PORT = "selectedPort";

    public static final String PROPERTY_BAUDRATE = "baudRate";

    public static final String PROPERTY_CONNECTED = "connected";

    public static final String PROPERTY_DISCONNECTED = "disconnected";

    public static final String PROPERTY_LINE_ENDING = "lineEnding";

    private String selectedPort;

    private Integer baudRate;

    private boolean connected;

    private LineEndingEnum lineEnding = LineEndingEnum.CRLF;

    /**
     * @return the selectedPort
     */
    public String getSelectedPort() {
        return selectedPort;
    }

    /**
     * @param selectedPort
     *            the selectedPort to set
     */
    public void setSelectedPort(String selectedPort) {
        String oldValue = this.selectedPort;

        this.selectedPort = selectedPort;
        firePropertyChange(PROPERTY_SELECTED_PORT, oldValue, selectedPort);
    }

    /**
     * @return the baudRate
     */
    public Integer getBaudRate() {
        return baudRate;
    }

    /**
     * @param baudRate
     *            the baudRate to set
     */
    public void setBaudRate(Integer baudRate) {
        Integer oldValue = this.baudRate;
        this.baudRate = baudRate;
        firePropertyChange(PROPERTY_BAUDRATE, oldValue, baudRate);
    }

    /**
     * @return the connected
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * @param connected
     *            the connected to set
     */
    public void setConnected(boolean connected) {
        boolean oldValue = this.connected;
        this.connected = connected;
        firePropertyChange(PROPERTY_CONNECTED, oldValue, connected);
        firePropertyChange(PROPERTY_DISCONNECTED, !oldValue, !connected);
    }

    /**
     * @return the disconnected state
     */
    public boolean isDisconnected() {
        return !connected;
    }

    /**
     * @param disconnected
     *            the connected to set
     */
    public void setDisconnected(boolean disconnected) {
        setConnected(!disconnected);
    }

    /**
     * @return the lineEnding
     */
    public LineEndingEnum getLineEnding() {
        return lineEnding;
    }

    /**
     * @param lineEnding
     *            the lineEnding to set
     */
    public void setLineEnding(LineEndingEnum lineEnding) {
        LineEndingEnum oldValue = this.lineEnding;
        this.lineEnding = lineEnding;
        firePropertyChange(PROPERTY_LINE_ENDING, oldValue, lineEnding);
    }
}
