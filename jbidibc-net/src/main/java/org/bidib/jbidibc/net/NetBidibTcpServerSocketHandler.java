package org.bidib.jbidibc.net;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the handler for TCP client connections. Every client connection is handled with its own handler
 * instance.
 */
public class NetBidibTcpServerSocketHandler extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibTcpServerSocketHandler.class);

    private Socket socket;

    private final NetMessageHandler messageReceiver;

    private AtomicBoolean runEnabled = new AtomicBoolean();

    private ByteArrayOutputStream output = new ByteArrayOutputStream(2048);

    private final String remoteHost;

    public NetBidibTcpServerSocketHandler(final Socket socket, final NetMessageHandler messageReceiver) {
        this.socket = socket;
        this.messageReceiver = messageReceiver;

        remoteHost = socket.getInetAddress().getHostAddress();
    }

    public void run() {
        runEnabled.set(true);

        byte[] receiveData = new byte[1024];
        try (BufferedInputStream in = new BufferedInputStream(socket.getInputStream());) {
            int receivedCount = 0;

            // wait for client sending data
            while ((receivedCount = in.read(receiveData)) > 0 && runEnabled.get()) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Received data from tcp socket, len: {}, data: {}.", receivedCount,
                        ByteUtils.bytesToHex(receiveData, receivedCount));
                }

                // forward processing to handler
                if (messageReceiver != null) {
                    LOGGER.info("Received a packet. Forward packet to messageReveiver: {}", messageReceiver);

                    LOGGER.info("Received data: {}", ByteUtils.bytesToHex(receiveData, receivedCount));

                    InetAddress address = socket.getInetAddress();
                    int portNumber = socket.getPort();

                    output.write(receiveData, 0, receivedCount);

                    try {
                        parsePackets(output, messageReceiver, address, portNumber);
                    }
                    catch (Exception pex) {
                        LOGGER.warn("Receive message failed.", pex);
                    }

                    // do not clear here because this is managed from within parsePackets
                }
                else {
                    LOGGER.warn("No message receiver configured, data: {}",
                        ByteUtils.bytesToHex(receiveData, receivedCount));
                }
            }
        }
        catch (IOException ex) {
            if (runEnabled.get()) {
                LOGGER.warn("--- Interrupt NetBidibTcpPort-run", ex);
            }
            else {
                LOGGER.info("The NetBidibTcpPort worker is terminating.");
            }
        }
        finally {
            LOGGER.info("The socket connection was closed.");
            if (socket != null) {
                try {
                    socket.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close socket failed.", ex);
                }
                socket = null;
            }

            if (messageReceiver != null) {
                LOGGER.info("Cleanup the messageReceiver.");
                // say goodbye to the message receiver to cleanup resources
                messageReceiver.cleanup(remoteHost);
            }
            LOGGER.info("Cleanup work after close socked has finished.");
        }
    }

    protected void parsePackets(
        final ByteArrayOutputStream output, NetMessageHandler messageReceiver, InetAddress address, int portNumber)
        throws ProtocolException {

        // call utils
        NetBidibPacketUtils.parsePackets(output, messageReceiver, address, portNumber);

    }
}