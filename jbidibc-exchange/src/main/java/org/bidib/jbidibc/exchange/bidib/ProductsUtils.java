package org.bidib.jbidibc.exchange.bidib;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.schema.bidib.products.ProductType;
import org.bidib.jbidibc.core.schema.bidib2.common.DocumentationType;

public class ProductsUtils {

    public static DocumentationType getDocumentationOfLanguage(ProductType product, String language) {
        if (!CollectionUtils.isEmpty(product.getDocumentation())) {
            for (DocumentationType documentation : product.getDocumentation()) {
                if (language.equalsIgnoreCase(documentation.getLanguage())) {
                    return documentation;
                }
            }
        }
        return null;
    }
}
