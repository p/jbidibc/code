package org.bidib.jbidibc.serial;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.MessageProcessor;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerialMessageParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(SerialMessageParser.class);

    protected static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("RAW");

    private ByteArrayOutputStream receiveBuffer = new ByteArrayOutputStream(2048);

    private boolean escapeHot;

    public boolean isEscapeHot() {
        return escapeHot;
    }

    public void setEscapeHot(boolean escapeHot) {
        this.escapeHot = escapeHot;
    }

    public void reset() {
        synchronized (receiveBuffer) {
            MSG_RAW_LOGGER.info("Reset the receiveBuffer in SerialMessageParser.");
            receiveBuffer.reset();
        }
    }

    /**
     * Parse the received data to process the received bidib packets.
     * 
     * @param messageProcessor
     *            the message processor that receives the bidib messages
     * @param receivedData
     *            the received data
     * @param len
     *            the len of the recevided data packet
     * @throws ProtocolException
     */
    public void parseInput(final MessageProcessor messageProcessor, final byte[] receivedData, int len)
        throws ProtocolException {

        if (receivedData != null) {
            int data = 0;

            MSG_RAW_LOGGER.info("<<<< len: {}, data: {}", len, ByteUtils.bytesToHex(receivedData, len));

            synchronized (receiveBuffer) {
                // iterate over the content of the new data
                for (int index = 0; index < len; index++) {
                    data = (receivedData[index] & 0xFF);
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace("received data: {}", ByteUtils.byteToHex(data));
                    }

                    // check if the current is the end of a packet
                    if (data == BidibLibrary.BIDIB_PKT_MAGIC) {

                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Received raw message: {}", ByteUtils.bytesToHex(receiveBuffer));
                        }
                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
                            MSG_RAW_LOGGER.info("<< [{}] - {}", receiveBuffer.size(),
                                ByteUtils.bytesToHex(receiveBuffer));
                        }

                        // if a CRC error is detected in splitMessages the reading loop will terminate ...
                        try {
                            messageProcessor.processMessages(receiveBuffer);
                        }
                        catch (ProtocolException ex) {
                            LOGGER.warn("Process messages failed.", ex);
                            // reset the escape hot flag
                            escapeHot = false;
                        }

                        // after process messages there could be more data in the stream that will be continued with
                        // the next packet
                    }
                    else if (data == BidibLibrary.BIDIB_PKT_ESCAPE) {
                        escapeHot = true;
                    }
                    else {
                        if (escapeHot) {
                            data ^= 0x20;
                            escapeHot = false;
                        }
                        // append data to output array
                        receiveBuffer.write(ByteUtils.getLowByte(data));
                    }
                }

                if (receiveBuffer.size() > 0) {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Data remaining in output: {}", ByteUtils.bytesToHex(receiveBuffer));
                    }
                }
            }

        }
        else {
            LOGGER.error("No input available.");
        }
    }

}
