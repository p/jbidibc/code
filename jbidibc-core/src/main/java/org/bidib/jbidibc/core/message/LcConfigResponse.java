package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.LcConfig;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcConfigResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_LC_CONFIG;

    LcConfigResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 6) {
            throw new ProtocolException("No valid MSG_LC_CONFIG received.");
        }
    }

    public LcConfigResponse(byte[] addr, int num, BidibPort bidibPort, byte[] config) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_LC_CONFIG, ByteUtils.concat(bidibPort.getValues(), config));
    }

    public String getName() {
        return "MSG_LC_CONFIG";
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port type
     */
    public LcOutputType getPortType(PortModelEnum portModel) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat_extended:
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(getData()[0]);
                break;
        }
        return outputType;
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port number
     */
    public int getPortNumber(PortModelEnum portModel) {
        int portNumber = -1;
        switch (portModel) {
            case flat_extended:
                portNumber = ByteUtils.getInt(getData()[0], getData()[0] & 0x7F);
                break;
            case flat:
                portNumber = ByteUtils.getInt(getData()[0]);
                break;
            default:
                portNumber = ByteUtils.getInt(getData()[1], 0x7F);
                break;
        }
        return portNumber;
    }

    public LcConfig getLcConfig() {
        byte[] data = getData();

        BidibPort bidibPort = BidibPort.prepareBidibPort(data[0], data[1]);
        return new LcConfig(bidibPort, ByteUtils.getInt(data[2]), ByteUtils.getInt(data[3]), ByteUtils.getInt(data[4]),
            ByteUtils.getInt(data[5]));
    }
}
