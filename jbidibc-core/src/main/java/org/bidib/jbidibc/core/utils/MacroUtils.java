package org.bidib.jbidibc.core.utils;

import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroUtils.class);

    /**
     * @return returns the initialized macro point
     */
    public static LcMacro getMacro(byte[] data) {

        try {
            return new LcMacro(data);
        }
        catch (Exception ex) {
            LOGGER.warn("Convert macro data to LcMacro failed: {}", ByteUtils.bytesToHex(data), ex);

            throw new InvalidConfigurationException("Convert macro data to LcMacro failed.");
        }
    }

}
