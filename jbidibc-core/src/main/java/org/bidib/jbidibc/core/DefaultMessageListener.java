package org.bidib.jbidibc.core;

import java.util.List;

import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.ActivateCoilEnum;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusDecoderType;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;

/**
 * Default MessageListener implementation with empty method body.
 */
public abstract class DefaultMessageListener implements MessageListener {

    @Override
    public void address(byte[] address, int detectorNumber, List<AddressData> addressData) {
        // no implementation
    }

    @Override
    public void boosterDiag(byte[] address, int current, int voltage, int temperature) {
        // no implementation
    }

    @Override
    public void boosterState(byte[] address, BoosterState state) {
        // no implementation
    }

    @Override
    public void confidence(byte[] address, int invalid, int freeze, int noSignal) {
        // no implementation
    }

    @Override
    public void position(byte[] address, int decoderAddress, int locationType, int locationAddress) {
        // no implementation
    }

    @Override
    public void speed(byte[] address, AddressData addressData, int speed) {
        // no implementation
    }

    @Override
    public void identify(byte[] address, IdentifyState identifyState) {
        // no implementation
    }

    @Override
    public void pong(byte[] address, int marker) {
        // no implementation
    }

    @Override
    public void key(byte[] address, int keyNumber, int keyState) {
        // no implementation
    }

    @Override
    public void error(byte[] address, int errorCode, byte[] reasonData) {
        // no implementation
    }

    @Override
    public void stall(byte[] address, boolean stall) {
        // no implementation
    }

    @Override
    public void accessoryState(
        byte[] address, final AccessoryState accessoryState, final AccessoryStateOptions accessoryStateOptions) {
        // no implementation
    }

    @Override
    public void lcStat(byte[] address, BidibPort bidibPort, int portStatus) {
        // no implementation
    }

    @Override
    public void lcWait(byte[] address, BidibPort bidibPort, int predRotationTime) {
        // no implementation
    }

    @Override
    public void lcNa(byte[] address, BidibPort bidibPort, Integer errorCode) {
        // no implementation
    }

    @Override
    public void lcConfig(byte[] address, LcConfig lcConfig) {
        // no implementation
    }

    @Override
    public void lcConfigX(byte[] address, LcConfigX lcConfigX) {
        // no implementation
    }

    @Override
    public void feedbackAccessory(byte[] address, int detectorNumber, int accessoryAddress) {
        // no implementation
    }

    @Override
    public void feedbackCv(byte[] address, AddressData decoderAddress, int cvNumber, int dat) {
        // no implementation
    }

    @Override
    public void feedbackXPom(byte[] address, AddressData decoderAddress, int cvNumber, int[] data) {
        // no implementation
    }

    @Override
    public void feedbackXPom(byte[] address, DecoderIdAddressData did, int cvNumber, int[] data) {
        // no implementation
    }

    @Override
    public void dynState(
        byte[] address, int detectorNumber, AddressData decoderAddress, int dynNumber, int dynValue,
        Integer timestamp) {
        // no implementation
    }

    @Override
    public void csProgState(
        byte[] address, CommandStationProgState commandStationProgState, int remainingTime, int cvNumber, int cvData) {
        // no implementation
    }

    @Override
    public void csState(byte[] address, CommandStationState commandStationState) {
        // no implementation
    }

    @Override
    public void csDriveManual(byte[] address, DriveState driveState) {
        // no implementation
    }

    @Override
    public void csDriveState(byte[] address, DriveState driveState) {
        // no implementation
    }

    @Override
    public void csAccessoryManual(byte[] address, AddressData decoderAddress, ActivateCoilEnum activate, int aspect) {
        // no implementation
    }

    @Override
    public void csAccessoryAcknowledge(byte[] address, int decoderAddress, AccessoryAcknowledge acknowledge) {
        // no implementation
    }

    @Override
    public void csRcPlusTid(byte[] address, TidData tid) {
        // no implementation
    }

    @Override
    public void csRcPlusPingAcknState(byte[] address, RcPlusPhase phase, RcPlusAcknowledge acknState) {
        // no implementation
    }

    @Override
    public void csRcPlusBindAnswer(byte[] address, RcPlusDecoderAnswerData decoderAnswer) {
        // no implementation
    }

    @Override
    public void csRcPlusFindAnswer(byte[] address, RcPlusPhase phase, RcPlusDecoderAnswerData decoderAnswer) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusBindAccepted(
        byte[] address, int detectorNum, RcPlusDecoderType decoderType, RcPlusFeedbackBindData rcPlusBindAccepted) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusPingCollision(byte[] address, int detectorNum, RcPlusPhase phase) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusFindCollision(
        byte[] address, int detectorNum, RcPlusPhase phase, DecoderUniqueIdData uniqueId) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusPongOkay(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusPongNew(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId) {
        // no implementation
    }

    @Override
    public void nodeLost(byte[] address, Node node) {
        // no implementation

    }

    @Override
    public void nodeNew(byte[] address, Node node) {
        // no implementation

    }
}
