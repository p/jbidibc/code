package org.bidib.jbidibc.debug;

import gnu.io.SerialPort;

import java.io.ByteArrayOutputStream;

public interface DebugMessageProcessor {

    /**
     * Process the messages in the provided byte array output stream.
     * 
     * @param output
     *            the output stream that contains the messages
     */
    void processMessages(final ByteArrayOutputStream output);

    /**
     * Disable the message processor.
     */
    void disable();

    /**
     * Enable the message processor.
     */
    void enable();

    /**
     * Receive data from the serial port instance.
     * 
     * @param port
     *            the serial port instance
     */
    void receive(final SerialPort port);
}
