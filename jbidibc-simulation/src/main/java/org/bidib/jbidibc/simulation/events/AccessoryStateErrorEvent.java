package org.bidib.jbidibc.simulation.events;

import org.bidib.jbidibc.core.enumeration.AccessoryStateErrorEnum;

public class AccessoryStateErrorEvent {

    private final String nodeAddr;

    private final AccessoryStateErrorEnum accessoryStateError;

    public AccessoryStateErrorEvent(final String nodeAddr, final AccessoryStateErrorEnum accessoryStateError) {
        this.nodeAddr = nodeAddr;
        this.accessoryStateError = accessoryStateError;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public AccessoryStateErrorEnum getAccessoryStateError() {
        return accessoryStateError;
    }
}
