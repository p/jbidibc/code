package org.bidib.jbidibc.core.port;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ReconfigPortConfigValueTest {

    @Test
    public void getCurrentOutputType() {

        Assert.assertEquals(new ReconfigPortConfigValue(0x000401).getCurrentOutputType(), LcOutputType.LIGHTPORT);
        Assert.assertEquals(new ReconfigPortConfigValue(0x000402).getCurrentOutputType(), LcOutputType.SERVOPORT);
    }

    @Test
    public void getPortMap() {
        Assert.assertEquals(new ReconfigPortConfigValue(0x000401).getPortMap(), 0x0004);
        Assert.assertEquals(new ReconfigPortConfigValue(0x800302).getPortMap(), 0x8003);
    }

    @Test
    public void getPortMapSwitchPair() {
        Assert.assertEquals(new ReconfigPortConfigValue(0x808107).getPortMap(), 0x8081);
    }

    @Test
    public void getPortMapAndType() {
        Assert.assertEquals(new ReconfigPortConfigValue(0x01, 0x0004).getPortMap(), 0x0004);
        Assert.assertEquals(new ReconfigPortConfigValue(0x02, 0x8003).getPortMap(), 0x8003);

        Assert.assertEquals(new ReconfigPortConfigValue(0x01, 0x0004).getCurrentOutputType(), LcOutputType.LIGHTPORT);
        Assert.assertEquals(new ReconfigPortConfigValue(0x02, 0x0004).getCurrentOutputType(), LcOutputType.SERVOPORT);
        Assert.assertEquals(new ReconfigPortConfigValue(0x02, 0x8003).getCurrentOutputType(), LcOutputType.SERVOPORT);
        Assert.assertEquals(new ReconfigPortConfigValue(0x00, 0x8003).getCurrentOutputType(), LcOutputType.SWITCHPORT);
    }

    @Test
    public void getPortMapAndTypeSwitchPair() {
        Assert.assertEquals(new ReconfigPortConfigValue(0x07, 0x8081).getPortMap(), 0x8081);

        Assert.assertEquals(new ReconfigPortConfigValue(0x07, 0x8081).getCurrentOutputType(),
            LcOutputType.SWITCHPAIRPORT);
    }
}
