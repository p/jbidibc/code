package org.bidib.jbidibc.core.port;

import org.bidib.jbidibc.core.enumeration.LcOutputType;

public class ReconfigPortConfigValue implements PortConfigValue<Integer> {
    private final Integer value;

    public ReconfigPortConfigValue(Integer value) {
        this.value = value;
    }

    public ReconfigPortConfigValue(int portType, int portMap) {
        this.value = ((portMap & 0xFFFF) << 8) | (portType & 0xFF);
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public LcOutputType getCurrentOutputType() {
        byte type = (byte) (value & 0xFF);
        return LcOutputType.valueOf(type);
    }

    public int getPortMap() {
        int portMap = (value >> 8) & 0xFFFF;
        return portMap;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ReconfigPortConfigValue)) {
            return false;
        }
        ReconfigPortConfigValue other = (ReconfigPortConfigValue) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        }
        else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb =
            new StringBuilder("ReconfigPortConfigValue, outputType: ")
                .append(getCurrentOutputType()).append(", portMap: ").append(getPortMap());
        return sb.toString();
    }
}
