package org.bidib.jbidibc.serial;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SerialMessageEncoderTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SerialMessageEncoderTest.class);

    @Test
    public void encodeSingleMessage() throws IOException {

        ByteArrayOutputStream data = new ByteArrayOutputStream();
        data.write(new byte[] { 0x03, 0x00, 0x00, 0x04 });

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        SerialMessageEncoder.encodeMessage(data, output);

        LOGGER.info("Data in output: {}", ByteUtils.bytesToHex(output));

        Assert.assertEquals(new byte[] { (byte) 0xFE, 0x03, 0x00, 0x00, 0x04, (byte) 0xE9, (byte) 0xFE },
            output.toByteArray());
    }

    @Test
    public void encodeSingleMessage2() throws IOException {

        // 28.11.2017 17:15:45.008: [receiveQueueWorker] - >> MSG_LC_MACRO_PARA_GET[[6],num=253,type=76,data=[11, 3]] :
        // 06 06 00 FD 4C 0B 03

        ByteArrayOutputStream data = new ByteArrayOutputStream();
        data.write(new byte[] { 0x06, 0x06, 0x00, (byte) 0xFD, 0x4C, 0x0B, 0x03 });

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        SerialMessageEncoder.encodeMessage(data, output);

        LOGGER.info("Data in output: {}", ByteUtils.bytesToHex(output));

        Assert.assertEquals(new byte[] { (byte) 0xFE, 0x06, 0x06, 0x00, (byte) 0xFD, (byte) 0xDD, 0x4C, 0x0B, 0x03,
            (byte) 0x9B, (byte) 0xFE }, output.toByteArray());
    }

    @Test
    public void encodeMultipleMessages() throws IOException {

        ByteArrayOutputStream data = new ByteArrayOutputStream();
        data.write(new byte[] { 0x03, 0x00, 0x00, 0x04, /**/ 0x05, 0x00, 0x01, 0x19, 0x00, 0x00 });

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        SerialMessageEncoder.encodeMessage(data, output);

        LOGGER.info("Data in output: {}", ByteUtils.bytesToHex(output));

        Assert.assertEquals(new byte[] { (byte) 0xFE, 0x03, 0x00, 0x00, 0x04, 0x05, 0x00, 0x01, 0x19, 0x00, 0x00,
            (byte) 0x1F, (byte) 0xFE }, output.toByteArray());
    }
}
