package org.bidib.jbidibc.core.schema;

import java.util.ArrayList;
import java.util.List;

import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.schema.bidib2.Accessories;
import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.bidib.jbidibc.core.schema.bidib2.Aspect;
import org.bidib.jbidibc.core.schema.bidib2.Aspects;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.Feature;
import org.bidib.jbidibc.core.schema.bidib2.Features;
import org.bidib.jbidibc.core.schema.bidib2.FunctionAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.FunctionCriticalSection;
import org.bidib.jbidibc.core.schema.bidib2.FunctionDelay;
import org.bidib.jbidibc.core.schema.bidib2.FunctionFlag;
import org.bidib.jbidibc.core.schema.bidib2.FunctionInput;
import org.bidib.jbidibc.core.schema.bidib2.FunctionMacro;
import org.bidib.jbidibc.core.schema.bidib2.FunctionOutputLight;
import org.bidib.jbidibc.core.schema.bidib2.FunctionOutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.IOBehaviour;
import org.bidib.jbidibc.core.schema.bidib2.InputKey;
import org.bidib.jbidibc.core.schema.bidib2.Macro;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterClockStart;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterRepeat;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterSlowdown;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameters;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointCriticalSection;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointDelay;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointFlag;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointInput;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointMacro;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputLight;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputServo;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointServoMoveQuery;
import org.bidib.jbidibc.core.schema.bidib2.MacroPoints;
import org.bidib.jbidibc.core.schema.bidib2.Macros;
import org.bidib.jbidibc.core.schema.bidib2.MinuteExtension;
import org.bidib.jbidibc.core.schema.bidib2.Node;
import org.bidib.jbidibc.core.schema.bidib2.Nodes;
import org.bidib.jbidibc.core.schema.bidib2.OutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.OutputLight;
import org.bidib.jbidibc.core.schema.bidib2.OutputServo;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.Port;
import org.bidib.jbidibc.core.schema.bidib2.Ports;
import org.bidib.jbidibc.core.schema.bidib2.WeekdayExtension;

public class BidibTestFactory {

    public BiDiB createDemoBidib() {

        Node node = new Node();
        node.withUserName("SampleNode").withProductName("SampleProductName").withManufacturerId(13);
        node.withUniqueId(/* String.format("0x%08x", */ 0x05340D75001236L/* ) */);
        node.withManufacturerId(13).withProductId(117);
        node.withFirmwareRelease("2.00.07").withProtocol("0.6");

        node.withPorts(new Ports());
        List<Port> ports = node.getPorts().getPort();
        for (int portId = 0; portId < 8; portId++) {
            ports.add(createServo(portId, "Servo Port " + portId));
        }
        for (int portId = 8; portId < 24; portId++) {
            ports.add(createSwitch(portId, "Switch Port " + portId));
        }
        for (int portId = 24; portId < 32; portId++) {
            ports.add(createSwitch(portId, "GPIO Switch Port " + portId));
        }
        for (int portId = 32; portId < 40; portId++) {
            ports.add(createInput(portId, "GPIO Input Port " + portId));
        }
        for (int portId = 40; portId < 44; portId++) {
            ports.add(createLight(portId, "Light Port " + portId));
        }
        for (int portId = 44; portId < 48; portId++) {
            ports.add(createBacklight(portId, "Backlight Port " + portId));
        }

        // prepare some features
        node.withFeatures(new Features());
        List<Feature> features = node.getFeatures().getFeature();
        features.add(createFeature(FeatureEnum.FEATURE_BM_SIZE, 32));
        features.add(createFeature(FeatureEnum.FEATURE_BM_CV_AVAILABLE, 1));
        features.add(createFeature(FeatureEnum.FEATURE_BM_CV_ON, 1));
        node.getFeatures().withFeature(features);

        // prepare some macros
        List<Macro> macros = new ArrayList<>();
        Macro macro = new Macro();
        macro.setName("Macro_0");
        macro.setNumber(0);

        {
            MacroPointOutputSwitch macroPoint1 = new MacroPointOutputSwitch();
            macroPoint1.withDelay(35).withFunction(FunctionOutputSwitch.ON).withOutputNumber(8);
            MacroPointOutputSwitch macroPoint2 = new MacroPointOutputSwitch();
            macroPoint2.withDelay(235).withFunction(FunctionOutputSwitch.OFF).withOutputNumber(8);
            MacroPointOutputServo macroPoint3 = new MacroPointOutputServo();
            macroPoint3.withDelay(235).withOutputNumber(3).withPosition(45);
            MacroPointOutputServo macroPoint4 = new MacroPointOutputServo();
            macroPoint4.withDelay(12).withOutputNumber(3).withPosition(245);

            MacroPointInput macroPoint5 = new MacroPointInput();
            macroPoint5.withInputNumber(33).withFunction(FunctionInput.WAIT_FOR_0);
            MacroPointInput macroPoint6 = new MacroPointInput();
            macroPoint6.withInputNumber(33).withFunction(FunctionInput.WAIT_FOR_1);

            MacroPointDelay macroPoint7 = new MacroPointDelay();
            macroPoint7.withDelay(33).withFunction(FunctionDelay.FIXED);
            MacroPointDelay macroPoint8 = new MacroPointDelay();
            macroPoint8.withDelay(33).withFunction(FunctionDelay.RANDOM);

            MacroPointAccessoryNotification macroPoint9 = new MacroPointAccessoryNotification();
            macroPoint9.withInputNumber(34).withFunction(FunctionAccessoryNotification.OKAY_IF_INPUT_0);
            MacroPointAccessoryNotification macroPoint10 = new MacroPointAccessoryNotification();
            macroPoint10.withInputNumber(34).withFunction(FunctionAccessoryNotification.OKAY_IF_INPUT_1);
            MacroPointAccessoryNotification macroPoint11 = new MacroPointAccessoryNotification();
            macroPoint11.withInputNumber(34).withFunction(FunctionAccessoryNotification.OKAY);

            MacroPointCriticalSection macroPoint12 = new MacroPointCriticalSection();
            macroPoint12.withFunction(FunctionCriticalSection.BEGIN);
            MacroPointCriticalSection macroPoint13 = new MacroPointCriticalSection();
            macroPoint13.withFunction(FunctionCriticalSection.END);

            MacroPointFlag macroPoint14 = new MacroPointFlag();
            macroPoint14.withFunction(FunctionFlag.SET).withFlagNumber(3);
            MacroPointFlag macroPoint15 = new MacroPointFlag();
            macroPoint15.withFunction(FunctionFlag.QUERY_0).withFlagNumber(4);

            MacroPointMacro macroPoint16 = new MacroPointMacro();
            macroPoint16.withMacroNumber(2).withFunction(FunctionMacro.START);
            MacroPointMacro macroPoint17 = new MacroPointMacro();
            macroPoint17.withMacroNumber(2).withFunction(FunctionMacro.STOP);
            MacroPointMacro macroPoint18 = new MacroPointMacro();
            macroPoint18.withMacroNumber(2).withFunction(FunctionMacro.END);

            MacroPoints macroPoints = new MacroPoints();
            macroPoints
                .withMacroPoint(macroPoint1, macroPoint2, macroPoint3, macroPoint4, macroPoint5, macroPoint6,
                    macroPoint7, macroPoint8, macroPoint9, macroPoint10, macroPoint11, macroPoint12, macroPoint13,
                    macroPoint14, macroPoint15, macroPoint16, macroPoint17, macroPoint18);

            MacroPointServoMoveQuery macroPoint19 = new MacroPointServoMoveQuery();
            macroPoint19.withOutputNumber(2);
            macroPoints.withMacroPoint(macroPoint19);

            MacroPointOutputLight macroPoint20 = new MacroPointOutputLight();
            macroPoint20.withDelay(235).withOutputNumber(43).withFunction(FunctionOutputLight.TURN_ON);
            macroPoints.withMacroPoint(macroPoint20);
            MacroPointOutputLight macroPoint21 = new MacroPointOutputLight();
            macroPoint21.withDelay(235).withOutputNumber(43).withFunction(FunctionOutputLight.TURN_OFF);
            macroPoints.withMacroPoint(macroPoint21);

            MacroPointOutputBacklight macroPoint22 = new MacroPointOutputBacklight();
            macroPoint22.withDelay(235).withOutputNumber(46).withBrightness(45);
            macroPoints.withMacroPoint(macroPoint22);
            macro.withMacroPoints(macroPoints);
        }

        macro.withMacroParameters(createMacroParameters(false, 12));
        macros.add(macro);

        macro = new Macro();
        macro.setName("Macro_1");
        macro.setNumber(1);

        {
            MacroPointOutputSwitch macroPoint1 = new MacroPointOutputSwitch();
            macroPoint1.withDelay(35).withFunction(FunctionOutputSwitch.ON).withOutputNumber(9);
            MacroPointOutputSwitch macroPoint2 = new MacroPointOutputSwitch();
            macroPoint2.withDelay(235).withFunction(FunctionOutputSwitch.OFF).withOutputNumber(9);
            MacroPointOutputServo macroPoint3 = new MacroPointOutputServo();
            macroPoint3.withDelay(235).withOutputNumber(4).withPosition(45);
            MacroPointOutputServo macroPoint4 = new MacroPointOutputServo();
            macroPoint4.withDelay(12).withOutputNumber(4).withPosition(245);

            MacroPoints macroPoints = new MacroPoints();
            macroPoints.withMacroPoint(macroPoint1, macroPoint2, macroPoint3, macroPoint4);
            macro.withMacroPoints(macroPoints);
        }

        macro.withMacroParameters(createMacroParameters(true, 14));
        macros.add(macro);

        macro = new Macro();
        macro.setName("Macro_2");
        macro.setNumber(2);

        macro.withMacroParameters(createMacroParameters(true, 8));
        macros.add(macro);

        node.withMacros(new Macros());
        node.getMacros().withMacro(macros);

        // prepare some accessories
        node.withAccessories(new Accessories());
        List<Accessory> accessories = node.getAccessories().getAccessory();
        Accessory accessory0 = new Accessory();
        accessory0.setName("Accessory_0");
        accessory0.setNumber(0);

        Aspects aspects = new Aspects();
        aspects.withAspect(createAspect(0), createAspect(1), createAspect(2));
        accessory0.withAspects(aspects);
        accessories.add(accessory0);

        Accessory accessory1 = new Accessory();
        accessory1.setName("Accessory_1");
        accessory1.setNumber(1);

        Aspects aspects1 = new Aspects();
        aspects1.withAspect(createAspect(0), createAspect(1), createAspect(2));
        accessory1.withAspects(aspects1);

        accessories.add(accessory1);

        final BiDiB bidib = new BiDiB();
        bidib.withSchemaVersion("1.0");
        bidib.withNodes(new Nodes());
        List<Node> nodes = bidib.getNodes().getNode();
        nodes.add(node);
        // bidib.withNodes(nodes);

        return bidib;
    }

    private Aspect createAspect(int number) {
        Aspect aspect = new Aspect();
        aspect.setNumber(number);
        aspect.setName("Aspect_" + number);
        aspect.setMacroNumber(number);
        return aspect;
    }

    private MacroParameters createMacroParameters(boolean withClockStart, int hour) {
        MacroParameters macroParameters = new MacroParameters();

        MacroParameterSlowdown macroParameterSlowdown = new MacroParameterSlowdown();
        macroParameterSlowdown.setSpeed(4);
        macroParameters.withMacroParameter(macroParameterSlowdown);

        MacroParameterRepeat macroParameterRepeat = new MacroParameterRepeat();
        macroParameterRepeat.setRepetitions(25);
        macroParameters.withMacroParameter(macroParameterRepeat);

        MacroParameterClockStart macroParameterClockStart = new MacroParameterClockStart();
        macroParameterClockStart.setIsEnabled(withClockStart);
        if (withClockStart) {
            macroParameterClockStart.setHour(hour);
            macroParameterClockStart.setMinute(39);
            macroParameterClockStart.setPeriodicalRepetition(MinuteExtension.EVERY_30_MINUTES.value());
            // macroParameterClockStart.setWeekday(Weekday.MONDAY.value());
            macroParameterClockStart.setWeekday(WeekdayExtension.EVERY_DAY.value());
        }
        macroParameters.withMacroParameter(macroParameterClockStart);

        return macroParameters;
    }

    private Feature createFeature(FeatureEnum featureEnum, int value) {
        Feature feature = new Feature();
        feature.setFeatureCodeId(featureEnum.getNumber());
        feature.setValue(value);
        return feature;
    }

    private OutputServo createServo(int portId, String name) {
        OutputServo servo = new OutputServo();
        servo.setLowerLimit(20);
        servo.setUpperLimit(220);
        servo.setMovingTime(5);
        servo.setNumber(portId);
        servo.setName(name);
        return servo;
    }

    private OutputSwitch createSwitch(int portId, String name) {
        OutputSwitch outputSwitch = new OutputSwitch();
        outputSwitch.setIoBehaviour(IOBehaviour.OUTPUT);
        outputSwitch.setSwitchOffTime(15);
        outputSwitch.setNumber(portId);
        outputSwitch.setName(name);
        return outputSwitch;
    }

    private InputKey createInput(int portId, String name) {
        InputKey input = new InputKey();
        input.setNumber(portId);
        input.setName(name);
        return input;
    }

    private OutputLight createLight(int portId, String name) {
        OutputLight outputLight = new OutputLight();
        outputLight.setNumber(portId);
        outputLight.setName(name);
        outputLight.setBrightnessOff(0);
        outputLight.setBrightnessOn(255);
        outputLight.setDimmingDownSpeed(4);
        outputLight.setDimmingUpSpeed(5);
        return outputLight;
    }

    private OutputBacklight createBacklight(int portId, String name) {
        OutputBacklight outputBacklight = new OutputBacklight();
        outputBacklight.setNumber(portId);
        outputBacklight.setName(name);
        outputBacklight.setDimmingDownSpeed(4);
        outputBacklight.setDimmingUpSpeed(5);
        outputBacklight.setDmxChannel(portId);
        return outputBacklight;
    }
}
