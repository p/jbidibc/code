package org.bidib.jbidibc.simulation.events;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class IdentifyEvent {

    private final String nodeAddr;

    private Boolean identify;

    public IdentifyEvent(final String nodeAddr, final Boolean identify) {
        this.nodeAddr = nodeAddr;
        this.identify = identify;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public Boolean getIdentify() {
        return identify;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
