package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;

public enum LoadTypeEnum implements BidibEnum {
    // @formatter:off
    SWICH_ONLY(0, "switchOnly"), RESISTIVE(1, "resistive"), MAG_COIL_NO_LIMITSTOP(2, "magCoilNoLimitStop"), 
    MAG_COIL_WITH_LIMITSTOP(3, "magCoilWithLimitStop"), UNKNOWN(0xFF, "unknown");
    // @formatter:on

    private final byte type;

    private final String key;

    LoadTypeEnum(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public byte getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    /**
     * Create a load type enum.
     * 
     * @param type
     *            numeric value of the load type enum
     * 
     * @return LoadTypeEnum
     */
    public static LoadTypeEnum valueOf(byte type) {
        LoadTypeEnum result = null;

        for (LoadTypeEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a load type enum");
        }
        return result;
    }

    public static LoadTypeEnum[] getValues() {

        return values();
    }
}