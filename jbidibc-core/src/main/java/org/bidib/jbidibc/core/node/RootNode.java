package org.bidib.jbidibc.core.node;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.SysClockMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a master (interface) node in the BiDiB system.
 * 
 */
public class RootNode extends BidibNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(RootNode.class);

    public static final byte[] ROOTNODE_ADDR = new byte[] { 0 };

    private AtomicBoolean initialReadNodesPassed = new AtomicBoolean();

    private long startTime;

    private int timeAccelerationFactor = 1;

    RootNode(MessageReceiver messageReceiver, boolean ignoreWaitTimeout) {
        super(new byte[] { 0 }, messageReceiver, ignoreWaitTimeout);

        setStartTime(new Date().getTime());
    }

    /**
     * Send the sys clock message.
     * 
     * @param date
     *            the current date
     * @param factor
     *            the time factor
     * @throws ProtocolException
     */
    public void clock(Calendar date, int factor) throws ProtocolException {
        LOGGER.info("Send the clock message.");
        sendNoWait(new SysClockMessage(date, factor));
    }

    /**
     * Send the system reset message to the node.
     * 
     * @throws ProtocolException
     */
    public void reset() throws ProtocolException {
        super.reset();
    }

    /**
     * Clear all bidib related cached data from the node.
     */
    public void clear() {

    }

    /**
     * Get the magic from the node.
     * 
     * @param receiveTimeout
     *            the timeout in milliseconds used to wait for a response from the node. If null the default timeout is
     *            used.
     * @return the magic
     * @throws ProtocolException
     */
    @Override
    public int getMagic(Integer receiveTimeout) throws ProtocolException {
        LOGGER.trace("Get magic from root node!");
        int magic = super.getMagic(receiveTimeout);
        LOGGER.info("Get magic from root node returns: {}", magic);
        if (BIDIB_MAGIC_UNKNOWN == magic) {
            LOGGER.warn("The interface did not respond the get magic request!");

            StringBuilder sb = new StringBuilder("The interface did not respond the get magic request!");

            String errorInfo = getMessageReceiver().getErrorInformation();
            if (StringUtils.isNotBlank(errorInfo)) {
                LOGGER.warn("Found received data that was not identifed as BiDiB messages: {}", errorInfo);

                sb.append("\r\n");
                sb.append(errorInfo);
            }
            NoAnswerException ex = new NoAnswerException("Establish communication with interface failed.");
            ex.setDescription(sb.toString());
            LOGGER.warn("Prepared exception to throw:", ex);
            throw ex;
        }
        return magic;
    }

    public void setReadNodesPassed(boolean readNodesPassed) {
        initialReadNodesPassed.set(readNodesPassed);
    }

    public boolean getReadNodesPassed() {
        return initialReadNodesPassed.get();
    }

    /**
     * @return the timeAccelerationFactor
     */
    public int getTimeAccelerationFactor() {
        return timeAccelerationFactor;
    }

    /**
     * @param timeAccelerationFactor
     *            the timeAccelerationFactor to set
     */
    public void setTimeAccelerationFactor(int timeAccelerationFactor) {
        this.timeAccelerationFactor = timeAccelerationFactor;
    }

    /**
     * @return the startTime
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     *            the startTime to set
     */
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
