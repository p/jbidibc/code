package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CommandStationDriveStateResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationDriveStateResponseTest.class);

    @Test
    public void getFunctions1() throws ProtocolException {

        // 18.06.2017 10:21:17.502: [receiveQueueWorker] - << MSG_CS_DRIVE_STATE[[0],num=56,type=229,data=[235, 21, 2,
        // 3, 0, 1, 0, 0, 0]] : 0C 00 38 69 EB 15 02 03 00 01 00 00 00

        byte[] message =
            new byte[] { 0x0D, 0x00, 0x38, (byte) 0xEA, 0x41, (byte) 0xEB, 0x15, 0x02, 0x03, 0x00, 0x01, 0x00, 0x00,
                0x00 };
        BidibMessage result = new BidibMessage(message);

        Assert.assertEquals(ByteUtils.getInt(result.getType()), CommandStationDriveStateResponse.TYPE.intValue());

        CommandStationDriveStateResponse response =
            new CommandStationDriveStateResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(response);
        LOGGER.info("Prepared response: {}", response);

        Assert.assertEquals(ByteUtils.getInt(response.getOpCode()), 0x41);

        LOGGER.info("Prepared driveState: {}", response.getDriveState());
        Assert.assertNotNull(response.getDriveState());

        byte functionsF0toF4 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F0_F4);
        Assert.assertEquals(functionsF0toF4, (byte) 0x01);
        byte functionsF5toF12 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F5_F12);
        Assert.assertEquals(functionsF5toF12, (byte) 0x00);
        byte functionsF13toF20 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F13_F20);
        Assert.assertEquals(functionsF13toF20, (byte) 0x00);
        byte functionsF21toF28 = response.getFunctions(CommandStationDriveManualResponse.FUNCTIONS_INDEX_F21_F28);
        Assert.assertEquals(functionsF21toF28, (byte) 0x00);
    }

}
