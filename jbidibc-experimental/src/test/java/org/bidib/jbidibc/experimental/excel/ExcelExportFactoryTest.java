package org.bidib.jbidibc.experimental.excel;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.BidibTestFactory;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ExcelExportFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelExportFactoryTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/excel-test/bidib";

    private static final String OUTPUT_FILENAME = "bidib-excel.xlsx";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void createWorkbook() throws IOException {
        ExcelExportFactory factory = new ExcelExportFactory();

        final BiDiB bidib = new BidibTestFactory().createDemoBidib();

        String fileName = OUTPUT_TARGET_DIR + "/" + OUTPUT_FILENAME;
        factory.createWorkbook(bidib, fileName);
    }
}
