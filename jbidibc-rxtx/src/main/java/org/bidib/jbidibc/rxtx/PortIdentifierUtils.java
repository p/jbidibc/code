package org.bidib.jbidibc.rxtx;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.bidib.jbidibc.core.exception.InvalidLibraryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gnu.io.CommPortIdentifier;

public class PortIdentifierUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortIdentifierUtils.class);

    public static List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new ArrayList<String>();

        try {
            // get the comm port identifiers
            Enumeration<?> e = CommPortIdentifier.getPortIdentifiers();
            while (e.hasMoreElements()) {
                CommPortIdentifier id = (CommPortIdentifier) e.nextElement();
                LOGGER.info("Process current CommPortIdentifier, name: {}, portType: {}", id.getName(),
                    id.getPortType());

                if (id.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                    portIdentifiers.add(id.getName());
                }
                else {
                    LOGGER.debug("Skip port because no serial port, name: {}, portType: {}", id.getName(),
                        id.getPortType());
                }
            }
        }
        catch (UnsatisfiedLinkError ule) {
            LOGGER.warn("Get comm port identifiers failed.", ule);
            throw new InvalidLibraryException(ule.getMessage(), ule.getCause());
        }
        catch (Error error) {
            LOGGER.warn("Get comm port identifiers failed.", error);
            throw new RuntimeException(error.getMessage(), error.getCause());
        }
        return portIdentifiers;
    }
}
