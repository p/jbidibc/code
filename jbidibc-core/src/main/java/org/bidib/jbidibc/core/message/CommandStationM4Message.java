package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.M4OpCodes;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandStationM4Message extends BidibCommandMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationM4Message.class);

    public CommandStationM4Message(M4OpCodes opCode) {
        super(0, BidibLibrary.MSG_CS_M4, opCode.getType());
    }

    public CommandStationM4Message(M4OpCodes opCode, byte[] data) {
        super(0, BidibLibrary.MSG_CS_M4, prepareData(opCode, data));
    }

    /**
     * Create the command for M4.
     * 
     * @param opCode
     *            the operation code
     * @param tid
     *            the TID
     */
    public CommandStationM4Message(M4OpCodes opCode, TidData tid) {
        super(0, BidibLibrary.MSG_CS_M4, prepareData(opCode, tid));
    }

    public CommandStationM4Message(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_CS_M4";
    }

    public M4OpCodes getOpCode() {
        byte opCode = getData()[0];
        return M4OpCodes.valueOf(opCode);
    }

    public TidData getTid() {
        TidData tid = TidData.fromByteArray(getData(), 1);
        return tid;
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return new Integer[] { CommandStationM4AcknowledgeResponse.TYPE };
    }

    private static byte[] prepareData(M4OpCodes opCode, byte[] data) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // op code
        out.write(opCode.getType());

        if (data != null) {
            // data
            out.write(data, 0, data.length);
        }
        return out.toByteArray();
    }

    private static byte[] prepareData(M4OpCodes opCode, TidData tid) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // op code
        out.write(opCode.getType());

        if (tid != null) {
            // write tid
            tid.writeToStream(out);
        }
        return out.toByteArray();
    }
}
