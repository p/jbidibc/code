package org.bidib.jbidibc.core;

import org.bidib.jbidibc.core.utils.ByteUtils;

public class ProtocolVersion implements Comparable<ProtocolVersion> {
    private final int majorVersion;

    private final int minorVersion;

    public static final ProtocolVersion VERSION_0_5 = new ProtocolVersion(0, 5);

    public static final ProtocolVersion VERSION_0_6 = new ProtocolVersion(0, 6);

    public static final ProtocolVersion VERSION_0_7 = new ProtocolVersion(0, 7);

    public static final ProtocolVersion VERSION_0_8 = new ProtocolVersion(0, 8);

    /**
     * Check if the protocol version is supported.
     * 
     * @param version
     *            the version to test
     * @return the protocol version is supported
     */
    public static boolean isSupportedProtocolVersion(ProtocolVersion version) {
        return version != null && version.isHigherThan(VERSION_0_8);
    }

    public ProtocolVersion(int majorVersion, int minorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }

    private Integer toInt() {
        return (majorVersion << 8) + minorVersion;
    }

    public byte[] toByteArray() {
        return new byte[] { ByteUtils.getLowByte(majorVersion), ByteUtils.getLowByte(minorVersion) };
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    public String toString() {
        return majorVersion + "." + minorVersion;
    }

    @Override
    public int compareTo(ProtocolVersion version) {
        return toInt().compareTo(version.toInt());
    }

    public boolean isLowerThan(ProtocolVersion versionToCompare) {
        return toInt() < versionToCompare.toInt();
    }

    public boolean isHigherThan(ProtocolVersion versionToCompare) {
        return toInt() > versionToCompare.toInt();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProtocolVersion) {
            return toInt().equals(((ProtocolVersion) obj).toInt());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toInt();
    }
}
