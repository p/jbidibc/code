<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/bidib/2.0" xmlns:bidib="http://www.bidib.org/schema/bidib/2.0" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<xsl:template name="decimalToHex">
	    <xsl:param name="dec"/>
	    <xsl:if test="$dec > 0">
	        <xsl:call-template name="decimalToHex">
	            <xsl:with-param name="dec" select="floor($dec div 16)"/>
	        </xsl:call-template>
	        <xsl:value-of select="substring('0123456789ABCDEF', (($dec mod 16) + 1), 1)"/>
	    </xsl:if>
	</xsl:template>
	
	<xsl:template match="bidib:BiDiB">
		<html>
			<head>
				<title>BiDiB Messages Report</title>
				<style>
				h1   {color: blue;}
				p    {color: red;}
				table{
				border-collapse:collapse;
				border:1px solid #FF0000;
				}
				table th{
				border:1px solid #FF0000;
				}
				table td{
				border:1px solid #FF0000;
				}
				</style>
			</head>
			<body>
				<h2>BiDiB Messages</h2>
				<table border="0" rules="none" cellpadding="4" cellspacing="0"
					>
				<colgroup>
					<col span="1" style="width: 150px;" />
					<col span="1" style="width: 20px;" />
					<col span="1" style="width: 25px;" />
					<col span="1" style="width: 120px;" />
				</colgroup>
				<tr>
					<th bgcolor="lightgrey">Typ</th>
					<th bgcolor="lightgrey">Direction</th>
					<th bgcolor="lightgrey">Nr</th>
					<th bgcolor="lightgrey">Message</th>
				</tr>
				<xsl:apply-templates />
				</table>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="bidib:protocol/bidib:messageTypes">
		<xsl:for-each select="bidib:messageType">
		<tr>
			<td>
				<xsl:value-of select="@category" />
			</td>			
			<td>
				<xsl:value-of select="upper-case(@direction)" />
			</td>			
			<td>
				<xsl:call-template name="decimalToHex">
				    <xsl:with-param name="dec"><xsl:value-of select="@id" /></xsl:with-param>
				</xsl:call-template>
<!-- 				<xsl:value-of select="@id" /> -->
			</td>
			<td>
				<xsl:value-of select="@name" />
			</td>			
		</tr>
		</xsl:for-each>
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>
