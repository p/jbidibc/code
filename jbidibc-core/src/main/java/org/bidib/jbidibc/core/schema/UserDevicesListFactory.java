package org.bidib.jbidibc.core.schema;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.bidib.jbidibc.core.schema.decoder.commontypes.DecoderTypeType;
import org.bidib.jbidibc.core.schema.decoder.commontypes.VersionType;
import org.bidib.jbidibc.core.schema.decoder.userdevices.UserDeviceType;
import org.bidib.jbidibc.core.schema.decoder.userdevices.UserDevicesList;
import org.bidib.jbidibc.core.schema.decoder.userdevices.UserDevicesType;
import org.bidib.jbidibc.core.schema.exception.InvalidContentException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class UserDevicesListFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDevicesListFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.decoder.userdevices";

    public static final String XSD_LOCATION_BASE = "/xsd/commonTypes.xsd";

    public static final String XSD_LOCATION = "/xsd/userDevices.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/userDevices/1.3 xsd/userDevices.xsd";

    private UserDevicesListFactory() {
    }

    public static UserDevicesList getUserDevicesList(File userDevicesFile) {
        return new UserDevicesListFactory().loadUserDevicesFile(userDevicesFile);
    }

    private UserDevicesList loadUserDevicesFile(File userDevicesFile) {

        UserDevicesList userDevicesList = null;

        try (InputStream is = new FileInputStream(userDevicesFile)) {
            userDevicesList = loadUserDevicesList(is);
        }
        catch (IOException ex) {
            LOGGER.info("No UserDevicesList file found.");
        }
        return userDevicesList;
    }

    private UserDevicesList loadUserDevicesList(InputStream is) {

        UserDevicesList userDevicesList = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource =
                new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase =
                new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            JAXBElement<UserDevicesList> jaxbElement =
                (JAXBElement<UserDevicesList>) unmarshaller.unmarshal(xmlr, UserDevicesList.class);
            userDevicesList = jaxbElement.getValue();
        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from input stream failed failed.", ex);

            List<String> errors = validate(is);

            throw new InvalidContentException("Load userDevicesList failed.", errors);
        }
        return userDevicesList;
    }

    /**
     * Find the userDevices that match the provided deviceType.
     * 
     * @param deviceType
     *            the device type
     * @param source
     *            the source list
     * @return the filtered list
     */
    public static List<UserDeviceType> findUserDevices(DecoderTypeType deviceType, List<UserDeviceType> source) {

        List<UserDeviceType> filtered =
            source.stream().filter(device -> deviceType.equals(device.getDeviceType())).collect(Collectors.toList());

        return filtered;
    }

    /**
     * Find the userDevices that match the provided deviceType.
     * 
     * @param source
     *            the source list
     * @param deviceType
     *            the device types
     * @return the filtered list
     */
    public static List<UserDeviceType> findUserDevices(List<UserDeviceType> source, DecoderTypeType... deviceType) {

        List<DecoderTypeType> decoderTypes = new ArrayList<>();
        decoderTypes.addAll(Arrays.asList(deviceType));
        List<UserDeviceType> filtered =
            source
                .stream().filter(device -> decoderTypes.contains(device.getDeviceType())).collect(Collectors.toList());

        return filtered;
    }

    /**
     * Save the user devices list.
     * 
     * @param userDevicesList
     *            the instance
     * @param fileName
     *            the filename
     */
    public static void saveUserDevicesList(final UserDevicesList userDevicesList, File file) {
        LOGGER.info("Save user devices list to file: {}, userDevicesList: {}", file.getPath(), userDevicesList);
        OutputStream os = null;
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource =
                new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase =
                new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });
            marshaller.setSchema(schema);

            ByteArrayOutputStream bas = new ByteArrayOutputStream();

            marshaller.marshal(userDevicesList, new OutputStreamWriter(bas, Charset.forName("UTF-8")));

            os = new BufferedOutputStream(new FileOutputStream(file));

            os.write(bas.toByteArray());

            os.flush();

            LOGGER.info("Save userDevicesList content to file passed: {}", file.getPath());
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save userDevicesList failed.", ex);

            throw new RuntimeException("Save userDevicesList failed.", ex);
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }
        }
    }

    private List<String> validate(final InputStream is) {
        List<String> errors = null;

        if (is instanceof FileInputStream) {
            FileInputStream fis = (FileInputStream) is;
            try {
                LOGGER.info("Try to set file position to 0.");
                fis.getChannel().position(0);
            }
            catch (IOException e) {
                LOGGER.warn("Set file position to 0 failed.", e);
            }
        }

        StreamSource inputStreamSource = new StreamSource(is);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(UserDevicesListFactory.class.getResourceAsStream(XSD_LOCATION));

        XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();

        try {
            Schema schema = schemaFactory.newSchema(streamSource);

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);

            validator.validate(inputStreamSource);
        }
        catch (IOException ex) {
            LOGGER.warn("Validate failed.", ex);
        }
        catch (SAXException ex) {
            LOGGER.warn("Validate failed.", ex);
        }

        errors = errorHandler.getErrors();
        LOGGER.info("Found errors: {}", errors);

        return errors;
    }

    public static UserDevicesList prepareEmptyList(final Date created) {

        XMLGregorianCalendar createdDate = null;
        try {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(created);
            createdDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        }
        catch (DatatypeConfigurationException ex) {
            LOGGER.warn("Create the XML gregorian calendar failed.", ex);
        }

        UserDevicesList userDevicesList = new UserDevicesList();
        userDevicesList.withVersion(new VersionType()
            .withAuthor("Wizard").withCreatedBy("Wizard").withLastUpdate("2018-03-08T07:35:49").withCreated(createdDate)
            .withCreatorLink("http://bidib.org"));
        userDevicesList.withUserDevices(new UserDevicesType());

        return userDevicesList;
    }
}
