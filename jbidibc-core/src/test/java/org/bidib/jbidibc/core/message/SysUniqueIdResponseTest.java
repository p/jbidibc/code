package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SysUniqueIdResponseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SysUniqueIdResponseTest.class);

    @Test
    public void getConfigFingerPrint() throws ProtocolException {

        long uniqueId = 0x05340d6B001234L;
        byte[] unique = ByteUtils.convertLongToUniqueId(uniqueId);
        SysUniqueIdResponse sysUniqueIdResponse =
            new SysUniqueIdResponse(new byte[] { 2 }, 1, SysUniqueIdResponse.TYPE,
                ByteUtils.concat(unique, new byte[] { 1, 0, 0, 0 }));

        LOGGER.info("Prepared sysUniqueIdResponse: {}", sysUniqueIdResponse);

        long configFingerPrint = sysUniqueIdResponse.getConfigFingerPrint();

        LOGGER.info("Returned configFingerPrint: {}", configFingerPrint);
        Assert.assertEquals(configFingerPrint, 1);
    }

    @Test
    public void getConfigFingerPrint2() throws ProtocolException {

        long uniqueId = 0x05340d6B001234L;
        byte[] unique = ByteUtils.convertLongToUniqueId(uniqueId);
        SysUniqueIdResponse sysUniqueIdResponse =
            new SysUniqueIdResponse(new byte[] { 2 }, 1, SysUniqueIdResponse.TYPE,
                ByteUtils.concat(unique, new byte[] { 0, 0, 0, 1 }));

        LOGGER.info("Prepared sysUniqueIdResponse: {}", sysUniqueIdResponse);

        long configFingerPrint = sysUniqueIdResponse.getConfigFingerPrint();

        LOGGER.info("Returned configFingerPrint: {}", configFingerPrint);
        Assert.assertEquals(configFingerPrint, 0x01000000L);
    }

    @Test
    public void getConfigFingerPrint3() throws ProtocolException {

        long uniqueId = 0x05340d6B001234L;
        byte[] unique = ByteUtils.convertLongToUniqueId(uniqueId);
        SysUniqueIdResponse sysUniqueIdResponse =
            new SysUniqueIdResponse(new byte[] { 2 }, 1, SysUniqueIdResponse.TYPE,
                ByteUtils.concat(unique, new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF }));

        LOGGER.info("Prepared sysUniqueIdResponse: {}", sysUniqueIdResponse);

        long configFingerPrint = sysUniqueIdResponse.getConfigFingerPrint();

        LOGGER.info("Returned configFingerPrint: {}", configFingerPrint);
        Assert.assertEquals(configFingerPrint, 0xFF_FF_FF_FFL);
    }
}
