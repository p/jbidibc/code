package org.bidib.jbidibc.exchange.vendorcv;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.bidib.jbidibc.core.SoftwareVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VendorCvFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(VendorCvFactoryTest.class);

    @Test
    public void findMatchingVendorCV() {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("BiDiBCV-13-117-1.03.24.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-117.xml"));

        String defaultFilename = "BiDiBCV-13-117.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(1, 3, 24);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCV2() {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("BiDiBCV-13-117-11.03.24.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-117-1.03.22.xml"));
        files.add(new File("BiDiBCV-13-117.xml"));

        String defaultFilename = "BiDiBCV-13-117.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(11, 3, 24);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCVClosestMatching() {

        Collection<File> files = new LinkedList<File>();

        // must accept 1.03 as closest matching because it is expanded to 1.03.99
        File expected = new File("BiDiBCV-13-117-1.03.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-117-1.03.22.xml"));
        files.add(new File("BiDiBCV-13-117-1.03.23.xml"));
        files.add(new File("BiDiBCV-13-117.xml"));

        String defaultFilename = "BiDiBCV-13-117.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(1, 3, 24);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCVClosestMatching1() {

        Collection<File> files = new LinkedList<File>();

        // must accept 1.03.23 as closest matching
        File expected = new File("BiDiBCV-13-117-1.03.23.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-117-1.03.xml"));
        files.add(new File("BiDiBCV-13-117.xml"));

        String defaultFilename = "BiDiBCV-13-117.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(1, 3, 22);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCVClosestMatching2() {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("BiDiBCV-13-117-1.03.25.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-117.xml"));
        files.add(new File("BiDiBCV-13-117-1.03.xml"));

        String defaultFilename = "BiDiBCV-13-117.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(1, 0, 0);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCVClosestMatching3() {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("BiDiBCV-13-104-2.02.02.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-104-2.02.00.xml"));
        // files.add(new File("BiDiBCV-13-104-2.02.02.xml"));
        files.add(new File("BiDiBCV-13-104-2.03.00.xml"));

        String defaultFilename = "BiDiBCV-13-104.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(2, 2, 1);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCVClosestMatching4() {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("BiDiBCV-13-104.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-104-2.02.00.xml"));
        files.add(new File("BiDiBCV-13-104-2.02.02.xml"));

        String defaultFilename = "BiDiBCV-13-104.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(2, 2, 5);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }

    @Test
    public void findMatchingVendorCVClosestMatching5() {

        Collection<File> files = new LinkedList<File>();
        File expected = new File("BiDiBCV-13-104.xml");
        files.add(expected);
        files.add(new File("BiDiBCV-13-104-2.02.00.xml"));
        files.add(new File("BiDiBCV-13-104-2.02.02.xml"));

        String defaultFilename = "BiDiBCV-13-104.xml";
        SoftwareVersion softwareVersion = new SoftwareVersion(2, 3, 0);

        VendorCvFactory vendorCvFactory = new VendorCvFactory();
        File matchingFile = vendorCvFactory.findMatchingVendorCV(files, defaultFilename, softwareVersion);
        Assert.assertNotNull(matchingFile);

        LOGGER.info("Found matching file: {}", matchingFile);
        Assert.assertEquals(matchingFile, expected);
    }
}
