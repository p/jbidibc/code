package org.bidib.jbidibc.pomupdate;

import org.bidib.jbidibc.core.CRC8;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirmwarePacket {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwarePacket.Target.class);

    public enum Target {
        flash, eeprom;
    }

    private int targetAddress;

    private Target target;

    private byte[] data;

    private byte crc;

    private byte len;

    public FirmwarePacket(String line) {
        // line must start with ':'
        if (line.charAt(0) != ':') {
            throw new IllegalArgumentException("Line of Intel hex must start with ':'");
        }

        byte[] dataLine = ByteUtils.parseHexBinary(line.substring(1));
        len = dataLine[0];
        targetAddress = ByteUtils.getInt(dataLine[2], dataLine[1]);
        target = (ByteUtils.getInt(dataLine[3]) == 0 ? Target.flash : Target.eeprom);
        data = ByteUtils.subArray(dataLine, 4, dataLine.length - 5);

        crc = dataLine[dataLine.length - 1];

        // check the crc
        int calcCRC = CRC8.getIntelCrc(dataLine, 0, dataLine.length - 1);

        if (crc != calcCRC) {
            LOGGER.error("The calculated CRC ({}) does not matched the provided CRC ({})", calcCRC, crc);
            throw new IllegalArgumentException("CRC check failed.");
        }
    }

    /**
     * @return the target address
     */
    public int getTargetAddress() {
        return targetAddress;
    }

    /**
     * @param address
     *            the target address to set
     */
    public void setTargetAddress(int address) {
        this.targetAddress = address;
    }

    /**
     * @return the target
     */
    public Target getTarget() {
        return target;
    }

    /**
     * @param target
     *            the target to set
     */
    public void setTarget(Target target) {
        this.target = target;
    }

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(byte[] data) {
        this.data = data;
    }

    /**
     * @return the crc
     */
    public byte getCrc() {
        return crc;
    }

    public byte getLen() {
        return len;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(FirmwarePacket.class.getSimpleName()).append("[len=").append(len).append(",address=")
            .append(targetAddress).append(",target=").append(target).append(",data={")
            .append(ByteUtils.bytesToHex(data)).append("},crc=").append(ByteUtils.byteToHex(crc)).append("]");
        return sb.toString();
    }
}
