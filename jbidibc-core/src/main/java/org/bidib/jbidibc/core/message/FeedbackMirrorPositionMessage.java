package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackMirrorPositionMessage extends BidibCommandMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackMirrorPositionMessage.class);

    public FeedbackMirrorPositionMessage(int decoderAddress, int locationType, int locationAddress) {
        super(0, BidibLibrary.MSG_BM_MIRROR_POSITION,
            new Builder(decoderAddress).addType(locationType).addAddress(locationAddress).build());
    }

    public FeedbackMirrorPositionMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_BM_MIRROR_POSITION";
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }

    private static class Builder {

        private ByteArrayOutputStream stream;

        public Builder(int decoderAddress) {
            stream = new ByteArrayOutputStream(10);
            try {
                stream.write(ByteUtils.toWORD(decoderAddress));
            }
            catch (IOException ex) {
                LOGGER.warn("Add decoder address to stream failed", ex);
            }
        }

        public Builder addType(int type) {
            stream.write(ByteUtils.getLowByte(type));
            return this;
        }

        public Builder addAddress(int address) {
            try {
                stream.write(ByteUtils.toWORD(address));
            }
            catch (IOException ex) {
                LOGGER.warn("Add address to stream failed", ex);
            }
            return this;
        }

        public byte[] build() {
            return stream.toByteArray();
        }
    }
}
