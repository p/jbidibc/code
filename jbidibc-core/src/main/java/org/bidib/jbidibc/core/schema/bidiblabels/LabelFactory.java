package org.bidib.jbidibc.core.schema.bidiblabels;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.UnexpectedCharacterException;
import org.bidib.jbidibc.core.schema.BidibFactory;
import org.bidib.jbidibc.core.schema.bidibbase.PortType;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class LabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(LabelFactory.class);

    public static final String XSD_LOCATION = "/xsd/bidib2Labels.xsd";

    public static final String XSD_LOCATION_BASE = "/xsd/bidib2Base.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/nodeLabels/1.0 xsd/bidib2Labels.xsd";

    private static JAXBContext jaxbContext;

    /**
     * Prepare the filename based on the provided uniqueId.
     * 
     * @param uniqueId
     *            the uniqueId
     * @return the hex formatted, zero-leading 14-digits representation of the uniqueId
     */
    public static String prepareNodeFilename(long uniqueId) {
        String nodeFileName = String.format("Node_%014X.xml", uniqueId);
        return nodeFileName;
    }

    /**
     * Save the node labels.
     * 
     * @param nodeLabels
     *            the node labels
     * @param file
     *            the file to write
     * @param gzip
     *            {@code true} : file is gzipped, {@code false} : file not gzipped
     */
    public void saveNodeLabel(final NodeLabels nodeLabels, File file, boolean gzip) {

        LOGGER.debug("Save nodeLabels to file: {}, nodeLabels: {}", file, nodeLabels);
        OutputStream os = null;
        boolean passed = false;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", NodeLabels.class.getPackage());
                jaxbContext = JAXBContext.newInstance(NodeLabels.class);
            }

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, JAXB_SCHEMA_LOCATION);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(LabelFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase = new StreamSource(LabelFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });
            marshaller.setSchema(schema);

            os = new BufferedOutputStream(new FileOutputStream(file));
            if (gzip) {
                LOGGER.debug("Use gzip to compress accessories.");
                os = new GZIPOutputStream(os);
            }

            JAXBElement<NodeLabels> jaxbElement = new ObjectFactory().createNodeLabels(nodeLabels);
            marshaller.marshal(jaxbElement, new OutputStreamWriter(os, Charset.forName("UTF-8")));

            os.flush();

            LOGGER.info("Save nodeLabels to file passed: {}", file.getPath());

            passed = true;
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save nodeLabels failed, file: {}", file, ex);

            InvalidConfigurationException icex = new InvalidConfigurationException("Save nodeLabels failed.", ex);
            icex.setReason(file.getAbsolutePath());
            throw icex;
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }

            if (!passed) {
                LOGGER.warn("Delete the file because the export has failed.");
                FileUtils.deleteQuietly(file);
            }
        }
    }

    public NodeLabels loadLabels(final InputStream is) {

        NodeLabels nodeLabels = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", NodeLabels.class.getPackage());
                jaxbContext = JAXBContext.newInstance(NodeLabels.class);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase = new StreamSource(LabelFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            try {
                JAXBElement<NodeLabels> jaxbElement =
                    (JAXBElement<NodeLabels>) unmarshaller.unmarshal(xmlr, NodeLabels.class);
                nodeLabels = jaxbElement.getValue();
            }
            catch (UnmarshalException ex) {
                LOGGER.warn("Load content from file failed due to an unmarshal exception.", ex);
                if (ex.getLinkedException() instanceof SAXException) {
                    validate(is);
                    throw new InvalidSchemaException("Load NodeLabels from file failed");
                }
                if (ex.getCause() instanceof XMLStreamException) {
                    XMLStreamException wex = (XMLStreamException) ex.getCause();
                    LOGGER.warn("The inner exception signals an unexpected character.", wex);

                    throw new UnexpectedCharacterException(wex.getMessage());
                }
            }

        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from file failed.", ex);
        }
        return nodeLabels;
    }

    private List<String> validate(final InputStream is) {
        List<String> errors = null;

        if (is instanceof FileInputStream) {
            FileInputStream fis = (FileInputStream) is;
            try {
                LOGGER.info("Try to set file position to 0.");
                fis.getChannel().position(0);
            }
            catch (IOException e) {
                LOGGER.warn("Set file position to 0 failed.", e);
            }
        }

        StreamSource inputStreamSource = new StreamSource(is);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));

        XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();

        try {
            Schema schema = schemaFactory.newSchema(streamSource);

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);

            validator.validate(inputStreamSource);
        }
        catch (IOException ex) {
            LOGGER.warn("Validate failed.", ex);
        }
        catch (SAXException ex) {
            LOGGER.warn("Validate failed.", ex);
        }

        errors = errorHandler.getErrors();
        LOGGER.info("Found errors: {}", errors);

        return errors;
    }

    public static List<PortLabel> getPortsOfType(final List<PortLabel> allPorts, final PortType type) {
        List<PortLabel> selection = ListUtils.select(allPorts, new Predicate<PortLabel>() {

            @Override
            public boolean evaluate(PortLabel portLabel) {

                return (portLabel.type == type);
            }
        });

        return selection;
    }
}
