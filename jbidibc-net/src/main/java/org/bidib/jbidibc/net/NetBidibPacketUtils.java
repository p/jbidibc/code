package org.bidib.jbidibc.net;

import java.io.ByteArrayOutputStream;
import java.net.InetAddress;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetBidibPacketUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibPacketUtils.class);

    public static void parsePackets(
        final ByteArrayOutputStream output, NetMessageHandler messageReceiver, InetAddress address, int portNumber)
        throws ProtocolException {

        int receivedCount = output.size();

        byte[] receiveData = output.toByteArray();

        LOGGER.info("Received data packet, len: {} : {}", receivedCount, ByteUtils.bytesToHex(output));

        int index = 0;
        int processedIndex = 0;
        // iterate over the data and find the packets
        try {

            // TODO This has changed logic now. The messages are no longer splited ...

            while (index < receivedCount) {
                // int startIndex = index;

                // get the length of the packet
                int lenOfMessage = receiveData[index];

                // publish the packet
                final DataPacket receivedPacket =
                    new DataPacket(receiveData, index, lenOfMessage + 1 /* len */, address, portNumber);

                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Received data: {}", ByteUtils.bytesToHex(receivedPacket.getData()));
                }

                messageReceiver.receive(receivedPacket);

                index += lenOfMessage + 1 /* len */;

                LOGGER.info("Move index to: {}", index);
                processedIndex = index;
            }
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            LOGGER.warn("Prepare data packet failed because not all data was delivered in the packet.");
        }
        catch (RuntimeException pex) {
            LOGGER.warn("Receive message failed, reason: {}", pex.getMessage(), pex);
            throw new ProtocolException("Receive and process message failed.");
        }
        finally {
            // append the remaining data
            output.reset();
            if (processedIndex < receivedCount) {
                LOGGER.info("Keep pending data from received packet, processedIndex: {}, receivedCount: {}",
                    processedIndex, receivedCount);
                output.write(receiveData, processedIndex, receivedCount - processedIndex);
            }
            else {
                LOGGER.info("No pending data from received packet.");
            }
        }
    }
}
