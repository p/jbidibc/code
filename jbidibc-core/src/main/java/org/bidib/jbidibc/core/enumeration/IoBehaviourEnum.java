package org.bidib.jbidibc.core.enumeration;

@Deprecated
public enum IoBehaviourEnum implements BidibEnum {
    // @formatter:off
    OUTPUT(0 /* BidibLibrary.BIDIB_PORT_TURN_ON */), HIGH_PULSE(1 /* BidibLibrary.BIDIB_PORT_TURN_OFF */), LOW_PULSE(2), TRI_STATE(
        3), INPUT_PULLUP(4), INPUT_PULLDOWN(5), OUTPUT_LOW_ACTIVE(6 /* BidibLibrary.BIDIB_PORT_??? */), UNKNOWN(0xFF);
    // @formatter:on

    private final byte type;

    IoBehaviourEnum(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a io behaviour enum.
     * 
     * @param type
     *            numeric value of the io behaviour enum
     * 
     * @return IoBehaviourEnum
     */
    public static IoBehaviourEnum valueOf(byte type) {
        IoBehaviourEnum result = null;

        for (IoBehaviourEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a io behaviour enum");
        }
        return result;
    }

    public static IoBehaviourEnum[] getValues() {
        return new IoBehaviourEnum[] { OUTPUT, HIGH_PULSE, LOW_PULSE, TRI_STATE, INPUT_PULLUP, INPUT_PULLDOWN,
            OUTPUT_LOW_ACTIVE, UNKNOWN };
    }

    public static IoBehaviourEnum[] getInputOnlyValues() {
        return new IoBehaviourEnum[] { INPUT_PULLUP, INPUT_PULLDOWN, UNKNOWN };
    }

    public static IoBehaviourEnum[] getOutputOnlyValues() {
        return new IoBehaviourEnum[] { OUTPUT, HIGH_PULSE, LOW_PULSE, TRI_STATE, OUTPUT_LOW_ACTIVE, UNKNOWN };
    }
}
