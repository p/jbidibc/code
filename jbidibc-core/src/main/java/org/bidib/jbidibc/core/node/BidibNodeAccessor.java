package org.bidib.jbidibc.core.node;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibCommand;

public class BidibNodeAccessor {

    /**
     * Send the message via the node without wait for response.
     * 
     * @param node
     *            the node
     * @param message
     *            the message to send
     * @throws ProtocolException
     */
    public static void sendNoWait(final BidibNode node, final BidibCommand message) throws ProtocolException {
        node.sendNoWait(message);
    }

}
