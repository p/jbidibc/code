package org.bidib.jbidibc.net.udp;

import java.net.InetAddress;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bidib.jbidibc.net.BidibNetAddress;

public class BidibUdpNetAddress extends BidibNetAddress {

    private int sessionKey;

    private int sequence;

    public BidibUdpNetAddress(final InetAddress address, final int portNumber) {
        super(address, portNumber);
    }

    /**
     * @return the sessionKey
     */
    public int getSessionKey() {
        return sessionKey;
    }

    /**
     * @param sessionKey
     *            the sessionKey to set
     */
    public void setSessionKey(int sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * @return the sequence
     */
    public int getSequence() {
        return sequence;
    }

    /**
     * @param sequence
     *            the sequence to set
     */
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     * Increment the sequence.
     */
    public void nextSequence() {
        sequence++;
        if (sequence > 65535) {
            sequence = 0;
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
