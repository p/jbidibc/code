package org.bidib.jbidibc.simulation;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.demo.DemoMaster;
import org.bidib.jbidibc.simulation.nodes.FeatureType;
import org.bidib.jbidibc.simulation.nodes.FeaturesType;
import org.bidib.jbidibc.simulation.nodes.LightPortParamsType;
import org.bidib.jbidibc.simulation.nodes.LightPortType;
import org.bidib.jbidibc.simulation.nodes.MasterType;
import org.bidib.jbidibc.simulation.nodes.NodeType;
import org.bidib.jbidibc.simulation.nodes.Simulation;
import org.bidib.jbidibc.simulation.nodes.SubNodesType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SimulatorRegistryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulatorRegistryTest.class);

    private static final String EXPORTED_SIM_TARGET_DIR = "target/exported-sim";

    private final String MASTER_UNIQUE = "da000d680064ea";

    @BeforeTest
    public void prepare() {
        LOGGER.info("Create export directory: {}", EXPORTED_SIM_TARGET_DIR);

        try {
            FileUtils.forceMkdir(new File(EXPORTED_SIM_TARGET_DIR));
        }
        catch (IOException e) {
            LOGGER.warn("Create export directory failed: " + EXPORTED_SIM_TARGET_DIR, e);
        }
    }

    @Test
    public void saveSimulationConfiguration() {

        File exportFile = new File(EXPORTED_SIM_TARGET_DIR, "simulation-test.xml");
        String fileName = exportFile.getPath();

        Simulation simulation = new Simulation();

        long uniqueId = NodeUtils.parseUniqueId(MASTER_UNIQUE);

        MasterType master =
            new MasterType()
                .withAddress(NodeUtils.formatAddress(NodeUtils.ROOT_ADDRESS))
                .withUniqueId(NodeUtils.getUniqueId(uniqueId)).withClassName(DemoMaster.class.getName())
                .withProductName("Demo Master Simulator");
        master
            .withFeatures(new FeaturesType().withFeature(new FeatureType().withType("FEATURE_BM_SIZE").withValue(16)));

        NodeType subNode =
            new NodeType()
                .withAddress(NodeUtils.formatAddress(new byte[] { 1 }))
                .withUniqueId(NodeUtils.getUniqueId(0x05340d6B001234L)).withClassName("DemoSubNode")
                .withProductName("Demo SubNode");
        master.withSubNodes(new SubNodesType().withNode(subNode));

        subNode.withFeatures(
            new FeaturesType().withFeature(new FeatureType().withType("FEATURE_CTRL_MAC_START_MAN").withValue(1),
                new FeatureType().withType("FEATURE_CTRL_MAC_START_DCC").withValue(1)));
        subNode.withLPORT(new LightPortType().withCount(32).withPort(
            new LightPortParamsType()
                .withPortId(0).withDimSlopeDown(6).withDimSlopeUp(3).withIntensityOff(20).withIntensityOn(240),
            new LightPortParamsType()
                .withPortId(1).withDimSlopeDown(6).withDimSlopeUp(3).withIntensityOff(20).withIntensityOn(240),
            new LightPortParamsType()
                .withPortId(2).withDimSlopeDown(6).withDimSlopeUp(3).withIntensityOff(20).withIntensityOn(240)));

        simulation.setMaster(master);

        SimulatorRegistry registry = SimulatorRegistry.getInstance();
        registry.saveSimulationConfiguration(simulation, fileName, false);

    }
}
