package org.bidib.jbidibc.decoder.decoderdb;

import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturersList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManufacturerAccess extends AbstractDecoderDbAccess {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManufacturerAccess.class);

    public ManufacturersList fetch(String login, char[] password) {

        ManufacturersList listManufacturers =
            fetch(login, password, REST_SERVICE_URI, "/?manufacturers", ManufacturersList.class);

        LOGGER.info("Retrieved listManufacturers: {}", listManufacturers);

        return listManufacturers;
    }

}
