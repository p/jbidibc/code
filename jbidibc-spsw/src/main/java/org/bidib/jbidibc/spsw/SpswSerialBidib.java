package org.bidib.jbidibc.spsw;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.exception.InvalidLibraryException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.serial.AbstractSerialBidib;
import org.bidib.jbidibc.serial.SerialMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ibapl.spsw.api.DataBits;
import de.ibapl.spsw.api.FlowControl;
import de.ibapl.spsw.api.Parity;
import de.ibapl.spsw.api.SerialPortSocket;
import de.ibapl.spsw.api.SerialPortSocketFactory;
import de.ibapl.spsw.api.Speed;
import de.ibapl.spsw.api.StopBits;
import de.ibapl.spsw.api.TimeoutIOException;
import de.ibapl.spsw.jniprovider.SerialPortSocketFactoryImpl;

public class SpswSerialBidib extends AbstractSerialBidib {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpswSerialBidib.class);

    private static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("RAW");

    private SerialPortSocket port;

    private ReceiverThread receiverThread;

    private final ServiceLoader<SerialPortSocketFactory> serialPortSocketLoader;

    private SerialPortSocketFactory serialPortFactory;

    private boolean useHardwareFlowControl;

    private SpswSerialBidib() {
        serialPortSocketLoader = ServiceLoader.load(SerialPortSocketFactory.class);

        for (SerialPortSocketFactory factory : serialPortSocketLoader) {

            if (factory instanceof SerialPortSocketFactoryImpl) {
                this.serialPortFactory = factory;
                break;
            }
        }

        if (serialPortFactory == null) {
            throw new IllegalArgumentException("Failed to fetch the SPSW serial port factory.");
        }
    }

    /**
     * Get a new initialized instance of SpswSerialBidib.
     *
     * @return the instance of SpswSerialBidib
     */
    public static BidibInterface createInstance() {
        LOGGER.info("Create new instance of SpswSerialBidib.");

        SpswSerialBidib instance = new SpswSerialBidib();
        instance.initialize();

        return instance;
    }

    @Override
    public void close() {

        if (port != null) {

            LOGGER.info("Start closing the port: {}", port);
            long start = System.currentTimeMillis();

            // Deactivate DTR
            if (this.useHardwareFlowControl) {
                try {
                    LOGGER.info("Deactivate DTR.");

                    port.setDTR(false); // pin 1 in DIN8; on main connector, this is DTR
                }
                catch (Exception e) {
                    LOGGER.warn("Set DTR to false failed.", e);
                }
            }

            LOGGER.info("Set the receiver running flag to false.");
            receiverRunning.set(false);

            final BidibMessageProcessor serialMessageReceiver = getMessageReceiver();
            stopReceiverAndQueues(serialMessageReceiver);

            try {
                fireCtsChanged(false);
            }
            catch (Exception e) {
                LOGGER.warn("Get CTS value failed.", e);
            }

            try {
                port.close();
            }
            catch (IOException ex) {
                LOGGER.warn("Close serial port failed.", ex);
            }

            port = null;

            stopReceiverThread();

            long end = System.currentTimeMillis();
            LOGGER.info("Closed the port. duration: {}", end - start);

            cleanupAfterClose(serialMessageReceiver);
        }
        else {
            LOGGER.info("No port to close available.");
        }
    }

    @Override
    public List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new ArrayList<>();
        try {
            // get the comm port identifiers
            portIdentifiers.addAll(serialPortFactory.getPortNames(true));
        }
        catch (UnsatisfiedLinkError ule) {
            LOGGER.warn("Get comm port identifiers failed.", ule);
            throw new InvalidLibraryException(ule.getMessage(), ule.getCause());
        }
        catch (Error error) {
            LOGGER.warn("Get comm port identifiers failed.", error);
            throw new RuntimeException(error.getMessage(), error.getCause());
        }
        return portIdentifiers;
    }

    @Override
    protected boolean isImplAvaiable() {
        return (port != null);
    }

    @Override
    protected void internalOpen(String portName, Context context) throws Exception {

        final BidibMessageProcessor serialMessageReceiver = getMessageReceiver();

        SerialPortSocket serialPort = serialPortFactory.createSerialPortSocket(portName);

        if (serialPort == null) {
            throw new IllegalArgumentException("Failed to open SPSW port: " + portName);
        }

        Boolean useHardwareFlowControl = context.get("serial.useHardwareFlowControl", Boolean.class, Boolean.TRUE);

        LOGGER.info("Open port with portName: {}, useHardwareFlowControl: {}", portName, useHardwareFlowControl);

        Set<FlowControl> flowControl = FlowControl.getFC_NONE();
        if (useHardwareFlowControl) {
            LOGGER.info("Set flow control mode to RTS_CTS!");
            flowControl = FlowControl.getFC_RTS_CTS();

            this.useHardwareFlowControl = true;
        }
        else {
            LOGGER.info("Set flow control mode to NONE!");
            this.useHardwareFlowControl = false;
        }

        Integer baudRate = context.get("serial.baudrate", Integer.class, Integer.valueOf(115200));
        LOGGER.info("Open port with baudRate: {}", baudRate);

        serialPort.open(Speed.fromNative(baudRate), DataBits.DB_8, StopBits.SB_1, Parity.NONE, flowControl);

        // keep the port
        port = serialPort;

        serialPort.setTimeouts(2, 100, 100);

        startReceiverAndQueues(serialMessageReceiver, context);

        // Activate DTR
        if (this.useHardwareFlowControl) {
            try {
                LOGGER.info("Activate DTR.");
                serialPort.setDTR(true);
            }
            catch (Exception e) {
                LOGGER.warn("Set DTR true failed.", e);
            }
        }

        try {
            if (this.useHardwareFlowControl) {
                fireCtsChanged(serialPort.isCTS());
            }
            else {
                fireCtsChanged(true);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Get CTS value failed.", e);
        }
    }

    @Override
    public boolean isOpened() {
        return port != null && port.isOpen();
    }

    private final ByteArrayOutputStream sendBuffer = new ByteArrayOutputStream(2048);

    @Override
    protected void sendData(ByteArrayOutputStream data) {
        if (port != null && data != null) {

            try {
                sendBuffer.reset();

                if (this.useHardwareFlowControl && !port.isCTS()) {
                    LOGGER.error("CTS not set! The receiving part is not ready!");

                    throw new RuntimeException("CTS not set! The receiving part is not ready!");
                }

                final OutputStream os = port.getOutputStream();

                if (!firstPacketSent) {
                    LOGGER.info("Send initial sequence.");

                    try {
                        byte[] initialSequence = new byte[] { (byte) BidibLibrary.BIDIB_PKT_MAGIC };
                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
                            MSG_RAW_LOGGER.info(">> [{}] - {}", initialSequence.length,
                                ByteUtils.bytesToHex(initialSequence));
                        }
                        os.write(initialSequence);
                        Thread.sleep(10);
                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
                            MSG_RAW_LOGGER.info(">> [{}] - {}", initialSequence.length,
                                ByteUtils.bytesToHex(initialSequence));
                        }
                        os.write(initialSequence);

                        firstPacketSent = true;

                        LOGGER.info("Send initial sequence passed.");
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Send initial sequence failed.", ex);
                    }
                }

                SerialMessageEncoder.encodeMessage(data, sendBuffer);

                if (MSG_RAW_LOGGER.isInfoEnabled()) {
                    MSG_RAW_LOGGER.info(">> [{}] - {}", sendBuffer.toByteArray().length,
                        ByteUtils.bytesToHex(sendBuffer.toByteArray()));
                }

                os.write(sendBuffer.toByteArray());

                os.flush();
            }
            catch (IOException ex) {
                byte[] bytes = data.toByteArray();
                LOGGER.warn("Send message to output stream failed: [{}] - {}", bytes.length,
                    ByteUtils.bytesToHex(bytes));

                throw new RuntimeException("Send message to output stream failed: " + ByteUtils.bytesToHex(bytes), ex);
            }
            finally {
                sendBuffer.reset();
            }
        }
    }

    @Override
    protected void startReceiverAndQueues(BidibMessageProcessor serialMessageReceiver, Context context) {
        LOGGER.info("Start receiver and queues.");

        if (receiverThread == null) {
            receiverThread = new ReceiverThread();
        }

        receiverThread.start();

        super.startReceiverAndQueues(serialMessageReceiver, context);
    }

    @Override
    protected void stopReceiverAndQueues(final BidibMessageProcessor serialMessageReceiver) {
        LOGGER.info("Stop receiver and queues.");

        super.stopReceiverAndQueues(serialMessageReceiver);
    }

    private byte[] inputBuffer = new byte[2048];

    private AtomicBoolean receiverRunning = new AtomicBoolean();

    private void stopReceiverThread() {
        LOGGER.info("Stop the receiver thread by set the running flag to false.");
        receiverRunning.set(false);

        if (receiverThread != null) {
            LOGGER.info("Wait for termination of receiver thread.");

            synchronized (receiverThread) {
                try {
                    receiverThread.join(5000);
                }
                catch (InterruptedException ex) {
                    LOGGER.warn("Wait for termination of receiver thread failed.", ex);
                }
            }

            LOGGER.info("Free the receiver thread.");
            receiverThread = null;
        }

    }

    public class ReceiverThread extends Thread {

        @Override
        public void run() {
            receiverRunning.set(true);

            final SerialPortSocket serialPort = port;

            while (receiverRunning.get()) {
                try {
                    InputStream input = serialPort.getInputStream();
                    int len = input.read(inputBuffer, 0, inputBuffer.length);

                    // LOGGER.trace("Read len: {}", len);

                    if (len < 0) {
                        // check if the port was closed.
                        boolean portClosed = serialPort.isClosed();
                        LOGGER.info("Port closed: {}", portClosed);

                        if (portClosed) {
                            // say good-bye
                            LOGGER.info("The port is closed. Leave the receiver loop.");

                            receiverRunning.set(false);
                            continue;
                        }
                    }

                    int remaining = input.available();
                    if (remaining > 0) {
                        LOGGER.warn("More data in inputStream might be available, remaining: {}", remaining);
                    }

                    if (len > -1) {
                        receive(inputBuffer, len);
                    }
                }
                catch (TimeoutIOException ex) {
                    LOGGER.trace("Timout during wait for data.");
                }
                catch (IOException ex) {
                    LOGGER.error("Receive data failed with an exception!", ex);

                    receiverRunning.set(false);

                    if (serialPort == null || serialPort.isOpen()) {
                        triggerClosePort();
                    }

                }
                catch (NullPointerException ex) {
                    LOGGER.error("Receive data failed with an NPE! The port might be closed.", ex);

                    receiverRunning.set(false);
                }
                catch (Exception ex) {
                    LOGGER.error("Message receiver returned from receive with an exception!", ex);
                }
            }

            LOGGER.info("Leaving receiver loop.");
        }

    }

    private void triggerClosePort() {
        LOGGER.warn("Close the port.");
        Thread worker = new Thread(new Runnable() {
            public void run() {

                LOGGER.info("Start close port because error was detected.");
                try {
                    // the listeners are notified in close()
                    close();
                }
                catch (Exception ex) {
                    LOGGER.warn("Close after error failed.", ex);
                }
                LOGGER.warn("The port was closed.");
            }
        });
        worker.start();

    }

}
