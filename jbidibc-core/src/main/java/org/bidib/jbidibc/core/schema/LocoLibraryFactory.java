package org.bidib.jbidibc.core.schema;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.bidib.jbidibc.core.schema.exception.InvalidContentException;
import org.bidib.jbidibc.core.schema.locolibrary.v1_0.LocoLibrary;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class LocoLibraryFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocoLibraryFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.locolibrary.v1_0";

    public static final String XSD_LOCATION = "/xsd/locoLibrary.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/locolibrary/1.0 xsd/locoLibrary.xsd";

    private LocoLibraryFactory() {
    }

    /**
     * Load the locoLibrary from the provided file.
     * 
     * @param locoLibraryFile
     *            the file
     * @return the loco library
     */
    public static LocoLibrary getLocoLibrary(File locoLibraryFile) {
        return new LocoLibraryFactory().loadLocoLibraryFile(locoLibraryFile);
    }

    private LocoLibrary loadLocoLibraryFile(File locoLibraryFile) {

        LocoLibrary locoLibrary = null;

        try (InputStream is = new FileInputStream(locoLibraryFile)) {
            locoLibrary = loadLocoLibrary(is);
        }
        catch (IOException ex) {
            LOGGER.info("No locoLibrary file found.");
        }
        return locoLibrary;
    }

    private LocoLibrary loadLocoLibrary(InputStream is) {

        LocoLibrary locoLibrary = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(LocoLibraryFactory.class.getResourceAsStream(XSD_LOCATION));

            Schema schema = schemaFactory.newSchema(new Source[] { streamSource });
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            JAXBElement<LocoLibrary> jaxbElement =
                (JAXBElement<LocoLibrary>) unmarshaller.unmarshal(xmlr, LocoLibrary.class);
            locoLibrary = jaxbElement.getValue();
        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from input stream failed failed.", ex);

            List<String> errors = validate(is);

            throw new InvalidContentException("Load LocoLibraryType failed.", errors);
        }
        return locoLibrary;
    }

    /**
     * Save the loco library.
     * 
     * @param locoLibrary
     *            the instance
     * @param fileName
     *            the filename
     */
    public static void saveLocoLibrary(final LocoLibrary locoLibrary, File file) {
        LOGGER.info("Save locoLibrary to file: {}, locoLibrary: {}", file.getPath(), locoLibrary);
        OutputStream os = null;
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(LocoLibraryFactory.class.getResourceAsStream(XSD_LOCATION));

            Schema schema = schemaFactory.newSchema(new Source[] { streamSource });
            marshaller.setSchema(schema);

            ByteArrayOutputStream bas = new ByteArrayOutputStream();

            marshaller.marshal(locoLibrary, new OutputStreamWriter(bas, Charset.forName("UTF-8")));

            os = new BufferedOutputStream(new FileOutputStream(file));

            os.write(bas.toByteArray());

            os.flush();

            LOGGER.info("Save LibraryType content to file passed: {}", file.getPath());
        }
        catch (IOException ex) {
            // TODO add better exception handling
            LOGGER.warn("Save loco library failed.", ex);

            throw new RuntimeException(ex.getMessage(), ex);
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save loco library failed.", ex);

            throw new RuntimeException("Save loco library failed.", ex);
        }
        finally {
            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputstream failed.", ex);
                }
            }
        }
    }

    private List<String> validate(final InputStream is) {
        List<String> errors = null;

        if (is instanceof FileInputStream) {
            FileInputStream fis = (FileInputStream) is;
            try {
                LOGGER.info("Try to set file position to 0.");
                fis.getChannel().position(0);
            }
            catch (IOException e) {
                LOGGER.warn("Set file position to 0 failed.", e);
            }
        }

        StreamSource inputStreamSource = new StreamSource(is);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(LocoLibraryFactory.class.getResourceAsStream(XSD_LOCATION));

        XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();

        try {
            Schema schema = schemaFactory.newSchema(streamSource);

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);

            validator.validate(inputStreamSource);
        }
        catch (IOException ex) {
            LOGGER.warn("Validate failed.", ex);
        }
        catch (SAXException ex) {
            LOGGER.warn("Validate failed.", ex);
        }

        errors = errorHandler.getErrors();
        LOGGER.info("Found errors: {}", errors);

        return errors;
    }

}
