package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.RcPlusBindData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodes;
import org.bidib.jbidibc.core.exception.ProtocolException;

/**
 * Command to send programming commands for railcom plus
 */
public class CommandStationRcPlusMessage extends BidibCommandMessage {
    /**
     * Create the command for railcom plus.
     * 
     * @param opCode
     *            the operation code
     */
    public CommandStationRcPlusMessage(RcPlusOpCodes opCode) {
        super(0, BidibLibrary.MSG_CS_RCPLUS, prepareData(opCode, (byte[]) null));
    }

    /**
     * Create the command for railcom plus.
     * 
     * @param opCode
     *            the operation code
     */
    public CommandStationRcPlusMessage(RcPlusOpCodes opCode, byte[] data) {
        super(0, BidibLibrary.MSG_CS_RCPLUS, prepareData(opCode, data));
    }

    /**
     * Create the command for railcom plus.
     * 
     * @param opCode
     *            the operation code
     * @param tid
     *            the TID
     */
    public CommandStationRcPlusMessage(RcPlusOpCodes opCode, TidData tid) {
        super(0, BidibLibrary.MSG_CS_RCPLUS, prepareData(opCode, tid));
    }

    /**
     * Create the command for railcom plus.
     * 
     * @param opCode
     *            the operation code
     * @param decoder
     *            the decoder data
     */
    public CommandStationRcPlusMessage(RcPlusOpCodes opCode, DecoderUniqueIdData decoder) {
        super(0, BidibLibrary.MSG_CS_RCPLUS, prepareData(opCode, decoder));
    }

    public CommandStationRcPlusMessage(RcPlusBindData bindData) {
        super(0, BidibLibrary.MSG_CS_RCPLUS, prepareData(RcPlusOpCodes.RC_BIND, bindData));
    }

    /**
     * Create the command for railcom plus.
     * 
     * @param message
     *            the message
     * @throws ProtocolException
     */
    public CommandStationRcPlusMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_CS_RCPLUS";
    }

    private static byte[] prepareData(RcPlusOpCodes opCode, DecoderUniqueIdData decoder) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // op code
        out.write(opCode.getType());

        if (decoder != null) {
            // write decoder data
            decoder.writeToStream(out);
        }
        return out.toByteArray();
    }

    private static byte[] prepareData(RcPlusOpCodes opCode, TidData tid) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // op code
        out.write(opCode.getType());

        if (tid != null) {
            // write tid
            tid.writeToStream(out);
        }
        return out.toByteArray();
    }

    private static byte[] prepareData(RcPlusOpCodes opCode, byte[] data) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // op code
        out.write(opCode.getType());

        if (data != null) {
            // data
            out.write(data, 0, data.length);
        }
        return out.toByteArray();
    }

    private static byte[] prepareData(RcPlusOpCodes opCode, RcPlusBindData bindData) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // op code
        out.write(opCode.getType());
        bindData.writeToStream(out);
        return out.toByteArray();
    }

    public RcPlusOpCodes getOpCode() {
        byte opCode = getData()[0];
        return RcPlusOpCodes.valueOf(opCode);
    }

    public TidData getTid() {
        TidData tid = TidData.fromByteArray(getData(), 1);
        return tid;
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return new Integer[] { CommandStationRcPlusAcknowledgeResponse.TYPE };
    }
}
