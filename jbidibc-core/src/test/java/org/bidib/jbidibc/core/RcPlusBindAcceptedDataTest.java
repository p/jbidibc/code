package org.bidib.jbidibc.core;

import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RcPlusBindAcceptedDataTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RcPlusBindAcceptedDataTest.class);

    @Test
    public void getShortAddress() {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message =
            { 0x0C, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_ACCEPTED, 0x05, 0x0A, 0x0B, 0x0C, 0x0D, 13, 0x0C, 0 };

        RcPlusBindAcceptedData answerData = RcPlusBindAcceptedData.fromByteArray(message, 5);
        Assert.assertNotNull(answerData);

        LOGGER.info("Current answerData: {}", answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.RECEIVED_DECODER_ACKN);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));

        Assert.assertEquals(answerData.getAddress(), new AddressData(0x0C, AddressTypeEnum.LOCOMOTIVE_FORWARD));
    }

    @Test
    public void getLongAddress() {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message =
            { 0x0C, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_ACCEPTED, 0x05, 0x0A, 0x0B, 0x0C, 0x0D, 13, 0x0C,
                0x0C };

        RcPlusBindAcceptedData answerData = RcPlusBindAcceptedData.fromByteArray(message, 5);
        Assert.assertNotNull(answerData);

        LOGGER.info("Current answerData: {}", answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.RECEIVED_DECODER_ACKN);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));

        Assert.assertEquals(answerData.getAddress(), new AddressData(0x0C0C, AddressTypeEnum.LOCOMOTIVE_FORWARD));
    }

    @Test
    public void getLongAddressStdAccessory() {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message =
            { 0x0C, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_ACCEPTED, 0x05, 0x0A, 0x0B, 0x0C, 0x0D, 13, 0x0C,
                0x4C };

        RcPlusBindAcceptedData answerData = RcPlusBindAcceptedData.fromByteArray(message, 5);
        Assert.assertNotNull(answerData);

        LOGGER.info("Current answerData: {}", answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.RECEIVED_DECODER_ACKN);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));

        Assert.assertEquals(answerData.getAddress(), new AddressData(0x0C0C, AddressTypeEnum.ACCESSORY));
    }

    @Test
    public void getLongAddressExtAccessory() {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message =
            { 0x0C, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_ACCEPTED, 0x05, 0x0A, 0x0B, 0x0C, 0x0D, 13, 0x0C,
                (byte) 0xCC };

        RcPlusBindAcceptedData answerData = RcPlusBindAcceptedData.fromByteArray(message, 5);
        Assert.assertNotNull(answerData);

        LOGGER.info("Current answerData: {}", answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.RECEIVED_DECODER_ACKN);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));

        Assert.assertEquals(answerData.getAddress(), new AddressData(0x0C0C, AddressTypeEnum.EXTENDED_ACCESSORY));
    }
}
