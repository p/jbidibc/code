package org.bidib.jbidibc.core.accessory;

public interface OptionsValue<T> {

    T getValue();
}
