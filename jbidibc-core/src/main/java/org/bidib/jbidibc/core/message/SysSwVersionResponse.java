package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SysSwVersionResponse extends BidibMessage {
	private static final Logger LOGGER = LoggerFactory.getLogger(SysSwVersionResponse.class);
	
    public static final Integer TYPE = BidibLibrary.MSG_SYS_SW_VERSION;

    SysSwVersionResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 3) {
            throw new ProtocolException("No valid MSG_SYS_SW_VERSION received.");
        }
    }

    public SysSwVersionResponse(byte[] addr, int num, byte[] version) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_SYS_SW_VERSION, version);
    }

    public String getName() {
        return "MSG_SYS_SW_VERSION";
    }

    public SoftwareVersion getVersion() {
        byte[] data = getData();

        SoftwareVersion softwareVersion = new SoftwareVersion(ByteUtils.getInt(data[2]), ByteUtils.getInt(data[1]), ByteUtils.getInt(data[0]));
        if (data.length > 3) {
        	// add the children versions
        	int totalChildren = (data.length - 3) / 3;
        	LOGGER.info("Total number of children: {}", totalChildren);
        	
        	int index = 3;
        	while (index < data.length) {
        		SoftwareVersion childVersion = new SoftwareVersion(ByteUtils.getInt(data[index+2]), ByteUtils.getInt(data[index+1]), ByteUtils.getInt(data[index]));
        		softwareVersion.addChildVersion(childVersion);
        		index += 3;
        	}
        }
        return softwareVersion;
    }
}
