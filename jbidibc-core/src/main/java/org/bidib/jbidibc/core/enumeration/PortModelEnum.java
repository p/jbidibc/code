package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;

public enum PortModelEnum {
    type, flat, flat_extended;

    public static PortModelEnum getPortModel(final Node node) {
        // TODO add support for flat_extended

        // this is a hack to support wrong implementation on OneControl and OneDriveTurn
        if (ProductUtils.isOneControl(node.getUniqueId())
            && node.getSoftwareVersion().compareTo(SoftwareVersion.build(2, 0, 6)) < 1) {

            return type;
        }
        // special processing for OneDriveTurn < 1.01.07
        else if (ProductUtils.isOneDriveTurn(node.getUniqueId())
            && node.getSoftwareVersion().compareTo(SoftwareVersion.build(1, 1, 7)) < 1) {

            return type;
        }

        if (node.isPortFlatModelAvailable()) {
            return flat;
        }
        return type;
    }

    /**
     * @param portModel
     *            the port model to use
     * @param lowByte
     *            the low byte of the port values
     * @return the port type
     */
    public static LcOutputType getPortType(PortModelEnum portModel, byte lowByte) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat_extended:
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(lowByte);
                break;
        }
        return outputType;
    }

    /**
     * @param portModel
     *            the port model to use
     * @param lowByte
     *            the low byte of the port values
     * @param highByte
     *            the high byte of the port values
     * @return the port number
     */
    public static int getPortNumber(PortModelEnum portModel, byte lowByte, byte highByte) {
        int portNumber = -1;
        switch (portModel) {
            case flat_extended:
                portNumber = ByteUtils.getInt(lowByte, highByte & 0x7F);
                break;
            case flat:
                portNumber = ByteUtils.getInt(lowByte);
                break;
            default:
                portNumber = ByteUtils.getInt(highByte, 0x7F);
                break;
        }
        return portNumber;
    }
}
