package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.RcPlusDecoderAnswerData;
import org.bidib.jbidibc.core.RcPlusFeedbackBindData;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.enumeration.LcMacroState;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodeBm;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodesAck;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtilsTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BidibResponseFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibResponseFactoryTest.class);

    @Test
    public void createValidSysMagicResponseMessage() throws ProtocolException {
        byte[] message = { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof SysMagicResponse, "Expected a SysMagicResponse message.");
        Assert.assertEquals(bidibMessage.getType(), (byte) BidibLibrary.MSG_SYS_MAGIC);
        Assert.assertEquals(((SysMagicResponse) bidibMessage).getMagic(), 0xAFFE);
    }

    @Test
    public void createMessageBootMagicResponseFromByteArray() throws ProtocolException {
        byte[] message = { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0x0D, (byte) 0xB0 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof SysMagicResponse, "Expected a SysMagicResponse message.");
        Assert.assertEquals(bidibMessage.getType(), (byte) BidibLibrary.MSG_SYS_MAGIC);
        Assert.assertEquals(((SysMagicResponse) bidibMessage).getMagic(), 0xB00D);
    }

    @Test(expectedExceptions = ProtocolException.class, expectedExceptionsMessageRegExp = "No valid MSG_SYS_MAGIC received.")
    public void createInvalidSysMagicResponseMessage() throws ProtocolException {
        byte[] message = { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAD };

        new BidibResponseFactory().create(message);

        Assert.fail("Should have thrown an exception!");
    }

    @Test
    public void createValidNodeTabCountResponseMessage() throws ProtocolException {
        byte[] message = { 0x04, 0x00, 0x01, (byte) 0x88, 0x01 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeTabCountResponse, "Expected a NodeTabCountResponse message.");
    }

    @Test
    public void createValidNodeTabResponseMessage() throws ProtocolException {
        byte[] message = { 0x0c, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0xc0, 0x00, 0x0d, 0x68, 0x00, 0x01, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeTabResponse, "Expected a NodeTabResponse message.");

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        Node node = nodeTabResponse.getNode(nodeTabResponse.getAddr());
        Assert.assertNotNull(node);
        Assert.assertEquals(node.getAddr(), new byte[] { 0 });
    }

    @Test
    public void createValidNodeTabResponse2Message() throws ProtocolException {
        // 18.08.2013 09:07:38.188: receive NodeTabResponse[[0],num=1,type=137,data=[1, 1, 5, 0, 13, 108, 0, 51, 0]] :
        // 0c 00 01 89 01 01 05 00 0d 6c 00 33 00

        byte[] message =
            new byte[] { 0x0c, 0x00, 0x01, (byte) 0x89, 0x01, 0x01, 0x05, 0x00, (byte) 0x0d, (byte) 0x6c, 0x00, 0x33,
                0x00 };
        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeTabResponse, "Expected a NodeTabResponse message.");

        // 09:07:38.148 [INFO] org.bidib.jbidibc.message.NodeTabResponse [main] - Created new node:
        // Node[version=1,addr=[0],uniqueId=0xd2000d68000036]
        Node parentNode = new Node(1, new byte[] { 0 }, NodeUtilsTest.prepareUniqueid("d2000d68000036"));

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        Node node = nodeTabResponse.getNode(parentNode.getAddr());
        Assert.assertNotNull(node);
        Assert.assertEquals(node.getAddr(), new byte[] { 1 });
    }

    @Test
    public void createValidNodeTabResponse4Message() throws ProtocolException {
        // 15:03:15.910 [DEBUG] org.bidib.jbidibc.MessageReceiver [Thread-9] - Received raw message: 0d 01 00 03 89 02
        // 00 81 00 0d 72 00 1e 00 16 fe

        // message: length msg_addr msg_num msg_type data
        //
        // 0x0d : len -> 13
        // 01 00 : msg_addr -> 1
        // 03 : msg_num -> 3
        // 0x89 : msg_type -> node tab response

        // nodetab_data: nodetab_version nodetab_entry
        // 02 : version -> 2
        // nodetab_entry: node_addr uniqueId
        // 00 : node_addr -> 0
        // 81 00 0d 72 00 1e 00 16 fe

        byte[] message =
            new byte[] { 0x0d, 0x01, 0x00, 0x03, (byte) 0x89, 0x02, 0x00, (byte) 0x81, 0x00, (byte) 0x0d, (byte) 0x72,
                0x00, 0x1e, 0x00, 0x16, (byte) 0xfe };
        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeTabResponse, "Expected a NodeTabResponse message.");

        // 09:07:38.148 [INFO] org.bidib.jbidibc.message.NodeTabResponse [main] - Created new node:
        // Node[version=1,addr=[0],uniqueId=0xd2000d68000036]
        Node parentNode = new Node(1, new byte[] { 1 }, NodeUtilsTest.prepareUniqueid("81000d72001e00"));

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        Node node = nodeTabResponse.getNode(parentNode.getAddr());
        Assert.assertNotNull(node);
        Assert.assertEquals(node.getAddr(), new byte[] { 1 });
    }

    @Test
    public void createValidNodeNewResponseMessage() throws ProtocolException {
        // 09:34:34.047 [DEBUG] org.bidib.jbidibc.MessageReceiver [Thread-9] - Received raw message: 0c 00 21 8d 06 01
        // 05 00 0d 6b 00 69 ea aa fe

        byte[] message =
            new byte[] { 0x0c, 0x00, 0x21, (byte) 0x8d, 0x06, 0x01, 0x05, 0x00, (byte) 0x0d, (byte) 0x6b, 0x00, 0x69,
                (byte) 0xea, (byte) 0xaa, (byte) 0xfe };
        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeNewResponse, "Expected a NodeNewResponse message.");

        // 09:07:38.148 [INFO] org.bidib.jbidibc.message.NodeTabResponse [main] - Created new node:
        // Node[version=1,addr=[0],uniqueId=0xd2000d68000036]
        // Node parentNode = new Node(1, new byte[] { 0 }, NodeUtilsTest.prepareUniqueid("d2000d68000036"));

        NodeNewResponse nodeNewResponse = (NodeNewResponse) bidibMessage;
        Node node = nodeNewResponse.getNode(bidibMessage.getAddr());
        Assert.assertNotNull(node);
        Assert.assertEquals(node.getAddr(), new byte[] { 1 });
    }

    @Test
    public void createValidNodeNewResponse2Message() throws ProtocolException {
        // 09:34:34.047 [DEBUG] org.bidib.jbidibc.MessageReceiver [Thread-9] - Received raw message: 0c 00 21 8d 06 01
        // 05 00 0d 6b 00 69 ea aa fe
        // 21:55:23.149 [DEBUG] org.bidib.jbidibc.MessageReceiver [Thread-9] - Received raw message: 0d 01 00 01 8d 06
        // 01 42 00 0d 67 00 65 ea fb fe
        // 15:03:15.910 [DEBUG] org.bidib.jbidibc.MessageReceiver [Thread-9] - Received raw message: 0d 01 00 03 89 02
        // 00 81 00 0d 72 00 1e 00 16 fe

        // message: length msg_addr msg_num msg_type data
        //
        // 0x0d : len -> 13
        // 01 00 : msg_addr -> 1
        // 01 : msg_num -> 1
        // 0x8d : msg_type -> node new

        // nodetab_data: nodetab_version nodetab_entry
        // 06 : version -> 6
        // nodetab_entry: node_addr uniqueId
        // 01 : node_addr -> 1
        // 42 00 0d 67 00 65 ea fb fe

        byte[] message =
            new byte[] { 0x0D, 0x01, 0x00, 0x01, (byte) 0x8d, 0x06, 0x01, 0x42, 0x00, (byte) 0x0d, (byte) 0x67, 0x00,
                0x65, (byte) 0xea, (byte) 0xfb, (byte) 0xfe };
        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeNewResponse, "Expected a NodeNewResponse message.");

        // 09:07:38.148 [INFO] org.bidib.jbidibc.message.NodeTabResponse [main] - Created new node:
        // Node[version=1,addr=[0],uniqueId=0xd2000d68000036]
        // Node parentNode = new Node(1, new byte[] { 0 }, NodeUtilsTest.prepareUniqueid("d2000d68000036"));

        NodeNewResponse nodeNewResponse = (NodeNewResponse) bidibMessage;
        Node node = nodeNewResponse.getNode(bidibMessage.getAddr());
        Assert.assertNotNull(node);
        Assert.assertEquals(node.getAddr(), new byte[] { 1, 1 });
    }

    // [20130817165457078] Input : |0C 00 7C 8D 02 01 81 00 0D 72 00 1F 00 45 FE |
    // [20130817165457109] IN --- : MSG_NODE_NEW 0C 00 7C 8D 02 01 81 00 0D 72 00 1F 00
    // Node Count 2
    // Node added : 81.00.0D.72.00.1F.00 with Adr : 1.0.0.0
    //
    // [20130817165457531] Input : |0D 01 00 06 89 01 00 81 00 0D 72 00 1F 00 8D FE |
    //
    // [20130817165509375] Input : |0D 01 00 14 8D 02 01 42 00 0D 67 00 65 EA 16 FE |
    // [20130817165509406] IN --- : MSG_NODE_NEW 0D 01 00 14 8D 02 01 42 00 0D 67 00 65 EA
    // Node Count 3
    // Node added : 42.00.0D.67.00.65.EA with Adr : 1.1.0.0

    @Test
    public void createValidNodeTabResponse3Message() throws ProtocolException {
        // 14.08.2013 21:55:23.149: receive NodeNewResponse[[1, 0],num=1,type=141,data=[6, 1, 66, 0, 13, 103, 0, 101,
        // 234]] : 0d 01 00 01 8d 06 01 42 00 0d 67 00 65 ea

        byte[] message =
            new byte[] { 0x0d, 0x01, 0x00, 0x01, (byte) 0x8d, 0x06, 0x01, (byte) 0x42, 0x00, (byte) 0x0d, (byte) 0x67,
                0x00, 0x65, (byte) 0xea };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeTabResponse, "Expected a NodeTabResponse message.");

        // Create new node has finished: Node[version=2,addr=[1, 0],uniqueId=0x81000d72001f00]
        Node parentNode = new Node(2, new byte[] { 1 }, NodeUtilsTest.prepareUniqueid("81000d72001f00"));

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        Node node = nodeTabResponse.getNode(parentNode.getAddr());
        Assert.assertNotNull(node);

        Assert.assertEquals(node.getAddr(), new byte[] { 1, 1 });
    }

    @Test
    public void createValidNodeTabResponse5Message() throws ProtocolException {
        // 29.08.2013 07:00:29.919: receive NodeTabResponse[[2],num=1,type=137,data=[1, 1, 5, 0, 13, 108, 0, 51, 0]] :
        // 0d 02 00 01 89 01 01 05 00 0d 6c 00 33 00

        byte[] message =
            new byte[] { 0x0d, 0x02, 0x00, 0x01, (byte) 0x89, 0x01, 0x01, (byte) 0x05, 0x00, (byte) 0x0d, (byte) 0x6c,
                0x00, 0x33, (byte) 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof NodeTabResponse, "Expected a NodeTabResponse message.");

        // Create new node has finished: Node[version=2,addr=[1, 0],uniqueId=0x81000d72001f00]
        Node parentNode = new Node(2, new byte[] { 2 }, NodeUtilsTest.prepareUniqueid("81000d72001f00"));

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        Node node = nodeTabResponse.getNode(parentNode.getAddr());
        Assert.assertNotNull(node);

        Assert.assertEquals(node.getAddr(), new byte[] { 2, 1 });
    }

    @Test
    public void createValidFeatureResponseMessage() throws ProtocolException {
        byte[] message = { 0x05, 0x00, 0x01, (byte) 0x90, 0x00, 0x10 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof FeatureResponse, "Expected a FeatureResponse message.");
    }

    @Test
    public void createValidFeatureNotAvailableResponseMessage() throws ProtocolException {
        byte[] message = { 0x04, 0x00, 0x04, (byte) 0x91, (byte) 0xfd, (byte) 0xde };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof FeatureNotAvailableResponse,
            "Expected a FeatureNotAvailableResponse message.");
    }

    @Test(expectedExceptions = ProtocolException.class, expectedExceptionsMessageRegExp = "got unknown response with type 223")
    public void createUndefinedResponseMessage() throws ProtocolException {
        byte[] message = { 0x04, 0x00, 0x01, (byte) 0xDF, 0x01 };

        new BidibResponseFactory().create(message);

        Assert.fail("Should have thrown an exception!");
    }

    @Test
    public void createBoostStatOffResponseMessage() throws ProtocolException {
        // 04 00 04 B1 2C
        byte[] message = { 0x04, 0x00, 0x0e, (byte) 0xB0, (byte) 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof BoostStatResponse, "Expected a BoostStatResponse message.");

        LOGGER.info("Booster state: {}", ((BoostStatResponse) bidibMessage).getState());
        Assert.assertEquals(((BoostStatResponse) bidibMessage).getState(), BoosterState.OFF);
    }

    @Test
    public void createBoostStatOffNoDccResponseMessage() throws ProtocolException {
        // 04 00 04 B1 2C
        byte[] message = { 0x04, 0x00, 0x11, (byte) 0xB0, (byte) 0x06 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof BoostStatResponse, "Expected a BoostStatResponse message.");

        LOGGER.info("Booster state: {}", ((BoostStatResponse) bidibMessage).getState());
        Assert.assertEquals(((BoostStatResponse) bidibMessage).getState(), BoosterState.OFF_NO_DCC);
    }

    @Test
    public void createValidFeedbackMultipleResponse() throws ProtocolException {
        byte[] message = { 0x07, 0x00, 0x01, (byte) 0xA2, 0x01, (byte) 0x80, 0x03, 0x04 };
        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertEquals(((FeedbackMultipleResponse) bidibMessage).getSize(), 128);
    }

    @Test
    public void createMessageFeedbackMultipleResponseFromByteArray() throws ProtocolException {
        byte[] message =
            { 0x16, 0x01, 0x00, (byte) 0x86, (byte) 0xA2, 0x00, (byte) 0x80, (byte) 0xff, (byte) 0xff, (byte) 0xff,
                (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
                (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertEquals(bidibMessage.getType(), (byte) BidibLibrary.MSG_BM_MULTIPLE);

        Assert.assertEquals(((FeedbackMultipleResponse) bidibMessage).getSize(), 0x80);
    }

    @Test
    public void createMessageCommandStationDriveManualResponseFromByteArray() throws ProtocolException {
        byte[] message = { 0x0C, 0x00, 0x14, (byte) 0xE5, 0x5E, 0x00, 0x02, 0x02, 0x00, 0x10, 0x00, 0x00, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertEquals(bidibMessage.getType(), (byte) BidibLibrary.MSG_CS_DRIVE_MANUAL);

        Assert.assertEquals(((CommandStationDriveManualResponse) bidibMessage).getAddress(), 94);
        Assert.assertEquals(((CommandStationDriveManualResponse) bidibMessage).getSpeed(), 0);
    }

    @Test
    public void createMessageCommandStationAccessoryManualResponseFromByteArray() throws ProtocolException {
        byte[] message = { 0x07, 0x01, 0x00, (byte) 0xd4, (byte) 0xe7, 0x02, 0x01, 0x02 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertEquals(bidibMessage.getType(), (byte) BidibLibrary.MSG_CS_ACCESSORY_MANUAL);

        Assert.assertNotNull(((CommandStationAccessoryManualResponse) bidibMessage).getDecoderAddress());
        Assert.assertEquals(((CommandStationAccessoryManualResponse) bidibMessage).getDecoderAddress().getAddress(),
            258);
        Assert.assertEquals(((CommandStationAccessoryManualResponse) bidibMessage).getAspect(), 2);
    }

    @Test
    public void createLcMacroStateResponse1FromByteArray() throws ProtocolException {
        byte[] message = { 0x06, 0x01, 0x00, (byte) 0xd4, (byte) 0xc8, 0x00, (byte) 0xfc };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO_STATE);

        LcMacroStateResponse lcMacroStateResponse = (LcMacroStateResponse) bidibMessage;
        LOGGER.info("lcMacroStateResponse: {}", lcMacroStateResponse);
        Assert.assertEquals(lcMacroStateResponse.getMacroNumber(), 0);

        LcMacroState lcMacroState = lcMacroStateResponse.getMacroState();
        Assert.assertNotNull(lcMacroState);
        LOGGER.info("lcMacroState: {}", lcMacroState);

        Assert.assertEquals(LcMacroState.valueOf(lcMacroState.getType()), LcMacroState.RESTORE);
    }

    @Test
    public void createLcMacroParaResponse1FromByteArray() throws ProtocolException {
        byte[] message =
            { 0x0a, 0x01, 0x00, (byte) 0xd9, (byte) 0xca, 0x00, 0x03, (byte) 0x3f, (byte) 0xbf, (byte) 0x7f,
                (byte) 0xff };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO_PARA);

        // start clock
        LcMacroParaResponse lcMacroParaResponse = (LcMacroParaResponse) bidibMessage;
        LOGGER.info("lcMacroParaResponse: {}", lcMacroParaResponse);
        Assert.assertEquals(lcMacroParaResponse.getMacroNumber(), 0);
        Assert.assertEquals(lcMacroParaResponse.getParameterIndex(), 3);
        Assert.assertNotNull(lcMacroParaResponse.getLcMacroParaValue());
        Assert.assertEquals(lcMacroParaResponse.getLcMacroParaValue().getValue(),
            new byte[] { 63, (byte) 191, 127, (byte) 255 });
    }

    @Test
    public void createStringResponseMessage() throws ProtocolException {
        byte[] message =
            { 0x0B, 0x00, 0x00, (byte) BidibLibrary.MSG_STRING, (byte) 0x00, (byte) 0x00, 0x05, 0x42, 0x69, 0x44, 0x69,
                0x42 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);

        Assert.assertNotNull(bidibMessage);
        Assert.assertTrue(bidibMessage instanceof StringResponse, "Expected a StringResponse message.");
        Assert.assertEquals(bidibMessage.getType(), (byte) BidibLibrary.MSG_STRING);
        StringResponse stringResponse = (StringResponse) bidibMessage;
        Assert.assertNotNull(stringResponse.getStringData());
        StringData stringData = stringResponse.getStringData();

        Assert.assertEquals(stringData.getNamespace(), 0);
        Assert.assertEquals(stringData.getIndex(), 0);
        Assert.assertEquals(stringData.getValue(), "BiDiB");
    }

    @Test
    public void createCommandStationDriveAcknowledgeResponseMessage() throws ProtocolException {
        byte[] message = { 0x06, 0x00, (byte) 0xB8, (byte) 0xE2, (byte) 0x62, (byte) 0x02, 0x01 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertTrue(bidibMessage instanceof CommandStationDriveAcknowledgeResponse,
            "Expected a CommandStationDriveAcknowledgeResponse message.");
        CommandStationDriveAcknowledgeResponse response = (CommandStationDriveAcknowledgeResponse) bidibMessage;
        LOGGER.info("Received address: {}, state: {}", response.getAddress(), response.getState());
    }

    @Test
    public void createBoostDiagnosticResponseMessage() throws ProtocolException {
        byte[] message = { 0x09, 0x00, 0x62, (byte) 0xB2, 0x00, 0x44, 0x01, (byte) 0xA9, 0x02, 0x13 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertTrue(bidibMessage instanceof BoostDiagnosticResponse,
            "Expected a BoostDiagnosticResponse message.");
        BoostDiagnosticResponse response = (BoostDiagnosticResponse) bidibMessage;
        LOGGER.info("Received current: {}, voltage: {}", response.getCurrent(), response.getVoltage());
        Assert.assertEquals(response.getCurrent(), 272);
        Assert.assertEquals(response.getVoltage(), 169);

        message =
            new byte[] { 0x09, 0x00, (byte) 0x4C, (byte) 0xB2, 0x00, (byte) 0x47, 0x01, (byte) 0x8A, 0x02,
                (byte) 0x1C };
        bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertTrue(bidibMessage instanceof BoostDiagnosticResponse,
            "Expected a BoostDiagnosticResponse message.");
        response = (BoostDiagnosticResponse) bidibMessage;
        LOGGER.info("II. Received current: {}, voltage: {}", response.getCurrent(), response.getVoltage());
        Assert.assertEquals(response.getCurrent(), 320);
        Assert.assertEquals(response.getVoltage(), 138);
    }

    // 07 06 01 00 3f 90 46 28 43 fe ....?ÂF(CÃ¾

    @Test
    public void createMessageFeatureFromByteArray() throws ProtocolException {
        byte[] message = { 0x07, 0x06, 0x01, 0x00, 0x3f, (byte) 0x90, 0x46, 0x28, (byte) 0x43 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_FEATURE));
        Feature feature = ((FeatureResponse) bidibMessage).getFeature();
        LOGGER.info("Returned feature: {}", feature);
        Assert.assertEquals(feature, new Feature(FeatureEnum.FEATURE_CTRL_PORT_FLAT_MODEL.getType(), 40));
    }

    // 11.03.2015 06:38:51.935: [Thread-10] - << 09 03 00 14 C2 02 00 0E 10 10 E5 FE
    // 11.03.2015 06:38:51.950: [Thread-10] - << 09 03 00 15 C2 02 01 0E 10 10 57 FE
    // 11.03.2015 06:38:51.966: [Thread-10] - << 09 03 00 16 C2 02 02 0E 10 10 98 FE
    // 11.03.2015 06:38:51.997: [Thread-10] - << 09 03 00 17 C2 02 03 0E 10 10 2A FE

    @Test(expectedExceptions = { ProtocolException.class })
    public void createMessageUnknownFromByteArray() throws ProtocolException {
        // invalid LcConfig message: length of data is too short
        byte[] message = { 0x09, 0x03, 0x00, 0x14, (byte) 0xC2, 0x02, 0x00, (byte) 0x0E, 0x10, 0x10, (byte) 0xE5 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_FEATURE));
    }

    @Test
    public void createRcPlusAckTid() throws ProtocolException {
        // IN --- : MSG_CS_RCPLUS_ACK 0A 00 30 E8 02 0A 0B 0C 0D 0D 03
        byte[] message = { 0x0A, 0x00, 0x30, (byte) 0xE8, 0x02, 0x0A, 0x0B, 0x0C, 0x0D, 0x0D, 0x03 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_CS_RCPLUS_ACK));

        CommandStationRcPlusAcknowledgeResponse response = (CommandStationRcPlusAcknowledgeResponse) bidibMessage;
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodesAck.RC_TID);
        Assert.assertNotNull(response.getTid());
        Assert.assertEquals(response.getTid(), new TidData(new DecoderUniqueIdData(0x0D0C0B0A, 13), 3));
    }

    @Test
    public void createRcPlusAckPingP0() throws ProtocolException {
        // IN --- : MSG_CS_RCPLUS_ACK 05 00 32 E8 04 01
        byte[] message = { 0x05, 0x00, 0x32, (byte) 0xE8, 0x04, 0x01 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_CS_RCPLUS_ACK));

        CommandStationRcPlusAcknowledgeResponse response = (CommandStationRcPlusAcknowledgeResponse) bidibMessage;
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodesAck.RC_PING_ONCE_P0);

        Assert.assertNull(response.getTid());
    }

    @Test
    public void createRcPlusAckPingP1() throws ProtocolException {
        // IN --- : MSG_CS_RCPLUS_ACK 05 00 32 E8 05 01
        byte[] message = { 0x05, 0x00, 0x32, (byte) 0xE8, 0x05, 0x01 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_CS_RCPLUS_ACK));

        CommandStationRcPlusAcknowledgeResponse response = (CommandStationRcPlusAcknowledgeResponse) bidibMessage;
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodesAck.RC_PING_ONCE_P1);

        Assert.assertNull(response.getTid());
    }

    @Test
    public void createRcPlusAckBind() throws ProtocolException {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message = { 0x0A, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_RES, 0x04, 0x0A, 0x0B, 0x0C, 0x0D, 13 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_CS_RCPLUS_ACK));

        CommandStationRcPlusAcknowledgeResponse response = (CommandStationRcPlusAcknowledgeResponse) bidibMessage;
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodesAck.RC_BIND);
        // the TID is not available
        Assert.assertNull(response.getTid());

        RcPlusDecoderAnswerData answerData = response.getRcPlusDecoderAnswer();
        Assert.assertNotNull(answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.SENT_BUT_NO_RESPONSE);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));
    }

    @Test
    public void createRcPlusAckFindP0() throws ProtocolException {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message = { 0x0A, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_FIND_P0, 0x04, 0x0A, 0x0B, 0x0C, 0x0D, 13 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_CS_RCPLUS_ACK));

        CommandStationRcPlusAcknowledgeResponse response = (CommandStationRcPlusAcknowledgeResponse) bidibMessage;
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodesAck.RC_FIND_P0);
        // the TID is not available
        Assert.assertNull(response.getTid());

        RcPlusDecoderAnswerData answerData = response.getRcPlusDecoderAnswer();
        Assert.assertNotNull(answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.SENT_BUT_NO_RESPONSE);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));
    }

    @Test
    public void createRcPlusAckFindP1() throws ProtocolException {
        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message = { 0x0A, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_FIND_P1, 0x04, 0x0A, 0x0B, 0x0C, 0x0D, 13 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_CS_RCPLUS_ACK));

        CommandStationRcPlusAcknowledgeResponse response = (CommandStationRcPlusAcknowledgeResponse) bidibMessage;
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodesAck.RC_FIND_P1);
        // the TID is not available
        Assert.assertNull(response.getTid());

        RcPlusDecoderAnswerData answerData = response.getRcPlusDecoderAnswer();
        Assert.assertNotNull(answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.SENT_BUT_NO_RESPONSE);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));
    }

    @Test
    public void createFeedbackRcPlusNewLoco() throws ProtocolException {
        // IN --- : MSG_BM_RCPLUS 0A 00 49 AB 0F 0D D5 16 D4 FF 97
        byte[] message =
            { 0x0A, 0x00, 0x49, (byte) BidibLibrary.MSG_BM_RCPLUS, 0x0F, 0x0D, (byte) 0xD5, 0x16, (byte) 0xD4,
                (byte) 0xFF, (byte) 0x97 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_BM_RCPLUS));

        FeedbackRcPlusResponse response = (FeedbackRcPlusResponse) bidibMessage;
        Assert.assertEquals(response.getDetectorNumber(), 0x0F);
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodeBm.RC_PONG_NEW_LOCO_P1);

        DecoderUniqueIdData uniqueId = response.getRcPlusUniqueId();
        Assert.assertNotNull(uniqueId);
        Assert.assertEquals(uniqueId, new DecoderUniqueIdData(0xFFD416D5L, 0x97 /* ESU */));
    }

    @Test
    public void createFeedbackRcPlusBindAccepted() throws ProtocolException {
        // IN --- : MSG_BM_RCPLUS 0A 00 E1 AB 0F 00 D5 16 D4 FF 97 01 01
        byte[] message =
            { 0x0C, 0x00, (byte) 0xE1, (byte) BidibLibrary.MSG_BM_RCPLUS, 0x0F, 0x00, (byte) 0xD5, 0x16, (byte) 0xD4,
                (byte) 0xFF, (byte) 0x97, 0x01, 0x01 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_BM_RCPLUS));

        FeedbackRcPlusResponse response = (FeedbackRcPlusResponse) bidibMessage;
        Assert.assertEquals(response.getDetectorNumber(), 0x0F);
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodeBm.RC_BIND_ACCEPTED_LOCO);

        RcPlusFeedbackBindData rcPlusFeedbackBind = response.getRcPlusFeedbackBind();
        Assert.assertNotNull(rcPlusFeedbackBind);
        DecoderUniqueIdData uniqueId = rcPlusFeedbackBind.getUniqueId();
        Assert.assertNotNull(uniqueId);
        Assert.assertEquals(uniqueId, new DecoderUniqueIdData(0xFFD416D5L, 0x97 /* ESU */));

        AddressData addressData = rcPlusFeedbackBind.getAddress();
        Assert.assertNotNull(addressData);
        Assert.assertEquals(addressData.getType(), AddressTypeEnum.LOCOMOTIVE_FORWARD);
        Assert.assertEquals(addressData.getAddress(), 0x0101);
    }

    @Test
    public void createFeedbackRcPlusBindAcceptedAccessory() throws ProtocolException {
        // IN --- : MSG_BM_RCPLUS 0A 00 30 AB 05 02 10 00 01 91 3E
        byte[] message =
            { 0x0A, 0x00, 0x30, (byte) BidibLibrary.MSG_BM_RCPLUS, 0x05, 0x02, 0x10, 0x00, 0x01, (byte) 0x91,
                (byte) 0x3E };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);

        LOGGER.info("prepared message: {}", bidibMessage);

        Assert.assertEquals(bidibMessage.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_BM_RCPLUS));

        FeedbackRcPlusResponse response = (FeedbackRcPlusResponse) bidibMessage;
        Assert.assertEquals(response.getDetectorNumber(), 0x05);
        Assert.assertEquals(response.getOpCode(), RcPlusOpCodeBm.RC_BIND_ACCEPTED_ACCESSORY);
    }
}
