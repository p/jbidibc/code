package org.bidib.jbidibc.net;

public interface NetMessageHandler {

    /**
     * Receive messages from the configured port
     * 
     * @param packet
     *            the received data
     */
    void receive(DataPacket packet);

    /**
     * Send data to the port.
     * 
     * @param port
     *            the port
     * @param bytes
     *            the data
     */
    void send(final NetBidibPort port, byte[] bytes);

    /**
     * Accept new client.
     * 
     * @param remoteHost
     *            the host
     */
    void acceptClient(String remoteHost);

    /**
     * Cleanup the connection of the client.
     * 
     * @param remoteHost
     */
    void cleanup(String remoteHost);
}
