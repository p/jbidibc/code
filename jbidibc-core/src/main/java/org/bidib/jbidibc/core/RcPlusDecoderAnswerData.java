package org.bidib.jbidibc.core;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcPlusDecoderAnswerData {
    private static final Logger LOGGER = LoggerFactory.getLogger(RcPlusDecoderAnswerData.class);

    private final RcPlusAcknowledge acknState;

    private final DecoderUniqueIdData uniqueIdData;

    public RcPlusDecoderAnswerData(RcPlusAcknowledge acknState, DecoderUniqueIdData uniqueIdData) {
        this.acknState = acknState;
        this.uniqueIdData = uniqueIdData;
    }

    /**
     * @return the rcPlus unique id
     */
    public DecoderUniqueIdData getUniqueId() {
        return uniqueIdData;
    }

    /**
     * @return the rcPlus acknowledge state
     */
    public RcPlusAcknowledge getAcknState() {
        return acknState;
    }

    /**
     * Create a RcPlusDecoderAnswerData instance from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset to start
     * @return the RcPlusDecoderAnswerData instance
     */
    public static RcPlusDecoderAnswerData fromByteArray(byte[] data, int offset) {
        if (data.length < (offset + 5)) {
            LOGGER.warn("The size of the provided data does not meet the expected length.");
            throw new IllegalArgumentException(
                "The size of the provided data does not meet the expected length (>=5). Provided length: "
                    + data.length);
        }

        RcPlusAcknowledge acknState = RcPlusAcknowledge.valueOf(data[offset]);
        DecoderUniqueIdData uniqueIdData = DecoderUniqueIdData.fromByteArray(data, offset + 1);

        RcPlusDecoderAnswerData answerData = new RcPlusDecoderAnswerData(acknState, uniqueIdData);

        LOGGER.trace("Return answerData: {}", answerData);
        return answerData;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
