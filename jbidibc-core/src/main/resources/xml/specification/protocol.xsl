<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns="http://www.bidib.org/schema/bidib/2.0/protocol" 
	xmlns:bidib="http://www.bidib.org/schema/bidib"
	xmlns:cmn="http://www.bidib.org/schema/bidib/2.0/common"
	
	exclude-result-prefixes="bidib" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<xsl:template name="decimalToHex">
	    <xsl:param name="dec"/>
	    <xsl:if test="$dec > 0">
	        <xsl:call-template name="decimalToHex">
	            <xsl:with-param name="dec" select="floor($dec div 16)"/>
	        </xsl:call-template>
	        <xsl:value-of select="substring('0123456789ABCDEF', (($dec mod 16) + 1), 1)"/>
	    </xsl:if>
	</xsl:template>
	
	<xsl:template match="bidib:BiDiB">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="bidib:Protocol">
		<Protocol xmlns="http://www.bidib.org/schema/bidib/2.0/protocol" xmlns:cmn="http://www.bidib.org/schema/bidib/2.0/common" >
		<xsl:copy-of select="@*"/> <!-- this copies all its attributes -->
		<xsl:apply-templates />
		</Protocol>
	</xsl:template>
	
	<xsl:template match="bidib:Protocol/bidib:MessageTypes">
		<xsl:element name="{node-name(.)}" namespace="http://www.bidib.org/schema/bidib/2.0/protocol">
		<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
	<xsl:template match="bidib:Protocol/bidib:FeatureCodes">
		<xsl:element name="{node-name(.)}" namespace="http://www.bidib.org/schema/bidib/2.0/protocol">
		<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="bidib:Protocol/bidib:MessageTypes/*">
		<xsl:element name="{node-name(.)}" namespace="http://www.bidib.org/schema/bidib/2.0/protocol">
			<xsl:copy-of select="node() | @*"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="bidib:Protocol/bidib:FeatureCodes/*">
		<xsl:element name="{node-name(.)}" namespace="http://www.bidib.org/schema/bidib/2.0/protocol">
		<xsl:copy-of select="@*"/> <!-- this copies all its attributes -->
		<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="bidib:Protocol/bidib:FeatureCodes/bidib:FeatureCode/bidib:Documentation">
		<xsl:element name="cmn:{node-name(.)}" >
			<xsl:copy-of select="node() | @*"/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
