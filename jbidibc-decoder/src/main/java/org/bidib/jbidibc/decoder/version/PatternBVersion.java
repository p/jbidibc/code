package org.bidib.jbidibc.decoder.version;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternBVersion extends AbstractDecoderVersion {

    public static final String PATTERN_B = "[0-9]+";

    private Integer minorVersion;

    private Integer microVersion;

    public PatternBVersion(String pattern) {
        super(pattern);
    }

    @Override
    public Integer getMinorVersion() {
        return minorVersion;
    }

    @Override
    public Integer getMicroVersion() {
        return microVersion;
    }

    protected void setMinorVersion(Integer minorVersion) {
        this.minorVersion = minorVersion;
    }

    protected void setMicroVersion(Integer microVersion) {
        this.microVersion = microVersion;
    }

    public void parse(String versionString) {

        Matcher m = Pattern.compile(PATTERN_B).matcher(versionString);

        int index = 0;

        while (m.find()) {
            String text = m.group(0);

            switch (index) {
                case 0:
                    setMajorVersion(Integer.parseInt(text));
                    break;
                case 1:
                    setMinorVersion(Integer.parseInt(text));
                    break;
                default:
                    setMicroVersion(Integer.parseInt(text));
                    break;
            }

            index++;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((microVersion == null) ? 0 : microVersion.hashCode());
        result = prime * result + ((minorVersion == null) ? 0 : minorVersion.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PatternBVersion other = (PatternBVersion) obj;
        if (microVersion == null) {
            if (other.microVersion != null)
                return false;
        }
        else if (!microVersion.equals(other.microVersion))
            return false;
        if (minorVersion == null) {
            if (other.minorVersion != null)
                return false;
        }
        else if (!minorVersion.equals(other.minorVersion))
            return false;
        return true;
    }

}
