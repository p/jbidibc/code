package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddressTypeEnumTest {

    @Test
    public void addressTypeBackwardTest() {

        byte type = ByteUtils.getLowByte(0x80);
        // AddressTypeEnum addressTypeEnum = AddressTypeEnum.valueOf(type);
        AddressTypeEnum addressTypeEnum = AddressTypeEnum.valueOf((byte) ((type & 0xC0) >> 6));
        Assert.assertEquals(addressTypeEnum, AddressTypeEnum.LOCOMOTIVE_BACKWARD);
    }

    @Test
    public void addressTypeForwardTest() {

        byte type = ByteUtils.getLowByte(0x00);
        // AddressTypeEnum addressTypeEnum = AddressTypeEnum.valueOf(type);
        AddressTypeEnum addressTypeEnum = AddressTypeEnum.valueOf((byte) ((type & 0xC0) >> 6));
        Assert.assertEquals(addressTypeEnum, AddressTypeEnum.LOCOMOTIVE_FORWARD);
    }
}
