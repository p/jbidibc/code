package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum RcPlusOpCodeBm implements BidibEnum {
    RC_BIND_ACCEPTED_LOCO(BidibLibrary.RC_BIND_ACCEPTED_LOCO, "bind-accepted-loco"), RC_BIND_ACCEPTED_ACCESSORY(
        BidibLibrary.RC_BIND_ACCEPTED_ACCESSORY, "bind-accepted-accessory"), RC_PING_COLLISION_P0(
        BidibLibrary.RC_PING_COLLISION_P0, "ping-collision-p0"), RC_PING_COLLISION_P1(
        BidibLibrary.RC_PING_COLLISION_P1, "ping-collision-p1"), RC_FIND_COLLISION_P0(
        BidibLibrary.RC_FIND_COLLISION_P0, "find-collision-p0"), RC_FIND_COLLISION_P1(
        BidibLibrary.RC_FIND_COLLISION_P1, "find-collision-p1"), RC_PONG_OKAY_LOCO_P0(
        BidibLibrary.RC_PONG_OKAY_LOCO_P0, "pong-okay-loco-p0"), RC_PONG_OKAY_LOCO_P1(
        BidibLibrary.RC_PONG_OKAY_LOCO_P1, "pong-okay-loco-p1"), RC_PONG_OKAY_ACCESSORY_P0(
        BidibLibrary.RC_PONG_OKAY_ACCESSORY_P0, "pong-okay-accessory-p0"), RC_PONG_OKAY_ACCESSORY_P1(
        BidibLibrary.RC_PONG_OKAY_ACCESSORY_P1, "pong-okay-accessory-p1"), RC_PONG_NEW_LOCO_P0(
        BidibLibrary.RC_PONG_NEW_LOCO_P0, "pong-new-loco-p0"), RC_PONG_NEW_LOCO_P1(BidibLibrary.RC_PONG_NEW_LOCO_P1,
        "pong-new-loco-p1"), RC_PONG_NEW_ACCESSORY_P0(BidibLibrary.RC_PONG_NEW_ACCESSORY_P0, "pong-new-accessory-p0"), RC_PONG_NEW_ACCESSORY_P1(
        BidibLibrary.RC_PONG_NEW_ACCESSORY_P1, "pong-new-accessory-p1");

    private final String key;

    private final byte type;

    private RcPlusOpCodeBm(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create a rcplus opcode for BM.
     * 
     * @param type
     *            numeric value of the rcplus opcode
     * 
     * @return RcPlusOpCodeBm
     */
    public static RcPlusOpCodeBm valueOf(byte type) {
        RcPlusOpCodeBm result = null;

        for (RcPlusOpCodeBm e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a rcplus opcode bm");
        }
        return result;
    }

}
