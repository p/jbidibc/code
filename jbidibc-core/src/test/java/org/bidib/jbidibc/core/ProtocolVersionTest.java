package org.bidib.jbidibc.core;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ProtocolVersionTest {

    @Test
    public void compareTo() {
        ProtocolVersion pv1 = new ProtocolVersion(1, 1);
        ProtocolVersion pv2 = new ProtocolVersion(1, 1);
        ProtocolVersion pv3 = new ProtocolVersion(1, 0);
        ProtocolVersion pv4 = new ProtocolVersion(2, 0);

        Assert.assertEquals(pv1.compareTo(pv2), 0);
        Assert.assertEquals(pv1.compareTo(pv3), 1);
        Assert.assertEquals(pv1.compareTo(pv4), -1);
        // pv4 is greater than pv3
        Assert.assertEquals(pv4.compareTo(pv3), 1);
    }

    @Test
    public void equals() {
        ProtocolVersion pv1 = new ProtocolVersion(1, 1);
        ProtocolVersion pv2 = new ProtocolVersion(1, 1);
        ProtocolVersion pv3 = new ProtocolVersion(1, 0);
        ProtocolVersion pv4 = new ProtocolVersion(2, 0);

        Assert.assertTrue(pv1.equals(pv2));
        Assert.assertFalse(pv1.equals(pv3));
        Assert.assertFalse(pv1.equals(pv4));
        Assert.assertFalse(pv3.equals(pv4));
    }

    @Test
    public void testCompareVersion() {
        ProtocolVersion protocolVersion1 = new ProtocolVersion(1, 0);
        ProtocolVersion protocolVersion2 = new ProtocolVersion(1, 1);

        Assert.assertTrue(protocolVersion1.isLowerThan(protocolVersion2));
        Assert.assertTrue(protocolVersion2.isHigherThan(protocolVersion1));
    }
}
