package org.bidib.jbidibc.core.schema;

import java.util.List;

import org.bidib.jbidibc.core.schema.decodervendor.VendorType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DecoderVendorFactoryTest {

    @Test
    public void getDecoderVendors() {

        List<VendorType> vendors = DecoderVendorFactory.getDecoderVendors();
        Assert.assertNotNull(vendors);

        Assert.assertEquals(vendors.size(), 11);
    }

    @Test
    public void getDecoderVendorById() {

        List<VendorType> vendors = DecoderVendorFactory.getDecoderVendors();
        Assert.assertNotNull(vendors);

        Assert.assertEquals(vendors.size(), 11);

        String vendorName = DecoderVendorFactory.getDecoderVendorName(vendors, 62);
        Assert.assertNotNull(vendorName);
        Assert.assertEquals(vendorName, "Tams Elektronik GmbH");

        vendorName = DecoderVendorFactory.getDecoderVendorName(vendors, 251);
        Assert.assertNotNull(vendorName);
        Assert.assertEquals(vendorName, "FichtelBahn");

        vendorName = DecoderVendorFactory.getDecoderVendorName(vendors, 999);
        Assert.assertNull(vendorName);
    }
}
