package org.bidib.jbidibc.simulation.net;

import java.io.File;
import java.util.Collections;

import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.net.NetBidib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NetBidibUdpTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibUdpTest.class);

    private static final String SIMULATION_CONFIGURATION_LOCATION = "/simulation/simulation-demo.xml";

    private SimulationNetBidib simulationNetBidib;

    @BeforeClass
    public void prepare() {
        LOGGER.info("Prepare the simulator.");

        // create the test instance
        simulationNetBidib = new SimulationNetBidib("udp");
        // simulationNetBidib.start(SIMULATION_CONFIGURATION_LOCATION);

        try {
            File simulationConfigurationLocation =
                new File(getClass().getResource(SIMULATION_CONFIGURATION_LOCATION).toURI());
            simulationNetBidib.start(simulationConfigurationLocation);
        }
        catch (Exception ex) {
            LOGGER.warn("Get simulationConfiguration file failed", ex);
        }

        if (simulationNetBidib.isStarted()) {
            LOGGER.info("Prepared and started the simulator.");
        }
        else {
            LOGGER.warn("The simulator was not started!");
            throw new IllegalArgumentException("The simulator was not started!");
        }
    }

    @AfterClass
    public void destroy() {
        LOGGER.info("Stop the simulation.");

        if (simulationNetBidib != null) {
            simulationNetBidib.stop();

            simulationNetBidib = null;
        }

        LOGGER.info("Stopped the simulation.");
    }

    @Test
    public void openUdp() throws PortNotFoundException, PortNotOpenedException, ProtocolException {

        NetBidib netBidib = (NetBidib) NetBidib.createInstance();
        Assert.assertNotNull(netBidib);

        netBidib.setResponseTimeout(BidibInterface.DEFAULT_TIMEOUT * 100);
        try {
            netBidib.open("udp:localhost:" + NetBidib.BIDIB_UDP_PORT_NUMBER, new ConnectionListener() {

                @Override
                public void opened(String port) {
                    LOGGER.info("The port was opened: {}", port);
                }

                @Override
                public void closed(String port) {
                    LOGGER.info("The port was closed: {}", port);
                }

                @Override
                public void status(String messageKey) {
                    // no implementation
                }

            }, Collections.<NodeListener> emptySet(), Collections.<MessageListener> emptySet(),
                Collections.<TransferListener> emptySet(), null);

            Assert.assertNotNull(netBidib.getRootNode());
            Assert.assertEquals(netBidib.getRootNode().getNodeMagic(), Integer.valueOf(BidibLibrary.BIDIB_SYS_MAGIC));

            long uniqueId = netBidib.getRootNode().getUniqueId();

            Assert.assertEquals(uniqueId, 0xD2000D680064EAL);

            // and again
            uniqueId = netBidib.getRootNode().getUniqueId(true);

            Assert.assertEquals(uniqueId, 0xD2000D680064EAL);

            netBidib.getRootNode().getNextFeature();
            netBidib.getRootNode().getNextFeature();
            netBidib.getRootNode().getNextFeature();
            netBidib.getRootNode().getNextFeature();

        }
        finally {
            netBidib.close();
        }
    }
}
