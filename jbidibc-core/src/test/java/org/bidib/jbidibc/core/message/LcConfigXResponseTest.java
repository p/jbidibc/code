package org.bidib.jbidibc.core.message;

import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LcConfigXResponseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LcConfigXResponseTest.class);

    @Test
    public void getLcConfigX() throws ProtocolException {

        byte[] message =
            { 0x0e, 0x02, 0x00, (byte) 0xAB, (byte) 0xc6, 0x00, 0x10, 0x01, (byte) 0xFF, 0x02, 0x00, 0x03, 0x06, 0x04,
                0x05 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 16);
        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.SWITCHPORT);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        // byte val1 = lcConfigX.getPortConfigValue(Byte.valueOf((byte) 1));
        // Assert.assertEquals(val1 & 0xFF, 0xFF);

        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 1)) & 0xFF, 0xFF);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 2)) & 0xFF, 0x00);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 3)) & 0xFF, 0x06);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 4)) & 0xFF, 0x05);

        Assert
            .assertEquals(((BytePortConfigValue) portConfig.get(Byte.valueOf((byte) 1))).getValue().byteValue() & 0xFF,
                0xFF);
        Assert
            .assertEquals(((BytePortConfigValue) portConfig.get(Byte.valueOf((byte) 2))).getValue().byteValue() & 0xFF,
                0x00);
        Assert
            .assertEquals(((BytePortConfigValue) portConfig.get(Byte.valueOf((byte) 3))).getValue().byteValue() & 0xFF,
                0x06);
        Assert
            .assertEquals(((BytePortConfigValue) portConfig.get(Byte.valueOf((byte) 4))).getValue().byteValue() & 0xFF,
                0x05);
    }

    @Test
    public void getLcConfigXWithInt() throws ProtocolException {

        byte[] message =
            { 0x11, 0x02, 0x00, (byte) 0xAB, (byte) 0xc6, 0x00, 0x10, 0x01, (byte) 0xFF, 0x02, 0x00, 0x03, 0x06,
                (byte) 0x82, (byte) 0xBA, (byte) 0xDC, (byte) 0xFE, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 16);
        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.SWITCHPORT);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 1)) & 0xFF, 0xFF);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 2)) & 0xFF, 0x00);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 3)) & 0xFF, 0x06);
        Assert.assertEquals((long) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 0x82)) & 0xFFFFFFFFL, 0xFEDCBAL);
    }

    @Test
    public void getLcConfigXWithInt32AndContinue() throws ProtocolException {

        // 0x82 is assumed to be a real 32-bit value

        byte[] message =
            { 0x12, 0x02, 0x00, (byte) 0xAB, (byte) 0xc6, 0x00, 0x10, 0x01, (byte) 0xFF, 0x02, 0x00, 0x03, 0x06,
                (byte) 0x82, (byte) 0xBA, (byte) 0xDC, (byte) 0xFE, (byte) 0x01, (byte) 0xFF };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 16);
        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.SWITCHPORT);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 1)) & 0xFF, 0xFF);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 2)) & 0xFF, 0x00);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 3)) & 0xFF, 0x06);
        Assert.assertEquals((long) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 0x82)) & 0xFFFFFFFF, 0x01FEDCBAL);

        Assert.assertNotNull(portConfig.containsKey(Byte.valueOf(BidibLibrary.BIDIB_PCFG_CONTINUE)));

        Assert.assertTrue(lcConfigX.isContinueDetected());
    }

    @Test
    public void getLcConfigXWithRGBAndContinue() throws ProtocolException {

        byte[] message =
            { 0x11, 0x02, 0x00, (byte) 0xAB, (byte) 0xc6, 0x01, 0x10, 0x01, (byte) 0xFF, 0x02, 0x00, 0x03, 0x06,
                BidibLibrary.BIDIB_PCFG_RGB, (byte) 0xBA, (byte) 0xDC, (byte) 0xFE, BidibLibrary.BIDIB_PCFG_CONTINUE };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 16);
        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.LIGHTPORT);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 1)) & 0xFF, 0xFF);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 2)) & 0xFF, 0x00);
        Assert.assertEquals((byte) lcConfigX.getPortConfigValue(Byte.valueOf((byte) 3)) & 0xFF, 0x06);
        Assert
            .assertEquals((int) lcConfigX.getPortConfigValue(Byte.valueOf(BidibLibrary.BIDIB_PCFG_RGB)) & 0xFFFFFFFF,
                0xBADCFE);

        Assert.assertNotNull(portConfig.containsKey(Byte.valueOf(BidibLibrary.BIDIB_PCFG_CONTINUE)));

        Assert.assertTrue(lcConfigX.isContinueDetected());
    }

    @Test
    public void getLcConfigXReconfigLightPort() throws ProtocolException {

        // !!! with ACT_TYPE !!!
        byte[] message =
            { 0x0A, 0x02, 0x00, (byte) 0xAB, (byte) 0xc6, 0x01/* PORT_TYPE */, 0x02, BidibLibrary.BIDIB_PCFG_RECONFIG,
                0x01/* ACT_TYPE */, 0x03, (byte) 0x80 };

        // BIDIB_PCFG_RECONFIG (byte) 0x81

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.LIGHTPORT);
        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 2);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert
            .assertEquals((int) lcConfigX.getPortConfigValue(Byte.valueOf(BidibLibrary.BIDIB_PCFG_RECONFIG)), 0x800301);
    }

    @Test
    public void getLcConfigXReconfigServoPort() throws ProtocolException {

        // !!! with ACT_TYPE !!!
        byte[] message =
            { 0x0A, 0x02, 0x00, (byte) 0xAB, (byte) 0xc6, 0x02/* OUTPUT_TYPE */, 0x02, BidibLibrary.BIDIB_PCFG_RECONFIG,
                0x02/* ACT_TYPE */, 0x04, (byte) 0x00 };

        // BIDIB_PCFG_RECONFIG (byte) 0x81

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.SERVOPORT);
        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 2);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert
            .assertEquals((int) lcConfigX.getPortConfigValue(Byte.valueOf(BidibLibrary.BIDIB_PCFG_RECONFIG)), 0x000402);
    }

    @Test
    public void getLcConfigXReconfigServoPortOneControl() throws ProtocolException {
        // 30.01.2015 20:25:49.667: receive MSG_LC_CONFIGX[[2],num=16,type=198,data=[0, 0, 7, 20, 8, 250, 9, 4]] : 0C 02
        // 00 10 C6 00 00 07 14 08 FA 09 04

        byte[] message =
            { 0x0C, 0x02, 0x00, 0x10, (byte) 0xc6, 0x02/* OUTPUT_TYPE */, 0x00, 07, 0x14, 0x08, (byte) 0xFA, 0x09,
                0x04 };

        // BIDIB_PCFG_RECONFIG (byte) 0x81

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.type), LcOutputType.SERVOPORT);
        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.type), 0);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);
    }

    // TODO add tests for flat model

    @Test
    public void getLcConfigXReconfigLedIo() throws ProtocolException {
        // 04.12.2016 16:45:26.652: [receiveQueueWorker] - << MSG_LC_CONFIGX[[4],num=19,type=198,data=[0, 0, 129, 1, 2,
        // 0, 4, 2, 3, 6, 2, 0, 1, 254]] : 12 04 00 13 C6 00 00 81 01 02 00 04 02 03 06 02 00 01 FE

        byte[] message =
            { 0x12, 0x04, 0x00, 0x13, (byte) 0xc6, 0x00/* OUTPUT_TYPE */, 0x00, BidibLibrary.BIDIB_PCFG_RECONFIG,
                0x01/* ACT_TYPE */, 0x02, 0x00, 0x04, 0x02, 0x03, 0x06, 0x02, 0x00, 0x01, (byte) 0xFE };

        // BIDIB_PCFG_RECONFIG (byte) 0x81

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.flat), LcOutputType.SWITCHPORT);
        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.flat), 0);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert
            .assertEquals((int) lcConfigX.getPortConfigValue(Byte.valueOf(BidibLibrary.BIDIB_PCFG_RECONFIG)), 0x000201);
    }

    @Test
    public void getLcConfigXReconfigLedIoPort24() throws ProtocolException {
        // 28.12.2018 10:18:17.595: [receiveQueueWorker] - << MSG_LC_CONFIGX[[1],num=48,type=198,data=[24, 0, 129, 15,
        // 0, 128]] : 0A 01 00 30 C6 18 00 81 0F 00 80

        byte[] message =
            { 0x0A, 0x01, 0x00, 0x30, (byte) 0xC6, 0x18/* OUTPUT_TYPE */, 0x00, BidibLibrary.BIDIB_PCFG_RECONFIG,
                0x0F/* ACT_TYPE */, 0x00, (byte) 0x80 };

        // BIDIB_PCFG_RECONFIG (byte) 0x81

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_CONFIGX);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) bidibMessage;
        LOGGER.info("lcConfigXResponse: {}", lcConfigXResponse);

        LcConfigX lcConfigX = lcConfigXResponse.getLcConfigX();
        Assert.assertNotNull(lcConfigX);
        LOGGER.info("lcConfigX: {}", lcConfigX);

        Assert.assertEquals(lcConfigX.getOutputType(PortModelEnum.flat), LcOutputType.SWITCHPORT);
        Assert.assertEquals(lcConfigX.getOutputNumber(PortModelEnum.flat), 24);

        Map<Byte, PortConfigValue<?>> portConfig = lcConfigX.getPortConfig();
        Assert.assertNotNull(portConfig);

        Assert
            .assertEquals((int) lcConfigX.getPortConfigValue(Byte.valueOf(BidibLibrary.BIDIB_PCFG_RECONFIG)), 0x80000F);
    }

}
