package org.bidib.jbidibc.decoder.schema.nmramanufacturers;

import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturersList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NMRAManufacturersFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NMRAManufacturersFactoryTest.class);

    @Test
    public void loadNMRAManufacturersTest() {
        String filePath = "/manufacturers/Manufacturers.xml";

        filePath = NMRAManufacturersFactoryTest.class.getResource(filePath).getPath();
        LOGGER.info("Load nmraManufacturers from path: {}", filePath);

        NMRAManufacturersFactory factory = new NMRAManufacturersFactory();
        ManufacturersList nmraManufacturers = factory.loadNmraManufacturers(filePath);

        Assert.assertNotNull(nmraManufacturers);
    }
}
