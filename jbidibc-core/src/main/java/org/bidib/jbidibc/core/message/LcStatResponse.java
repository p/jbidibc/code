package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcStatResponse extends BidibMessage {

    public static final Integer TYPE = BidibLibrary.MSG_LC_STAT;

    LcStatResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 3) {
            throw new ProtocolException("No valid MSG_LC_STAT received.");
        }
    }

    public LcStatResponse(byte[] addr, int num, BidibPort bidibPort, byte portStatus) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_LC_STAT, ByteUtils.concat(bidibPort.getValues(), new byte[] { portStatus }));
    }

    public String getName() {
        return "MSG_LC_STAT";
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port type
     */
    public LcOutputType getPortType(PortModelEnum portModel) {
        return PortModelEnum.getPortType(portModel, getData()[0]);
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port number
     */
    public int getPortNumber(PortModelEnum portModel) {
        return PortModelEnum.getPortNumber(portModel, getData()[0], getData()[1]);
    }

    public BidibPort getBidibPort() {
        return BidibPort.prepareBidibPort(getData()[0], getData()[1]);
    }

    public int getPortStatus() {
        return ByteUtils.getInt(getData()[2]);
    }
}
