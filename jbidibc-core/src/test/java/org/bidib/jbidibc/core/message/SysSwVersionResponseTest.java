package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SysSwVersionResponseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SysSwVersionResponseTest.class);

    @Test
    public void getVersion() throws ProtocolException {
        SysSwVersionResponse swVersionResponse =
            new SysSwVersionResponse(new byte[] { 2 }, 1, SysSwVersionResponse.TYPE, new byte[] { (byte) 255, 2, 1 });
        SoftwareVersion softwareVersion = swVersionResponse.getVersion();

        Assert.assertNotNull(softwareVersion);
        LOGGER.info("Returned softwareVersion: {}", softwareVersion);
        Assert.assertEquals(softwareVersion.toString(), "1.02.255");
        Assert.assertNull(softwareVersion.getChildrenVersions());
    }

    @Test
    public void getVersionWithChildren() throws ProtocolException {
        SysSwVersionResponse swVersionResponse =
            new SysSwVersionResponse(new byte[] { 2 }, 1, SysSwVersionResponse.TYPE, new byte[] { (byte) 255, 2, 1, 3, 4, 5, 6, 7, 8 });
        SoftwareVersion softwareVersion = swVersionResponse.getVersion();

        Assert.assertNotNull(softwareVersion);
        LOGGER.info("Returned softwareVersion: {}", softwareVersion);
        Assert.assertEquals(softwareVersion.toString(), "1.02.255, 5.04.03, 8.07.06");
        Assert.assertNotNull(softwareVersion.getChildrenVersions());
        Assert.assertEquals(softwareVersion.getChildrenVersions().size(), 2);
    }
}
