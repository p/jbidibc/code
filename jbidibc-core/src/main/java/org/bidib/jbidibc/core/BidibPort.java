package org.bidib.jbidibc.core;

import java.util.Arrays;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibPort.class);

    private byte[] values;

    public BidibPort(byte[] values) {
        if (values == null || values.length != 2) {
            LOGGER.warn("BidibPort accepts only 2 byte values. Provided values: {}", values);
            throw new IllegalArgumentException("BidibPort accepts only 2 byte values. Provided values: " + values);
        }

        this.values = values;
    }

    public byte[] getValues() {
        return values;
    }

    public byte getLowValue() {
        return values[0];
    }

    public byte getHighValue() {
        return values[1];
    }

    public static BidibPort prepareBidibPort(int portNumber) {
        return new BidibPort(new byte[] { ByteUtils.getLowByte(portNumber), ByteUtils.getHighByte(portNumber) });
    }

    public static BidibPort prepareBidibPort(byte lowByte, byte highByte) {
        return new BidibPort(new byte[] { lowByte, highByte });
    }

    public static BidibPort prepareBidibPort(PortModelEnum portModelEnum, LcOutputType lcOutputType, int port) {
        BidibPort bidibPort = null;
        if (portModelEnum == PortModelEnum.type) {
            bidibPort = new BidibPort(new byte[] { lcOutputType.getType(), ByteUtils.getLowByte(port) });
        }
        else {
            bidibPort = new BidibPort(new byte[] { ByteUtils.getLowByte(port), ByteUtils.getHighByte(port) });
        }
        return bidibPort;
    }

    public static BidibPort prepareSystemFunctionBidibPort(PortModelEnum portModel, byte lowByte, byte highByte) {
        BidibPort bidibPort = null;
        if (portModel == PortModelEnum.type) {
            bidibPort = BidibPort.prepareBidibPort((byte) 0, lowByte);
        }
        else {
            bidibPort = BidibPort.prepareBidibPort(lowByte, highByte);
        }
        return bidibPort;
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port type
     */
    public LcOutputType getPortType(PortModelEnum portModel) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat_extended:
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(values[0]);
                break;
        }
        return outputType;
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port number
     */
    public int getPortNumber(PortModelEnum portModel) {
        int portNumber = -1;
        switch (portModel) {
            case flat_extended:
                portNumber = ByteUtils.getInt(values[0], values[1] & 0x7F);
                break;
            case flat:
                portNumber = ByteUtils.getInt(values[0]);
                break;
            default:
                portNumber = ByteUtils.getInt(values[1], 0x7F);
                break;
        }
        return portNumber;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BidibPort) {
            BidibPort other = (BidibPort) obj;
            if (!Arrays.equals(values, other.values)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BidibPort[low=");
        sb.append(ByteUtils.getInt(values[0])).append(",high=").append(ByteUtils.getInt(values[1])).append("]");
        return sb.toString();
    }
}
