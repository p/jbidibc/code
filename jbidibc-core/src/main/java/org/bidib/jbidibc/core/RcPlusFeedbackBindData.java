package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RcPlusFeedbackBindData {

    private static final Logger LOGGER = LoggerFactory.getLogger(RcPlusFeedbackBindData.class);

    private final DecoderUniqueIdData uniqueIdData;

    private final AddressData address;

    public RcPlusFeedbackBindData(DecoderUniqueIdData uniqueIdData, AddressData address) {
        this.uniqueIdData = uniqueIdData;
        this.address = address;
    }

    /**
     * @return the rcPlus unique id
     */
    public DecoderUniqueIdData getUniqueId() {
        return uniqueIdData;
    }

    public AddressData getAddress() {
        return address;
    }

    /**
     * Create a RcPlusDecoderAnswerData instance from the provided byte array.
     * 
     * @param data
     *            the byte array
     * @param offset
     *            the offset to start
     * @return the RcPlusDecoderAnswerData instance
     */
    public static RcPlusFeedbackBindData fromByteArray(byte[] data, int offset) {
        // TODO change this to be 7 after the GBM returns the address
        if (data.length < (offset + 5)) {
            LOGGER.warn("The size of the provided data does not meet the expected length.");
            throw new IllegalArgumentException(
                "The size of the provided data does not meet the expected length (=5). Provided length: " + data.length
                    + ", offset: " + offset);
        }

        DecoderUniqueIdData uniqueIdData = DecoderUniqueIdData.fromByteArray(data, offset);
        AddressData address = null;
        if (data.length > (offset + 5)) {
            address = AddressData.fromByteArray(data, offset + 5);
        }

        RcPlusFeedbackBindData answerData = new RcPlusFeedbackBindData(uniqueIdData, address);

        LOGGER.info("Return RcPlusBindAcceptedData: {}", answerData);
        return answerData;
    }

    public void writeToStream(ByteArrayOutputStream out) {
        getUniqueId().writeToStream(out);
        getAddress().writeToStream(out);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
