package org.bidib.jbidibc.net.serialovertcp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bidib.jbidibc.core.AbstractBidib;
import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.net.NetBidibPort;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.bidib.jbidibc.net.NetMessageReceiver;
import org.bidib.jbidibc.serial.SerialMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetBidib extends AbstractBidib {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidib.class);

    public static final int BIDIB_UDP_PORT_NUMBER = 62875;

    private NetBidibPort port;

    private NetMessageHandler netMessageHandler;

    private Thread portWorker;

    private String connectedPortName;

    private InetAddress address;

    private int portNumber;

    private String protocol;

    private ConnectionListener connectionListener;

    protected NetBidib() {
        LOGGER.info("Create new instance of plain tcp NetBidib.");
    }

    @Override
    protected BidibMessageProcessor createMessageReceiver(NodeRegistry nodeFactory) {
        // the serial over tcp needs CRC check
        return new NetMessageReceiver(nodeFactory, true);
    }

    private MessageReceiver getNetMessageReceiver() {
        return (MessageReceiver) getMessageReceiver();
    }

    @Override
    public void setConnectionListener(final ConnectionListener connectionListener) {

        this.connectionListener = new ConnectionListener() {

            private boolean active;

            @Override
            public void status(String messageKey) {
                connectionListener.status(messageKey);
            }

            @Override
            public void opened(String port) {
                connectionListener.opened(port);
            }

            @Override
            public void closed(String port) {

                if (active) {
                    connectionListener.closed(port);
                    return;
                }
                try {
                    active = true;
                    close();
                }
                finally {
                    active = false;
                }
            }
        };

        super.setConnectionListener(this.connectionListener);
    }

    /**
     * Get a new initialized instance of NetBidib.
     *
     * @return the instance of NetBidib
     */
    public static BidibInterface createInstance() {
        LOGGER.info("Create new instance of NetBidib.");

        NetBidib instance = new NetBidib();
        instance.initialize();

        return instance;
    }

    @Override
    public void open(
        String portName, ConnectionListener connectionListener, Set<NodeListener> nodeListeners,
        Set<MessageListener> messageListeners, Set<TransferListener> transferListeners, final Context context)
        throws PortNotFoundException, PortNotOpenedException {

        LOGGER.info("Open port: {}", portName);

        setConnectionListener(connectionListener);

        // register the listeners
        registerListeners(nodeListeners, messageListeners, transferListeners);

        if (port == null) {
            LOGGER.info("Open port with name: {}", portName);
            if (portName == null || portName.trim().isEmpty()) {
                throw new PortNotFoundException("");
            }

            if (portName.indexOf(":") < 0) {
                portName += ":" + NetBidib.BIDIB_UDP_PORT_NUMBER;
                LOGGER.info("Added portnumber to portName: {}", portName);
            }

            try {
                // close();
                port = internalOpen(portName, context);
                connectedPortName = portName;

                LOGGER.info("Port is opened, send the magic. The connected port is: {}", connectedPortName);
                sendMagic();
            }
            catch (Exception ex) {
                LOGGER.warn("Open port and send magic failed.", ex);

                throw new PortNotOpenedException(portName, PortNotOpenedException.UNKNOWN);
            }
            LOGGER.info("Open port passed: {}", portName);
        }
        else {
            LOGGER.warn("Port is already opened.");
        }
    }

    private NetBidibPort internalOpen(String portName, final Context context) throws IOException {
        LOGGER.info("Internal open port: {}", portName);

        String[] hostAndPort = portName.split(":");

        if (hostAndPort.length > 2) {
            // protocol provided
            protocol = hostAndPort[0];
            address = InetAddress.getByName(hostAndPort[1]);
            portNumber = Integer.parseInt(hostAndPort[2]);
        }
        else {
            protocol = "tcp";
            address = InetAddress.getByName(hostAndPort[0]);
            portNumber = Integer.parseInt(hostAndPort[1]);
        }

        LOGGER.info("Configured address: {}, portNumber: {}, protocol: {}", address, portNumber, protocol);

        final BidibMessageProcessor messageReceiver = getMessageReceiver();

        if (context != null) {
            Boolean ignoreWrongMessageNumber = context.get("ignoreWrongMessageNumber", Boolean.class, Boolean.FALSE);
            messageReceiver.setIgnoreWrongMessageNumber(ignoreWrongMessageNumber);
        }

        // enable the message receiver before the event listener is added
        getNetMessageReceiver().enable();

        netMessageHandler = new DefaultNetMessageHandler(messageReceiver, address, portNumber, connectionListener);

        // open the port
        NetBidibPort netBidibPort = null;

        netBidibPort = new NetBidibPlainTcpPort(address, portNumber, netMessageHandler);

        LOGGER.info("Prepare and start the port worker for netBidibPort: {}", netBidibPort);

        startReceiverAndQueues(messageReceiver, context);

        portWorker = new Thread(netBidibPort);
        portWorker.start();

        return netBidibPort;
    }

    @Override
    public boolean isOpened() {
        return port != null;
    }

    @Override
    public void close() {
        LOGGER.info("Close the port.");

        if (port != null) {
            LOGGER.info("Stop the port.");
            port.stop();

            if (portWorker != null) {
                synchronized (portWorker) {
                    try {
                        portWorker.join(5000L);
                    }
                    catch (InterruptedException ex) {
                        LOGGER.warn("Wait for termination of port worker failed.", ex);
                    }
                    portWorker = null;
                }
            }

            port = null;
        }

        stopReceiverAndQueues(null);

        if (getConnectionListener() != null) {
            getConnectionListener().closed(connectedPortName);
        }

        // clear the connectedPortName
        connectedPortName = null;
        cleanupAfterClose(null);

        LOGGER.info("Close the port finished.");
    }

    private ByteArrayOutputStream output = new ByteArrayOutputStream(100);

    @Override
    public void sendData(ByteArrayOutputStream data) {
        if (port != null) {
            // byte[] bytes = data.toByteArray();
            LOGGER.info("Send message to net message handler: {}, port: {}", ByteUtils.bytesToHex(data), port);

            // forward the message to the netMessageReceiver
            try {

                SerialMessageEncoder.encodeMessage(data, output);

                LOGGER.info("Send, after encoding: {}", ByteUtils.bytesToHex(output));

                netMessageHandler.send(port, output.toByteArray());
            }
            catch (Exception ex) {
                LOGGER.warn("Forward message to send with netMessageReceiver failed.", ex);
                throw new RuntimeException("Forward message to send with netMessageReceiver failed.", ex);
            }
            finally {
                output.reset();
            }
        }
        else {
            LOGGER.warn("Send not possible, the port is closed.");
        }
    }

    /**
     * Get the magic from the root node
     * 
     * @return the magic provided by the root node
     * @throws ProtocolException
     */
    private int sendMagic() throws ProtocolException {
        BidibNode rootNode = getRootNode();

        // Ignore the first exception ...
        int magic = -1;
        try {
            magic = rootNode.getMagic(15000);
        }
        catch (Exception e) {
            magic = rootNode.getMagic(15000);
        }
        LOGGER.info("The node returned magic: {}", magic);
        return magic;
    }

    @Override
    public void setResponseTimeout(int timeout) {
        LOGGER.info("Set the response timeout to: {}", timeout);
        super.setResponseTimeout(timeout);
    }

    @Override
    public List<String> getPortIdentifiers() {
        return Collections.emptyList();
    }
}
