package org.bidib.jbidibc.simulation.net;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.net.NetBidib;
import org.bidib.jbidibc.net.NetBidibPort;
import org.bidib.jbidibc.net.NetBidibServerTcpPort;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.bidib.jbidibc.net.udp.NetBidibServerUdpPort;
import org.bidib.jbidibc.simulation.SimulationInterface;
import org.bidib.jbidibc.simulation.SimulatorRegistry;
import org.bidib.jbidibc.simulation.net.udp.SimulationUdpNetMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationNetBidib extends NetBidib implements SimulationInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationNetBidib.class);

    private Thread portWorker;

    private NetMessageHandler netSimulationMessageHandler;

    private NetBidibPort netBidibPortSimulator;

    private SimulatorRegistry simulatorRegistry;

    private int sessionKey;

    private int sequence;

    private AtomicBoolean isStarted = new AtomicBoolean();

    private final String protocol;

    public SimulationNetBidib(String protocol) {
        this.protocol = protocol;
    }

    protected int getSessionKey() {
        return sessionKey;
    }

    protected int getSequence() {
        return sequence;
    }

    protected SimulatorRegistry getSimulatorRegistry() {
        return simulatorRegistry;
    }

    public boolean isStarted() {
        return isStarted.get();
    }

    @Override
    public void start(File simulationConfigurationFile) {
        LOGGER.info("Start the simulator.");

        // create the simulation components
        try {

            LOGGER.info("Create simulator for protocol: {}", protocol);

            String[] splited = protocol.split(":");

            // create the message receiver that handles incoming commands from the host and forward the commands to the
            // simulators
            netSimulationMessageHandler = createSimulationMessageHandler(simulationConfigurationFile, splited[0]);

            // open the port that simulates the interface device
            if ("tcp".equalsIgnoreCase(splited[0])) {
                LOGGER.info("Create a NetBidibTcpPort with the portnumber: {}", NetBidib.BIDIB_UDP_PORT_NUMBER);
                netBidibPortSimulator =
                    new NetBidibServerTcpPort(NetBidib.BIDIB_UDP_PORT_NUMBER, null, netSimulationMessageHandler);
            }
            else {
                LOGGER.info("Create a NetBidibUdpPort with the portnumber: {}", NetBidib.BIDIB_UDP_PORT_NUMBER);
                netBidibPortSimulator =
                    new NetBidibServerUdpPort(NetBidib.BIDIB_UDP_PORT_NUMBER, netSimulationMessageHandler);
            }
            LOGGER.info("Prepare and start the port worker for netBidibPortSimulator: {}", netBidibPortSimulator);

            portWorker = new Thread(netBidibPortSimulator);
            portWorker.start();

            isStarted.set(true);
        }
        catch (Exception ex) {
            LOGGER.warn("Start the simulator failed.", ex);
        }
    }

    protected NetMessageHandler createSimulationMessageHandler(File simulationConfigurationFile, String protocolType) {
        // load the SimulatorRegistry with the simulation configuration
        simulatorRegistry = SimulatorRegistry.getInstance();
        simulatorRegistry.removeAll();

        // create the message handler that delegates the incoming messages to the message receiver that has a
        // simulator node configured
        final SimulationMessageReceiver simulationMessageReceiver = new SimulationMessageReceiver() {
            @Override
            public void publishResponse(ByteArrayOutputStream output) throws ProtocolException {

                // Publish the responses to the host
                LOGGER.info(
                    "Publish the response. Prepare message to send to host using netSimulationMessageHandler: {}",
                    netSimulationMessageHandler);
                try {
                    // send to handler
                    netSimulationMessageHandler.send(netBidibPortSimulator, output.toByteArray());
                }
                catch (Exception ex) {
                    LOGGER.warn("Process messages failed.", ex);
                }
            }

            @Override
            public void removeNodeListener(NodeListener nodeListener) {

            }

            @Override
            public void setIgnoreWrongMessageNumber(boolean ignoreWrongMessageNumber) {

            }
        };
        simulationMessageReceiver.setSimulatorRegistry(simulatorRegistry);

        LOGGER.info("Load simulationConfigurationFile from: {}", simulationConfigurationFile);

        InputStream simulationConfiguration = null;
        try {
            simulationConfiguration = new FileInputStream(simulationConfigurationFile);
            simulatorRegistry.loadSimulationConfiguration(simulationConfiguration, simulationMessageReceiver);
        }
        catch (IOException ex) {
            LOGGER.warn("Open simulationConfiguration stream failed", ex);
        }
        finally {
            if (simulationConfiguration != null) {
                try {
                    simulationConfiguration.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close simulationConfiguration stream failed", ex);
                }
            }
        }

        LOGGER.info("Simulator registry is loaded.");
        NetMessageHandler netMessageHandler = null;
        if ("tcp".equalsIgnoreCase(protocolType)) {
            netMessageHandler = new SimulationNetMessageHandler(simulationMessageReceiver);
        }
        else {
            netMessageHandler = new SimulationUdpNetMessageHandler(simulationMessageReceiver);
        }
        LOGGER.info("Created the simulation netMessageHandler: {}", netMessageHandler);
        return netMessageHandler;
    }

    @Override
    public void stop() {
        LOGGER.info("Stop the simulator.");

        if (netBidibPortSimulator != null) {
            LOGGER.info("Stop the port.");
            netBidibPortSimulator.stop();

            if (portWorker != null) {
                synchronized (portWorker) {
                    try {
                        portWorker.join(5000L);
                    }
                    catch (InterruptedException ex) {
                        LOGGER.warn("Wait for termination of port worker failed.", ex);
                    }
                    portWorker = null;
                }
            }

            isStarted.set(false);
            netBidibPortSimulator = null;
        }

        LOGGER.info("Stop the simulator finished.");
    }

    @Override
    public List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new LinkedList<>();
        portIdentifiers.add("mock");
        return portIdentifiers;
    }
}
