package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;

public enum LcMappingPortType implements BidibEnum {
    // @formatter:off
    SWITCHPORT(BidibLibrary.BIDIB_PORTTYPE_SWITCH), 
    LIGHTPORT(BidibLibrary.BIDIB_PORTTYPE_LIGHT), 
    SERVOPORT(BidibLibrary.BIDIB_PORTTYPE_SERVO), 
    SOUNDPORT(BidibLibrary.BIDIB_PORTTYPE_SOUND), 
    MOTORPORT(BidibLibrary.BIDIB_PORTTYPE_MOTOR), 
    ANALOGPORT(BidibLibrary.BIDIB_PORTTYPE_ANALOGOUT), 
    BACKLIGHTPORT(BidibLibrary.BIDIB_PORTTYPE_BACKLIGHT), 
    SWITCHPAIRPORT(BidibLibrary.BIDIB_PORTTYPE_SWITCHPAIR), 
    INPUTPORT(BidibLibrary.BIDIB_PORTTYPE_INPUT);
    // @formatter:on

    private final byte type;

    LcMappingPortType(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a LcMappingPortType type.
     * 
     * @param type
     *            numeric value of the lc mapping port type
     * 
     * @return LcMappingPortType
     */
    public static LcMappingPortType valueOf(byte type) {
        LcMappingPortType result = null;

        for (LcMappingPortType e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a LcMappingPortType type");
        }
        return result;
    }

}
