package org.bidib.jbidibc.decoder.version;

public abstract class AbstractDecoderVersion implements DecoderVersion {

    private Integer majorVersion;

    protected String pattern;

    protected AbstractDecoderVersion(String pattern) {
        this.pattern = pattern;
    }

    /**
     * @param majorVersion
     *            the major version to set
     */
    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    @Override
    public Integer getMajorVersion() {
        return majorVersion;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((majorVersion == null) ? 0 : majorVersion.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractDecoderVersion other = (AbstractDecoderVersion) obj;
        if (majorVersion == null) {
            if (other.majorVersion != null)
                return false;
        }
        else if (!majorVersion.equals(other.majorVersion))
            return false;
        return true;
    }

}
