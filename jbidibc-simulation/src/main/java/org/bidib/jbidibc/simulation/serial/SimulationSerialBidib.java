package org.bidib.jbidibc.simulation.serial;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bidib.jbidibc.core.AbstractBidib;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.serial.SerialMessageEncoder;
import org.bidib.jbidibc.simulation.SimulationInterface;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.jbidibc.simulation.SimulatorRegistry;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.jbidibc.simulation.net.SimulationMessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationSerialBidib extends AbstractBidib implements SimulationInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationSerialBidib.class);

    private SimulationBidibMessageProcessor simulationMessageReceiver;

    private String requestedPortName;

    private RequestFactory requestFactory;

    /*
     * This method is called from open().
     */
    @Override
    protected BidibMessageProcessor createMessageReceiver(NodeRegistry nodeFactory) {
        return new MessageReceiver(nodeFactory, new BidibResponseFactory(), true) {

            @Override
            public void receive(ByteArrayOutputStream data) {
                LOGGER.warn("receive is not implemented!");
            }
        };
    }

    @Override
    public void start(File simulationConfigurationFile) {
        LOGGER.info("Start the simulator, simulationConfigurationFile: {}", simulationConfigurationFile);

        // load the SimulatorRegistry with the simulation configuration
        SimulatorRegistry.getInstance().removeAll();

        // InputStream simulationConfiguration = getClass().getResourceAsStream(simulationConfigurationLocation);
        InputStream simulationConfiguration = null;
        try {
            simulationConfiguration = new FileInputStream(simulationConfigurationFile);
            SimulatorRegistry
                .getInstance().loadSimulationConfiguration(simulationConfiguration,
                    (SimulationBidibMessageProcessor) this.getSimulationMessageReceiver());
        }
        catch (InvalidConfigurationException ex) {
            LOGGER.warn("Create simulationConfiguration failed.", ex);

            throw ex;
        }
        catch (IOException ex) {
            LOGGER.warn("Open simulationConfiguration stream failed.", ex);
        }
        finally {
            if (simulationConfiguration != null) {
                try {
                    simulationConfiguration.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close simulationConfiguration stream failed", ex);
                }
            }
        }
        requestFactory = new RequestFactory();
    }

    private SimulationBidibMessageProcessor getSimulationMessageReceiver() {

        if (simulationMessageReceiver == null) {
            simulationMessageReceiver = new SimulationMessageReceiver() {
                @Override
                public void publishResponse(ByteArrayOutputStream output) throws ProtocolException {
                    LOGGER
                        .info("Publish the response from the SimulationMessageReceiver to the messageReceiver: {}",
                            ByteUtils.bytesToHex(output));

                    getMessageReceiver().processMessages(output);
                }

                @Override
                public void removeNodeListener(NodeListener nodeListener) {
                    getMessageReceiver().removeNodeListener(nodeListener);
                }

                @Override
                public void setIgnoreWrongMessageNumber(boolean ignoreWrongMessageNumber) {
                    LOGGER.info("Set ignoreWrongMessageNumber: {}", ignoreWrongMessageNumber);
                    getMessageReceiver().setIgnoreWrongMessageNumber(ignoreWrongMessageNumber);
                }

            };
        }
        return simulationMessageReceiver;
    }

    @Override
    public void stop() {
        LOGGER.info("Stop was called. Stop all simulators.");

        try {
            SimulatorRegistry.getInstance().removeAll();
        }
        catch (Exception ex) {
            LOGGER.warn("Remove all simulators from the registry failed.", ex);
        }
    }

    private ByteArrayOutputStream output = new ByteArrayOutputStream(100);

    @Override
    public void sendData(ByteArrayOutputStream data) {

        try {
            byte[] bytes = data.toByteArray();
            LOGGER.info("Send is called with bytes: {}", ByteUtils.bytesToHex(bytes));

            // encode the message
            SerialMessageEncoder.encodeMessage(data, output);

            List<BidibCommand> bidibMessages = requestFactory.create(output.toByteArray());
            for (BidibCommand bidibMessage : bidibMessages) {
                byte[] address = ((BidibMessage) bidibMessage).getAddr();

                String nodeAddress = NodeUtils.formatAddress(address);

                SimulatorNode simulator = SimulatorRegistry.getInstance().getSimulator(nodeAddress);
                if (simulator == null) {
                    LOGGER.warn("No simulator found for nodeAddress: {}", nodeAddress);
                }
                else {
                    simulator.processRequest(bidibMessage);
                }
                LOGGER.debug("Forwarded message to simulator: {}", bidibMessage);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Process request failed.", ex);
        }
        finally {
            output.reset();
        }
    }

    @Override
    public void open(
        String portName, ConnectionListener connectionListener, Set<NodeListener> nodeListeners,
        Set<MessageListener> messageListeners, Set<TransferListener> transferListeners, final Context context)
        throws PortNotFoundException, PortNotOpenedException {

        LOGGER.info("Open the serial simulation.");
        setConnectionListener(connectionListener);
        registerListeners(nodeListeners, messageListeners, transferListeners);

        final BidibMessageProcessor messageReceiver = getSimulationMessageReceiver();

        if (context != null) {
            Boolean ignoreWrongMessageNumber =
                context.get("ignoreWrongReceiveMessageNumber", Boolean.class, Boolean.FALSE);
            messageReceiver.setIgnoreWrongMessageNumber(ignoreWrongMessageNumber);
        }

        startReceiverAndQueues(messageReceiver, context);

        if (getConnectionListener() != null) {
            LOGGER.info("Notify that the port was opened: {}", requestedPortName);
            getConnectionListener().opened(requestedPortName);
        }

        LOGGER.info("The port was opened internally, get the magic.");
        try {
            int magic = sendResetAndMagic();
            LOGGER.info("The root node returned the magic: {}", ByteUtils.magicToHex(magic));
        }
        catch (ProtocolException ex) {
            LOGGER.error("Send reset and magic to root node failed.", ex);
        }

    }

    @Override
    public boolean isOpened() {
        return isOpened;
    }

    private boolean isOpened;

    @Override
    public void close() {
        LOGGER.info("Close the serial simulation.");
        isOpened = false;

        if (getConnectionListener() != null) {
            getConnectionListener().closed(requestedPortName);
        }

        requestedPortName = null;

        // if (simulationInterface != null) {
        // simulationInterface.stop();
        // }

        releaseRootNode();

        // release the simulation message receiver
        simulationMessageReceiver = null;

        cleanupAfterClose(null);

        // remove all simulators from the simulation registry
        SimulatorRegistry.getInstance().removeAll();

        super.stopReceiverAndQueues(simulationMessageReceiver);
    }

    @Override
    public List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new LinkedList<>();
        portIdentifiers.add("mock");
        return portIdentifiers;
    }

    /**
     * Get the magic from the root node
     * 
     * @return the magic provided by the root node
     * @throws ProtocolException
     */
    private int sendResetAndMagic() throws ProtocolException {
        RootNode rootNode = getRootNode();

        LOGGER.info("Send reset to the rootNode.");
        rootNode.reset();

        // try {
        // LOGGER.info("Wait 500ms before send the magic request.");
        // Thread.sleep(500);
        // }
        // catch (InterruptedException ex) {
        // LOGGER.warn("Wait before send the magic request failed.", ex);
        // }

        LOGGER.info("Enable the message receiver before send magic.");
        // enable the message receiver
        getSimulationMessageReceiver().enable();

        try {
            LOGGER.info("Wait 500ms before send the magic request.");
            Thread.sleep(500);
        }
        catch (InterruptedException ex) {
            LOGGER.warn("Wait before send the magic request failed.", ex);
        }
        LOGGER.info("Purge the receive buffer because we will send the MAGIC request!");
        getSimulationMessageReceiver().purgeReceivedDataInBuffer();

        int magic = rootNode.getMagic(1500);

        LOGGER.debug("The node returned magic: {}", magic);
        return magic;
    }
}
