package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;

import org.bidib.jbidibc.core.utils.ByteUtils;

public class DecoderIdAddressData {

    private final byte[] didAddress;

    public DecoderIdAddressData(byte[] didAddress) {
        if (didAddress == null || didAddress.length != 5) {
            throw new IllegalArgumentException("The didAddress is mandatory and must have a length of 5 bytes.");
        }
        this.didAddress = didAddress;
    }

    public DecoderIdAddressData(Long decoderMun, Integer decoderMid) {
        if (decoderMun == null || decoderMid == null) {
            throw new IllegalArgumentException("The decoderMun and decoderMid is mandatory!");
        }

        // get the 4 byte from decoderMun and the byte from decoderMid
        long decMun = decoderMun.longValue();
        int decMid = decoderMid.intValue();

        byte[] munBytes = ByteUtils.toDWORD(decMun);

        this.didAddress = ByteUtils.append(munBytes, ByteUtils.getLowByte(decMid));
    }

    /**
     * @return the didAddress
     */
    public byte[] getDidAddress() {
        return didAddress;
    }

    public String toString() {
        return getClass().getSimpleName() + "[didAddress=" + ByteUtils.didToHex(didAddress) + "]";
    }

    public void writeToStream(ByteArrayOutputStream out) {
        // write address
        for (int index = 0; index < 5; index++) {
            out.write(didAddress[index]);
        }
    }

    public byte getManufacturedId() {
        return didAddress[4];
    }
}
