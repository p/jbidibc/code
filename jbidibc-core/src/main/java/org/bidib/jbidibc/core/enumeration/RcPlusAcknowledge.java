package org.bidib.jbidibc.core.enumeration;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public enum RcPlusAcknowledge implements BidibEnum {
    // @formatter:off
    NOT_ACKNOWLEDGED(0), ACKNOWLEDGED(1), DELAYED(2), SENT_BUT_NO_RESPONSE(4), RECEIVED_DECODER_ACKN(5);
    // @formatter:on

    private final byte type;

    RcPlusAcknowledge(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a RcPlus acknowledge.
     * 
     * @param type
     *            numeric value of the RcPlus acknowledge
     * 
     * @return RcPlusAcknowledge
     */
    public static RcPlusAcknowledge valueOf(byte type) {
        RcPlusAcknowledge result = null;

        for (RcPlusAcknowledge e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a RcPlus acknowledge");
        }
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
