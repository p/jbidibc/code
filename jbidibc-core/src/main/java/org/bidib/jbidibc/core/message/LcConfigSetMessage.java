package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.LcConfig;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcConfigSetMessage extends BidibCommandMessage {

    /**
     * This constructor only supports the type port model!
     * 
     * @param config
     *            the config
     */
    public LcConfigSetMessage(PortModelEnum portModel, LcConfig config) {
        super(0, BidibLibrary.MSG_LC_CONFIG_SET, new byte[] { config.getOutputType(portModel).getType(),
            ByteUtils.getLowByte(config.getOutputNumber(portModel)), ByteUtils.getLowByte(config.getValue1()),
            ByteUtils.getLowByte(config.getValue2()), ByteUtils.getLowByte(config.getValue3()),
            ByteUtils.getLowByte(config.getValue4()) });
    }

    public LcConfigSetMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_LC_CONFIG_SET";
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port type
     */
    public LcOutputType getPortType(PortModelEnum portModel) {
        return PortModelEnum.getPortType(portModel, getData()[0]);
    }

    /**
     * @param portModel
     *            the port model to use
     * @return the port number
     */
    public int getPortNumber(PortModelEnum portModel) {
        return PortModelEnum.getPortNumber(portModel, getData()[0], getData()[1]);
    }

    public byte[] getPortConfig() {
        byte[] config = new byte[] { getData()[2], getData()[3], getData()[4], getData()[5] };
        return config;
    }

    public byte getValue1() {
        return getData()[2];
    }

    public byte getValue2() {
        return getData()[3];
    }

    public byte getValue3() {
        return getData()[4];
    }

    public byte getValue4() {
        return getData()[5];
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return new Integer[] { LcConfigResponse.TYPE, LcNotAvailableResponse.TYPE };
    }
}
