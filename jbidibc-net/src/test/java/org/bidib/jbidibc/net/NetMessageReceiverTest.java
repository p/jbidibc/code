package org.bidib.jbidibc.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.mockito.Mockito;
import org.testng.annotations.Test;

public class NetMessageReceiverTest {

    private ByteArrayOutputStream pack(byte[] data) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        output.write(data);
        return output;
    }

    @Test
    public void receiveMessageTest() throws IOException {
        byte[] address = new byte[] { 0 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        // prepare the object to test
        NetMessageReceiver receiver = new NetMessageReceiver(nodeFactory, false);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data =
            new byte[] { 0x05, 0x00, 0x01, (byte) 0x86, (byte) 0x02, (byte) 0x00, 0x06, 0x00, 0x01, (byte) 0xc1, 0x00,
                0x01, 0x02 };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(1));

        receiver.receive(pack(data));

        // verify that error was called once
        Mockito.verify(messageListener, Mockito.times(1)).error(address, 2, address);

        Mockito.verify(messageListener, Mockito.times(1)).lcNa(Mockito.eq(address), Mockito.any(), Mockito.eq(2));
    }
}
