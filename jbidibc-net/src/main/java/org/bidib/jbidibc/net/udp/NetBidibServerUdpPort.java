package org.bidib.jbidibc.net.udp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.net.NetBidibPacketUtils;
import org.bidib.jbidibc.net.NetBidibPort;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetBidibServerUdpPort implements NetBidibPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibServerUdpPort.class);

    private final DatagramSocket datagramSocket;

    private final NetMessageHandler messageReceiver;

    private AtomicBoolean runEnabled = new AtomicBoolean();

    private int currentSessionKey;

    private List<BidibUdpNetAddress> knownBidibHosts = new LinkedList<>();

    /**
     * Creates a new instance of {@code NetBidibUdpPort} for clients.
     * 
     * @param portNumber
     *            the port number
     * @param messageReceiver
     *            the message receiver
     * @throws SocketException
     */
    public NetBidibServerUdpPort(NetMessageHandler messageReceiver) throws SocketException {
        this(null, messageReceiver);
    }

    /**
     * Creates a new instance of {@code NetBidibUdpPort}. For client port the port number is omitted and set to
     * {@code null}.
     * 
     * @param portNumber
     *            the port number
     * @param messageReceiver
     *            the message receiver
     * @throws SocketException
     */
    public NetBidibServerUdpPort(Integer portNumber, NetMessageHandler messageReceiver) throws SocketException {
        if (portNumber != null) {
            this.datagramSocket = new DatagramSocket(portNumber);
            LOGGER.info("Created new datagram socket on portNumber: {}", portNumber);
        }
        else {
            this.datagramSocket = new DatagramSocket();
            LOGGER.info("Created new datagram socket without portNumber");
        }
        this.messageReceiver = messageReceiver;
    }

    private ByteArrayOutputStream receiveBuffer = new ByteArrayOutputStream(100);

    private static final int UDP_PAKET_HEADER_LEN = 4;

    @Override
    public void run() {
        LOGGER.info("Start the port.");
        runEnabled.set(true);

        byte[] receiveData = new byte[1024];
        try {
            while (runEnabled.get()) {
                // wait for client sending data
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                LOGGER.debug("Wait to receive a datagram packet.");

                datagramSocket.receive(receivePacket);

                // forward processing to handler
                if (messageReceiver != null) {
                    LOGGER.info("Received a datagram packet. Forward packet to messageReveiver: {}, len: {}, data: {}",
                        messageReceiver, receivePacket.getLength(),
                        ByteUtils.bytesToHex(receivePacket.getData(), receivePacket.getLength()));

                    InetAddress address = receivePacket.getAddress();
                    int portNumber = receivePacket.getPort();

                    receiveBuffer.write(receivePacket.getData(), UDP_PAKET_HEADER_LEN,
                        receivePacket.getLength() - UDP_PAKET_HEADER_LEN);

                    // problem here is the UDP packet header
                    try {
                        BidibUdpNetAddress current = new BidibUdpNetAddress(address, portNumber);
                        if (!knownBidibHosts.contains(current)) {
                            // generate a session key and support listen only hosts ...
                            currentSessionKey++;
                            current.setSessionKey(currentSessionKey);

                            LOGGER.warn("Adding new known Bidib host: {}", current);
                            knownBidibHosts.add(current);
                        }

                        NetBidibPacketUtils.parsePackets(receiveBuffer, messageReceiver, address, portNumber);
                    }
                    catch (Exception pex) {
                        LOGGER.warn("Receive message failed.", pex);
                    }

                    // do not clear the receiveBuffer here because this is managed from within parsePackets
                }
                else {
                    LOGGER.warn("No message receiver configured, received packet: {}", receivePacket);
                }
            }
        }
        catch (IOException ex) {
            if (runEnabled.get()) {
                LOGGER.warn("--- Interrupt NetBidibPort-run", ex);
            }
            else {
                LOGGER.info("The NetBidibPort worker is terminating.");
            }
        }

        LOGGER.info("Receiver thread has finished.");
    }

    public void stop() {
        LOGGER.info("Stop the datagram packet receiver.");
        runEnabled.set(false);

        datagramSocket.close();
    }

    private ByteArrayOutputStream sendDataBuffer = new ByteArrayOutputStream(100);

    /**
     * Send the data to the host.
     * 
     * @param sendData
     *            the data to send
     * @param address
     *            the receiving address of the host
     * @param portNumber
     *            the receiving port number of the host
     * @throws IOException
     */
    public void send(byte[] sendData, InetAddress address, int portNumber) throws IOException {
        // if (LOGGER.isTraceEnabled()) {
        LOGGER.info("Send data to socket, port: {}, bytes: {}", portNumber, ByteUtils.bytesToHex(sendData));
        // }

        // send the message to every known host
        for (BidibUdpNetAddress host : knownBidibHosts) {
            // bos.reset();
            // bos.write(sendData);
            // add the UDP packet wrapper ...
            sendDataBuffer.write(ByteUtils.getLowByte(host.getSessionKey()));
            sendDataBuffer.write(ByteUtils.getHighByte(host.getSessionKey()));
            sendDataBuffer.write(ByteUtils.getLowByte(host.getSequence()));
            sendDataBuffer.write(ByteUtils.getHighByte(host.getSequence()));
            sendDataBuffer.write(sendData);

            // LOGGER.info("Send message to address: {}, port: {}", host.getAddress(), host.getPortNumber());
            LOGGER.info("Send data to address: {}, portNumber: {}, sendData: {}", host.getAddress(),
                host.getPortNumber(), ByteUtils.bytesToHex(sendDataBuffer));

            // send the data to the host and free the buffer content
            sendBuffer(sendDataBuffer, host.getAddress(), host.getPortNumber());

            // increment sequence counter for host
            host.nextSequence();
        }
    }

    private void sendBuffer(final ByteArrayOutputStream sendData, InetAddress address, int portNumber)
        throws IOException {
        try {
            // sendDataBuffer.write(sendData);
            DatagramPacket sendPacket =
                new DatagramPacket(sendData.toByteArray(), sendData.size(), address, portNumber);
            datagramSocket.send(sendPacket);
        }
        finally {
            sendData.reset();
        }
    }
}
