package org.bidib.jbidibc.com0com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class BidibPortProviderTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibPortProviderTest.class);

    /**
     * Prepare the system:
     * 
     * <pre>
     * command> install - -
     *        CNCA0 PortName=-
     *        CNCB0 PortName=-
     * command> change CNCB0 ExclusiveMode=yes
     *        CNCA0 PortName=-
     *        CNCB0 PortName=-
     * change CNCB0 PortName=-,ExclusiveMode=yes
     * Restarted CNCB0 com0com\port \Device\com0com20
     * command> change CNCA0 PlugInMode=yes
     *        CNCA0 PortName=-
     * change CNCA0 PortName=-,PlugInMode=yes
     * Restarted CNCA0 com0com\port \Device\com0com10
     *        CNCB0 PortName=-,ExclusiveMode=yes
     * command> change CNCA0 PortName=COM13
     *        CNCA0 PortName=-,PlugInMode=yes
     * change CNCA0 PortName=COM13,PlugInMode=yes 
     * Restarted CNCA0 com0com\port \Device\com0com10
     *        CNCB0 PortName=-,ExclusiveMode=yes       
     * command>
     * </pre>
     * 
     * After this configuration the port {@code CNCA0} is visible as {@code COM13} to the user applications. The proxy
     * port for the bidib-proxy is {@code CNCB0}.
     */
    @Test(enabled = false)
    public void start() {

        BidibPortProvider provider = new BidibPortProvider();

        try {
            // the proxy port is the port that is opened by the com0com route
            // provider.start("COM24", "CNCB0");

            // connect to the bidib interface running on port COM7
            // and connect to the proxy-port on CNCB0
            provider.start("COM7", "CNCB0");

            try {
                Thread.sleep(300000);
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Thread was interrupted", ex);
            }
        }
        finally {
            provider.stop();
        }
    }
}
