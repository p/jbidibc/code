package org.bidib.jbidibc.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ByteUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ByteUtilsTest.class);

    @Test
    public void convertLongToUniqueId() {
        long uniqueId = 0xd2000d68000036L;
        byte[] uniqueIdentifier = ByteUtils.convertLongToUniqueId(uniqueId);

        LOGGER.info("Returned uniqueId: {}", ByteUtils.bytesToHex(uniqueIdentifier));

        Assert.assertEquals(new byte[] { (byte) 0xd2, 0x00, 0x0d, 0x68, 0x00, 0x00, 0x36 }, uniqueIdentifier);
    }

    @Test
    public void convertLongToUniqueId2() {
        long uniqueId = 0x05000d6b0069eaL;
        byte[] uniqueIdentifier = ByteUtils.convertLongToUniqueId(uniqueId);

        Assert.assertEquals(new byte[] { (byte) 0x05, 0x00, 0x0d, 0x6b, 0x00, 0x69, (byte) 0xea }, uniqueIdentifier);
    }

    @Test
    public void convertLongToUniqueId3() {
        long uniqueId = 0x0d70000d00L;
        byte[] uniqueIdentifier = ByteUtils.convertLongToUniqueId(uniqueId);

        LOGGER.info("Returned uniqueId: {}", ByteUtils.bytesToHex(uniqueIdentifier));

        Assert.assertEquals(new byte[] { 0x00, 0x00, 0x0d, 0x70, 0x00, 0x0d, 0x00 }, uniqueIdentifier);
    }

    @Test
    public void convertLongToUniqueId4() {
        long uniqueId = 0x0DE9123456L;
        byte[] uniqueIdentifier = ByteUtils.convertLongToUniqueId(uniqueId);

        LOGGER.info("Returned uniqueId: {}", ByteUtils.bytesToHex(uniqueIdentifier));

        Assert.assertEquals(new byte[] { 0x00, 0x00, 0x0d, (byte) 0xE9, 0x12, 0x34, 0x56 }, uniqueIdentifier);
    }

    @Test
    public void convertLongToUniqueId5() {
        long uniqueId = 0x0DE9123456L;

        String uniqueIdString = ByteUtils.convertUniqueIdToString(ByteUtils.convertLongToUniqueId(uniqueId));

        LOGGER.info("Returned uniqueId: {}", uniqueIdString);

        Assert.assertEquals(uniqueIdString, "00.00.0D.E9.12.34.56");
    }

    @Test
    public void getVidPidFromUniqueId() {
        long uniqueId = 0xd2000d68000036L;
        byte[] uniqueIdentifier = ByteUtils.getVidPidFromUniqueId(uniqueId);

        Assert.assertEquals(new byte[] { (byte) 0xd2, 0x00, 0x0d, 0x68, 0x00, 0x00, 0x36 }, uniqueIdentifier);
    }

    @Test
    public void getVidPidFromUniqueId2() {
        long uniqueId = 0x05000d6b0069eaL;
        byte[] uniqueIdentifier = ByteUtils.getVidPidFromUniqueId(uniqueId);

        Assert.assertEquals(new byte[] { (byte) 0x05, 0x00, 0x0d, 0x6b, 0x00, 0x69, (byte) 0xea }, uniqueIdentifier);
    }

    @Test
    public void getClassIdFromUniqueId() {
        long uniqueId = 0xd2000d68000036L;
        int classId = ByteUtils.getClassIdFromUniqueId(uniqueId);
        LOGGER.info("ClassId: {}", classId);
        Assert.assertEquals(0xD2, classId);
    }

    @Test
    public void getClassIdFromUniqueId2() {
        long uniqueId = 0x05000d6b0069eaL;
        int classId = ByteUtils.getClassIdFromUniqueId(uniqueId);
        LOGGER.info("ClassId: {}", classId);
        Assert.assertEquals(0x05, classId);
    }

    @Test
    public void getUniqueIdFormByteArray() {

        byte[] data = new byte[] { 0x40, 0x00, 0x0D, (byte) 0xCF, 0x00, 0x22, 0x00 };
        long uniqueId = ByteUtils.convertUniqueIdToLong(data);

        LOGGER.info("uniqueId: {}", uniqueId);
        Assert.assertEquals(uniqueId, 0x40000DCF002200L);

        byte[] converted = ByteUtils.convertLongToUniqueId(uniqueId);
        LOGGER.info("converted: {}", ByteUtils.bytesToHex(converted));
        Assert.assertEquals(converted, data);
    }

    @Test
    public void getWORD() {
        byte[] sample = new byte[] { 0x50, 0x51 };
        int word = ByteUtils.getWORD(sample);
        LOGGER.info("Converted word: {}", word);

        Assert.assertEquals(word, 0x5150);
    }

    @Test
    public void getWORD2() {
        byte[] sample = new byte[] { 0x51, 0x50 };
        int word = ByteUtils.getWORD(sample);
        LOGGER.info("Converted word: {}", word);

        Assert.assertEquals(word, 0x5051);
    }

    @Test
    public void getDWORD() {
        byte[] sample = new byte[] { 0x01, 0x02, 0x03, 0x04 };
        long dword = ByteUtils.getDWORD(sample);
        LOGGER.info("Converted dword: {}", dword);

        Assert.assertEquals(dword, 0x04030201L);
    }

    @Test
    public void getDWORD2() {
        byte[] sample = new byte[] { (byte) 0xF1, 0x02, 0x03, (byte) 0xF4 };
        long dword = ByteUtils.getDWORD(sample);
        LOGGER.info("Converted dword: {}", dword);

        Assert.assertEquals(dword, 0xF40302F1L);
    }

    @Test
    public void getDWORD3() {
        byte[] sample = new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF };
        long dword = ByteUtils.getDWORD(sample);
        LOGGER.info("Converted dword: {}", dword);

        Assert.assertEquals(dword, 0xFFFFFFFFL);
    }

    @Test
    public void getDWORDWithOffset() {
        byte[] sample = new byte[] { (byte) 0xFF, (byte) 0xFF, 0x01, 0x02, 0x03, 0x04 };
        long dword = ByteUtils.getDWORD(sample, 2);
        LOGGER.info("Converted dword: {}", dword);

        Assert.assertEquals(dword, 0x04030201L);
    }

    @Test
    public void getDWORD2WithOffset() {
        byte[] sample = new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xF1, 0x02, 0x03, (byte) 0xF4 };
        long dword = ByteUtils.getDWORD(sample, 2);
        LOGGER.info("Converted dword: {}", dword);

        Assert.assertEquals(dword, 0xF40302F1L);
    }

    @Test
    public void toDWORD() {
        byte[] sample = new byte[] { 0x01, 0x02, 0x03, 0x04 };
        long dword = 0x04030201L;
        byte[] test = ByteUtils.toDWORD(dword);
        LOGGER.info("Converted test: {}", test);

        Assert.assertEquals(sample, test);
    }

    @Test
    public void toDWORD2() {
        byte[] sample = new byte[] { (byte) 0xF1, 0x02, 0x03, (byte) 0xF4 };
        long dword = 0xF40302F1L;
        byte[] test = ByteUtils.toDWORD(dword);
        LOGGER.info("Converted test: {}", test);

        Assert.assertEquals(sample, test);
    }

    @Test
    public void byteToHexTest() {
        int value = 0xE0;
        String hex = ByteUtils.byteToHex(value);
        LOGGER.info("Converted hex: {}", hex);

        Assert.assertEquals("E0".toCharArray(), hex.toCharArray());
    }

    @Test
    public void int16ToHexTest() {
        int value = 0xE0F0;
        String hex = ByteUtils.int16ToHex(value);
        LOGGER.info("Converted hex: {}", hex);

        Assert.assertEquals("E0F0".toCharArray(), hex.toCharArray());
    }

    @Test
    public void int32ToHexTest() {
        long value = 0xFFFFFFFFL;
        String hex = ByteUtils.int32ToHex(value);
        LOGGER.info("Converted hex: {}", hex);

        Assert.assertEquals("FFFFFFFF".toCharArray(), hex.toCharArray());
    }

    @Test
    public void isBitSetEqualIntTest() {
        int cvValue = 0x08;
        Assert.assertTrue(ByteUtils.isBitSetEqual(cvValue, 1, 3));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue, 0, 3));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue, 1, 2));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue, 1, 4));

        int cvValue2 = 0x28;
        Assert.assertTrue(ByteUtils.isBitSetEqual(cvValue2, 1, 5));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue2, 0, 5));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue2, 1, 4));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue2, 1, 6));
    }

    @Test
    public void isBitSetEqualByteTest() {
        byte cvValue = 0x08;
        Assert.assertTrue(ByteUtils.isBitSetEqual(cvValue, 1, 3));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue, 0, 3));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue, 1, 2));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue, 1, 4));

        byte cvValue2 = 0x28;
        Assert.assertTrue(ByteUtils.isBitSetEqual(cvValue2, 1, 5));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue2, 0, 5));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue2, 1, 4));
        Assert.assertFalse(ByteUtils.isBitSetEqual(cvValue2, 1, 6));
    }

    @Test
    public void getBitTest() {
        byte cvValue = 0x08;
        Assert.assertEquals(1, ByteUtils.getBit(cvValue, 3));
        Assert.assertEquals(0, ByteUtils.getBit(cvValue, 2));
        Assert.assertEquals(0, ByteUtils.getBit(cvValue, 4));
    }

    @Test
    public void getBitIntTest() {
        int cvValue = 0x08;
        Assert.assertEquals(1, ByteUtils.getBit(cvValue, 3));
        Assert.assertEquals(0, ByteUtils.getBit(cvValue, 2));
        Assert.assertEquals(0, ByteUtils.getBit(cvValue, 4));

        int cvValue2 = 237;
        Assert.assertEquals(1, ByteUtils.getBit(cvValue2, 3));
    }

    @Test
    public void getMagicToHexTest() {
        int magic = 0xB00D;
        Assert.assertEquals(ByteUtils.magicToHex(magic), "B00D");

        int magic2 = 0xAFFE;
        Assert.assertEquals(ByteUtils.magicToHex(magic2), "AFFE");
    }

    @Test
    public void getRGBTest() {
        byte[] rgbBytes = new byte[] { 0x11, 0x33, (byte) 0xFF };
        int rgbValue = 0x1133FF;
        Assert.assertEquals(ByteUtils.getRGB(rgbBytes), rgbValue);
    }

    @Test
    public void toRGBTest() {
        int rgbValue = 0x0033FF;
        byte[] rgbBytes = new byte[] { 0x00, 0x33, (byte) 0xFF };
        Assert.assertEquals(ByteUtils.toRGB(rgbValue), rgbBytes);
    }

    @Test
    public void toReconfigTest() {
        int rgbValue = 0xFF3300;
        byte[] rgbBytes = new byte[] { 0x00, 0x33, (byte) 0xFF };
        Assert.assertEquals(ByteUtils.toReconfig(rgbValue), rgbBytes);
    }

    @Test
    public void getCvXNumberTest() {
        byte[] cvxBytes = new byte[] { 0x11, 0x33, 0x34, 0x35 };
        int cvxNumberValue = 0x353433;
        Assert.assertEquals(ByteUtils.getCvXNumber(cvxBytes, 1), cvxNumberValue);
    }

    @Test
    public void getCvXNumber2Test() {
        byte[] cvxBytes = new byte[] { 0x11, 0x33, 0x34, 0x35, 0x36, 0x37 };
        int cvxNumberValue = 0x363534;
        Assert.assertEquals(ByteUtils.getCvXNumber(cvxBytes, 2), cvxNumberValue);
    }

    @Test
    public void getCVXValueTest() {
        byte[] cvxBytes = new byte[] { 0x11, 0x33, 0x34, 0x35, 0x36, 0x37 };
        int cvxValue = 0x36353433;
        Assert.assertEquals(ByteUtils.getCvXValue(cvxBytes, 1, 4), cvxValue);
    }

    @Test
    public void getCVXValue2Test() {
        byte[] cvxBytes = new byte[] { 0x11, 0x33, 0x34, 0x35, 0x36, 0x37 };
        int cvxValue = 0x363534;
        Assert.assertEquals(ByteUtils.getCvXValue(cvxBytes, 2, 3), cvxValue);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void getCVXValueIllegalTest() {
        byte[] cvxBytes = new byte[] { 0x11, 0x33, 0x34, 0x35, 0x36, 0x37 };

        // count=5 will throw an illegal argument exception
        ByteUtils.getCvXValue(cvxBytes, 2, 5);
    }

    @Test
    public void bstrTest() {

        String testString = "Test";
        byte[] bstr = ByteUtils.bstr(testString);

        Assert.assertEquals(bstr, new byte[] { 4, 'T', 'e', 's', 't' });
    }

    @Test
    public void bstr2Test() {
        String testString = "Test";
        String testValue = "val";

        byte[] bstr = ByteUtils.bstr(testString, testValue);

        Assert.assertEquals(bstr, new byte[] { 4, 'T', 'e', 's', 't', 3, 'v', 'a', 'l' });
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Maximum string length of 255 exceeded\\.")
    public void bstr3Test() {
        String testString =
            "1Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. ";

        ByteUtils.bstr(testString);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Maximum string length of 255 exceeded\\.")
    public void bstr4Test() {
        String testString =
            "1Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. ";
        String testValue =
            "1Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. "
                + "Lorem ipsum. Lorem ipsum. Lorem ipsum. ";

        ByteUtils.bstr(testString, testValue);
    }

    @Test
    public void cstrTest() {

        String testString = "Der Knoten hat nicht die Konfiguration f\u00fcr alle Ports geliefert!";
        byte[] bstr = ByteUtils.bstr(testString);
        String ret = ByteUtils.cstr(bstr, 0);

        Assert.assertNotNull(ret);
        Assert.assertEquals(ret, "Der Knoten hat nicht die Konfiguration für alle Ports geliefert!");
    }

    @Test
    public void toArrayTest() {
        byte[] ret = ByteUtils.toArray((byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x00);
        Assert.assertEquals(ret, new byte[] { (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x00 });
    }

    @Test
    public void appendTest() {
        byte[] array1 = ByteUtils.toArray((byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x00);
        byte value = (byte) 0x05;
        byte[] ret = ByteUtils.append(array1, value);
        Assert.assertEquals(ret, new byte[] { (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x00, (byte) 0x05 });
    }

    @Test
    public void toIntTest() {
        int highValue = 0xFE;
        int lowValue = 0xABCD;

        int ret = ByteUtils.toInt(highValue, lowValue);

        Assert.assertEquals(ret, 0xFECD);
    }

    @Test
    public void longToHexTest() {
        long value = 0xABCDEF01L;

        String ret = ByteUtils.longToHex(value);

        Assert.assertEquals(ret, "ABCDEF01");
    }

    @Test
    public void longToHex2Test() {
        long value = 0x1234ABCDL;

        String ret = ByteUtils.longToHex(value);

        Assert.assertEquals(ret, "1234ABCD");
    }

    @Test
    public void longToHex3Test() {
        int value = 0xFA0A1375;

        String ret = ByteUtils.longToHex(value);

        Assert.assertEquals(ret, "FA0A1375");
    }

    @Test
    public void int32ToHex3Test() {
        long value = 0xFA0A1375;

        String ret = ByteUtils.int32ToHex(value);

        Assert.assertEquals(ret, "FA0A1375");
    }

    @Test
    public void didToHexTest() {
        byte[] value = new byte[] { (byte) 0xDE, 0x27, (byte) 0xC8, (byte) 0xFF, (byte) 0x97 };

        String ret = ByteUtils.didToHex(value);

        Assert.assertEquals(ret, "97 FFC827DE");
    }

    @Test
    public void toStringTest() {
        byte bVal1 = 5;
        byte bVal2 = ByteUtils.getLowByte(255);
        byte bVal3 = 0;

        Assert.assertEquals(ByteUtils.toString(bVal1), "5");
        Assert.assertEquals(ByteUtils.toString(bVal2), "255");
        Assert.assertEquals(ByteUtils.toString(bVal3), "0");
    }

    @Test
    public void parseHexBinarySpaceTerminatedTest() {
        String hexBinary = "02 C5 00 01 04 03 C0 00 00 ";
        byte[] parsed = ByteUtils.parseHexBinarySpaceTerminated(hexBinary);

        Assert.assertNotNull(parsed);
        Assert.assertEquals(parsed.length, 9);

        Assert.assertEquals(parsed[0], (byte) 0x02);
        Assert.assertEquals(parsed[1], (byte) 0xC5);
    }

    @Test
    public void getMaskedAndShiftedValueTest() {

        Assert.assertEquals(ByteUtils.getMaskedAndShiftedValue(0x03, 0x03), 0x03);

        Assert.assertEquals(ByteUtils.getMaskedAndShiftedValue(0x03, 0x02), 0x01);

        Assert.assertEquals(ByteUtils.getMaskedAndShiftedValue(0x03, 0x20), 0x00);

        // mask: 0111 1000
        Assert.assertEquals(ByteUtils.getMaskedAndShiftedValue(0x03, 0x78), 0x00);
        Assert.assertEquals(ByteUtils.getMaskedAndShiftedValue(0x30, 0x78), 0x06);
    }
}
