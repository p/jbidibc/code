package org.bidib.jbidibc.simulation.net;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.jbidibc.simulation.SimulatorRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SimulationMessageReceiver implements SimulationBidibMessageProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationMessageReceiver.class);

    protected static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("RAW");

    private SimulatorRegistry simulatorRegistry;

    private RequestFactory requestFactory;

    boolean escapeHot = false;

    private final Object accessLock = new Object();

    protected AtomicBoolean running = new AtomicBoolean();

    public SimulationMessageReceiver() {

        // enable the running flag
        running.set(true);
    }

    public void setSimulatorRegistry(SimulatorRegistry simulatorRegistry) {
        this.simulatorRegistry = simulatorRegistry;
    }

    private synchronized RequestFactory getRequestFactory() {
        if (requestFactory == null) {
            requestFactory = new RequestFactory();
        }

        return requestFactory;
    }

    public void enable() {
        LOGGER.info("enable is called.");
        synchronized (accessLock) {
            escapeHot = false;
        }
        MSG_RAW_LOGGER.info("++++ Enable the message receiver.");

        running.set(true);
    }

    public void disable() {
        LOGGER.info("Disable is called.");

        running.set(false);

        MSG_RAW_LOGGER.info("++++ Disable the message receiver.");

        synchronized (accessLock) {
            escapeHot = false;
        }
    }

    public void purgeReceivedDataInBuffer() {

    }

    @Override
    public void receive(final ByteArrayOutputStream output) {

        if (!running.get()) {
            LOGGER.warn("Receiver is not running. Discard messages.");

            return;
        }

        LOGGER.info("Receive data: {}", ByteUtils.bytesToHex(output));

        // if a CRC error is detected in splitMessages the reading loop will terminate ...
        try {
            List<BidibCommand> commands = new ArrayList<BidibCommand>();
            getRequestFactory().createFromRaw(output.toByteArray(), commands);

            if (CollectionUtils.isNotEmpty(commands)) {

                for (BidibCommand bidibCommand : commands) {
                    LOGGER.info("Process the current bidibCommand: {}", bidibCommand);

                    String nodeAddress = NodeUtils.formatAddress(bidibCommand.getAddr());

                    SimulatorNode simulatorNode = simulatorRegistry.getSimulator(nodeAddress);
                    if (simulatorNode != null) {
                        simulatorNode.processRequest(bidibCommand);
                    }
                    else {
                        LOGGER.error("No simulator available for address: {}", nodeAddress);
                    }
                }
            }
            else {
                LOGGER.warn("No commands in packet received.");
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create BiDiB message failed.", ex);
        }

    }

    @Override
    public abstract void publishResponse(ByteArrayOutputStream output) throws ProtocolException;

    @Override
    public void processMessages(ByteArrayOutputStream output) throws ProtocolException {
        LOGGER.warn("processMessages() is not implemented in SimulationMessageReceiver.");
    }

    @Override
    public String getErrorInformation() {
        return null;
    }

    @Override
    public void addMessageListener(MessageListener messageListener) {
    }

    @Override
    public void removeMessageListener(MessageListener messageListener) {
    }

    @Override
    public void addNodeListener(NodeListener nodeListener) {
    }

}
