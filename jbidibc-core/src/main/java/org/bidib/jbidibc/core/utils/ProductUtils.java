package org.bidib.jbidibc.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductUtils.class);

    public static boolean isOneOC(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneOC for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 204) {
            return true;
        }
        return false;
    }

    public static boolean isOneDMX(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneDMX for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 115) {
            return true;
        }
        return false;
    }

    public static boolean isOneServoTurn(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneServoTurn for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 121) {
            return true;
        }
        return false;
    }

    public static boolean isSTu(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a STu for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 123) {
            return true;
        }
        return false;
    }

    public static boolean isOneDriveTurn(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneServoTurn for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && (pid == 122 || pid == 143 || pid == 144 || pid == 145)) {
            return true;
        }
        return false;
    }

    public static boolean isOneControl(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneControl for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && (pid == 117 || pid == 140 || pid == 141 || pid == 142)) {
            return true;
        }
        return false;
    }

    public static boolean isOneHub(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneHub for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 114) {
            return true;
        }
        return false;
    }

    public static boolean isOneBootloader(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a OneBootloader for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 112) {
            return true;
        }
        return false;
    }

    public static boolean isLightControl(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a LightControl for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 107) {
            return true;
        }
        return false;
    }

    public static boolean isMobaList(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a MobaList for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 108) {
            return true;
        }
        return false;
    }

    public static boolean isWs2812(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a Ws2812 for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 159) {
            return true;
        }
        return false;
    }

    public static boolean isStepControl(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a StepControl for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if ((vid == 13 && pid == 120) || (vid == 251 && (pid == 202 || pid == 201))) {
            return true;
        }
        return false;
    }

    public static boolean isNeoControl(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a NeoControl for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && (pid == 205 || pid == 206)) {
            return true;
        }
        return false;
    }

    public static boolean isNeoEWS(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a NeoEWS for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 129) {
            return true;
        }
        return false;
    }

    // uniqueId=0x50000d00808a00
    public static boolean isRFBasisNode(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a RFBasisNode for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 32770) {
            return true;
        }
        return false;
    }

    public static boolean isSpeedometer(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a Speedometer for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 13 && pid == 32780) {
            return true;
        }
        return false;
    }

    public static boolean isMultiDecoder(long uniqueId) {
        long pid = NodeUtils.getPid(uniqueId);
        long vid = NodeUtils.getVendorId(uniqueId);
        LOGGER.debug("Check if node is a MultiDecoder for uniqueId: {}, pid: {}, vid: {}",
            NodeUtils.getUniqueIdAsString(uniqueId), pid, vid);

        if (vid == 62 && pid == 151) {
            return true;
        }
        return false;
    }

}
