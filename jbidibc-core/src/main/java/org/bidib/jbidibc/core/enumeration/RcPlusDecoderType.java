package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;

public enum RcPlusDecoderType implements BidibEnum {

    LOCO(0, "loco"), ACCESSORY(1, "accessory");

    private final byte type;

    private final String key;

    private RcPlusDecoderType(int type, String key) {
        this.type = ByteUtils.getLowByte(type);
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public byte getType() {
        return type;
    }

}
