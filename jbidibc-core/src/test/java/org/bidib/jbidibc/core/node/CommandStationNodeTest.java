package org.bidib.jbidibc.core.node;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.enumeration.DriveAcknowledge;
import org.bidib.jbidibc.core.enumeration.PomAcknowledge;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CommandStationNodeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibNodeTest.class);

    private int responseCounter = 1;

    private RequestFactory requestFactory;

    @BeforeTest
    public void prepare() {
        LOGGER.info("Prepare the request factory.");
        requestFactory = new RequestFactory();
    }

    @Test
    public void clearLoco() throws ProtocolException {
        final byte[] address = new byte[] { 0 };

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        MessageReceiver messageReceiver = null;
        boolean ignoreWaitTimeout = false;
        final BidibNode bidibNode = new BidibNode(address, messageReceiver, ignoreWaitTimeout);
        bidibNode.setNodeMagic(BidibLibrary.BIDIB_SYS_MAGIC);
        bidibNode.setBidib(bidib);
        bidibNode.setRequestFactory(requestFactory);

        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                byte[] bytes = (byte[]) args[0];
                LOGGER.info("Received data: {}", ByteUtils.bytesToHex(bytes));
                byte addr_l = bytes[5];
                byte addr_h = bytes[6];

                LOGGER.info("Got addr_l: {}, addr_h: {}", addr_l, addr_h);

                byte[] message =
                    { 0x06, 0x00, 0x14, (byte) BidibLibrary.MSG_CS_DRIVE_ACK, addr_l, addr_h,
                        DriveAcknowledge.NOT_ACKNOWLEDGED.getType() };
                BidibMessage response;
                try {
                    LOGGER.info("Create new response, responseCounter: {}", responseCounter);
                    response = new BidibResponseFactory().create(message);
                    LOGGER.info("Prepared response: {}", response);
                    // bidibNode.getReceiveQueue().add(response);
                    bidibNode.addResponseToReceiveQueue(response);
                }
                catch (ProtocolException ex) {
                    LOGGER.warn("Put message to receive queue failed.", ex);
                }

                return null;
            }
        }).when(bidib).send(Mockito.any(byte[].class));

        final CommandStationNode commandStationNode = new CommandStationNode(bidibNode);

        DriveAcknowledge driveAckn = commandStationNode.clearLoco(1);
        LOGGER.info("Returned driveAckn result: {}", driveAckn);
        Assert.assertNotNull(driveAckn);

        Mockito.verify(bidib, Mockito.times(1)).send(Mockito.any(byte[].class));

        Assert.assertEquals(driveAckn, DriveAcknowledge.NOT_ACKNOWLEDGED);
    }

    @Test
    public void clearLocoAll() throws ProtocolException {
        final byte[] address = new byte[] { 0 };

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        MessageReceiver messageReceiver = null;
        boolean ignoreWaitTimeout = false;
        final BidibNode bidibNode = new BidibNode(address, messageReceiver, ignoreWaitTimeout);
        bidibNode.setNodeMagic(BidibLibrary.BIDIB_SYS_MAGIC);
        bidibNode.setBidib(bidib);
        bidibNode.setRequestFactory(requestFactory);

        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                byte[] bytes = (byte[]) args[0];
                LOGGER.info("Received data: {}", ByteUtils.bytesToHex(bytes));
                byte addr_l = bytes[5];
                byte addr_h = bytes[6];

                LOGGER.info("Got addr_l: {}, addr_h: {}", addr_l, addr_h);

                byte[] message =
                    { 0x06, 0x00, 0x14, (byte) BidibLibrary.MSG_CS_DRIVE_ACK, addr_l, addr_h,
                        DriveAcknowledge.ACKNOWLEDGED.getType() };
                BidibMessage response;
                try {
                    LOGGER.info("Create new response, responseCounter: {}", responseCounter);
                    response = new BidibResponseFactory().create(message);
                    LOGGER.info("Prepared response: {}", response);
                    // bidibNode.getReceiveQueue().add(response);
                    bidibNode.addResponseToReceiveQueue(response);
                }
                catch (ProtocolException ex) {
                    LOGGER.warn("Put message to receive queue failed.", ex);
                }

                return null;
            }
        }).when(bidib).send(Mockito.any(byte[].class));

        final CommandStationNode commandStationNode = new CommandStationNode(bidibNode);

        DriveAcknowledge driveAckn = commandStationNode.clearLoco(0);
        LOGGER.info("Returned driveAckn result: {}", driveAckn);
        Assert.assertNotNull(driveAckn);

        Mockito.verify(bidib, Mockito.times(1)).send(Mockito.any(byte[].class));

        Assert.assertEquals(driveAckn, DriveAcknowledge.ACKNOWLEDGED);
    }

    @Test
    public void writePom_XWR_BYTE2() throws ProtocolException {
        final byte[] address = new byte[] { 0 };

        BidibInterface bidib = Mockito.mock(BidibInterface.class);

        MessageReceiver messageReceiver = null;
        boolean ignoreWaitTimeout = false;
        final BidibNode bidibNode = new BidibNode(address, messageReceiver, ignoreWaitTimeout);
        bidibNode.setNodeMagic(BidibLibrary.BIDIB_SYS_MAGIC);
        bidibNode.setBidib(bidib);
        bidibNode.setRequestFactory(requestFactory);

        // mock
        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                byte[] bytes = (byte[]) args[0];
                LOGGER.info("Received data: {}", ByteUtils.bytesToHex(bytes));
                byte addr_l = bytes[4];
                byte addr_h = bytes[5];

                LOGGER.info("Got addr_l: {}, addr_h: {}", addr_l, addr_h);

                Assert.assertEquals(bytes[4], (byte) 0x34);
                Assert.assertEquals(bytes[5], (byte) 0x12);

                Assert.assertEquals(bytes[9], CommandStationPom.XWR_BYTE2.getType());

                Assert.assertEquals(bytes[10], (byte) 0x04);
                Assert.assertEquals(bytes[13], (byte) 0x45);
                Assert.assertEquals(bytes[14], (byte) 0x23);

                byte[] message =
                    { 0x09, 0x00, 0x14, (byte) BidibLibrary.MSG_CS_POM_ACK, addr_l, addr_h, 0x00, 0x00, 0x00,
                        PomAcknowledge.ACKNOWLEDGED.getType() };
                BidibMessage response;
                try {
                    LOGGER.info("Create new response, responseCounter: {}", responseCounter);
                    response = new BidibResponseFactory().create(message);
                    LOGGER.info("Prepared response: {}", response);
                    // bidibNode.getReceiveQueue().add(response);
                    bidibNode.addResponseToReceiveQueue(response);
                }
                catch (ProtocolException ex) {
                    LOGGER.warn("Put message to receive queue failed.", ex);
                }

                return null;
            }
        }).when(bidib).send(Mockito.any(byte[].class));

        //

        AddressData locoAddress = new AddressData(0x1234, AddressTypeEnum.LOCOMOTIVE_FORWARD);

        final CommandStationNode commandStationNode = new CommandStationNode(bidibNode);

        int cvNumber = 0x04;
        int cvValue = 0x2345;
        PomAcknowledge pomAckn =
            commandStationNode.writePom(locoAddress, CommandStationPom.XWR_BYTE2, cvNumber, cvValue);
        LOGGER.info("Returned pomAckn result: {}", pomAckn);
        Assert.assertNotNull(pomAckn);

        Mockito.verify(bidib, Mockito.times(1)).send(Mockito.any(byte[].class));

        Assert.assertEquals(pomAckn, PomAcknowledge.ACKNOWLEDGED);
    }
}
