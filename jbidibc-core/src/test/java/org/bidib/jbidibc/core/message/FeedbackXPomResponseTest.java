package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FeedbackXPomResponseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackXPomResponseTest.class);

    // IN --- : MSG_BM_XPOM 0F 00 D9 A8 01 02 00 00 00 02 01 00 04 05 06 07
    @Test
    public void getCvNumber() throws ProtocolException {
        // dec vid: 0
        // decoder addr: 01 02
        // opcode: 0x81 /*BidibLibrary.BIDIB_CS_XPOM_RD_BLOCK*/
        // CV: 2 (1 + 1 because 0 -> CV1)

        byte[] message =
            new byte[] { 0x10, 0x00, (byte) 0xD9, (byte) 0xA8, 0x02, 0x01, 0x00, 0x00, 0x00,
                (byte) BidibLibrary.BIDIB_CS_XPOM_RD_BLOCK, 0x01, 0x00, 0x00, 0x04, 0x05, 0x06, 0x07 };
        BidibMessage result = new BidibMessage(message);
        FeedbackXPomResponse feedbackXPomResponse =
            new FeedbackXPomResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(feedbackXPomResponse);
        LOGGER.info("Prepared feedbackXPomResponse: {}", feedbackXPomResponse);

        Assert.assertNotNull(feedbackXPomResponse.getDecoderAddress());
        Assert.assertEquals(feedbackXPomResponse.getDecoderAddress().getAddress(), 258);
        Assert.assertEquals(feedbackXPomResponse.getOpCode(), BidibLibrary.BIDIB_CS_XPOM_RD_BLOCK);
        Assert.assertEquals(feedbackXPomResponse.getCvNumber(), 2);
        Assert.assertEquals(feedbackXPomResponse.getCvXValue(), new int[] { 0x04, 0x05, 0x06, 0x07 });
    }

    // IN --- : MSG_BM_XPOM 0F 00 D9 A8 01 02 03 04 61 02 00 01 00 04 05 06 07
    @Test
    public void getDecoderSerialNumber() throws ProtocolException {
        // 0x61 : D&H

        byte[] message =
            new byte[] { 0x10, 0x00, (byte) 0xD9, (byte) 0xA8, 0x01, 0x02, 0x03, 0x04, 0x61,
                (byte) BidibLibrary.BIDIB_CS_XPOM_RD_BLOCK, 0x02, 0x01, 0x00, 0x04, 0x05, 0x06, 0x07 };
        BidibMessage result = new BidibMessage(message);
        FeedbackXPomResponse feedbackXPomResponse =
            new FeedbackXPomResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(feedbackXPomResponse);
        LOGGER.info("Prepared feedbackXPomResponse: {}", feedbackXPomResponse);

        Assert.assertNull(feedbackXPomResponse.getDecoderAddress());

        Assert.assertEquals(feedbackXPomResponse.getDecoderVendorId(), 0x61);
        Assert.assertEquals(feedbackXPomResponse.getDecoderSerialNum(), 0x04030201);

        Assert.assertEquals(feedbackXPomResponse.getCvNumber(), 259);
        Assert.assertEquals(feedbackXPomResponse.getCvXValue(), new int[] { 0x04, 0x05, 0x06, 0x07 });
    }

    // << MSG_BM_XPOM[[0],num=56,type=168,data=[191, 74, 200, 255, 151, 0, 28, 0, 0, 12, 0, 0, 0]] : 10 00 38 A8 BF 4A
    // C8 FF 97 00 1C 00 00 0C 00 00 00
    @Test
    public void getCvNumberWithDID() throws ProtocolException {
        byte[] message =
            new byte[] { 0x10, 0x00, 0x38, (byte) 0xA8, (byte) 0xBF, 0x4A, (byte) 0xC8, (byte) 0xFF, (byte) 0x97, 0x00,
                0x1C, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00 };

        BidibMessage result = new BidibMessage(message);
        FeedbackXPomResponse feedbackXPomResponse =
            new FeedbackXPomResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(feedbackXPomResponse);
        LOGGER.info("Prepared feedbackXPomResponse: {}", feedbackXPomResponse);

        Assert.assertTrue(feedbackXPomResponse.isDiDAddress());

        Assert.assertEquals(feedbackXPomResponse.getDecoderVendorId(), 0x97);
        Assert.assertEquals(feedbackXPomResponse.getDecoderSerialNum(), 0xFFC84ABF);

        Assert.assertEquals(feedbackXPomResponse.getCvNumber(), 29);
        Assert.assertEquals(feedbackXPomResponse.getCvXValue(), new int[] { 0x0C });
    }
}
