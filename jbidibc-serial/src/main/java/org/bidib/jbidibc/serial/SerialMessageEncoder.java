package org.bidib.jbidibc.serial;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.CRC8;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class SerialMessageEncoder {

    public static void encodeMessage(final ByteArrayOutputStream data, final OutputStream output) throws IOException {

        byte[] message = data.toByteArray();

        // encode all messages
        sendDelimiter(output);

        int index = 0;
        int totalLen = message.length;

        int txCrc = 0;
        while (index < totalLen) {
            // get the length
            byte length = message[index++];

            escape(output, length);

            txCrc = CRC8.getCrcValue((length ^ txCrc) & 0xFF);

            for (int i = index; i <= (length + index - 1); i++) {
                escape(output, message[i]);
                txCrc = CRC8.getCrcValue((message[i] ^ txCrc) & 0xFF);
            }
            index += length;
        }
        escape(output, ByteUtils.getLowByte(txCrc));
        sendDelimiter(output);
    }

    private static void sendDelimiter(final OutputStream output) throws IOException {
        output.write((byte) BidibLibrary.BIDIB_PKT_MAGIC);
    }

    private static void escape(final OutputStream output, byte c) throws IOException {
        if ((c == (byte) BidibLibrary.BIDIB_PKT_MAGIC) || (c == (byte) BidibLibrary.BIDIB_PKT_ESCAPE)) {
            output.write((byte) BidibLibrary.BIDIB_PKT_ESCAPE);
            c = (byte) (c ^ 0x20);
        }
        output.write(c);
    }

}
