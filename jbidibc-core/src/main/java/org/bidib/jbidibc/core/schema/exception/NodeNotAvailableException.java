package org.bidib.jbidibc.core.schema.exception;

public class NodeNotAvailableException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NodeNotAvailableException(String message) {
        super(message);
    }
}
