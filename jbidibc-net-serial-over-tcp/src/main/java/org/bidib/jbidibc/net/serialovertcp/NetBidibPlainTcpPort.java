package org.bidib.jbidibc.net.serialovertcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.net.NetBidibPort;
import org.bidib.jbidibc.net.NetMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetBidibPlainTcpPort implements NetBidibPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibPlainTcpPort.class);

    private final InetAddress address;

    private final int portNumber;

    private final NetMessageHandler messageReceiver;

    private AtomicBoolean runEnabled = new AtomicBoolean();

    private Socket socket;

    public NetBidibPlainTcpPort(InetAddress address, int portNumber, NetMessageHandler messageReceiver)
        throws IOException {

        this.messageReceiver = messageReceiver;
        this.address = address;
        this.portNumber = portNumber;

        if (address == null) {
            throw new IllegalArgumentException("address must not be null!");
        }

        this.socket = new Socket(address, portNumber);
        LOGGER.info("Created TCP socket for address: {},  port number: {}", address, portNumber);
    }

    private final ScheduledExecutorService acceptWorker = Executors.newScheduledThreadPool(1);

    @Override
    public void run() {
        LOGGER.info("Start the TCP socket.");
        runEnabled.set(true);

        LOGGER.info("Start client receiver handler.");
        // add a task to the worker to let the node process the send queue
        acceptWorker.submit(new NetBidibPlainTcpServerSocketHandler(socket, messageReceiver));
        LOGGER.info("Start client receiver handler has passed.");

        LOGGER.info("Start has passed.");
    }

    public void stop() {
        LOGGER.info("Stop the TCP packet receiver, socket: {}", socket);
        runEnabled.set(false);

        if (socket != null) {
            try {
                socket.close();
            }
            catch (IOException ex) {
                LOGGER.warn("Close socket failed.", ex);
            }
            socket = null;
        }
    }

    /**
     * Send the data to the host.
     * 
     * @param sendData
     *            the data to send
     * @param address
     *            the receiving address of the host
     * @param portNumber
     *            the receiving port number of the host
     * @throws IOException
     */
    public void send(byte[] sendData, InetAddress address, int portNumber) throws IOException {
        // if (LOGGER.isTraceEnabled()) {
        LOGGER.info("Send data to socket, port: {}, bytes: {}", portNumber, ByteUtils.bytesToHex(sendData));
        // }

        socket.getOutputStream().write(sendData);
        socket.getOutputStream().flush();
    }
}
