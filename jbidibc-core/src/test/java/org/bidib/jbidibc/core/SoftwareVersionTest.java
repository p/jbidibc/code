package org.bidib.jbidibc.core;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SoftwareVersionTest {

    @Test
    public void testEqualsPass() {
        SoftwareVersion softwareVersion1 = new SoftwareVersion(1, 0, 0);
        SoftwareVersion softwareVersion2 = new SoftwareVersion(1, 0, 0);

        Assert.assertTrue(softwareVersion1.equals(softwareVersion2));
    }

    @Test
    public void testEqualsFail() {
        SoftwareVersion softwareVersion1 = new SoftwareVersion(1, 0, 0);
        SoftwareVersion softwareVersion2 = new SoftwareVersion(1, 0, 1);

        Assert.assertFalse(softwareVersion1.equals(softwareVersion2));
    }

    @Test
    public void testToInt() {
        SoftwareVersion softwareVersion1 = new SoftwareVersion(1, 0, 0);
        Assert.assertEquals(softwareVersion1.toInt(), Integer.valueOf(0x010000));

        SoftwareVersion softwareVersion2 = new SoftwareVersion(1, 0, 1);
        Assert.assertEquals(softwareVersion2.toInt(), Integer.valueOf(0x010001));

        SoftwareVersion softwareVersion3 = new SoftwareVersion(1, 1, 1);
        Assert.assertEquals(softwareVersion3.toInt(), Integer.valueOf(0x010101));
    }

    @Test
    public void testCompareVersion() {
        SoftwareVersion softwareVersion1 = new SoftwareVersion(1, 0, 0);
        SoftwareVersion softwareVersion2 = new SoftwareVersion(1, 0, 1);

        Assert.assertTrue(softwareVersion1.compareTo(softwareVersion2) < 0);
        Assert.assertTrue(softwareVersion2.compareTo(softwareVersion1) > 0);
    }

    @Test
    public void testCompareVersion2() {
        SoftwareVersion softwareVersion1 = new SoftwareVersion(1, 0, 0);
        SoftwareVersion softwareVersion2 = new SoftwareVersion(1, 0, 1);

        Assert.assertTrue(softwareVersion1.isLowerThan(softwareVersion2));
        Assert.assertTrue(softwareVersion2.isHigherThan(softwareVersion1));
    }

    @Test
    public void testParseVersion() {
        SoftwareVersion softwareVersion = SoftwareVersion.parse("1.0.0");
        Assert.assertNotNull(softwareVersion);
        Assert.assertEquals(softwareVersion.toInt(), Integer.valueOf(0x010000));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testParseInvalidVersion() {
        SoftwareVersion.parse("1.0");
    }
}
