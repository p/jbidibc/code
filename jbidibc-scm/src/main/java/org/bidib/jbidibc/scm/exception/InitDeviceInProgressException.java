package org.bidib.jbidibc.scm.exception;

public class InitDeviceInProgressException extends Exception {

    private static final long serialVersionUID = 1L;

    public InitDeviceInProgressException(String message, Throwable ex) {
        super(message, ex);
    }
}
