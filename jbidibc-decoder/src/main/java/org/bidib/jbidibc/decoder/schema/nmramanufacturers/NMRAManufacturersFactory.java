package org.bidib.jbidibc.decoder.schema.nmramanufacturers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.exception.InvalidContentException;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.bidib.jbidibc.decoder.schema.manufacturers.ManufacturersList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class NMRAManufacturersFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(NMRAManufacturersFactory.class);

    private static JAXBContext jaxbContext;

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.decoder.schema.manufacturers";

    public static final String XSD_LOCATION = "/xsd/manufacturers.xsd";

    public static final String XSD_LOCATION_COMMON_TYPES = "/xsd/commonTypes.xsd";

    /**
     * Load the NMRA manufacturers from the file.
     * 
     * @param filePath
     *            the file path
     * @return the list of NMRA manufacturers
     */
    public ManufacturersList loadNmraManufacturers(String filePath) {

        if (StringUtils.isBlank(filePath)) {
            LOGGER.error("The NmraManufacturers file must be specified!");
            return null;
        }

        LOGGER.info("Load NmraManufacturers from file: {}", filePath);

        ManufacturersList nmraManufacturers = null;
        FileInputStream is = null;
        File file = null;
        try {
            file = new File(filePath);

            is = new FileInputStream(file);

            nmraManufacturers = loadNmraManufacturersFile(is);
            LOGGER.trace("Loaded nmraManufacturers: {}", nmraManufacturers);

        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load saved NmraManufacturers from file failed with schema exception: {}", ex.getMessage());

            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    LOGGER.warn("Close input stream failed.", e);
                }
                is = null;
            }

            if (file != null) {

                try {
                    is = new FileInputStream(file);
                    List<String> errors = validateNmraManufacturersFile(is, filePath);

                    // LOGGER.warn("The following validation errors were detected: {}", errors);

                    throw new InvalidContentException("Validation of NmraManufacturers file failed: " + filePath,
                        errors);
                }
                catch (FileNotFoundException e) {
                    LOGGER.warn("Load saved NmraManufacturers for validation from file failed: {}", e.getMessage());
                }
            }
        }
        catch (IllegalArgumentException | FileNotFoundException ex) {
            LOGGER.warn("Load saved NmraManufacturers from file failed: {}", ex.getMessage());
        }
        catch (InvalidContentException ex) {
            // rethrow exception
            throw ex;
        }
        catch (Exception ex) {
            LOGGER.warn("Load saved NmraManufacturers from file failed: {}", ex.getMessage());
        }

        return nmraManufacturers;
    }

    protected String getNmraManufacturersFileName() {
        // TODO change it
        return "test";
    }

    /**
     * Load the labels from the provided input stream.
     * 
     * @param is
     *            the input stream
     * @return the labels
     */
    private List<String> validateNmraManufacturersFile(final InputStream is, String filePath) {
        LOGGER.info("Validate NmraManufacturers file: {}", filePath);

        List<String> errors = null;
        try {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // Schema schema =
            // schemaFactory.newSchema(new File(NMRAManufacturersFactory.class.getResource(XSD_LOCATION).getPath()));
            StreamSource streamSourceSchemaCommonTypes =
                new StreamSource(NMRAManufacturersFactory.class.getResourceAsStream(XSD_LOCATION_COMMON_TYPES));
            StreamSource streamSourceSchema =
                new StreamSource(NMRAManufacturersFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceSchemaCommonTypes, streamSourceSchema });

            StreamSource streamSource = new StreamSource(is);

            Validator validator = schema.newValidator();
            XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();
            validator.setErrorHandler(errorHandler);

            validator.validate(streamSource);

            errors = errorHandler.getErrors();
        }
        catch (Exception ex) {
            // TODO: handle exception
            LOGGER.warn("Validate NmraManufacturers from file failed: {}", getNmraManufacturersFileName(), ex);
            throw new InvalidSchemaException("Load NmraManufacturers from file failed.");
        }

        return errors;
    }

    /**
     * Load the labels from the provided input stream.
     * 
     * @param is
     *            the input stream
     * @return the labels
     */
    private ManufacturersList loadNmraManufacturersFile(final InputStream is) {
        LOGGER.info("Load NmraManufacturers file.");

        ManufacturersList nmraManufacturers = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for NmraManufacturers.");
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            StreamSource streamSourceSchemaCommonTypes =
                new StreamSource(NMRAManufacturersFactory.class.getResourceAsStream(XSD_LOCATION_COMMON_TYPES));
            StreamSource streamSourceSchema =
                new StreamSource(NMRAManufacturersFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceSchemaCommonTypes, streamSourceSchema });
            // Schema schema =
            // schemaFactory.newSchema(new File(NMRAManufacturersFactory.class.getResource(XSD_LOCATION).getPath()));

            unmarshaller.setSchema(schema);

            nmraManufacturers = (ManufacturersList) unmarshaller.unmarshal(is);
        }
        catch (UnmarshalException ex) {
            LOGGER.warn("Load NmraManufacturers from file failed with UnmarshalException.", ex);
            if (ex.getCause() instanceof SAXParseException) {
                throw new InvalidSchemaException("Load NmraManufacturers from file failed");
            }

            throw new IllegalArgumentException("Load NmraManufacturers from file failed with UnmarshalException.");
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load NmraManufacturers from file failed: {}", getNmraManufacturersFileName(), ex);

            throw new InvalidSchemaException(
                "Load NmraManufacturers from file failed: " + getNmraManufacturersFileName());
        }
        catch (Exception ex) {
            LOGGER.warn("Load NmraManufacturers from file failed: {}", getNmraManufacturersFileName(), ex);
            throw new RuntimeException("Load NmraManufacturers from file failed: " + getNmraManufacturersFileName());
        }
        return nmraManufacturers;
    }

}
