package org.bidib.jbidibc.core;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcConfigX {

    private final BidibPort bidibPort;

    // the values are stored in a map
    private Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

    public LcConfigX(BidibPort bidibPort, Map<Byte, PortConfigValue<?>> values) {
        this.bidibPort = bidibPort;
        // copy the values to the list
        this.values.putAll(values);
    }

    public LcOutputType getOutputType(PortModelEnum portModel) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(bidibPort.getValues()[0]);
                break;
        }
        return outputType;
    }

    public int getOutputNumber(PortModelEnum portModel) {
        int outputNumber = -1;
        switch (portModel) {
            case flat:
                outputNumber = ByteUtils.getInt(bidibPort.getValues()[0], bidibPort.getValues()[1]);
                break;
            default:
                outputNumber = ByteUtils.getInt(bidibPort.getValues()[1]);
                break;
        }
        return outputNumber;
    }

    public Map<Byte, PortConfigValue<?>> getPortConfig() {
        return values;
    }

    public <T> T getPortConfigValue(Byte key) {
        return (T) values.get(key).getValue();
    }

    /**
     * @return the continue marker is contained in the values map
     */
    public boolean isContinueDetected() {
        if (MapUtils.isNotEmpty(values) && values.containsKey(BidibLibrary.BIDIB_PCFG_CONTINUE)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof LcConfigX) {
            LcConfigX other = (LcConfigX) obj;
            if (!bidibPort.equals(other.bidibPort)) {
                return false;
            }
            if (!values.equals(other.values)) {
                return false;
            }
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
