package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class LcConfigXGetMessage extends BidibCommandMessage implements BidibBulkCommand {

    protected LcConfigXGetMessage(BidibPort bidibPort) {
        super(0, BidibLibrary.MSG_LC_CONFIGX_GET, bidibPort.getValues());
    }

    public LcConfigXGetMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_LC_CONFIGX_GET";
    }

    public LcOutputType getPortType(PortModelEnum portModel) {
        LcOutputType outputType = null;
        switch (portModel) {
            case flat_extended:
            case flat:
                outputType = LcOutputType.SWITCHPORT;
                break;
            default:
                outputType = LcOutputType.valueOf(getData()[0]);
                break;
        }
        return outputType;
    }

    public int getPortNumber(PortModelEnum portModel) {
        int portNumber = -1;
        switch (portModel) {
            case flat_extended:
                portNumber = ByteUtils.getInt(getData()[0], getData()[0] & 0x7F);
                break;
            case flat:
                portNumber = ByteUtils.getInt(getData()[0]);
                break;
            default:
                portNumber = ByteUtils.getInt(getData()[1], 0x7F);
                break;
        }
        return portNumber;
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }

    @Override
    public Integer[] getExpectedBulkResponseTypes() {
        return new Integer[] { LcConfigXResponse.TYPE, LcNotAvailableResponse.TYPE };
    }
}
