package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.RcPlusFeedbackBindData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.enumeration.RcPlusOpCodeBm;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackRcPlusResponse extends BidibMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackRcPlusResponse.class);

    public static final Integer TYPE = BidibLibrary.MSG_BM_RCPLUS;

    FeedbackRcPlusResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 1) {
            throw new ProtocolException("No valid MSG_BM_RCPLUS received.");
        }
    }

    public FeedbackRcPlusResponse(byte[] addr, int num, int detectorAddress) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_RCPLUS, ByteUtils.getLowByte(detectorAddress));
    }

    public String getName() {
        return "MSG_BM_RCPLUS";
    }

    public int getDetectorNumber() {
        return getData()[0];
    }

    /**
     * @return the opCode of the message
     */
    public RcPlusOpCodeBm getOpCode() {
        return RcPlusOpCodeBm.valueOf(getData()[1]);
    }

    public DecoderUniqueIdData getRcPlusUniqueId() {

        byte[] data = getData();
        DecoderUniqueIdData uniqueIdData = null;
        switch (getOpCode()) {
            case RC_FIND_COLLISION_P0:
            case RC_FIND_COLLISION_P1:
            case RC_PONG_OKAY_LOCO_P0:
            case RC_PONG_OKAY_LOCO_P1:
            case RC_PONG_OKAY_ACCESSORY_P0:
            case RC_PONG_OKAY_ACCESSORY_P1:
            case RC_PONG_NEW_LOCO_P0:
            case RC_PONG_NEW_LOCO_P1:
            case RC_PONG_NEW_ACCESSORY_P0:
            case RC_PONG_NEW_ACCESSORY_P1:
            case RC_BIND_ACCEPTED_LOCO:
            case RC_BIND_ACCEPTED_ACCESSORY:
                uniqueIdData = DecoderUniqueIdData.fromByteArray(data, 2);
                break;
            default:
                LOGGER.warn("The current opCode is not RC_FIND_COLLISION or RC_PONG_OKAY/NEW or BIND_ACCEPTED!");
                break;
        }
        return uniqueIdData;
    }

    public AddressData getAddress() {
        byte[] data = getData();
        AddressData addressData = null;
        switch (getOpCode()) {
            case RC_BIND_ACCEPTED_LOCO:
            case RC_BIND_ACCEPTED_ACCESSORY:
                addressData = AddressData.fromByteArray(data, 2 + 5);
                break;
            default:
                LOGGER.warn("The current opCode is not RC_BIND_ACCEPTED!");
                break;
        }
        return addressData;
    }

    public RcPlusFeedbackBindData getRcPlusFeedbackBind() {
        byte[] data = getData();
        RcPlusFeedbackBindData bindData = null;
        switch (getOpCode()) {
            case RC_BIND_ACCEPTED_LOCO:
            case RC_BIND_ACCEPTED_ACCESSORY:
                bindData = RcPlusFeedbackBindData.fromByteArray(data, 2);
                break;
            default:
                LOGGER.warn("The current opCode is not RC_BIND_ACCEPTED!");
                break;
        }
        return bindData;
    }
}
