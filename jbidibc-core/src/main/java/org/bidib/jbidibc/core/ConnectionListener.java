package org.bidib.jbidibc.core;

public interface ConnectionListener {

    /**
     * The communication was opened.
     * 
     * @param port
     *            the port identifier
     */
    void opened(String port);

    /**
     * The communication was closed.
     * 
     * @param port
     *            the port identifier
     */
    void closed(String port);

    /**
     * The communication provided a status message.
     * 
     * @param messageKey
     *            the message key
     */
    void status(String messageKey);
}
