package org.bidib.jbidibc.debug;

public interface DebugMessageListener {

    /**
     * A debug message was received.
     * 
     * @param message
     *            the message
     */
    void debugMessage(String message);
}
