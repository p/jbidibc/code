package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TimeBaseUnitEnumTest {

    @Test
    public void valueOfTest() {

        byte value = ByteUtils.getLowByte(0x80);
        Assert.assertEquals(TimeBaseUnitEnum.valueOf(ByteUtils.getLowByte(ByteUtils.getBit(value, 7))),
            TimeBaseUnitEnum.UNIT_1S);

        Assert.assertEquals(TimeBaseUnitEnum.valueOf(ByteUtils.getBit(0x81, 7)), TimeBaseUnitEnum.UNIT_1S);
    }

    @Test
    public void valueOf100msTest() {

        byte value = ByteUtils.getLowByte(0x08);
        Assert.assertEquals(TimeBaseUnitEnum.valueOf(ByteUtils.getLowByte(ByteUtils.getBit(value, 7))),
            TimeBaseUnitEnum.UNIT_100MS);

        Assert.assertEquals(TimeBaseUnitEnum.valueOf(ByteUtils.getBit(0x08, 7)), TimeBaseUnitEnum.UNIT_100MS);
    }
}
