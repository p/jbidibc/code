package org.bidib.jbidibc.ch341a;

public interface AT24CX {

    // AT24Cx I2C adress
    // 80
    // 0x50
    // B1010000
    public static final int AT24CX_ID = 0x50;

    /**
     * Open CH341A device with the provided index. For the first device use {@code 0}.
     * 
     * @param ch341Index
     *            the CH341 device index
     */
    void open(long ch341Index);

    /**
     * Close the CH341A device.
     */
    void close();

    /**
     * Read a byte from memory at address.
     * 
     * @param memAddress
     *            the memory address
     * @return the bye
     */
    public byte read(int memAddress);

    public byte[] read(int address, int size);

}
