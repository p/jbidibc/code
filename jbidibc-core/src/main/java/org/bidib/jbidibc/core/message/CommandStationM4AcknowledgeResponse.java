package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.M4OpCodesAck;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Response from command station with the pom acknowledge state
 */
public class CommandStationM4AcknowledgeResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_CS_M4_ACK;

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationM4AcknowledgeResponse.class);

    CommandStationM4AcknowledgeResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 1) {
            throw new ProtocolException("No valid MSG_CS_M4_ACK received.");
        }

        LOGGER.debug("Received MSG_CS_M4_ACK response, opCode: {}", getOpCode());
    }

    public String getName() {
        return "MSG_CS_M4_ACK";
    }

    /**
     * @return the opCode of the message
     */
    public M4OpCodesAck getOpCode() {
        return M4OpCodesAck.valueOf(getData()[0]);
    }

    /**
     * @return the tid data
     */
    public TidData getTid() {
        byte[] data = getData();

        if (!M4OpCodesAck.M4_TID.equals(getOpCode())) {
            LOGGER.warn("The current opCode is not M4_TID!");
            return null;
        }
        return TidData.fromByteArray(data, 1);
    }

    public Integer getInterval(M4OpCodesAck expectedOpCode) {
        byte[] data = getData();

        if (!expectedOpCode.equals(getOpCode())) {
            LOGGER.warn("The current opCode is not {}!", expectedOpCode);
            return null;
        }
        Integer beaconInterval = Integer.valueOf(ByteUtils.getInt(data[1]));
        LOGGER.info("The returned beaconInterval: {}", beaconInterval);
        return beaconInterval;
    }

    public Integer getDid(M4OpCodesAck expectedOpCode) {
        if (!expectedOpCode.equals(getOpCode())) {
            LOGGER.warn("The current opCode is not {}!", expectedOpCode);
            return null;
        }

        byte[] data = getData();

        int value = 0;
        for (int index = 1; index < 5; index++) {
            value = value << 8 | data[index];
        }

        LOGGER.info("Prepared the DID value: {} (0x{})", value, ByteUtils.int32ToHex(value));

        return value;
    }
}
