package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class SysPongResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_SYS_PONG;

    SysPongResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 1) {
            throw new ProtocolException("No valid MSG_SYS_PONG received.");
        }
    }

    public SysPongResponse(byte[] addr, int num, byte marker) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_SYS_PONG, new byte[] { marker });
    }

    public String getName() {
        return "MSG_SYS_PONG";
    }

    public int getMarker() {
        return ByteUtils.getInt(getData()[0]);
    }
}
