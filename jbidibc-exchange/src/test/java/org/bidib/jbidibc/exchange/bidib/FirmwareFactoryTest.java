package org.bidib.jbidibc.exchange.bidib;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.bidib.jbidibc.exchange.bidib.FirmwareFactory.NodetextUtils;
import org.bidib.jbidibc.exchange.firmware.DeviceNode;
import org.bidib.jbidibc.exchange.firmware.FilenameType;
import org.bidib.jbidibc.exchange.firmware.Firmware;
import org.bidib.jbidibc.exchange.firmware.FirmwareDefinitionType;
import org.bidib.jbidibc.exchange.firmware.FirmwareNode;
import org.bidib.jbidibc.exchange.firmware.NodeType;
import org.bidib.jbidibc.exchange.firmware.SimpleNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FirmwareFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareFactoryTest.class);

    @Test
    public void loadFirmwareDefinition() {

        URL url = FirmwareFactoryTest.class.getResource("/xml-test/firmware-sample.xml");
        Firmware firmware = FirmwareFactory.getFirmware(url.getPath());
        Assert.assertNotNull(firmware);
        Assert.assertNotNull(firmware.getFirmwareDefinition());

        FirmwareDefinitionType firmwareDefinition = firmware.getFirmwareDefinition();

        Assert.assertNotNull(firmwareDefinition.getNode());

        List<NodeType> nodes = firmwareDefinition.getNode();

        NodeType node0 = nodes.get(0);
        Assert.assertNotNull(node0);
        Assert.assertTrue(node0 instanceof DeviceNode);

        DeviceNode deviceNode = (DeviceNode) node0;
        Assert.assertEquals(deviceNode.getNodetext().size(), 2);
        Assert.assertEquals(deviceNode.getNode().size(), 3);
        Assert.assertEquals(deviceNode.getVID(), "013");
        Assert.assertEquals(deviceNode.getPID(), "201");
        // we expect comment
        Assert.assertNotNull(deviceNode.getComment(), "Comment expected!");

        Assert.assertTrue(node0.getNode().get(0) instanceof FirmwareNode);
        Assert.assertTrue(node0.getNode().get(1) instanceof FirmwareNode);
        Assert.assertTrue(node0.getNode().get(2) instanceof SimpleNode);
    }

    @Test
    public void loadFirmwareDefinitionMinimum() {

        URL url = FirmwareFactoryTest.class.getResource("/xml-test/firmware-minimum.xml");
        Firmware firmware = FirmwareFactory.getFirmware(url.getPath());
        Assert.assertNotNull(firmware);
        Assert.assertNotNull(firmware.getFirmwareDefinition());

        FirmwareDefinitionType firmwareDefinition = firmware.getFirmwareDefinition();

        Assert.assertNotNull(firmwareDefinition.getNode());

        List<NodeType> nodes = firmwareDefinition.getNode();

        NodeType node0 = nodes.get(0);
        Assert.assertNotNull(node0);
        Assert.assertTrue(node0 instanceof DeviceNode);

        DeviceNode deviceNode = (DeviceNode) node0;
        Assert.assertEquals(deviceNode.getNodetext().size(), 2);
        Assert.assertEquals(deviceNode.getNode().size(), 3);
        Assert.assertEquals(deviceNode.getVID(), "013");
        Assert.assertEquals(deviceNode.getPID(), "201");
        Assert.assertNull(deviceNode.getComment(), "No comment expected!");

        Assert.assertTrue(node0.getNode().get(0) instanceof FirmwareNode);
        Assert.assertTrue(node0.getNode().get(1) instanceof FirmwareNode);
        Assert.assertTrue(node0.getNode().get(2) instanceof SimpleNode);
    }

    @Test
    public void loadFirmwareDefinitionBasic() {

        URL url = FirmwareFactoryTest.class.getResource("/xml-test/firmware-basic.xml");
        Firmware firmware = FirmwareFactory.getFirmware(url.getPath());
        Assert.assertNotNull(firmware);
        Assert.assertNotNull(firmware.getFirmwareDefinition());

        FirmwareDefinitionType firmwareDefinition = firmware.getFirmwareDefinition();

        Assert.assertNotNull(firmwareDefinition.getNode());

        List<NodeType> nodes = firmwareDefinition.getNode();
        Assert.assertNotNull(nodes);
        Assert.assertEquals(nodes.size(), 2);

        NodeType node0 = nodes.get(0);
        Assert.assertNotNull(node0);
        Assert.assertTrue(node0 instanceof DeviceNode);

        DeviceNode deviceNode = (DeviceNode) node0;
        Assert.assertEquals(deviceNode.getNodetext().size(), 2);

        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "de-DE"), "StepControl: Neuinstallation");
        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "en-EN"),
            "StepControl: Initial installation");

        Assert.assertEquals(deviceNode.getNode().size(), 3);
        Assert.assertEquals(deviceNode.getVID(), "013");
        Assert.assertEquals(deviceNode.getPID(), "201");
        Assert.assertNull(deviceNode.getComment(), "No comment expected!");

        // get the update node
        deviceNode = (DeviceNode) nodes.get(1);
        Assert.assertEquals(deviceNode.getNodetext().size(), 2);

        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "de-DE"),
            "StepControl: Update Installation");
        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "en-EN"),
            "StepControl: Update installation");

    }

    @Test
    public void loadFirmwareFileCaseInsensitive() throws URISyntaxException {
        LOGGER.info("loadFirmwareFileCaseInsensitive");

        URL url = FirmwareFactoryTest.class.getResource("/bidib-test/OneOC-2.00.00.zip");
        Firmware firmware = FirmwareFactory.getFirmware(url.getPath());
        Assert.assertNotNull(firmware);

        Assert.assertNotNull(firmware.getFirmwareDefinition());
        Assert.assertNotNull(firmware.getFirmwareDefinition().getNode());
        Assert.assertEquals(firmware.getFirmwareDefinition().getNode().size(), 2);

        Assert.assertTrue(firmware.getFirmwareDefinition().getNode().get(0) instanceof DeviceNode);

        DeviceNode deviceNode = (DeviceNode) firmware.getFirmwareDefinition().getNode().get(0);

        Assert.assertNotNull(deviceNode.getNode());
        Assert.assertEquals(deviceNode.getNode().size(), 2);

        Assert.assertTrue(deviceNode.getNode().get(0) instanceof FirmwareNode);

        FirmwareNode firmwareNode = (FirmwareNode) deviceNode.getNode().get(0);

        Assert.assertEquals(firmwareNode.getDestinationNumber(), 0);
        Assert.assertEquals(firmwareNode.getFilename(), "OneOC_STD_2.00.00.000.hex");

        File firmwareFile = new File(url.toURI());
        List<String> content = FirmwareFactory.getFirmwareContent(firmwareFile, firmwareNode.getFilename());

        Assert.assertNotNull(content);
        Assert.assertEquals(content.size(), 1523);
    }

    @Test
    public void loadFirmwareDefinitionDefaultLabels() {

        URL url = FirmwareFactoryTest.class.getResource("/xml-test/firmware-default-labels.xml");
        Firmware firmware = FirmwareFactory.getFirmware(url.getPath());
        Assert.assertNotNull(firmware);
        Assert.assertNotNull(firmware.getFirmwareDefinition());

        FirmwareDefinitionType firmwareDefinition = firmware.getFirmwareDefinition();

        Assert.assertNotNull(firmwareDefinition.getNode());
        Assert.assertNotNull(firmwareDefinition.getDefaultLabels());

        List<NodeType> nodes = firmwareDefinition.getNode();
        Assert.assertNotNull(nodes);
        Assert.assertEquals(nodes.size(), 2);

        NodeType node0 = nodes.get(0);
        Assert.assertNotNull(node0);
        Assert.assertTrue(node0 instanceof DeviceNode);

        DeviceNode deviceNode = (DeviceNode) node0;
        Assert.assertEquals(deviceNode.getNodetext().size(), 2);

        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "de-DE"), "StepControl: Neuinstallation");
        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "en-EN"),
            "StepControl: Initial installation");

        Assert.assertEquals(deviceNode.getNode().size(), 3);
        Assert.assertEquals(deviceNode.getVID(), "013");
        Assert.assertEquals(deviceNode.getPID(), "201");
        Assert.assertNull(deviceNode.getComment(), "No comment expected!");

        // get the update node
        deviceNode = (DeviceNode) nodes.get(1);
        Assert.assertEquals(deviceNode.getNodetext().size(), 2);

        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "de-DE"),
            "StepControl: Update Installation");
        Assert.assertEquals(NodetextUtils.getText(deviceNode.getNodetext(), "en-EN"),
            "StepControl: Update installation");

        Assert.assertNotNull(firmwareDefinition.getDefaultLabels().getDefaultLabelsFile());
        Assert.assertEquals(firmwareDefinition.getDefaultLabels().getDefaultLabelsFile().size(), 2);

        FilenameType labelFileName = firmwareDefinition.getDefaultLabels().getDefaultLabelsFile().get(0);
        Assert.assertEquals(labelFileName.getLang(), "de-DE");
        Assert.assertEquals(labelFileName.getFilename(), "bidib-default-names-13-201-de.xml");

        labelFileName = firmwareDefinition.getDefaultLabels().getDefaultLabelsFile().get(1);
        Assert.assertEquals(labelFileName.getLang(), "en-EN");
        Assert.assertEquals(labelFileName.getFilename(), "bidib-default-names-13-201-en.xml");
    }

}
