package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SysErrorResponseTest {

    @Test
    public void getErrorCode() throws ProtocolException {
        byte[] data = new byte[] { 1, 2 };
        SysErrorResponse resp = new SysErrorResponse(new byte[] { 1 }, 1, SysErrorEnum.BIDIB_ERR_SEQUENCE, data);

        Assert.assertEquals(resp.getType(), ByteUtils.getLowByte(SysErrorResponse.TYPE.intValue()));
        Assert.assertEquals(resp.getErrorCode(), BidibLibrary.BIDIB_ERR_SEQUENCE);
        Assert.assertNotNull(resp.getReasonData());
        Assert.assertTrue(resp.getReasonData().length == 2);

        Assert.assertEquals(resp.getReasonData()[0], ByteUtils.getLowByte(1));
        Assert.assertEquals(resp.getReasonData()[1], ByteUtils.getLowByte(2));
    }
}
