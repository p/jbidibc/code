package org.bidib.jbidibc.core.bidib;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Set;

import org.bidib.jbidibc.core.AbstractBidib;
import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.node.listener.TransferListener;

public class DummyBidib extends AbstractBidib {

    public static BidibInterface createInstance() {

        return new DummyBidib();
    }

    @Override
    protected BidibMessageProcessor createMessageReceiver(NodeRegistry nodeFactory) {
        return null;
    }

    @Override
    public void sendData(ByteArrayOutputStream data) {

    }

    @Override
    public void open(
        String portName, ConnectionListener connectionListener, Set<NodeListener> nodeListeners,
        Set<MessageListener> messageListeners, Set<TransferListener> transferListeners, Context context)
        throws PortNotFoundException, PortNotOpenedException {

    }

    @Override
    public boolean isOpened() {
        return false;
    }

    @Override
    public void close() {
        cleanupAfterClose(getMessageReceiver());
    }

    @Override
    public List<String> getPortIdentifiers() {
        return null;
    }
}
