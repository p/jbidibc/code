package org.bidib.jbidibc.net.exception;

public class ClientNotAcceptedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ClientNotAcceptedException(String message) {
        super(message);
    }
}
