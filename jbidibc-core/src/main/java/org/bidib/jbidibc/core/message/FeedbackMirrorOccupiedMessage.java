package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class FeedbackMirrorOccupiedMessage extends BidibCommandMessage {
    public FeedbackMirrorOccupiedMessage(int detectorNumber) {
        super(0, BidibLibrary.MSG_BM_MIRROR_OCC, ByteUtils.getLowByte(detectorNumber));
    }

    public FeedbackMirrorOccupiedMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_BM_MIRROR_OCC";
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }
}
