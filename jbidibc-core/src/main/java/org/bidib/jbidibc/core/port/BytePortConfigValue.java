package org.bidib.jbidibc.core.port;

import org.bidib.jbidibc.core.utils.ByteUtils;

public class BytePortConfigValue implements PortConfigValue<Byte> {

    private final Byte value;

    public BytePortConfigValue(Byte value) {
        this.value = value;
    }

    @Override
    public Byte getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof BytePortConfigValue)) {
            return false;
        }
        BytePortConfigValue other = (BytePortConfigValue) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        }
        else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BytePortConfigValue:");
        sb.append(ByteUtils.byteToHex(value));
        return sb.toString();
    }
}
