package org.bidib.jbidibc.decoder.decoderdb;

import org.bidib.jbidibc.decoder.schema.decoderdetection.DecoderDetection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DecoderDetectionAccess extends AbstractDecoderDbAccess {
    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderDetectionAccess.class);

    public DecoderDetection fetch(String login, char[] password) {

        DecoderDetection decoderDetection =
            fetch(login, password, REST_SERVICE_URI, "/?decoderdetection", DecoderDetection.class);

        LOGGER.info("Retrieved decoderDetection: {}", decoderDetection);

        return decoderDetection;
    }

}
