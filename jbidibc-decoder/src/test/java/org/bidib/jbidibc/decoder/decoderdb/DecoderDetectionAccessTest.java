package org.bidib.jbidibc.decoder.decoderdb;

import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.decoder.schema.commontypes.ProtocolTypeType;
import org.bidib.jbidibc.decoder.schema.decoderdetection.DecoderDetection;
import org.bidib.jbidibc.decoder.schema.decoderdetection.DecoderDetectionProtocolType;
import org.bidib.jbidibc.decoder.schema.decoderdetection.ManufacturerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DecoderDetectionAccessTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderDetectionAccessTest.class);

    @Test
    public void fetch() {
        LOGGER.info("Fetch decoderDetection from DecoderDB.");

        DecoderDetectionAccess decoderDetectionAccess = new DecoderDetectionAccess();
        DecoderDetection decoderDetection = decoderDetectionAccess.fetch("cv", "cv".toCharArray());

        LOGGER.info("Retrieved decoderDetection: {}", decoderDetection);

        Assert.assertNotNull(decoderDetection);

        Assert.assertNotNull(decoderDetection.getProtocols());
        Assert.assertNotNull(decoderDetection.getProtocols().getProtocol());
        Assert.assertFalse(decoderDetection.getProtocols().getProtocol().isEmpty());

        // find DCC protocol

        List<DecoderDetectionProtocolType> protocols = decoderDetection.getProtocols().getProtocol();
        DecoderDetectionProtocolType dccProtocol =
            IterableUtils.find(protocols, new Predicate<DecoderDetectionProtocolType>() {

                @Override
                public boolean evaluate(DecoderDetectionProtocolType protocol) {
                    return ProtocolTypeType.DCC.equals(protocol.getType());
                }
            });

        Assert.assertNotNull(dccProtocol);

        // find the DIY manufacturer
        final short ID = 13;

        List<ManufacturerType> manufacturers = dccProtocol.getManufacturer();
        ManufacturerType manufacturerDiy = IterableUtils.find(manufacturers, new Predicate<ManufacturerType>() {

            @Override
            public boolean evaluate(ManufacturerType manufacturer) {
                return manufacturer.getId() == ID;
            }
        });

        Assert.assertNotNull(manufacturerDiy);
        LOGGER.info("Found DIY manufacturer: {}", manufacturerDiy);

        // find the OpenCar manufacturer
        final Integer EXTENDED_ID_OPENCAR = Integer.valueOf(257);

        ManufacturerType manufacturerOpenDcc = IterableUtils.find(manufacturers, new Predicate<ManufacturerType>() {

            @Override
            public boolean evaluate(ManufacturerType manufacturer) {
                return manufacturer.getId() == ID && EXTENDED_ID_OPENCAR.equals(manufacturer.getExtendedId());
            }
        });

        Assert.assertNotNull(manufacturerOpenDcc);
        LOGGER.info("Found OpenCar manufacturer: {}", manufacturerOpenDcc);
    }
}
