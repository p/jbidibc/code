package org.bidib.jbidibc.decoder.schema.decoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DecoderDefinitionFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderDefinitionFactoryTest.class);

    @Test
    public void loadDecoderDefinitionTest() {
        String filePath = "/decoder/97/Decoder_97_DH05C.xml";

        filePath = DecoderDefinitionFactoryTest.class.getResource(filePath).getPath();
        LOGGER.info("Load productDefinition from path: {}", filePath);

        DecoderDefinitionFactory factory = new DecoderDefinitionFactory();
        DecoderDefinition decoderDefinition = factory.loadDecoderDefinition(filePath);

        Assert.assertNotNull(decoderDefinition);
        DecoderType decoder = decoderDefinition.getDecoder();
        Assert.assertNotNull(decoder);

        Assert.assertEquals(decoder.getName(), "DH05C");
    }
}
