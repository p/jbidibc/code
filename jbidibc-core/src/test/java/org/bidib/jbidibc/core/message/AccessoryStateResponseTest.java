package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.AccessoryStateOptions;
import org.bidib.jbidibc.core.AccessoryStateOptions.Options;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.accessory.ByteOptionsValue;
import org.bidib.jbidibc.core.accessory.OptionsValue;
import org.bidib.jbidibc.core.enumeration.AccessoryStateOptionsKeys;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AccessoryStateResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryStateResponseTest.class);

    @Test
    public void getAccessoryState() throws ProtocolException {
        byte[] message =
            new byte[] { 0x09, 0x01, 0x00, (byte) 0x79, (byte) BidibLibrary.MSG_ACCESSORY_STATE, 0x02, 0x01, 0x03,
                (byte) 0x81, BidibLibrary.BIDIB_ACC_STATE_ERROR_SERVO };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_ACCESSORY_STATE);

        AccessoryStateResponse accessoryStateResponse = (AccessoryStateResponse) bidibMessage;
        LOGGER.info("accessoryStateResponse: {}", accessoryStateResponse);

        Assert.assertNotNull(accessoryStateResponse.getAccessoryState());
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAccessoryNumber(), 2);
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAspect(), ByteUtils.getLowByte(1));
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getErrorCode(),
            Integer.valueOf(BidibLibrary.BIDIB_ACC_STATE_ERROR_SERVO));

    }

    @Test
    public void getAccessoryStateWait() throws ProtocolException {
        byte[] message =
            new byte[] { 0x09, 0x01, 0x00, (byte) 0x79, (byte) BidibLibrary.MSG_ACCESSORY_STATE, 0x02, 0x01, 0x03,
                (byte) 0x01, (byte) 0x83 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_ACCESSORY_STATE);

        AccessoryStateResponse accessoryStateResponse = (AccessoryStateResponse) bidibMessage;
        LOGGER.info("accessoryStateResponse: {}", accessoryStateResponse);

        Assert.assertNotNull(accessoryStateResponse.getAccessoryState());
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAccessoryNumber(), 2);
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAspect(), ByteUtils.getLowByte(1));
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getWait(), 3000 /* ms */);
    }

    @Test
    public void getAccessoryStateStepControl() throws ProtocolException {
        byte[] message =
            new byte[] { 0x0F, 0x01, 0x00, (byte) 0xC6, (byte) BidibLibrary.MSG_ACCESSORY_STATE, 0x00, 0x21, 0x30,
                (byte) 0x00, (byte) 0x00, 0x01 /* CURRENT ANGLE */, (byte) 0xA5, 0x02 /* TARGET ANGLE */, (byte) 0xA5,
                0x55 /* kufer marker */, (byte) 0xA5 };
        // 0F 01 00 C6 B8 00 21 30 00 00 00 A5 01 A5 55 A5

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_ACCESSORY_STATE);

        AccessoryStateResponse accessoryStateResponse = (AccessoryStateResponse) bidibMessage;
        LOGGER.info("accessoryStateResponse: {}", accessoryStateResponse);

        Assert.assertNotNull(accessoryStateResponse.getAccessoryState());
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAccessoryNumber(), 0);
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAspect(), 33);
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getWait(), 0 /* ms */);

        AccessoryStateOptions accessoryStateOptions = accessoryStateResponse.getAccessoryStateOptions();
        Assert.assertNotNull(accessoryStateOptions);

        Options stateOptions = accessoryStateOptions.getOptions();
        Assert.assertNotNull(stateOptions);

        Assert.assertEquals(
            stateOptions.getOptionsValue(Byte.valueOf(AccessoryStateOptionsKeys.STATE_OPTION_CURRENT_ANGLE.getType())),
            ByteUtils.getLowByte(Integer.valueOf(0xA5)));
        Assert.assertEquals(
            stateOptions.getOptionsValue(Byte.valueOf(AccessoryStateOptionsKeys.STATE_OPTION_TARGET_ANGLE.getType())),
            ByteUtils.getLowByte(Integer.valueOf(0xA5)));

        OptionsValue<?> optionsValue =
            stateOptions.getOptionsValue(AccessoryStateOptionsKeys.STATE_OPTION_CURRENT_ANGLE);
        Assert.assertEquals(ByteOptionsValue.getIntValue(optionsValue), 0xA5);

    }

    @Test
    public void getAccessoryStateStepControl2() throws ProtocolException {
        byte[] message =
            new byte[] { 0x0F, 0x01, 0x00, (byte) 0xCA, (byte) BidibLibrary.MSG_ACCESSORY_STATE, 0x00, (byte) 0xFF,
                0x30, (byte) 0x00, (byte) 0x00, 0x01 /* CURRENT ANGLE */, (byte) 0xCC, 0x02 /* TARGET ANGLE */,
                (byte) 0x05, 0x55 /* kufer marker */, (byte) 0xCC };

        // 0F 01 00 CA B8 00 FF 30 00 00 00 CC 01 05 55 CC

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_ACCESSORY_STATE);

        AccessoryStateResponse accessoryStateResponse = (AccessoryStateResponse) bidibMessage;
        LOGGER.info("accessoryStateResponse: {}", accessoryStateResponse);

        Assert.assertNotNull(accessoryStateResponse.getAccessoryState());
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAccessoryNumber(), 0);
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getAspect(), 255);
        Assert.assertEquals(accessoryStateResponse.getAccessoryState().getWait(), 0 /* ms */);

        AccessoryStateOptions accessoryStateOptions = accessoryStateResponse.getAccessoryStateOptions();
        Assert.assertNotNull(accessoryStateOptions);

        Options stateOptions = accessoryStateOptions.getOptions();
        Assert.assertNotNull(stateOptions);

        Assert.assertEquals(
            stateOptions.getOptionsValue(Byte.valueOf(AccessoryStateOptionsKeys.STATE_OPTION_CURRENT_ANGLE.getType())),
            ByteUtils.getLowByte(Integer.valueOf(0xCC)));
        Assert.assertEquals(
            stateOptions.getOptionsValue(Byte.valueOf(AccessoryStateOptionsKeys.STATE_OPTION_TARGET_ANGLE.getType())),
            ByteUtils.getLowByte(Integer.valueOf(0x05)));
    }
}
