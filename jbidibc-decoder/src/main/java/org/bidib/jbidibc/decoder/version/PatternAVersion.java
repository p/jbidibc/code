package org.bidib.jbidibc.decoder.version;

public class PatternAVersion extends AbstractDecoderVersion {

    public PatternAVersion(String pattern) {
        super(pattern);
    }

    @Override
    public Integer getMinorVersion() {
        return null;
    }

    @Override
    public Integer getMicroVersion() {
        return null;
    }

    public void parse(String versionString) {

        setMajorVersion(Integer.parseInt(versionString.trim()));
    }

}
