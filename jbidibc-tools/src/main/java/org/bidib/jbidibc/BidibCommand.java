package org.bidib.jbidibc;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.bidib.jbidibc.core.BidibFactory;
import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.helpers.DefaultContext;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.rxtx.RxtxSerialBidib;
import org.bidib.jbidibc.simulation.comm.SimulationBidib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

@Parameters(separators = "=")
public abstract class BidibCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibCommand.class);

    @Parameter(names = {
        "-port" }, description = "Port to use, e.g. COM1. For simulation purposes use 'sim' to start the simulation.", required = true)
    private String portName;

    private Set<TransferListener> transferListeners = new LinkedHashSet<TransferListener>();

    private BidibInterface instance;

    protected String getPortName() {
        return portName;
    }

    /**
     * @return the connected BiDiB instance of {@code null} if connection is not established
     */
    protected BidibInterface getBidib() {
        return instance;
    }

    protected BidibCommand() {
        // redirect System.out and System.error calls to SLF4J
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LogLevel.INFO, LogLevel.WARN);
    }

    protected void openPort(String portName, Context context) throws PortNotFoundException, PortNotOpenedException {

        if (context == null) {
            // logger create empty context
            context = new DefaultContext();
        }

        transferListeners.add(new TransferListener() {

            @Override
            public void sendStopped() {
                // no implementation
            }

            @Override
            public void sendStarted() {
                // no implementation
            }

            @Override
            public void receiveStopped() {
                // no implementation
            }

            @Override
            public void receiveStarted() {
                // no implementation
            }

            @Override
            public void ctsChanged(boolean cts) {
                // no implementation
            }
        });

        BidibInterface bidib = null;
        if (portName.toLowerCase().startsWith("sim")) {
            LOGGER.info("Create simulation instance of bidib.");
            bidib = SimulationBidib.createInstance();
        }
        else {
            LOGGER.info("Create RXTX instance of bidib.");
            bidib = BidibFactory.createBidib(RxtxSerialBidib.class.getName());
        }

        bidib.open(portName, new ConnectionListener() {
            @Override
            public void opened(String port) {
                // no implementation
            }

            @Override
            public void closed(String port) {
                // no implementation
            }

            @Override
            public void status(String messageKey) {
                // no implementation
            }
        }, Collections.<NodeListener> emptySet(), Collections.<MessageListener> emptySet(), transferListeners, context);

        instance = bidib;
    }

    protected void releaseBidib() {

        if (instance != null) {
            instance.close();
        }

        instance = null;
    }

    /**
     * Execute the command
     * 
     * @return the exit code
     */
    public abstract int execute();

    public static void run(BidibCommand command, String[] args) {

        JCommander jc = null;
        int result = 20;
        try {
            jc = new JCommander(command);
            jc.setProgramName(command.getClass().getName());
            jc.parse(args);

            result = command.execute();
        }
        catch (ParameterException ex) {
            LOGGER.warn("Execution of " + command.getClass().getSimpleName() + " command failed: " + ex.getMessage());
            StringBuilder sb = new StringBuilder();
            jc.usage(sb);
            LOGGER.warn(sb.toString());
        }
        System.exit(result);
    }

}
