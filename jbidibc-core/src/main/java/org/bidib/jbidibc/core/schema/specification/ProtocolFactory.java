package org.bidib.jbidibc.core.schema.specification;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.IOUtils;
import org.bidib.jbidibc.core.schema.BidibFactory;
import org.bidib.jbidibc.core.schema.bidib2.protocol.FeatureCode;
import org.bidib.jbidibc.core.schema.bidib2.protocol.MessageType;
import org.bidib.jbidibc.core.schema.bidib2.protocol.Protocol;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import net.sf.saxon.lib.FeatureKeys;

public class ProtocolFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProtocolFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.bidib2.protocol";

    public static final String XSD_LOCATION_BASE = "/xsd/bidib2BaseTypes.xsd";

    public static final String XSD_LOCATION_COMMON = "/xsd/bidib2Common.xsd";

    public static final String XSD_LOCATION = "/xsd/bidib2Protocol.xsd";

    public static final String JAXB_SCHEMA_LOCATION =
        "http://www.bidib.org/schema/bidib/2.0/protocol xsd/bidib2Protocol.xsd";

    private static JAXBContext jaxbContext;

    // cache the protocol files
    private static Map<String, Protocol> protocolCache = new HashMap<>();

    public ProtocolFactory() {
    }

    public static List<MessageType> getMessageTypes() {

        return new ProtocolFactory().loadMessageTypes();
    }

    public static List<FeatureCode> getFeatureCodes() {

        return new ProtocolFactory().loadFeatureCodes();
    }

    protected List<FeatureCode> loadFeatureCodes() {
        final String fileName = "/xml/specification/Protocol.bidib";

        Protocol protocol = null;
        synchronized (protocolCache) {
            protocol = protocolCache.get(fileName);
            if (protocol == null) {
                protocol = loadProtocolFromStream(fileName);

                if (protocol != null) {
                    protocolCache.put(fileName, protocol);
                }
            }
        }

        if (protocol != null) {
            try {
                List<FeatureCode> featureCodes = protocol.getFeatureCodes().getFeatureCode();
                LOGGER.info("Loaded number of featureCodes: {}", featureCodes.size());

                return featureCodes;
            }
            catch (Exception ex) {
                LOGGER.warn("Get the feature codes failed.", ex);
            }
        }
        return null;
    }

    protected List<MessageType> loadMessageTypes() {
        final String fileName = "/xml/specification/Protocol.bidib";

        Protocol protocol = null;
        synchronized (protocolCache) {
            protocol = protocolCache.get(fileName);
            if (protocol == null) {
                protocol = loadProtocolFromStream(fileName);

                if (protocol != null) {
                    protocolCache.put(fileName, protocol);
                }
            }
        }

        if (protocol != null) {
            try {
                List<MessageType> messageTypes = protocol.getMessageTypes().getMessageType();
                LOGGER.info("Loaded number of messageTypes: {}", messageTypes.size());

                return messageTypes;
            }
            catch (Exception ex) {
                LOGGER.warn("Get the message types failed.", ex);
            }
        }
        return null;
    }

    private Protocol loadProtocolFromStream(String fileName) {
        InputStream is = BidibFactory.class.getResourceAsStream(fileName);
        if (is == null) {
            LOGGER.warn("Load protocol file failed.");
            return null;
        }

        Protocol protocol = null;
        try {
            protocol = loadProtocolFile(is);
        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load Protocol file failed due to schema violation. Try to transform.", ex);

            if (is != null) {
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }

            final String migrationXSL = "/xml/specification/protocol.xsl";

            final StringWriter outputXML = new StringWriter();
            try {
                is = BidibFactory.class.getResourceAsStream(fileName);
                transform(null, is, migrationXSL, outputXML);
            }
            catch (XMLStreamException | IOException e) {
                LOGGER.warn("Transform Protocol file failed", e);
            }

            // try to load after transformation
            LOGGER.info("Try to load the Protocol file after transformation.");
            try {
                is = new ByteArrayInputStream(outputXML.toString().getBytes(StandardCharsets.UTF_8));

                protocol = loadProtocolFile(is);
            }
            catch (Exception e1) {
                LOGGER.info("No protocol defintion found.");
            }
        }

        return protocol;

    }

    public Protocol loadProtocolFile(File protocolFile) throws FileNotFoundException {

        Protocol protocol = null;
        InputStream is = null;

        try {
            is = new FileInputStream(protocolFile);
            protocol = loadProtocolFile(is);
        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load Protocol file failed due to schema violation. Try to transform.", ex);

            if (is != null) {
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }

            StringBuilder transformed = new StringBuilder();

            final String migrationXSL = "/xml/specification/protocol.xsl";

            FileOutputStream fos = null;
            final StringWriter outputXML = new StringWriter();
            try {
                is = new FileInputStream(protocolFile);
                transform(null, is, migrationXSL, outputXML);

                LOGGER.info("Save transformed Protocol to file: {}", protocolFile.getAbsolutePath());

                // save the transformed bidib file
                fos = new FileOutputStream(protocolFile);
                IOUtils.write(transformed, fos);
            }
            catch (XMLStreamException | IOException e) {
                LOGGER.warn("Transform Protocol file failed: {}", protocolFile.getAbsolutePath(), e);
            }
            finally {
                if (fos != null) {
                    try {
                        fos.flush();
                        fos.close();
                    }
                    catch (Exception e1) {
                        LOGGER.warn("Close output stream failed.", e1);
                    }
                }
            }

            // try to load after transformation
            LOGGER.info("Try to load the Protocol file after transformation.");
            try {
                is = new FileInputStream(protocolFile);
                protocol = loadProtocolFile(is);
            }
            catch (FileNotFoundException e1) {
                LOGGER.info("No Products file found.");
            }
        }
        finally {
            if (is != null) {
                LOGGER.info("Close input stream.");
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }
        }
        return protocol;
    }

    private Source[] prepareSchemaStreamSources() {
        StreamSource streamSource = new StreamSource(ProtocolFactory.class.getResourceAsStream(XSD_LOCATION));
        StreamSource streamSourceBaseTypes =
            new StreamSource(ProtocolFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
        StreamSource streamSourceCommon =
            new StreamSource(ProtocolFactory.class.getResourceAsStream(XSD_LOCATION_COMMON));
        return new Source[] { streamSourceBaseTypes, streamSourceCommon, streamSource };
    }

    public Protocol loadProtocolFile(final InputStream is) {

        Protocol protocol = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", JAXB_PACKAGE);
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(prepareSchemaStreamSources());
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            try {
                JAXBElement<Protocol> jaxbElement =
                    (JAXBElement<Protocol>) unmarshaller.unmarshal(xmlr, Protocol.class);
                protocol = jaxbElement.getValue();
            }
            catch (UnmarshalException ex) {
                LOGGER.warn("Load content from file failed.", ex);
                if (ex.getLinkedException() instanceof SAXException) {
                    validate(is);
                    throw new InvalidSchemaException("Load Protocol from file failed");
                }
            }

        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from file failed.", ex);
        }
        return protocol;
    }

    private List<String> validate(final InputStream is) {
        List<String> errors = null;

        if (is instanceof FileInputStream) {
            FileInputStream fis = (FileInputStream) is;
            try {
                LOGGER.info("Try to set file position to 0.");
                fis.getChannel().position(0);
            }
            catch (IOException e) {
                LOGGER.warn("Set file position to 0 failed.", e);
            }
        }

        StreamSource inputStreamSource = new StreamSource(is);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();

        try {
            Schema schema = schemaFactory.newSchema(prepareSchemaStreamSources());

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);

            validator.validate(inputStreamSource);
        }
        catch (IOException ex) {
            LOGGER.warn("Validate failed.", ex);
        }
        catch (SAXException ex) {
            LOGGER.warn("Validate failed.", ex);
        }

        errors = errorHandler.getErrors();
        LOGGER.info("Found errors: {}", errors);

        return errors;
    }

    /**
     * Transform the data from source.
     * 
     * @param sourceXML
     *            the source data
     * @param migrationXSL
     *            the migration XSL
     * @param outputXML
     *            the output XML
     * @throws XMLStreamException
     * @throws IOException
     */
    public void transform(
        Map<String, String> params, final InputStream sourceXML, String migrationXSL, final StringWriter outputXML)
        throws XMLStreamException, IOException {

        LOGGER.info("perform find nodes with migrationXSL: {}", migrationXSL);

        InputStream inputXSL = null;
        InputStream is = null;
        try {
            inputXSL = ProtocolFactory.class.getResourceAsStream(migrationXSL);

            LOGGER.info("Prepared inputXSL: {}", inputXSL);

            // perform the transformation
            doTransform(params, inputXSL, sourceXML, outputXML);

            LOGGER.info("The generated XML document is:\r\n{}", outputXML);
        }
        catch (TransformerException ex) {
            // TODO: handle exception
            LOGGER.warn("Perform find nodes with transformation failed.", ex);
        }
        finally {

            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close inputStream failed.", ex);
                }
            }

            if (inputXSL != null) {
                try {
                    inputXSL.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close inputXSL stream failed.", ex);
                }
            }
        }

    }

    private static final String NAMESPACE_PREFIX = "{http://www.bidib.org/schema/labels}";

    protected static final String KEY_MIGRATIONXSL = "migrationXSL";

    protected void doTransform(
        Map<String, String> params, final InputStream inputXSL, final InputStream dataXML, final StringWriter outputXML)
        throws TransformerException {

        TransformerFactory factory = TransformerFactory.newInstance();
        // disable warning in saxon
        factory.setFeature(FeatureKeys.SUPPRESS_XSLT_NAMESPACE_CHECK, true);
        // prevent XXE
        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        }
        catch (IllegalArgumentException e) {
            LOGGER.warn("XSL transformer implementation doesn't support {} feature", XMLConstants.ACCESS_EXTERNAL_DTD);
        }

        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        }
        catch (IllegalArgumentException e) {
            LOGGER.warn("XSL transformer implementation doesn't support {} feature", XMLConstants.ACCESS_EXTERNAL_DTD);
        }

        StreamSource xslStream = new StreamSource(inputXSL);

        Transformer transformer = factory.newTransformer(xslStream);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

        if (MapUtils.isNotEmpty(params)) {

            for (Entry<String, String> param : params.entrySet()) {

                if (!KEY_MIGRATIONXSL.equals(param.getKey())) {
                    // set the transformation parameter
                    LOGGER.info("Set transformer parameter: {}", param);
                    transformer.setParameter(NAMESPACE_PREFIX + param.getKey(), param.getValue());
                }
                else {
                    LOGGER.info("Skip KEY_MIGRATIONXSL.");
                }
            }
        }

        StreamSource in = new StreamSource(dataXML);

        StreamResult out = new StreamResult(outputXML);

        transformer.transform(in, out);

        outputXML.flush();
    }

}
