package org.bidib.jbidib.pi.tools;

import org.bidib.jbidib.pi.BiDiBPiConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BiDiBPiStandalone {

    private static final Logger LOGGER = LoggerFactory.getLogger(BiDiBPiStandalone.class);

    public static void main(String[] args) {
        LOGGER.info("Standalone is started.");

        try (BiDiBPiConnector connector = new BiDiBPiConnector()) {

            connector.connect();

            LOGGER.info("Press Ctrl+C to terminate the loop.");
            // keep program running until user aborts (CTRL-C)
            while (true) {

                try {
                    Thread.sleep(500);
                }
                catch (InterruptedException e) {
                    LOGGER.warn("Wait was interrupted. Leave the loop!");

                    break;
                }
            }

            connector.disconnect();

            LOGGER.info("Standalone has finished.");
        }
        catch (Exception ex) {
            LOGGER.warn("Start the standalone pi connector failed.", ex);
        }
    }
}
