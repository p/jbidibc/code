package org.bidib.jbidibc.gateway;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.mockito.Mockito;
import org.testng.annotations.Test;

public class GatewayMessageReceiverTest {

    private ByteArrayOutputStream pack(byte[] data) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        output.write(data);
        return output;
    }

    @Test
    public void receiveMessageTest() throws IOException {
        byte[] address = new byte[] { 0 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        GatewayMessagePeer gatewayMessagePeer = Mockito.mock(GatewayMessagePeer.class);

        // prepare the object to test
        GatewayMessageReceiver receiver = new GatewayMessageReceiver(nodeFactory);
        receiver.setGatewayMessagePeer(gatewayMessagePeer);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data = new byte[] { 0x03, 0x00, 0x01, 0x0e, (byte) 0x46, (byte) 0xFE };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(1));

        receiver.receive(pack(data));

        // verify that error was called once
        Mockito.verify(gatewayMessagePeer, Mockito.times(1)).forwardMessage(Mockito.any(BidibCommand.class));
    }
}
