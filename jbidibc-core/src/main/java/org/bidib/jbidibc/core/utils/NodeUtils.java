package org.bidib.jbidibc.core.utils;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.schema.bidib2.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Unique ID: 1. Byte: Class ID Bit 7: interface node (has subnodes) Bit 6: feedback functions Bit 5: Bit 4: command
 * station functions Bit 3: command station programming functions Bit 2: accessory functions Bit 1: booster functions
 * Bit 0: switch functions
 * 
 */
public class NodeUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeUtils.class);

    // the interface address is always 0
    public static final int INTERFACE_ADDRESS = 0;

    public static final byte[] ROOT_ADDRESS = new byte[] { 0 };

    /**
     * Check if the addresses are equal.
     * 
     * @param addr1
     *            the first address
     * @param addr2
     *            the second address
     * @return the addresses are equal
     */
    public static boolean isAddressEqual(byte[] addr1, byte[] addr2) {

        return Arrays.equals(addr1, addr2);
    }

    /**
     * Convert a node address into an integer value. All byte parts are converted into an int value by shifting the
     * parts to their position in the int value.
     * 
     * @param address
     *            node address
     * 
     * @return integer value for that address
     */
    public static int convertAddress(byte[] address) {
        int result = 0;

        if (address != null) {
            for (int index = 0; index < address.length; index++) {
                result += address[index] << (index * 8);
            }
        }
        return result;
    }

    /**
     * Format the address in the format { {@code #?.#?.#?.#}.
     * 
     * @param address
     *            the address bytes
     * @return the formatted address
     */
    public static String formatAddress(byte[] address) {
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < address.length; index++) {
            sb.append(Byte.toString(address[index]));
            if (index < address.length - 1) {
                sb.append(".");
            }
        }
        return sb.toString();
    }

    /**
     * Format the provided address in long format #.#.#.#.
     * 
     * @param address
     *            the address
     * @return the formated string
     */
    public static String formatAddressLong(byte[] address) {
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < 4; index++) {
            if (index < address.length) {
                sb.append(Byte.toString(address[index]));
            }
            else {
                sb.append(Byte.toString((byte) 0));
            }
            if (index < 3) {
                sb.append(".");
            }
        }
        return sb.toString();
    }

    /**
     * @return returns the class part of the uniqueId
     */
    public static int getClass(long uniqueId) {
        return (int) ((uniqueId >> 48) & 0xFF);
    }

    /**
     * @return returns if node supports accessory functions
     */
    public static boolean hasAccessoryFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 4) == 4;
    }

    /**
     * @return returns if node supports booster functions
     */
    public static boolean hasBoosterFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 2) == 2;
    }

    /**
     * @return returns if node supports feedback functions
     */
    public static boolean hasFeedbackFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 0x40) == 0x40;
    }

    /**
     * @return returns if node supports feedback functions
     */
    public static long setHasFeedbackFunctions(long uniqueId) {
        long temp = (0x40L << 48);

        uniqueId = (uniqueId | temp);

        return uniqueId;
    }

    /**
     * @return returns if node supports subnodes functions
     */
    public static boolean hasSubNodesFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 128) == 128;
    }

    /**
     * @return returns if node supports switch functions
     */
    public static boolean hasSwitchFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 1) == 1;
    }

    /**
     * @return returns if node supports command station programming functions
     */
    public static boolean hasCommandStationProgrammingFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 8) == 8;
    }

    /**
     * @return returns if node supports command station functions
     */
    public static boolean hasCommandStationFunctions(long uniqueId) {
        return ((uniqueId >> 48) & 16) == 16;
    }

    /**
     * @param uniqueId
     *            the unique id of the node
     * @param pidBits
     *            the relevant PID bits
     * @return returns the uniqueId of the node as formatted hex string
     */
    public static String[] getPidAndSerialNumberAsString(long uniqueId, int pidBits) {
        // TODO add support for the pidBits ...

        return new String[] { "PID: ???", "Serial: ???" };
    }

    /**
     * Return the unique id formatted with the pattern 'V %02X P %08X'.
     * 
     * @param uniqueId
     *            the unique id of the node
     * @return returns the uniqueId of the node as formatted hex string
     */
    public static String getUniqueIdAsString(long uniqueId) {
        return String.format("V %02X P %08X", (uniqueId >> 32) & 0xff, uniqueId & 0xffffffffL);
    }

    /**
     * @param uniqueId
     *            the unique id of the node
     * @return returns the uniqueId of the node as byte array
     */
    public static byte[] getUniqueId(long uniqueId) {
        byte[] result = new byte[7];
        ByteBuffer bb = ByteBuffer.allocate(8);

        bb.putLong(uniqueId);
        System.arraycopy(bb.array(), 1, result, 0, result.length);
        return result;
    }

    /**
     * @param uniqueId
     *            the unique id of the node as byte array
     * @return returns the uniqueId of the node as long
     */
    public static long getUniqueId(byte[] uniqueId) {
        long result = 0;

        for (int i = 0; i < uniqueId.length; i++) {
            result = (result << 8) + (uniqueId[i] & 0xFF);
        }
        return result;
    }

    /**
     * Parse the unique id from a hex encoded string value. If the string starts with '0x' prefix, this prefix is
     * removed to let the parse pass.
     * 
     * @param uniqueIdString
     *            the hex encoded string
     * @return the unique id value
     */
    public static long parseUniqueId(String uniqueIdString) {

        String hexUniqueId = uniqueIdString;
        if (hexUniqueId.startsWith("0x")) {
            hexUniqueId = hexUniqueId.substring(2);
        }

        long uniqueId = Long.parseLong(hexUniqueId, 16);
        return uniqueId;
    }

    /**
     * Format the uniqueId as hex encoded string with 14 digits and '0x' prefix.
     * 
     * @param uniqueId
     *            the unique id
     * @return the formatted string
     */
    public static String formatHexUniqueId(long uniqueId) {
        return String.format("0x%014X", uniqueId & 0xffffffffffffffL);
    }

    /**
     * Format the uniqueId as hex encoded string with 14 digits and '0x' prefix.
     * 
     * @param uniqueId
     *            the unique id
     * @return the formatted string
     */
    public static String formatHexUniqueId(Long uniqueId) {
        if (uniqueId != null) {
            return String.format("0x%014X", uniqueId.longValue() & 0xffffffffffffffL);
        }
        return null;
    }

    /**
     * Get the vendor id from the unique id.
     * 
     * @param uniqueId
     *            the unique id
     * @return the vendor id
     */
    public static int getVendorId(long uniqueId) {
        return (int) (uniqueId >> 32) & 0xff;
    }

    /**
     * Get the product id from the unique id.
     * 
     * @param uniqueId
     *            the unique id
     * @return the product id
     */
    public static int getPid(long uniqueId) {

        long pid = (uniqueId >> 24) & 0xffL | (uniqueId >> 8) & 0xff00L;
        return (int) pid;
    }

    /**
     * Get the product id from the unique id.
     * 
     * @param uniqueId
     *            the unique id
     * @param relevantPidBits
     *            the relevant PID bits
     * @return the product id
     */
    public static int getPid(long uniqueId, int relevantPidBits) {

        long pidAndSerial = (uniqueId >> 24) & 0xffL | (uniqueId >> 8) & 0xff00L;

        long pid = pidAndSerial & ((1 << relevantPidBits) - 1);

        return (int) pid;
    }

    /**
     * Find a node by its node address in the provided list of nodes.
     * 
     * @param nodes
     *            the list of nodes
     * @param address
     *            the address of the node to find
     * @return the found node or <code>null</code> if no node was found with the provided node address
     */
    public static Node findNodeByAddress(Iterable<Node> nodes, final byte[] address) {
        Node node = IterableUtils.find(nodes, new Predicate<Node>() {

            @Override
            public boolean evaluate(final Node node) {
                if (Arrays.equals(node.getAddr(), address)) {
                    LOGGER.debug("Found node: {}", node);
                    return true;
                }
                return false;
            }
        });
        return node;
    }

    /**
     * Find a node by its node uuid in the provided list of nodes.
     * 
     * @param nodes
     *            the list of nodes
     * @param uuid
     *            the uuid of the node to find
     * @return the found node or <code>null</code> if no node was found with the provided node uuid
     */
    public static Node findNodeByUuid(Iterable<Node> nodes, final long uuid) {
        Node node = IterableUtils.find(nodes, new Predicate<Node>() {

            @Override
            public boolean evaluate(final Node node) {
                if (node.getUniqueId() == uuid) {
                    LOGGER.debug("Found node: {}", node);
                    return true;
                }
                return false;
            }
        });
        return node;
    }

    /**
     * Check if the node is a subnode of the parent.
     * 
     * @param parent
     *            the parent node
     * @param node
     *            the child node
     * @return child node is child of parent node
     */
    public static boolean isSubNode(Node parent, Node node) {
        byte[] parentAddress = parent.getAddr();
        byte[] nodeAddress = node.getAddr();

        // check if the nodes are equal
        if (Arrays.equals(parentAddress, nodeAddress)) {
            return false;
        }

        if (parentAddress.length == 1 && parentAddress[0] == 0) {
            // interface node needs special processing
            parentAddress = new byte[0];
        }

        if (nodeAddress.length == (parentAddress.length + 1)) {
            byte[] compareAddress = new byte[parentAddress.length];
            System.arraycopy(nodeAddress, 0, compareAddress, 0, parentAddress.length);

            if (Arrays.equals(parentAddress, compareAddress)) {
                LOGGER.debug("The current node is a subnode: {}, parent: {}", node, parent);
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the node is a subnode of the parent.
     * 
     * @param parentAddress
     *            the parent address
     * @param nodeAddress
     *            the child address
     * @return child node is child of parent node
     */
    public static boolean isSubNode(byte[] parentAddress, byte[] nodeAddress) {

        // check if the nodes are equal
        if (Arrays.equals(parentAddress, nodeAddress)) {
            return false;
        }

        if (parentAddress.length == 1 && parentAddress[0] == 0) {
            // interface node needs special processing
            parentAddress = new byte[0];
        }

        if (nodeAddress.length == (parentAddress.length + 1)) {
            byte[] compareAddress = new byte[parentAddress.length];
            System.arraycopy(nodeAddress, 0, compareAddress, 0, parentAddress.length);

            if (Arrays.equals(parentAddress, compareAddress)) {
                LOGGER.debug("The current node is a subnode: {}, parent: {}", nodeAddress, parentAddress);
                return true;
            }
        }
        return false;
    }

    /**
     * Create a new node based on the parent address and the received data.
     * 
     * @param parentAddress
     *            the parent address
     * @return the node
     */
    public static Node getNode(byte[] parentAddress, byte version, byte localAddr, byte[] uniqueId) {
        // LOGGER.debug("Create new node with parent address: {}", parentAddress);
        byte[] addr = new byte[parentAddress.length + 1];

        LOGGER.info("Create new node with parent address: {}, current local address: {}", parentAddress, localAddr);

        if (parentAddress.length == 1 && parentAddress[0] == 0) {
            // the parent is the interface node
            addr = new byte[1];
            addr[0] = localAddr;
        }
        else if (localAddr == 0) {
            // we have the node itself
            addr = new byte[parentAddress.length];
            System.arraycopy(parentAddress, 0, addr, 0, parentAddress.length);
        }
        else {
            // add the local address of the subnode to the parent address
            System.arraycopy(parentAddress, 0, addr, 0, parentAddress.length);
            addr[parentAddress.length] = localAddr;
        }

        // create the new node with the received data
        Node node = new Node(ByteUtils.getInt(version), addr, ByteUtils.convertUniqueIdToLong(uniqueId));
        LOGGER.info("Created node from received data: {}", node);
        return node;
    }

    /**
     * Prepare the node label from the stored string data of a node.
     * 
     * @param node
     *            the node
     * @return the node label
     */
    public static String prepareNodeLabel(Node node) {
        String nodeLabel = node.getStoredString(StringData.INDEX_USERNAME);
        String productString = node.getStoredString(StringData.INDEX_PRODUCTNAME);
        if (StringUtils.isBlank(nodeLabel)) {
            nodeLabel = getUniqueIdAsString(node.getUniqueId());
        }
        // try to get the product name
        if (StringUtils.isNotBlank(productString)) {
            nodeLabel += " (" + productString + ")";
        }

        return nodeLabel;
    }

    /**
     * Search a feature in the provided list by feature number.
     * 
     * @param featureKey
     *            the key
     * @param features
     *            the list of features
     * @return the matching feature or <code>null</code> if no matching feature was found
     */
    public static Feature getFeature(final FeatureEnum featureKey, List<Feature> features) {
        final int featureNumber = featureKey.getNumber();
        Feature feature = IterableUtils.find(features, new Predicate<Feature>() {

            @Override
            public boolean evaluate(Feature feature) {
                if (feature.getType() == featureNumber) {
                    return true;
                }
                return false;
            }
        });
        return feature;
    }

    /**
     * Find a port by the provided port number.
     * 
     * @param portList
     *            the port list
     * @param portNumber
     *            the port number
     * @return the port instance
     */
    public static <T extends Port> T findPortByNumber(Iterable<T> portList, final int portNumber) {
        T instance = IterableUtils.find(portList, new Predicate<T>() {

            @Override
            public boolean evaluate(T object) {
                return object.getNumber() == portNumber;
            }
        });
        return instance;
    }
}
