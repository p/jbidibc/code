package org.bidib.jbidibc.core.exception;

public class PortNotReadyForSendException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String portName;

    public PortNotReadyForSendException(String portName) {
        this.portName = portName;
    }

    public String getPortName() {
        return portName;
    }
}
