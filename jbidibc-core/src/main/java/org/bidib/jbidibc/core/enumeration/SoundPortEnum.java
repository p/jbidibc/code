package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum SoundPortEnum implements BidibEnum {
    // @formatter:off
    PLAY(BidibLibrary.BIDIB_PORT_TURN_ON), STOP(BidibLibrary.BIDIB_PORT_TURN_OFF), TEST(ByteUtils.getLowByte(0xFF));
    // @formatter:on

    private final byte type;

    SoundPortEnum(int type) {
        this.type = (byte) type;
    }

    public byte getType() {
        return type;
    }

    /**
     * Create a sound port enum.
     * 
     * @param type
     *            numeric value of the sound port enum
     * 
     * @return SoundPortEnum
     */
    public static SoundPortEnum valueOf(byte type) {
        SoundPortEnum result = null;

        for (SoundPortEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a sound port enum");
        }
        return result;
    }
}
