package org.bidib.jbidibc.core.schema;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.bidib2.Accessories;
import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.FeatureCode;
import org.bidib.jbidibc.core.schema.bidib2.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class BidibFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibFactoryTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/xml-test/bidib";

    private static final String OUTPUT_FILENAME = "bidib-test1.xml";

    private static final String OUTPUT_FILENAME_NODE = "bidib-test-node.xml";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void loadMessageTypesTest() {
        List<MessageType> messageTypes = BidibFactory.getMessageTypes();
        Assert.assertNotNull(messageTypes);
        Assert.assertEquals(messageTypes.size(), 127);
    }

    @Test
    public void loadFeatureCodesTest() {
        List<FeatureCode> featureCodes = BidibFactory.getFeatureCodes();
        Assert.assertNotNull(featureCodes);
        Assert.assertEquals(featureCodes.size(), 72);
    }

    @Test
    public void saveBiDiBTest() {

        List<Accessory> accessories = new ArrayList<>();
        Accessory accessory = new Accessory();
        accessory.setName("Test 1");
        accessory.setNumber(0);
        accessories.add(accessory);

        BiDiB bidib = new BiDiB();
        bidib.withSchemaVersion("1.0");
        bidib.withAccessories(new Accessories()).getAccessories().withAccessory(accessories);

        String fileName = OUTPUT_TARGET_DIR + "/" + OUTPUT_FILENAME;
        BidibFactory.saveBiDiB(bidib, fileName, false);
    }

    @Test
    public void saveBiDiBNodeTest() {

        BiDiB bidib = new BidibTestFactory().createDemoBidib();
        String fileName = OUTPUT_TARGET_DIR + "/" + OUTPUT_FILENAME_NODE;
        BidibFactory.saveBiDiB(bidib, fileName, false);
    }

    @Test
    public void prepareNodeFilenameTest() {
        long uniqueId = 19421831190592234L;
        String formatedFilename = BidibFactory.prepareNodeFilename(uniqueId);
        LOGGER.info("formatedFilename: {}", formatedFilename);

        Assert.assertEquals(formatedFilename, "45000D7500BAEA.xml");
    }

    @Test
    public void prepareNodeFilename2Test() {
        long uniqueId = 1407432681110250L;
        String formatedFilename = BidibFactory.prepareNodeFilename(uniqueId);
        LOGGER.info("formatedFilename: {}", formatedFilename);

        Assert.assertEquals(formatedFilename, "05000D7500BAEA.xml");
    }
}
