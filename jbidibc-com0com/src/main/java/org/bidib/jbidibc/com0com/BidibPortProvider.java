package org.bidib.jbidibc.com0com;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.bidib.jbidibc.com0com.BidibPortStatusListener.PortStatus;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.MessageProcessor;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.helpers.DefaultContext;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.bidib.jbidibc.debug.DebugInterface;
import org.bidib.jbidibc.debug.DebugMessageProcessor;
import org.bidib.jbidibc.debug.DebugMessageReceiver;
import org.bidib.jbidibc.debug.DebugReaderFactory;
import org.bidib.jbidibc.debug.DebugReaderFactory.SerialImpl;
import org.bidib.jbidibc.serial.SerialMessageParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibPortProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibPortProvider.class);

    private DebugInterface interfacePort;

    private DebugInterface proxyPort;

    private RequestFactory requestFactory;

    private ConnectionListener interfaceConnectionListener;

    private ConnectionListener proxyConnectionListener;

    private Set<NodeListener> nodeListeners;

    private Set<MessageListener> messageListeners;

    private Set<TransferListener> transferListeners;

    private Set<BidibPortStatusListener> statusListeners = new LinkedHashSet<>();

    private final Context context = new DefaultContext();

    public BidibPortProvider() {
        LOGGER.info("Create new instance of BidibPortProvider.");
    }

    public void addStatusListener(BidibPortStatusListener statusListener) {
        if (!statusListeners.contains(statusListener)) {
            statusListeners.add(statusListener);
        }
    }

    public void removeStatusListener(BidibPortStatusListener statusListener) {
        if (statusListeners.contains(statusListener)) {
            statusListeners.remove(statusListener);
        }
    }

    /**
     * Start the provider. This will open the interface port and the open the proxy port if successful.
     * 
     * @param interfacePortName
     *            the name of the 'real' interface port
     * @param proxyPortName
     *            the name of the 'proxy' port that is opened
     */
    public void start(String interfacePortName, String proxyPortName) {

        System.setProperty("jbidibc.appsuffix", "bidib-proxy");

        LOGGER.info("Create the request factory and start the interface and proxy port.");

        requestFactory = new RequestFactory();

        interfaceConnectionListener = new ConnectionListener() {

            @Override
            public void status(String messageKey) {
                LOGGER.info("The status has changed: {}", messageKey);

            }

            @Override
            public void opened(String port) {
                LOGGER.info("The port was opened: {}", port);

                openProxyPort(context, proxyPortName);
            }

            @Override
            public void closed(String port) {
                LOGGER.info("The port was closed: {}", port);

                closeProxyPort();
            }
        };

        proxyConnectionListener = new ConnectionListener() {

            @Override
            public void status(String messageKey) {
                LOGGER.info("The status has changed: {}", messageKey);

            }

            @Override
            public void opened(String port) {
                LOGGER.info("The port was opened: {}", port);

                for (BidibPortStatusListener listener : statusListeners) {
                    listener.statusChanged(PortStatus.CONNECTED);
                }
            }

            @Override
            public void closed(String port) {
                LOGGER.info("The port was closed: {}", port);

                for (BidibPortStatusListener listener : statusListeners) {
                    listener.statusChanged(PortStatus.DISCONNECTED);
                }
            }
        };

        openInterfacePort(context, interfacePortName);

    }

    // private ByteArrayOutputStream proxyReceiveBuffer = new ByteArrayOutputStream(2048);

    private SerialMessageParser proxyMessageParser;

    private ProxyMessageProcessor messageProcessor = new ProxyMessageProcessor(true);

    private void openInterfacePort(final Context context, final String interfacePortName) {

        LOGGER.info("Begin open the interface bidib port: {}", interfacePortName);

        int baudRate = 115200;

        proxyMessageParser = new SerialMessageParser();

        final DebugMessageProcessor messageReceiver = new DebugMessageReceiver() {

            @Override
            public void processMessages(ByteArrayOutputStream output) {
                try {
                    if (proxyPort != null) {

                        LOGGER.info("<< Received data from proxy: {}", ByteUtils.bytesToHex(output));

                        try {
                            proxyMessageParser.parseInput(messageProcessor, output.toByteArray(), output.size());
                        }
                        catch (Exception ex1) {
                            LOGGER.warn("Prepare messages from to proxy for logging failed.", ex1);
                        }

                        proxyPort.send(output.toByteArray());
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Send message from interface to proxy failed.", ex);
                }
                finally {
                    output.reset();
                }
            }
        };

        // support remote interface ports
        SerialImpl serialImpl = SerialImpl.SCM;
        if (interfacePortName.contains(":")) {
            String[] splited = interfacePortName.split(":");
            if (InetAddressValidator.getInstance().isValid(splited[0])) {
                LOGGER.info("Valid IP address detected: {}", splited[0]);
                serialImpl = SerialImpl.SPSW_NET;
            }
            else if (Pattern.matches(VALID_HOSTNAME_REGEX, splited[0])) {
                LOGGER.info("Valid hostname detected: {}", splited[0]);
                serialImpl = SerialImpl.SPSW_NET;
            }
        }

        try {
            interfacePort = DebugReaderFactory.getDebugReader(serialImpl, messageReceiver);
            interfacePort.open(interfacePortName, baudRate, interfaceConnectionListener, context);

            LOGGER.info("Open the interface bidib port passed.");
        }
        catch (PortNotFoundException ex) {
            LOGGER.warn("Selected port for interface bidib is not available.", ex);

            throw new RuntimeException("Selected port for interface bidib is not available: " + ex.getMessage());
        }
        catch (PortNotOpenedException ex) {
            LOGGER.warn("Open port for interface bidib failed.", ex);

            throw new RuntimeException("Open port for interface bidib failed: " + ex.getMessage());
        }
        catch (Exception ex) {
            LOGGER.warn("Create interface bidib failed.", ex);

            throw new RuntimeException("Create interface bidib failed.");
        }

    }

    public static final String VALID_HOSTNAME_REGEX =
        "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";

    private static final class ProxyMessageProcessor implements MessageProcessor {
        private static final Logger MSG_RX_LOGGER = LoggerFactory.getLogger("RX");

        private boolean checkCRC;

        private final BidibResponseFactory responseFactory;

        public ProxyMessageProcessor(boolean checkCRC) {

            this.responseFactory = new BidibResponseFactory();
            this.checkCRC = checkCRC;

            LOGGER.info("Create new ProxyMessageProcessor with checkCRC: {}", checkCRC);
        }

        @Override
        public void processMessages(ByteArrayOutputStream messageData) throws ProtocolException {

            if (messageData.size() < 1) {
                LOGGER.info("No data in provided buffer, skip process messages.");
                return;
            }

            // if a CRC error is detected in splitMessages the reading loop will terminate ...
            Collection<byte[]> messages = MessageUtils.splitBidibMessages(messageData, checkCRC);

            if (messages == null) {
                LOGGER.warn("No messages to process available.");
                return;
            }

            LOGGER.debug("Number of splited messages: {}", messages.size());

            for (byte[] messageArray : messages) {
                BidibMessage message = null;

                try {
                    message = responseFactory.create(messageArray);

                    StringBuilder sb = new StringBuilder("<< ");
                    sb.append(message);
                    sb.append(" : ");
                    sb.append(ByteUtils.bytesToHex(messageArray));

                    MSG_RX_LOGGER.info(sb.toString());
                }
                catch (ProtocolException ex) {
                    LOGGER.warn("Process received messages failed: {}", ByteUtils.bytesToHex(messageArray), ex);

                    StringBuilder sb = new StringBuilder("<< received invalid: ");
                    sb.append(message);
                    sb.append(" : ");
                    sb.append(ByteUtils.bytesToHex(messageArray));

                    MSG_RX_LOGGER.warn(sb.toString());

                    throw ex;
                }
                catch (Exception ex) {
                    LOGGER.warn("Process received messages failed: {}", ByteUtils.bytesToHex(messageArray), ex);
                }
            }
        }

    }

    private static final Logger MSG_TX_LOGGER = LoggerFactory.getLogger("TX");

    private void openProxyPort(final Context context, final String proxyPortName) {

        LOGGER.info("Begin open the proxy port: {}", proxyPortName);

        int baudRate = 115200;

        final DebugMessageProcessor messageReceiver = new DebugMessageReceiver() {

            @Override
            public void processMessages(ByteArrayOutputStream output) {
                try {
                    if (interfacePort != null) {

                        try {
                            List<BidibCommand> bidibMessages = requestFactory.create(output.toByteArray());
                            for (BidibCommand message : bidibMessages) {
                                MSG_TX_LOGGER.info(">> {} : {}", message, ByteUtils.bytesToHex(message.getContent()));
                            }
                        }
                        catch (Exception e1) {
                            LOGGER.warn("Prepare logging outgoing messages to interface failed.", e1);
                        }

                        interfacePort.send(output.toByteArray());
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Send message from proxy to interface failed.", ex);
                }
                finally {
                    output.reset();
                }
            }
        };

        try {
            proxyPort = DebugReaderFactory.getDebugReader(SerialImpl.SCM, messageReceiver);
            proxyPort.open(proxyPortName, baudRate, proxyConnectionListener, context);

            LOGGER.info("Open the proxy bidib port passed.");
        }
        catch (PortNotFoundException ex) {
            LOGGER.warn("Selected port for proxy bidib is not available.", ex);
        }
        catch (PortNotOpenedException ex) {
            LOGGER.warn("Open port for proxy bidib failed.", ex);
        }
        catch (Exception ex) {
            LOGGER.warn("Create proxy bidib failed.", ex);
        }

    }

    public void stop() {

        closeProxyPort();

        closeInterfacePort();
    }

    private void closeInterfacePort() {
        LOGGER.info("Close the interface port.");

        if (interfacePort != null) {
            LOGGER.info("Close the interface port: {}", interfacePort);
            interfacePort.close();

            interfacePort = null;
        }
        else {
            LOGGER.info("No interface port to close available.");
        }

    }

    private void closeProxyPort() {
        LOGGER.info("Close the proxy port.");

        if (proxyPort != null) {
            LOGGER.info("Close the proxy port: {}", proxyPort);
            proxyPort.close();

            proxyPort = null;
        }
        else {
            LOGGER.info("No proxy port to close available.");
        }

    }
}
