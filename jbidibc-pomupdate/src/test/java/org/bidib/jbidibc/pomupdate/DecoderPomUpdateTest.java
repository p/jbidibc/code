package org.bidib.jbidibc.pomupdate;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.bidib.jbidibc.core.message.CommandStationPomMessage;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.pomupdate.DecoderInformation.DecoderType;
import org.bidib.jbidibc.pomupdate.FirmwarePacket.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DecoderPomUpdateTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderPomUpdateTest.class);

    @Test
    public void parseLineTest() {
        String line = ":10F130002990285F7369636865725F3E0000310241";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        Assert.assertEquals(packet.getTarget(), Target.flash);
        Assert.assertEquals(packet.getTargetAddress(), 0xF130);
    }

    @Test
    public void parseFirstLineTest() {
        String line = ":1000000080090020ED000000C1000000C1000000D8";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        Assert.assertEquals(packet.getTarget(), Target.flash);
        Assert.assertEquals(packet.getTargetAddress(), 0);
    }

    @Test
    public void findDecoderInformationTest() throws URISyntaxException {

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        File firmwareFile =
            new File(DecoderPomUpdate.class.getResource("/firmware/LD-G-31-Plus_V2_2.hex").toURI().getPath());
        List<String> firmwareContent = decoderPomUpdate.loadFirmwareFile(firmwareFile);

        DecoderInformation information = decoderPomUpdate.findDecoderInformation(firmwareContent);
        Assert.assertNotNull(information);

        Assert.assertEquals(information.getVendorId(), 62 /* tams */);
        Assert.assertEquals(information.getDecoderType(), DecoderType.locoDecoder /* loco */);
        Assert.assertEquals(information.getDecoderId(), 0x31 /* LD-G-31 */);

        Assert.assertEquals(information.getFirmwareVersion(), "2.2.0.0");
        Assert.assertEquals(information.getFirmwareReleaseDate(), "03.05.2015");
    }

    @Test
    public void findDecoderInformationFDRbasic2Test() throws URISyntaxException {

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        File firmwareFile =
            new File(DecoderPomUpdate.class.getResource("/firmware/FD-Rbasic_2_V1_2_0_0.hex").toURI().getPath());
        List<String> firmwareContent = decoderPomUpdate.loadFirmwareFile(firmwareFile);

        DecoderInformation information = decoderPomUpdate.findDecoderInformation(firmwareContent);
        Assert.assertNotNull(information);

        Assert.assertEquals(information.getVendorId(), 62 /* tams */);
        Assert.assertEquals(information.getDecoderType(), DecoderType.locoDecoder /* loco */);
        Assert.assertEquals(information.getDecoderId(), 0xB2 /* FD-Rbasic-2 */);

        Assert.assertEquals(information.getFirmwareVersion(), "1.2.0.0");
        Assert.assertEquals(information.getFirmwareReleaseDate(), "17.03.2015");
    }

    @Test
    public void prepareNextRowPacketTest() {
        String line = ":1000000080090020ED000000C1000000C1000000D8";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        CommandStationPomMessage message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Current message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 0x300 + 1);
        Assert.assertEquals(message.getCvValue(), 0);

        line = ":10001000C1000000C1000000C1000000000000009D";

        packet = decoderPomUpdate.parseLine(line);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Current message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 0x300 + 1);
        Assert.assertEquals(message.getCvValue(), 16);
    }

    @Test
    public void prepareSendRowPacketsTest() {
        String line = ":1000000080090020ED000000C1000000C1000000D8";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        // 802,398s R15 E2 01 EC 00 31 3E L08705 CV Access: Write: 0001:0049
        // ------------ 22 01 --> 8705 <-- decoder address (value & 0x3F is decoder address)
        // ------------------ EC --> POM Write (111011xx) --> Bit 1,0: 00 -> Part of CV address
        // --------------------- 00 -----> CV: decoder identification high
        // ------------------------ 31 --> data: decoder identification low

        // :1000000080090020ED000000C1000000C1000000D8
        // :aabbbbxx80090020ED000000C1000000C1000000D8
        // aa : num of relevant bytes in row
        // bbbb: address of first byte in flash
        // xx: target -> 0 -> flash
        //
        // 805,562s R12 E1 00 EF 00 00 0E L08448 CV Access: Write: 0769:0000
        // ------------ 21 00 --> 8448 <-- decoder address for new line
        // ------------------ EF --> POM Write (111011xx) --> Bit 1,0: 11 -> Part of CV address >>> num of packets: 16
        // >>> (bit 1*2 + bit 0 + 1) * 4 -> 16
        // --------------------- 00 -----> CV: 00
        // ------------------------ 00 --> data: 00

        // 80 09 00
        // 805,610s R14 E0 88 EC 09 00 8D L08328 CV Access: Write: 0010:0000
        // ------------ 20 88 --> 8328 > 0x2088 (8192 + 136 = 0x88)
        // ------------------ EC --> POM Write (111011xx) --> Bit 1,0: 00 -> Part of CV address >>> pos: 0
        // >>> (bit 1*2 + bit 0) * 3 -> 0
        // --------------------- 09 -----> CV: 09
        // ------------------------ 00 --> data: 00

        // 20 ED 00
        // 805,657s R13 E0 20 ED ED 00 C0 L08224 CV Access: Write: 0494:0000
        // ------------ 20 20 --> 8224 > 0x2020 (8192 + 32 = 0x20)
        // ------------------ ED --> POM Write (111011xx) --> Bit 1,0: 01 -> Part of CV address >>> pos: 3
        // >>> (bit 1*2 + bit 0) * 3 -> 0
        // --------------------- ED -----> CV: ED
        // ------------------------ 00 --> data: 00

        CommandStationPomMessage message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Row packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8448);
        Assert.assertEquals(message.getCvNumber(), 0x300 + 1);
        Assert.assertEquals(message.getCvValue(), 0);

        // 80 09 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 128 /* 8320 */);
        Assert.assertEquals(message.getCvNumber(), 0x09 + 1);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 20 ED 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 32 /* 8224 */);
        Assert.assertEquals(message.getCvNumber(), 0xED + 1 + 0x0100);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 00 00 C1
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0200);
        Assert.assertEquals(message.getCvValue(), 0xC1);

        // 00 00 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0300);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // C1 00 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 193 /* 8385 */);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0000);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0100);
        Assert.assertEquals(message.getCvValue(), 0x00);

        String line2 = ":10001000C1000000C1000000C1000000000000009D";
        packet = decoderPomUpdate.parseLine(line2);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        // 805,930s R13 E1 00 EF 00 10 1E L08448 CV Access: Write: 0769:0016

        // 10 0010 00
        message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Row packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8448);
        Assert.assertEquals(message.getCvNumber(), 0x300 + 1);
        Assert.assertEquals(message.getCvValue(), 16);

        // C1 00 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 193 /* 0xC1 : 8385 */);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 00 C1 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0 /* 0x00 : 8192 */);
        Assert.assertEquals(message.getCvNumber(), 0xC1 + 1 + 0x0100);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 00 00 C1
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0 /* 0x00 : 8192 */);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0200);
        Assert.assertEquals(message.getCvValue(), 0xC1);
    }

    @Test
    public void prepareLine2Test() {
        String line2 = ":1000000080090020ED000000C1000000C1000000D8";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line2);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        int packetLen = ByteUtils.getInt(packet.getLen());

        // 08 F2D0 00
        CommandStationPomMessage message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Row packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8448);
        Assert.assertEquals(message.getCvNumber(), 0x300 + 1);
        Assert.assertEquals(message.getCvValue(), 0);

        for (int index = 0; index < (packetLen / 3 + 1); index++) {

            message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
            Assert.assertNotNull(message);
            LOGGER.info("Data packet, message: {}, formatted data: {}", message,
                ByteUtils.bytesToHex(message.getData()));
        }
        // verify the last message
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0100);
        Assert.assertEquals(message.getCvValue(), 0x00);
    }

    @Test
    public void prepareSmallerLine3Test() {
        String line2 = ":0CF2F8001027000000805101008051012F";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line2);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        // 0C F2F8 00
        CommandStationPomMessage message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Row packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8448);
        Assert.assertEquals(message.getCvNumber(), 0xF2 + 0x100 + 1);
        Assert.assertEquals(message.getCvValue(), 0xF8);

        // 10 27 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 16 /* 0x10 : 8208 */);
        Assert.assertEquals(message.getCvNumber(), 0x27 + 1);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 00 00 80
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0 /* 0x00 : 8192 */);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0100);
        Assert.assertEquals(message.getCvValue(), 0x80);

        // 51 01 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 81 /* 0x51 : 8273 */);
        Assert.assertEquals(message.getCvNumber(), 0x01 + 1 + 0x0200);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 80 51 01
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 128 /* 0x80 : 8320 */);
        Assert.assertEquals(message.getCvNumber(), 0x51 + 1 + 0x0300);
        Assert.assertEquals(message.getCvValue(), 0x01);
    }

    @Test
    public void prepareSmallerLineTest() {
        String line2 = ":08F2D000047E00003281000001";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line2);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        // 08 F2D0 00
        CommandStationPomMessage message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Row packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8448);
        Assert.assertEquals(message.getCvNumber(), 0xF2 + 1);
        Assert.assertEquals(message.getCvValue(), 0xD0);

        // 04 7E 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 4 /* 0x04 : 8196 */);
        Assert.assertEquals(message.getCvNumber(), 0x7E + 1);
        Assert.assertEquals(message.getCvValue(), 0x00);

        // 00 32 81
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0 /* 0x00 : 8192 */);
        Assert.assertEquals(message.getCvNumber(), 0x32 + 1 + 0x0100);
        Assert.assertEquals(message.getCvValue(), 0x81);

        // 00 00
        message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0 /* 0x00 : 8192 */);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0200);
        Assert.assertEquals(message.getCvValue(), 0x00);
    }

    @Test
    public void prepareSmallerLine2Test() {
        String line2 = ":08F2D000047E00003281000001";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        FirmwarePacket packet = decoderPomUpdate.parseLine(line2);
        Assert.assertNotNull(packet);
        LOGGER.info("Current packet: {}", packet);

        int packetLen = ByteUtils.getInt(packet.getLen());

        // 08 F2D0 00
        CommandStationPomMessage message = decoderPomUpdate.prepareNextRowPacket(packet, DecoderType.locoDecoder);
        Assert.assertNotNull(message);
        LOGGER.info("Row packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8448);
        Assert.assertEquals(message.getCvNumber(), 0xF2 + 1);
        Assert.assertEquals(message.getCvValue(), 0xD0);

        for (int index = 0; index < (packetLen / 3 + 1); index++) {

            message = decoderPomUpdate.prepareNextDataPacket(packet, DecoderType.locoDecoder);
            Assert.assertNotNull(message);
            LOGGER.info("Data packet, message: {}, formatted data: {}", message,
                ByteUtils.bytesToHex(message.getData()));
        }
        // verify the last message
        Assert.assertNotNull(message);
        LOGGER.info("Data packet, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8192 + 0 /* 0x00 : 8192 */);
        Assert.assertEquals(message.getCvNumber(), 0x00 + 1 + 0x0200);
        Assert.assertEquals(message.getCvValue(), 0x00);
    }

    @Test
    public void addPacketsTest() {
        String line2 = ":08F2D000047E00003281000001";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        List<CommandStationPomMessage> messages = new ArrayList<CommandStationPomMessage>();
        decoderPomUpdate.addPackets(line2, messages);

        Assert.assertFalse(messages.isEmpty());
        Assert.assertEquals(messages.size(), 4);
    }

    @Test
    public void addPackets2Test() {
        String line2 = ":0CF2F8001027000000805101008051012F";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        List<CommandStationPomMessage> messages = new ArrayList<CommandStationPomMessage>();
        decoderPomUpdate.addPackets(line2, messages);

        Assert.assertFalse(messages.isEmpty());
        Assert.assertEquals(messages.size(), 5);
    }

    @Test
    public void addPackets3Test() {
        String line = ":1000000080090020ED000000C1000000C1000000D8";

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        List<CommandStationPomMessage> messages = new ArrayList<CommandStationPomMessage>();
        decoderPomUpdate.addPackets(line, messages);

        Assert.assertFalse(messages.isEmpty());
        Assert.assertEquals(messages.size(), 7);
    }

    @Test
    public void prepareDecoderInfoPomMessageTest() throws URISyntaxException {

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();

        File firmwareFile =
            new File(DecoderPomUpdate.class.getResource("/firmware/LD-G-31-Plus_V2_2.hex").toURI().getPath());

        List<String> firmwareContent = decoderPomUpdate.loadFirmwareFile(firmwareFile);

        DecoderInformation information = decoderPomUpdate.findDecoderInformation(firmwareContent);
        Assert.assertNotNull(information);

        Assert.assertEquals(information.getVendorId(), 62 /* tams */);
        Assert.assertEquals(information.getDecoderType(), DecoderType.locoDecoder /* loco */);
        Assert.assertEquals(information.getDecoderId(), 0x31 /* LD-G-31 */);

        Assert.assertEquals(information.getFirmwareVersion(), "2.2.0.0");
        Assert.assertEquals(information.getFirmwareReleaseDate(), "03.05.2015");

        int decoderAddress = 122;

        CommandStationPomMessage message =
            decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 800);
        Assert.assertNotNull(message);
        LOGGER.info("CV800, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 800);
        Assert.assertEquals(message.getCvValue(), 62);

        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 801);
        Assert.assertNotNull(message);
        LOGGER.info("CV801, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 801);
        Assert.assertEquals(message.getCvValue(), DecoderType.locoDecoder.ordinal());

        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 802);
        Assert.assertNotNull(message);
        LOGGER.info("CV802, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 802);
        Assert.assertEquals(message.getCvValue(), 0);

        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 803);
        Assert.assertNotNull(message);
        LOGGER.info("CV803, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 803);
        Assert.assertEquals(message.getCvValue(), 0x31); /* LD-G-31 */

        // firmware version
        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 804);
        Assert.assertNotNull(message);
        LOGGER.info("CV804, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 804);
        Assert.assertEquals(message.getCvValue(), 2);

        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 805);
        Assert.assertNotNull(message);
        LOGGER.info("CV805, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 805);
        Assert.assertEquals(message.getCvValue(), 2);

        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 806);
        Assert.assertNotNull(message);
        LOGGER.info("CV806, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 806);
        Assert.assertEquals(message.getCvValue(), 0);

        message = decoderPomUpdate.prepareDecoderInfoPomMessage(information, decoderAddress, 807);
        Assert.assertNotNull(message);
        LOGGER.info("CV807, message: {}, formatted data: {}", message, ByteUtils.bytesToHex(message.getData()));
        Assert.assertEquals(message.getCvNumber(), 807);
        Assert.assertEquals(message.getCvValue(), 0);
    }

    @Test
    public void prepareDecoderInfoPomMessagesTest() throws URISyntaxException {

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        File firmwareFile =
            new File(DecoderPomUpdate.class.getResource("/firmware/LD-G-31-Plus_V2_2.hex").toURI().getPath());
        List<String> firmwareContent = decoderPomUpdate.loadFirmwareFile(firmwareFile);

        DecoderInformation information = decoderPomUpdate.findDecoderInformation(firmwareContent);
        Assert.assertNotNull(information);

        Assert.assertEquals(information.getVendorId(), 62 /* tams */);
        Assert.assertEquals(information.getDecoderType(), DecoderType.locoDecoder /* loco */);
        Assert.assertEquals(information.getDecoderId(), 0x31 /* LD-G-31 */);

        Assert.assertEquals(information.getFirmwareVersion(), "2.2.0.0");
        Assert.assertEquals(information.getFirmwareReleaseDate(), "03.05.2015");

        int decoderAddress = 122;

        List<CommandStationPomMessage> messages =
            decoderPomUpdate.prepareDecoderInfoPomMessages(information, decoderAddress);
        Assert.assertNotNull(messages);
        LOGGER.info("Prepared messages: {}, formatted data: {}", messages);
        Assert.assertEquals(messages.size(), 12);

        int cvNumber = 800;
        for (CommandStationPomMessage message : messages) {
            Assert.assertEquals(message.getCvNumber(), cvNumber);
            LOGGER.info("CV{}, message: {}, formatted data: {}", cvNumber, message,
                ByteUtils.bytesToHex(message.getData()));
            cvNumber++;
        }
    }

    @Test
    public void prepareDecoderUpdatePomMessagesTest() throws URISyntaxException {

        DecoderPomUpdate decoderPomUpdate = new DecoderPomUpdate();
        File firmwareFile =
            new File(DecoderPomUpdate.class.getResource("/firmware/LD-G-31-Plus_V2_2.hex").toURI().getPath());
        List<String> firmwareContent = decoderPomUpdate.loadFirmwareFile(firmwareFile);
        Assert.assertNotNull(firmwareContent);

        DecoderInformation decoderInformation = new DecoderInformation("3E000031020200000305201500000000");

        List<CommandStationPomMessage> messages =
            decoderPomUpdate.prepareDecoderUpdatePomMessages(firmwareContent, decoderInformation);
        Assert.assertNotNull(messages);

        CommandStationPomMessage message = messages.get(0);
        Assert.assertEquals(message.getDecoderAddress().getAddress(), 8705);

        Assert.assertEquals(message.getCvNumber(), 1);
        Assert.assertEquals(message.getCvValue(), 0x31);
    }
}
