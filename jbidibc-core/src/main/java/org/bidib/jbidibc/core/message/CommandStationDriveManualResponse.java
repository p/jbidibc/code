package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Response from command station with the information of manual drive
 */
public class CommandStationDriveManualResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_CS_DRIVE_MANUAL;

    public static final int FUNCTIONS_INDEX_F0_F4 = 0;

    public static final int FUNCTIONS_INDEX_F5_F12 = 1;

    public static final int FUNCTIONS_INDEX_F13_F20 = 2;

    public static final int FUNCTIONS_INDEX_F21_F28 = 3;

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationDriveManualResponse.class);

    CommandStationDriveManualResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 9) {
            throw new ProtocolException("No valid MSG_CS_DRIVE_MANUAL received.");
        }

        LOGGER.debug("Received response, speed: {}", getSpeed());
    }

    public String getName() {
        return "MSG_CS_DRIVE_MANUAL";
    }

    public int getAddress() {
        byte[] data = getData();

        return (data[0] & 0xFF) + ((data[1] & 0xFF) << 8);
    }

    public byte getFormat() {
        return getData()[2];
    }

    public byte getOutputActive() {
        return getData()[3];
    }

    public int getSpeed() {
        return ByteUtils.getInt(getData()[4]);
    }

    public byte getLights() {
        return (byte) (getData()[5] & 0x10);
    }

    public byte getFunctions(int index) {
        return getData()[5 + index];
    }

    public DriveState getDriveState() {
        DriveState driveState =
            new DriveState(getAddress(), ByteUtils.getInt(getData()[2]), ByteUtils.getInt(getData()[3]), getSpeed(),
                ByteUtils.getInt(getLights()));

        // TODO add the functions ...
        byte[] functions = new byte[4];
        for (int index = 0; index < DriveState.FUNCTIONS_INDEX_F21_F28 + 1; index++) {
            functions[index] = getFunctions(index);
        }
        driveState.setFunctions(functions);

        return driveState;
    }
}
