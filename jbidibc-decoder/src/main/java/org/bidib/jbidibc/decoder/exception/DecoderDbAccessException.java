package org.bidib.jbidibc.decoder.exception;

public class DecoderDbAccessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DecoderDbAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
