package org.bidib.jbidibc.decoder.schema.decoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.exception.InvalidContentException;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class DecoderDefinitionFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderDefinitionFactory.class);

    private static JAXBContext jaxbContext;

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.decoder.schema.decoder";

    public static final String XSD_LOCATION = "/xsd/decoder.xsd";

    public static final String XSD_LOCATION_COMMON_TYPES = "/xsd/commonTypes.xsd";

    public DecoderDefinition loadDecoderDefinition(String filePath) {

        if (StringUtils.isBlank(filePath)) {
            LOGGER.error("The DecoderDefinition file must be specified!");
            return null;
        }

        LOGGER.info("Load DecoderDefinition from file: {}", filePath);

        DecoderDefinition decoderDefinition = null;
        FileInputStream is = null;
        File file = null;
        try {
            file = new File(filePath);

            is = new FileInputStream(file);

            decoderDefinition = loadDecoderDefinitionFile(is);
            LOGGER.trace("Loaded DecoderDefinition: {}", decoderDefinition);

        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load saved DecoderDefinition from file failed with schema exception: {}", ex.getMessage());

            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    LOGGER.warn("Close input stream failed.", e);
                }
                is = null;
            }

            if (file != null) {

                try {
                    is = new FileInputStream(file);
                    List<String> errors = validateDecoderDefinitionFile(is, filePath);

                    // LOGGER.warn("The following validation errors were detected: {}", errors);

                    throw new InvalidContentException("Validation of DecoderDefinition file failed: " + filePath,
                        errors);
                }
                catch (FileNotFoundException e) {
                    LOGGER.warn("Load saved DecoderDefinition for validation from file failed: {}", e.getMessage());
                }
            }
        }
        catch (IllegalArgumentException | FileNotFoundException ex) {
            LOGGER.warn("Load saved DecoderDefinition from file failed: {}", ex.getMessage());
        }
        catch (InvalidContentException ex) {
            // rethrow exception
            throw ex;
        }
        catch (Exception ex) {
            LOGGER.warn("Load saved DecoderDefinition from file failed: {}", ex.getMessage());
        }

        return decoderDefinition;
    }

    protected String getProductDefinitionFileName() {
        // TODO change it
        return "test";
    }

    /**
     * Validate the decoder definition from the provided input stream.
     * 
     * @param is
     *            the input stream
     * @param filePath
     *            the file path
     * @return the list of errors
     */
    private List<String> validateDecoderDefinitionFile(final InputStream is, String filePath) {
        LOGGER.info("Validate decoderDefinition file: {}", filePath);

        List<String> errors = null;
        try {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // Schema schema =
            // schemaFactory.newSchema(new File(DecoderDefinitionFactory.class.getResource(XSD_LOCATION).getPath()));

            StreamSource streamSourceSchemaCommonTypes =
                new StreamSource(DecoderDefinitionFactory.class.getResourceAsStream(XSD_LOCATION_COMMON_TYPES));
            StreamSource streamSourceSchema =
                new StreamSource(DecoderDefinitionFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceSchemaCommonTypes, streamSourceSchema });

            StreamSource streamSource = new StreamSource(is);

            Validator validator = schema.newValidator();
            XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();
            validator.setErrorHandler(errorHandler);

            validator.validate(streamSource);

            errors = errorHandler.getErrors();
        }
        catch (Exception ex) {
            LOGGER.warn("Validate ProductDefinition from file failed: {}", getProductDefinitionFileName(), ex);
            throw new InvalidSchemaException("Load ProductDefinition from file failed.");
        }

        return errors;
    }

    /**
     * Load the decoder definition from the provided input stream.
     * 
     * @param is
     *            the input stream
     * @return the decoder defintion
     */
    private DecoderDefinition loadDecoderDefinitionFile(final InputStream is) {
        LOGGER.info("Load DecoderDefinition file.");

        DecoderDefinition decoderDefinition = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for DecoderDefinition.");
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            StreamSource streamSourceSchemaCommonTypes =
                new StreamSource(DecoderDefinitionFactory.class.getResourceAsStream(XSD_LOCATION_COMMON_TYPES));
            StreamSource streamSourceSchema =
                new StreamSource(DecoderDefinitionFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceSchemaCommonTypes, streamSourceSchema });
            // Schema schema =
            // schemaFactory.newSchema(new File(DecoderDefinitionFactory.class.getResource(XSD_LOCATION).getPath()));

            unmarshaller.setSchema(schema);

            decoderDefinition = (DecoderDefinition) unmarshaller.unmarshal(is);
        }
        catch (UnmarshalException ex) {
            LOGGER.warn("Load DecoderDefinition from file failed with UnmarshalException.", ex);
            if (ex.getCause() instanceof SAXParseException) {
                throw new InvalidSchemaException("Load DecoderDefinition from file failed");
            }

            throw new IllegalArgumentException("Load DecoderDefinition from file failed with UnmarshalException.");
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load DecoderDefinition from file failed: {}", getProductDefinitionFileName(), ex);

            throw new InvalidSchemaException(
                "Load DecoderDefinition from file failed: " + getProductDefinitionFileName());
        }
        catch (Exception ex) {
            LOGGER.warn("Load DecoderDefinition from file failed: {}", getProductDefinitionFileName(), ex);
            throw new RuntimeException("Load DecoderDefinition from file failed: " + getProductDefinitionFileName());
        }
        return decoderDefinition;
    }

}
