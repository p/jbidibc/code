package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;

public class SysUniqueIdResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_SYS_UNIQUE_ID;

    SysUniqueIdResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 7) {
            throw new ProtocolException("No valid MSG_SYS_UNIQUE_ID received.");
        }
    }

    public SysUniqueIdResponse(byte[] addr, int num, long uniqueId) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_SYS_UNIQUE_ID, NodeUtils.getUniqueId(uniqueId));
    }

    // #define MSG_SYS_UNIQUE_ID (MSG_USYS + 0x04) // 1:class, 2:classx, 3:vid, 4..7:pid+uid, [7..11:
    // config_fingerprint]

    public String getName() {
        return "MSG_SYS_UNIQUE_ID";
    }

    public byte[] getUniqueId() {
        return getData();
    }

    public long getConfigFingerPrint() {
        long configFingerPrint = -1;
        if (getData() != null && getData().length > 6) {
            configFingerPrint = ByteUtils.getDWORD(getData(), 7);
        }
        return configFingerPrint;
    }
}
