package org.bidib.jbidibc.core.schema;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.FeatureCode;
import org.bidib.jbidibc.core.schema.bidib2.InputKey;
import org.bidib.jbidibc.core.schema.bidib2.MessageType;
import org.bidib.jbidibc.core.schema.bidib2.Node;
import org.bidib.jbidibc.core.schema.bidib2.ObjectFactory;
import org.bidib.jbidibc.core.schema.bidib2.OutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.OutputLight;
import org.bidib.jbidibc.core.schema.bidib2.OutputMotor;
import org.bidib.jbidibc.core.schema.bidib2.OutputServo;
import org.bidib.jbidibc.core.schema.bidib2.OutputSound;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitchPair;
import org.bidib.jbidibc.core.schema.bidib2.Port;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.jbidibc.core.schema.exception.NodeNotAvailableException;
import org.bidib.jbidibc.core.schema.validation.XsdValidationLoggingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javanet.staxutils.IndentingXMLEventWriter;

public class BidibFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.jbidibc.core.schema.bidib2";

    public static final String XSD_LOCATION = "/xsd/bidib2.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/schema/bidib/2.0 xsd/bidib2.xsd";

    private static JAXBContext jaxbContext;

    public BidibFactory() {
    }

    public static List<MessageType> getMessageTypes() {

        return new BidibFactory().loadMessageTypes();
    }

    public static List<FeatureCode> getFeatureCodes() {

        return new BidibFactory().loadFeatureCodes();
    }

    protected List<FeatureCode> loadFeatureCodes() {
        final String fileName = "/xml/protocol/Protocol.bidib";

        InputStream is = BidibFactory.class.getResourceAsStream(fileName);
        if (is != null) {
            BiDiB bidib = loadBiDiBFile(is);
            if (bidib != null) {
                try {
                    List<FeatureCode> featureCodes = bidib.getProtocol().getFeatureCodes().getFeatureCode();
                    LOGGER.info("Loaded number of featureCodes: {}", featureCodes.size());

                    return featureCodes;
                }
                catch (Exception ex) {
                    LOGGER.warn("Get the feature codes failed.", ex);
                }
            }
        }
        else {
            LOGGER.warn("Load protocol file failed.");
        }
        return null;
    }

    protected List<MessageType> loadMessageTypes() {
        final String fileName = "/xml/protocol/Protocol.bidib";

        InputStream is = BidibFactory.class.getResourceAsStream(fileName);
        if (is != null) {
            BiDiB bidib = loadBiDiBFile(is);
            if (bidib != null) {
                try {
                    List<MessageType> messageTypes = bidib.getProtocol().getMessageTypes().getMessageType();
                    LOGGER.info("Loaded number of messageTypes: {}", messageTypes.size());
                    // tweakMessageTypes(messageTypes);
                    return messageTypes;
                }
                catch (Exception ex) {
                    LOGGER.warn("Get the message types failed.", ex);
                }
            }
        }
        else {
            LOGGER.warn("Load protocol file failed.");
        }
        return null;
    }

    /**
     * Load the BiDiB from the provided file.
     * 
     * @param bidibFile
     *            the file
     * @return the BiDiB data instance
     * @throws FileNotFoundException
     */
    public static BiDiB loadBiDiBFile(File bidibFile) throws FileNotFoundException {

        BiDiB bidib = null;
        InputStream is = null;

        try {
            is = new FileInputStream(bidibFile);
            bidib = loadBiDiBFile(is);
        }
        catch (InvalidSchemaException ex) {
            LOGGER.warn("Load bidib file failed due to schema violation. Try to transform.", ex);

            if (is != null) {
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }

            StringBuilder transformed = new StringBuilder();

            FileOutputStream fos = null;
            try {
                is = new FileInputStream(bidibFile);
                transform(is, transformed);

                LOGGER.info("Save transformed bidib to file: {}", bidibFile.getAbsolutePath());

                // save the transformed bidib file
                fos = new FileOutputStream(bidibFile);
                IOUtils.write(transformed, fos, Charset.forName("UTF-8"));
            }
            catch (XMLStreamException | IOException e) {
                LOGGER.warn("Transform bidib file failed: {}", bidibFile.getAbsolutePath(), e);
            }
            finally {
                if (fos != null) {
                    try {
                        fos.flush();
                        fos.close();
                    }
                    catch (Exception e1) {
                        LOGGER.warn("Close output stream failed.", e1);
                    }
                }
            }

            // try to load after transformation
            LOGGER.info("Try to load the bidib file after transformation.");
            try {
                is = new FileInputStream(bidibFile);
                bidib = loadBiDiBFile(is);
            }
            catch (FileNotFoundException e1) {
                LOGGER.info("No Products file found.");
            }
        }
        // catch (FileNotFoundException ex) {
        // LOGGER.info("No Products file found.");
        // }
        finally {
            if (is != null) {
                LOGGER.info("Close input stream.");
                try {
                    is.close();
                }
                catch (Exception e1) {
                    LOGGER.warn("Close output stream failed.", e1);
                }
                is = null;
            }
        }
        return bidib;
    }

    public static BiDiB loadBiDiBFile(final InputStream is) {

        BiDiB bidib = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", JAXB_PACKAGE);
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            try {
                JAXBElement<BiDiB> jaxbElement = (JAXBElement<BiDiB>) unmarshaller.unmarshal(xmlr, BiDiB.class);
                bidib = jaxbElement.getValue();
            }
            catch (UnmarshalException ex) {
                LOGGER.warn("Load content from file failed.", ex);
                if (ex.getLinkedException() instanceof SAXException) {
                    validate(is);
                    throw new InvalidSchemaException("Load BiDiB from file failed");
                }
            }

        }
        catch (JAXBException | XMLStreamException | SAXException ex) {
            LOGGER.warn("Load content from file failed.", ex);
        }
        return bidib;
    }

    private static List<String> validate(final InputStream is) {
        List<String> errors = null;

        if (is instanceof FileInputStream) {
            FileInputStream fis = (FileInputStream) is;
            try {
                LOGGER.info("Try to set file position to 0.");
                fis.getChannel().position(0);
            }
            catch (IOException e) {
                LOGGER.warn("Set file position to 0 failed.", e);
            }
        }

        StreamSource inputStreamSource = new StreamSource(is);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));

        XsdValidationLoggingErrorHandler errorHandler = new XsdValidationLoggingErrorHandler();

        try {
            Schema schema = schemaFactory.newSchema(streamSource);

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);

            validator.validate(inputStreamSource);
        }
        catch (IOException ex) {
            LOGGER.warn("Validate failed.", ex);
        }
        catch (SAXException ex) {
            LOGGER.warn("Validate failed.", ex);
        }

        errors = errorHandler.getErrors();
        LOGGER.info("Found errors: {}", errors);

        return errors;
    }

    /**
     * Save the bidib instance to a file.
     * 
     * @param bidib
     *            the bidib instance
     * @param fileName
     *            the file
     * @param gzip
     *            use gzip
     */
    public static void saveBiDiB(BiDiB bidib, String fileName, boolean gzip) {

        LOGGER.info("Save bidib content to file: {}, bidib: {}", fileName, bidib);
        OutputStream os = null;
        OutputStreamWriter osw = null;
        boolean passed = false;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", JAXB_PACKAGE);
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, JAXB_SCHEMA_LOCATION);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            marshaller.setSchema(schema);

            os = new BufferedOutputStream(new FileOutputStream(fileName));
            if (gzip) {
                LOGGER.debug("Use gzip to compress bidib.");
                os = new GZIPOutputStream(os);
            }

            JAXBElement<BiDiB> jaxbElement = new ObjectFactory().createBiDiB(bidib);
            osw = new OutputStreamWriter(os, Charset.forName("UTF-8"));
            marshaller.marshal(jaxbElement, osw);

            osw.flush();
            os.flush();

            LOGGER.info("Save bidib content to file passed: {}", fileName);

            passed = true;
        }
        catch (Exception ex) {
            // TODO add better exception handling
            LOGGER.warn("Save bidib failed.", ex);

            throw new RuntimeException("Save bidib failed.", ex);
        }
        finally {
            if (osw != null) {
                try {
                    osw.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputStreamWriter failed.", ex);
                }
                osw = null;
            }

            if (os != null) {
                try {
                    os.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputStream failed.", ex);
                }
                os = null;
            }

            if (!passed) {
                LOGGER.warn("Delete the file because the export has failed.");
                FileUtils.deleteQuietly(new File(fileName));
            }
        }
    }

    /**
     * Get all ports of the requested port type from the provided list of ports.
     * 
     * @param allPorts
     *            the list of ports
     * @param type
     *            the port type
     * @return the list of ports with the requested port type
     */
    public static <PT extends Port> List<PT> getPortsOfType(final List<Port> allPorts, final LcOutputType type) {
        List<PT> ports = new LinkedList<>();
        Collection<Port> selection = CollectionUtils.select(allPorts, new Predicate<Port>() {

            @Override
            public boolean evaluate(Port port) {
                switch (type) {
                    case LIGHTPORT:
                        if (port instanceof OutputLight) {
                            return true;
                        }
                        break;
                    case BACKLIGHTPORT:
                        if (port instanceof OutputBacklight) {
                            return true;
                        }
                        break;
                    case SERVOPORT:
                        if (port instanceof OutputServo) {
                            return true;
                        }
                        break;
                    case SWITCHPORT:
                        if (port instanceof OutputSwitch) {
                            return true;
                        }
                        break;
                    case SWITCHPAIRPORT:
                        if (port instanceof OutputSwitchPair) {
                            return true;
                        }
                        break;
                    case SOUNDPORT:
                        if (port instanceof OutputSound) {
                            return true;
                        }
                        break;
                    case MOTORPORT:
                        if (port instanceof OutputMotor) {
                            return true;
                        }
                        break;
                    case INPUTPORT:
                        if (port instanceof InputKey) {
                            return true;
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });

        ports.addAll((Collection<? extends PT>) selection);

        return ports;
    }

    public static Node getNode(final BiDiB bidib, int index) {
        Node node = null;
        try {
            node = bidib.getNodes().getNode().get(index);
        }
        catch (Exception ex) {
            LOGGER.warn("Get node failed, index: {}, bidib: {}", index, bidib, ex);

            throw new NodeNotAvailableException("No node available at index: " + index);
        }
        return node;
    }

    public static Node getNodeOrNull(final BiDiB bidib, int index) {
        Node node = null;
        try {
            node = bidib.getNodes().getNode().get(index);
        }
        catch (Exception ex) {
            LOGGER.warn("Get node failed, index: {}", index, ex);
        }
        return node;
    }

    /**
     * Prepare the filename based on the provided uniqueId.
     * 
     * @param uniqueId
     *            the uniqueId
     * @return the hex formatted, zero-leading 14-digits representation of the uniqueId
     */
    public static String prepareNodeFilename(long uniqueId) {
        String nodeFileName = String.format("%014X.xml", uniqueId & 0xffffffffffffffL);
        return nodeFileName;
    }

    /**
     * Transform the data from source.
     * 
     * @param source
     *            the source data
     * @param target
     *            the target
     * @throws XMLStreamException
     * @throws IOException
     */
    public static void transform(final InputStream source, final StringBuilder target)
        throws XMLStreamException, IOException {

        XMLInputFactory inFactory = XMLInputFactory.newInstance();
        XMLEventReader eventReader = inFactory.createXMLEventReader(source);
        XMLOutputFactory factory = XMLOutputFactory.newInstance();

        StringBuilderWriter stringBuilderWriter = new StringBuilderWriter(target);

        XMLEventWriter writer =
            new IndentingXMLEventWriter(factory.createXMLEventWriter(new BufferedWriter(stringBuilderWriter)));

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();

        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            // LOGGER.info("Current event: {}", event);

            if (event.getEventType() == XMLEvent.START_ELEMENT) {
                LOGGER.debug("Current element name: {}", event.asStartElement().getName().toString());
                if (event.asStartElement().getName().getLocalPart().equals("node")) {

                    // create new node element
                    StartElement node = eventFactory.createStartElement("", null, "node");
                    writer.add(node);

                    // get the original element as startElement
                    StartElement nodeElement = event.asStartElement();
                    QName uniqueIdName = new QName("uniqueId");

                    // get the original attributes
                    Iterator<Attribute> attributes = nodeElement.getAttributes();

                    // iterate over the attributes and copy the attributes to the new node element
                    while (attributes.hasNext()) {
                        Attribute attribute = attributes.next();
                        if (attribute.getName().equals(uniqueIdName)) {
                            LOGGER.info("Found uniqueId attribute: {}", attribute);

                            String value = attribute.getValue();

                            // check if we must tweak the values
                            if (StringUtils.startsWithIgnoreCase(value, "0x")) {
                                // we must strip the leading '0x'
                                long uniqueId = Long.parseLong(value.substring(2), 16);

                                value = Long.toString(uniqueId);
                                Attribute newAttribute = eventFactory.createAttribute("uniqueId", value);
                                writer.add(newAttribute);
                            }
                            else {
                                writer.add(attribute);
                            }
                        }
                        else {
                            writer.add(attribute);
                        }
                    }
                }
                else {
                    writer.add(event);
                }
            }
            else {
                writer.add(event);
            }
        }
        writer.close();
    }
}
