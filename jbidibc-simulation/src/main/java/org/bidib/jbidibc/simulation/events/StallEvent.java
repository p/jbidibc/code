package org.bidib.jbidibc.simulation.events;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class StallEvent {

    private final String nodeAddr;

    private boolean stall;

    public StallEvent(final String nodeAddr, final boolean stall) {
        this.nodeAddr = nodeAddr;
        this.stall = stall;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public boolean getStall() {
        return stall;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
