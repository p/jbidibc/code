package org.bidib.jbidibc.debug;

import java.util.List;

import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.helpers.Context;

public interface DebugInterface {

    /**
     * Perform initialization task.
     */
    void initialize();

    /**
     * @return returns the list of serial port identifiers that are available in the system.
     */
    List<String> getPortIdentifiers();

    /**
     * @return the message processor
     */
    DebugMessageProcessor getMessageReceiver();

    /**
     * Close the port.
     */
    void close();

    /**
     * @return port is opened
     */
    boolean isOpened();

    /**
     * Open connection to the specified port.
     * 
     * @param portName
     *            the port identifier
     * @param baudRate
     *            the baud rate to use
     * @param connectionListener
     *            the connection listener
     * @param context
     *            the context
     * @throws PortNotFoundException
     * @throws PortNotOpenedException
     */
    void open(String portName, int baudRate, ConnectionListener connectionListener, Context context)
        throws PortNotFoundException, PortNotOpenedException;

    /**
     * @param message
     *            the message to send
     */
    void send(final String message, LineEndingEnum lineEnding);

    /**
     * @param content
     *            the content to send
     */
    void send(final byte[] content);
}
