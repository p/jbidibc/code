package org.bidib.jbidibc.simulation.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.assertj.core.api.Assertions;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.nodes.MasterType;
import org.bidib.jbidibc.simulation.nodes.NodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SimulationFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationFactoryTest.class);

    private static final String EXPORTED_SIM_TARGET_DIR = "target/exported-sim";

    private final long MASTER_UNIQUE = 0xda000d680064eaL;

    @BeforeTest
    public void prepare() {
        LOGGER.info("Create export directory: {}", EXPORTED_SIM_TARGET_DIR);

        try {
            FileUtils.forceMkdir(new File(EXPORTED_SIM_TARGET_DIR));
        }
        catch (IOException e) {
            LOGGER.warn("Create export directory failed: " + EXPORTED_SIM_TARGET_DIR, e);
        }
    }

    @Test
    public void toNodeType() {

        final SimulationFactory factory = new SimulationFactory();
        factory.initialize();

        Node masterNode = new Node(1, NodeUtils.ROOT_ADDRESS, MASTER_UNIQUE);
        masterNode.setProtocolVersion(ProtocolVersion.VERSION_0_7);
        masterNode.setSoftwareVersion(SoftwareVersion.build(1, 3, 7));

        NodeType masterNodeType = factory.toNodeType(masterNode);

        Assertions.assertThat(masterNodeType).isNotNull().isInstanceOf(MasterType.class);
    }
}
