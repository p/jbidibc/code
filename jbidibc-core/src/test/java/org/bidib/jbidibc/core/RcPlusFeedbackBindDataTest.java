package org.bidib.jbidibc.core;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RcPlusFeedbackBindDataTest {

    @Test
    public void fromByteArrayTest() {

        byte[] message = { 0x0A, 0x00, 0x30, (byte) 0xAB, 0x05, 0x02, 0x10, 0x00, 0x01, (byte) 0x91, 0x3E };

        RcPlusFeedbackBindData feedbackBindData = RcPlusFeedbackBindData.fromByteArray(message, 6);
        Assert.assertNotNull(feedbackBindData);
        Assert.assertEquals(feedbackBindData.getUniqueId(), new DecoderUniqueIdData(0x91010010L, 0x3E));
        // TODO change after address is delivered
        Assert.assertNull(feedbackBindData.getAddress());
    }
}
