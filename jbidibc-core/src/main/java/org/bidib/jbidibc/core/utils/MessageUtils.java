package org.bidib.jbidibc.core.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.MapUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.CRC8;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.enumeration.AccessoryOkayEnum;
import org.bidib.jbidibc.core.enumeration.AnalogPortEnum;
import org.bidib.jbidibc.core.enumeration.BacklightPortEnum;
import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.enumeration.DirectionEnum;
import org.bidib.jbidibc.core.enumeration.FeedbackPortEnum;
import org.bidib.jbidibc.core.enumeration.FlagEnum;
import org.bidib.jbidibc.core.enumeration.InputPortEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.enumeration.MacroEnum;
import org.bidib.jbidibc.core.enumeration.MotorPortEnum;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.ServoPortEnum;
import org.bidib.jbidibc.core.enumeration.SoundPortEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.Int32PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.bidib.jbidibc.core.port.RgbPortConfigValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageUtils.class);

    /**
     * Convert the value of an output type to the corresponding BidibEnum.
     * 
     * @param outputType
     *            the output type
     * @param value
     *            the value.
     * @return the BidibEnum value
     */
    public static BidibEnum toPortStatus(LcOutputType outputType, byte value) {
        BidibEnum result = null;

        switch (outputType) {
            case ANALOGPORT:
                result = AnalogPortEnum.valueOf(value);
                break;
            case BACKLIGHTPORT:
                // the pseudo status is always start, because the value is delivered
                result = BacklightPortEnum.START;
                break;
            case LIGHTPORT:
                result = LightPortEnum.valueOf(value);
                break;
            case MOTORPORT:
                result = MotorPortEnum.valueOf(value);
                break;
            case SERVOPORT:
                // the pseudo status is always start, because the value is delivered
                result = ServoPortEnum.START;
                break;
            case SOUNDPORT:
                result = SoundPortEnum.valueOf(value);
                break;
            case SWITCHPORT:
                result = SwitchPortEnum.valueOf(value);
                break;
            case SWITCHPAIRPORT:
                result = SwitchPortEnum.valueOf(value);
                break;
            case INPUTPORT:
                result = InputPortEnum.valueOf(value);
                break;
            case FEEDBACKPORT:
                result = FeedbackPortEnum.valueOf(value);
                break;
            case END_OF_MACRO:
            case ACCESSORY_OKAY_INPUTQUERY0:
            case ACCESSORY_OKAY_INPUTQUERY1:
            case ACCESSORY_OKAY_NF:
            case BEGIN_CRITICAL:
            case END_CRITICAL:
            case FLAG_CLEAR:
            case FLAG_QUERY:
            case FLAG_QUERY0:
            case FLAG_QUERY1:
            case FLAG_SET:
            case INPUT_QUERY0:
            case INPUT_QUERY1:
            case DELAY_FIXED:
            case RANDOM_DELAY:
            case SERVOMOVE_QUERY:
            case START_MACRO:
            case STOP_MACRO:
            case WAIT_FOR_END_OF_MACRO:
                break;
            default:
                LOGGER.warn("No port status available for output type: {}", outputType);
                throw new InvalidConfigurationException("No port status available for output type: " + outputType);

        }

        return result;
    }

    /**
     * Get the port status value from the provided macro step.
     * 
     * @param macro
     *            the macro step
     * @return the port status
     */
    public static byte getPortStatus(LcMacro macro, LcOutputType outputType) {
        byte result = 0;
        BidibEnum status = macro.getStatus(outputType);

        if (status != null) {
            if (status instanceof AnalogPortEnum) {
                result = ((AnalogPortEnum) status).getType();
            }
            else if (status instanceof BacklightPortEnum) {
                result = macro.getPortValue(outputType);
            }
            else if (status instanceof FlagEnum) {
                result = ((FlagEnum) status).getType();
            }
            else if (status instanceof LightPortEnum) {
                result = ((LightPortEnum) status).getType();
            }
            else if (status instanceof MotorPortEnum) {
                result = ((MotorPortEnum) status).getType();
            }
            else if (status instanceof ServoPortEnum) {
                result = macro.getPortValue(outputType);
            }
            else if (status instanceof SoundPortEnum) {
                result = ((SoundPortEnum) status).getType();
            }
            else if (status instanceof SwitchPortEnum) {
                result = ((SwitchPortEnum) status).getType();
            }
            else if (status instanceof AccessoryOkayEnum) {
                result = ((AccessoryOkayEnum) status).getType();
            }
            else if (status instanceof MacroEnum) {
                result = ((MacroEnum) status).getType();
            }
            else {
                LOGGER.warn("Unsupported macro status detected: {}", status);
            }
        }
        return result;
    }

    // the value is only available for servoport and backlightport
    public static byte getPortValue(LcOutputType outputType, byte value) {
        byte result = 0;

        switch (outputType) {
            case BACKLIGHTPORT:
            case SERVOPORT:
                result = value;
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * Split the byte array from the outputStream into separate messages. The CRC value at the end is calculated over
     * the whole array. The outputStream must only contain messages that are unescaped already. The outputStream is
     * reset after processing.
     * 
     * @param outputStream
     *            stream with byte array containing at least one message
     * @param checkCRC
     *            flag if we must check the CRC
     * 
     * @return list of the separated messages
     * 
     * @throws ProtocolException
     *             Thrown if the CRC failed.
     */
    public static Collection<byte[]> splitBidibMessages(final ByteArrayOutputStream outputStream, boolean checkCRC)
        throws ProtocolException {
        Collection<byte[]> result = new LinkedList<byte[]>();
        int index = 0;

        final byte[] output = outputStream.toByteArray();

        LOGGER.trace("splitMessages: {}", output);

        while (index < output.length) {
            int size = ByteUtils.getInt(output[index]) + 1 /* len */;
            LOGGER.trace("Current size: {}", size);

            if (size <= 0) {
                throw new ProtocolException("cannot split messages, array size is " + size);
            }

            byte[] message = new byte[size];

            try {
                System.arraycopy(output, index, message, 0, message.length);
            }
            catch (ArrayIndexOutOfBoundsException ex) {
                LOGGER
                    .warn("Failed to copy, msg.len: {}, size: {}, output.len: {}, index: {}, output: {}",
                        message.length, size, output.length, index, ByteUtils.bytesToHex(output));
                throw new ProtocolException("Copy message data to buffer failed.");
            }
            result.add(message);
            index += size;

            if (checkCRC) {
                // CRC
                if (index == output.length - 1) {
                    int crc = 0;
                    int crcIndex = 0;
                    for (crcIndex = 0; crcIndex < output.length - 1; crcIndex++) {
                        crc = CRC8.getCrcValue((output[crcIndex] ^ crc) & 0xFF);
                    }
                    if (crc != (output[crcIndex] & 0xFF)) {
                        throw new ProtocolException(
                            "CRC failed: should be " + crc + " but was " + (output[crcIndex] & 0xFF));
                    }
                    break;
                }
            }
        }

        // clear the content in the outputStream because the message is processed
        outputStream.reset();

        return result;
    }

    /**
     * Convert the current value.
     * 
     * @param current
     *            the current in bidib units
     * @return the current value
     */
    public static int convertCurrent(int current) {
        if (current > 15) {
            if (current >= 16 && current <= 63) {
                current = (current - 12) * 4;
            }
            else if (current >= 64 && current <= 127) {
                current = (current - 51) * 16;
            }
            else if (current >= 128 && current <= 191) {
                current = (current - 108) * 64;
            }
            else if (current >= 192 && current <= 250) {
                current = (current - 171) * 256;
            }
            else {
                current = 0;
            }
        }
        return current;
    }

    /**
     * Convert the voltage value.
     * 
     * @param value
     *            the voltage in bidib units
     * @return the voltage value
     */
    public static int convertVoltageValue(int value) {
        int result = 0;

        if (value >= 1 && value <= 250) {
            result = value;
        }
        return result;
    }

    /**
     * Convert the temperature value.
     * 
     * @param value
     *            the temperature in bidib units.
     * @return the temperature value
     */
    public static int convertTemperatureValue(int value) {
        int result = 0;

        if (value >= 1 && value <= 250) {
            result = value;
        }
        return result;
    }

    /**
     * Create the LcCOnfigX from the provided byte array.
     * 
     * @param data
     *            the encoded byte array
     * @return the LcConfigX instance
     */
    public static LcConfigX getLcConfigX(byte[] data) {

        Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();
        // get the values
        if (data.length > 2) {
            try (ByteArrayInputStream bais = new ByteArrayInputStream(data)) {

                // skip the port
                bais.skip(2);

                while (bais.available() > 0) {
                    byte pEnum = ByteUtils.getLowByte(bais.read());
                    int bytesRead = 0;
                    if ((pEnum & 0x80) == 0x80) {
                        switch (pEnum) {
                            case BidibLibrary.BIDIB_PCFG_CONTINUE:
                                LOGGER.info("Continue detected, more config will be received.");
                                values.put(pEnum, null);
                                break;
                            case BidibLibrary.BIDIB_PCFG_RGB:
                                // RGB
                                byte[] rgbValue = new byte[3];
                                bytesRead = bais.read(rgbValue);
                                LOGGER
                                    .info("Read a RGB (0x{}) value: {}, bytesRead: {}, pEnum: {}",
                                        ByteUtils.byteToHex(pEnum), ByteUtils.toString(rgbValue), bytesRead,
                                        formatPortConfigKey(pEnum));

                                Integer integerValue = ByteUtils.getRGB(rgbValue);
                                values.put(pEnum, new RgbPortConfigValue(integerValue));
                                break;
                            case BidibLibrary.BIDIB_PCFG_RECONFIG:
                                // RECONFIG
                                byte[] reconfigValue = new byte[3];
                                bytesRead = bais.read(reconfigValue);
                                LOGGER
                                    .info("Read a RECONFIG (0x{}) value: {}, bytesRead: {}, pEnum: {}",
                                        ByteUtils.byteToHex(pEnum), ByteUtils.toString(reconfigValue), bytesRead,
                                        formatPortConfigKey(pEnum));

                                integerValue = ByteUtils.getReconfig(reconfigValue);
                                values.put(pEnum, new ReconfigPortConfigValue(integerValue));
                                break;
                            default:
                                // int32
                                byte[] intValue = new byte[4];
                                bytesRead = bais.read(intValue);
                                LOGGER
                                    .info("Read a int32 (0x{}) value: {}, bytesRead: {}, pEnum: {}",
                                        ByteUtils.byteToHex(pEnum), ByteUtils.toString(intValue), bytesRead,
                                        formatPortConfigKey(pEnum));

                                Long longValue = ByteUtils.getDWORD(intValue);
                                values.put(pEnum, new Int32PortConfigValue(longValue));
                                break;
                        }
                    }
                    else if ((pEnum & 0x40) == 0x40) {
                        // int16
                        byte[] intValue = new byte[2];
                        bytesRead = bais.read(intValue);
                        LOGGER
                            .info("Read a int16 (0x{}) value: {}, bytesRead: {}, pEnum: {}", ByteUtils.byteToHex(pEnum),
                                ByteUtils.toString(intValue), bytesRead, formatPortConfigKey(pEnum));

                        Integer integerValue = ByteUtils.getWORD(intValue);
                        values.put(pEnum, new Int16PortConfigValue(integerValue));
                    }
                    else { // byte
                        byte[] byteVal = new byte[1];
                        bytesRead = bais.read(byteVal);
                        LOGGER
                            .info("Read a byte (0x{}) value: {}, bytesRead: {}, pEnum: {}", ByteUtils.byteToHex(pEnum),
                                ByteUtils.toString(byteVal), bytesRead, formatPortConfigKey(pEnum));

                        Byte byteValue = Byte.valueOf(byteVal[0]);
                        values.put(pEnum, new BytePortConfigValue(byteValue));
                    }
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Read content of message failed.", ex);
            }
        }

        BidibPort bidibPort = BidibPort.prepareBidibPort(data[0], data[1]);

        return new LcConfigX(bidibPort, values);
    }

    private static String formatPortConfigKey(byte pEnum) {
        try {
            PortConfigKeys pck = PortConfigKeys.valueOf(pEnum);
            return pck.name();
        }
        catch (Exception ex) {
            LOGGER.warn("Convert pEnum to PortConfigKeys failed: {}", ex.getMessage());
            return ByteUtils.byteToHex(pEnum);
        }
    }

    public static byte[] getCodedPortConfig(final LcConfigX lcConfigX, final PortModelEnum portModel) {
        final LcOutputType outputType = lcConfigX.getOutputType(portModel);
        final int outputNumber = lcConfigX.getOutputNumber(portModel);
        final Map<Byte, PortConfigValue<?>> values = new HashMap<>();
        values.putAll(lcConfigX.getPortConfig());

        byte outputTypeValue = outputType.getType();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        switch (portModel) {
            case flat:
                baos.write(ByteUtils.getLowByte(outputNumber));
                baos.write(ByteUtils.getHighByte(outputNumber));
                break;
            default:
                baos.write(outputTypeValue);
                baos.write(ByteUtils.getLowByte(outputNumber, 0x7F));
                break;
        }

        if (values.containsKey(BidibLibrary.BIDIB_PCFG_RECONFIG)) {
            LOGGER.info("Found BIDIB_PCFG_RECONFIG.");
            // send reconfig first
            // RECONFIG
            ReconfigPortConfigValue reconfigPortConfigValue =
                (ReconfigPortConfigValue) values.get(BidibLibrary.BIDIB_PCFG_RECONFIG);
            int pIntValue = reconfigPortConfigValue.getValue().intValue();
            try {
                baos.write(BidibLibrary.BIDIB_PCFG_RECONFIG);
                baos.write(ByteUtils.toReconfig(pIntValue));
            }
            catch (IOException ex) {
                LOGGER.warn("Prepare coded port config failed.", ex);
            }
            values.remove(BidibLibrary.BIDIB_PCFG_RECONFIG);
        }

        // prepare the values
        if (MapUtils.isNotEmpty(values)) {
            try {
                for (Entry<Byte, PortConfigValue<?>> entry : values.entrySet()) {
                    byte pEnum = entry.getKey();
                    if (entry.getValue() != null) {
                        baos.write(pEnum);
                        if ((pEnum & 0x80) == 0x80) {
                            switch (pEnum) {
                                case BidibLibrary.BIDIB_PCFG_CONTINUE:
                                    LOGGER.info("Continue detected, more config will be sent.");
                                    break;
                                case BidibLibrary.BIDIB_PCFG_RGB:
                                    // RGB
                                    RgbPortConfigValue rgbPortConfigValue = (RgbPortConfigValue) entry.getValue();
                                    int pIntValue = rgbPortConfigValue.getValue().intValue();
                                    baos.write(ByteUtils.toRGB(pIntValue));
                                    break;
                                case BidibLibrary.BIDIB_PCFG_RECONFIG:
                                    // RECONFIG
                                    ReconfigPortConfigValue reconfigPortConfigValue =
                                        (ReconfigPortConfigValue) entry.getValue();
                                    pIntValue = reconfigPortConfigValue.getValue().intValue();
                                    baos.write(ByteUtils.toReconfig(pIntValue));
                                    break;
                                default:
                                    // LOGGER.info("Current enum value: {}", ByteUtils.byteToHex(pEnum));
                                    // int32
                                    Int32PortConfigValue int32PortConfigValue = (Int32PortConfigValue) entry.getValue();
                                    pIntValue = int32PortConfigValue.getValue().intValue();
                                    baos.write(ByteUtils.toDWORD(pIntValue));
                                    break;
                            }
                        }
                        else if ((pEnum & 0x40) == 0x40) {
                            // int16
                            Int16PortConfigValue int16PortConfigValue = (Int16PortConfigValue) entry.getValue();
                            int pIntValue = int16PortConfigValue.getValue().intValue();
                            baos.write(ByteUtils.toWORD(pIntValue));
                        }
                        else { // byte
                            BytePortConfigValue bytePortConfigValue = (BytePortConfigValue) entry.getValue();
                            byte pValue = bytePortConfigValue.getValue().byteValue();
                            baos.write(pValue);
                        }
                    }
                    else {
                        LOGGER.warn("No value available for pEnum: {}", ByteUtils.toString(new byte[] { pEnum }));
                    }
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Prepare coded port config failed.", ex);
            }
        }
        return baos.toByteArray();
    }

    /**
     * Format the sysError to a human readable string.
     * 
     * @param addr
     *            the address of the node
     * @param sysErrorEnum
     *            the sysError enum value
     * @param data
     *            the optional data
     * @return the formatted string
     */
    public static String formatSysError(byte[] addr, SysErrorEnum sysErrorEnum, byte... data) {
        StringBuilder sb = new StringBuilder();
        if (sysErrorEnum != null) {
            try {
                switch (sysErrorEnum) {
                    case BIDIB_ERR_NONE:
                        sb
                            .append("BIDIB_ERR_NONE for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append(")");
                        break;
                    case BIDIB_ERR_TXT:
                        sb
                            .append("BIDIB_ERR_TXT for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append("), text len: ")
                            .append(ByteUtils.getInt(data[0])).append(", text: ").append(ByteUtils.cstr(data, 0));
                        break;
                    case BIDIB_ERR_CRC:
                        sb
                            .append("BIDIB_ERR_CRC for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType()))
                            .append("), sequence num of invalid message: ").append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_SIZE:
                        sb
                            .append("BIDIB_ERR_SIZE for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType()))
                            .append("), sequence num of invalid message: ").append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_SEQUENCE:
                        sb
                            .append("BIDIB_ERR_SEQUENCE for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append("), last correct sequence: ")
                            .append(ByteUtils.getInt(data[0]));
                        if (data.length > 1) {
                            sb.append(", current sequence: ").append(ByteUtils.getInt(data[1]));
                        }
                        break;
                    case BIDIB_ERR_PARAMETER:
                        sb
                            .append("BIDIB_ERR_PARAMETER for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType()))
                            .append("), sequence num of invalid message: ").append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_BUS:
                        sb
                            .append("BIDIB_ERR_BUS for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append("), error number: ")
                            .append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_ADDRSTACK:
                        sb
                            .append("BIDIB_ERR_ADDRSTACK for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append("), data: ")
                            .append(ByteUtils.toString(data));
                        break;
                    case BIDIB_ERR_IDDOUBLE:
                        sb
                            .append("BIDIB_ERR_IDDOUBLE for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append(")");
                        break;
                    case BIDIB_ERR_SUBCRC:
                        sb
                            .append("BIDIB_ERR_SUBCRC for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append("), local node addr: ")
                            .append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_SUBTIME:
                        sb
                            .append("BIDIB_ERR_SUBTIME for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", ByteUtils.getInt(sysErrorEnum.getType())))
                            .append("), local node addr: ").append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_SUBPAKET:
                        sb
                            .append("BIDIB_ERR_SUBPAKET for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append("), local node addr: ")
                            .append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_OVERRUN:
                        sb
                            .append("BIDIB_ERR_OVERRUN for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append(")");
                        break;
                    case BIDIB_ERR_HW:
                        sb
                            .append("BIDIB_ERR_HW for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType()))
                            .append("), manufacturer specific error number: ").append(ByteUtils.getInt(data[0]));
                        break;
                    case BIDIB_ERR_RESET_REQUIRED:
                        sb
                            .append("BIDIB_ERR_RESET_REQUIRED for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append(")");
                        break;
                    case BIDIB_ERR_NO_SECACK_BY_HOST:
                        sb
                            .append("BIDIB_ERR_NO_SECACK_BY_HOST for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append(")");
                        break;
                    default:
                        sb
                            .append("Unknown error for address: ").append(ByteUtils.bytesToHex(addr))
                            .append(", errorCode: ").append(sysErrorEnum.getType()).append(" (0x")
                            .append(String.format("%02x", sysErrorEnum.getType())).append(")");
                        break;
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Format error message failed.", ex);
                sb = new StringBuilder("Format error message failed. Check logfile!");
            }
        }
        return sb.toString();
    }

    public static int convertSpeed(int speed) {
        int result = 0;

        DirectionEnum direction = (speed > 0 ? DirectionEnum.FORWARD : DirectionEnum.BACKWARD);
        LOGGER.info("Convert speed: {}, direction: {}", speed, direction);

        // don't care about emergency stop (=> FS1)
        if (speed == 0) {
            // no change
            result = speed;
        }
        else if (speed > 0) {
            // forward
            result = speed + 1; // +1 to skip emergency stop
        }
        else {
            // backward
            speed = -(speed - 1);
            result = speed;
        }

        result = result & 0x7F;

        if (direction == DirectionEnum.FORWARD) {

            result |= 0x80;
        }

        LOGGER.info("Converted speed value: {}", result);

        return result;
    }

}
