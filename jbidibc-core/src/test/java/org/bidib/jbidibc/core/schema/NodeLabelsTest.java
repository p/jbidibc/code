package org.bidib.jbidibc.core.schema;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.bidib.jbidibc.core.schema.bidib2.Aspect;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.FeedbackPort;
import org.bidib.jbidibc.core.schema.bidib2.Macro;
import org.bidib.jbidibc.core.schema.bidib2.Node;
import org.bidib.jbidibc.core.schema.bidib2.OutputLight;
import org.bidib.jbidibc.core.schema.bidib2.OutputServo;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NodeLabelsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeLabelsTest.class);

    private static final String LABELS_FILENAME_NODE = "/xml-test/nodes/node-labels-test1.xml";

    private static final String LABELS_FILENAME_NODE2 = "/xml-test/nodes/xx05340D75001237.xml";

    @Test
    public void loadLabelsTest() throws URISyntaxException, FileNotFoundException {
        LOGGER.info("Load labels from BiDiB.");

        URL fileUrl = NodeLabelsTest.class.getResource(LABELS_FILENAME_NODE);
        Assert.assertNotNull(fileUrl);

        File labelsFile = new File(fileUrl.toURI());

        BiDiB bidib = BidibFactory.loadBiDiBFile(labelsFile);

        Assert.assertNotNull(bidib);

        List<Node> nodes = bidib.getNodes().getNode();
        Assert.assertNotNull(nodes);
        Assert.assertEquals(nodes.size(), 1);

        Node node = nodes.get(0);

        Assert.assertEquals(node.getUniqueId(), 0x05340D6B001234L);

        long uniqueId = node.getUniqueId();
        LOGGER.info("Current uniqueId: {} ({})", uniqueId, String.format("0x%08x", uniqueId));

        // get the ports
        Assert.assertNotNull(node.getPorts());

        List<Port> ports = node.getPorts().getPort();
        Assert.assertTrue(ports.size() > 0);

        verifyPort(ports.get(0), OutputLight.class, 0, "Light_0");
        verifyPort(ports.get(1), OutputLight.class, 1, "Light_1");

        verifyPort(ports.get(2), OutputSwitch.class, 0, "Switch_0");
        verifyPort(ports.get(3), OutputServo.class, 0, "Servo_0");

        List<FeedbackPort> feedbackPorts = node.getFeedbackPorts().getFeedbackPort();
        Assert.assertNotNull(feedbackPorts);
        Assert.assertEquals(feedbackPorts.size(), 1);
        Assert.assertEquals(feedbackPorts.get(0).getNumber(), 0);
        Assert.assertEquals(feedbackPorts.get(0).getName(), "Feedback_0");

        List<Accessory> accessories = node.getAccessories().getAccessory();
        Assert.assertNotNull(accessories);
        Assert.assertEquals(accessories.size(), 1);

        Accessory accessory = accessories.get(0);
        Assert.assertNotNull(accessory);

        Assert.assertEquals(accessory.getNumber(), 0);
        Assert.assertEquals(accessory.getName(), "Accessory Test 0");

        Assert.assertNotNull(accessory.getAspects());
        Assert.assertNotNull(accessory.getAspects().getAspect());
        List<Aspect> aspects = accessory.getAspects().getAspect();
        Assert.assertEquals(aspects.size(), 2);

        Aspect aspect = aspects.get(0);
        Assert.assertNotNull(aspect);
        Assert.assertEquals(aspect.getNumber(), Integer.valueOf(0));
        Assert.assertEquals(aspect.getName(), "Aspect Test 0");

        aspect = aspects.get(1);
        Assert.assertNotNull(aspect);
        Assert.assertEquals(aspect.getNumber(), Integer.valueOf(1));
        Assert.assertEquals(aspect.getName(), "Aspect Test 1");

        List<Macro> macros = node.getMacros().getMacro();
        Assert.assertNotNull(macros);
        Assert.assertEquals(macros.size(), 2);

        Macro macro = macros.get(0);
        Assert.assertNotNull(macro);
        Assert.assertEquals(macro.getNumber(), 0);
        Assert.assertEquals(macro.getName(), "Macro Test 0");

        macro = macros.get(1);
        Assert.assertNotNull(macro);
        Assert.assertEquals(macro.getNumber(), 1);
        Assert.assertEquals(macro.getName(), "Macro Test 1");

    }

    @Test
    public void loadLabels2Test() throws URISyntaxException, FileNotFoundException {
        LOGGER.info("Load labels from BiDiB.");

        URL fileUrl = NodeLabelsTest.class.getResource(LABELS_FILENAME_NODE2);
        Assert.assertNotNull(fileUrl);

        File labelsFile = new File(fileUrl.toURI());

        BiDiB bidib = BidibFactory.loadBiDiBFile(labelsFile);

        Assert.assertNotNull(bidib);

        List<Node> nodes = bidib.getNodes().getNode();
        Assert.assertNotNull(nodes);
        Assert.assertEquals(nodes.size(), 1);

        Node node = nodes.get(0);

        Assert.assertEquals(node.getUniqueId(), 0x90000D84002400L);

        long uniqueId = node.getUniqueId();
        LOGGER.info("Current uniqueId: {} ({})", uniqueId, String.format("0x%08x", uniqueId));

    }

    private void verifyPort(final Port port, Class<?> portClazz, int portNum, String portName) {
        LOGGER.info("Current port: {}", port);

        Assert.assertTrue(portClazz.isAssignableFrom(port.getClass()));
        Assert.assertEquals(port.getNumber(), portNum);
        Assert.assertEquals(port.getName(), portName);

    }
}
