package org.bidib.jbidibc.ch341a;

import org.bidib.jbidibc.ch341a.Helper.UCHARByReference;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.Memory;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.UCHAR;
import com.sun.jna.platform.win32.WinDef.ULONG;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.PointerByReference;

public class AT24CXImpl implements AT24CX {

    private static final Logger LOGGER = LoggerFactory.getLogger(AT24CXImpl.class);

    private int deviceAddress;

    private int pageSize;

    private Ch341a ch341a;

    private ULONG iIndex;

    public AT24CXImpl() {
        this(AT24CX_ID);
    }

    public AT24CXImpl(int i2cAddress) {
        // TODO Auto-generated constructor stub

        init(0, 32);

    }

    private void init(int index, int pageSize) {
        deviceAddress = AT24CX_ID | (index & 0x7);
        this.pageSize = pageSize;
    }

    @Override
    public void open(long ch341Index) {
        ch341a = Ch341a.INSTANCE;

        LOGGER.info("Open the CH341A device with index: {}", ch341Index);

        iIndex = new ULONG(ch341Index);
        HANDLE handle = ch341a.CH341OpenDevice(iIndex);
        LOGGER.info("Current handle: {}", handle);
    }

    public void close() {
        LOGGER.info("Close the CH341A device with index: {}", iIndex);
        // close the device
        ch341a.CH341CloseDevice(iIndex);

        iIndex = null;
    }

    @Override
    public byte read(int memAddress) {
        LOGGER.info("Read a byte at memory address: {}, deviceAddress: {}", memAddress,
            ByteUtils.byteToHex(deviceAddress));

        UCHAR iAddr = new UCHAR();
        iAddr.setValue(memAddress);
        UCHAR iDevice = new UCHAR();
        iDevice.setValue(AT24CX_ID);
        UCHARByReference oByte = new UCHARByReference();

        BOOL retVal = ch341a.CH341ReadI2C(iIndex, iDevice, iAddr, oByte);
        LOGGER.info("Current result: {}, read byte: {}", retVal.booleanValue(), oByte.getValue());

        return oByte.getValue().byteValue();
    }

    @Override
    public byte[] read(int address, int size) {

        ULONG iWriteLength = new ULONG(3); // I2C slave address and 2 byte address
        ULONG iReadLength = new ULONG(1);

        PointerByReference iWriteBuffer = new PointerByReference(new Memory(8));
        iWriteBuffer.getPointer().setByte(0, (byte) ((AT24CX_ID << 1) & 0xFF));
        iWriteBuffer.getPointer().setByte(1, (byte) ((address >> 8) & 0xFF));
        iWriteBuffer.getPointer().setByte(2, (byte) (address & 0xFF));

        LOGGER.info("CH341StreamI2C, prepared wr buffer: {}", iWriteBuffer.getPointer().getByteArray(0, 3));

        PointerByReference oReadBuffer = new PointerByReference(new Memory(8));

        BOOL retVal = ch341a.CH341StreamI2C(iIndex, iWriteLength, iWriteBuffer, iReadLength, oReadBuffer);
        LOGGER.info("CH341StreamI2C, current result: {}, read byte: {}", retVal.booleanValue(),
            oReadBuffer.getPointer().getByte(0));

        // TODO Auto-generated method stub
        return new byte[] { oReadBuffer.getPointer().getByte(0) };
    }

}
