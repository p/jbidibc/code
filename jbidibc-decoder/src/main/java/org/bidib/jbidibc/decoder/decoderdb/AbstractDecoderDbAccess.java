package org.bidib.jbidibc.decoder.decoderdb;

import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bidib.jbidibc.decoder.exception.DecoderDbAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.markusbernhardt.proxy.ProxySearch;

public abstract class AbstractDecoderDbAccess {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDecoderDbAccess.class);

    protected static final String REST_SERVICE_URI = "https://www.decoderdb.de";

    private boolean proxyNeedsUserAndPassword;

    /**
     * Fetch the data from the decoderDB.
     * 
     * @param login
     *            the login name
     * @param password
     *            the password
     * @param baseUrl
     *            the service url
     * @param urlPath
     *            the path
     * @return the fetched instance
     * @throws URISyntaxException
     * @throws MalformedURLException
     */
    public <T> T fetch(String login, char[] password, String baseUrl, String urlPath, Class<T> responseType) {

        String plainCreds = new StringBuilder(login).append(":").append(String.copyValueOf(password)).toString();
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        LOGGER.info("base64Creds: {}", base64Creds);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);

        try {
            RestTemplate restTemplate = createRestTemplate(baseUrl);

            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<T> response =
                restTemplate.exchange(baseUrl + urlPath, HttpMethod.GET, request, responseType);
            T responseBody = (T) response.getBody();

            LOGGER.info("Retrieved responseBody: {}", responseBody);

            return responseBody;
        }
        catch (MalformedURLException | URISyntaxException ex) {
            LOGGER.warn("Fetch data from decoderDb failed.", ex);
            throw new DecoderDbAccessException("Fetch data from decoderDb failed.", ex);
        }
    }

    protected RestTemplate createRestTemplate(String restServiceUri) throws MalformedURLException, URISyntaxException {

        CredentialsProvider credsProvider = null;

        if (proxyNeedsUserAndPassword) {
            final String username = "username";
            final String password = "pa$$word";
            final String proxyUrl = "proxy.nyc.bigtower.com";
            final int port = 8080;

            credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(new AuthScope(proxyUrl, port),
                new UsernamePasswordCredentials(username, password));
        }

        HttpHost myProxy = null;
        Proxy proxy = findProxy(new URL(REST_SERVICE_URI).toURI());
        if (!Proxy.NO_PROXY.equals(proxy)) {
            try {
                InetSocketAddress addr = (InetSocketAddress) proxy.address();
                final String proxyUrl = addr.getHostName();
                final int port = addr.getPort();

                myProxy = new HttpHost(proxyUrl, port);
            }
            catch (Exception ex) {
                LOGGER.warn("Prepare proxy HttpHost failed.", ex);
            }
        }

        HttpClientBuilder clientBuilder = HttpClientBuilder.create();

        if (myProxy != null) {
            clientBuilder.setProxy(myProxy);
            if (credsProvider != null) {
                clientBuilder.setDefaultCredentialsProvider(credsProvider);
            }
        }
        clientBuilder.disableCookieManagement();

        HttpClient httpClient = clientBuilder.build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(httpClient);

        return new RestTemplate(factory);
    }

    private Proxy findProxy(URI uri) throws URISyntaxException {

        // Use the static factory method getDefaultProxySearch to create a proxy search instance
        // configured with the default proxy search strategies for the current environment.
        ProxySearch proxySearch = ProxySearch.getDefaultProxySearch();

        // Invoke the proxy search. This will create a ProxySelector with the detected proxy settings.
        ProxySelector proxySelector = proxySearch.getProxySelector();

        if (proxySelector != null) {
            // Install this ProxySelector as default ProxySelector for all connections.
            ProxySelector.setDefault(proxySelector);

            Proxy proxy = (Proxy) ProxySelector.getDefault().select(uri).iterator().next();

            LOGGER.info("proxy type: {}", proxy.type());
            InetSocketAddress addr = (InetSocketAddress) proxy.address();
            if (addr == null) {
                LOGGER.info("No Proxy");
            }
            else {
                LOGGER.info("proxy hostname: {}", addr.getHostName());
                LOGGER.info("proxy port: {}", addr.getPort());

                return proxy;
            }
        }
        else {
            LOGGER.info("No proxy selector available.");
        }
        return Proxy.NO_PROXY;
    }

}
