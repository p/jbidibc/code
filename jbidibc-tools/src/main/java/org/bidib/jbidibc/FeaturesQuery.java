package org.bidib.jbidibc;

import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.node.BidibNode;

import com.beust.jcommander.Parameters;

/**
 * This commands reads the value of the specified CV from the specified node.
 * 
 */
@Parameters(separators = "=")
public class FeaturesQuery extends BidibNodeCommand {
    public static void main(String[] args) {
        run(new FeaturesQuery(), args);
    }

    public int execute() {
        int result = 20;

        try {
            openPort(getPortName(), null);

            Node node = findNode();

            if (node != null) {
                BidibNode bidibNode = getBidib().getNode(node);

                int featureCount = bidibNode.getFeatureCount();

                System.out.println("featureCount: " + featureCount); // NOSONAR
                Feature feature = null;
                while ((feature = bidibNode.getNextFeature()) != null) {
                    System.out.println("feature.type: " + feature.getType() + ", value: " + feature.getValue() // NOSONAR
                        + ", name: " + feature.getFeatureName());
                }
                System.out.println("Finished query features."); // NOSONAR
                result = 0;
            }
            else {
                System.err.println("node with unique id \"" + getNodeIdentifier() + "\" not found"); // NOSONAR
            }

            getBidib().close();
        }
        catch (PortNotFoundException ex) {
            System.err.println("The provided port was not found: " + ex.getMessage() // NOSONAR
                + ". Verify that the BiDiB device is connected.");
        }
        catch (Exception ex) {
            System.err.println("Execute command failed: " + ex); // NOSONAR
        }
        return result;
    }
}
