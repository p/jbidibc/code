package org.bidib.jbidibc.gateway.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;

public class PortEventListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortEventListener.class);

    @Subscribe
    public void portEvent(PortEvent event) {
        LOGGER.info("Received portEvent: {}", event);

        handlePortEvent(event);
    }

    /**
     * Handle the port event.
     * 
     * @param event
     *            the port event
     */
    protected void handlePortEvent(PortEvent event) {

    }

}
