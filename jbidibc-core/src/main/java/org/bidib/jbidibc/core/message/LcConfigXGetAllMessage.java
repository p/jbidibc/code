package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;

public class LcConfigXGetAllMessage extends BidibCommandMessage implements BidibBulkCommand {

    private int expectedCountResponses;

    protected LcConfigXGetAllMessage() {
        super(0, BidibLibrary.MSG_LC_CONFIGX_GET_ALL);
    }

    protected LcConfigXGetAllMessage(BidibPort rangeFrom, BidibPort rangeTo) {

        super(0, BidibLibrary.MSG_LC_CONFIGX_GET_ALL, new byte[] { rangeFrom.getValues()[0], rangeFrom.getValues()[1],
            rangeTo.getValues()[0], rangeTo.getValues()[1] });
    }

    public LcConfigXGetAllMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_LC_CONFIGX_GET_ALL";
    }

    public LcOutputType getPortTypeFrom(PortModelEnum portModel) {
        return PortModelEnum.getPortType(portModel, getData()[0]);
    }

    public LcOutputType getPortTypeTo(PortModelEnum portModel) {
        return PortModelEnum.getPortType(portModel, getData()[2]);
    }

    public int getPortRangeFrom(PortModelEnum portModel) {
        return PortModelEnum.getPortNumber(portModel, getData()[0], getData()[1]);
    }

    public int getPortRangeTo(PortModelEnum portModel) {
        return PortModelEnum.getPortNumber(portModel, getData()[2], getData()[3]);
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return null;
    }

    @Override
    public Integer[] getExpectedBulkResponseTypes() {
        return new Integer[] { LcConfigXResponse.TYPE, LcNotAvailableResponse.TYPE };
    }

    /**
     * @return the expectedCountResponses
     */
    public int getExpectedCountResponses() {
        return expectedCountResponses;
    }

    /**
     * @param expectedCountResponses
     *            the expectedCountResponses to set
     */
    public void setExpectedCountResponses(int expectedCountResponses) {
        this.expectedCountResponses = expectedCountResponses;
    }
}
