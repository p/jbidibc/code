package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum AccessoryStateErrorEnum implements BidibEnum {
    BIDIB_ACC_STATE_ERROR_NONE(BidibLibrary.BIDIB_ACC_STATE_ERROR_NONE), BIDIB_ACC_STATE_ERROR_SERVO(
        BidibLibrary.BIDIB_ACC_STATE_ERROR_SERVO);

    private final byte type;

    AccessoryStateErrorEnum(int type) {
        this.type = ByteUtils.getLowByte(type);
    }

    @Override
    public byte getType() {
        return type;
    }

    /**
     * Create an AccessoryStateErrorEnum from a byte value.
     * 
     * @param type
     *            numeric value of the sys error
     * 
     * @return AccessoryStateErrorEnum
     */
    public static AccessoryStateErrorEnum valueOf(byte type) {
        AccessoryStateErrorEnum result = null;

        for (AccessoryStateErrorEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("Cannot map " + type + " to an accessory state error.");
        }
        return result;
    }

}
