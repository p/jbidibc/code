package org.bidib.jbidibc.serial;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.OccupationState;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.BidibResponseFactory;
import org.bidib.jbidibc.core.message.LcConfigXResponse;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SerialMessageReceiverTest {

    private ByteArrayOutputStream pack(byte[] data) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        output.write(data);
        return output;
    }

    @Test
    public void receiveMessageTest() throws IOException {
        byte[] address = new byte[] { 0 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        // prepare the object to test
        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data = new byte[] { 0x05, 0x00, 0x01, (byte) 0x86, (byte) 0x02, (byte) 0x00, (byte) 0x46, (byte) 0xFE };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(1));

        receiver.receive(pack(data));

        // verify that error was called once
        Mockito.verify(messageListener, Mockito.times(1)).error(address, 2, address);
    }

    @Test
    public void receiveMessageLeadingMagicTest() throws IOException {
        byte[] address = new byte[] { 0 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        // prepare the object to test
        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data =
            new byte[] { (byte) 0xFE, 0x05, 0x00, 0x01, (byte) 0x86, (byte) 0x02, (byte) 0x00, (byte) 0x46,
                (byte) 0xFE };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(1));

        receiver.receive(pack(data));

        // verify that error was called once
        Mockito.verify(messageListener, Mockito.times(1)).error(address, 2, address);
    }

    @Test
    public void receiveMessageAddrStackTest() throws IOException {
        byte[] address = new byte[] { 1, 2, 3 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        // prepare the object to test
        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data =
            new byte[] { 0x0B, 0x01, 0x02, 0x03, 0x00, 0x01, (byte) 0x86, SysErrorEnum.BIDIB_ERR_ADDRSTACK.getType(),
                (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x56, (byte) 0xFE };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(1));

        receiver.receive(pack(data));

        // verify that error was called once
        Mockito
            .verify(messageListener, Mockito.times(1))
            .error(address, SysErrorEnum.BIDIB_ERR_ADDRSTACK.getType(), new byte[] { 1, 2, 3, 4 });
    }

    @Test
    public void receiveFeedbackMultipleResponseOccupiedTest() throws IOException {

        byte[] address = new byte[] { 2 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        // prepare the object to test
        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 30.03.2016 07:50:26.860: - << [17] - 0E 02 00 48 A2 00 40 FF FF FF FF FF FF FF FF 1B FE

        // ByteArrayInputStream is =
        // new ByteArrayInputStream(new byte[] { (byte) 0xfe, 0x16, (byte) 0x01, (byte) 0x00, (byte) 0x06,
        // (byte) 0xa2, (byte) 0x00, (byte) 0x80, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
        // (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
        // (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0x1B, (byte) 0xfe });
        byte[] data =
            new byte[] { (byte) 0x0E, 0x02, 0x00, 0x48, (byte) 0xA2, (byte) 0x00, (byte) 0x40, (byte) 0xff, (byte) 0xff,
                (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0x1B,
                (byte) 0xfe };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(72));

        receiver.receive(pack(data));

        // verify that only occupied was called once
        Mockito.verify(messageListener, Mockito.never()).occupation(address, 2, OccupationState.FREE, null);
        // feedback 2 is occupied
        Mockito.verify(messageListener, Mockito.times(1)).occupation(address, 2, OccupationState.OCCUPIED, null);
    }

    @Test
    public void receiveFeedbackMultipleResponseFreeTest() throws IOException {

        byte[] address = new byte[] { 2 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        // prepare the object to test
        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // ByteArrayInputStream is =
        // new ByteArrayInputStream(new byte[] { (byte) 0xfe, 0x16, (byte) 0x01, (byte) 0x00, (byte) 0x06,
        // (byte) 0xa2, (byte) 0x00, (byte) 0x80, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
        // (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
        // (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xfe });

        byte[] data =
            new byte[] { (byte) 0x0E, 0x02, 0x00, 0x48, (byte) 0xA2, (byte) 0x00, (byte) 0x80, (byte) 0x00, (byte) 0xff,
                (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xB1,
                (byte) 0xfe };

        Mockito.when(nodeFactory.findNode(address)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(72));

        receiver.receive(pack(data));

        // verify that only occupied was called once
        Mockito.verify(messageListener, Mockito.never()).occupation(address, 2, OccupationState.OCCUPIED, null);
        // feedback 0 is free
        Mockito.verify(messageListener, Mockito.times(1)).occupation(address, 0, OccupationState.FREE, null);
        // feedback 2 is free
        Mockito.verify(messageListener, Mockito.times(1)).occupation(address, 2, OccupationState.FREE, null);
        // feedback 7 is free
        Mockito.verify(messageListener, Mockito.times(1)).occupation(address, 7, OccupationState.FREE, null);
        // feedback 8 is occupied
        Mockito.verify(messageListener, Mockito.times(1)).occupation(address, 8, OccupationState.OCCUPIED, null);
    }

    @Test
    public void receive2MessagesTest() throws IOException, ProtocolException {

        BidibMessage featureResponse1 =
            new BidibResponseFactory().create(new byte[] { 0x06, 0x01, 0x00, 0x0e, (byte) 0x90, 0x00, 0x20 });

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        // BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data =
            new byte[] { 0x06, 0x01, 0x00, 0x0e, (byte) 0x90, 0x00, 0x20, /* CRC */
                (byte) 0xA7, (byte) 0xFE, /* start 2nd message */0x06, 0x01, 0x00, 0x0f, (byte) 0x90, 0x00, 0x20, /*
                                                                                                                   * CRC
                                                                                                                   */
                (byte) 0x28, (byte) 0xFE };

        final byte[] nodeAddress = new byte[] { 1 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(0x0f));
        Mockito.when(bidibNode.getNextReceiveMsgNum(featureResponse1)).thenReturn(Integer.valueOf(0x0e));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);

        receiver.receive(pack(data));

        // verify that no message listener methods were called.
        Mockito.verifyZeroInteractions(messageListener);
    }

    @Test
    public void receiveBoostStatResponseTest() throws IOException {
        byte[] address = new byte[] { 0 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] data = new byte[] { 0x04, 0x00, 0x3a, (byte) 0xb0, (byte) 0x06, (byte) 0x74, (byte) 0xFE };

        final byte[] nodeAddress = new byte[] { 0 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(58));

        receiver.receive(pack(data));

        // verify that booster state is on
        Mockito.verify(messageListener, Mockito.never()).boosterState(address, BoosterState.ON);
    }

    @Test
    public void receiveNodeTabResponseTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 11.08.2013 22:38:40.383: receive NodeTabResponse[[1, 0],num=2,type=137,data=[1, 0, 129, 0, 13, 114, 0, 31,
        // 0]] : 0d 01 00 02 89 01 00 81 00 0d 72 00 1f 00
        byte[] message =
            new byte[] { 0x0d, 0x01, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0x81, 0x00, (byte) 0x0d, (byte) 0x72,
                0x00, 0x1f, 0x00, (byte) 0xFE };

        final byte[] nodeAddress = new byte[] { 1 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(2));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);

        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(message));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response);
    }

    @Test
    public void receiveMagicResponseTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFD, (byte) 0xDE, (byte) 0xAF, (byte) 0x89, 0xFE
        byte[] rawmessage =
            new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFD, (byte) 0xDE, (byte) 0xAF, (byte) 0x89,
                (byte) 0xFE };

        byte[] message = new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF };

        final byte[] nodeAddress = new byte[] { 0 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(0));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response);
    }

    @Test
    public void receiveMagicResponses2PartsTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFD, (byte) 0xDE, (byte) 0xAF, (byte) 0x89, 0xFE
        byte[] rawmessage1 =
            new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFD, (byte) 0xDE, (byte) 0xAF, (byte) 0x89, (byte) 0xFE,
                0x05, 0x00, 0x01, (byte) 0x81 };
        byte[] rawmessage2 = new byte[] { (byte) 0xFD, (byte) 0xDE, (byte) 0xAF, (byte) 0x06, (byte) 0xFE };

        byte[] message = new byte[] { 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF };

        final byte[] nodeAddress = new byte[] { 0 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito
            .when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class)))
            .thenReturn(Integer.valueOf(0), Integer.valueOf(1));

        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage1));

        BidibMessage response1 = new BidibResponseFactory().create(message);

        receiver.receive(pack(rawmessage2));

        // verify that message is received
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response1);
    }

    @Test
    public void receiveMagicResponses2PartsWithEscapeTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] rawmessage1 = new byte[] { 0x07, 0x01, 0x00, (byte) 0xAF, (byte) 0xB9, (byte) 0x13, (byte) 0xFD };
        byte[] rawmessage2 = new byte[] { (byte) 0xDD, (byte) 0xFF, (byte) 0x38, (byte) 0xFE };

        byte[] message =
            new byte[] { 0x07, 0x01, 0x00, (byte) 0xAF, (byte) 0xB9, (byte) 0x13, (byte) 0xFD, (byte) 0xFF, (byte) 0x38,
                (byte) 0xFE };

        final byte[] nodeAddress = new byte[] { 1 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito
            .when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class)))
            .thenReturn(Integer.valueOf(175), Integer.valueOf(1));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        // create the expected message
        BidibMessage response1 = new BidibResponseFactory().create(message);

        receiver.receive(pack(rawmessage1));

        receiver.receive(pack(rawmessage2));

        // verify that message is received
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response1);
    }

    @Test
    public void receiveLcMacroParaResponseTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 0A 01 00 2E CA 02 03 3F BF 7F FF C3 FE
        byte[] rawmessage =
            new byte[] { 0x0A, 0x01, 0x00, 0x2E, (byte) 0xCA, 0x02, 0x03, 0x3F, (byte) 0xBF, 0x7F, (byte) 0xFF,
                (byte) 0xC3, (byte) 0xFE };

        byte[] message =
            new byte[] { 0x0A, 0x01, 0x00, 0x2E, (byte) 0xCA, 0x02, 0x03, 0x3F, (byte) 0xBF, 0x7F, (byte) 0xFF };

        final byte[] nodeAddress = new byte[] { 1 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(46));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response);

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_LC_MACRO_PARA));
    }

    @Test
    public void receiveLcMacroResponseTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 0A 01 00 2D C9 02 03 00 0F FF FF 00 FE
        byte[] rawmessage =
            new byte[] { 0x0A, 0x01, 0x00, 0x2D, (byte) 0xC9, 0x02, 0x03, 0x00, (byte) 0x0F, (byte) 0xFF, (byte) 0xFF,
                (byte) 0x00, (byte) 0xFE };

        byte[] message =
            new byte[] { 0x0A, 0x01, 0x00, 0x2D, (byte) 0xC9, 0x02, 0x03, 0x00, (byte) 0x0F, (byte) 0xFF, (byte) 0xFF };

        final byte[] nodeAddress = new byte[] { 1 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(45));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response);

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_LC_MACRO));
    }

    // the CONFIGX is signaled as bulk response
    @Test
    public void receiveLcConfigXResponseTest() throws IOException, ProtocolException {

        final byte[] nodeAddress = new byte[] { 1 };

        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 0E 01 00 15 C6 01 03 04 02 03 06 02 00 01 FD DE 49 FE
        byte[] rawmessage =
            new byte[] { 0x0E, 0x01, 0x00, 0x15, (byte) 0xC6, 0x01, 0x03, 0x04, 0x02, 0x03, 0x06, 0x02, 0x00, 0x01,
                (byte) 0xFD, (byte) 0xDE, (byte) 0x49, (byte) 0xFE };

        byte[] message =
            new byte[] { 0x0E, 0x01, 0x00, 0x15, (byte) 0xC6, 0x01, 0x03, 0x04, 0x02, 0x03, 0x06, 0x02, 0x00, 0x01,
                (byte) 0xFE };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(21));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);
        Assert.assertTrue(response instanceof LcConfigXResponse);

        LcConfigXResponse lcConfigXResponse = (LcConfigXResponse) response;

        Mockito.verify(messageListener, Mockito.times(1)).lcConfigX(nodeAddress, lcConfigXResponse.getLcConfigX());

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_LC_CONFIGX));
    }

    // 31.03.2016 09:16:31.457: [Thread-13] - << [13] - 09 01 00 71 B9 00 FD DD 00 01 FF 2E FE
    @Test
    public void receiveAccessoryParaResponseTest() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 09 01 00 71 B9 00 FD DD 00 01 FF 2E FE
        byte[] rawmessage =
            new byte[] { 0x09, 0x01, 0x00, 0x71, (byte) 0xB9, 0x00, (byte) 0xFD, (byte) 0xDD, 0x00, 0x01, (byte) 0xFF,
                (byte) 0x2E, (byte) 0xFE };

        // 09 01 00 71 B9 00 FD 00 01 FF
        byte[] message = new byte[] { 0x09, 0x01, 0x00, 0x71, (byte) 0xB9, 0x00, (byte) 0xFD, 0x00, 0x01, (byte) 0xFF };

        final byte[] nodeAddress = new byte[] { 1 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(113));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response);

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_ACCESSORY_PARA));
    }

    // RAW: 07 02 00 D1 B9 07 FC FD DE FD DE FE
    // RXTX: 07 02 00 D1 B9 07 FC FE
    @Test
    public void receiveAccessoryParaResponse2Test() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        // 07 02 00 D1 B9 07 FC FD DE FD DE FE
        byte[] rawmessage =
            new byte[] { 0x07, 0x02, 0x00, (byte) 0xD1, (byte) 0xB9, 0x07, (byte) 0xFC, (byte) 0xFD, (byte) 0xDE,
                (byte) 0xFD, (byte) 0xDE, (byte) 0xFE };

        // 07 02 00 D1 B9 07 FC FE
        byte[] message = new byte[] { 0x07, 0x02, 0x00, (byte) 0xD1, (byte) 0xB9, 0x07, (byte) 0xFC, (byte) 0xFE };

        final byte[] nodeAddress = new byte[] { 2 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito.when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(0xD1));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response);

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_ACCESSORY_PARA));
    }

    @Test
    public void receive3Test() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] rawmessage =
            { 0x08, 0x03, 0x00, 0x17, (byte) 0xC6, 0x00, 0x00, 0x00, 0x00, 0x08, 0x03, 0x00, 0x18, (byte) 0xC6, 0x00,
                0x01, 0x00, 0x00, 0x08, 0x03, 0x00, 0x19, (byte) 0xC6, 0x00, 0x02, 0x00, 0x00, 0x08, 0x03, 0x00, 0x1A,
                (byte) 0xC6, 0x00, 0x03, 0x00, 0x00, 0x08, 0x03, 0x00, 0x1B, (byte) 0xC6, 0x00, 0x04, 0x00, 0x00, 0x08,
                0x03, 0x00, 0x1C, (byte) 0xC6, 0x00, 0x05, 0x00, 0x00, 0x08, 0x03, 0x00, 0x1D, (byte) 0xC6, 0x00, 0x06,
                0x00, 0x00, (byte) 0xF1, (byte) 0xFE };

        byte[] message = new byte[] { 0x08, 0x03, 0x00, 0x17, (byte) 0xC6, 0x00, 0x00, 0x00, 0x00 };

        final byte[] nodeAddress = new byte[] { 3 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito
            .when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(0x17),
                Integer.valueOf(0x18), Integer.valueOf(0x19), Integer.valueOf(0x1A), Integer.valueOf(0x1B),
                Integer.valueOf(0x1C), Integer.valueOf(0x1D));
        // Mockito.when(bidibNode.getReceiveQueue()).thenReturn(receiveQueue);
        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(messageListener, Mockito.times(7)).lcConfigX(Mockito.eq(nodeAddress), Mockito.any());

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_LC_CONFIGX));
    }

    @Test
    public void receive4Test() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] rawmessage =
            { 0x08, 0x03, 0x00, 0x1E, (byte) 0xC6, 0x00, 0x07, 0x00, 0x00, 0x08, 0x03, 0x00, 0x1F, (byte) 0xC6, 0x00,
                0x08, 0x00, 0x00, 0x08, 0x03, 0x00, 0x20, (byte) 0xC6, 0x00, 0x09, 0x00, 0x00, 0x08, 0x03, 0x00, 0x21,
                (byte) 0xC6, 0x00, 0x0A, 0x00, 0x00, 0x08, 0x03, 0x00, 0x22, (byte) 0xC6, 0x00, 0x0B, 0x00, 0x00, 0x08,
                0x03, 0x00, 0x23, (byte) 0xC6, 0x00, 0x0C, 0x00, 0x00, 0x08, 0x03, 0x00, 0x24, (byte) 0xC6, 0x00, 0x0D,
                0x00, 0x00, (byte) 0xF5, (byte) 0xFE };

        byte[] message = new byte[] { 0x08, 0x03, 0x00, 0x17, (byte) 0xC6, 0x00, 0x00, 0x00, 0x00 };

        final byte[] nodeAddress = new byte[] { 3 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito
            .when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class))).thenReturn(Integer.valueOf(0x1E),
                Integer.valueOf(0x1F), Integer.valueOf(0x20), Integer.valueOf(0x21), Integer.valueOf(0x22),
                Integer.valueOf(0x23), Integer.valueOf(0x24));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(messageListener, Mockito.times(7)).lcConfigX(Mockito.eq(nodeAddress), Mockito.any());

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_LC_CONFIGX));
    }

    @Test
    public void receive5Test() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);
        // set the receive queue

        byte[] rawmessage =
            { 0x08, 0x03, 0x00, 0x25, (byte) 0xC6, 0x00, 0x0E, 0x00, 0x00, 0x08, 0x03, 0x00, 0x26, (byte) 0xC6, 0x00,
                0x0F, 0x00, 0x00, (byte) 0xDA, (byte) 0xFE };

        byte[] message = new byte[] { 0x08, 0x03, 0x00, 0x17, (byte) 0xC6, 0x00, 0x00, 0x00, 0x00 };

        final byte[] nodeAddress = new byte[] { 3 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito
            .when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class)))
            .thenReturn(Integer.valueOf(0x25), Integer.valueOf(0x26));

        receiver.receive(pack(rawmessage));

        BidibMessage response = new BidibResponseFactory().create(message);

        // verify that message was signaled
        Mockito.verify(messageListener, Mockito.times(2)).lcConfigX(Mockito.eq(nodeAddress), Mockito.any());

        Assert.assertEquals(response.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_LC_CONFIGX));
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(SerialMessageReceiverTest.class);

    @Test
    public void receive6Test() throws IOException, ProtocolException {
        NodeRegistry nodeFactory = Mockito.mock(NodeRegistry.class);
        BidibNode bidibNode = Mockito.mock(BidibNode.class);
        MessageListener messageListener = Mockito.mock(MessageListener.class);
        BlockingQueue<BidibMessage> receiveQueue = Mockito.mock(BlockingQueue.class);

        SerialMessageReceiver receiver = new SerialMessageReceiver(nodeFactory);
        receiver.addMessageListener(messageListener);

        // @formatter:off
        //    08.12.2017 19:30:27.339: [receiveQueueWorker] - <<<< len: 39, data: 07 02 01 00 02 90 FD DE 00 07 02 01 00 03 90 FC 18 41 FE 07 02 01 00 04 90 00 00 BB FE 07 02 01 00 05 90 01 01 AE FE
        //    08.12.2017 19:30:27.339: [receiveQueueWorker] - << [5] - 07 02 01 00 02 90 FE 00 07 02 01 00 03 90 FC 18 41
        //    08.12.2017 19:30:27.344: [receiveQueueWorker] - << [3] - 07 02 01 00 04 90 00 00 BB
        //    08.12.2017 19:30:27.344: [receiveQueueWorker] - << [3] - 07 02 01 00 05 90 01 01 AE
        //@formatter:on

        byte[] rawmessage =
            { 0x07, 0x02, 0x01, 0x00, 0x02, (byte) 0x90, (byte) 0xFD, (byte) 0xDE, 0x00, 0x07, 0x02, 0x01, 0x00, 0x03,
                (byte) 0x90, (byte) 0xFC, 0x18, 0x41, (byte) 0xFE, 0x07, 0x02, 0x01, 0x00, 0x04, (byte) 0x90, 0x00,
                0x00, (byte) 0xBB, (byte) 0xFE, 0x07, 0x02, 0x01, 0x00, 0x05, (byte) 0x90, 0x01, 0x01, (byte) 0xAE,
                (byte) 0xFE };

        byte[] message0 = new byte[] { 0x07, 0x02, 0x01, 0x00, 0x02, (byte) 0x90, (byte) 0xFE, 0x00 };
        byte[] message1 = new byte[] { 0x07, 0x02, 0x01, 0x00, 0x03, (byte) 0x90, (byte) 0xFC, 0x18 };
        byte[] message2 = new byte[] { 0x07, 0x02, 0x01, 0x00, 0x04, (byte) 0x90, (byte) 0x00, 0x00 };
        byte[] message3 = new byte[] { 0x07, 0x02, 0x01, 0x00, 0x05, (byte) 0x90, (byte) 0x01, 0x01 };

        final byte[] nodeAddress = new byte[] { 0x02, 0x01 };

        Mockito.when(nodeFactory.findNode(nodeAddress)).thenReturn(bidibNode);
        Mockito
            .when(bidibNode.getNextReceiveMsgNum(Mockito.any(BidibMessage.class)))
            .thenReturn(Integer.valueOf(0x02), Integer.valueOf(0x03), Integer.valueOf(0x04), Integer.valueOf(0x05));

        Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                LOGGER.info("addResponseToReceiveQueue is called with args: {}", args);
                receiveQueue.offer((BidibMessage) args[0]);
                return null;
            }
        }).when(bidibNode).addResponseToReceiveQueue(Mockito.any(BidibMessage.class));

        receiver.receive(pack(rawmessage));

        BidibMessage response0 = new BidibResponseFactory().create(message0);
        BidibMessage response1 = new BidibResponseFactory().create(message1);
        BidibMessage response2 = new BidibResponseFactory().create(message2);
        BidibMessage response3 = new BidibResponseFactory().create(message3);

        // verify that message was signaled
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response0);
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response1);
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response2);
        Mockito.verify(receiveQueue, Mockito.times(1)).offer(response3);

        Assert.assertEquals(response0.getType(), ByteUtils.getLowByte(BidibLibrary.MSG_FEATURE));
    }
}
