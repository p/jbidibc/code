package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.LcMacroParaValue;
import org.bidib.jbidibc.core.enumeration.BacklightPortEnum;
import org.bidib.jbidibc.core.enumeration.LcMacroState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.ServoPortEnum;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MacroUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LcMacroResponseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LcMacroResponseTest.class);

    // private MacroFunctionsNode macroFunctionsNode;

    // @BeforeClass
    // public void init() {
    // macroFunctionsNode = new MacroFunctionsNode() {
    //
    // @Override
    // public boolean isPortFlatModelAvailable() {
    // return false;
    // }
    //
    // @Override
    // public LcOutputType getPortType(BidibPort bidibPort) {
    // return LcOutputType.SWITCHPORT;
    // }
    // };
    // }

    @Test
    public void createLcMacroResponse1FromByteArray() throws ProtocolException {
        byte[] message = { 0x0a, 0x01, 0x00, (byte) 0xd5, (byte) 0xc9, 0x00, 0x00, 0x00, 0x06, 0x02, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        LOGGER.info("lcMacro: {}", lcMacro);

        Assert.assertEquals(lcMacro.getStepNumber(), 0);
        Assert.assertEquals(lcMacro.getBidibPort().getPortNumber(PortModelEnum.type), 2);
        Assert.assertEquals(lcMacro.getBidibPort().getPortType(PortModelEnum.type), LcOutputType.BACKLIGHTPORT);
        Assert.assertEquals(lcMacro.getStatus(LcOutputType.BACKLIGHTPORT), BacklightPortEnum.START);
        Assert.assertEquals(lcMacro.getPortValue(LcOutputType.BACKLIGHTPORT), 0);
    }

    @Test
    public void createLcMacroResponse2FromByteArray() throws ProtocolException {
        byte[] message = { 0x0a, 0x01, 0x00, (byte) 0xd6, (byte) 0xc9, 0x00, 0x01, (byte) 0xc8, 0x06, 0x02, 0x05 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        LOGGER.info("lcMacro: {}", lcMacro);

        Assert.assertEquals(lcMacro.getStepNumber(), 1);
        Assert.assertEquals(lcMacro.getBidibPort().getPortNumber(PortModelEnum.type), 2);
        Assert.assertEquals(lcMacro.getBidibPort().getPortType(PortModelEnum.type), LcOutputType.BACKLIGHTPORT);
        Assert.assertEquals(lcMacro.getStatus(LcOutputType.BACKLIGHTPORT), BacklightPortEnum.START);
        Assert.assertEquals(lcMacro.getPortValue(LcOutputType.BACKLIGHTPORT), 5);
    }

    @Test
    public void createLcMacroResponse3FromByteArray() throws ProtocolException {
        byte[] message = { 0x0a, 0x01, 0x00, (byte) 0xd7, (byte) 0xc9, 0x00, 0x02, (byte) 0xc8, 0x01, 0x01, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        LOGGER.info("lcMacro: {}", lcMacro);

        Assert.assertEquals(lcMacro.getStepNumber(), 2);
        Assert.assertEquals(lcMacro.getBidibPort().getPortNumber(PortModelEnum.type), 1);
        Assert.assertEquals(lcMacro.getBidibPort().getPortType(PortModelEnum.type), LcOutputType.LIGHTPORT);
        Assert.assertEquals(lcMacro.getStatus(LcOutputType.LIGHTPORT), LightPortEnum.OFF);
        Assert.assertEquals(lcMacro.getPortValue(LcOutputType.LIGHTPORT), 0); // value is not set for light port
    }

    @Test
    public void createLcMacroResponse4FromByteArray() throws ProtocolException {
        byte[] message =
            { 0x0a, 0x01, 0x00, (byte) 0xd8, (byte) 0xc9, 0x00, 0x03, (byte) 0xff, (byte) 0xff, 0x00, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        LOGGER.info("lcMacro: {}", lcMacro);

        Assert.assertEquals(lcMacro.getStepNumber(), 3);
        Assert.assertEquals(lcMacro.getBidibPort().getPortNumber(PortModelEnum.type), 0);
        Assert.assertEquals(lcMacro.getBidibPort().getPortType(PortModelEnum.type), LcOutputType.END_OF_MACRO);
    }

    @Test
    public void createLcMacroResponse5FromByteArray() throws ProtocolException {
        byte[] message = { 0x0a, 0x01, 0x00, (byte) 0xd6, (byte) 0xc9, 0x00, 0x01, (byte) 0xc8, 0x02, 0x02, 0x25 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        LOGGER.info("lcMacro: {}", lcMacro);

        Assert.assertEquals(lcMacro.getStepNumber(), 1);
        Assert.assertEquals(lcMacro.getBidibPort().getPortNumber(PortModelEnum.type), 2);
        Assert.assertEquals(lcMacro.getBidibPort().getPortType(PortModelEnum.type), LcOutputType.SERVOPORT);
        Assert.assertEquals(lcMacro.getStatus(LcOutputType.SERVOPORT), ServoPortEnum.START);
        Assert.assertEquals(lcMacro.getPortValue(LcOutputType.SERVOPORT), 0x25);
    }

    @Test
    public void createLcMacroResponse6FromByteArray() throws ProtocolException {
        // 16.12.2013 22:32:57.578: receive LcMacroResponse[[1],num=42,type=201,data=[40, 1, 255, 244, 250, 0]] : 0A 01
        // 00 2A C9 28 01 FF F4 FA 00
        byte[] message =
            { 0x0a, 0x01, 0x00, (byte) 0x2a, (byte) 0xc9, 0x28, 0x01, (byte) 0xFF, (byte) 0xF4, (byte) 0xFA, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        LOGGER.info("lcMacro: {}", lcMacro);

        Assert.assertEquals(lcMacro.getStepNumber(), 1);
        Assert.assertEquals(lcMacro.getSystemFunctionType(), LcOutputType.DELAY_FIXED);
        Assert.assertEquals(ByteUtils.getInt(lcMacro.getSystemFunctionValue()), 250);
        Assert.assertEquals(lcMacro.getStatus(LcOutputType.DELAY_FIXED), null);
        Assert.assertEquals(lcMacro.getPortValue(LcOutputType.DELAY_FIXED), 0);
    }

    // 09.10.2013 15:50:50.345: receive LcMacroStateResponse[[1],num=121,type=200,data=[0, 252]] : 06 01 00 79 c8 00 fc
    @Test
    public void createLcMacroStateResponseFromByteArray() throws ProtocolException {
        byte[] message = { 0x06, 0x01, 0x00, (byte) 0x79, (byte) 0xc8, 0x00, (byte) 0xfc };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO_STATE);

        LcMacroStateResponse lcMacroStateResponse = (LcMacroStateResponse) bidibMessage;
        LOGGER.info("lcMacroStateResponse: {}", lcMacroStateResponse);

        Assert.assertEquals(lcMacroStateResponse.getMacroNumber(), 0);
        Assert.assertEquals(lcMacroStateResponse.getMacroState(), LcMacroState.RESTORE);
    }

    // 29.06.2013 12:37:51.584: receive LcMacroResponse[[1],num=170,type=201,data=[0, 1, 255, 240, 1, 0]] : 0A 01
    // 00 AA C9 00 01 FF F0 01 00
    @Test
    public void createLcMacroResponse7FromByteArray() throws ProtocolException {
        byte[] message =
            { 0x0A, 0x01, 0x00, (byte) 0xAA, (byte) 0xc9, 0x00, 0x01, (byte) 0xFF, (byte) 0xF0, 0x01, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);

        Assert.assertEquals(lcMacro.getBidibPort().getPortNumber(PortModelEnum.type), 1);
        Assert.assertEquals(lcMacro.getBidibPort().getPortType(PortModelEnum.type), LcOutputType.SERVOMOVE_QUERY);
    }

    // 02.11.2015 18:58:17.602: receive MSG_LC_MACRO_PARA[[1],num=46,type=202,data=[2, 3, 63, 191, 127, 255]] :
    // 0A 01 00 2E CA 02 03 3F BF 7F FF
    @Test
    public void createLcMacroParaResponseFromByteArray() throws ProtocolException {
        byte[] message =
            { 0x0A, 0x01, 0x00, (byte) 0x2E, (byte) 0xCA, 0x02, 0x03, (byte) 0x3F, (byte) 0xBF, 0x7F, (byte) 0xFF };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO_PARA);

        LcMacroParaResponse lcMacroParaResponse = (LcMacroParaResponse) bidibMessage;
        LOGGER.info("lcMacroParaResponse: {}", lcMacroParaResponse);

        Assert.assertEquals(lcMacroParaResponse.getMacroNumber(), 2);
        Assert.assertEquals(lcMacroParaResponse.getParameterIndex(), 3);

        LcMacroParaValue lcMacroParaValue = lcMacroParaResponse.getLcMacroParaValue();
        Assert.assertNotNull(lcMacroParaValue);
    }

    // 02.11.2015 18:58:17.587: receive MSG_LC_MACRO[[1],num=45,type=201,data=[2, 3, 0, 15, 255, 255]] : 0A 01 00 2D C9
    // 02 03 00 0F FF FF
    @Test(expectedExceptions = { InvalidConfigurationException.class })
    public void createLcMacroResponse8FromByteArray() throws ProtocolException {
        byte[] message =
            { 0x0A, 0x01, 0x00, (byte) 0x2D, (byte) 0xC9, 0x02, 0x03, 0x00, (byte) 0x0F, (byte) 0xFF, (byte) 0xFF,
                (byte) 0xFF };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_LC_MACRO);

        LcMacroResponse lcMacroResponse = (LcMacroResponse) bidibMessage;
        LOGGER.info("lcMacroResponse: {}", lcMacroResponse);

        LcMacro lcMacro = MacroUtils.getMacro(lcMacroResponse.getData());
        Assert.assertNotNull(lcMacro);
        lcMacro.getPortValue(LcOutputType.INPUTPORT);
        lcMacro.getStatus(LcOutputType.INPUTPORT);
    }
}
