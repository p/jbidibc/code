package org.bidib.jbidibc.core.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The node signals the MSG_BM_POSITION <code>1,2:addr_l, addr_h, 3:type, 4,5:locid_l, locid_h</code>
 */
public class FeedbackPositionResponse extends BidibMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPositionResponse.class);

    public static final Integer TYPE = BidibLibrary.MSG_BM_POSITION;

    FeedbackPositionResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        // len == 1 if no sensor time is delivered
        if (data == null || (data.length < 5)) {
            throw new ProtocolException("No valid MSG_BM_POSITION received.");
        }
    }

    public FeedbackPositionResponse(byte[] addr, int num, int decoderAddress, int locationType, int locationAddress)
        throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_POSITION,
            new Builder(decoderAddress).addType(locationType).addAddress(locationAddress).build());
    }

    public String getName() {
        return "MSG_BM_POSITION";
    }

    public int getDecoderAddress() {
        return ByteUtils.getWORD(getData(), 0);
    }

    public int getLocationType() {
        return ByteUtils.getInt(getData()[2]);
    }

    public int getLocationAddress() {
        return ByteUtils.getWORD(getData(), 3);
    }

    private static class Builder {

        private ByteArrayOutputStream stream;

        public Builder(int decoderAddress) {
            stream = new ByteArrayOutputStream(10);
            try {
                stream.write(ByteUtils.toWORD(decoderAddress));
            }
            catch (IOException ex) {
                LOGGER.warn("Add decoder address to stream failed", ex);
            }
        }

        public Builder addType(int locationType) {
            stream.write(ByteUtils.getLowByte(locationType));
            return this;
        }

        public Builder addAddress(int address) {
            try {
                stream.write(ByteUtils.toWORD(address));
            }
            catch (IOException ex) {
                LOGGER.warn("Add address to stream failed", ex);
            }
            return this;
        }

        public byte[] build() {
            return stream.toByteArray();
        }
    }

}
