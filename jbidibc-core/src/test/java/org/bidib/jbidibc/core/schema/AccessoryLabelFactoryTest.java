package org.bidib.jbidibc.core.schema;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AccessoryLabelFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryLabelFactoryTest.class);

    private static final String INPUT_DIR = "/xml-test/accessories";

    private static final String OUTPUT_TARGET_DIR = "target/xml-test/accessories";

    private static final String OUTPUT_FILENAME = "accessories-test1.xml";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void saveAccessories() {

        List<Accessory> accessories = new ArrayList<>();
        Accessory accessory = new Accessory();
        accessory.setName("Test 1");
        accessory.setNumber(0);
        accessories.add(accessory);

        accessory = new Accessory();
        accessory.setName("Test 2");
        accessory.setNumber(1);
        accessories.add(accessory);

        AccessoryLabelFactory accessoryLabelFactory = new AccessoryLabelFactory();
        String fileName = OUTPUT_TARGET_DIR + "/" + OUTPUT_FILENAME;
        accessoryLabelFactory.saveAccessoryAspectsLabels(accessories, fileName, false);
    }

    @Test
    public void loadAccessories() throws FileNotFoundException {

        AccessoryLabelFactory accessoryLabelFactory = new AccessoryLabelFactory();
        String fileName = INPUT_DIR + "/" + OUTPUT_FILENAME;
        List<Accessory> accessories = accessoryLabelFactory.getAccessories(fileName);

        Assert.assertNotNull(accessories);
        LOGGER.info("Loaded accessories: {}", accessories);

        Assert.assertEquals(accessories.size(), 2);
        Assert.assertEquals(accessories.get(0).getName(), "Test 0");
        Assert.assertEquals(accessories.get(1).getName(), "Test 1");
    }
}
