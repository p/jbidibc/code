package org.bidib.jbidibc.core.schema.exception;

import java.util.List;

public class InvalidContentException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final List<String> errors;

    public InvalidContentException(String message, final List<String> errors) {
        super(message);

        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
