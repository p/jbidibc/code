package org.bidib.jbidibc.debug.rxtx;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.TooManyListenersException;
import java.util.concurrent.Semaphore;

import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.debug.DebugInterface;
import org.bidib.jbidibc.debug.DebugMessageProcessor;
import org.bidib.jbidibc.debug.LineEndingEnum;
import org.bidib.jbidibc.debug.exception.InvalidLibraryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

public class DebugReader implements DebugInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(DebugReader.class);

    private static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("DEBUG_RAW");

    static final int DEFAULT_TIMEOUT = /* 1500 */300;

    private SerialPort port;

    private DebugMessageProcessor messageReceiver;

    private String requestedPortName;

    private ConnectionListener connectionListener;

    private Semaphore portSemaphore = new Semaphore(1);

    private Semaphore sendSemaphore = new Semaphore(1);

    public DebugReader(DebugMessageProcessor messageReceiver) {
        this.messageReceiver = messageReceiver;
    }

    @Override
    public void initialize() {

    }

    @Override
    public List<String> getPortIdentifiers() {
        List<String> portIdentifiers = new ArrayList<String>();

        try {
            // get the comm port identifiers
            Enumeration<?> e = CommPortIdentifier.getPortIdentifiers();
            while (e.hasMoreElements()) {
                CommPortIdentifier id = (CommPortIdentifier) e.nextElement();
                LOGGER
                    .debug("Process current CommPortIdentifier, name: {}, portType: {}", id.getName(),
                        id.getPortType());

                if (id.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                    portIdentifiers.add(id.getName());
                }
                else {
                    LOGGER
                        .debug("Skip port because no serial port, name: {}, portType: {}", id.getName(),
                            id.getPortType());
                }
            }
        }
        catch (UnsatisfiedLinkError ule) {
            LOGGER.warn("Get comm port identifiers failed.", ule);
            throw new InvalidLibraryException(ule.getMessage(), ule.getCause());
        }
        catch (Error error) {
            LOGGER.warn("Get comm port identifiers failed.", error);
            throw new RuntimeException(error.getMessage(), error.getCause());
        }
        return portIdentifiers;
    }

    @Override
    public DebugMessageProcessor getMessageReceiver() {
        return messageReceiver;
    }

    /**
     * @return the connectionListener
     */
    public ConnectionListener getConnectionListener() {
        return connectionListener;
    }

    /**
     * @param connectionListener
     *            the connectionListener to set
     */
    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    private SerialPort internalOpen(CommPortIdentifier commPort, int baudRate, Context context)
        throws PortInUseException, UnsupportedCommOperationException, TooManyListenersException {

        // open the port
        SerialPort serialPort = (SerialPort) commPort.open(DebugReader.class.getName(), 2000);

        getConnectionListener().opened(commPort.getName());

        LOGGER.info("Set flow control mode to SerialPort.FLOWCONTROL_NONE!");
        serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);

        // serialPort.enableReceiveTimeout(DEFAULT_TIMEOUT);

        serialPort.setSerialPortParams(baudRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

        clearInputStream(serialPort);

        // // react on port removed ...
        // serialPort.notifyOnCTS(true);

        // enable the message receiver before the event listener is added
        getMessageReceiver().enable();
        serialPort.addEventListener(new SerialPortEventListener() {

            @Override
            public void serialEvent(SerialPortEvent event) {
                // this callback is called every time data is available
                LOGGER.trace("serialEvent received: {}", event);
                switch (event.getEventType()) {
                    case SerialPortEvent.DATA_AVAILABLE:
                        try {
                            getMessageReceiver().receive(port);
                        }
                        catch (Exception ex) {
                            LOGGER.error("Message receiver has terminated with an exception!", ex);
                        }
                        break;
                    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                        LOGGER.info("The output buffer is empty.");

                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
                            MSG_RAW_LOGGER.info(">> Notify output empty");
                        }

                        break;
                    case SerialPortEvent.CD:
                        LOGGER.warn("CD is signalled.");
                        break;
                    case SerialPortEvent.CTS:
                        LOGGER
                            .warn("The CTS value has changed, old value: {}, new value: {}",
                                new Object[] { event.getOldValue(), event.getNewValue() });

                        if (event.getNewValue() == false) {
                            LOGGER.warn("Close the port.");
                            Thread worker = new Thread(new Runnable() {
                                public void run() {
                                    LOGGER.info("Start close port because error was detected.");
                                    try {
                                        close();

                                        // TODO notify the listeners ...

                                    }
                                    catch (Exception ex) {
                                        LOGGER.warn("Close after error failed.", ex);
                                    }
                                    LOGGER.warn("The port was closed.");

                                    try {
                                        if (connectionListener != null) {
                                            connectionListener.closed(requestedPortName);
                                        }
                                    }
                                    catch (Exception ex) {
                                        LOGGER.warn("Notify connection listener failed.", ex);
                                    }
                                }
                            });
                            worker.start();
                        }
                        break;
                    default:
                        LOGGER
                            .warn("SerialPortEvent was triggered, type: {}, old value: {}, new value: {}",
                                new Object[] { event.getEventType(), event.getOldValue(), event.getNewValue() });
                        break;
                }
            }
        });
        serialPort.notifyOnDataAvailable(true);
        // react on port removed ...
        serialPort.notifyOnCTS(true);
        serialPort.notifyOnCarrierDetect(true);
        serialPort.notifyOnBreakInterrupt(true);
        serialPort.notifyOnDSR(true);
        serialPort.notifyOnOverrunError(true);

        // serialPort.notifyOnOutputEmpty(true);

        // Activate DTR
        try {
            LOGGER.info("Activate DTR.");

            serialPort.setDTR(true); // pin 1 in DIN8; on main connector, this is DTR
        }
        catch (Exception e) {
            LOGGER.warn("Set DTR true failed.", e);
        }

        // Activate RTS
        try {
            LOGGER.info("Activate RTS.");
            serialPort.setRTS(true);
        }
        catch (Exception e) {
            LOGGER.warn("Set RTS true failed.", e);
        }

        return serialPort;
    }

    private void clearInputStream(SerialPort serialPort) {

        // get and clear stream

        try {
            InputStream serialStream = serialPort.getInputStream();
            // purge contents, if any
            int count = serialStream.available();
            LOGGER.debug("input stream shows {} bytes available", count);
            while (count > 0) {
                serialStream.skip(count);
                count = serialStream.available();
            }
            LOGGER.debug("input stream shows {} bytes available after purge.", count);
        }
        catch (Exception e) {
            LOGGER.warn("Clear input stream failed.", e);
        }

    }

    @Override
    public void close() {
        if (port != null) {
            LOGGER.debug("Close the port.");
            long start = System.currentTimeMillis();

            // this makes the close operation faster ...
            try {
                port.removeEventListener();
                // port.enableReceiveTimeout(200);
            }
            catch (Exception e) {
                LOGGER.warn("Remove event listener and set receive timeout failed.", e);
            }
            try {
                port.close();
            }
            catch (Exception e) {
                LOGGER.warn("Close port failed.", e);
            }

            long end = System.currentTimeMillis();
            LOGGER.debug("Closed the port. duration: {}", end - start);

            port = null;

            // no longer process received messages
            getMessageReceiver().disable();

            requestedPortName = null;
        }
    }

    @Override
    public boolean isOpened() {
        boolean isOpened = false;
        try {
            portSemaphore.acquire();

            LOGGER.debug("Check if port is opened: {}", port);
            isOpened = (port != null && port.getOutputStream() != null);
        }
        catch (InterruptedException ex) {
            LOGGER.warn("Wait for portSemaphore was interrupted.", ex);
        }
        catch (IOException ex) {
            LOGGER.warn("OutputStream is not available.", ex);
        }
        finally {
            portSemaphore.release();
        }
        return isOpened;
    }

    @Override
    public void open(String portName, int baudRate, ConnectionListener connectionListener, Context context)
        throws PortNotFoundException, PortNotOpenedException {

        setConnectionListener(connectionListener);

        if (port == null) {
            if (portName == null || portName.trim().isEmpty()) {
                throw new PortNotFoundException("");
            }

            LOGGER.info("Open port with name: {}", portName);

            CommPortIdentifier commPort = null;
            try {
                commPort = CommPortIdentifier.getPortIdentifier(portName);
            }
            catch (NoSuchPortException ex) {
                LOGGER.warn("Requested port is not available: {}", portName, ex);
                throw new PortNotFoundException(portName);
            }

            LOGGER.info("Set the requestedPortName: {}, baudRate: {}", portName, baudRate);
            requestedPortName = portName;

            try {
                portSemaphore.acquire();

                try {
                    // try to open the port
                    close();
                    port = internalOpen(commPort, baudRate, context);
                    LOGGER.info("The port was opened internally.");
                }
                catch (PortInUseException ex) {
                    LOGGER.warn("Open communication failed because port is in use.", ex);
                    try {
                        close();
                    }
                    catch (Exception e4) {
                        // ignore
                    }
                    throw new PortNotOpenedException(portName, PortNotOpenedException.PORT_IN_USE);
                }
                catch (TooManyListenersException | UnsupportedCommOperationException ex) {
                    LOGGER.warn("Open communication failed because port has thrown an exception.", ex);
                    try {
                        close();
                    }
                    catch (Exception e4) {
                        // ignore
                    }
                    throw new PortNotOpenedException(portName, PortNotOpenedException.UNKNOWN);
                }
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Wait for portSemaphore was interrupted.", ex);
                throw new PortNotOpenedException(portName, PortNotOpenedException.UNKNOWN);
            }
            finally {
                portSemaphore.release();
            }
        }
        else {
            LOGGER.warn("Port is already opened.");
        }
    }

    /**
     * Send the bytes of the message to the outputstream and add &lt;CR>+&lt;LF>.
     * 
     * @param bytes
     *            the bytes to send
     */
    @Override
    public void send(final String message, LineEndingEnum lineEnding) {
        if (port != null) {
            try {
                sendSemaphore.acquire();

                if (MSG_RAW_LOGGER.isInfoEnabled()) {
                    MSG_RAW_LOGGER.info(">> '{}'", message);
                }

                OutputStream output = port.getOutputStream();

                output.write(message.getBytes());
                output.write(lineEnding.getValues());

                // output.flush();
            }
            catch (Exception e) {
                throw new RuntimeException("Send message to output stream failed.", e);
            }
            finally {
                sendSemaphore.release();
            }
        }
    }

    @Override
    public void send(byte[] content) {
        if (port != null) {
            try {
                sendSemaphore.acquire();

                if (MSG_RAW_LOGGER.isInfoEnabled()) {
                    MSG_RAW_LOGGER.info(">> [{}] - {}", content.length, ByteUtils.bytesToHex(content));
                }

                OutputStream output = port.getOutputStream();

                output.write(content);
                // output.flush();
            }
            catch (Exception e) {
                throw new RuntimeException("Send message to output stream failed.", e);
            }
            finally {
                sendSemaphore.release();
            }
        }
    }
}
