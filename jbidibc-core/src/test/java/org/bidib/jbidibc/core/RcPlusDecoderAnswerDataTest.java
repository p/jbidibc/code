package org.bidib.jbidibc.core;

import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RcPlusDecoderAnswerDataTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RcPlusDecoderAnswerDataTest.class);

    @Test
    public void fromByteArrayReceivedDecoderAckn() {

        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message = { 0x0A, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_RES, 0x05, 0x0A, 0x0B, 0x0C, 0x0D, 13 };

        RcPlusDecoderAnswerData answerData = RcPlusDecoderAnswerData.fromByteArray(message, 5);
        Assert.assertNotNull(answerData);

        LOGGER.info("Current answerData: {}", answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.RECEIVED_DECODER_ACKN);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));
    }

    @Test
    public void fromByteArraySentButNoResponse() {

        // IN --- : MSG_CS_RCPLUS_ACK
        byte[] message = { 0x0A, 0x00, 0x32, (byte) 0xE8, BidibLibrary.RC_BIND_RES, 0x04, 0x0A, 0x0B, 0x0C, 0x0D, 13 };

        RcPlusDecoderAnswerData answerData = RcPlusDecoderAnswerData.fromByteArray(message, 5);
        Assert.assertNotNull(answerData);

        LOGGER.info("Current answerData: {}", answerData);

        Assert.assertEquals(answerData.getAcknState(), RcPlusAcknowledge.SENT_BUT_NO_RESPONSE);
        Assert.assertEquals(answerData.getUniqueId(), new DecoderUniqueIdData(0x0D0C0B0A, 13));
    }
}
