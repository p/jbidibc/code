package org.bidib.jbidibc.core.port;

import java.util.HashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PortConfigUtilsTest {

    @Test
    public void isSupportsSoundPort() {

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(0x03, 0x0008));
        boolean isSupportsSoundPort = PortConfigUtils.isSupportsSoundPort(portConfig);
        Assert.assertTrue(isSupportsSoundPort);
    }

    @Test
    public void isSupportsServoPort() {

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(0x02, 0x0004));
        boolean isSupportsServoPort = PortConfigUtils.isSupportsServoPort(portConfig);
        Assert.assertTrue(isSupportsServoPort);
    }

    @Test
    public void isSupportsSwitchPort() {

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(0x00, 0x0001));
        boolean isSupportsSwitchPort = PortConfigUtils.isSupportsSwitchPort(portConfig);
        Assert.assertTrue(isSupportsSwitchPort);
    }

    @Test
    public void isSupportsSwitchPairPort() {

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(0x07, 0x0080));
        boolean isSupportsSwitchPairPort = PortConfigUtils.isSupportsSwitchPairPort(portConfig);
        Assert.assertTrue(isSupportsSwitchPairPort);
    }

    @Test
    public void isSupportsSwitchPairPortNot() {

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(0x00, 0x0001));
        boolean isSupportsSwitchPairPort = PortConfigUtils.isSupportsSwitchPairPort(portConfig);
        Assert.assertFalse(isSupportsSwitchPairPort);
    }
}
