package org.bidib.jbidibc.core.schema;

import java.util.List;

import org.bidib.jbidibc.core.schema.bidib.products.ProductType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BidibProductsFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibProductsFactoryTest.class);

    @Test
    public void getProducts() {
        int vendorId = 13;
        List<ProductType> products =
            BidibProductsFactory.getProducts(vendorId, "classpath:/xml/BiDiBProducts", "D:/temp");

        Assert.assertNotNull(products);
        LOGGER.info("Loaded products: {}", products);
    }

    @Test
    public void getProductsNotFound() {
        int vendorId = -13;
        List<ProductType> products =
            BidibProductsFactory.getProducts(vendorId, "classpath:/xml/BiDiBProducts", "D:/temp");

        Assert.assertNull(products);
    }

    @Test
    public void getProduct() {
        int vendorId = 13;
        int productId = 112;
        ProductType product = BidibProductsFactory.getProduct(vendorId, productId, "classpath:/xml/BiDiBProducts");

        Assert.assertNotNull(product);
        LOGGER.info("Loaded product: {}", product);

        Assert.assertEquals(product.getName(), "BiDiBOne Bootloader");
    }
}
