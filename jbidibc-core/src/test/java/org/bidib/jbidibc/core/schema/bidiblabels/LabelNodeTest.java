package org.bidib.jbidibc.core.schema.bidiblabels;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.transform.Source;

import org.apache.commons.io.FileUtils;
import org.bidib.jbidibc.core.schema.bidibbase.BaseLabel;
import org.bidib.jbidibc.core.schema.bidibbase.DefaultLabelsActionType;
import org.bidib.jbidibc.core.schema.bidibbase.PortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import org.xmlunit.assertj.XmlAssert;
import org.xmlunit.builder.Input;

public class LabelNodeTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LabelNodeTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/xml-test/bidiblabels";

    private static final String OUTPUT_FILENAME = "bidiblabels-test1.xml";

    @BeforeTest
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void prepareNodeFilenameTest() {
        long uniqueId = 0x40000D7C008384L;
        String filename = LabelFactory.prepareNodeFilename(uniqueId);
        LOGGER.info("Prepared filename: {}", filename);

        Assert.assertNotNull(filename);
        Assert.assertEquals(filename, "Node_40000D7C008384.xml");
    }

    @Test
    public void setPortLabels() throws IOException, SAXException {

        LabelFactory factory = new LabelFactory();

        // prepare the root node
        NodeLabels nodeLabels = new NodeLabels();

        NodeLabel nodeLabel = new NodeLabel();
        nodeLabel.withUniqueId(0x90000D84002400L).withUserName("LabelDemoNode").withProductName("LabelDemoProductNode");

        Assert.assertNotNull(nodeLabel);
        LOGGER.info("Current labelNode: {}", nodeLabel);

        nodeLabels.withNodeLabel(nodeLabel);

        PortLabels portLabels = new PortLabels();
        nodeLabels.withPortLabels(portLabels);

        // add some switch ports
        for (int portNumber = 0; portNumber < 8; portNumber++) {
            addPort(portLabels, portNumber, PortType.SWITCH);
        }
        // addPort(portLabels, 2, PortType.SWITCH);

        // add some servo ports
        for (int portNumber = 0; portNumber < 8; portNumber++) {
            addPort(portLabels, portNumber, PortType.SERVO);
        }

        // add some light ports
        for (int portNumber = 0; portNumber < 32; portNumber++) {
            addPort(portLabels, portNumber, PortType.LIGHT);
        }

        FeedbackPortLabels feedbackPortLabels = new FeedbackPortLabels();
        nodeLabels.withFeedbackPortLabels(feedbackPortLabels);

        // add some feedback ports
        for (int portNumber = 0; portNumber < 48; portNumber++) {
            addFeedbackPort(feedbackPortLabels, portNumber);
        }
        // addFeedbackPort(feedbackPortLabels, 3);

        MacroLabels macroLabels = new MacroLabels();
        nodeLabels.withMacroLabels(macroLabels);

        // add some macros
        for (int macroNumber = 0; macroNumber < 20; macroNumber++) {
            addMacro(macroLabels, macroNumber);
        }

        AccessoryLabels accessoryLabels = new AccessoryLabels();
        nodeLabels.withAccessoryLabels(accessoryLabels);

        // add some accessories
        for (int accessoryNumber = 0; accessoryNumber < 20; accessoryNumber++) {
            addAccessory(accessoryLabels, accessoryNumber);
        }

        LOGGER.info("Current nodeLabels: {}", nodeLabels);

        String fileName = OUTPUT_TARGET_DIR + "/" + OUTPUT_FILENAME;
        factory.saveNodeLabel(nodeLabels, new File(fileName), false);

        // load the saved file to compare the content
        File file = new File(fileName);
        LOGGER.info("Current file: {}", file);
        InputStream isTest = new FileInputStream(file);

        Source testDoc = Input.fromStream(isTest).build();

        InputStream isResult = LabelNodeTest.class.getResourceAsStream("/xml-test/result/bidiblabels-test1.xml");
        Source controlDoc = Input.fromStream(isResult).build();

        XmlAssert.assertThat(controlDoc).and(testDoc).areIdentical();
    }

    @Test
    public void loadPortLabels() {
        LabelFactory factory = new LabelFactory();
        InputStream is = LabelNodeTest.class.getResourceAsStream("/xml-test/bidiblabels/Node_00000000000001.xml");
        NodeLabels nodeLabels = factory.loadLabels(is);

        Assert.assertNotNull(nodeLabels);

        LOGGER.info("Current nodeLabels: {}", nodeLabels);

        DefaultLabelsActionType defaultLabelsActionType = nodeLabels.getDefaultLabelsApplied();
        Assert.assertNotNull(defaultLabelsActionType);
        Assert.assertEquals(defaultLabelsActionType, DefaultLabelsActionType.UNKNOWN);

        Assert.assertNotNull(nodeLabels.getNodeLabel());
        Assert.assertEquals(nodeLabels.getNodeLabel().getUniqueId(), 1L);

        Assert.assertNotNull(nodeLabels.getAccessoryLabels());
        List<AccessoryLabel> accessoryLabels = nodeLabels.getAccessoryLabels().getAccessoryLabel();
        Assert.assertEquals(accessoryLabels.size(), 2);

        AccessoryLabel accessoryLabel = accessoryLabels.get(0);
        Assert.assertNotNull(accessoryLabel);
        Assert.assertEquals(accessoryLabel.getIndex(), 0);
        Assert.assertEquals(accessoryLabel.getLabel(), "Test 1");

        // next accessory
        accessoryLabel = accessoryLabels.get(1);
        Assert.assertNotNull(accessoryLabel);
        Assert.assertEquals(accessoryLabel.getIndex(), 1);
        Assert.assertEquals(accessoryLabel.getLabel(), "Signal 1");

        List<BaseLabel> aspectLabels = accessoryLabel.getAspectLabel();
        Assert.assertEquals(aspectLabels.size(), 2);

        BaseLabel aspectLabel = aspectLabels.get(0);
        Assert.assertNotNull(aspectLabel);
        Assert.assertEquals(aspectLabel.getIndex(), 0);
        Assert.assertEquals(aspectLabel.getLabel(), "Hp0");

        aspectLabel = aspectLabels.get(1);
        Assert.assertNotNull(aspectLabel);
        Assert.assertEquals(aspectLabel.getIndex(), 1);
        Assert.assertEquals(aspectLabel.getLabel(), "Hp1");
    }

    private void addPort(final PortLabels portLabels, int portNumber, final PortType portType) {

        PortLabel portLabel = new PortLabel();
        portLabel.withIndex(portNumber).withType(portType).withLabel(portType.name() + "_" + portNumber);
        portLabels.withPortLabel(portLabel);
    }

    private void addFeedbackPort(final FeedbackPortLabels portLabels, int portNumber) {

        BaseLabel portLabel = new BaseLabel();
        portLabel.withIndex(portNumber).withLabel("Feedback_" + portNumber);
        portLabels.withPortLabel(portLabel);
    }

    private void addMacro(final MacroLabels macroLabels, int macroNumber) {

        BaseLabel portLabel = new BaseLabel();
        portLabel.withIndex(macroNumber).withLabel("Macro_" + macroNumber);
        macroLabels.withMacroLabel(portLabel);
    }

    private void addAccessory(final AccessoryLabels accessoryLabels, int accessoryNumber) {

        AccessoryLabel accessoryLabel = new AccessoryLabel();
        accessoryLabel.withIndex(accessoryNumber).withLabel("Accessory_" + accessoryNumber);

        // add some aspects
        for (int aspectNumber = 0; aspectNumber < 4; aspectNumber++) {
            addAspect(accessoryLabel, aspectNumber);
        }

        accessoryLabels.withAccessoryLabel(accessoryLabel);
    }

    private void addAspect(final AccessoryLabel accessoryLabel, int aspectNumber) {

        BaseLabel portLabel = new BaseLabel();
        portLabel.withIndex(aspectNumber).withLabel("Aspect_" + aspectNumber);
        accessoryLabel.withAspectLabel(portLabel);
    }
}
