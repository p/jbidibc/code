package org.bidib.jbidibc.core.node;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.FirmwareUpdateStat;
import org.bidib.jbidibc.core.LcConfig;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.MessageReceiver;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.VendorData;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateOperation;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.exception.ProtocolNoAnswerException;
import org.bidib.jbidibc.core.message.BidibBulkCommand;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.BoostOffMessage;
import org.bidib.jbidibc.core.message.BoostOnMessage;
import org.bidib.jbidibc.core.message.FeatureCountResponse;
import org.bidib.jbidibc.core.message.FeatureNotAvailableResponse;
import org.bidib.jbidibc.core.message.FeatureResponse;
import org.bidib.jbidibc.core.message.FeedbackGetAddressRangeMessage;
import org.bidib.jbidibc.core.message.FeedbackGetConfidenceMessage;
import org.bidib.jbidibc.core.message.FeedbackGetRangeMessage;
import org.bidib.jbidibc.core.message.FeedbackMirrorFreeMessage;
import org.bidib.jbidibc.core.message.FeedbackMirrorMultipleMessage;
import org.bidib.jbidibc.core.message.FeedbackMirrorOccupiedMessage;
import org.bidib.jbidibc.core.message.FeedbackMirrorPositionMessage;
import org.bidib.jbidibc.core.message.FwUpdateOpMessage;
import org.bidib.jbidibc.core.message.FwUpdateStatResponse;
import org.bidib.jbidibc.core.message.LcConfigResponse;
import org.bidib.jbidibc.core.message.LcConfigSetMessage;
import org.bidib.jbidibc.core.message.LcConfigXGetAllMessage;
import org.bidib.jbidibc.core.message.LcConfigXResponse;
import org.bidib.jbidibc.core.message.LcConfigXSetMessage;
import org.bidib.jbidibc.core.message.LcNotAvailableResponse;
import org.bidib.jbidibc.core.message.NodeChangedAckMessage;
import org.bidib.jbidibc.core.message.NodeTabCountResponse;
import org.bidib.jbidibc.core.message.NodeTabResponse;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.message.StringGetMessage;
import org.bidib.jbidibc.core.message.StringResponse;
import org.bidib.jbidibc.core.message.StringSetMessage;
import org.bidib.jbidibc.core.message.SysErrorResponse;
import org.bidib.jbidibc.core.message.SysGetPVersionMessage;
import org.bidib.jbidibc.core.message.SysGetSwVersionMessage;
import org.bidib.jbidibc.core.message.SysGetUniqueIdMessage;
import org.bidib.jbidibc.core.message.SysIdentifyMessage;
import org.bidib.jbidibc.core.message.SysMagicResponse;
import org.bidib.jbidibc.core.message.SysPVersionResponse;
import org.bidib.jbidibc.core.message.SysPingMessage;
import org.bidib.jbidibc.core.message.SysResetMessage;
import org.bidib.jbidibc.core.message.SysSwVersionResponse;
import org.bidib.jbidibc.core.message.SysUniqueIdResponse;
import org.bidib.jbidibc.core.message.VendorAckResponse;
import org.bidib.jbidibc.core.message.VendorDisableMessage;
import org.bidib.jbidibc.core.message.VendorEnableMessage;
import org.bidib.jbidibc.core.message.VendorResponse;
import org.bidib.jbidibc.core.message.VendorSetMessage;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.CollectionUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@code BidibNode} is the communication wrapper for the node instance.
 * <p>
 * The {@code BidibNode} instances are stored in the nodes list of the NodeFactory.
 * </p>
 *
 */
public class BidibNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibNode.class);

    public static final int BIDIB_MAGIC_UNKNOWN = -1;

    protected static final int BULK_WINDOW_SIZE = 4;

    private static final Logger MSG_TX_LOGGER = LoggerFactory.getLogger("TX");

    private Object outputStreamLock = new Object();

    private final byte[] addr;

    private int nextReceiveMsgNum = 0;

    private Object nextReceiveMsgNumLock = new Object();

    private int nextSendMsgNum = -1;

    private Object nextSendMsgNumLock = new Object();

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    private MessageReceiver messageReceiver;

    private BidibInterface bidib;

    private Integer nodeMagic;

    private Long uniqueId;

    private ProtocolVersion protocolVersion;

    private BlockingQueue<BidibMessage> receiveQueue = new LinkedBlockingQueue<BidibMessage>();

    protected boolean ignoreWaitTimeout;

    private boolean secureAckEnabled;

    private int responseTimeout = BidibInterface.DEFAULT_TIMEOUT;

    private int firmwarePacketTimeout = BidibInterface.DEFAULT_TIMEOUT;

    private RequestFactory requestFactory;

    // the sendLock is used to synchronize the access to the send methods
    private Object sendLock = new Object();

    private BlockingQueue<BidibCommand> sendQueue = new LinkedBlockingQueue<BidibCommand>();

    /**
     * Processing the sendQueue during bulk sending must be prohibited if the sendQueue worker is currently processing
     * the sendQueues of the bidibNode because otherwise the order of the messages is not correct.
     */
    protected enum ProcessSendQueue {
        enabled, disabled
    }

    /**
     * Create a new BidibNode that represents a connected node (slave) on the BiDiB bus.
     * 
     * @param addr
     *            the address
     * @param messageReceiver
     *            the message receiver
     * @param ignoreWaitTimeout
     *            ignore the wait timeout
     */
    protected BidibNode(byte[] addr, MessageReceiver messageReceiver, boolean ignoreWaitTimeout) {
        if (addr == null) {
            throw new IllegalArgumentException("The address must not be null!");
        }
        this.addr = addr.clone();
        this.messageReceiver = messageReceiver;
        this.ignoreWaitTimeout = ignoreWaitTimeout;
        LOGGER.debug("Create new BidibNode with address: {}, ignoreWaitTimeout: {}", addr, ignoreWaitTimeout);
    }

    /**
     * @param bidib
     *            the bidib to set
     */
    public void setBidib(BidibInterface bidib) {
        this.bidib = bidib;
    }

    /**
     * @param requestFactory
     *            the requestFactory to set
     */
    public void setRequestFactory(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    protected RequestFactory getRequestFactory() {
        return requestFactory;
    }

    /**
     * @return the responseTimeout
     */
    public int getResponseTimeout() {
        return responseTimeout;
    }

    /**
     * @param responseTimeout
     *            the responseTimeout to set
     */
    public void setResponseTimeout(int responseTimeout) {
        LOGGER.info("Set the response timeout: {}", responseTimeout);
        this.responseTimeout = responseTimeout;
    }

    /**
     * @param firmwarePacketTimeout
     *            the firmwarePacketTimeout to set
     */
    public void setFirmwarePacketTimeout(int firmwarePacketTimeout) {
        this.firmwarePacketTimeout = firmwarePacketTimeout;
    }

    protected MessageReceiver getMessageReceiver() {
        return messageReceiver;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName()).append("@").append(hashCode());
        sb.append(Arrays.toString(addr));
        sb.append(",magic=").append(nodeMagic);
        return sb.toString();
    }

    /**
     * @return the cached magic value of the node
     */
    public Integer getNodeMagic() {
        return nodeMagic;
    }

    /**
     * @param magic
     *            the magic to set
     */
    public void setNodeMagic(Integer magic) {
        LOGGER.debug("Set magic of node: {}", magic);
        this.nodeMagic = magic;
    }

    /**
     * Returns if the node is a bootloader node with limited functionality.
     * 
     * @return node is bootloader node
     */
    public boolean isBootloaderNode() {
        if (nodeMagic != null) {
            return (BidibLibrary.BIDIB_BOOT_MAGIC == nodeMagic);
        }
        LOGGER.warn("No magic available for current node. Assume this is a bootloader node!");
        return true;
    }

    /**
     * @return the address of the node
     */
    public byte[] getAddr() {
        return addr;
    }

    /**
     * Create a ProtocolNoAnswerException if no response is available.
     * 
     * @param messageName
     *            the message name that is inserted in the exception message
     * @return the ProtocolNoAnswerException
     */
    private ProtocolException createNoResponseAvailable(String messageName) {
        ProtocolException ex =
            new ProtocolNoAnswerException(
                "No response received from '" + messageName + "' message! Current node: " + this);
        return ex;
    }

    private ProtocolException createNotSupportedByBootloaderNode(String messageName) {
        ProtocolException ex =
            new ProtocolException("The current node is a limited bootloader node and does not support the '"
                + messageName + "' message! Current node: " + this);
        return ex;
    }

    /**
     * Prepare the next receive message number in the communication with the communication partner. Every received
     * message should be incremented by 1 and on overflow start again with 1.
     * 
     * @param message
     *            the message
     * @return the next receive message number
     */
    public int getNextReceiveMsgNum(BidibMessage message) {

        synchronized (nextReceiveMsgNumLock) {

            nextReceiveMsgNum++;
            if (nextReceiveMsgNum > 255) {
                nextReceiveMsgNum = 1;
            }
            if (message.getNum() == 0) {
                nextReceiveMsgNum = 0;
            }
        }
        return nextReceiveMsgNum;
    }

    public void adjustReceiveMsgNum(int newReceiveMsgNum) {
        LOGGER.warn("The receive message number is adjusted on request: {}, current node: {}", newReceiveMsgNum, this);

        synchronized (nextReceiveMsgNumLock) {

            nextReceiveMsgNum = newReceiveMsgNum;
            if (nextReceiveMsgNum > 255) {
                nextReceiveMsgNum = 1;
            }
        }
    }

    private int getNextSendMsgNum() {

        synchronized (nextSendMsgNumLock) {

            nextSendMsgNum++;
            if (nextSendMsgNum > 255) {
                nextSendMsgNum = 1;
            }
        }
        return nextSendMsgNum;
    }

    /**
     * Send the node changed acknowledge message.
     * 
     * @param versionNumber
     *            the version number of the node table
     * @throws ProtocolException
     */
    public void acknowledgeNodeChanged(int versionNumber) throws ProtocolException {
        // sendNoWait(new NodeChangedAckMessage(versionNumber));
        LOGGER.info("Add the NodeChangedAckMessage to sendQueue, versionNumber: {}", versionNumber);
        boolean added = sendQueue.offer(new NodeChangedAckMessage(versionNumber));
        if (!added) {
            LOGGER.warn("Add the NodeChangedAckMessage to sendQueue failed.");
            throw new ProtocolException("Add the NodeChangedAckMessage to sendQueue failed.");
        }
    }

    /**
     * Send the feedback mirror free message for the specified detector number.
     * 
     * @param detectorNumber
     *            the detector number
     * @throws ProtocolException
     */
    public void acknowledgeFree(int detectorNumber) throws ProtocolException {
        LOGGER.info("Add the FeedbackMirrorFreeMessage to sendQueue, detectorNumber: {}", detectorNumber);
        boolean added = sendQueue.offer(new FeedbackMirrorFreeMessage(detectorNumber));
        if (!added) {
            LOGGER.warn("Add the FeedbackMirrorFreeMessage to sendQueue failed.");
            throw new ProtocolException("Add the FeedbackMirrorFreeMessage to sendQueue failed.");
        }
    }

    /**
     * Send the feedback mirror multiple message.
     * 
     * @param baseAddress
     *            the base address
     * @param size
     *            the size
     * @param detectorData
     *            the detector data
     * @throws ProtocolException
     */
    public void acknowledgeMultiple(int baseAddress, int size, byte[] detectorData) throws ProtocolException {
        LOGGER.info("Send FeedbackMirrorMultipleMessage to baseAddress: {}", baseAddress);
        boolean added = sendQueue.offer(new FeedbackMirrorMultipleMessage(baseAddress, size, detectorData));
        if (!added) {
            LOGGER.warn("Add the FeedbackMirrorMultipleMessage to sendQueue failed.");
            throw new ProtocolException("Add the FeedbackMirrorMultipleMessage to sendQueue failed.");
        }
    }

    /**
     * Send the feedback mirror occupied message for the specified detector number.
     * 
     * @param detectorNumber
     *            the detector number
     * @throws ProtocolException
     */
    public void acknowledgeOccupied(int detectorNumber) throws ProtocolException {
        LOGGER.info("Add the FeedbackMirrorOccupiedMessage to sendQueue, detectorNumber: {}", detectorNumber);
        boolean added = sendQueue.offer(new FeedbackMirrorOccupiedMessage(detectorNumber));
        if (!added) {
            LOGGER.warn("Add the FeedbackMirrorOccupiedMessage to sendQueue failed.");
            throw new ProtocolException("Add the FeedbackMirrorOccupiedMessage to sendQueue failed.");
        }
    }

    /**
     * Send the feedback position occupied message for the specified decoder and location number.
     * 
     * @param decoderAddress
     *            the decoder address
     * @param locationType
     *            the location type
     * @param locationAddress
     *            the location address
     * @throws ProtocolException
     */
    public void acknowledgePosition(int decoderAddress, int locationType, int locationAddress)
        throws ProtocolException {
        LOGGER
            .info("Add the FeedbackMirrorPostionMessage to sendQueue, decoderAddress: {}, locationAddress: {}",
                decoderAddress, locationAddress);
        boolean added =
            sendQueue.offer(new FeedbackMirrorPositionMessage(decoderAddress, locationType, locationAddress));
        if (!added) {
            LOGGER.warn("Add the FeedbackMirrorPositionMessage to sendQueue failed.");
            throw new ProtocolException("Add the FeedbackMirrorPositionMessage to sendQueue failed.");
        }
    }

    /**
     * Switch on track signal on the booster.
     * 
     * @param broadcast
     *            broadcast command
     * @throws ProtocolException
     */
    public void boosterOn(byte broadcast) throws ProtocolException {
        sendNoWait(new BoostOnMessage(broadcast));
    }

    /**
     * Switch off track signal on the booster.
     * 
     * @param broadcast
     *            broadcast command
     * @throws ProtocolException
     */
    public void boosterOff(byte broadcast) throws ProtocolException {
        sendNoWait(new BoostOffMessage(broadcast));
    }

    /**
     * Get the loco addresses in the specified range from the feedback system.
     * 
     * @param begin
     *            the start of Melderbits to be transfered
     * @param end
     *            the end of Melderbits to be transfered
     */
    public void getAddressState(int begin, int end) throws ProtocolException {
        sendNoWait(new FeedbackGetAddressRangeMessage(begin, end));
    }

    /**
     * Get the current 'quality' of the track sensoring.
     */
    public void getConfidence() throws ProtocolException {
        sendNoWait(new FeedbackGetConfidenceMessage());
    }

    /**
     * Get the feature with the specified number from the node.
     * 
     * @param featureNumber
     *            the feature number
     * @return the returned feature
     * @throws ProtocolException
     */
    public Feature getFeature(int featureNumber) throws ProtocolException {
        LOGGER.info("Get feature with number: {}", featureNumber);
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support feature requests.");
            throw createNotSupportedByBootloaderNode("MSG_FEATURE_GET");
        }

        // if a node does not support the feature a feature not available response is received
        BidibMessage response =
            send(requestFactory.createFeatureGet(featureNumber), null, true, FeatureResponse.TYPE,
                FeatureNotAvailableResponse.TYPE);

        LOGGER.info("get feature with number '{}' returned: {}", featureNumber, response);
        if (response instanceof FeatureResponse) {
            Feature feature = ((FeatureResponse) response).getFeature();

            if (feature != null && !feature.isRequestedFeature(featureNumber)) {
                LOGGER
                    .warn("The requested feature ({}) does not match the returned feature: {}", featureNumber, feature);
                feature = null;
            }

            return feature;
        }
        else if (response instanceof FeatureNotAvailableResponse) {
            FeatureNotAvailableResponse result = (FeatureNotAvailableResponse) response;
            // TODO change this in version 2.0 to throw an exception
            // throw new ProtocolException("The requested feature is not available, featureNumber: "
            // + result.getFeatureNumber());
            int returnedFeatureNumber = result.getFeatureNumber();
            try {
                FeatureEnum featureEnum = FeatureEnum.valueOf(ByteUtils.getLowByte(returnedFeatureNumber));
                LOGGER.info("The requested feature is not available: {}", featureEnum);
                if (returnedFeatureNumber != featureNumber) {
                    LOGGER
                        .warn("The requested feature ({}) does not match the returned feature: {}", featureNumber,
                            featureEnum);
                }
            }
            catch (Exception ex) {
                LOGGER
                    .warn("The requested feature is not available, featureNumber: {}, returnedFeatureNumber: {}",
                        featureNumber, returnedFeatureNumber);
            }

            return null;
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return null;
        }
        throw createNoResponseAvailable("get feature");
    }

    /**
     * Request the number of features of the node. This call will reset the internal counter for the next
     * <code>getNextFeature()</code> request.
     * 
     * @return number of features on the node
     * @throws IOException
     * @throws ProtocolException
     * @throws InterruptedException
     */
    public Integer getFeatureCount() throws ProtocolException {
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support feature requests.");
            throw createNotSupportedByBootloaderNode("MSG_FEATURE_GETALL");
        }

        BidibMessage response = send(requestFactory.createFeatureGetAll(), null, true, FeatureCountResponse.TYPE);
        if (response instanceof FeatureCountResponse) {
            Integer result = ((FeatureCountResponse) response).getCount();
            return result;
        }
        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return Integer.valueOf(0);
        }

        throw createNoResponseAvailable("get feature count");
    }

    /**
     * Returns the next feature of the node. Call <code>getFeatureCount()</code> to reset the internal counter for the
     * feature index
     * 
     * @return a feature or null if no more features available
     * @throws ProtocolException
     */
    public Feature getNextFeature() throws ProtocolException {
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support feature requests.");
            throw createNotSupportedByBootloaderNode("MSG_FEATURE_GETNEXT");
        }

        BidibMessage response =
            send(requestFactory.createFeatureGetNext(), null, true, FeatureResponse.TYPE,
                FeatureNotAvailableResponse.TYPE);
        if (response instanceof FeatureResponse) {
            return ((FeatureResponse) response).getFeature();
        }
        else if (response instanceof FeatureNotAvailableResponse) {
            FeatureNotAvailableResponse result = (FeatureNotAvailableResponse) response;
            // TODO change this in version 2.0 to throw an exception
            // throw new ProtocolException("The requested feature is not available, featureNumber: "
            // + result.getFeatureNumber());
            LOGGER.warn("The requested feature is not available, featureNumber: {}", result.getFeatureNumber());
            return null;
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return null;
        }

        throw createNoResponseAvailable("get next feature");
    }

    /**
     * Returns the features of the node up top the provided feature count. Call <code>getFeatureCount()</code> to reset
     * the internal counter for the feature index and use the returned feature count as parameter for the call of this
     * method.
     * 
     * @param featureCount
     *            the number of features to read
     * @return list of features or null if no features available
     * @throws ProtocolException
     */
    public List<Feature> getFeaturesBulk(int featureCount) throws ProtocolException {
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support feature requests.");
            throw createNotSupportedByBootloaderNode("MSG_FEATURE_GETNEXT");
        }

        List<BidibCommand> messages = new LinkedList<>();
        for (int index = 0; index < featureCount; index++) {
            messages.add(requestFactory.createFeatureGetNext());
        }

        int bulkWindowSize = BULK_WINDOW_SIZE;
        if (ProductUtils.isMultiDecoder(getUniqueId())) {
            bulkWindowSize = 2;
        }

        List<BidibMessage> responses = sendBulk(bulkWindowSize, messages, true, ProcessSendQueue.enabled);

        LOGGER.debug("Recevied feature responses: {}", responses);

        if (CollectionUtils.hasElements(responses)) {
            List<Feature> features = new LinkedList<>();
            for (BidibMessage response : responses) {
                if (response instanceof FeatureResponse) {
                    Feature feature = ((FeatureResponse) response).getFeature();
                    features.add(feature);
                }
                else if (response instanceof FeatureNotAvailableResponse) {
                    FeatureNotAvailableResponse result = (FeatureNotAvailableResponse) response;
                    // TODO change this in version 2.0 to throw an exception
                    // throw new ProtocolException("The requested feature is not available, featureNumber: "
                    // + result.getFeatureNumber());
                    LOGGER.warn("The requested feature is not available, featureNumber: {}", result.getFeatureNumber());
                }
            }
            return features;
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return null;
        }

        throw createNoResponseAvailable("get next feature");
    }

    /**
     * Get the status of the track sensoring in the specified range.
     * 
     * @param begin
     *            the start of Melderbits to be transfered
     * @param end
     *            the end of Melderbits to be transfered
     */
    public void getFeedbackState(int begin, int end) throws ProtocolException {
        sendNoWait(new FeedbackGetRangeMessage(begin, end));
    }

    /**
     * Get the magic from the node.
     * 
     * @param receiveTimeout
     *            the timeout in milliseconds used to wait for a response from the node. If null the default timeout is
     *            used.
     * @return the magic
     * @throws ProtocolException
     */
    public int getMagic(Integer receiveTimeout) throws ProtocolException {
        LOGGER.debug("Get the magic, receiveTimeout: {}", receiveTimeout);

        BidibMessage response = send(requestFactory.createSysMagic(), receiveTimeout, true, SysMagicResponse.TYPE);
        LOGGER.debug("getMagic, received response: {}", response);
        if (response instanceof SysMagicResponse) {
            int magic = ((SysMagicResponse) response).getMagic();
            setNodeMagic(magic);
            return magic;
        }

        // a node must always respond to the magic request
        /*
         * if (ignoreWaitTimeout) { LOGGER.warn(
         * "No response received but ignoreWaitTimeout ist set! Return BIDIB_MAGIC_UNKNOWN!");
         * setNodeMagic(BIDIB_MAGIC_UNKNOWN); return BIDIB_MAGIC_UNKNOWN; }
         */
        LOGGER.warn("No MAGIC response received from node: {}", this);

        throw createNoResponseAvailable("get magic");
    }

    /**
     * Get the next node from the system.
     * 
     * @return the node
     * @throws ProtocolException
     */
    public Node getNextNode() throws ProtocolException {
        BidibMessage response = send(requestFactory.createNodeTabGetNext(), null, true, NodeTabResponse.TYPE);

        if (response instanceof NodeTabResponse) {
            // create a new node from the received data
            LOGGER.debug("Get next tab returned: {}, own addr: {}", response, addr);

            // create the new Node instance
            Node childNode = ((NodeTabResponse) response).getNode(addr);
            LOGGER.debug("Fetched child node: {}", childNode);
            return childNode;
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return null;
        }

        throw createNoResponseAvailable("get next node");
    }

    /**
     * Get the number of nodes from the system.
     * 
     * @return the number of nodes
     * @throws ProtocolException
     */
    public Integer getNodeCount() throws ProtocolException {
        LOGGER.debug("Get all registered nodes from system.");

        BidibMessage response = send(requestFactory.createNodeTabGetAll(), null, true, NodeTabCountResponse.TYPE);

        if (response instanceof NodeTabCountResponse) {
            int totalNodes = ((NodeTabCountResponse) response).getCount();

            LOGGER.debug("Found total nodes: {}", totalNodes);
            return totalNodes;
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return Integer.valueOf(0);
        }

        throw createNoResponseAvailable("get node count");
    }

    /**
     * Get the protocol version of the node.
     * 
     * @return the protocol version
     * @throws ProtocolException
     */
    public ProtocolVersion getProtocolVersion() throws ProtocolException {

        if (protocolVersion == null) {
            BidibMessage response = send(new SysGetPVersionMessage(), null, true, SysPVersionResponse.TYPE);
            if (response instanceof SysPVersionResponse) {
                protocolVersion = ((SysPVersionResponse) response).getVersion();
                LOGGER.info("ProtocolVersion of current node: {}", protocolVersion);
                return protocolVersion;
            }

            if (ignoreWaitTimeout) {
                LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
                return null;
            }

            throw createNoResponseAvailable("get protocol version");
        }
        LOGGER.debug("Return the cached protocol version: {}", protocolVersion);
        return protocolVersion;
    }

    /**
     * Get the software version from the node.
     * 
     * @return the software version
     * @throws ProtocolException
     */
    public SoftwareVersion getSwVersion() throws ProtocolException {
        BidibMessage response = send(new SysGetSwVersionMessage(), null, true, SysSwVersionResponse.TYPE);
        if (response instanceof SysSwVersionResponse) {
            return ((SysSwVersionResponse) response).getVersion();
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return null;
        }

        throw createNoResponseAvailable("get sw version");
    }

    /**
     * Get the unique id from the node.
     * 
     * @return the unique id
     * @throws ProtocolException
     */
    public long getUniqueId() throws ProtocolException {

        return getUniqueId(false);
    }

    /**
     * Get the unique id from the node.
     * 
     * @param ignoreCache
     *            ignore the cached value
     * @return the unique id
     * @throws ProtocolException
     */
    public long getUniqueId(boolean ignoreCache) throws ProtocolException {

        if (ignoreCache || uniqueId == null) {

            BidibMessage response = send(new SysGetUniqueIdMessage(), null, true, SysUniqueIdResponse.TYPE);
            if (response instanceof SysUniqueIdResponse) {
                byte[] encodedUniqueId = ((SysUniqueIdResponse) response).getUniqueId();

                // keep the uniqueId
                uniqueId = NodeUtils.getUniqueId(encodedUniqueId);
                if (uniqueId != null) {
                    LOGGER.info("Fetched uniqueId from node: {}", NodeUtils.getUniqueIdAsString(uniqueId));
                }
                return uniqueId;
            }

            throw createNoResponseAvailable("get unique id");
        }
        // return the uniqueId from the cache
        return uniqueId;
    }

    public void getKeyState(List<Integer> portIds) throws ProtocolException {
        // response is signaled asynchronously

        int windowSize = BULK_WINDOW_SIZE;
        LOGGER.info("Get key states with bulk read for windowSize: {}, portIds: {}", windowSize, portIds);

        List<BidibCommand> messages = new LinkedList<>();
        for (int portId : portIds) {
            messages.add(requestFactory.createLcKey(portId));
        }

        // send bulk messages and wait for bulk answers
        sendBulk(windowSize, messages, true, ProcessSendQueue.enabled);
        LOGGER.info("Send LcKey messages finished.");
    }

    /**
     * Sets the identify state of the node. The result is received asynchronously.
     * 
     * @param state
     *            the identify state to set.
     * @throws ProtocolException
     */
    public void identify(IdentifyState state) throws ProtocolException {
        // do not wait for the response
        sendNoWait(new SysIdentifyMessage(state));
    }

    /**
     * Verify if the node supports FW updates.
     * 
     * @return true if the node has the FW update feature set, false otherwise
     * @throws ProtocolException
     */
    public boolean isUpdatable(Node node) throws ProtocolException {
        try {
            Feature feature = getFeature(BidibLibrary.FEATURE_FW_UPDATE_MODE);
            if (feature != null) {
                node.setFeature(feature);
                return feature.getValue() == 1;
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Check if node is updatable caused protocol exception.", ex);
        }
        return false;
    }

    /**
     * Send the ping message to the node.
     * 
     * @param marker
     *            the marker value
     * @throws ProtocolException
     */
    public void ping(byte marker) throws ProtocolException {
        sendNoWait(new SysPingMessage(marker));
    }

    /**
     * Send the system reset message to the node.
     * 
     * @throws ProtocolException
     */
    public void reset() throws ProtocolException {
        sendNoWait(new SysResetMessage());
    }

    /**
     * Wait until the expected response is received or the timeout has expired.
     * 
     * @param expectedResponseTypes
     *            list of the expected response message types
     * @param receiveTimeout
     *            the timeout in milliseconds used to wait for a response from the node. If <code>null</code> the
     *            default timeout is used.
     * @return the received response
     * @throws InterruptedException
     *             thrown if wait wait for response is interrupted
     */
    private BidibMessage receive(List<Integer> expectedResponseTypes, Integer receiveTimeout)
        throws InterruptedException {
        BidibMessage result = null;
        LOGGER.info("Receive message, expected responseTypes: {}", expectedResponseTypes);

        try {
            result = getMessage(expectedResponseTypes, receiveTimeout);
        }
        finally {
            if (result == null) {
                // no result was fetched
                resetNextSendMsgNum();
            }
        }

        LOGGER.info("Received result: {}", result);
        return result;
    }

    public void addResponseToReceiveQueue(final BidibMessage message) {
        LOGGER.debug("Offer received message: {} ", message);
        try {
            boolean successful = receiveQueue.offer(message);

            // LOGGER.info("Offer received message was successful: {}", successful);

            if (!successful) {
                LOGGER.error("Add response to receive queue failed: {}", message);
            }
        }
        catch (Exception ex) {
            LOGGER.error("Offer received message to node failed. Message was: " + message, ex);
        }
    }

    /**
     * Get a message from the receiveQueue for the defined timeout period.
     * 
     * @param responseTypes
     *            the optional list of responseType ids to wait for
     * @param receiveTimeout
     *            the timeout in milliseconds used to wait for a response from the node. If <code>null</code> the
     *            default timeout is used.
     * 
     * @return the received message or null if no message was received during the defined period.
     * @throws InterruptedException
     *             thrown if wait wait for response is interrupted
     */
    public BidibMessage getMessage(List<Integer> responseTypes, Integer receiveTimeout) throws InterruptedException {
        LOGGER.debug("get message with responseType: {}", responseTypes);
        BidibMessage result = null;

        // wait configured time to receive message
        long cancelReceiveTs = System.currentTimeMillis() + BidibInterface.DEFAULT_TIMEOUT;
        boolean leaveLoop = false;

        do {
            if (receiveTimeout == null) {
                result = receiveQueue.poll(responseTimeout, TimeUnit.MILLISECONDS);
            }
            else {
                LOGGER.trace("+++ Wait for response: {}", receiveTimeout);
                result = receiveQueue.poll(receiveTimeout, TimeUnit.MILLISECONDS);
                LOGGER.trace("+++ Polled response: {}", result);
            }

            long now = System.currentTimeMillis();

            // handling of specific response type
            if (result instanceof SysErrorResponse) {
                LOGGER.warn("Received an error response: {}", result);
                leaveLoop = true;
            }
            else if (result != null && CollectionUtils.hasElements(responseTypes)) {
                BidibMessage response = null;
                for (Integer responseType : responseTypes) {
                    LOGGER
                        .debug("Check if the response type is equal, responseType: {}, response.type: {}",
                            responseType.byteValue(), result.getType());

                    if (responseType == ByteUtils.getInt(result.getType())) {
                        LOGGER.info("This is the expected response: {}", result);
                        response = result;
                        break;
                    }
                }

                if (response == null) {
                    LOGGER.debug("This is NOT the expected response: {}", result);
                    result = null;
                }

                LOGGER.debug("startReceive: {}, now: {}, result: {}", cancelReceiveTs, now, result);
                if (result != null || cancelReceiveTs < now) {
                    LOGGER.debug("leave loop.");
                    leaveLoop = true;
                }
            }
            // handling if no specific response type expected ...
            else if (result != null || (cancelReceiveTs < now)) {
                LOGGER.debug("leave loop, result: {}", result);
                leaveLoop = true;
            }
        }
        while (!leaveLoop);
        LOGGER.debug("Return received message: {}", result);
        return result;
    }

    private void resetNextSendMsgNum() {
        // reset the counter. The next message will be sent with SendMsgNum=0 which
        // will reset the counter on the receiver side
        synchronized (nextSendMsgNumLock) {

            nextSendMsgNum = -1;
        }
    }

    private AtomicInteger bulkAnswerCounter = new AtomicInteger();

    private void waitForBulkAnswer(int prevReceivedMessages) {
        try {
            synchronized (bulkAnswerCounter) {

                if (bulkAnswerCounter.get() > prevReceivedMessages) {
                    LOGGER
                        .info("Received response already, prevReceivedMessages: {}, current receive counter: {}",
                            prevReceivedMessages, bulkAnswerCounter.get());
                }
                else {
                    LOGGER.info("waitForBulkAnswer, responseTimeout: {}", responseTimeout);
                    bulkAnswerCounter.wait(responseTimeout);

                    LOGGER.info("waitForBulkAnswer, after wait");
                }
            }
        }
        catch (InterruptedException ex) {
            LOGGER.warn("Wait for bulk answer failed.", ex);
        }
    }

    private Integer[] expectedBulkAnswers;

    public void notifyBulkAnswer(Integer answerType) {
        try {
            synchronized (bulkAnswerCounter) {
                boolean notifyAnswer = false;
                if (expectedBulkAnswers != null && answerType != null) {
                    for (Integer expectedAnswerType : expectedBulkAnswers) {
                        if (expectedAnswerType.equals(answerType)) {
                            LOGGER.info("notifyBulkAnswer, found expected answer type: {}", answerType);
                            notifyAnswer = true;
                            break;
                        }
                    }
                }
                else {
                    notifyAnswer = true;
                }

                if (notifyAnswer) {
                    int current = bulkAnswerCounter.incrementAndGet();

                    LOGGER.info("Notify bulk answer after increment, current receive counter: {}", current);

                    bulkAnswerCounter.notifyAll();
                }
                else {
                    LOGGER.info("Received unexpected answer, don't increment and notify bulk answer counter.");
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Wait for bulk answer failed.", ex);
        }
    }

    /**
     * Send a message without waiting for response.
     * 
     * @param message
     *            the message to send
     * @throws ProtocolException
     */
    protected void sendNoWait(BidibCommand message) throws ProtocolException {
        send(message, null, false, (Integer) null);
    }

    /**
     * Send a message and wait for the unspecific result.
     * 
     * @param message
     *            the message to send
     * @return the received result
     * @throws ProtocolException
     */
    protected BidibMessage send(BidibCommand message) throws ProtocolException {
        return send(message, null, true, (Integer) null);
    }

    /**
     * Send a message and wait for answer if expectAnswer is true.
     * 
     * @param message
     *            the message to send
     * @param receiveTimeout
     *            the timeout in milliseconds used to wait for a response from the node. If <code>null</code> the
     *            default timeout is used.
     * @param expectAnswer
     *            answer is expected, this will cause to wait for an answer
     * @param expectedResponseTypes
     *            the list of expected type of response (optional)
     * @return the response from the node or null if no answer was expected
     * @throws ProtocolException
     *             thrown if no response was received if an answer was expected
     */
    protected BidibMessage send(
        BidibCommand message, Integer receiveTimeout, boolean expectAnswer, Integer... expectedResponseTypes)
        throws ProtocolException {
        BidibMessage response = null;

        // the send block is synchronized
        synchronized (sendLock) {
            LOGGER.info("Send message.");

            synchronized (bulkAnswerCounter) {
                resetExpectedBulkAnswers();
            }

            prepareAndSendMessage(message);

            if (expectAnswer) {
                // wait for the answer
                try {
                    response =
                        receive((expectedResponseTypes != null && expectedResponseTypes[0] != null)
                            ? Arrays.asList(expectedResponseTypes) : null, receiveTimeout);
                }
                catch (InterruptedException ex) {
                    LOGGER.warn("Waiting for response was interrupted", ex);
                }
                catch (Exception ex) {
                    LOGGER.warn("Waiting for response caused an exception.", ex);
                    throw new ProtocolNoAnswerException("Failed to get the resonse for: " + message);
                }

                if (response == null && !ignoreWaitTimeout) {
                    LOGGER.warn("Receive message timed out. Get response failed for message:  {}", message);
                    throw new ProtocolNoAnswerException("Got no answer to " + message);
                }
                LOGGER.debug("Return the response message: {}", response);
            }
        }
        return response;
    }

    /**
     * Encode the message and prepare the send message number.
     * 
     * @param message
     *            the message
     * @return the encoded message with the send message number
     */
    protected EncodedMessage encodeMessage(BidibCommand message) {

        // get the next send msg number
        int num = getNextSendMsgNum();
        message.setSendMsgNum(num);

        // logRecord.append("send ").append(message).append(" to ").append(this);

        byte type = message.getType();
        byte[] data = message.getData();
        byte[] bytes = null;
        int index = 0;

        LOGGER.trace("Current node addr: {}", addr);

        // the addr is stored in the node
        message.setAddr(addr);
        if (addr.length != 0 && addr[0] != 0) {
            bytes = new byte[1 + (addr.length + 1) + 2 + (data != null ? data.length : 0)];
            bytes[index++] = (byte) (bytes.length - 1);
            for (int addrIndex = 0; addrIndex < addr.length; addrIndex++) {
                bytes[index++] = addr[addrIndex];
            }
        }
        else {
            LOGGER.trace("Current address is the root node.");
            bytes = new byte[1 + (addr.length /* len of root node */) + 2 + (data != null ? data.length : 0)];
            bytes[index++] = (byte) (bytes.length - 1);
        }
        bytes[index++] = 0; // 'terminating zero' of the address

        bytes[index++] = (byte) num;
        bytes[index++] = type;
        if (data != null) {
            // LOGGER.debug("Add data: {}", ByteUtils.bytesToHex(data));
            for (int dataIndex = 0; dataIndex < data.length; dataIndex++) {
                bytes[index++] = data[dataIndex];
            }
        }
        EncodedMessage encodedMessage = new EncodedMessage(bytes);
        return encodedMessage;
    }

    public static class EncodedMessage {
        private byte[] message;

        public EncodedMessage(byte[] message) {
            this.message = message;
        }

        public byte[] getMessage() {
            return message;
        }
    }

    // the prepareAndSendMessage and prepareAndSendMessages must be
    // called exclusive to send the correct message number

    private void prepareAndSendMessage(BidibCommand message) {

        // encode the message (adds the message number)
        EncodedMessage encodedMessage = encodeMessage(message);

        sendMessage(encodedMessage, message);
    }

    private void prepareAndSendMessages(List<BidibCommand> messages) throws ProtocolException {

        try {
            List<EncodedMessage> encodedMessages = new LinkedList<EncodedMessage>();
            for (BidibCommand message : messages) {

                // encode the message (adds the message number)
                EncodedMessage encodedMessage = encodeMessage(message);
                encodedMessages.add(encodedMessage);
            }
            sendMessages(encodedMessages, messages);
        }
        catch (IOException ex) {
            LOGGER.warn("Send messages failed.", ex);
            throw new ProtocolException("Send messages failed: " + messages);
        }
    }

    private void sendMessages(List<EncodedMessage> encodedMessages, List<BidibCommand> bidibMessages)
        throws IOException {

        output.reset();

        for (EncodedMessage encodedMessage : encodedMessages) {
            byte[] message = encodedMessage.getMessage();
            output.write(message);
        }

        // log the bidib message and the content of the "output" stream
        StringBuilder sb = new StringBuilder(">> ");
        sb.append(bidibMessages);
        sb.append(" : ");
        sb.append(ByteUtils.bytesToHex(output));
        MSG_TX_LOGGER.info(sb.toString());

        // send the output to Bidib
        bidib.send(output.toByteArray());

        output.reset();
    }

    /**
     * Put the contents of the message into the "output" stream and call the flush method that sends the content of the
     * "output" stream to bidib.
     * 
     * @param message
     *            the message contents
     * @param bidibMessage
     *            the bidib message instance
     * @throws IOException
     */
    private void sendMessage(EncodedMessage encodedMessage, BidibCommand bidibMessage) {
        byte[] message = encodedMessage.getMessage();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Send the message: {}", ByteUtils.bytesToHex(message));
        }

        synchronized (outputStreamLock) {

            // log the bidib message and the content of the "output" stream
            StringBuilder sb = new StringBuilder(">> ");
            sb.append(bidibMessage);
            sb.append(" : ");
            sb.append(ByteUtils.bytesToHex(message));
            MSG_TX_LOGGER.info(sb.toString());

            // send the output to Bidib
            bidib.send(message);
        }
    }

    private void resetExpectedBulkAnswers() {
        LOGGER.debug("Reset the expectedBulkAnswers.");
        expectedBulkAnswers = null;
        bulkAnswerCounter.set(0);
    }

    /**
     * Send a bulk of messages with a specified window size to the node.
     * 
     * @param windowSize
     *            the window size
     * @param messages
     *            the messages
     * @param waitForBulkAnswer
     *            wait for bulk answer if no expected response types are defined
     * @return the list of responses
     * @throws ProtocolException
     */
    protected List<BidibMessage> sendBulk(
        int windowSize, List<BidibCommand> messages, boolean waitForBulkAnswer, ProcessSendQueue processSendQueue)
        throws ProtocolException {

        List<BidibMessage> responses = null;

        // the send block is synchronized
        synchronized (sendLock) {

            int numMessages = messages.size();
            LOGGER.debug("Send bulk messages total: {}, windowSize: {}", numMessages, windowSize);

            if (numMessages == 0) {
                LOGGER.warn("No messages to send available.");
                return null;
            }

            // we must reset the bulk answer counter
            synchronized (bulkAnswerCounter) {
                LOGGER.debug("Reset the bulk answer counter for node: {}", this);
                // bulkAnswerCounter.set(0);

                // TODO changed this hack ...
                resetExpectedBulkAnswers();
                if (messages.get(0) instanceof BidibBulkCommand) {
                    BidibBulkCommand bulkCommand = (BidibBulkCommand) messages.get(0);
                    expectedBulkAnswers = bulkCommand.getExpectedBulkResponseTypes();
                    LOGGER.info("Expect bulk answers: {}", new Object[] { expectedBulkAnswers });
                }
            }

            // maximum wait duration for answers
            long maxWaitDuration = numMessages * (BidibInterface.DEFAULT_TIMEOUT * 2);
            long startOperationTs = System.currentTimeMillis();

            // send messages with a window-size
            int fromIndex = 0;
            int receivedMessages = 0;
            List<BidibCommand> messagesToWaitForResponse = new LinkedList<BidibCommand>();
            boolean noAnswerExpected = false;

            while (fromIndex < numMessages) {
                // calculate the index of messages to send
                LOGGER.debug("Send bulk messages fromIndex: {}, numMessages: {}", fromIndex, numMessages);

                // the window size must be dynamically calculated on the maximum expected size of the responses of 48
                // bytes
                // see http://forum.opendcc.de/viewtopic.php?f=31&t=1332
                int toIndex = fromIndex;
                int maxTotalExpectedResponseLength = 48;
                int totalExpectedResponseLength = 0;
                List<BidibCommand> messagesToSend = new LinkedList<>();

                // add pending acknowledges from spontaneous events before the next messages

                if ((ProcessSendQueue.enabled == processSendQueue) && !sendQueue.isEmpty()) {
                    LOGGER.info("Insert send pending acknowledge message.");
                    BidibCommand acknowledge = null;
                    do {
                        acknowledge = sendQueue.poll();
                        if (acknowledge != null) {
                            LOGGER.info("Add acknowledge message to send: {}", acknowledge);
                            messagesToSend.add(acknowledge);
                        }
                    }
                    while (acknowledge != null);
                }

                // add the next messages to send
                for (int index = fromIndex; index < numMessages; index++) {
                    BidibCommand command = messages.get(index);
                    if (totalExpectedResponseLength + command.getAnswerSize() <= maxTotalExpectedResponseLength) {
                        // add the command to send
                        messagesToSend.add(command);
                        toIndex++;
                        // fromIndex++;
                        totalExpectedResponseLength += command.getAnswerSize();

                        LOGGER.debug("Current totalExpectedResponseLength: {}", totalExpectedResponseLength);
                    }
                    else {
                        // max total response size exceeded
                        LOGGER.debug("Max total response size exceeded.");
                        break;
                    }

                    if (windowSize > -1 && (toIndex - fromIndex >= windowSize)) {
                        // window size exceeded
                        LOGGER.debug("Window size exceeded, stop adding messages to send.");
                        break;
                    }
                }

                // clear the messages to wait for a response
                messagesToWaitForResponse.clear();
                // check if we have to wait for responses
                for (BidibCommand message : messagesToSend) {
                    if (message.getExpectedResponseTypes() != null) {
                        // we must wait for the response
                        messagesToWaitForResponse.add(message);
                    }
                }

                LOGGER.info("Prepared messages to send: {}", messagesToSend);

                // prepare and send the messages
                prepareAndSendMessages(messagesToSend);

                // send the next message if one is received
                fromIndex = toIndex;

                LOGGER.debug("Prepeared new fromIndex: {}, toIndex: {}", fromIndex, toIndex);

                // handle received responses ...
                noAnswerExpected = false;
                if (!messagesToWaitForResponse.isEmpty()) {

                    LOGGER.info("Wait for responses: {}", messagesToWaitForResponse);

                    BidibMessage response = null;
                    // wait for the answer
                    try {
                        if (responses == null) {
                            responses = new LinkedList<BidibMessage>();
                        }
                        int responseIndex = 0;
                        while (receivedMessages < numMessages) {
                            LOGGER
                                .trace("Receive response, receivedMessages: {}, numMessages: {}", receivedMessages,
                                    numMessages);
                            response =
                                receive(Arrays
                                    .asList(messagesToWaitForResponse.get(responseIndex).getExpectedResponseTypes()),
                                    null);

                            int expectedAnswerSize = messagesToWaitForResponse.get(responseIndex).getAnswerSize();

                            responseIndex++;

                            LOGGER
                                .trace("Inside sendBulk, received message response: {}, expectedAnswerSize: {}",
                                    response, expectedAnswerSize);

                            if (response != null) {
                                responses.add(response);

                                totalExpectedResponseLength -= expectedAnswerSize;

                            }
                            receivedMessages++;

                            if (receivedMessages < fromIndex) {
                                LOGGER
                                    .trace(
                                        "Wait for all messages that were send in the current window, receivedMessages: {}, fromIndex: {}",
                                        receivedMessages, fromIndex);
                                continue;
                            }
                            LOGGER.trace("All messages of the sent window were received.");

                            if (fromIndex < numMessages) {
                                LOGGER.trace("Not all messages sent yet. Leave receive loop to send next message.");
                                break;
                            }
                        }
                    }
                    catch (InterruptedException ex) {
                        LOGGER.warn("Waiting for response was interrupted", ex);
                    }
                    if (responses.size() == 0 && !ignoreWaitTimeout) {
                        LOGGER.warn("Receive message timed out. Get response failed for messages:  {}", messages);
                        throw new ProtocolNoAnswerException("Got no answer to " + messages);
                    }
                }
                else if (waitForBulkAnswer) {
                    LOGGER.info("Wait for bulk answers.");

                    try {
                        // the bulkAnswerCounter is used to wait for responses
                        int expectedBulkAnswerCount = numMessages;
                        boolean waitForMultipleResponses = false;

                        // TODO replace this hack
                        if (messages.get(0) instanceof LcConfigXGetAllMessage) {
                            LcConfigXGetAllMessage configXGetAllMessage = (LcConfigXGetAllMessage) messages.get(0);
                            expectedBulkAnswerCount = configXGetAllMessage.getExpectedCountResponses();

                            waitForMultipleResponses = true;

                            if (expectedBulkAnswerCount == 0) {
                                expectedBulkAnswerCount = numMessages;
                            }

                            maxWaitDuration = expectedBulkAnswerCount * (BidibInterface.DEFAULT_TIMEOUT * 2);
                            LOGGER
                                .info("Adjusted expectedBulkAnswerCount to new value: {}, maxWaitDuration: {}",
                                    expectedBulkAnswerCount, maxWaitDuration);
                        }

                        while (receivedMessages < expectedBulkAnswerCount) {
                            LOGGER
                                .info(
                                    "Wait for bulk response response, receivedMessages: {}, expectedBulkAnswerCount: {}",
                                    receivedMessages, expectedBulkAnswerCount);

                            waitForBulkAnswer(receivedMessages);

                            synchronized (bulkAnswerCounter) {
                                // update the new number of received messages
                                receivedMessages = bulkAnswerCounter.get();
                                LOGGER
                                    .info("sendBulk, after wait for bulk answer, receivedMessages: {}",
                                        receivedMessages);
                            }

                            // add a maximum time to wait for answers
                            long now = System.currentTimeMillis();
                            if (now > (startOperationTs + maxWaitDuration)) {
                                LOGGER.warn("The maximum time to wait for answers has exceeded on node: {}", this);
                                throw new ProtocolNoAnswerException(
                                    "The maximum time to wait for answers has exceeded.");
                            }

                            if (receivedMessages < fromIndex) {
                                LOGGER
                                    .info(
                                        "Wait for all bulk answers that were send in the current window, receivedMessages: {}, fromIndex: {}",
                                        receivedMessages, fromIndex);
                                continue;
                            }
                            else if (waitForMultipleResponses && (receivedMessages < expectedBulkAnswerCount)) {
                                LOGGER
                                    .info(
                                        "Wait for multiple bulk answers that should be received in the current window, receivedMessages: {}, expectedBulkAnswerCount: {}",
                                        receivedMessages, expectedBulkAnswerCount);
                                continue;
                            }

                            LOGGER
                                .info(
                                    "All messages of the sent window were received, fromIndex: {}, expectedBulkAnswerCount: {}",
                                    fromIndex, expectedBulkAnswerCount);

                            if (fromIndex < expectedBulkAnswerCount) {
                                LOGGER.trace("Not all messages sent yet. Leave receive loop to send next message.");
                                break;
                            }
                        }
                    }
                    catch (ProtocolNoAnswerException ex) {
                        LOGGER.warn("The required answer from node was not received, node: {}", this);
                        // TODO we must signal the error to the caller
                        break;
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Waiting for bulk answer was interrupted", ex);
                    }

                }
                else {
                    LOGGER.info("No answer expected in send bulk.");
                    noAnswerExpected = true;
                }
            }
            LOGGER.debug("Return the response messages: {}", responses);

            // TODO check this log statement ...
            if (!waitForBulkAnswer && !noAnswerExpected) {
                if (responses == null) {
                    LOGGER.warn("No responses received! Expected: {}", numMessages);
                }
                else if (responses.size() < numMessages) {
                    LOGGER.warn("Received not all responses! Expected: {}, actual: {}", numMessages, responses.size());
                }
            }

            LOGGER.debug("Send bulk messages has finished.");
        }
        return responses;
    }

    /**
     * Process the pending send queue that might contain acknowledge messages.
     * 
     * @throws ProtocolException
     */
    public void processPendingSendQueue() throws ProtocolException {
        LOGGER.info("Process the pending acknowledge messages in the send queue.");

        synchronized (sendLock) {
            List<BidibCommand> commandsToSend = new ArrayList<BidibCommand>();
            BidibCommand acknowledge = null;
            do {
                acknowledge = sendQueue.poll();

                if (acknowledge != null) {
                    commandsToSend.add(acknowledge);
                }
            }
            while (acknowledge != null);

            if (!commandsToSend.isEmpty()) {
                LOGGER.info("Send the acknowledge messages for current node: {}", this);
                sendBulk(commandsToSend.size(), commandsToSend, false, ProcessSendQueue.disabled);
            }
            else {
                LOGGER.info("No acknowledge messages to send for current node: {}", this);
            }
        }

    }

    /**
     * Send an firmware update operation message to the node.
     * 
     * @param operation
     *            the operation identifier
     * @param data
     *            the data to send
     * @return the returned firmware update status
     * @throws ProtocolException
     */
    public FirmwareUpdateStat sendFirmwareUpdateOperation(FirmwareUpdateOperation operation, byte... data)
        throws ProtocolException {

        Integer receiveTimeout = firmwarePacketTimeout;

        if (FirmwareUpdateOperation.EXIT == operation) {
            LOGGER.info("The operation is done. Give the node more time do do the work.");
            receiveTimeout = 4 * firmwarePacketTimeout;
        }

        BidibMessage response =
            send(new FwUpdateOpMessage(operation, data), receiveTimeout, true, FwUpdateStatResponse.TYPE);
        if (response instanceof FwUpdateStatResponse) {
            return ((FwUpdateStatResponse) response).getUpdateStat();
        }

        throw createNoResponseAvailable("firmware update operation");
    }

    /**
     * Sets the feature value on the node.
     * 
     * @param number
     *            the feature number
     * @param value
     *            the feature value
     * @throws ProtocolException
     */
    public Feature setFeature(int number, int value) throws ProtocolException {
        BidibMessage response =
            send(requestFactory.createFeatureSet(number, value), null, true, FeatureResponse.TYPE,
                FeatureNotAvailableResponse.TYPE);
        if (response instanceof FeatureResponse) {
            Feature result = ((FeatureResponse) response).getFeature();
            return result;
        }
        else if (response instanceof FeatureNotAvailableResponse) {
            FeatureNotAvailableResponse result = (FeatureNotAvailableResponse) response;
            throw new ProtocolException(
                "The requested feature is not available, featureNumber: " + result.getFeatureNumber());
        }

        if (ignoreWaitTimeout) {
            LOGGER.warn("No response received but ignoreWaitTimeout ist set! Current node: {}", this);
            return null;
        }

        throw createNoResponseAvailable("feature set");
    }

    /**
     * Send the system disable message to the node.
     * 
     * @throws ProtocolException
     */
    public void sysDisable() throws ProtocolException {
        sendNoWait(requestFactory.createSysDisable());
    }

    /**
     * Send the system enable message to the node.
     * 
     * @throws ProtocolException
     */
    public void sysEnable() throws ProtocolException {
        sendNoWait(requestFactory.createSysEnable());
    }

    /**
     * Send the vendor disable message to the node.
     * 
     * @return the user config mode is off
     * @throws ProtocolException
     */
    public boolean vendorDisable() throws ProtocolException {
        BidibMessage result = send(new VendorDisableMessage(), null, true, VendorAckResponse.TYPE);
        if (result instanceof VendorAckResponse) {
            return ((VendorAckResponse) result).getReturnCode() == VendorAckResponse.USER_CONFIG_MODE_OFF;
        }
        return false;
    }

    /**
     * Send the vendor enable message to the node.
     * 
     * @return the user config mode is on
     * @throws ProtocolException
     */
    public boolean vendorEnable(long uniqueId) throws ProtocolException {
        BidibMessage result = send(new VendorEnableMessage(uniqueId), null, true, VendorAckResponse.TYPE);
        if (result instanceof VendorAckResponse) {
            return ((VendorAckResponse) result).getReturnCode() == VendorAckResponse.USER_CONFIG_MODE_ON;
        }
        return false;
    }

    /**
     * Get the vendor data with the provided name from the node.
     * 
     * @param name
     *            the vendor specific name
     * @return the current vendor data values received from the node
     * @throws ProtocolException
     */
    public VendorData vendorGet(String name) throws ProtocolException {
        LOGGER.info("Get vendor message, name: {}", name);
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support vendor data requests.");
            throw createNotSupportedByBootloaderNode("MSG_VENDOR_GET");
        }

        BidibCommand bidibCommand = requestFactory.createVendorGet(name);
        BidibMessage result = send(bidibCommand, null, true, VendorResponse.TYPE);
        if (result instanceof VendorResponse) {
            return ((VendorResponse) result).getVendorData();
        }
        return null;
    }

    /**
     * Get the vendor data with the provided name from the node.
     * 
     * @param cvNumbers
     *            the list of CV numbers to fetch
     * @return the current vendor data values received from the node
     * @throws ProtocolException
     */
    public List<VendorData> vendorGetBulk(List<String> cvNumbers) throws ProtocolException {
        LOGGER.info("Get vendor message, cvNumbers: {}", cvNumbers);
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support vendor data requests.");
            throw createNotSupportedByBootloaderNode("MSG_VENDOR_GET");
        }

        // prepare all messages
        List<BidibCommand> messages = new LinkedList<BidibCommand>();
        for (String name : cvNumbers) {
            LOGGER.debug("Add new CV name: {}", name);
            BidibCommand bidibCommand = requestFactory.createVendorGet(name);
            messages.add(bidibCommand);
        }

        List<BidibMessage> results = sendBulk(BULK_WINDOW_SIZE, messages, true, ProcessSendQueue.enabled);
        if (results != null) {
            List<VendorData> vendorDataList = new LinkedList<VendorData>();
            for (BidibMessage result : results) {
                if (result instanceof VendorResponse) {
                    VendorData vendorData = ((VendorResponse) result).getVendorData();
                    LOGGER.debug("Received vendor data: {}", vendorData);
                    vendorDataList.add(vendorData);
                }
            }
            return vendorDataList;
        }
        else {
            LOGGER.warn("No result returned from sendBulk!");
        }
        return null;
    }

    /**
     * Set the provided vendor data on the node.
     * 
     * @param name
     *            the vendor specific name
     * @param value
     *            the value to set
     * @return the current vendor data values received from the node
     * @throws ProtocolException
     */
    public VendorData vendorSet(String name, String value) throws ProtocolException {
        if (isBootloaderNode()) {
            LOGGER.warn("The current node is a bootloader node and does not support vendor data requests.");
            throw createNotSupportedByBootloaderNode("MSG_VENDOR_SET");
        }

        BidibMessage result = send(new VendorSetMessage(name, value), null, true, VendorResponse.TYPE);
        if (result instanceof VendorResponse) {
            return ((VendorResponse) result).getVendorData();
        }
        LOGGER.warn("No result received for name: {}, value: {}", name, value);
        return null;
    }

    /**
     * Get a string value from the node.
     * 
     * @param namespace
     *            the namespace
     * @param index
     *            the index
     * @return the string data instance
     * @throws ProtocolException
     */
    public StringData getString(int namespace, int index) throws ProtocolException {
        BidibMessage response = send(new StringGetMessage(namespace, index), null, true, StringResponse.TYPE);
        if (response instanceof StringResponse) {
            return ((StringResponse) response).getStringData();
        }
        return null;
    }

    /**
     * Set a string value in the node.
     * 
     * @param namespace
     *            the namespace
     * @param index
     *            the index
     * @return the string data instance
     * @throws ProtocolException
     */
    public StringData setString(int namespace, int index, String value) throws ProtocolException {
        BidibMessage response = send(new StringSetMessage(namespace, index, value), null, true, StringResponse.TYPE);
        if (response instanceof StringResponse) {
            return ((StringResponse) response).getStringData();
        }
        return null;
    }

    public void setOutput(PortModelEnum portModel, LcOutputType outputType, int outputNumber, int state)
        throws ProtocolException {
        LOGGER
            .debug("Set the new output state, type: {}, outputNumber: {}, state: {}", outputType, outputNumber, state);

        // the response MSG_LC_STAT is signaled asynchronously
        BidibCommand command = requestFactory.createLcOutputMessage(portModel, outputType, outputNumber, state);
        sendNoWait(command);
    }

    public void queryOutputState(PortModelEnum portModel, LcOutputType outputType, int outputNumber)
        throws ProtocolException {
        LOGGER.info("Query the output state, type: {}, outputNumber: {}", outputType, outputNumber);
        BidibCommand command = requestFactory.createLcOutputQuery(portModel, outputType, outputNumber);
        sendNoWait(command);
    }

    public void queryPortStates(PortModelEnum portModel, LcOutputType outputType, List<Integer> portIds)
        throws ProtocolException {
        // response is signaled asynchronously

        int windowSize = BULK_WINDOW_SIZE;
        LOGGER.info("Get port states with bulk read for windowSize: {}, portIds: {}", windowSize, portIds);

        List<BidibCommand> messages = new LinkedList<>();
        for (int portId : portIds) {
            messages.add(requestFactory.createLcOutputQuery(portModel, outputType, portId));
        }

        // send bulk messages and wait for bulk answers
        sendBulk(windowSize, messages, true, ProcessSendQueue.enabled);
        LOGGER.info("Send LcOutputQuery messages finished.");
    }

    public void queryPortStatesAll(PortModelEnum portModel, int portTypeMask, int rangeFrom, int rangeTo)
        throws ProtocolException {
        LOGGER
            .info("Query the output state for all ports ,portTypeMask: {}, rangeFrom: {}, rangeTo: {}", portTypeMask,
                rangeFrom, rangeTo);
        BidibCommand command = requestFactory.createPortQueryAll(portTypeMask, rangeFrom, rangeTo);
        sendNoWait(command);
    }

    public void setConfig(PortModelEnum portModel, LcConfig config) throws ProtocolException {
        LOGGER.debug("Send LcConfigSet to node, config: {}", config);

        send(new LcConfigSetMessage(portModel, config), null, false, LcConfigResponse.TYPE,
            LcNotAvailableResponse.TYPE);
    }

    /**
     * Set the port confix and wait max. 1000ms for a response.
     * 
     * @param portModel
     *            the port model
     * @param config
     *            the config map
     * @throws ProtocolException
     */
    public void setConfigX(PortModelEnum portModel, LcConfigX config) throws ProtocolException {
        LOGGER.info("Send LcConfigXSet to node, config: {}, portModel: {}", config, portModel);

        final LcConfigXSetMessage msg = new LcConfigXSetMessage(config, portModel);

        final Object continueLock = new Object();

        final MessageListener msgListener = new DefaultMessageListener() {
            @Override
            public void lcConfigX(byte[] address, LcConfigX lcConfigX) {
                LOGGER
                    .info("Received the LcConfigX response, address: {}, lcConfigX: {}", ByteUtils.bytesToHex(address),
                        lcConfigX);

                if (NodeUtils.isAddressEqual(address, getAddr())) {
                    synchronized (continueLock) {
                        continueLock.notifyAll();
                    }
                }
                else {
                    LOGGER.debug("Received lcConfigX for another node.");
                }
            }

            @Override
            public void lcNa(byte[] address, BidibPort bidibPort, Integer errorCode) {
                LOGGER
                    .info("Received the LcNa response, address: {}, port: {}", ByteUtils.bytesToHex(address),
                        bidibPort);

                if (NodeUtils.isAddressEqual(address, getAddr())) {
                    synchronized (continueLock) {
                        continueLock.notifyAll();
                    }
                }
                else {
                    LOGGER.debug("Received lcNa for another node.");
                }
            }

        };
        try {
            getMessageReceiver().addMessageListener(msgListener);

            sendNoWait(msg);

            LOGGER.info("Sent the new port parameters and wait for response before continue.");
            synchronized (continueLock) {
                continueLock.wait(1000);
            }
            LOGGER.info("Wait for response before continue has finished.");
        }
        catch (InterruptedException e) {
            LOGGER.warn("Wait for response before continue was interrupted.");
        }
        finally {
            getMessageReceiver().removeMessageListener(msgListener);
        }

    }

    /**
     * Get the configuration of the specified port.
     * 
     * @param outputType
     *            the port type
     * @param outputNumbers
     *            the list of output numbers
     * @return the configuration of the specified ports.
     * @throws ProtocolException
     */
    public List<LcConfig> getConfigBulk(PortModelEnum portModel, LcOutputType outputType, int... outputNumbers)
        throws ProtocolException {
        List<LcConfig> result = null;

        List<BidibCommand> messages = new LinkedList<>();
        for (int outputNumber : outputNumbers) {
            messages.add(requestFactory.createLcConfigGet(portModel, outputType, outputNumber));
        }

        List<BidibMessage> responses = sendBulk(BULK_WINDOW_SIZE, messages, true, ProcessSendQueue.enabled);
        if (CollectionUtils.hasElements(responses)) {
            result = new LinkedList<>();
            for (BidibMessage response : responses) {
                if (response instanceof LcConfigResponse) {
                    result.add(((LcConfigResponse) response).getLcConfig());
                }
            }
        }
        return result;
    }

    /**
     * Get the configuration of the specified ports.
     * 
     * @param outputType
     *            the port type
     * @param outputNumbers
     *            the list of output numbers
     * @return the configuration of the specified ports.
     * @throws ProtocolException
     */
    public List<LcConfigX> getConfigXBulk(
        PortModelEnum portModel, LcOutputType outputType, int windowSize, int... outputNumbers)
        throws ProtocolException {
        List<LcConfigX> result = null;

        LOGGER
            .info("Get configX with bulk read for outputType: {}, windowSize: {}, outputNumbers: {}", outputType,
                windowSize, outputNumbers);

        List<BidibCommand> messages = new LinkedList<>();
        for (int outputNumber : outputNumbers) {
            messages.add(requestFactory.createLcConfigXGet(portModel, outputType, outputNumber));
        }

        // send bulk messages and wait for bulk answers
        List<BidibMessage> responses = sendBulk(windowSize, messages, true, ProcessSendQueue.enabled);
        if (CollectionUtils.hasElements(responses)) {
            result = new LinkedList<>();
            for (BidibMessage response : responses) {
                if (response instanceof LcConfigXResponse) {
                    result.add(((LcConfigXResponse) response).getLcConfigX());
                }
            }
        }
        return result;
    }

    /**
     * Get the configuration of all ports from the node.
     * 
     * @return the configuration of the specified ports.
     * @throws ProtocolException
     */
    public List<LcConfigX> getAllConfigX(
        PortModelEnum portModel, LcOutputType outputType, Integer rangeFrom, Integer rangeTo) throws ProtocolException {
        List<LcConfigX> result = null;

        // TODO add support for outputType and range
        int totalPorts = rangeTo - rangeFrom;

        int expectedCountResponses = totalPorts;

        List<BidibCommand> messages = new LinkedList<>();

        BidibCommand message = null;
        if (outputType == null) {
            LOGGER.info("Get all configX from the node: {}, totalPorts: {}", this, totalPorts);

            message = requestFactory.createLcConfigXGetAll();
        }
        else {
            LOGGER
                .info("Get all configX from the node: {}, outputType: {}, rangeFrom: {}, rangeTo: {}", this, outputType,
                    rangeFrom, rangeTo);

            message = requestFactory.createLcConfigXGetAll(portModel, outputType, rangeFrom, rangeTo);
        }
        LcConfigXGetAllMessage lcConfigXGetAllMessage = (LcConfigXGetAllMessage) message;
        lcConfigXGetAllMessage.setExpectedCountResponses(expectedCountResponses);
        messages.add(message);

        // send bulk messages and wait for bulk answers
        List<BidibMessage> responses = sendBulk(1/* windowSize */, messages, true, ProcessSendQueue.enabled);
        if (CollectionUtils.hasElements(responses)) {
            result = new LinkedList<>();
            for (BidibMessage response : responses) {
                if (response instanceof LcConfigXResponse) {
                    result.add(((LcConfigXResponse) response).getLcConfigX());
                }
            }
        }

        LOGGER.info("Return received LcConfigX values: {}", result);
        return result;
    }

    /**
     * @return the cached uniqueId
     */
    public Long getCachedUniqueId() {
        return uniqueId;
    }

    /**
     * @param uniqueId
     *            the uniqueId to set
     */
    public void setUniqueId(Long uniqueId) {
        LOGGER.info("Set the uniqueId on the node: {}, previous stored uniqueId: {}", uniqueId, this.uniqueId);
        this.uniqueId = uniqueId;
    }

    /**
     * @return the secureAckEnabled
     */
    public boolean isSecureAckEnabled() {
        return secureAckEnabled;
    }

    /**
     * @param secureAckEnabled
     *            the secureAckEnabled to set
     */
    public void setSecureAckEnabled(boolean secureAckEnabled) {
        LOGGER.info("Set secureAckEnabled on node with uniqueId: {}, secureAckEnabled: {}", uniqueId, secureAckEnabled);
        this.secureAckEnabled = secureAckEnabled;
    }
}
