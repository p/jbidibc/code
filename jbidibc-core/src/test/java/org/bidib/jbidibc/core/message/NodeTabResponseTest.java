package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NodeTabResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeTabResponseTest.class);

    @Test
    public void createNodeTabResponseFromArray() throws ProtocolException {

        byte[] message = { 0x0C, 0x00, 0x12, (byte) 0x89, 0x02, 0x01, 0x00, 0x00, 0x0D, 0x70, 0x00, 0x0D, 0x00 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_NODETAB);

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        LOGGER.info("nodeTabResponse: {}", nodeTabResponse);

        Assert.assertEquals(nodeTabResponse.getNodeTabVersion(), 2);

        byte[] parentAddress = new byte[] { 0 };
        Node node = nodeTabResponse.getNode(parentAddress);

        Assert.assertNotNull(node);
        LOGGER.info("New node: {}", node);

        Assert.assertEquals(node.getUniqueId(), 0x0d70000d00L);
    }

    @Test
    public void createNodeTabResponseFromArray2() throws ProtocolException {

        byte[] message = { 0x0C, 0x00, 0x26, (byte) 0x89, 0x02, 0x01, 0x00, 0x00, 0x0D, (byte) 0xE9, 0x12, 0x34, 0x56 };

        BidibMessage bidibMessage = new BidibResponseFactory().create(message);
        Assert.assertNotNull(bidibMessage);
        Assert.assertEquals(ByteUtils.getInt(bidibMessage.getType()), BidibLibrary.MSG_NODETAB);

        NodeTabResponse nodeTabResponse = (NodeTabResponse) bidibMessage;
        LOGGER.info("nodeTabResponse: {}", nodeTabResponse);

        Assert.assertEquals(nodeTabResponse.getNodeTabVersion(), 2);

        byte[] parentAddress = new byte[] { 0 };
        Node node = nodeTabResponse.getNode(parentAddress);

        Assert.assertNotNull(node);
        LOGGER.info("New node: {}", node);

        Assert.assertEquals(node.getUniqueId(), 0x0dE9123456L);
    }

}
