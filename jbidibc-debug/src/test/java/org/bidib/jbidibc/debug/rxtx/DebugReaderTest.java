package org.bidib.jbidibc.debug.rxtx;

import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.debug.DebugMessageProcessor;
import org.bidib.jbidibc.debug.DebugMessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DebugReaderTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DebugReaderTest.class);

    @Test(enabled = false)
    public void openDebugConnection() throws PortNotFoundException, PortNotOpenedException {

        LOGGER.info("Open the debug connection.");

        final AtomicBoolean openPortPassed = new AtomicBoolean();

        ConnectionListener connectionListener = new ConnectionListener() {

            @Override
            public void opened(String port) {
                LOGGER.info("Port is opened: {}", port);

                openPortPassed.set(true);
            }

            @Override
            public void closed(String port) {
                LOGGER.info("Port is closed: {}", port);
            }

            @Override
            public void status(String messageKey) {
                // no implementation
            }
        };

        Context context = null;

        DebugMessageProcessor messageProcessor = new DebugMessageReceiver();

        DebugReader debugReader = new DebugReader(messageProcessor);
        debugReader.open("COM5", 115200, connectionListener, context);

        LOGGER.info("The port was opened: {}", openPortPassed);

        Assert.assertTrue(openPortPassed.get());

        try {
            Thread.sleep(5000);
        }
        catch (InterruptedException e) {
            LOGGER.warn("Wait was interrupted.", e);
        }

        debugReader.close();
    }

}
