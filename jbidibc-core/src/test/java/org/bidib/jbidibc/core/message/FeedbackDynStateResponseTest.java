package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FeedbackDynStateResponseTest {

    @Test
    public void getDynValue() throws ProtocolException {

        byte mNum = 2;
        byte addrLow = 3;
        byte addrHigh = 0;
        final FeedbackDynStateResponse response =
            new FeedbackDynStateResponse(new byte[] { 1 }, 1, FeedbackDynStateResponse.TYPE, mNum, addrLow, addrHigh,
                (byte) 6, (byte) 0x12, (byte) 0x34);

        final AddressData addressData = new AddressData(3, AddressTypeEnum.LOCOMOTIVE_FORWARD);
        Assert.assertEquals(response.getDetectorNumber(), 2);
        Assert.assertEquals(response.getAddress(), addressData);
        Assert.assertEquals(response.getDynNumber(), 6);
        Assert.assertEquals(response.getDynValue(), 0x3412);
        Assert.assertNull(response.getTimestamp());
    }

    @Test
    public void getDynValueWithTimestamp() throws ProtocolException {

        byte mNum = 2;
        byte addrLow = 3;
        byte addrHigh = 0;
        final FeedbackDynStateResponse response =
            new FeedbackDynStateResponse(new byte[] { 1 }, 1, FeedbackDynStateResponse.TYPE, mNum, addrLow, addrHigh,
                (byte) 6, (byte) 0x12, (byte) 0x34, (byte) 0x12, (byte) 0x34);

        final AddressData addressData = new AddressData(3, AddressTypeEnum.LOCOMOTIVE_FORWARD);
        Assert.assertEquals(response.getDetectorNumber(), 2);
        Assert.assertEquals(response.getAddress(), addressData);
        Assert.assertEquals(response.getDynNumber(), 6);
        Assert.assertEquals(response.getDynValue(), 0x3412);
        Assert.assertEquals(response.getTimestamp(), Integer.valueOf(0x3412));
    }
}
