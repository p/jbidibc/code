package org.bidib.jbidibc.core.port;

import org.bidib.jbidibc.core.utils.ByteUtils;

public class RgbPortConfigValue implements PortConfigValue<Integer> {
    private final Integer value;

    public RgbPortConfigValue(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RgbPortConfigValue)) {
            return false;
        }
        RgbPortConfigValue other = (RgbPortConfigValue) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        }
        else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Int32PortConfigValue:");
        sb.append(ByteUtils.rgbToHex(value));
        return sb.toString();
    }
}
