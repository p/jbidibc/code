package org.bidib.jbidibc.core;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.exception.PortNotReadyForSendException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.message.RequestFactory;
import org.bidib.jbidibc.core.node.AccessoryNode;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.BoosterNode;
import org.bidib.jbidibc.core.node.CommandStationNode;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The classes extending {@code AbstractBidib} class are instantiated from {@link BidibFactory}.
 *
 */
public abstract class AbstractBidib implements BidibInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBidib.class);

    private int responseTimeout = BidibInterface.DEFAULT_TIMEOUT;

    private int firmwarePacketTimeout = BidibInterface.DEFAULT_TIMEOUT;

    private BidibMessageProcessor messageReceiver;

    private NodeRegistry nodeRegistry;

    private RequestFactory requestFactory;

    private ConnectionListener connectionListener;

    private Set<TransferListener> transferListeners = new HashSet<>();

    private BlockingQueue<ByteArrayOutputStream> sendQueue = new LinkedBlockingQueue<>();

    private Thread sendQueueWorker;

    private AtomicBoolean running = new AtomicBoolean();

    private AtomicLong sendQueueWorkerThreadId = new AtomicLong();

    private BlockingQueue<ByteArrayOutputStream> receiveQueue = new LinkedBlockingQueue<>();

    private BlockingQueue<ByteArrayOutputStream> freeBufferQueue = new LinkedBlockingQueue<>();

    private Thread receiveQueueWorker;

    private AtomicBoolean receiverRunning = new AtomicBoolean();

    private AtomicLong receiveQueueWorkerThreadId = new AtomicLong();

    protected AbstractBidib() {
    }

    /**
     * Initialize the instance. This must only be called from this class
     */
    protected void initialize() {
        LOGGER.info("Initialize AbstractBidib, create a NodeRegistry.");
        nodeRegistry = new NodeRegistry();
        LOGGER.info("Created nodeRegistry: {}", nodeRegistry);
        nodeRegistry.setBidib(this);

        // create the request factory
        requestFactory = new RequestFactory();
        nodeRegistry.setRequestFactory(requestFactory);

        requestFactory.initialize();

        // warmup
        for (int i = 0; i < 100; i++) {
            ByteArrayOutputStream item = new ByteArrayOutputStream(64);

            freeBufferQueue.add(item);
        }

        // create the message receiver
        messageReceiver = createMessageReceiver(nodeRegistry);
    }

    protected void cleanupAfterClose(final BidibMessageProcessor bidibMessageProcessor) {
        transferListeners.clear();
    }

    /**
     * Create the message receiver that processes the messages that are received from the <b>interface</b>.
     * 
     * @param nodeRegistry
     *            the node registry
     * @return the message receiver
     */
    protected abstract BidibMessageProcessor createMessageReceiver(NodeRegistry nodeRegistry);

    /**
     * Register the node and message listeners.
     * 
     * @param nodeListeners
     *            the node listeners
     * @param messageListeners
     *            the message listeners
     * @param transferListeners
     *            the transfer listeners
     */
    public void registerListeners(
        Set<NodeListener> nodeListeners, Set<MessageListener> messageListeners,
        Set<TransferListener> transferListeners) {

        if (CollectionUtils.isNotEmpty(nodeListeners)) {
            for (NodeListener nodeListener : nodeListeners) {
                messageReceiver.addNodeListener(nodeListener);
            }
        }

        if (CollectionUtils.isNotEmpty(messageListeners)) {
            for (MessageListener messageListener : messageListeners) {
                messageReceiver.addMessageListener(messageListener);
            }
        }

        if (CollectionUtils.isNotEmpty(transferListeners)) {
            LOGGER.info("Add the transfer listeners to the root node.");

            this.transferListeners.addAll(transferListeners);
        }
        else {
            LOGGER.warn("No transfer listeners available!");
        }
    }

    @Override
    public BidibMessageProcessor getMessageReceiver() {
        return messageReceiver;
    }

    /**
     * @return the connectionListener
     */
    public ConnectionListener getConnectionListener() {
        return connectionListener;
    }

    /**
     * @param connectionListener
     *            the connectionListener to set
     */
    public void setConnectionListener(ConnectionListener connectionListener) {
        LOGGER.info("Set the connection listener: {}", connectionListener);
        this.connectionListener = connectionListener;
    }

    protected NodeRegistry getNodeRegistry() {
        return nodeRegistry;
    }

    @Override
    public AccessoryNode getAccessoryNode(Node node) {
        return nodeRegistry.getAccessoryNode(node.getAddr());
    }

    @Override
    public BoosterNode getBoosterNode(Node node) {
        return nodeRegistry.getBoosterNode(node);
    }

    @Override
    public CommandStationNode getCommandStationNode(Node node) {
        return nodeRegistry.getCommandStationNode(node);
    }

    @Override
    public BidibNode getNode(Node node) {
        return nodeRegistry.getNode(node);
    }

    @Override
    public boolean isValidCoreNode(Node node) {
        return (nodeRegistry.findNode(node.getAddr()) != null);
    }

    @Override
    public BidibNode findNode(byte[] nodeAddress) {
        return nodeRegistry.findNode(nodeAddress);
    }

    @Override
    public RootNode getRootNode() {
        return nodeRegistry.getRootNode();
    }

    @Override
    public void releaseRootNode() {
        LOGGER.info("Release the root node.");

        if (getNodeRegistry() != null) {
            // remove all stored nodes from the node factory
            getNodeRegistry().reset(false);
        }
    }

    @Override
    public void setIgnoreWaitTimeout(boolean ignoreWaitTimeout) {
        if (nodeRegistry != null) {
            LOGGER.info("Set ignoreWaitTimeout flag: {}", ignoreWaitTimeout);
            nodeRegistry.setIgnoreWaitTimeout(ignoreWaitTimeout);
        }
        else {
            LOGGER.warn("The node factory is not available, set the ignoreWaitTimeout is discarded.");
        }
    }

    @Override
    public int getResponseTimeout() {
        return responseTimeout;
    }

    @Override
    public void setResponseTimeout(int responseTimeout) {
        LOGGER.info("Set the response timeout: {}", responseTimeout);
        this.responseTimeout = responseTimeout;
    }

    @Override
    public void setFirmwarePacketTimeout(int firmwarePacketTimeout) {
        LOGGER.info("Set the firmware packet timeout: {}", firmwarePacketTimeout);
        this.firmwarePacketTimeout = firmwarePacketTimeout;
    }

    @Override
    public int getFirmwarePacketTimeout() {
        return firmwarePacketTimeout;
    }

    /**
     * Send the bytes to the send queue.
     * 
     * @param data
     *            the data to send
     */
    @Override
    public void send(final byte[] data) {

        try {
            ByteArrayOutputStream buffer = freeBufferQueue.take();
            buffer.write(data, 0, data.length);
            boolean added = sendQueue.offer(buffer);

            if (!added) {
                LOGGER.error("The message was not added to the send queue: {}", ByteUtils.bytesToHex(data));
            }
        }
        catch (InterruptedException ex) {
            LOGGER.warn("Get message from sendQueue failed.", ex);
        }
    }

    protected void startReceiverAndQueues(final BidibMessageProcessor messageReceiver, final Context context) {

        startSendQueueWorker();
        startReceiveQueueWorker();

        if (messageReceiver != null && context != null) {
            Boolean ignoreWrongMessageNumber =
                context.get("ignoreWrongReceiveMessageNumber", Boolean.class, Boolean.FALSE);
            messageReceiver.setIgnoreWrongMessageNumber(ignoreWrongMessageNumber);
        }
        else {
            LOGGER.warn("No message receiver available.");
        }
    }

    protected void stopReceiverAndQueues(final BidibMessageProcessor serialMessageReceiver) {

        // stop the send queue worker
        stopSendQueueWorker();
        stopReceiveQueueWorker();
    }

    private void startSendQueueWorker() {
        running.set(true);

        LOGGER.info("Start the sendQueueWorker. Current sendQueueWorker: {}", sendQueueWorker);
        sendQueueWorker = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    processSendQueue();
                }
                catch (Exception ex) {
                    LOGGER.warn("The processing of the send queue was terminated with an exception!", ex);

                    running.set(false);
                }

                LOGGER.info("Process send queue has finished.");
            }
        }, "sendQueueWorker");

        try {
            sendQueueWorkerThreadId.set(sendQueueWorker.getId());
            sendQueueWorker.start();
        }
        catch (Exception ex) {
            LOGGER.error("Start the sendQueueWorker failed.", ex);
        }
    }

    private void stopSendQueueWorker() {
        LOGGER.info("Stop the send queue worker.");
        running.set(false);

        if (sendQueueWorker != null) {
            try {
                sendQueueWorker.interrupt();

                sendQueueWorker.join(1000);

                LOGGER.info("sendQueueWorker has finished.");
            }
            catch (Exception ex) {
                LOGGER.warn("Interrupt sendQueueWorker failed.", ex);
            }
        }
        else {
            LOGGER.info("No sendQueueWorker to stop assigned.");
        }
        sendQueueWorker = null;
    }

    /**
     * Send the data to the connected interface.
     * 
     * @param data
     *            the data to send
     */
    protected abstract void sendData(ByteArrayOutputStream data);

    private void fireSendStarted() {
        for (TransferListener l : transferListeners) {
            l.sendStarted();
        }
    }

    private void fireSendStopped() {
        for (TransferListener l : transferListeners) {
            l.sendStopped();
        }
    }

    private void fireReceiveStarted() {
        for (TransferListener l : transferListeners) {
            l.receiveStarted();
        }
    }

    private void fireReceiveStopped() {
        for (TransferListener l : transferListeners) {
            l.receiveStopped();
        }
    }

    protected void fireCtsChanged(boolean cts) {
        LOGGER.info("Signal the changed CTS value: {}", cts);
        for (TransferListener l : transferListeners) {
            l.ctsChanged(cts);
        }
    }

    /**
     * Process the send queue. This method takes a message from the sendQueue and sends the data to the connected
     * interface.
     */
    private void processSendQueue() {
        // byte[] data = null;
        ByteArrayOutputStream data = null;
        LOGGER.info("The sendQueueWorker is ready for processing.");
        while (running.get()) {

            try {
                // get the message to process
                data = sendQueue.take();

                fireSendStarted();

                // send the data to the interface
                sendData(data);
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Get message from sendQueue failed because thread was interrupted.");
            }
            catch (PortNotReadyForSendException ex) {
                LOGGER.warn("The message was not send because the port is not ready for sending, e.g. CTS low.", ex);

                // TODO if send message failed e.g. because the port was closed we have to notify the user

            }
            catch (Exception ex) {

                LOGGER.warn("Get message from sendQueue or send data failed.", ex);
            }
            finally {
                fireSendStopped();

                if (data != null) {
                    data.reset();
                    freeBufferQueue.add(data);
                }
            }

            // release the bytes
            data = null;
        }

        LOGGER.info("The sendQueueWorker has finished processing.");
        sendQueueWorkerThreadId.set(0);
    }

    /**
     * @param data
     *            the received data
     * @param len
     *            the len of data
     */
    protected void receive(final byte[] data, int len) {

        try {
            ByteArrayOutputStream buffer = freeBufferQueue.take();
            buffer.write(data, 0, len);
            boolean added = receiveQueue.offer(buffer);
            if (!added) {
                LOGGER
                    .error("The message was not added to the receive queue: {}",
                        ByteUtils.bytesToHex(buffer.toByteArray()));
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Add buffer to receive queue failed.", ex);
        }
    }

    private void startReceiveQueueWorker() {
        receiverRunning.set(true);

        LOGGER.info("Start the receiveQueueWorker. Current receiveQueueWorker: {}", receiveQueueWorker);
        receiveQueueWorker = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    processReceiveQueue();
                }
                catch (Exception ex) {
                    LOGGER.warn("The processing of the receive queue was terminated with an exception!", ex);
                }

                LOGGER.info("Process receive queue has finished.");
            }
        }, "receiveQueueWorker");

        try {
            // raise priority of receiver thread above normal
            receiveQueueWorker.setPriority(Thread.NORM_PRIORITY + 1);

            receiveQueueWorkerThreadId.set(receiveQueueWorker.getId());
            receiveQueueWorker.start();
        }
        catch (Exception ex) {
            LOGGER.error("Start the receiveQueueWorker failed.", ex);
        }
    }

    private void stopReceiveQueueWorker() {
        LOGGER.info("Stop the receive queue worker.");
        receiverRunning.set(false);

        if (receiveQueueWorker != null) {
            try {
                receiveQueueWorker.interrupt();

                receiveQueueWorker.join(1000);

                LOGGER.info("receiveQueueWorker has finished.");
            }
            catch (Exception ex) {
                LOGGER.warn("Interrupt receiveQueueWorker failed.", ex);
            }
        }
        else {
            LOGGER.info("No receiveQueueWorker to stop assigned.");
        }
        receiveQueueWorker = null;
    }

    /**
     * Process the receive queue. This method takes a message from the receive queue and forwards it to the message
     * receiver.
     */
    private void processReceiveQueue() {
        // byte[] bytes = null;
        LOGGER.info("The receiveQueueWorker is ready for processing.");

        final BidibMessageProcessor messageReceiver = getMessageReceiver();

        while (receiverRunning.get()) {
            ByteArrayOutputStream bytes = null;
            try {
                // get the message to process
                bytes = receiveQueue.take();

                if (bytes != null) {
                    // process
                    try {
                        fireReceiveStarted();

                        // send the data to the message receiver
                        messageReceiver.receive(bytes);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Process received bytes failed.", ex);
                    }
                    finally {
                        fireReceiveStopped();
                    }

                }
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Get message from receiveQueue failed because thread was interrupted.");
            }
            catch (Exception ex) {
                LOGGER.warn("Get message from receiveQueue failed.", ex);
            }
            finally {
                if (bytes != null) {
                    bytes.reset();
                    freeBufferQueue.add(bytes);
                }
            }
            bytes = null;
        }

        LOGGER.info("The receiveQueueWorker has finished processing.");
        receiveQueueWorkerThreadId.set(0);
    }
}
