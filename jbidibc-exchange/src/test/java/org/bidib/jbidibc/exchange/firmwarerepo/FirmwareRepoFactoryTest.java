package org.bidib.jbidibc.exchange.firmwarerepo;

import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.bidib.jbidibc.exchange.bidib.FirmwareFactoryTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

/**
 * @deprecated Use FirmwareRepoFactory from the wizard project instead
 *
 */
@Deprecated
public class FirmwareRepoFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareRepoFactoryTest.class);

    @Test
    public void loadFirmwareRepoMinimum() throws JAXBException, SAXException, XMLStreamException {
        LOGGER.info("Prepare the JAXBContext to load the firmware repo file.");

        URL url = FirmwareFactoryTest.class.getResource("/xml-test/firmware-repo-minimum.xml");
        FirmwareRepoType firmwareRepo = FirmwareRepoFactory.getFirmwareRepo(url.getPath());
        Assert.assertNotNull(firmwareRepo);
        Assert.assertNotNull(firmwareRepo.getFirmware());

        List<FirmwareDefinitionType> firmwareDefinition = firmwareRepo.getFirmware();

        Assert.assertEquals(firmwareDefinition.size(), 2);
    }

    @Test
    public void loadFirmwareRepoBasic() throws JAXBException, SAXException, XMLStreamException {
        LOGGER.info("Prepare the JAXBContext to load the firmware repo file.");

        URL url = FirmwareFactoryTest.class.getResource("/xml-test/firmware-repo-basic.xml");
        FirmwareRepoType firmwareRepo = FirmwareRepoFactory.getFirmwareRepo(url.getPath());
        Assert.assertNotNull(firmwareRepo);
        Assert.assertNotNull(firmwareRepo.getFirmwareRepo());

        Assert.assertEquals(firmwareRepo.getFirmwareRepo().size(), 1);

        List<FirmwareDefinitionType> firmwareDefinition = firmwareRepo.getFirmwareRepo().get(0).getFirmware();

        Assert.assertEquals(firmwareDefinition.size(), 2);
    }
}
