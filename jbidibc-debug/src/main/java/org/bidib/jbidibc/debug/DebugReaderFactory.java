package org.bidib.jbidibc.debug;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DebugReaderFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebugReaderFactory.class);

    public enum SerialImpl {
        RXTX, SCM, SPSW_NET;
    }

    public static DebugInterface getDebugReader(SerialImpl impl, final DebugMessageProcessor messageReceiver) {
        LOGGER.info("Create debug reader, serial impl: {}", impl);

        DebugInterface debugInterface = null;
        switch (impl) {
            case SCM:
                debugInterface = new org.bidib.jbidibc.debug.scm.DebugReader(messageReceiver);
                break;
            case SPSW_NET:
                debugInterface = new org.bidib.jbidibc.debug.spsw.NetDebugReader(messageReceiver);
                break;
            default:
                debugInterface = new org.bidib.jbidibc.debug.rxtx.DebugReader(messageReceiver);
                break;
        }
        debugInterface.initialize();

        return debugInterface;
    }
}
