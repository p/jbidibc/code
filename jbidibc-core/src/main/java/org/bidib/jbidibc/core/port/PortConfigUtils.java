package org.bidib.jbidibc.core.port;

import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortConfigUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortConfigUtils.class);

    public static boolean isSupportsSwitchPort(int portTypeMask) {
        boolean isSupportsSwitchPort =
            (portTypeMask & (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCH)) == (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCH);
        return isSupportsSwitchPort;
    }

    public static boolean isSupportsSwitchPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return isSupportsSwitchPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsInputPort(int portTypeMask) {

        boolean isSupportsInputPort =
            (portTypeMask & (1 << BidibLibrary.BIDIB_PORTTYPE_INPUT)) == (1 << BidibLibrary.BIDIB_PORTTYPE_INPUT);
        return isSupportsInputPort;
    }

    public static boolean isSupportsInputPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {

            return isSupportsInputPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsServoPort(int portTypeMask) {
        boolean isSupportsServoPort =
            (portTypeMask & (1 << BidibLibrary.BIDIB_PORTTYPE_SERVO)) == (1 << BidibLibrary.BIDIB_PORTTYPE_SERVO);
        return isSupportsServoPort;
    }

    public static boolean isSupportsServoPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {

            return isSupportsServoPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsLightPort(int portTypeMask) {
        boolean isSupportsLightPort =
            (portTypeMask & (1 << BidibLibrary.BIDIB_PORTTYPE_LIGHT)) == (1 << BidibLibrary.BIDIB_PORTTYPE_LIGHT);
        return isSupportsLightPort;
    }

    public static boolean isSupportsLightPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {

            return isSupportsLightPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsAnalogPort(int portTypeMask) {
        boolean isSupportsAnalogPort =
            (portTypeMask
                & (1 << BidibLibrary.BIDIB_PORTTYPE_ANALOGOUT)) == (1 << BidibLibrary.BIDIB_PORTTYPE_ANALOGOUT);
        return isSupportsAnalogPort;
    }

    public static boolean isSupportsAnalogPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return isSupportsAnalogPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsBacklightPort(int portTypeMask) {
        boolean isSupportsBacklightPort =
            (portTypeMask
                & (1 << BidibLibrary.BIDIB_PORTTYPE_BACKLIGHT)) == (1 << BidibLibrary.BIDIB_PORTTYPE_BACKLIGHT);
        return isSupportsBacklightPort;
    }

    public static boolean isSupportsBacklightPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return isSupportsBacklightPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsSoundPort(int portTypeMask) {
        boolean isSupportsSoundPort =
            (portTypeMask & (1 << BidibLibrary.BIDIB_PORTTYPE_SOUND)) == (1 << BidibLibrary.BIDIB_PORTTYPE_SOUND);
        return isSupportsSoundPort;
    }

    public static boolean isSupportsSoundPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return isSupportsSoundPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsMotorPort(int portTypeMask) {
        boolean isSupportsMotorPort =
            (portTypeMask & (1 << BidibLibrary.BIDIB_PORTTYPE_MOTOR)) == (1 << BidibLibrary.BIDIB_PORTTYPE_MOTOR);
        return isSupportsMotorPort;
    }

    public static boolean isSupportsMotorPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return isSupportsMotorPort(reconfig.getPortMap());
        }
        return false;
    }

    public static boolean isSupportsSwitchPairPort(int portTypeMask) {
        boolean isSupportsSwitchPairPort =
            (portTypeMask
                & (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCHPAIR)) == (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCHPAIR);
        return isSupportsSwitchPairPort;
    }

    public static boolean isSupportsSwitchPairPort(final Map<Byte, PortConfigValue<?>> portConfig) {
        ReconfigPortConfigValue reconfig = getPortConfig(portConfig, BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {

            return isSupportsSwitchPairPort(reconfig.getPortMap());
        }
        return false;
    }

    private static <T> T getPortConfig(final Map<Byte, PortConfigValue<?>> portConfig, Byte key) {
        try {
            PortConfigValue<?> portConfigValue = portConfig.get(key);
            if (portConfigValue != null) {
                return (T) portConfig.get(key);
            }
        }
        catch (ClassCastException ex) {
            LOGGER.warn("Cast value of key: {} to target type failed.", key, ex);
        }
        return null;
    }

}
