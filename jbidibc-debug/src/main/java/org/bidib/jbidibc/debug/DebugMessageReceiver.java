package org.bidib.jbidibc.debug;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gnu.io.SerialPort;

public class DebugMessageReceiver implements DebugMessageProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DebugMessageReceiver.class);

    // private static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("DEBUG_RAW");

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    protected AtomicBoolean running = new AtomicBoolean();

    private byte[] inputBuffer = new byte[2048];

    private final Set<DebugMessageListener> messageListeners = Collections
        .synchronizedSet(new LinkedHashSet<DebugMessageListener>());

    public DebugMessageReceiver() {

        // enable the running flag
        running.set(true);
    }

    public void addMessageListener(DebugMessageListener listener) {
        synchronized (messageListeners) {
            if (!messageListeners.contains(listener)) {

                LOGGER.info("Add new message listener: {}", listener);
                messageListeners.add(listener);
            }
            else {
                LOGGER.warn("Message listener is already registered: {}", listener);
            }
        }
    }

    public void removeMessageListener(DebugMessageListener listener) {
        synchronized (messageListeners) {
            if (messageListeners.contains(listener)) {

                LOGGER.info("Remove message listener: {}", listener);
                messageListeners.remove(listener);
            }
            else {
                LOGGER.warn("Message listener was not registered: {}", listener);
            }
        }
    }

    @Override
    public void processMessages(ByteArrayOutputStream output) {
        String message = output.toString();
        LOGGER.debug("processMessages, received message: {}", message);

        output.reset();

        synchronized (messageListeners) {
            for (DebugMessageListener l : messageListeners) {
                l.debugMessage(message);
            }
        }
    }

    @Override
    public void disable() {
        running.set(false);
    }

    @Override
    public void enable() {
        running.set(true);
    }

    private static final Logger MSG_RAW_LOGGER = LoggerFactory.getLogger("DEBUG_RAW");
    
    /**
     * Receive messages from the configured port
     */
    @Override
    public void receive(final SerialPort port) {
        LOGGER.debug("Start receiving messages.");

        synchronized (this) {
            LOGGER.debug("Starting message receiver.");
            try {
                InputStream input = null;

                if (port != null) {
                    input = port.getInputStream();
                }
                if (input != null) {

                    // read the values from in the port
                    int len = input.read(inputBuffer);
                    if (len > 0) {
                        output.write(inputBuffer, 0, len);
                        
                        if (MSG_RAW_LOGGER.isInfoEnabled()) {
        		            MSG_RAW_LOGGER.info("<<<< len: {}, data: {}", output.size(), ByteUtils.bytesToHex(output.toByteArray()));
        		        }

                        processMessages(output);

                        LOGGER.debug("Leaving receive loop, RUNNING: {}", running);

                        if (output != null && output.size() > 0) {

                            LOGGER.warn("Data in output: {}", output.toString());
                        }
                    }
                }
                else {
                    LOGGER.error("No input available.");
                }

            }
            catch (Exception e) {
                LOGGER.warn("Exception detected in message receiver!", e);
                throw new RuntimeException(e);
            }
        }
    }

    public static String bytesToStringUTFCustom(byte[] bytes) {
        char[] buffer = new char[bytes.length >> 1];
        for (int i = 0; i < buffer.length; i++) {
            int bpos = i << 1;
            char c = (char) (((bytes[bpos] & 0x00FF) << 8) + (bytes[bpos + 1] & 0x00FF));
            buffer[i] = c;
        }
        return new String(buffer);
    }

}
