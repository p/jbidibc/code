/**
 * 
 */
package org.bidib.jbidibc.core.utils;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.StringTokenizer;

import org.bidib.jbidibc.core.BidibLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains utility functions for byte conversations.
 */
public final class ByteUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ByteUtils.class);

    public final static byte MAGIC = ByteUtils.getLowByte(BidibLibrary.BIDIB_PKT_MAGIC);

    private ByteUtils() {
    }

    public static byte[] toDWORD(long value) {
        byte[] dword = new byte[4];

        for (int index = 0; index < 4; index++) {
            byte val = (byte) ((value >> (8 * index)) & 0xFF);
            dword[index] = val;
        }
        return dword;
    }

    public static long getDWORD(byte[] value) {
        long dword = 0;
        for (int index = 3; index > -1; index--) {
            dword = (dword << 8) | (value[index] & 0xFF);
        }
        return dword;
    }

    public static long getDWORD(byte[] data, int offset) {
        long dword = 0;
        for (int index = 3 + offset; index > (offset - 1); index--) {
            dword = (dword << 8) | (data[index] & 0xFF);
        }
        return dword;
    }

    public static byte[] toWORD(int value) {
        byte[] word = new byte[2];

        for (int index = 0; index < 2; index++) {
            byte val = (byte) ((value >> (8 * index)) & 0xFF);
            word[index] = val;
        }
        return word;
    }

    public static int getWORD(byte[] value) {
        int word = 0;
        for (int index = 1; index > -1; index--) {
            word = (word << 8) | (value[index] & 0xFF);
        }
        return word;
    }

    public static int getWORD(byte[] data, int offset) {
        int word = 0;
        for (int index = 1 + offset; index > (offset - 1); index--) {
            word = (word << 8) | (data[index] & 0xFF);
        }
        return word;
    }

    public static int getWORD(int value) {
        return (value & 0xFFFF);
    }

    public static int getWord(byte lowByte, byte highByte) {
        return ((highByte & 0x3F) << 8) + (lowByte & 0xFF);
    }

    /**
     * Get the (unsigned) int value from the low and high byte value.
     * 
     * @param lowByte
     *            the low byte
     * @param highByte
     *            the high byte
     * @return the combined int value
     */
    public static int getInt(byte lowByte, byte highByte) {
        return ((highByte & 0xFF) << 8) + (lowByte & 0xFF);
    }

    /**
     * Get the (unsigned) integer value from the low and high byte value.
     * 
     * @param lowByte
     *            the low byte
     * @param highByte
     *            the high byte
     * @return the combined int value or null if both values are null
     */
    public static Integer getInteger(Byte lowByte, Byte highByte) {
        if (lowByte != null && highByte != null) {
            return ((highByte & 0xFF) << 8) + (lowByte & 0xFF);
        }
        return null;
    }

    /**
     * Get the (unsigned) int value from the byte value.
     * 
     * @param byteValue
     *            the byte value
     * @return the int value
     */
    public static int getInt(byte byteValue) {
        return (byteValue & 0xFF);
    }

    /**
     * Get the (unsigned) int value from the byte value.
     * 
     * @param byteValue
     *            the byte value
     * @return the int value
     */
    public static int getInt(byte byteValue, int mask) {
        return (byteValue & mask);
    }

    public static Integer getRGB(Integer value) {
        if (value == null) {
            return null;
        }
        return (value & 0xFFFFFF);
    }

    public static int getRGB(byte[] value) {
        int dword = 0;
        for (int index = 0; index < 3; index++) {
            dword = (dword << 8) | (value[index] & 0xFF);
        }
        return dword;
    }

    public static byte[] toRGB(int value) {
        byte[] dword = new byte[3];

        int targetIndex = 0;
        for (int index = 0; index < 3; index++) {
            byte val = (byte) ((value >> (8 * (2 - index))) & 0xFF);
            dword[targetIndex] = val;
            targetIndex++;
        }
        return dword;
    }

    public static int getReconfig(byte[] value) {
        int dword = 0;
        for (int index = 3; index > 0; index--) {
            dword = (dword << 8) | (value[index - 1] & 0xFF);
        }
        return dword;
    }

    public static byte[] toReconfig(int value) {
        byte[] dword = new byte[3];

        int targetIndex = 2;
        for (int index = 3; index > 0; index--) {
            byte val = (byte) ((value >> (8 * (index - 1))) & 0xFF);
            dword[targetIndex] = val;
            targetIndex--;
        }
        return dword;
    }

    /**
     * Get the extended CV number (3 bytes) from the provided data.
     * 
     * @param data
     *            the data array
     * @param offset
     *            the offset in the byte array
     * @return the CVX number
     */
    public static int getCvXNumber(byte[] data, int offset) {
        int dword = 0;
        for (int index = (3 + offset); index > offset; index--) {
            dword = (dword << 8) | (data[index - 1] & 0xFF);
        }
        return dword;
    }

    /**
     * Get the extended CV value (up to 4 bytes) from the provided data.
     * 
     * @param data
     *            the data array
     * @param offset
     *            the offset in the byte array
     * @param count
     *            the number of bytes to read
     * @return the CVX value
     */
    public static int getCvXValue(byte[] data, int offset, int count) {

        if (count < 1 || count > 4) {
            throw new IllegalArgumentException("Allowed count values: 1..4");
        }
        // TODO use the count
        int dword = 0;
        for (int index = (count + offset); index > offset; index--) {
            dword = (dword << 8) | (data[index - 1] & 0xFF);
        }
        return dword;
    }

    /**
     * Concat a byte array.
     * 
     * @param array1
     *            the first array
     * @param array2
     *            the second array
     * @return the concatenated array
     */
    public static byte[] concat(byte[] array1, byte[] array2) {
        byte[] result = new byte[array1.length + array2.length];

        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }

    /**
     * Append a single byte value to a byte array.
     * 
     * @param array1
     *            the first array
     * @param value
     *            the single byte value to add
     * @return the concatenated array
     */
    public static byte[] append(byte[] array1, byte value) {
        byte[] result = new byte[array1.length + 1];

        System.arraycopy(array1, 0, result, 0, array1.length);
        result[array1.length] = value;
        return result;
    }

    /**
     * Append a single byte value to a byte array.
     * 
     * @param array1
     *            the first array
     * @param value
     *            the int16 value to add
     * @return the concatenated array
     */
    public static byte[] append(byte[] array1, int value) {
        byte[] result = new byte[array1.length + 2];

        System.arraycopy(array1, 0, result, 0, array1.length);

        result[array1.length] = ByteUtils.getLowByte(value);
        result[array1.length + 1] = ByteUtils.getHighByte(value);
        return result;
    }

    public static byte[] toArray(byte... element) {
        return element;
    }

    public static byte[] subArray(byte[] data, int offset) {
        byte[] result = new byte[data.length - offset];
        System.arraycopy(data, offset, result, 0, result.length);
        return result;
    }

    /**
     * Get the subarray of the provided data.
     * 
     * @param data
     *            the array
     * @param offset
     *            the offset to start
     * @param length
     *            the number of bytes to return
     * @return the subarray
     */
    public static byte[] subArray(byte[] data, int offset, int length) {
        byte[] result = new byte[length];
        System.arraycopy(data, offset, result, 0, result.length);
        return result;
    }

    public static byte[] convertLongToUniqueId(long uniqueId) {
        byte[] value = BigInteger.valueOf(uniqueId).toByteArray();

        byte[] uniqueIdArray = new byte[7];
        int len = (value.length > 7 ? 7 : value.length);

        System.arraycopy(value, value.length - len, uniqueIdArray, 7 - len, len);
        return uniqueIdArray;
    }

    public static byte[] getVidPidFromUniqueId(long uniqueId) {
        byte[] value = BigInteger.valueOf(uniqueId).toByteArray();

        byte[] uniqueIdArray = new byte[7];
        int len = (value.length > 7 ? 7 : value.length);

        System.arraycopy(value, value.length - len, uniqueIdArray, 7 - len, len);
        return uniqueIdArray;
    }

    public static int getClassIdFromUniqueId(long uniqueId) {

        byte[] uniqueIdArray = convertLongToUniqueId(uniqueId);
        return ByteUtils.getInt(uniqueIdArray[0]);
    }

    public static long convertUniqueIdToLong(byte[] uniqueId) {
        long result = 0;

        for (int i = 0; i < uniqueId.length; i++) {
            result = (result << 8) + (uniqueId[i] & 0xFF);
        }
        return result;
    }

    public static String convertUniqueIdToString(byte[] bytes) {
        if (bytes.length == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(bytes.length * 3);
        for (byte b : bytes) {
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            r.append('.');
        }
        return r.substring(0, r.length() - 1);
    }

    public static String toString(byte[] bytes) {
        StringBuilder result = new StringBuilder("[");

        if (bytes != null) {
            for (byte b : bytes) {
                if (result.length() > 1) {
                    result.append(", ");
                }
                result.append(b & 0xFF);
            }
        }
        result.append("]");
        return result.toString();
    }

    /**
     * Convert a single byte value to a string.
     * 
     * @param bValue
     *            the byte value
     * @return the string representation
     */
    public static String toString(byte bValue) {
        return Integer.toString(bValue & 0xFF);
    }

    /**
     * Compare two byte arrays.
     * 
     * @param a1
     *            the first byte array
     * @param a2
     *            the second byte array
     * @return true if both arrays are equal
     * @deprecated Use Arrays.equals() instead
     */
    public static boolean arrayEquals(byte[] a1, byte[] a2) {
        if (a1.length != a2.length) {
            return false;
        }
        for (int index = 0; index < a1.length; index++) {
            if (a1[index] != a2[index]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the low byte of an int value.
     * 
     * @param value
     *            the value
     * @return the low byte
     */
    public static byte getLowByte(int value) {
        byte lowByte = getLowByte(value, 0xFF);
        return lowByte;
    }

    /**
     * Returns the low byte of an int value.
     * 
     * @param value
     *            the value
     * @param mask
     *            the mask
     * @return the low byte
     */
    public static byte getLowByte(int value, int mask) {
        byte lowByte = (byte) (value & mask);
        return lowByte;
    }

    /**
     * Returns the low byte of an int value.
     * 
     * @param value
     *            the value
     * @return the low byte
     */
    public static Byte getLowByte(Integer value) {
        if (value != null) {
            byte lowByte = (byte) (value.intValue() & 0xFF);
            return lowByte;
        }
        return null;
    }

    /**
     * Returns the high byte of an int value.
     * 
     * @param value
     *            the value
     * @return the high byte
     */
    public static byte getHighByte(int value) {
        byte penultimateByte = (byte) ((value >> 8) & 0xFF);
        return penultimateByte;
    }

    public static int toInt(int highByte, int lowByte) {

        int ret = (highByte & 0xFF) << 8 | (lowByte & 0xFF);
        return ret;
    }

    /**
     * Returns the high word low byte of an int value.
     * 
     * @param value
     *            the value
     * @return the low byte
     */
    public static byte getHighWordLowByte(int value) {
        byte penultimateByte = (byte) ((value >> 16) & 0xFF);
        return penultimateByte;
    }

    /**
     * Returns the high word high byte of an int value.
     * 
     * @param value
     *            the value
     * @return the high byte
     */
    public static byte getHighWordHighByte(int value) {
        byte penultimateByte = (byte) ((value >> 24) & 0xFF);
        return penultimateByte;
    }

    /**
     * Returns the high byte of an int value.
     * 
     * @param value
     *            the value
     * @return the high byte
     */
    public static Byte getHighByte(Integer value) {
        if (value != null) {
            byte penultimateByte = (byte) ((value >> 8) & 0xFF);
            return penultimateByte;
        }
        return null;
    }

    /**
     * Returns the low byte value of an int value as int.
     * 
     * @param value
     *            the value
     * @return the low byte value as int
     */
    public static int getIntLowByteValue(int value) {
        return (value & 0xFF);
    }

    /**
     * Returns the high byte value of an int value as int.
     * 
     * @param value
     *            the value
     * @return the low byte value as int
     */
    public static int getIntHighByteValue(int value) {
        return (value & 0xFF00) >> 8;
    }

    /**
     * Converts the serial number to an int value.
     * 
     * @param serialData
     *            the serial number
     * @param offset
     *            the offset to read the data
     * @return the serial number
     */
    public static int convertSerial(byte[] serialData, int offset) {
        int result = 0;

        if (serialData != null) {
            for (int index = 0; index < 4; index++) {
                result += (serialData[index + offset] & 0xFF) << (index * 8);
            }
        }
        return result;
    }

    final protected static char[] HEXARRAY = "0123456789ABCDEF".toCharArray();

    /**
     * Convert a byte array to a hex string with spaces delimited. Original source: DatatypeConverterImpl.printHexBinary
     * 
     * @param bytes
     *            the byte array
     * @return the formatted hex string
     */
    public static String bytesToHex(byte[] bytes) {
        if (bytes.length == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(bytes.length * 3);
        for (byte b : bytes) {
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            r.append(' ');
        }
        return r.substring(0, r.length() - 1);
    }

    /**
     * Convert a byte array to a hex string with spaces delimited. Original source: DatatypeConverterImpl.printHexBinary
     * 
     * @param bytes
     *            the byte array
     * @return the formatted hex string
     */
    public static String bytesToHex(final ByteArrayOutputStream bytes) {
        if (bytes.size() == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(bytes.size() * 3);
        for (byte b : bytes.toByteArray()) {
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            r.append(' ');
        }
        return r.substring(0, r.length() - 1);
    }

    /**
     * Convert the byte values from an int array to a hex string with spaces delimited. Original source:
     * DatatypeConverterImpl.printHexBinary
     * 
     * @param bytes
     *            the int array
     * @return the formatted hex string
     */
    public static String bytesToHex(int[] bytes) {
        if (bytes.length == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(bytes.length * 3);
        for (int i : bytes) {
            byte b = ByteUtils.getLowByte(i);
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            r.append(' ');
        }
        return r.substring(0, r.length() - 1);
    }

    /**
     * Convert a byte array to a hex string with spaces delimited. Original source: DatatypeConverterImpl.printHexBinary
     * 
     * @param bytes
     *            the byte array
     * @param length
     *            the number of bytes to append
     * @return the formatted hex string
     */
    public static String bytesToHex(byte[] bytes, int length) {
        if (bytes.length == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(length * 3);
        for (int index = 0; index < length; index++) {
            byte b = bytes[index];
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            r.append(' ');
        }
        return r.substring(0, r.length() - 1);
    }

    /**
     * Convert the byte values from an int array to a hex string with spaces delimited. Original source:
     * DatatypeConverterImpl.printHexBinary
     * 
     * @param bytes
     *            the int array
     * @param length
     *            the number of bytes to append
     * @return the formatted hex string
     */
    public static String bytesToHex(int[] bytes, int length) {
        if (bytes.length == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(length * 3);
        for (int index = 0; index < length; index++) {
            byte b = ByteUtils.getLowByte(bytes[index]);
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            r.append(' ');
        }
        return r.substring(0, r.length() - 1);
    }

    public static String int16ToHex(int byteValue) {
        char[] hexChars = new char[4];
        hexChars[0] = HEXARRAY[(byteValue & 0xF000) >>> 12];
        hexChars[1] = HEXARRAY[(byteValue & 0x0F00) >>> 8];
        hexChars[2] = HEXARRAY[(byteValue & 0xF0) >>> 4];
        hexChars[3] = HEXARRAY[byteValue & 0x0F];
        return new String(hexChars);
    }

    public static String int32ToHex(long byteValue) {
        char[] hexChars = new char[8];
        hexChars[0] = HEXARRAY[((int) byteValue & 0xF0000000) >>> 28];
        hexChars[1] = HEXARRAY[((int) byteValue & 0x0F000000) >>> 24];
        hexChars[2] = HEXARRAY[((int) byteValue & 0xF00000) >>> 20];
        hexChars[3] = HEXARRAY[((int) byteValue & 0x0F0000) >>> 16];
        hexChars[4] = HEXARRAY[((int) byteValue & 0xF000) >>> 12];
        hexChars[5] = HEXARRAY[((int) byteValue & 0x0F00) >>> 8];
        hexChars[6] = HEXARRAY[((int) byteValue & 0xF0) >>> 4];
        hexChars[7] = HEXARRAY[(int) byteValue & 0x0F];
        return new String(hexChars);
    }

    public static String int32ToHex(int byteValue) {
        char[] hexChars = new char[8];
        hexChars[0] = HEXARRAY[((int) byteValue & 0xF0000000) >>> 28];
        hexChars[1] = HEXARRAY[((int) byteValue & 0x0F000000) >>> 24];
        hexChars[2] = HEXARRAY[((int) byteValue & 0xF00000) >>> 20];
        hexChars[3] = HEXARRAY[((int) byteValue & 0x0F0000) >>> 16];
        hexChars[4] = HEXARRAY[((int) byteValue & 0xF000) >>> 12];
        hexChars[5] = HEXARRAY[((int) byteValue & 0x0F00) >>> 8];
        hexChars[6] = HEXARRAY[((int) byteValue & 0xF0) >>> 4];
        hexChars[7] = HEXARRAY[(int) byteValue & 0x0F];
        return new String(hexChars);
    }

    public static String rgbToHex(int byteValue) {
        char[] hexChars = new char[6];
        hexChars[0] = HEXARRAY[(byteValue & 0xF00000) >>> 20];
        hexChars[1] = HEXARRAY[(byteValue & 0x0F0000) >>> 16];
        hexChars[2] = HEXARRAY[(byteValue & 0xF000) >>> 12];
        hexChars[3] = HEXARRAY[(byteValue & 0x0F00) >>> 8];
        hexChars[4] = HEXARRAY[(byteValue & 0xF0) >>> 4];
        hexChars[5] = HEXARRAY[byteValue & 0x0F];
        return new String(hexChars);
    }

    public static String rgbToHex(Integer byteValue) {
        if (byteValue != null) {

            char[] hexChars = new char[6];
            hexChars[0] = HEXARRAY[(byteValue & 0xF00000) >>> 20];
            hexChars[1] = HEXARRAY[(byteValue & 0x0F0000) >>> 16];
            hexChars[2] = HEXARRAY[(byteValue & 0xF000) >>> 12];
            hexChars[3] = HEXARRAY[(byteValue & 0x0F00) >>> 8];
            hexChars[4] = HEXARRAY[(byteValue & 0xF0) >>> 4];
            hexChars[5] = HEXARRAY[byteValue & 0x0F];
            return new String(hexChars);
        }
        return null;
    }

    public static String magicToHex(int magic) {
        char[] hexChars = new char[4];
        hexChars[0] = HEXARRAY[(magic & 0xF000) >>> 12];
        hexChars[1] = HEXARRAY[(magic & 0x0F00) >>> 8];
        hexChars[2] = HEXARRAY[(magic & 0xF0) >>> 4];
        hexChars[3] = HEXARRAY[magic & 0x0F];
        return new String(hexChars);
    }

    public static String byteToHex(byte byteValue) {
        char[] hexChars = new char[2];
        int v = byteValue & 0xFF;
        hexChars[0] = HEXARRAY[v >>> 4];
        hexChars[1] = HEXARRAY[v & 0x0F];
        return new String(hexChars);
    }

    public static String byteToHex(int byteValue) {
        char[] hexChars = new char[2];
        int v = byteValue & 0xFF;
        hexChars[0] = HEXARRAY[v >>> 4];
        hexChars[1] = HEXARRAY[v & 0x0F];
        return new String(hexChars);
    }

    public static String longToHex(long value) {
        char[] hexChars = new char[8];

        hexChars[0] = HEXARRAY[(int) ((value & 0xF0000000L) >>> 28)];
        hexChars[1] = HEXARRAY[(int) ((value & 0x0F000000L) >>> 24)];
        hexChars[2] = HEXARRAY[(int) ((value & 0x00F00000L) >>> 20)];
        hexChars[3] = HEXARRAY[(int) ((value & 0x000F0000L) >>> 16)];

        hexChars[4] = HEXARRAY[(int) ((value & 0x0000F000) >>> 12)];
        hexChars[5] = HEXARRAY[(int) ((value & 0x00000F00) >>> 8)];
        hexChars[6] = HEXARRAY[(int) ((value & 0x000000F0) >>> 4)];
        hexChars[7] = HEXARRAY[(int) value & 0x0000000F];
        return new String(hexChars);
    }

    public static String intToHex(int value) {
        char[] hexChars = new char[4];

        hexChars[0] = HEXARRAY[(int) ((value & 0x0000F000) >>> 12)];
        hexChars[1] = HEXARRAY[(int) ((value & 0x00000F00) >>> 8)];
        hexChars[2] = HEXARRAY[(int) ((value & 0x000000F0) >>> 4)];
        hexChars[3] = HEXARRAY[(int) value & 0x0000000F];
        return new String(hexChars);
    }

    /**
     * Convert a hex string to a byte array. Original source: DatatypeConverterImpl.printHexBinary
     * 
     * @param hexString
     *            the hex string
     * @return the byte array
     */
    public static byte[] parseHexBinary(String hexString) {
        final int len = hexString.length();

        // "111" is not a valid hex encoding.
        if (len % 2 != 0) {
            throw new IllegalArgumentException(
                "The hexBinary needs to be even-length. Current len: " + len + " => " + hexString);
        }

        byte[] out = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            int h = hexToBin(hexString.charAt(i));
            int l = hexToBin(hexString.charAt(i + 1));
            if (h == -1 || l == -1) {
                throw new IllegalArgumentException("contains illegal character for hexBinary: " + hexString);
            }

            out[i / 2] = (byte) (h * 16 + l);
        }

        return out;
    }

    /**
     * Convert a hex string to a byte array. The hex values must be separated by a space.
     * 
     * @param hexString
     *            the hex string
     * @return the byte array
     */
    public static byte[] parseHexBinarySpaceTerminated(String hexString) {
        final int len = hexString.length();

        LOGGER.debug("Len of hexString: {}", len);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        StringTokenizer tokenizer = new StringTokenizer(hexString, " ");
        while (tokenizer.hasMoreTokens()) {

            String tok = tokenizer.nextToken();

            int h = hexToBin(tok.charAt(0));
            int l = hexToBin(tok.charAt(1));
            if (h == -1 || l == -1) {
                throw new IllegalArgumentException("contains illegal character for hexBinary: " + hexString);
            }

            os.write((byte) (h * 16 + l));
        }

        return os.toByteArray();
    }

    private static int hexToBin(char ch) {
        if ('0' <= ch && ch <= '9') {
            return ch - '0';
        }
        if ('A' <= ch && ch <= 'F') {
            return ch - 'A' + 10;
        }
        if ('a' <= ch && ch <= 'f') {
            return ch - 'a' + 10;
        }
        return -1;
    }

    public static byte setBit(byte byteValue, boolean bit, int bitpos) {
        int newValue;
        int intValue = (byteValue & 0xFF);
        if (bit) {
            newValue = (intValue | (1 << bitpos));
        }
        else {
            newValue = (intValue & ~(1 << bitpos));
        }

        return ByteUtils.getLowByte(newValue);
    }

    public static byte setBit(int byteValue, boolean bit, int bitpos) {
        int newValue;
        int intValue = (byteValue & 0xFF);
        if (bit) {
            newValue = (intValue | (1 << bitpos));
        }
        else {
            newValue = (intValue & ~(1 << bitpos));
        }

        return ByteUtils.getLowByte(newValue);
    }

    public static int getBit(byte byteValue, int bitpos) {

        int intValue = (byteValue & 0xFF);
        return ((intValue >>> bitpos) & 1);
    }

    public static int getBit(int intValue, int bitpos) {

        return ((intValue >>> bitpos) & 1);
    }

    public static boolean isBitSetEqual(byte byteValue, int bit, int bitpos) {

        int intValue = (byteValue & 0xFF);
        if (((intValue >>> bitpos) & 1) == bit) {
            return true;
        }
        return false;
    }

    public static boolean isBitSetEqual(int intValue, int bit, int bitpos) {

        if (((intValue >>> bitpos) & 1) == bit) {
            return true;
        }
        return false;
    }

    public static byte[] bstr(String value) {
        byte[] valueBytes = value.getBytes(Charset.forName("ISO-8859-1"));

        if (valueBytes.length > 255) {
            LOGGER.warn("Maximum string length of 255 exceeded.");
            throw new IllegalArgumentException("Maximum string length of 255 exceeded.");
        }

        byte[] result = new byte[valueBytes.length + 1 /* terminating zero */];

        result[0] = ByteUtils.getLowByte(valueBytes.length);
        System.arraycopy(valueBytes, 0, result, 1, valueBytes.length);
        return result;
    }

    public static byte[] bstr(String name, String value) {
        byte[] nameBytes = name.getBytes();
        byte[] valueBytes = value.getBytes();

        if (nameBytes.length > 255 || valueBytes.length > 255) {
            LOGGER.warn("Maximum string length of 255 exceeded.");
            throw new IllegalArgumentException("Maximum string length of 255 exceeded.");
        }

        byte[] result = new byte[nameBytes.length + valueBytes.length + 2 /* 2x terminating zero */];

        result[0] = ByteUtils.getLowByte(nameBytes.length);
        System.arraycopy(nameBytes, 0, result, 1, nameBytes.length);
        result[nameBytes.length + 1] = ByteUtils.getLowByte(valueBytes.length);
        System.arraycopy(valueBytes, 0, result, nameBytes.length + 2, valueBytes.length);
        return result;
    }

    public static String cstr(byte[] bstr, int offset) {
        int length = bstr[offset];
        byte[] cstr = new byte[length];

        if (length > 0) {
            System.arraycopy(bstr, offset + 1, cstr, 0, length);
            return new String(cstr, Charset.forName("ISO-8859-1"));
        }
        return "";
    }

    /**
     * Convert the byte values from an int array to a hex string with spaces delimited. Original source:
     * DatatypeConverterImpl.printHexBinary
     * 
     * @param bytes
     *            the int array
     * @return the formatted hex string
     */
    public static String didToHex(byte[] bytes) {
        if (bytes.length == 0) {
            return "";
        }
        StringBuilder r = new StringBuilder(bytes.length * 2 + 1);
        int len = bytes.length;
        for (int index = len - 1; index > -1; index--) {
            byte b = bytes[index];
            r.append(HEXARRAY[(b >> 4) & 0xF]);
            r.append(HEXARRAY[(b & 0xF)]);
            if (index == len - 1) {
                r.append(' ');
            }
        }
        return r.substring(0, r.length());
    }

    public static int getMaskedAndShiftedValue(int source, int mask) {

        // shift the mask and the value to right until the mask has a bit set
        while ((mask & 0x01) == 0x00) {
            source >>= 1;
            mask >>= 1;
        }
        return source;
    }
}
