package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class FeedbackFreeResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_BM_FREE;

    FeedbackFreeResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        // len == 1 if no sensor time is delivered
        if (data == null || (data.length < 1 /* && data.length != 3 && data.length != 5 */)) {
            throw new ProtocolException("No valid MSG_BM_FREE received.");
        }
    }

    public FeedbackFreeResponse(byte[] addr, int num, int detectorAddress) throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_FREE, ByteUtils.getLowByte(detectorAddress));
    }

    public String getName() {
        return "MSG_BM_FREE";
    }

    public int getDetectorNumber() {
        return ByteUtils.getInt(getData()[0]);
    }

    public Long getTimestamp() {
        switch (getData().length) {
            case 3:
                int ts = ByteUtils.getWORD(getData(), 1);
                return Long.valueOf(ts);
            case 5:
                return ByteUtils.getDWORD(getData(), 1);
            default:
                break;
        }
        return null;
    }
}
