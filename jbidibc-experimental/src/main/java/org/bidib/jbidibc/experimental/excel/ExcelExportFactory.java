package org.bidib.jbidibc.experimental.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import org.apache.poi.ss.usermodel.ExtendedColor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.bidib.jbidibc.core.schema.bidib2.Aspect;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.Feature;
import org.bidib.jbidibc.core.schema.bidib2.FeedbackPort;
import org.bidib.jbidibc.core.schema.bidib2.FunctionAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.FunctionCriticalSection;
import org.bidib.jbidibc.core.schema.bidib2.FunctionDelay;
import org.bidib.jbidibc.core.schema.bidib2.FunctionFlag;
import org.bidib.jbidibc.core.schema.bidib2.FunctionMacro;
import org.bidib.jbidibc.core.schema.bidib2.FunctionOutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.InputKey;
import org.bidib.jbidibc.core.schema.bidib2.Macro;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameter;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterClockStart;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterRepeat;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterSlowdown;
import org.bidib.jbidibc.core.schema.bidib2.MacroPoint;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointCriticalSection;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointDelay;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointFlag;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointInput;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointMacro;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputLight;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputServo;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSwitchPair;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointServoMoveQuery;
import org.bidib.jbidibc.core.schema.bidib2.Node;
import org.bidib.jbidibc.core.schema.bidib2.OutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.OutputLight;
import org.bidib.jbidibc.core.schema.bidib2.OutputServo;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitchPair;
import org.bidib.jbidibc.core.schema.bidib2.Port;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelExportFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelExportFactory.class);

    private static final int ASPECT_PARAM_UNCHANGED = 255;

    private static final int ASPECT_PARAM_RESTORE = 254;

    private ExtendedColor colorEmptyCell;

    /**
     * Export the documentation for a bidib node.
     * 
     * @param bidib
     *            the node
     * @param fileName
     *            the filename
     */
    public static void exportDocumentation(final BiDiB bidib, String fileName) {
        LOGGER.info("Export BiDiB to excel: {}", fileName);

        try {
            new ExcelExportFactory().createWorkbook(bidib, fileName);
        }
        catch (IOException ex) {
            // propagate the exception
            LOGGER.warn("Create Excel workbook failed.", ex);

            throw new RuntimeException(ex.getMessage());
        }
    }

    private static final class ExportPortsProvider {
        private Node node;

        public ExportPortsProvider(final Node node) {
            this.node = node;
        }

        public List<OutputSwitch> getSwitchPorts() {
            List<OutputSwitch> switchPorts =
                org.bidib.jbidibc.core.schema.BidibFactory
                    .getPortsOfType(node.getPorts().getPort(), LcOutputType.SWITCHPORT);
            return switchPorts;
        }

        public List<OutputSwitchPair> getSwitchPairPorts() {
            List<OutputSwitchPair> switchPairPorts =
                org.bidib.jbidibc.core.schema.BidibFactory
                    .getPortsOfType(node.getPorts().getPort(), LcOutputType.SWITCHPAIRPORT);
            return switchPairPorts;
        }

        public List<OutputServo> getServoPorts() {
            List<OutputServo> servoPorts =
                org.bidib.jbidibc.core.schema.BidibFactory
                    .getPortsOfType(node.getPorts().getPort(), LcOutputType.SERVOPORT);
            return servoPorts;
        }

        public List<InputKey> getInputPorts() {
            List<InputKey> inputPorts =
                org.bidib.jbidibc.core.schema.BidibFactory
                    .getPortsOfType(node.getPorts().getPort(), LcOutputType.INPUTPORT);
            return inputPorts;
        }

        public List<OutputLight> getLightPorts() {
            List<OutputLight> lightPorts =
                org.bidib.jbidibc.core.schema.BidibFactory
                    .getPortsOfType(node.getPorts().getPort(), LcOutputType.LIGHTPORT);
            return lightPorts;
        }

        public List<OutputBacklight> getBacklightPorts() {
            List<OutputBacklight> backlightPorts =
                org.bidib.jbidibc.core.schema.BidibFactory
                    .getPortsOfType(node.getPorts().getPort(), LcOutputType.BACKLIGHTPORT);
            return backlightPorts;
        }

        public List<Macro> getMacros() {
            return node.getMacros().getMacro();
        }
    }

    protected void createWorkbook(final BiDiB bidib, String fileName) throws IOException {

        if (bidib == null) {
            throw new IllegalArgumentException("bidib is not provided!");
        }

        if (bidib.getNodes() == null || CollectionUtils.isEmpty(bidib.getNodes().getNode())) {
            throw new IllegalArgumentException("No nodes provided!");
        }

        // get the schema node
        Node node = bidib.getNodes().getNode().get(0);

        FileOutputStream fileOut = null;
        Workbook wb = null;
        try {
            // create the workbook
            wb = new XSSFWorkbook();

            colorEmptyCell = wb.getCreationHelper().createExtendedColor();
            colorEmptyCell.setARGBHex("FFF0F0F0");

            // You can use org.apache.poi.ss.util.WorkbookUtil#createSafeSheetName(String nameProposal)}
            // for a safe way to create valid names, this utility replaces invalid characters with a space (' ')
            // String safeName = WorkbookUtil.createSafeSheetName("[O'Brien's sales*?]"); // returns " O'Brien's sales "
            // Sheet sheet3 = wb.createSheet(safeName);

            // add summary sheet as first sheet
            createSheetSummary(wb, "Summary", node);

            ExportPortsProvider exportPortsProvider = new ExportPortsProvider(node);
            if (node.getPorts() != null && CollectionUtils.isNotEmpty(node.getPorts().getPort())) {

                // write the ports to the excel workbook

                List<OutputServo> servoPorts = exportPortsProvider.getServoPorts();
                if (CollectionUtils.isNotEmpty(servoPorts)) {
                    LOGGER.info("Create servo ports sheet.");
                    createSheetPorts(wb, node, "Servo Ports", servoPorts,
                        new String[] { "Port #", "Port Name", "Speed", "Lower Limit", "Upper Limit" });
                }

                List<OutputSwitch> switchPorts = exportPortsProvider.getSwitchPorts();
                if (CollectionUtils.isNotEmpty(switchPorts)) {
                    LOGGER.info("Create switch ports sheet.");
                    createSheetPorts(wb, node, "Switch Ports", switchPorts,
                        new String[] { "Port #", "Port Name", "I/O Behaviour", "LoadType", "SwitchOff Time" });
                }

                List<OutputSwitchPair> switchPairPorts = exportPortsProvider.getSwitchPairPorts();
                if (CollectionUtils.isNotEmpty(switchPairPorts)) {
                    LOGGER.info("Create switchPair ports sheet.");
                    createSheetPorts(wb, node, "SwitchPair Ports", switchPairPorts,
                        new String[] { "Port #", "Port Name", "LoadType", "SwitchOff Time" });
                }

                List<InputKey> inputPorts = exportPortsProvider.getInputPorts();
                if (CollectionUtils.isNotEmpty(inputPorts)) {
                    LOGGER.info("Create input ports sheet.");
                    createSheetPorts(wb, node, "Input Ports", inputPorts,
                        new String[] { "Port #", "Port Name", "I/O Behaviour", "SwitchOff Time" });
                }

                List<OutputLight> lightPorts = exportPortsProvider.getLightPorts();
                if (CollectionUtils.isNotEmpty(lightPorts)) {
                    LOGGER.info("Create light ports sheet.");
                    createSheetPorts(wb, node, "Light Ports", lightPorts, new String[] { "Port #", "Port Name",
                        "Brightness Off", "Brightness On", "Fade In", "Fade Out", "RGB" });
                }

                List<OutputBacklight> backlightPorts = exportPortsProvider.getBacklightPorts();
                if (CollectionUtils.isNotEmpty(backlightPorts)) {
                    LOGGER.info("Create backlight ports sheet.");
                    createSheetPorts(wb, node, "Backlight Ports", backlightPorts,
                        new String[] { "Port #", "Port Name", "Fade In", "Fade Out", "Channel" });
                }
            }

            // add sheets for macros overview
            if (node.getMacros() != null && CollectionUtils.isNotEmpty(node.getMacros().getMacro())) {
                createSheetMacrosOverview(wb, node, exportPortsProvider, "Macros", node.getMacros().getMacro());
            }

            // add sheet for accessories
            if (node.getAccessories() != null && CollectionUtils.isNotEmpty(node.getAccessories().getAccessory())) {
                createSheetAccessoriesOverview(wb, node, exportPortsProvider, "Accessories",
                    node.getAccessories().getAccessory());
            }

            if (node.getFeedbackPorts() != null
                && CollectionUtils.isNotEmpty(node.getFeedbackPorts().getFeedbackPort())) {
                List<FeedbackPort> feedbackPorts = node.getFeedbackPorts().getFeedbackPort();
                createSheetFeedbackPorts(wb, node, "Feedback Ports", feedbackPorts,
                    new String[] { "Port #", "Port Name" });
            }

            // add sheets for macros
            if (node.getMacros() != null && CollectionUtils.isNotEmpty(node.getMacros().getMacro())) {
                createSheetMacros(wb, node, exportPortsProvider, node.getMacros().getMacro());
            }

            // add sheet for accessories
            if (node.getAccessories() != null && CollectionUtils.isNotEmpty(node.getAccessories().getAccessory())) {
                createSheetAccessories(wb, node, exportPortsProvider, node.getAccessories().getAccessory());
            }

            LOGGER.info("Write workbook to: {}", fileName);
            fileOut = new FileOutputStream(fileName);
            wb.write(fileOut);
        }
        finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                }
                catch (Exception ex) {
                    LOGGER.warn("Close file output stream failed.", ex);
                }
            }

            if (wb != null) {
                try {
                    wb.close();
                }
                catch (Exception ex) {
                    LOGGER.warn("Close Excel workbook failed.", ex);
                }
            }
        }
    }

    private void createSheetAccessories(
        final Workbook wb, final Node node, final ExportPortsProvider exportPortsProvider,
        final List<Accessory> accessories) {
        LOGGER.info("Create accessories sheet");

        for (Accessory accessory : accessories) {
            String accessoryName = null;
            if (StringUtils.isBlank(accessory.getName())) {
                // accessoryName = "Accessory_" + accessory.getNumber();
                accessoryName = String.format("A%1$02d : %2$s", accessory.getNumber(), "");
            }
            else {
                accessoryName = String.format("A%1$02d : %2$s", accessory.getNumber(), accessory.getName());
            }
            String sheetName = WorkbookUtil.createSafeSheetName(accessoryName);
            createSheetAccessory(wb, node, exportPortsProvider, sheetName, accessory);
        }
    }

    private void createSheetAccessoriesOverview(
        final Workbook wb, final Node node, final ExportPortsProvider exportPortsProvider, String sheetName,
        final List<Accessory> accessories) {

        LOGGER.info("Create accessory overview sheet: {}", sheetName);
        Sheet sheetAccessories = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = sheetAccessories.createRow(rowNumber);
        // Create a cell and put a value in it.
        Cell cell = row.createCell(0);
        cell.setCellValue(sheetName);
        cell.setCellStyle(h1Style);

        rowNumber += 2;

        // add node info
        rowNumber = addNodeInfo(rowNumber, sheetAccessories, node);

        // add the header line
        row = sheetAccessories.createRow(rowNumber++);

        CellStyle thStyle = prepareThCellStyle(wb);

        String[] columnNames = new String[] { "Accessory Number", "Accessory Name" };

        for (short columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
            // Create a cell and put a value in it.
            cell = row.createCell(columnIndex);
            cell.setCellValue(columnNames[columnIndex]);
            cell.setCellStyle(thStyle);
        }

        for (Accessory accessory : accessories) {
            String macroName = null;
            String macroNumber = String.format("A%1$02d", accessory.getNumber());
            if (StringUtils.isBlank(accessory.getName())) {
                macroName = "";
            }
            else {
                macroName = accessory.getName();
            }

            addRow(sheetAccessories, rowNumber++, new String[] { macroNumber, macroName }, h1Style);
        }

        sheetAccessories
            .addMergedRegion(new CellRangeAddress(0, // first row (0-based)
                0, // last row (0-based)
                0, // first column (0-based)
                columnNames.length - 1 // last column (0-based)
            ));

        for (short column = 0; column < columnNames.length; column++) {
            // adjust column width to fit the content
            sheetAccessories.autoSizeColumn(column);
        }
    }

    private void createSheetAccessory(
        final Workbook wb, final Node node, final ExportPortsProvider exportPortsProvider, String sheetName,
        final Accessory accessory) {

        int lastColumn = 2;

        LOGGER.info("Create accessory sheet: {}", sheetName);
        Sheet sheetAccessory = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;

        String accessoryName = null;
        if (StringUtils.isBlank(accessory.getName())) {
            // accessoryName = "Accessory_" + accessory.getNumber();
            accessoryName = String.format("A%1$02d : %2$s", accessory.getNumber(), "");
        }
        else {
            accessoryName = String.format("A%1$02d : %2$s", accessory.getNumber(), accessory.getName());
        }

        LOGGER.info("Prepared accessoryName: {}", accessoryName);

        addRow(sheetAccessory, rowNumber++, new String[] { accessoryName }, h1Style);

        sheetAccessory
            .addMergedRegion(new CellRangeAddress(0, // first row (0-based)
                0, // last row (0-based)
                0, // first column (0-based)
                6 // last column (0-based)
            ));

        rowNumber += 2;

        // add node info
        rowNumber = addNodeInfo(rowNumber, sheetAccessory, node);

        if (accessory.getStartupState() != null) {

            String initialState = null;
            final int startupAspect = accessory.getStartupState();
            switch (startupAspect) {
                case ASPECT_PARAM_UNCHANGED:
                    initialState = "unchanged";
                    break;
                case ASPECT_PARAM_RESTORE:
                    initialState = "restore";
                    break;
                default:
                    if (accessory.getAspects() != null
                        && CollectionUtils.isNotEmpty(accessory.getAspects().getAspect())) {
                        final Aspect referencedAspect =
                            IterableUtils.find(accessory.getAspects().getAspect(), new Predicate<Aspect>() {

                                @Override
                                public boolean evaluate(Aspect object) {
                                    return object.getNumber() == startupAspect;
                                }
                            });
                        if (referencedAspect != null) {
                            initialState = referencedAspect.getName();
                        }
                    }
                    if (initialState == null) {
                        initialState = "Aspect_" + startupAspect;
                    }
                    break;
            }
            addRow(sheetAccessory, rowNumber++, new String[] { "Initial State", initialState });
            rowNumber += 2;
        }
        else {
            LOGGER.info("No startup state available.");
        }

        CellStyle thStyle = prepareThCellStyle(wb);

        addRow(sheetAccessory, rowNumber++, new String[] { "Aspect", "Macro" }, thStyle);

        int startRowNumber = rowNumber;

        // add accessories
        int aspectNumber = 0;
        if (accessory.getAspects() != null && CollectionUtils.isNotEmpty(accessory.getAspects().getAspect())) {
            for (Aspect aspect : accessory.getAspects().getAspect()) {

                String aspectName = aspect.getName();
                if (StringUtils.isBlank(aspectName)) {
                    aspectName = "Aspect_" + aspectNumber;
                }
                final Integer mappedMacroNumber = aspect.getMacroNumber();
                String macroName = null;
                if (mappedMacroNumber != null) {
                    final Macro referencedMacro =
                        IterableUtils.find(exportPortsProvider.getMacros(), new Predicate<Macro>() {

                            @Override
                            public boolean evaluate(Macro macro) {
                                return macro.getNumber() == mappedMacroNumber.intValue();
                            }
                        });
                    if (referencedMacro != null) {
                        LOGGER.info("Found referenced macro: {}", referencedMacro);

                        if (StringUtils.isNotBlank(referencedMacro.getName())) {
                            macroName = referencedMacro.getName();
                        }
                        else {
                            macroName = "Macro_" + mappedMacroNumber;
                        }
                    }
                }
                addRow(sheetAccessory, rowNumber++, new String[] { aspectName, macroName });

                aspectNumber++;
            }
        }

        for (short column = 0; column < lastColumn; column++) {
            // adjust column width to fit the content
            sheetAccessory.autoSizeColumn(column);
        }

        if (rowNumber > startRowNumber) {
            SheetConditionalFormatting sheetCF = sheetAccessory.getSheetConditionalFormatting();
            ConditionalFormattingRule rule1 =
                sheetCF.createConditionalFormattingRule("ISBLANK(A" + startRowNumber + ")");
            PatternFormatting patternFmt = rule1.createPatternFormatting();
            patternFmt.setFillBackgroundColor(colorEmptyCell);

            ConditionalFormattingRule[] cfRules = { rule1 };

            CellRangeAddress[] regions = { CellRangeAddress.valueOf("$A$" + startRowNumber + ":$B$" + rowNumber) };

            sheetCF.addConditionalFormatting(regions, cfRules);
        }
        else {
            LOGGER.info("Current accessory has no aspects: {}", accessory);
        }
    }

    private void createSheetMacros(
        final Workbook wb, final Node node, final ExportPortsProvider exportPortsProvider, final List<Macro> macros) {

        LOGGER.info("Create sheet for every macro.");
        for (Macro macro : macros) {
            String macroName = null;
            if (StringUtils.isBlank(macro.getName())) {
                macroName = String.format("M%1$02d : %2$s", macro.getNumber(), "");
            }
            else {
                macroName = String.format("M%1$02d : %2$s", macro.getNumber(), macro.getName());
            }
            String sheetName = WorkbookUtil.createSafeSheetName(macroName);
            createSheetMacro(wb, node, exportPortsProvider, sheetName, macro);
        }
    }

    private <PT extends MacroParameter> PT getParameter(final List<MacroParameter> parameters, Class<?> type) {

        for (MacroParameter mp : parameters) {
            if (mp.getClass().isAssignableFrom(type)) {
                LOGGER.info("Found matching type: {}", mp);

                return (PT) mp;
            }
        }
        return (PT) null;
    }

    private void createSheetMacrosOverview(
        final Workbook wb, final Node node, final ExportPortsProvider exportPortsProvider, String sheetName,
        final List<Macro> macros) {

        LOGGER.info("Create macros overview sheet: {}", sheetName);
        Sheet sheetMacros = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = sheetMacros.createRow(rowNumber);
        // Create a cell and put a value in it.
        Cell cell = row.createCell(0);
        cell.setCellValue(sheetName);
        cell.setCellStyle(h1Style);

        rowNumber += 2;

        // add node info
        rowNumber = addNodeInfo(rowNumber, sheetMacros, node);

        // add the header line
        row = sheetMacros.createRow(rowNumber++);

        CellStyle thStyle = prepareThCellStyle(wb);

        String[] columnNames = new String[] { "Macro Number", "Macro Name" };

        for (short columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
            // Create a cell and put a value in it.
            cell = row.createCell(columnIndex);
            cell.setCellValue(columnNames[columnIndex]);
            cell.setCellStyle(thStyle);
        }

        for (Macro macro : macros) {
            String macroName = null;
            String macroNumber = String.format("M%1$02d", macro.getNumber());
            if (StringUtils.isBlank(macro.getName())) {
                macroName = "";
            }
            else {
                macroName = macro.getName();
            }

            addRow(sheetMacros, rowNumber++, new String[] { macroNumber, macroName }, h1Style);
        }

        sheetMacros
            .addMergedRegion(new CellRangeAddress(0, // first row (0-based)
                0, // last row (0-based)
                0, // first column (0-based)
                columnNames.length - 1 // last column (0-based)
            ));

        for (short column = 0; column < columnNames.length; column++) {
            // adjust column width to fit the content
            sheetMacros.autoSizeColumn(column);
        }
    }

    private void createSheetMacro(
        final Workbook wb, final Node node, final ExportPortsProvider exportPortsProvider, String sheetName,
        final Macro macro) {

        int lastColumn = 6;

        LOGGER.info("Create macro sheet: {}", sheetName);
        Sheet sheetMacro = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;

        String macroName = null;
        if (StringUtils.isBlank(macro.getName())) {
            macroName = String.format("M%1$02d : %2$s", macro.getNumber(), "");
        }
        else {
            macroName = String.format("M%1$02d : %2$s", macro.getNumber(), macro.getName());
        }

        LOGGER.info("Prepared macroName: {}", macroName);

        addRow(sheetMacro, rowNumber++, new String[] { macroName }, h1Style);

        rowNumber += 2;

        // add node info
        rowNumber = addNodeInfo(rowNumber, sheetMacro, node);

        if (macro.getMacroParameters() != null
            && CollectionUtils.isNotEmpty(macro.getMacroParameters().getMacroParameter())) {

            // add macro start parameters
            List<MacroParameter> parameters = macro.getMacroParameters().getMacroParameter();
            LOGGER.info("Current parameters: {}", parameters);

            int paramStartRow = rowNumber;

            MacroParameterSlowdown slowdown = getParameter(parameters, MacroParameterSlowdown.class);
            MacroParameterRepeat repeat = getParameter(parameters, MacroParameterRepeat.class);
            addRow(sheetMacro, rowNumber++,
                new String[] { "Slowdown:", "", (slowdown != null ? Integer.toString(slowdown.getSpeed()) : null) });
            addRow(sheetMacro, rowNumber++, new String[] { "Repetitions:", "",
                (repeat != null ? Integer.toString(repeat.getRepetitions()) : null) });

            // add start time

            MacroParameterClockStart clockStart = getParameter(parameters, MacroParameterClockStart.class);
            if (clockStart != null && clockStart.isIsEnabled()) {

                // if (StringUtils.isNotBlank(clockStart.getPeriodicalRepetition())) {
                // // periodical repetition is configured
                // }

                String startTime = String.format("%1$02d:%2$02d", clockStart.getHour(), clockStart.getMinute());

                addRow(sheetMacro, rowNumber++, new String[] { "Clock Start:", "", startTime });
                addRow(sheetMacro, rowNumber++,
                    new String[] { "Repetitions:", "", clockStart.getWeekday(), clockStart.getPeriodicalRepetition() });
            }

            sheetMacro
                .addMergedRegion(new CellRangeAddress(paramStartRow, // first row (0-based)
                    paramStartRow, // last row (0-based)
                    0, // first column (0-based)
                    1 // last column (0-based)
                ));

            sheetMacro
                .addMergedRegion(new CellRangeAddress(paramStartRow + 1, // first row (0-based)
                    paramStartRow + 1, // last row (0-based)
                    0, // first column (0-based)
                    1 // last column (0-based)
                ));
            sheetMacro
                .addMergedRegion(new CellRangeAddress(paramStartRow + 2, // first row (0-based)
                    paramStartRow + 2, // last row (0-based)
                    0, // first column (0-based)
                    1 // last column (0-based)
                ));
            sheetMacro
                .addMergedRegion(new CellRangeAddress(paramStartRow + 3, // first row (0-based)
                    paramStartRow + 3, // last row (0-based)
                    0, // first column (0-based)
                    1 // last column (0-based)
                ));
        }

        rowNumber += 2;

        CellStyle thStyle = prepareThCellStyle(wb);

        addRow(sheetMacro, rowNumber++, new String[] { "Step#", "Delay", "Port Type", "Action", "Port", "Value" },
            thStyle);

        int startRowNumber = rowNumber;

        int macroStep = 1;
        if (macro.getMacroPoints() != null && CollectionUtils.isNotEmpty(macro.getMacroPoints().getMacroPoint())) {
            for (MacroPoint point : macro.getMacroPoints().getMacroPoint()) {

                if (point instanceof MacroPointOutputSwitch) {
                    final MacroPointOutputSwitch pointOutputSwitch = (MacroPointOutputSwitch) point;

                    final int portNumber = pointOutputSwitch.getOutputNumber();
                    final OutputSwitch port =
                        NodeUtils.findPortByNumber(exportPortsProvider.getSwitchPorts(), portNumber);

                    addRow(sheetMacro, rowNumber++,
                        new String[] { Integer.toString(macroStep), Integer.toString(pointOutputSwitch.getDelay()),
                            "Switch Port", pointOutputSwitch.getFunction().name(),
                            (port != null ? port.getName() : null), null });
                }
                else if (point instanceof MacroPointOutputSwitchPair) {
                    final MacroPointOutputSwitchPair pointOutputSwitchPair = (MacroPointOutputSwitchPair) point;

                    final int portNumber = pointOutputSwitchPair.getOutputNumber();
                    final OutputSwitch port =
                        NodeUtils.findPortByNumber(exportPortsProvider.getSwitchPorts(), portNumber);

                    FunctionOutputSwitch function = pointOutputSwitchPair.getFunction();
                    String functionName = null;
                    switch (function) {
                        case ON:
                            functionName = "straight";
                            break;
                        case OFF:
                            functionName = "round";
                            break;
                        default:
                            functionName = "unknown";
                            break;
                    }

                    addRow(sheetMacro, rowNumber++,
                        new String[] { Integer.toString(macroStep), Integer.toString(pointOutputSwitchPair.getDelay()),
                            "SwitchPair Port", functionName, (port != null ? port.getName() : null), null });
                }
                else if (point instanceof MacroPointOutputLight) {
                    final MacroPointOutputLight pointOutputLight = (MacroPointOutputLight) point;

                    final int portNumber = pointOutputLight.getOutputNumber();
                    final OutputLight port =
                        NodeUtils.findPortByNumber(exportPortsProvider.getLightPorts(), portNumber);

                    addRow(sheetMacro, rowNumber++,
                        new String[] { Integer.toString(macroStep), Integer.toString(pointOutputLight.getDelay()),
                            "Light Port", pointOutputLight.getFunction().name(), (port != null ? port.getName() : null),
                            null });
                }
                else if (point instanceof MacroPointOutputServo) {
                    final MacroPointOutputServo pointOutputServo = (MacroPointOutputServo) point;

                    final int portNumber = pointOutputServo.getOutputNumber();
                    final OutputServo port =
                        NodeUtils.findPortByNumber(exportPortsProvider.getServoPorts(), portNumber);

                    addRow(sheetMacro, rowNumber++,
                        new String[] { Integer.toString(macroStep), Integer.toString(pointOutputServo.getDelay()),
                            "Servo Port", "START", (port != null ? port.getName() : null),
                            Integer.toString(pointOutputServo.getPosition()) });
                }
                else if (point instanceof MacroPointOutputBacklight) {
                    final MacroPointOutputBacklight pointOutputBacklight = (MacroPointOutputBacklight) point;

                    final int portNumber = pointOutputBacklight.getOutputNumber();
                    OutputBacklight port =
                        NodeUtils.findPortByNumber(exportPortsProvider.getBacklightPorts(), portNumber);

                    addRow(sheetMacro, rowNumber++,
                        new String[] { Integer.toString(macroStep), Integer.toString(pointOutputBacklight.getDelay()),
                            "Backlight Port", "START", (port != null ? port.getName() : null),
                            Integer.toString(pointOutputBacklight.getBrightness()) });
                }
                else if (point instanceof MacroPointInput) {
                    final MacroPointInput pointInput = (MacroPointInput) point;

                    final int portNumber = pointInput.getInputNumber();
                    final InputKey port = NodeUtils.findPortByNumber(exportPortsProvider.getInputPorts(), portNumber);

                    addRow(sheetMacro, rowNumber++, new String[] { Integer.toString(macroStep), null, "Input Port",
                        pointInput.getFunction().name(), (port != null ? port.getName() : null), null });
                }
                else if (point instanceof MacroPointDelay) {
                    final MacroPointDelay pointDelay = (MacroPointDelay) point;

                    final FunctionDelay function = pointDelay.getFunction();
                    addRow(sheetMacro, rowNumber++,
                        new String[] { Integer.toString(macroStep), Integer.toString(pointDelay.getDelay()),
                            FunctionDelay.RANDOM == function ? "Random Delay" : "Delay", null, null, null });
                }
                else if (point instanceof MacroPointAccessoryNotification) {
                    final MacroPointAccessoryNotification pointAccessoryNotification =
                        (MacroPointAccessoryNotification) point;

                    InputKey port = null;
                    if (pointAccessoryNotification.getFunction() != FunctionAccessoryNotification.OKAY) {
                        final int portNumber = pointAccessoryNotification.getInputNumber();
                        port = NodeUtils.findPortByNumber(exportPortsProvider.getInputPorts(), portNumber);
                    }

                    final FunctionAccessoryNotification function = pointAccessoryNotification.getFunction();
                    addRow(sheetMacro, rowNumber++, new String[] { Integer.toString(macroStep), null,
                        "Accessory Notify", function.value(), (port != null ? port.getName() : null), null });
                }
                else if (point instanceof MacroPointCriticalSection) {
                    final MacroPointCriticalSection pointCriticalSection = (MacroPointCriticalSection) point;

                    final FunctionCriticalSection function = pointCriticalSection.getFunction();
                    addRow(sheetMacro, rowNumber++, new String[] { Integer.toString(macroStep), null,
                        "Critical Section", function.value(), null, null });
                }
                else if (point instanceof MacroPointFlag) {
                    final MacroPointFlag pointFlag = (MacroPointFlag) point;

                    final FunctionFlag function = pointFlag.getFunction();
                    addRow(sheetMacro, rowNumber++, new String[] { Integer.toString(macroStep), null, "Flag",
                        function.value(), Integer.toString(pointFlag.getFlagNumber()), null });
                }
                else if (point instanceof MacroPointMacro) {
                    final MacroPointMacro pointMacro = (MacroPointMacro) point;

                    final int macroNumber = pointMacro.getMacroNumber();
                    final Macro referencedMacro =
                        IterableUtils.find(exportPortsProvider.getMacros(), new Predicate<Macro>() {

                            @Override
                            public boolean evaluate(Macro object) {
                                return object.getNumber() == macroNumber;
                            }
                        });
                    final FunctionMacro function = pointMacro.getFunction();

                    // prepare the name of the macro
                    String referencedMacroName = null;

                    if (referencedMacro != null) {
                        referencedMacroName = referencedMacro.getName();
                        if (StringUtils.isBlank(referencedMacroName)) {
                            referencedMacroName = "Macro_" + referencedMacro.getNumber();
                        }
                    }

                    addRow(sheetMacro, rowNumber++, new String[] { Integer.toString(macroStep), null, "Macro",
                        function.value(), referencedMacroName, null });
                }
                else if (point instanceof MacroPointServoMoveQuery) {
                    final MacroPointServoMoveQuery pointServoMoveQuery = (MacroPointServoMoveQuery) point;

                    final int portNumber = pointServoMoveQuery.getOutputNumber();
                    final OutputServo port =
                        NodeUtils.findPortByNumber(exportPortsProvider.getServoPorts(), portNumber);

                    addRow(sheetMacro, rowNumber++, new String[] { Integer.toString(macroStep), null,
                        "Await Servo Move", null, (port != null ? port.getName() : null), null });
                }
                else {
                    LOGGER.warn("Unsupported macro point detected: {}", point);
                }
                macroStep++;
            }
        }

        LOGGER.info("After filling macro data, rowNumber: {}", rowNumber);

        sheetMacro
            .addMergedRegion(new CellRangeAddress(0, // first row (0-based)
                0, // last row (0-based)
                0, // first column (0-based)
                lastColumn - 1 // last column (0-based)
            ));

        for (short column = 0; column < lastColumn; column++) {
            // adjust column width to fit the content
            sheetMacro.autoSizeColumn(column);
        }

        if (rowNumber > 4) {
            SheetConditionalFormatting sheetCF = sheetMacro.getSheetConditionalFormatting();
            ConditionalFormattingRule rule1 =
                sheetCF.createConditionalFormattingRule("ISBLANK(A" + startRowNumber + ")");
            // FontFormatting fontFmt = rule1.createFontFormatting();
            // fontFmt.setFontStyle(true, false);
            // fontFmt.setFontColorIndex(IndexedColors.DARK_RED.index);
            PatternFormatting patternFmt = rule1.createPatternFormatting();
            patternFmt.setFillBackgroundColor(colorEmptyCell);

            ConditionalFormattingRule[] cfRules = { rule1 };

            CellRangeAddress[] regions = { CellRangeAddress.valueOf("$A$" + startRowNumber + ":$F$" + rowNumber) };

            sheetCF.addConditionalFormatting(regions, cfRules);
        }
        else {
            LOGGER.info("Current macro has no macro points: {}", macro);
        }
    }

    private short addNodeInfo(short rowNumber, final Sheet sheet, final Node node) {
        // add the summary lines
        addRow(sheet, rowNumber++, new String[] { "Product Name:", node.getProductName() });
        addRow(sheet, rowNumber++, new String[] { "User Name:", node.getUserName() });

        long uniqueId = node.getUniqueId();

        addRow(sheet, rowNumber++, new String[] { "Unique ID:", NodeUtils.getUniqueIdAsString(uniqueId) });
        addRow(sheet, rowNumber++, new String[] { "Firmware:", node.getFirmwareRelease() });
        addRow(sheet, rowNumber++, new String[] { "Protocol:", node.getProtocol() });

        // add empty line
        rowNumber++;

        return rowNumber;
    }

    private void createSheetSummary(final Workbook wb, String sheetName, final Node node) {

        LOGGER.info("Create summary sheet: {}", sheetName);
        Sheet sheetSummary = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = sheetSummary.createRow(rowNumber);
        // Create a cell and put a value in it.
        Cell cell = row.createCell(0);
        cell.setCellValue(sheetName);
        cell.setCellStyle(h1Style);

        rowNumber += 2;

        // add the summary lines
        rowNumber = addNodeInfo(rowNumber, sheetSummary, node);

        if (node.getFeatures() != null && CollectionUtils.isNotEmpty(node.getFeatures().getFeature())) {
            List<Feature> features = new LinkedList<>(node.getFeatures().getFeature());
            CellStyle thStyle = prepareThCellStyle(wb);

            addRow(sheetSummary, rowNumber++, new String[] { "Features:", null, null }, thStyle);
            addRow(sheetSummary, rowNumber++, new String[] { "Feature count:", Integer.toString(features.size()) });

            addRow(sheetSummary, rowNumber++, new String[] { "ID", "Value", "Feature Name" }, thStyle);

            // add the features
            Collections.sort(features, new Comparator<Feature>() {

                @Override
                public int compare(Feature f1, Feature f2) {
                    return Integer.compare(f1.getFeatureCodeId(), f2.getFeatureCodeId());
                }

            });
            for (Feature feature : features) {
                addRow(sheetSummary, rowNumber++, new String[] { Integer.toString(feature.getFeatureCodeId()),
                    Integer.toString(feature.getValue()),
                    org.bidib.jbidibc.core.Feature.getFeatureName(ByteUtils.getLowByte(feature.getFeatureCodeId())) });
            }
        }
        else {
            addRow(sheetSummary, rowNumber++, new String[] { "Features:", "not available" });
        }

        for (short column = 0; column < 3; column++) {
            // adjust column width to fit the content
            sheetSummary.autoSizeColumn(column);
        }
    }

    private void addRow(final Sheet sheet, int rowNumber, String[] cellValues) {
        addRow(sheet, rowNumber, cellValues, (CellStyle) null);
    }

    private void addRow(final Sheet sheet, int rowNumber, String[] cellValues, CellStyle cellStyle) {
        Row row = sheet.createRow(rowNumber);
        // int column = 0;
        if (cellValues != null) {
            for (int column = 0; column < cellValues.length; column++) {
                String cellValue = cellValues[column];
                Cell cell = row.createCell(column);
                cell.setCellValue(cellValue);
                if (cellStyle != null) {
                    cell.setCellStyle(cellStyle);
                }
            }
        }
    }

    private void createSheetFeedbackPorts(
        final Workbook wb, final Node node, String sheetName, List<FeedbackPort> ports, String[] columnNames) {

        LOGGER.info("Create ports sheet: {}", sheetName);
        Sheet sheetPorts = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = sheetPorts.createRow(rowNumber);
        // Create a cell and put a value in it.
        Cell cell = row.createCell(0);
        cell.setCellValue(sheetName);
        cell.setCellStyle(h1Style);

        rowNumber += 2;

        rowNumber = addNodeInfo(rowNumber, sheetPorts, node);

        // add the header line
        row = sheetPorts.createRow(rowNumber++);

        CellStyle thStyle = prepareThCellStyle(wb);

        for (short columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
            // Create a cell and put a value in it.
            cell = row.createCell(columnIndex);
            cell.setCellValue(columnNames[columnIndex]);
            cell.setCellStyle(thStyle);
        }

        if (CollectionUtils.isNotEmpty(ports)) {
            for (FeedbackPort port : ports) {
                // Create a row and put some cells in it. Rows are 0 based.
                Row rowPort = sheetPorts.createRow(rowNumber++);
                // Create a cell and put a value in it.
                Cell cellPort = rowPort.createCell(0);
                cellPort.setCellValue(port.getNumber());
                cellPort = rowPort.createCell(1);
                cellPort.setCellValue(port.getName());

            }
        }

        sheetPorts
            .addMergedRegion(new CellRangeAddress(0, // first row (0-based)
                0, // last row (0-based)
                0, // first column (0-based)
                columnNames.length - 1 // last column (0-based)
            ));

        for (short column = 0; column < columnNames.length; column++) {
            // adjust column width to fit the content
            sheetPorts.autoSizeColumn(column);
        }
    }

    private <PT extends Port> void createSheetPorts(
        final Workbook wb, final Node node, String sheetName, List<PT> ports, String[] columnNames) {

        LOGGER.info("Create ports sheet: {}", sheetName);
        Sheet sheetPorts = wb.createSheet(sheetName);

        CellStyle h1Style = prepareH1CellStyle(wb);

        short rowNumber = 0;
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = sheetPorts.createRow(rowNumber);
        // Create a cell and put a value in it.
        Cell cell = row.createCell(0);
        cell.setCellValue(sheetName);
        cell.setCellStyle(h1Style);

        rowNumber += 2;

        // add node info
        rowNumber = addNodeInfo(rowNumber, sheetPorts, node);

        // add the header line
        row = sheetPorts.createRow(rowNumber++);

        CellStyle thStyle = prepareThCellStyle(wb);

        for (short columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
            // Create a cell and put a value in it.
            cell = row.createCell(columnIndex);
            cell.setCellValue(columnNames[columnIndex]);
            cell.setCellStyle(thStyle);
        }

        if (CollectionUtils.isNotEmpty(ports)) {
            for (PT port : ports) {

                if (Boolean.TRUE.equals(port.isDisabled())) {
                    LOGGER.info("Skip disabled port: {}", port);
                    continue;
                }

                // Create a row and put some cells in it. Rows are 0 based.
                Row rowPort = sheetPorts.createRow(rowNumber++);
                // Create a cell and put a value in it.
                Cell cellPort = rowPort.createCell(0);
                cellPort.setCellValue(port.getNumber());
                cellPort = rowPort.createCell(1);
                cellPort.setCellValue(port.getName());

                if (port instanceof InputKey) {
                    InputKey inputPort = (InputKey) port;
                    if (inputPort.getIoInputBehaviour() != null) {
                        cellPort = rowPort.createCell(2);
                        cellPort.setCellValue(inputPort.getIoInputBehaviour().value());
                    }

                    if (inputPort.getSwitchOffTime() != null) {
                        cellPort = rowPort.createCell(3);
                        cellPort.setCellValue(inputPort.getSwitchOffTime());
                    }
                }
                else if (port instanceof OutputSwitch) {
                    OutputSwitch switchPort = (OutputSwitch) port;
                    if (switchPort.getIoSwitchBehaviour() != null) {
                        cellPort = rowPort.createCell(2);
                        cellPort.setCellValue(switchPort.getIoSwitchBehaviour().value());
                    }
                    if (switchPort.getLoadType() != null) {
                        cellPort = rowPort.createCell(3);
                        cellPort.setCellValue(switchPort.getLoadType().value());
                    }

                    if (switchPort.getSwitchOffTime() != null) {
                        cellPort = rowPort.createCell(4);
                        cellPort.setCellValue(switchPort.getSwitchOffTime());
                    }
                }
                else if (port instanceof OutputSwitchPair) {
                    OutputSwitchPair switchPairPort = (OutputSwitchPair) port;
                    if (switchPairPort.getLoadType() != null) {
                        cellPort = rowPort.createCell(2);
                        cellPort.setCellValue(switchPairPort.getLoadType().value());
                    }

                    if (switchPairPort.getSwitchOffTime() != null) {
                        cellPort = rowPort.createCell(3);
                        cellPort.setCellValue(switchPairPort.getSwitchOffTime());
                    }
                }
                else if (port instanceof OutputServo) {
                    OutputServo servoPort = (OutputServo) port;
                    cellPort = rowPort.createCell(2);
                    cellPort.setCellValue(servoPort.getMovingTime());

                    cellPort = rowPort.createCell(3);
                    cellPort.setCellValue(servoPort.getLowerLimit());

                    cellPort = rowPort.createCell(4);
                    cellPort.setCellValue(servoPort.getUpperLimit());
                }
                else if (port instanceof OutputLight) {
                    OutputLight lightPort = (OutputLight) port;
                    cellPort = rowPort.createCell(2);
                    cellPort.setCellValue(lightPort.getBrightnessOff());
                    cellPort = rowPort.createCell(3);
                    cellPort.setCellValue(lightPort.getBrightnessOn());
                    cellPort = rowPort.createCell(4);
                    cellPort.setCellValue(lightPort.getDimmingDownSpeed());
                    cellPort = rowPort.createCell(5);
                    cellPort.setCellValue(lightPort.getDimmingUpSpeed());
                    cellPort = rowPort.createCell(6);
                    if (lightPort.getRgbValue() != null) {
                        cellPort.setCellValue(lightPort.getRgbValue());
                    }

                }
                else if (port instanceof OutputBacklight) {
                    OutputBacklight backlightPort = (OutputBacklight) port;
                    cellPort = rowPort.createCell(2);
                    cellPort.setCellValue(backlightPort.getDimmingDownSpeed());
                    cellPort = rowPort.createCell(3);
                    cellPort.setCellValue(backlightPort.getDimmingUpSpeed());
                    cellPort = rowPort.createCell(4);
                    cellPort.setCellValue(backlightPort.getDmxChannel());
                }
            }
        }

        sheetPorts
            .addMergedRegion(new CellRangeAddress(0, // first row (0-based)
                0, // last row (0-based)
                0, // first column (0-based)
                columnNames.length - 1 // last column (0-based)
            ));

        for (short column = 0; column < columnNames.length; column++) {
            // adjust column width to fit the content
            sheetPorts.autoSizeColumn(column);
        }
    }

    private CellStyle prepareH1CellStyle(final Workbook wb) {
        // Create a new font and alter it.
        Font font = wb.createFont();
        font.setFontHeightInPoints((short) 24);
        // font.setFontName("Courier New");
        font.setItalic(true);
        font.setBold(true);
        // font.setStrikeout(true);
        font.setColor(IndexedColors.BLUE_GREY.getIndex());

        // Fonts are set into a style so create a new one to use.
        CellStyle style = wb.createCellStyle();
        style.setFont(font);
        return style;
    }

    private CellStyle prepareThCellStyle(final Workbook wb) {
        // Create a new font and alter it.
        Font font = wb.createFont();
        // font.setFontHeightInPoints((short) 24);
        // font.setItalic(true);
        font.setBold(true);
        // font.setStrikeout(true);
        // font.setColor(Font.COLOR_RED);

        // Fonts are set into a style so create a new one to use.
        CellStyle style = wb.createCellStyle();
        style.setFont(font);
        // style.setFillBackgroundColor((short) 0xC3C3C3);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        return style;
    }
}
