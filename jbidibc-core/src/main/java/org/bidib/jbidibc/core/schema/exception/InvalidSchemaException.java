package org.bidib.jbidibc.core.schema.exception;

public class InvalidSchemaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidSchemaException(String message) {
        super(message);
    }

    public InvalidSchemaException(String message, Throwable cause) {
        super(message, cause);
    }

}
