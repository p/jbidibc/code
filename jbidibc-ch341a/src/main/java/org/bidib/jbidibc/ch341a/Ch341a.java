package org.bidib.jbidibc.ch341a;

import org.bidib.jbidibc.ch341a.Helper.UCHARByReference;

import com.sun.jna.Callback;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.PVOID;
import com.sun.jna.platform.win32.WinDef.UCHAR;
import com.sun.jna.platform.win32.WinDef.ULONG;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

public interface Ch341a extends StdCallLibrary {

    /** The instance. */
    Ch341a INSTANCE = Native.loadLibrary("ch341dll", Ch341a.class, W32APIOptions.DEFAULT_OPTIONS);

    HANDLE CH341OpenDevice(WinDef.ULONG iIndex);

    void CH341CloseDevice(WinDef.ULONG iIndex);

    ULONG CH341GetVersion();

    ULONG CH341GetDrvVersion();

    BOOL CH341ResetDevice(ULONG iIndex);

    BOOL CH341GetDeviceDescr(ULONG iIndex, PVOID oBuffer, IntByReference ioLength);

    BOOL CH341GetConfigDescr(ULONG iIndex, PVOID oBuffer, IntByReference ioLength);

    BOOL CH341GetStatus(ULONG iIndex, IntByReference iStatus);

    BOOL CH341ReadI2C(ULONG iIndex, UCHAR iDevice, UCHAR iAddr, UCHARByReference oByte);

    BOOL CH341WriteI2C(ULONG iIndex, UCHAR iDevice, UCHAR iAddr, UCHAR iByte);

    PVOID CH341GetDeviceName(ULONG iIndex);

    ULONG CH341GetVerIC(ULONG iIndex);

    // define an interface that wraps the callback code
    public interface PCH341_INT_ROUTINE extends Callback {
        void invoke(ULONG iStatus);
    }

    BOOL CH341SetIntRoutine(ULONG iIndex, PCH341_INT_ROUTINE iIntRoutine);

    // define an implementation of the callback interface
    public static class PCH341_INT_ROUTINEImplementation implements PCH341_INT_ROUTINE {
        @Override
        public void invoke(ULONG iStatus) {
            System.out.println("example22: " + iStatus);
        }
    }

    // define an interface that wraps the callback code
    public interface PCH341_NOTIFY_ROUTINE extends Callback {
        void invoke(ULONG iEventStatus);
    }

    BOOL CH341SetDeviceNotify(ULONG iIndex, CHARByReference iDeviceID, PCH341_NOTIFY_ROUTINE iNotifyRoutine);

    // define an implementation of the callback interface
    public static class PCH341_NOTIFY_ROUTINEImplementation implements PCH341_NOTIFY_ROUTINE {
        @Override
        public void invoke(ULONG iEventStatus) {
            System.out.println("event status: " + iEventStatus);
        }
    }

    BOOL CH341SetStream(ULONG iIndex, ULONG iMode);

    BOOL CH341StreamI2C(
        ULONG iIndex, ULONG iWriteLength, PointerByReference iWriteBuffer, ULONG iReadLength,
        PointerByReference oReadBuffer);
}
