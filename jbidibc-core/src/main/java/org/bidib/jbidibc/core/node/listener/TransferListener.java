package org.bidib.jbidibc.core.node.listener;

public interface TransferListener {
    /**
     * Signal that receive message was started
     */
    void receiveStarted();

    /**
     * Signal that receive message was stopped
     */
    void receiveStopped();

    /**
     * Signal that send message was started
     */
    void sendStarted();

    /**
     * Signal that send message was stopped
     */
    void sendStopped();

    /**
     * The CTS status has changed. If CTS is {@code false} the interface cannot receive data.
     * 
     * @param cts
     *            the new CTS status
     */
    void ctsChanged(boolean cts);
}
