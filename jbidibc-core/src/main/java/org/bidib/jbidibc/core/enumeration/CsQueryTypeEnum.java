package org.bidib.jbidibc.core.enumeration;

import org.bidib.jbidibc.core.utils.ByteUtils;

public enum CsQueryTypeEnum {
    // @formatter:off
    LOCO_LIST("locoList", 1), ACCESSORY_LIST("accessoryList", 0x10), EXT_ACCESSORY_LIST("extAccessoryList", 0x11);
    // @formatter:on

    private final byte type;

    private final String key;

    CsQueryTypeEnum(String key, int type) {
        this.key = key;
        this.type = ByteUtils.getLowByte(type);
    }

    public String getKey() {
        return key;
    }

    /**
     * @return the feature number as byte value
     */
    public byte getType() {
        return type;
    }

    /**
     * Create a command station query type.
     * 
     * @param type
     *            numeric value of the command station query type
     * @return CsQueryTypeEnum
     */
    public static CsQueryTypeEnum valueOf(byte type) {
        CsQueryTypeEnum result = null;

        for (CsQueryTypeEnum e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a command station query type");
        }
        return result;
    }

}
