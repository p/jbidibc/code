package org.bidib.jbidibc.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class NetBidibTcpServerSocketHandlerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibTcpServerSocketHandlerTest.class);

    @Test
    public void parsePacketsStream() throws IOException, ProtocolException {
        LOGGER.info("Parse packets from received data.");

        // The message begins with the BIDIB_WIZARD_NET_PREFIX and contains 3 messages
        byte[] receiveData =
            new byte[] { 0x0C, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0xDA, 0x00, 0x0D, 0x68, 0x00, 0x64,
                (byte) 0xEA, /**/ 0x05, 0x00, 0x05, (byte) 0x90, 0x01, 0x01, /**/ 0x05, 0x00, 0x06, (byte) 0x90, 0x02,
                0x01, /**/ 0x05, 0x00, 0x07, (byte) 0x90, 0x03, 0x64, };

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        buffer.write(receiveData);

        NetMessageHandler messageReceiver = Mockito.mock(NetMessageHandler.class);
        InetAddress address = InetAddress.getLoopbackAddress();
        int portNumber = 12345;

        NetBidibTcpServerSocketHandler handler = Mockito.mock(NetBidibTcpServerSocketHandler.class);
        Mockito.doCallRealMethod().when(handler).parsePackets(Mockito.any(), Mockito.any(NetMessageHandler.class),
            Mockito.any(InetAddress.class), Mockito.anyInt());

        handler.parsePackets(buffer, messageReceiver, address, portNumber);

        Mockito.verify(messageReceiver, Mockito.times(4)).receive(Mockito.any());
    }

    @Test
    public void parsePacketsStream2() throws IOException, ProtocolException {
        LOGGER.info("Parse packets from received data.");

        // The message begins with the BIDIB_WIZARD_NET_PREFIX and contains 1 message
        byte[] receiveData =
            new byte[] { 0x0C, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0xDA, 0x00, 0x0D, 0x68, 0x00, 0x64,
                (byte) 0xEA, /**/ 0x05, 0x00, 0x05, (byte) 0x90, 0x01, 0x01 };
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        buffer.write(receiveData);

        NetMessageHandler messageHandler = Mockito.mock(NetMessageHandler.class);
        InetAddress address = InetAddress.getLoopbackAddress();
        int portNumber = 12345;

        NetBidibTcpServerSocketHandler handler = Mockito.mock(NetBidibTcpServerSocketHandler.class);
        Mockito.doCallRealMethod().when(handler).parsePackets(Mockito.any(), Mockito.any(NetMessageHandler.class),
            Mockito.any(InetAddress.class), Mockito.anyInt());

        handler.parsePackets(buffer, messageHandler, address, portNumber);

        Mockito.verify(messageHandler, Mockito.times(2)).receive(Mockito.any());
    }

    @Test
    public void parsePacketsStream3() throws IOException, ProtocolException {
        LOGGER.info("Parse packets from splited received data.");

        // The message begins with the BIDIB_WIZARD_NET_PREFIX and contains 2 messages splited over 2 packets
        byte[] receiveData =
            new byte[] { 0x0C, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0xDA, 0x00, 0x0D, 0x68, 0x00, 0x64,
                (byte) 0xEA, /**/ 0x05, 0x00, 0x05, (byte) 0x90, 0x01, 0x01, 0x03, 0x00, 0x09 };

        byte[] receiveData2 = new byte[] { 0x11 };

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        buffer.write(receiveData);

        NetMessageHandler messageReceiver = Mockito.mock(NetMessageHandler.class);
        InetAddress address = InetAddress.getLoopbackAddress();
        int portNumber = 12345;

        NetBidibTcpServerSocketHandler handler = Mockito.mock(NetBidibTcpServerSocketHandler.class);
        Mockito.doCallRealMethod().when(handler).parsePackets(Mockito.any(), Mockito.any(NetMessageHandler.class),
            Mockito.any(InetAddress.class), Mockito.anyInt());

        handler.parsePackets(buffer, messageReceiver, address, portNumber);
        buffer.write(receiveData2);
        handler.parsePackets(buffer, messageReceiver, address, portNumber);

        Mockito.verify(messageReceiver, Mockito.times(3)).receive(Mockito.any());
    }

    @Test
    public void parsePacketsStream4() throws IOException, ProtocolException {
        LOGGER.info("Parse packets from received data.");

        // The message begins with the BIDIB_WIZARD_NET_PREFIX and contains 1 message
        byte[] receiveData =
            new byte[] { 0x0C, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0xDA, 0x00, 0x0D, 0x68, 0x00, 0x64,
                (byte) 0xEA, /**/ 0x05, 0x00, 0x00, (byte) 0x81, (byte) 0xFE, (byte) 0xAF };

        LOGGER.info("Prepared received data: {}", ByteUtils.bytesToHex(receiveData));

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        buffer.write(receiveData);

        NetMessageHandler messageReceiver = Mockito.mock(NetMessageHandler.class);
        InetAddress address = InetAddress.getLoopbackAddress();
        int portNumber = 12345;

        NetBidibTcpServerSocketHandler handler = Mockito.mock(NetBidibTcpServerSocketHandler.class);
        Mockito.doCallRealMethod().when(handler).parsePackets(Mockito.any(), Mockito.any(NetMessageHandler.class),
            Mockito.any(InetAddress.class), Mockito.anyInt());

        handler.parsePackets(buffer, messageReceiver, address, portNumber);

        Mockito.verify(messageReceiver, Mockito.times(2)).receive(Mockito.any());
    }

    @Test
    public void parsePacketsStream5() throws IOException, ProtocolException {
        LOGGER.info("Parse packets from received data.");

        // The message begins with the BIDIB_WIZARD_NET_PREFIX and contains 4 messages
        byte[] receiveData =
            new byte[] { 0x0C, 0x00, 0x02, (byte) 0x89, 0x01, 0x00, (byte) 0xDA, 0x00, 0x0D, 0x68, 0x00, 0x64,
                (byte) 0xEA, /**/ 0x03, 0x00, 0x09, 0x11, 0x03, 0x00, 0x0A, 0x11, 0x03, 0x00, 0x0B, 0x11, 0x03, 0x00,
                0x0C, 0x11 };

        LOGGER.info("Prepared received data: {}", ByteUtils.bytesToHex(receiveData));

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        buffer.write(receiveData);

        NetMessageHandler messageReceiver = Mockito.mock(NetMessageHandler.class);
        InetAddress address = InetAddress.getLoopbackAddress();
        int portNumber = 12345;

        NetBidibTcpServerSocketHandler handler = Mockito.mock(NetBidibTcpServerSocketHandler.class);
        Mockito.doCallRealMethod().when(handler).parsePackets(Mockito.any(), Mockito.any(NetMessageHandler.class),
            Mockito.any(InetAddress.class), Mockito.anyInt());

        handler.parsePackets(buffer, messageReceiver, address, portNumber);

        Mockito.verify(messageReceiver, Mockito.times(5)).receive(Mockito.any());
    }

}
