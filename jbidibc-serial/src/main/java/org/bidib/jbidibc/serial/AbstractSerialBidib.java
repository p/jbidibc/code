package org.bidib.jbidibc.serial;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.AbstractBidib;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.exception.ProtocolNoAnswerException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.node.NodeRegistry;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the abstract serial bidib implementation. It creates and initializes the MessageReceiver and the NodeFactory
 * that is used in the system.
 */
public abstract class AbstractSerialBidib extends AbstractBidib {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSerialBidib.class);

    protected Semaphore portSemaphore = new Semaphore(1);

    protected String requestedPortName;

    protected Object receiveLock = new Object();

    protected boolean firstPacketSent = false;

    @Override
    protected BidibMessageProcessor createMessageReceiver(NodeRegistry nodeRegistry) {
        return new SerialMessageReceiver(nodeRegistry);
    }

    @Override
    protected void stopReceiverAndQueues(final BidibMessageProcessor serialMessageReceiver) {
        if (serialMessageReceiver instanceof SerialMessageReceiver) {
            // no longer process received messages
            ((SerialMessageReceiver) serialMessageReceiver).disable();
        }
        else {
            LOGGER.warn("No message receiver to disable available.");
        }

        super.stopReceiverAndQueues(serialMessageReceiver);
    }

    @Override
    protected void cleanupAfterClose(final BidibMessageProcessor bidibMessageProcessor) {
        releaseRootNode();

        firstPacketSent = false;

        InvalidConfigurationException ice = null;
        if (bidibMessageProcessor instanceof SerialMessageReceiver) {
            SerialMessageReceiver serialMessageReceiver = (SerialMessageReceiver) bidibMessageProcessor;
            serialMessageReceiver.clearMessageListeners();
            serialMessageReceiver.clearNodeListeners();

            LOGGER.info("Purge the received data in the message buffer.");

            try {
                serialMessageReceiver.purgeReceivedDataInBuffer();
            }
            catch (InvalidConfigurationException ex) {
                LOGGER.warn("Purge output stream has signaled an error.", ex);

                if ("debug-interface-active".equals(ex.getReason())) {
                    ice = ex;
                }
            }
        }
        else {
            LOGGER.warn("No message receiver to purge received data buffer available.");
        }

        if (getConnectionListener() != null) {
            LOGGER.info("Notify that the port was closed: {}", requestedPortName);
            getConnectionListener().closed(requestedPortName);
        }
        else {
            LOGGER.info("No connection listener available to publish the closed report for port: {}",
                requestedPortName);
        }

        requestedPortName = null;

        super.cleanupAfterClose(bidibMessageProcessor);

        if (ice != null) {
            LOGGER.warn("Signal the invalid configuration exception to the caller.");
            throw ice;
        }
    }

    protected void startReceiverAndQueues(final BidibMessageProcessor serialMessageReceiver, final Context context) {

        super.startReceiverAndQueues(serialMessageReceiver, context);

        // TODO remove this????
        if (serialMessageReceiver instanceof SerialMessageReceiver) {
            // enable the message receiver before the event listener is added
            ((SerialMessageReceiver) serialMessageReceiver).enable();
        }
        else {
            LOGGER.warn("No message receiver to enable available.");
        }

    }

    protected abstract boolean isImplAvaiable();

    protected abstract void internalOpen(String portName, final Context context) throws Exception;

    /**
     * @param messageKey
     *            the message key to notify
     */
    protected void notifyStatusKey(String messageKey) {

        if (getConnectionListener() != null) {
            getConnectionListener().status(messageKey);
        }
        else {
            LOGGER.info("No connection listener available.");
        }
    }

    @Override
    public void open(
        String portName, ConnectionListener connectionListener, Set<NodeListener> nodeListeners,
        Set<MessageListener> messageListeners, Set<TransferListener> transferListeners, Context context)
        throws PortNotFoundException, PortNotOpenedException {

        setConnectionListener(connectionListener);

        firstPacketSent = false;

        // register the listeners
        registerListeners(nodeListeners, messageListeners, transferListeners);

        if (!isImplAvaiable()) {
            if (StringUtils.isBlank(portName)) {
                throw new PortNotFoundException("No portName to open provided!");
            }

            LOGGER.info("Open port with name: {}", portName);

            File file = new File(portName);

            if (file.exists()) {

                Boolean symlink = context.get("symlink", Boolean.class, Boolean.FALSE);
                if (!symlink) {
                    try {
                        portName = file.getCanonicalPath();
                        LOGGER.info("Changed port name to: {}", portName);
                    }
                    catch (IOException ex) {
                        throw new PortNotFoundException(portName);
                    }
                }
                else {
                    LOGGER.info("Do not replace symlink name.");
                }
            }

            requestedPortName = portName;

            int[] baudRates = { 115200, 19200 };
            int baudRateIndex = 0;

            try {
                portSemaphore.acquire();

                while (baudRateIndex < baudRates.length) {
                    try {
                        // make sure the port is closed
                        close();

                        // open the commPort
                        Integer baudRate = baudRates[baudRateIndex];
                        context.register("serial.baudrate", baudRate);

                        LOGGER.info("Try to connect with baudRate: {}", context.get("serial.baudrate"));

                        internalOpen(portName, context);

                        LOGGER.info("The port was opened internally, get the magic.");

                        contactInterface();

                        if (getConnectionListener() != null) {
                            LOGGER.info("Notify that the port was opened: {}", requestedPortName);
                            getConnectionListener().opened(requestedPortName);
                        }

                        break;
                    }
                    catch (ProtocolNoAnswerException naex) {
                        LOGGER.warn("Open communication failed because the interface did not send an answer.", naex);
                        try {
                            close();
                        }
                        catch (Exception e4) { // NOSONAR
                            // ignore
                        }

                        // check if we could retry with another baudrate
                        if (baudRateIndex < (baudRates.length - 1)) {
                            baudRateIndex++;
                            LOGGER.info("Try to connect with the next baudRate: {}", baudRates[baudRateIndex]);

                            notifyStatusKey("bidib-reconnect-with-next-baudrate");

                            try {
                                Thread.sleep(150);
                            }
                            catch (InterruptedException ie) {
                                LOGGER.warn("Wait to display status message was interrupted.", ie);
                            }
                            continue;
                        }

                        throw new NoAnswerException(naex.getMessage());
                    }
                    catch (NoAnswerException naex) {
                        LOGGER.warn("Open communication failed.", naex);
                        try {
                            close();
                        }
                        catch (Exception e4) { // NOSONAR
                            // ignore
                        }
                        throw naex;
                    }
                    catch (PortNotFoundException pnfEx) {
                        LOGGER.info("Open port failed. Close port and throw exception.", pnfEx);

                        // close port to cleanup and stop the send queue worker

                        try {
                            close();
                        }
                        catch (Exception e3) { // NOSONAR
                            LOGGER.warn("Close port failed.", e3);
                        }
                        throw new PortNotOpenedException(portName, PortNotOpenedException.PORT_NOT_FOUND);
                    }
                    catch (IOException e2) {
                        LOGGER.info("Open port failed with IOException. Close port and throw exception.", e2);

                        String reason = e2.getMessage();

                        // close port to cleanup and stop the send queue worker

                        try {
                            close();
                        }
                        catch (Exception e3) { // NOSONAR
                            LOGGER.warn("Close port failed but this is ignored.", e3);
                        }
                        throw new PortNotOpenedException(portName,
                            (StringUtils.isNotBlank(reason) ? reason : PortNotOpenedException.UNKNOWN));
                    }
                    catch (Exception e2) {
                        LOGGER.info("Open port failed. Close port and throw exception.", e2);

                        // close port to cleanup and stop the send queue worker

                        try {
                            close();
                        }
                        catch (Exception e3) { // NOSONAR
                            LOGGER.warn("Close port failed.", e3);
                        }
                        throw new PortNotOpenedException(portName, PortNotOpenedException.UNKNOWN);
                    }
                    catch (UnsatisfiedLinkError err) {
                        LOGGER.info("Open port failed. Close port and throw exception.", err);

                        throw new PortNotOpenedException(portName, PortNotOpenedException.UNKNOWN);
                    }
                }
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Wait for portSemaphore was interrupted.", ex);
                throw new PortNotOpenedException(portName, PortNotOpenedException.UNKNOWN);
            }
            finally {
                portSemaphore.release();
            }
        }
        else {
            LOGGER.warn("Port is already opened.");
        }
    }

    private AtomicBoolean isConnected = new AtomicBoolean();

    protected boolean isConnected() {
        return isConnected.get();
    }

    protected void setConnected(boolean connected) {
        isConnected.set(connected);
    }

    @Override
    public abstract boolean isOpened();

    /**
     * Contact the interface and get the magic
     * 
     * @return the magic
     * @throws ProtocolException
     *             a ProtocolNoAnswerException is thrown if the interface node does not answer
     */
    protected int contactInterface() throws ProtocolException {

        int magic = sendResetAndMagic();

        LOGGER.info("The root node returned the magic: {}", ByteUtils.magicToHex(magic));
        return magic;
    }

    /**
     * Get the magic from the root node
     * 
     * @return the magic provided by the root node
     * @throws ProtocolException
     */
    private int sendResetAndMagic() throws ProtocolException {
        RootNode rootNode = getRootNode();

        LOGGER.info("Send sysDisable to the rootNode.");
        rootNode.sysDisable();
        // LOGGER.info("Send reset to the rootNode.");
        // rootNode.reset();

        try {
            LOGGER.info("Wait 300ms before send the magic request.");
            Thread.sleep(300);
        }
        catch (InterruptedException ex) {
            LOGGER.warn("Wait before send the magic request failed.", ex);
        }

        final BidibMessageProcessor serialMessageReceiver = getMessageReceiver();

        if (serialMessageReceiver instanceof SerialMessageReceiver) {
            LOGGER.info("Enable the message receiver before send magic: {}", serialMessageReceiver);
            // enable the message receiver
            ((SerialMessageReceiver) serialMessageReceiver).enable();
        }

        LOGGER.info("Send the magic request.");
        int magic = rootNode.getMagic(1500);

        LOGGER.debug("The node returned magic: {}", magic);
        return magic;
    }
}
