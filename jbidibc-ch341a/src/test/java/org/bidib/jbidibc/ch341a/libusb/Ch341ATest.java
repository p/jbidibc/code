package org.bidib.jbidibc.ch341a.libusb;

import org.apache.commons.lang3.SystemUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.usb4java.Device;
import org.usb4java.DeviceHandle;
import org.usb4java.LibUsb;

public class Ch341ATest {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ch341ATest.class);

    @BeforeTest
    public void prepare() throws Exception {
        LOGGER.info("Prepare Ch341ATest.");
        if (!SystemUtils.IS_OS_WINDOWS) {

            LOGGER.info("Prepare Ch341ATest, skip test because OS is: {}", SystemUtils.OS_NAME);
            throw new SkipException("Test should only run under Windows OS");
        }

        boolean deviceAttached = false;

        try (Ch341A ch341a = new Ch341A()) {
            ch341a.init(LibUsb.LOG_LEVEL_INFO);
            Device device = ch341a.findDevice();
            Assert.assertNotNull(device);

            deviceAttached = true;
        }
        catch (RuntimeException ex) {
            LOGGER.warn("No device attached.", ex);
        }

        if (!deviceAttached) {
            throw new SkipException("Test should only run if a CH341A device in I2C mode is attached.");
        }
    }

    @Test(enabled = true)
    public void readLocoCardTest() throws Exception {

        DeviceHandle handle = null;

        byte[] eepromContent = null;

        try (Ch341A ch341a = new Ch341A()) {

            ch341a.init(LibUsb.LOG_LEVEL_INFO);

            handle = ch341a.openDevice();

            // set the i2c bus speed (speed: 0 = 20kHz; 1 = 100kHz, 2 = 400kHz, 3 = 750kHz)
            int speed = 1;
            ch341a.setStreamSpeed(handle, speed);

            // read the eeprom content
            eepromContent = ch341a.ch341readEEPROM(handle, 8192);

            Assert.assertNotNull(eepromContent);
            Assert.assertEquals(eepromContent.length, 8192);

            Assert.assertEquals(eepromContent[0], ByteUtils.getLowByte(0x02));
            Assert.assertEquals(eepromContent[1], ByteUtils.getLowByte(0xC5));
        }

        // File temp = new File("/testdata.txt");
        // try (FileOutputStream fos = new FileOutputStream(temp)) {
        // IOUtils.write(eepromContent, fos);
        // fos.flush();
        // }
    }
}
