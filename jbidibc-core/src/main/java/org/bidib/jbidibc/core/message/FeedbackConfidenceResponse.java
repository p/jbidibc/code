package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackConfidenceResponse extends BidibMessage {
    public static final Integer TYPE = BidibLibrary.MSG_BM_CONFIDENCE;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackConfidenceResponse.class);

    FeedbackConfidenceResponse(byte[] addr, int num, int type, byte... data) throws ProtocolException {
        super(addr, num, type, data);
        if (data == null || data.length < 3) {
            throw new ProtocolException("No valid MSG_BM_CONFIDENCE received.");
        }
        LOGGER.debug("Received confidence response, invalid: {}, freeze: {}, noSignal: {}", getInvalid(), getFreeze(),
            getNoSignal());
    }

    public FeedbackConfidenceResponse(byte[] addr, int num, byte valid, byte freeze, byte signal)
        throws ProtocolException {
        this(addr, num, BidibLibrary.MSG_BM_CONFIDENCE, new byte[] { valid, freeze, signal });
    }

    public String getName() {
        return "MSG_BM_CONFIDENCE";
    }

    public int getInvalid() {
        return ByteUtils.getInt(getData()[0]);
    }

    public int getFreeze() {
        return ByteUtils.getInt(getData()[1]);
    }

    public int getNoSignal() {
        return ByteUtils.getInt(getData()[2]);
    }

}
