package org.bidib.jbidibc.core;

import java.io.ByteArrayInputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.accessory.ByteOptionsValue;
import org.bidib.jbidibc.core.accessory.OptionsValue;
import org.bidib.jbidibc.core.enumeration.AccessoryStateOptionsKeys;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessoryStateOptions {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryStateOptions.class);

    private final byte[] optionsData;

    public class Options {

        // the values are stored in a map
        private Map<Byte, OptionsValue<?>> values = new LinkedHashMap<>();

        public <T> T getOptionsValue(Byte key) {
            return (T) values.get(key).getValue();
        }

        public <T> T getOptionsValue(AccessoryStateOptionsKeys optionsKey) {
            return (T) values.get(Byte.valueOf(optionsKey.getType()));
        }

    }

    public AccessoryStateOptions(byte[] data, int offset) {
        optionsData = ByteUtils.subArray(data, offset);

        LOGGER.info("Current optionsData: {}", ByteUtils.bytesToHex(optionsData));
    }

    public byte[] getOptionsData() {
        return optionsData;
    }

    public Options getOptions() {
        Options options = null;
        // get the values
        if (optionsData.length > 0) {
            try (ByteArrayInputStream bais = new ByteArrayInputStream(optionsData)) {

                options = new Options();
                while (bais.available() > 0) {
                    byte pEnum = ByteUtils.getLowByte(bais.read());
                    int bytesRead = 0;

                    if ((pEnum & 0x80) == 0x80) {

                    }
                    else if ((pEnum & 0x40) == 0x40) {
                        // int16
                    }
                    else {
                        // byte
                        byte[] byteVal = new byte[1];
                        bytesRead = bais.read(byteVal);
                        LOGGER.info("Read a byte (0x{}) value: {}, bytesRead: {}, pEnum: {}",
                            ByteUtils.byteToHex(pEnum), ByteUtils.toString(byteVal), bytesRead,
                            formatOptionsKey(pEnum));

                        Byte byteValue = Byte.valueOf(byteVal[0]);
                        options.values.put(pEnum, new ByteOptionsValue(byteValue));
                    }
                }

            }
            catch (Exception ex) {
                LOGGER.warn("Read content of accessory state options failed.", ex);
            }
        }
        return options;
    }

    private static String formatOptionsKey(byte pEnum) {
        try {
            AccessoryStateOptionsKeys pck = AccessoryStateOptionsKeys.valueOf(pEnum);
            return pck.name();
        }
        catch (Exception ex) {
            LOGGER.warn("Convert pEnum to AccessoryStateOptionsKeys failed: {}", ex.getMessage());
            return ByteUtils.byteToHex(pEnum);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[ ");
        sb.append(getClass().getSimpleName());
        sb.append("optionsData: ").append(ByteUtils.bytesToHex(optionsData));
        sb.append("]");
        return sb.toString();
    }
}
