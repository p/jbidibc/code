package org.bidib.jbidibc.core.exception;

public class ProtocolInvalidParamException extends ProtocolException {
    private static final long serialVersionUID = 1L;

    private final int parameter;

    public ProtocolInvalidParamException(String message, int parameter) {
        super(message);
        this.parameter = parameter;
    }

    /**
     * @return the parameter
     */
    public int getParameter() {
        return parameter;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder(super.getMessage());
        sb.append(" The requested parameter: ").append(parameter);
        return sb.toString();
    }
}
