package org.bidib.jbidibc.core;

import java.util.ArrayList;
import java.util.List;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoftwareVersion implements Comparable<SoftwareVersion> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SoftwareVersion.class);

    protected final int firstVersion;

    protected final int secondVersion;

    protected final int thirdVersion;
    
    private List<SoftwareVersion> childrenVersions;

    public SoftwareVersion(int firstVersion, int secondVersion, int thirdVersion) {
        this.firstVersion = firstVersion;
        this.secondVersion = secondVersion;
        this.thirdVersion = thirdVersion;
    }

    /**
	 * @return the childrenVersions
	 */
	public List<SoftwareVersion> getChildrenVersions() {
		return childrenVersions;
	}

	/**
	 * @param childrenVersions the childrenVersions to set
	 */
	public void setChildrenVersions(List<SoftwareVersion> childrenVersions) {
		this.childrenVersions = childrenVersions;
	}

	/**
	 * @param childVersion the childVersion to add
	 */
	public void addChildVersion(SoftwareVersion childVersion) {
		if (childrenVersions == null) {
			childrenVersions = new ArrayList<>();
		}
		childrenVersions.add(childVersion);
	}

	public Integer toInt() {
        return (int) (firstVersion << 16) | (secondVersion << 8) | thirdVersion;
    }

    public byte[] asByteArray() {
        return new byte[] { ByteUtils.getLowByte(thirdVersion), ByteUtils.getLowByte(secondVersion),
            ByteUtils.getLowByte(firstVersion) };
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(String.format("%d.%02d.%02d", firstVersion, secondVersion, thirdVersion));
        if (childrenVersions != null) {
        	for (SoftwareVersion child : childrenVersions) {
        		sb.append(", ").append(child);
        	}
        }
        return sb.toString();
    }

    @Override
    public int compareTo(SoftwareVersion version) {
        return toInt().compareTo(version.toInt());
    }

    public boolean isLowerThan(SoftwareVersion versionToCompare) {
        return toInt() < versionToCompare.toInt();
    }

    public boolean isHigherThan(SoftwareVersion versionToCompare) {
        return toInt() > versionToCompare.toInt();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SoftwareVersion) {
            return toInt().equals(((SoftwareVersion) obj).toInt());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toInt();
    }

    public static SoftwareVersion build(int firstVersion, int secondVersion, int thirdVersion) {
        return new SoftwareVersion(firstVersion, secondVersion, thirdVersion);
    }

    public static SoftwareVersion parse(String version) {
        try {
            String[] splited = version.split("\\.");

            return new SoftwareVersion(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]),
                Integer.parseInt(splited[2]));
        }
        catch (Exception ex) {
            LOGGER.warn("Parse software version failed: {}", version, ex);
            throw new IllegalArgumentException("The provided version string is not a valid SoftwareVersion: " + version);
        }
    }
}
