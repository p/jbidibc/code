package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;

public class VendorGetMessage extends BidibCommandMessage {

    // private String cvName;

    protected VendorGetMessage(String cvName) {
        super(0, BidibLibrary.MSG_VENDOR_GET, ByteUtils.bstr(cvName));
        // this.cvName = cvName;
    }

    public VendorGetMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_VENDOR_GET";
    }

    public String getVendorDataName() {
        return ByteUtils.cstr(getData(), 0);
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return new Integer[] { VendorResponse.TYPE };
    }

    // @Override
    // public void setAnswerSize(int answerSize) {
    //
    // if (StringUtils.isNumeric(cvName)) {
    // // default processing for numbers
    // super.setAnswerSize(12);
    // }
    // else {
    // // the cvName is alphanumeric
    // super.setAnswerSize(answerSize);
    // }
    // }
}
