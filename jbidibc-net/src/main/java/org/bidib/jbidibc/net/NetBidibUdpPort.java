package org.bidib.jbidibc.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetBidibUdpPort implements NetBidibPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetBidibUdpPort.class);

    private final DatagramSocket datagramSocket;

    private final NetMessageHandler messageReceiver;

    private AtomicBoolean runEnabled = new AtomicBoolean();

    private int currentSessionKey;

    private boolean sessionKeyAvailable;

    private int sequence;

    /**
     * Creates a new instance of {@code NetBidibUdpPort} for clients.
     * 
     * @param portNumber
     *            the port number
     * @param messageReceiver
     *            the message receiver
     * @throws SocketException
     */
    public NetBidibUdpPort(NetMessageHandler messageReceiver) throws SocketException {
        this(null, messageReceiver);
    }

    /**
     * Creates a new instance of {@code NetBidibUdpPort}. For client port the port number is omitted and set to
     * {@code null}.
     * 
     * @param portNumber
     *            the port number
     * @param messageReceiver
     *            the message receiver
     * @throws SocketException
     */
    public NetBidibUdpPort(Integer portNumber, NetMessageHandler messageReceiver) throws SocketException {
        if (portNumber != null) {
            this.datagramSocket = new DatagramSocket(portNumber);
            LOGGER.info("Created new datagram socket on portNumber: {}", portNumber);
        }
        else {
            this.datagramSocket = new DatagramSocket();
            LOGGER.info("Created new datagram socket without portNumber");
        }
        this.messageReceiver = messageReceiver;
    }

    private ByteArrayOutputStream receiveBuffer = new ByteArrayOutputStream(100);

    private static final int UDP_PAKET_HEADER_LEN = 4;

    @Override
    public void run() {
        LOGGER.info("Start the port.");
        runEnabled.set(true);

        byte[] receiveData = new byte[1024];
        try {
            while (runEnabled.get()) {
                // wait for client sending data
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                LOGGER.debug("Wait to receive a datagram packet.");

                datagramSocket.receive(receivePacket);

                // for the first magic response we need special processing because we need to keep the session key
                if (!sessionKeyAvailable) {
                    // TODO verify that the session key is correct prepared
                    currentSessionKey = ByteUtils.getInt(receivePacket.getData()[0], receivePacket.getData()[1]);
                    int receiveSequence = ByteUtils.getInt(receivePacket.getData()[2], receivePacket.getData()[3]);

                    LOGGER.info("Received the sessionKey: {}, current receiveSequence: {}", currentSessionKey,
                        receiveSequence);

                    sessionKeyAvailable = true;
                }

                // forward processing to handler
                if (messageReceiver != null) {
                    LOGGER.info("Received a datagram packet. Forward packet to messageReveiver: {}, len: {}, data: {}",
                        messageReceiver, receivePacket.getLength(),
                        ByteUtils.bytesToHex(receivePacket.getData(), receivePacket.getLength()));

                    InetAddress address = receivePacket.getAddress();
                    int portNumber = receivePacket.getPort();

                    receiveBuffer.write(receivePacket.getData(), UDP_PAKET_HEADER_LEN,
                        receivePacket.getLength() - UDP_PAKET_HEADER_LEN);

                    // problem here is the UDP packet header
                    try {
                        NetBidibPacketUtils.parsePackets(receiveBuffer, messageReceiver, address, portNumber);
                    }
                    catch (Exception pex) {
                        LOGGER.warn("Receive message failed.", pex);
                    }

                    // do not clear the receiveBuffer here because this is managed from within parsePackets
                }
                else {
                    LOGGER.warn("No message receiver configured, received packet: {}", receivePacket);
                }
            }
        }
        catch (IOException ex) {
            if (runEnabled.get()) {
                LOGGER.warn("--- Interrupt NetBidibPort-run", ex);
            }
            else {
                LOGGER.info("The NetBidibPort worker is terminating.");
            }
        }

        LOGGER.info("Receiver thread has finished.");
    }

    public void stop() {
        LOGGER.info("Stop the datagram packet receiver.");
        runEnabled.set(false);

        datagramSocket.close();
    }

    private ByteArrayOutputStream sendDataBuffer = new ByteArrayOutputStream(100);

    /**
     * Send the data to the host.
     * 
     * @param sendData
     *            the data to send
     * @param address
     *            the receiving address of the host
     * @param portNumber
     *            the receiving port number of the host
     * @throws IOException
     */
    public void send(byte[] sendData, InetAddress address, int portNumber) throws IOException {
        // if (LOGGER.isTraceEnabled()) {
        LOGGER.info("Send data to socket, port: {}, len: {}, bytes: {}", portNumber, sendData.length,
            ByteUtils.bytesToHex(sendData));
        // }

        // send the data to the host
        // add the UDP packet wrapper ...
        sendDataBuffer.write(ByteUtils.getLowByte(currentSessionKey));
        sendDataBuffer.write(ByteUtils.getHighByte(currentSessionKey));
        sendDataBuffer.write(ByteUtils.getLowByte(sequence));
        sendDataBuffer.write(ByteUtils.getHighByte(sequence));
        sendDataBuffer.write(sendData);

        LOGGER.info("Send data to address: {}, portNumber: {}, sessionKey: {}, sequence: {}, sendData: {}", address,
            portNumber, currentSessionKey, sequence, ByteUtils.bytesToHex(sendDataBuffer));

        // send the data to the host
        sendBuffer(sendDataBuffer, address, portNumber);
    }

    private void sendBuffer(ByteArrayOutputStream sendData, InetAddress address, int portNumber) throws IOException {
        LOGGER.info("Send data to socket, address: {}, portNumber: {}, len: {}, sendData: {}", address, portNumber,
            sendData.size(), ByteUtils.bytesToHex(sendData));

        try {
            DatagramPacket sendPacket =
                new DatagramPacket(sendData.toByteArray(), sendData.size(), address, portNumber);
            datagramSocket.send(sendPacket);

            // increment the sequence counter after sending the message sucessfully
            prepareNextSequence();
        }
        finally {
            sendData.reset();
        }
    }

    private void prepareNextSequence() {
        sequence++;
        if (sequence > 65535) {
            sequence = 0;
        }

        LOGGER.info("Prepared next UDP sequence: {}", sequence);
    }

}
