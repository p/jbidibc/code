package org.bidib.jbidibc;

import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.node.BidibNode;

import com.beust.jcommander.Parameters;

/**
 * This commands gets the list of nodes from the specified port.
 * 
 */
@Parameters(separators = "=")
public class GetNodeList extends BidibCommand {
    public static void main(String[] args) {

        run(new GetNodeList(), args);
    }

    public int execute() {
        int result = 20;

        try {
            openPort(getPortName(), null);

            BidibNode rootNode = getBidib().getRootNode();
            int count = rootNode.getNodeCount();
            System.out.println("Number of nodes: " + count); // NOSONAR

            for (int index = 1; index <= count; index++) {
                Node node = rootNode.getNextNode();
                System.out.println("Found node: " + node); // NOSONAR
            }
            result = 0;

            releaseBidib();
        }
        catch (PortNotFoundException ex) {
            System.err
                .println("The provided port was not found: " + ex.getMessage() // NOSONAR
                    + ". Verify that the BiDiB device is connected.");
        }
        catch (Exception ex) {
            System.err.println("Get list of nodes failed: " + ex); // NOSONAR
            ex.printStackTrace();
        }
        return result;
    }
}
