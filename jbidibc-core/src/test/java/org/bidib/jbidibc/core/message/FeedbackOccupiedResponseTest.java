package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.exception.ProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FeedbackOccupiedResponseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackOccupiedResponseTest.class);

    @Test
    public void getDetectorNumber() throws ProtocolException {
        byte[] message = new byte[] { 0x04, 0x00, (byte) 0x1F, (byte) 0xA0, 0x03 };
        BidibMessage result = new BidibMessage(message);

        FeedbackOccupiedResponse feedbackOccupiedResponse =
            new FeedbackOccupiedResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(feedbackOccupiedResponse);
        LOGGER.info("Prepared feedbackFreeResponse: {}", feedbackOccupiedResponse);

        Assert.assertEquals(feedbackOccupiedResponse.getDetectorNumber(), 3);
        Assert.assertNull(feedbackOccupiedResponse.getTimestamp());
    }

    @Test
    public void getDetectorNumberWithTime() throws ProtocolException {
        byte[] message = new byte[] { 0x06, 0x00, (byte) 0x1F, (byte) 0xA0, 0x03, 0x12, 0x34 };
        BidibMessage result = new BidibMessage(message);

        FeedbackOccupiedResponse feedbackOccupiedResponse =
            new FeedbackOccupiedResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());

        Assert.assertNotNull(feedbackOccupiedResponse);
        LOGGER.info("Prepared feedbackFreeResponse: {}", feedbackOccupiedResponse);

        Assert.assertEquals(feedbackOccupiedResponse.getDetectorNumber(), 3);
        Assert.assertNotNull(feedbackOccupiedResponse.getTimestamp());
        Assert.assertEquals(feedbackOccupiedResponse.getTimestamp(), Long.valueOf(0x3412L));
    }

    @Test(expectedExceptions = {
        ProtocolException.class }, expectedExceptionsMessageRegExp = "No valid MSG_BM_OCC received.")
    public void getDetectorNumberInvalid() throws ProtocolException {
        byte[] message = new byte[] { 0x03, 0x00, (byte) 0x1F, (byte) 0xA0 /* , 0x03, 0x03 */ };
        BidibMessage result = new BidibMessage(message);

        // construction of message will throw exception
        new FeedbackOccupiedResponse(result.getAddr(), result.getNum(), result.getType(), result.getData());
    }
}
