package org.bidib.jbidibc.core.message;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command to trigger an action in the mobile decoder.
 */
public class CommandStationBinaryStateMessage extends BidibCommandMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationBinaryStateMessage.class);

    public CommandStationBinaryStateMessage(int address, int state, boolean value) {
        super(0, BidibLibrary.MSG_CS_BIN_STATE, new byte[] { (byte) (address & 0xFF), (byte) ((address & 0xFF00) >> 8),
            (byte) (state & 0xFF), (byte) ((state & 0xFF00) >> 8), value ? (byte) 1 : (byte) 0 });
    }

    public CommandStationBinaryStateMessage(byte[] message) throws ProtocolException {
        super(message);
    }

    public String getName() {
        return "MSG_CS_BIN_STATE";
    }

    @Override
    public Integer[] getExpectedResponseTypes() {
        return new Integer[] { CommandStationDriveAcknowledgeResponse.TYPE };
    }

    public AddressData getDecoderAddress() {
        int index = 0;
        byte lowByte = getData()[index++];
        byte highByte = getData()[index++];
        int address = ByteUtils.getWord(lowByte, highByte);

        AddressTypeEnum addressTypeEnum = AddressTypeEnum.LOCOMOTIVE_BACKWARD;
        AddressData addressData = new AddressData(address, addressTypeEnum);
        LOGGER.debug("Prepared address data: {}", addressData);
        return addressData;
    }

    public int getBinaryStateNumber() {
        int index = 2;
        byte lowByte = getData()[index++];
        byte highByte = getData()[index++];
        int binStateNumber = ByteUtils.getWord(lowByte, highByte);
        return binStateNumber;
    }

    public int getBinaryStateValue() {
        int binStateValue = ByteUtils.getInt(getData()[4]);
        return binStateValue;
    }
}
